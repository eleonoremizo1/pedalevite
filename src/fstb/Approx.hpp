/*****************************************************************************

        Approx.hpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#if ! defined (fstb_Approx_CODEHEADER_INCLUDED)
#define fstb_Approx_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/bit_cast.h"
#include "fstb/BitEquiv.h"
#include "fstb/Poly.h"
#include "fstb/Vs32.h"
#include "fstb/Vx32_conv.hpp"

#include <limits>

#include <cassert>



namespace fstb
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
Approximates 1 / x
P  max rel error
0: 5.05e-2
1: 1.31e-3
2: 1.09e-6. Not really faster than 1.f / x (x86-64)
3: 1.34e-7
For some reason, accuracy on ARMv7 is slightly better than on x86, but speed
is worse compared to the built-in 1 / x. Only P == 0 seems to be of interest.

Ref:
Leonid Moroz and Volodymyr Samotyy,
Efficient Floating-Point Division for Digital Signal Processing Application,
IEEE Signal Processing Magazine, 2019-01
*/

template <int P>
float	Approx::rcp (float x) noexcept
{
	static_assert (
		(P >= 0 && P <= 3),
		"The number of Newton iterations must be in [0 ; 3]"
	);
	assert (x != 0);

	constexpr auto cs1 = int32_t ((P == 3) ? 0x7EB210D8 : 0x7EF311C3);

	const auto     i = bit_cast <int32_t> (x);
	auto           y = bit_cast <float> (cs1 - i);

	if constexpr (P == 3)
	{
		// Using std::fmaf as suggested in the paper is actually much slower
		// for little extra precision. Better use standard operations.
		auto           fma = [] (float t, float a, float b) noexcept
		{
			return t * a + b;
		};
		auto           fms = [] (float t, float a, float b) noexcept
		{
			return b - t * a;
		};

		constexpr auto cs2 = (int32_t (1) << 23) + cs1;
		const auto     yy  = bit_cast <float> (cs2 - i);
		y = yy * (1.41430846f - x * y);
		const float    r  = fms (x, y, 1.f);
		y = fma (r, y, y);
	}
	else
	{
		// For P > 0, we could use the same beginning as P == 3, but this would
		// lower significantly the speed, bringing the 1-iteration speed to the
		// same level as the 2-iter. Therefore it's probably more interesting
		// to spread evenly both speed and accuracy.
		if constexpr (P > 0) { y *= 2.00130856f - x * y; }
		if constexpr (P > 1) { y *= 2.00000084f - x * y; }
	}

	return y;
}



/*
Approximates 1 / x
P  max rel error
0: 5.05e-02
1: 1.34e-04
2: 9.01e-09. Barely faster than 1.0 / x (x86-64)
3: 3.54e-16. Same, for ARMv7
4: 2.22e-16

Refs:
Leonid Moroz and Volodymyr Samotyy,
Efficient Floating-Point Division for Digital Signal Processing Application,
IEEE Signal Processing Magazine, 2019-01
Paul Khuong
0x7FDE623822FC16E6 : a magic constant for double float reciprocal, 2011-03-16
https://pvk.ca/Blog/LowLevel/software-reciprocal.html
*/

template <int P>
double	Approx::rcp (double x) noexcept
{
	static_assert (
		(P >= 0 && P <= 4),
		"The number of Newton iterations must be in [0 ; 4]"
	);
	assert (x != 0);

	constexpr auto cs1 = int64_t (
		(P == 0) ? 0x7FDE623822FC16E6 : 0x7FD6421Af0901626
	);

	const auto     i = bit_cast <int64_t> (x);
	auto           y = bit_cast <double> (cs1 - i);
	if constexpr (P > 0)
	{
		constexpr auto cs2 = (int64_t (1) << 52) + cs1;
		const auto     yy  = bit_cast <double> (cs2 - i);
		y = yy * (1.4143084573400108 - x * y);
	}
	if constexpr (P > 1) { y  =  y * (2.0000000090062634 - x * y); }
	if constexpr (P > 3) { y  =  y * (2                  - x * y); }
	if constexpr (P > 2)
	{
		// Using std::fma as suggested in the paper is actually much slower
		// for little extra precision. Better use standard operations.
		auto           fms = [] (double t, double a, double b) noexcept
		{
			return b - t * a;
		};
		y += y * fms (x, y, 1.0);
	}

	return y;
}



/*
Approximates 1 / sqrt (x)
P  max rel error
0: 3.5e-2
1: 6.5e-4
2: 4.9e-7
3: 9.77e-8. Not really faster than 1.f / sqrtf (x)

Refs:
Chris Lomont, Fast Inverse Square Root,
http://lomont.org/papers/2003/InvSqrt.pdf, 2003-02
Jan Kadlec,
Improving the fast inverse square root,
http://rrrola.wz.cz/inv_sqrt.html, 2010
Matthew Robertson,
A Brief History of InvSqrt,
https://mrober.io/papers/rsqrt.pdf, 2012
*/

template <int P>
float	Approx::rsqrt (float x) noexcept
{
	static_assert (
		(P >= 0 && P <= 3),
		"The number of Newton iterations must be in [0 ; 3]"
	);
	assert (x >= 0);

	constexpr int32_t cs  =
		  (P == 0) ? 0x5F37642F  // Chris Lomont
		: (P <= 2) ? 0x5F1FFFF9  // Jan Kadlec
		:            0x5F375A86; // Chris Lomont

	const auto     x0 = x;

	auto           i = fstb::bit_cast <int32_t> (x);
	i = cs - (i >> 1);
	x = fstb::bit_cast <float> (i);

	if constexpr (P == 1 || P == 2)
	{
		// Jan Kadlec NR constant optimisations for the 1st iteration
		constexpr auto c21 = 0.703952253f;
		constexpr auto c01 = 2.38924456f * c21;
		x *= c01 - (c21 * x0) * x * x;

		if constexpr (P > 1)
		{
			const auto     xh = x0 * 0.5f;

			// Hand-tweaked constant for the 2nd iteration (= 1.5 + 3.6e-7f).
			// With a standard NR iteration, the relative error is always
			// negative. By subtracting the error mean from the multiplicative
			// constant, we can center the new error, reducing both its maximum
			// magnitude and average.
			// 0x1.800006p+0f
			x *= 0x1.800006p+0f - xh * x * x;
		}
	}
	else if constexpr (P > 2)
	{
		const auto     xh = x0 * 0.5f;
		x *= 1.5f - xh * x * x;
		x *= 1.5f - xh * x * x;
		// Writing the iteration this way introduces 1 more addition but helps
		// increasing the accuracy.
		x += x * (0.5f - xh * x * x);
	}

	return x;
}

/*
P  max rel error
0: ? not tested
1: 9e-4
2: 6.1e-7
3: 2.8e-13
4: 3.21e-16. Not really faster than 1 / sqrt (x)
*/

template <int P>
double	Approx::rsqrt (double x) noexcept
{
	static_assert (
		(P >= 0 && P <= 4),
		"The number of Newton iterations must be in [0 ; 4]"
	);
	assert (x >= 0);

	constexpr int64_t cs = 0x5FE6EB50C7B537A9LL; // Matthew Robertson
	const double      xh = x * 0.5;

	auto           i = fstb::bit_cast <int64_t> (x);
	i = cs - (i >> 1);
	x = fstb::bit_cast <double> (i);

	// Hand-tweaked coefficients. Same remark as in the float version
	if constexpr (P > 0) { x *= (1.5 + 0.9e-03) - xh * x * x; }
	if constexpr (P > 1) { x *= (1.5 + 6.1e-07) - xh * x * x; }
	if constexpr (P > 2) { x *= (1.5 + 2.8e-13) - xh * x * x; }
	// Writing the iteration this way introduces 1 more addition but helps
	// increasing the accuracy (here, slightly).
	if constexpr (P > 3) { x += x * (0.5 - xh * x * x); }

	return x;
}

/*
Max rel error
P	x86 SSE   Arm NEON   C++ fallback
1:	2.59e-4   1.62e-5    6.5e-4
2:	2.18e-7   1.46e-7    4.98e-7
3:	1.3e-7    1.06e-7    1.04e-7
*/

template <int P>
Vf32	Approx::rsqrt (Vf32 x) noexcept
{
	static_assert (
		(P >= 0 && P <= 3),
		"The number of Newton iterations must be in [0 ; 3]"
	);
	assert (x >= Vf32::zero ());

	constexpr bool simd_flag =
#if defined (fstb_HAS_SIMD)
		true;
#else
		false;
#endif

	// Result is more or less equivalent to rough approx + 1 N-R iteration.
	auto           r = x.rsqrt_approx ();

	if constexpr (P > 1)
	{
		const auto     h   = Vf32 (0.5f);
		const auto     xh  = h * x;
		const auto     nrc = Vf32 (1.5f);
		if constexpr (! simd_flag && P == 2)
		{
			r *= Vf32 (0x1.800006p+0f) - xh * r * r;
		}
		else
		{
			r *= nrc - xh * r * r;
		}
		if constexpr (P > 2)
		{
			if (simd_flag)
			{
				r *= nrc - xh * r * r;
			}
			else
			{
				r += r * (h - xh * r * r);
			}
		}
	}

	return r;
}



/*
Piecewise linear approximation of sqrt (x)
Max relative error: 3.47 %
Derivate is not monotonous
0 gives something in the magnitude of sqrt (FLT_MIN). -0.f gives an error.
Vf32: not really faster than sqrt_approx() on x86
T is float, double or Vf32
*/

template <typename T>
T	Approx::sqrt_crude (T x) noexcept
{
	assert (x >= T (0));

	typedef BitEquiv <T> BE;

	// 0.963355f
	// 0x3F800000 gives exact values at 2^(2n) but much larger errors
	constexpr auto c =
		 (BE::_bits == 64) ? 0x3FEED3CDDD6E0400ll : 0x3F769E6Fll;

	return sqrt_crude_common <c> (x);
}



/*
Piecewise linear approximation of sqrt (x)
1 mul more, slightly more accurate.
0 gives something in the magnitude of sqrt (FLT_MIN). -0.f gives an error.
Max relative error: 2.94 %
Derivate is monotonous
Note: when denormals are disabled (flushed to 0), error becomes greater
below 0x1.0p-125f (2.35e-38) for float and 0x1.0p-1021 (4.45e-308) for double.
T is float, double or Vf32
*/

template <typename T>
T	Approx::sqrt_crude2 (T x) noexcept
{
	assert (x >= T (0));

	typedef BitEquiv <T> BE;
	constexpr auto mask =
		int64_t ((BE::_bits == 64) ? 0x3FF00000 : 0x3F800000) << (BE::_bits - 32);

	// 1.0f
	return sqrt_crude_common <mask> (x * T (0.942));
}



/*
Approximation of sqrt(x)
Crude approximation with bit wizardry refined with one step of the Heron's
method (NR on y^2 - x = 0)
Base calculations:
r = approx (1 / sqrt (x))
s = approx (1 / r)
Heron's refinement, rewritten as:
y = s + r * (x - s^2) / 2
0 gives something in the magnitude of sqrt (FLT_MIN). -0.f gives an error.
This approximation is useful mainly on CPU deprived of a decent floating
point unit.
Max relative error: 0.99e-3
Note: when float denormals are disabled (flushed to 0), error becomes greater
below 0x1.0p-121f (3.77e-37) for float and 0x1.0p-1017 (7.12e-307) for double.
The combination of constants has been manually refined. Without refinement,
the relative error maximum is 1.17e-3 but its RMS is lower.
T is float, double or Vf32
*/

template <typename T>
T	Approx::sqrt (T x) noexcept
{
	assert (x >= T (0));

	typedef BitEquiv <T> BE;
	typedef typename BE::ScalFlt SF;
	constexpr auto c_half = // -1 at the exponent
		int64_t (-(1LL << (std::numeric_limits <SF>::digits - 1)));

	if constexpr (BitEquiv <T>::_bits == 64)
	{
		// Matthew Robertson / Paul Khuong
		constexpr auto c_rsqrt = int64_t (0x5FE6EB50C7B537A9LL + (289500LL << 29));
		constexpr auto c_rcp   = int64_t (0x7FDE623822FC16E6LL + ( 95300LL << 29));

		return sqrt_common <T, c_rsqrt, c_rcp, c_half> (x);
	}
	else
	{
		constexpr auto c_rsqrt = int32_t (0x5F37642F + 287000); // Chris Lomont
		constexpr auto c_rcp   = int32_t (0x7EF311C3 +  95200); // Moroz and Samotyy

		return sqrt_common <T, c_rsqrt, c_rcp, c_half> (x);
	}
}



/*
Approximation of the Wright Omega function:
omega (x) = W0 (exp (x))
where W0 is one of the Lambert W functions.
Formula from:
Stefano D'Angelo, Leonardo Gabrielli, Luca Turchet,
Fast Approximation of the Lambert W Function for Virtual Analog Modelling,
DAFx-19
*/

template <typename T>
T	Approx::wright_omega_3 (T x) noexcept
{
	const T        a  = T (-1.314293149877800e-3);
	const T        b  = T ( 4.775931364975583e-2);
	const T        c  = T ( 3.631952663804445e-1);
	const T        d  = T ( 6.313183464296682e-1);
	const T        x1 = T (-3.341459552768620);
	const T        x2 = T ( 8.0);

	T              y  = T ( 0);
	if (x >= x2)
	{
		y = x - T (Approx::log2 (float (x))) * T (LN2);
	}
	else if (x > x1)
	{
		y = Poly::horner (x, d, c, b, a);
	}

	return y;
}

Vf32	Approx::wright_omega_3 (Vf32 x) noexcept
{
	const auto     a   = Vf32 (-1.314293149877800e-3f);
	const auto     b   = Vf32 ( 4.775931364975583e-2f);
	const auto     c   = Vf32 ( 3.631952663804445e-1f);
	const auto     d   = Vf32 ( 6.313183464296682e-1f);
	const auto     x1  = Vf32 (-3.341459552768620f);
	const auto     x2  = Vf32 ( 8.0f);
	const auto     ln2 = Vf32 (float (LN2));

	const auto     y0  = Vf32::zero ();
	const auto     y1  = Poly::horner (x, d, c, b, a);
	const auto     y2  = x - (Approx::log2 (x)) * ln2;

	const auto     tx1 = (x < x1);
	const auto     tx2 = (x < x2);
	auto           y   = y0;
	y = select (tx1, y, y1);
	y = select (tx2, y, y2);

	return y;
}



// One Newton-Raphson iteration added
template <typename T>
T	Approx::wright_omega_4 (T x) noexcept
{
	T              y = wright_omega_3 (x);
	y -=
		  (y - Approx::exp2 ((x - y) * T (LOG2_E)))
		/ (y + T (1));

	return y;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <long long C, typename T>
T	Approx::sqrt_crude_common (T x) noexcept
{
	typedef typename BitEquiv <T>::Int TI;

	const auto     i = fstb::bit_cast <TI> (x);
	assert (i >= TI (0));

	return fstb::bit_cast <T> ((i >> 1) + TI (C >> 1));
}



template <typename T, long long C_RSQRT, long long C_RCP, long long C_HALF>
T	Approx::sqrt_common (T x) noexcept
{
	typedef typename BitEquiv <T>::Int TI;

	const auto     i   = fstb::bit_cast <TI> (x);
	assert (i >= TI (0));
	const auto     ih  = i >> 1;
	const auto     rhi = TI (C_RSQRT + C_HALF ) - ih;
	const auto     si  = TI (C_RCP   - C_RSQRT) + ih;
	const auto     rh  = fstb::bit_cast <T> (rhi);
	const auto     s   = fstb::bit_cast <T> (si);
	const auto     y   = s + rh * (x - s * s);

	return y;
}



}  // namespace fstb



#endif   // fstb_Approx_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
