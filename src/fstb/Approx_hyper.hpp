/*****************************************************************************

        Approx_hyper.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_Approx_hyper_CODEHEADER_INCLUDED)
#define fstb_Approx_hyper_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "fstb/Poly.h"

#include <type_traits>

#include <cmath>



namespace fstb
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
Idea by BigTick
Max error: abs: 0.0211, rel: 0.0391
https://www.desmos.com/calculator/ueqdf2bbl4
*/

template <typename T>
T	Approx::tanh_bigtick_modlds (T x) noexcept
{
	using std::abs;

	const auto     m = x * T (1.0 / 3);
	const auto     c = limit (m, T (-1), T (1));
	const auto     d = c * (T (2) - abs (c));
	const auto     y = T (0.5) * d * (T (3) - d * d);

	return y;
}



/*
Formula concept by mystran
https://www.kvraudio.com/forum/viewtopic.php?f=33&t=521377
s (x) = x / sqrt (1 + x^2)
h3 (x) = s (x + 0.183426 * x^3)
Refinement on the p constant by Andrew Simper
Max error: abs: 3.6e-3, rel: 3.66e-3
https://www.desmos.com/calculator/sjxol8khaz
*/

template <typename T>
T	Approx::tanh_mystran (T x) noexcept
{
	const auto     p  = T (0.183426);

	// x <- x + p * x^3
	auto           x2 = x * x;
	const auto     s  = x + x * x2 * p;

	return tanh_from_sinh (s);
}



/*
Formula by Urs Heckmann, modified by Andrew Simper
https://www.desmos.com/calculator/s86jxqzqgo
Max error: abs: 1.35e-5, rel: 1.35e-5
Coefficients for 9th-order alternative:
(1), 0.166658916965, 8.34772973761e-3, 1.92238891072e-4, 3.54691185595e-6
*/

template <typename T>
T	Approx::tanh_urs (T x) noexcept
{
	const auto     c3 = T (0.166769511323);
	const auto     c5 = T (8.18265554763e-3);
	const auto     c7 = T (2.43041131705e-4);

	const auto     x2 = x * x;
	const auto     s  = Poly::horner (x2, c3, c5, c7) * x2 * x + x;

	return tanh_from_sinh (s);
}



/*
https://math.stackexchange.com/a/3485944
Interesting idea and good precision but:
- Accuracy between tanh_urs() and tan_2dat()
- Only slightly faster than tanh_2dat()
- Bad relative error near 0 because of precision loss (small + big - big).
Fixing it makes the algorithm too slow compared to the other ones.
Implementation removed but comment kept for history.
*/



/*
Formula by 2DaT
https://www.kvraudio.com/forum/viewtopic.php?p=7503081#p7503081
Max error:
32 bits: abs: 6.00e-7, rel: 6.17e-7
64 bits: abs: 3.09e-7, rel: 4.23e-7
https://www.desmos.com/calculator/s86jxqzqgo
*/

template <typename T>
constexpr T	Approx::tanh_2dat (T x) noexcept
{
	constexpr auto n0      = T (4.351839500e+06);
	constexpr auto n1      = T (5.605646250e+05);
	constexpr auto n2      = T (1.263485352e+04);
	constexpr auto n3      = T (4.751771164e+01);
	constexpr auto d0      = n0;
	constexpr auto d1      = T (2.011170000e+06);
	constexpr auto d2      = T (1.027901563e+05);
	constexpr auto d3      = T (1.009453430e+03);
	constexpr auto max_val = T (7.7539052963256836);

	x = limit (x, -max_val, max_val);
	const auto     x2  = x * x;
	const auto     num = Poly::horner (x2, n0, n1, n2, n3     ) * x;
	const auto     den = Poly::horner (x2, d0, d1, d2, d3 + x2);

	return num / den;
}

Vf32	Approx::tanh_2dat (Vf32 x) noexcept
{
	const auto     n0      = Vf32 (4.351839500e+06f);
	const auto     n1      = Vf32 (5.605646250e+05f);
	const auto     n2      = Vf32 (1.263485352e+04f);
	const auto     n3      = Vf32 (4.751771164e+01f);
	const auto     d0      = n0;
	const auto     d1      = Vf32 (2.011170000e+06f);
	const auto     d2      = Vf32 (1.027901563e+05f);
	const auto     d3      = Vf32 (1.009453430e+03f);
	const auto     max_val = Vf32 (7.7539052963256836f);

	const auto     s   = x.signbit ();
	x = abs (x);
	x = min (x, max_val);
	const auto     x2  = x * x;
	const auto     xs  = x ^ s;
	const auto     num = Poly::horner (x2, n0, n1, n2, n3     ) * xs;
	const auto     den = Poly::horner (x2, d0, d1, d2, d3 + x2);

	return num / den;
}



/*
This one fits in the range [0, 4] and matches the derivative at 4 to zero
so you have a smooth C1 at the point of clipping
ends at just under 1, but has a normalised error of +-6.5e-4
tanh_approx(4) = 0.9998
tanh(4) = 0.999329
Source: Andrew Simper
Max error: abs: 6.51e-4, rel: 6.52e-4
https://discord.com/channels/507604115854065674/507630527847596046/702375822941618207
*/

template <typename T>
constexpr T	Approx::tanh_andy (T x) noexcept
{
	x = limit (x, T (-4), T (+4));

	const auto     n3  = T (0.0812081221471);
	const auto     d0  = T (1);
	const auto     d2  = T (0.412523749044);
	const auto     d4  = T (0.00624523306500);

	const auto     x2  = x * x;
	const auto     num = n3 * x2 * x + x;
	const auto     den = Poly::horner (x2, d0, d2, d4);

	return num / den;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Computes x / sqrt (1 + x^2)
// because tanh (x) = sinh (x) / sqrt (1 + sinh (x) ^ 2)
template <typename T>
T	Approx::tanh_from_sinh (T x) noexcept
{
	if constexpr (! std::is_scalar_v <T>)
	{
		return x * (x * x + T (1)).rsqrt ();
	}
	else
	{
		using std::sqrt;
		return x / T (sqrt (T (1) + x * x));
	}
}



}  // namespace fstb



#endif // fstb_Approx_hyper_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
