/*****************************************************************************

        Approx.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_Approx_HEADER_INCLUDED)
#define fstb_Approx_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Vf32.h"

#include <array>

#include <cstdint>



namespace fstb
{



class Approx
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_rbj (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               cos_rbj (T x) noexcept;
	fstb_FLATINLINE static void
	               cos_sin_rbj (Vf32 &c, Vf32 &s, Vf32 x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_rbj_halfpi (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_rbj_pi (T x) noexcept;
	fstb_FLATINLINE static std::array <float, 2>
	               sin_rbj_halfpi_pi (float x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_andy_halfpi (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_andy_pi (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               sin_lds_pi (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               sin_nick (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               sin_nick_2pi (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static std::array <T, 2>
	               cos_sin_nick_2pi (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               log2 (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               log2_5th (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               log2_7th (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               log2_crude (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               log2_crude2 (T val) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               exp2 (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               exp2_5th (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               exp2_7th (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               exp2_crude (T val) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               exp2_crude2 (T val) noexcept;
	template <int A, typename T>
	fstb_FLATINLINE static constexpr T
	               exp_m (T val) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               pow_crude (T a, T b) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               pow_3 (T a, T b) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               pow_5 (T a, T b) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               pow_7 (T a, T b) noexcept;

	fstb_FLATINLINE static uint32_t
	               fast_partial_exp2_int_16_to_int_32 (int val) noexcept;
	fstb_FLATINLINE static uint32_t
	               fast_partial_exp2_int_16_to_int_32_4th (int val) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_taylor5 (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_7 (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_9 (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               tan_mystran (T x) noexcept;
	fstb_FLATINLINE static Vf32
	               tan_mystran (Vf32 x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_pade33 (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_t3a (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tan_pade55 (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               atan2_3th (T y, T x) noexcept;
	fstb_FLATINLINE static Vf32
	               atan2_3th (Vf32 y, Vf32 x) noexcept;

	template <typename T>
	fstb_FLATINLINE static constexpr T
	               atan2_7th (T y, T x) noexcept;
	fstb_FLATINLINE static Vf32
	               atan2_7th (Vf32 y, Vf32 x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               tanh_bigtick_modlds (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               tanh_mystran (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               tanh_urs (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tanh_2dat (T x) noexcept;
	fstb_FLATINLINE static Vf32
	               tanh_2dat (Vf32 x) noexcept;
	template <typename T>
	fstb_FLATINLINE static constexpr T
	               tanh_andy (T x) noexcept;

	template <int P = 1>
	fstb_FLATINLINE static float
	               rcp (float x) noexcept;
	template <int P = 1>
	fstb_FLATINLINE static double
	               rcp (double x) noexcept;

	template <int P = 1>
	fstb_FLATINLINE static float
	               rsqrt (float x) noexcept;
	template <int P = 1>
	fstb_FLATINLINE static double
	               rsqrt (double x) noexcept;
	template <int P = 1>
	fstb_FLATINLINE static Vf32
	               rsqrt (Vf32 x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               sqrt_crude (T x) noexcept;
	template <typename T>
	fstb_FLATINLINE static T
	               sqrt_crude2 (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               sqrt (T x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               wright_omega_3 (T x) noexcept;
	fstb_FLATINLINE static Vf32
	               wright_omega_3 (Vf32 x) noexcept;

	template <typename T>
	fstb_FLATINLINE static T
	               wright_omega_4 (T x) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	fstb_FORCEINLINE static Vf32
	               restrict_angle_to_mpi_pi (Vf32 x, const Vf32 &pm, const Vf32 &p, const Vf32 &tp) noexcept;
	fstb_FORCEINLINE static Vf32
	               restrict_sin_angle_to_mhpi_hpi (Vf32 x, const Vf32 &hpm, const Vf32 &hp, const Vf32 &pm, const Vf32 &p) noexcept;

	template <typename T, typename P>
	fstb_FORCEINLINE static T
	               log2_base (T val, P poly) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               log2_poly2c1 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               log2_poly5c4 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               log2_poly5 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               log2_poly7 (T x) noexcept;

	template <typename T, typename P>
	fstb_FLATINLINE static T
	               exp2_base (T val, P poly) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_poly2 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_poly5c2 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_poly5 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_poly7c1 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_poly7c2 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_frac32 (T x) noexcept;
	template <typename T>
	fstb_FORCEINLINE static constexpr T
	               exp2_frac33 (T x) noexcept;

	template <typename T>
	fstb_FORCEINLINE static T
	               tanh_from_sinh (T x) noexcept;

	template <typename T>
	fstb_FORCEINLINE static constexpr std::array <T, 2>
	               atan2_beg (T y, T x) noexcept;
	fstb_FORCEINLINE static std::array <Vf32, 2>
	               atan2_beg (Vf32 y, Vf32 x) noexcept;

	template <long long C, typename T>
	fstb_FORCEINLINE static T
	               sqrt_crude_common (T x) noexcept;

	template <typename T, long long C_RSQRT, long long C_RCP, long long C_HALF>
	fstb_FORCEINLINE static T
	               sqrt_common (T x) noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               Approx ()                               = delete;
	               Approx (const Approx &other)            = delete;
	virtual        ~Approx ()                              = delete;
	Approx &       operator = (const Approx &other)        = delete;
	bool           operator == (const Approx &other) const = delete;
	bool           operator != (const Approx &other) const = delete;

}; // class Approx



}  // namespace fstb



#include "fstb/Approx.hpp"
#include "fstb/Approx_hyper.hpp"
#include "fstb/Approx_trans.hpp"
#include "fstb/Approx_trigo.hpp"



#endif   // fstb_Approx_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
