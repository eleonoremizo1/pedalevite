/*****************************************************************************

		unicode.h
        Author: Laurent de Soras, 2009

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_txt_unicode_unicode_fnc_HEADER_INCLUDED)
#define	fstb_txt_unicode_unicode_fnc_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include	<string>



namespace fstb
{
namespace txt
{
namespace unicode
{



int	conv_utf8_to_unicode (std::u32string & dest, const char src_0 []);
int	conv_unicode_to_utf8 (std::string & dest, const char32_t src_0 []);

template <class C>	// C = ConvCi or ConvNeutral
int	compare (const std::u32string &s1, const std::u32string &s2);



}	// namespace unicode
}	// namespace txt
}	// namespace fstb


/*\\\ CODE HEADER INCLUSION \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/txt/unicode/unicode.hpp"



#endif	// fstb_txt_unicode_unicode_fnc_HEADER_INCLUDED



/*****************************************************************************

	LEGAL

	(c) 2002 fstb Force

	Use of this file or part of its content without written permission is
	strictly forbidden.
	For more information, please contact fstb Force at :
	contact@fstbforce.com

*****************************************************************************/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
