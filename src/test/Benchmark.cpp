/*****************************************************************************

        Benchmark.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "test/Benchmark.h"

#include <algorithm>

#include <cassert>
#include <cmath>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Benchmark::reset () noexcept
{
	_result_arr.clear ();
	_tim.reset ();
	_run_flag = false;
	_res_time = -1;
}



void	Benchmark::reserve (int max_nbr_tests)
{
	assert (max_nbr_tests >= 0);

	_result_arr.reserve (max_nbr_tests);
}



// Returns the measured time.
// It's a mix between average and median values for multiple measures.
// optstop is a value taken from the result of the benchmarked calculations.
// It helps preventing the compiler to bypass the tested code because its
// output is unused. The value itself is not important, it may even be a NaN.
double	Benchmark::compute_time (double optstop) noexcept
{
	assert (! _run_flag);
	assert (! _result_arr.empty ());

	if (! are_results_ready ())
	{
		prepare_results (optstop);
	}

	return _res_time;
}



// Returns the time as a rate (number of operation per seconds).
// nbr_op is the number of operations during one measure.
double	Benchmark::compute_rate (long long nbr_op, double optstop) noexcept
{
	assert (! _run_flag);
	assert (! _result_arr.empty ());

	if (! are_results_ready ())
	{
		prepare_results (optstop);
	}

	return double (nbr_op) / _res_time;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Benchmark::prepare_results (double optstop) noexcept
{
	assert (! _run_flag);
	assert (! _result_arr.empty ());

	// Minimum and maximum quantiles used for the average. If both are set
	// to 0.5, it takes the median value.
	constexpr auto quant_min = 0.4;
	constexpr auto quant_max = 0.6;

	std::sort (_result_arr.begin (), _result_arr.end ());

	const auto     nbr_tests = int (_result_arr.size ());
	const auto     idx_beg = fstb::floor_int ((nbr_tests - 1) * quant_min);
	const auto     idx_end = fstb::floor_int ((nbr_tests - 1) * quant_max) + 1;
	assert (idx_beg >= 0);
	assert (idx_beg < idx_end);
	assert (idx_end <= nbr_tests);

	double         sum = 0;
	for (int tst_idx = idx_beg; tst_idx < idx_end; ++tst_idx)
	{
		sum += _result_arr [tst_idx];
	}
	if (std::isfinite (optstop))
	{
		sum += fstb::limit (optstop, -1e-300, +1e-300);
	}
	const auto     weight = idx_end - idx_beg;
	_res_time = sum / double (weight);
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
