/*****************************************************************************

        TestPlugins.h
        Author: Laurent de Soras, 2024

Direct test of various Pedale Vite plug-ins.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestPlugins_HEADER_INCLUDED)
#define TestPlugins_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestPlugins
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     test_testgen ();
	static int     test_dist1 ();
	static int     test_phase1 ();
	static int     test_nzcl ();
	static int     test_nzbl ();
	static int     test_dly2 ();
	static int     test_osdet ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestPlugins ()                               = delete;
	               TestPlugins (const TestPlugins &other)       = delete;
	               TestPlugins (TestPlugins &&other)            = delete;
	TestPlugins &  operator = (const TestPlugins &other)        = delete;
	TestPlugins &  operator = (TestPlugins &&other)             = delete;
	bool           operator == (const TestPlugins &other) const = delete;
	bool           operator != (const TestPlugins &other) const = delete;

}; // class TestPlugins



//#include "test/TestPlugins.hpp"



#endif // TestPlugins_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
