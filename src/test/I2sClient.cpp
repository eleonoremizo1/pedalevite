/*****************************************************************************

        I2sClient.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/I2sClient.h"
#include "test/I2sClientCbInterface.h"

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Description:
	Builds the object with various parameters specified that won't change
	during the object life.
	All signals are assumed to be at the low state.
	Clock slave objects are assumed unsynchronised. Before complete
	synchronisaton with the master, the first received data may be corrupted.
Input parameters:
	- master_flag: indicates if the object is build as clock master.
	- bitdepth: number of bits per sample for all transfers.
	- transfer_lag: distance in bits between the LRCK edge (preceding) and the
		MSB of the corresponding sample (following). In range [0 ; bitdepth[.
		Usually 1 for standard I2S.
	- left_high_flag: polarity of the LRCK clock.
			false = low for left channel, high for right (default for I2S)
			true  = high for left channel, low for right
Throws: Nothing
==============================================================================
*/

I2sClient::I2sClient (bool master_flag, int bitdepth, int transfer_lag, bool left_high_flag) noexcept
:	_master_flag (master_flag)
,	_bitdepth (bitdepth)
,	_transfer_lag (transfer_lag)
,	_left_high_flag (left_high_flag)
,	_len_spl_e (_bitdepth << 1)
,	_len_frame_e (_len_spl_e * _nbr_chn)
,	_len_frame_b (_bitdepth  * _nbr_chn)
{
	assert (bitdepth > 0);
	assert (bitdepth <= 32);
	assert (transfer_lag >= 0);
	assert (transfer_lag < bitdepth);
}



/*
==============================================================================
Name: is_clock_master
Description:
	Checks if the object is clock master or slave.
Returns: true if it is clock master.
Throws: Nothing
==============================================================================
*/

bool	I2sClient::is_clock_master () const noexcept
{
	return _master_flag;
}



/*
==============================================================================
Name: reset
Description:
	Put the object in its initial state.
Throws: Nothing
==============================================================================
*/

void	I2sClient::reset () noexcept
{
	_clock_pos = 0;
	_st_cur.fill (false);
	_data_in.fill (0);
	_data_out.fill (0);
	_sync_count = 0;
}



/*
==============================================================================
Name: set_callback
Description:
	Registers a callback object for the various notifications.
	Mandatory before calling step().
Input parameters:
	- cb: callback object.
Throws: Nothing
==============================================================================
*/

void	I2sClient::set_callback (I2sClientCbInterface &cb) noexcept
{
	_cb_ptr = &cb;
}



/*
==============================================================================
Name: set_offset
Description:
	Change the tracked time position.
	A clock-slave object does not emit clock error notifications after this
	call.
	Current transferred data may become corrupted.
Input parameters:
	- edge_count: current number of SCLK edges after the sampling of an LRCK
		rising edge.
Throws: Nothing
==============================================================================
*/

void	I2sClient::set_offset (int edge_count) noexcept
{
	assert (edge_count >= 0);
	assert (edge_count < _len_frame_e);

	_clock_pos  = edge_count;
	_sync_count = 0;
}



/*
==============================================================================
Name: set_pin
Description:
	Reports a signal change on an input pin.
	Clock pins are considered input only on clock slave objects.
	Signal change will be effective at the beginning of the next call to
	step().
Input parameters:
	- p: pin to write.
	- state_flag: new level of the pin. false = low, true = high
Throws: Nothing
==============================================================================
*/

void	I2sClient::set_pin (I2sPin p, bool state_flag) noexcept
{
	assert (p >= 0);
	assert (p < I2sPin_NBR_ELT);
	assert (p != I2sPin_SDOUT);
	// Makes sure the caller doesn't write clock signals when configured
	// in clock master mode.
	assert (! _master_flag || (p != I2sPin_SCLK && p != I2sPin_LRCK));

	_st_cur [p] = state_flag;
}



/*
==============================================================================
Name: step
Description:
	Gets to the next SCLK edge. Generates new signals located at or right after
	this edge.
	This function may call methods on the callback object (setting pins,
	transferring data, reporting clock errors). The callback object should have
	been registered before.
	If the object is clock slave, the function returns directly if there were
	no change on the SCLK signal.
Throws: depends on the callback methods.
==============================================================================
*/

void	I2sClient::step ()
{
	assert (_cb_ptr != nullptr);

	auto           frame_pos   = compute_frame_pos ();
	const bool     rising_flag = ((_clock_pos & 1) == 0); // SCLK

	// In clock master mode, generates the clock signals
	// Always generate them even if there are no change, to make sure the
	// values at the other end is always as intended.
	if (_master_flag)
	{
		_cb_ptr->set_pin (I2sPin_SCLK, rising_flag);

		// Changes the LRCK signal at the preceding falling edge
		const auto     cp_shift  = shift_pos (_clock_pos, 1, _len_frame_e);
		const auto     lrck_flag = ((cp_shift < _len_spl_e) == _left_high_flag);
		_cb_ptr->set_pin (I2sPin_LRCK, lrck_flag);
	}

	// Clock slave mode
	else
	{
		// Leaves the function if we're not in the expected state
		if (_st_cur [I2sPin_SCLK] != rising_flag)
		{
			return;
		}

		// Checks LRCK consistency on rising edges
		const auto     left_flag = (_st_cur [I2sPin_LRCK] == _left_high_flag);
		if (   _st_cur [I2sPin_SCLK]
		    && left_flag != (_clock_pos < _len_spl_e))
		{
			if (_sync_count >= _len_frame_e)
			{
				_cb_ptr->notify_clock_error ();
				_sync_count = 0;
			}

			// Resynchronisation
			_clock_pos = (left_flag) ? 0 : _len_spl_e;

			// Recomputes the current position
			frame_pos = compute_frame_pos ();
		}

		if (_sync_count < _len_frame_e)
		{
			++ _sync_count;
		}
	}

	// Samples the input bits on the rising edge
	if (rising_flag)
	{
		const auto     bit_pos = compute_bit_pos (frame_pos);
		const auto     mask    = uint32_t (1) << bit_pos._bit;

		if (_st_cur [I2sPin_SDIN])
		{
			_data_in [bit_pos._chn] |= mask;
		}

		// Last bit done? Transfers the frames to/from the client
		if (bit_pos._chn == _nbr_chn - 1 && bit_pos._bit == 0)
		{
			I2sClientCbInterface::SampleFrame   in_out = _data_in;
			_cb_ptr->proc_audio (in_out);
			_data_out = in_out;
			_data_in  = I2sClientCbInterface::SampleFrame {};
		}
	}

	// And sends the corresponding output bits on the *preceding* falling edge
	else
	{
		const auto     fp_next = shift_pos (frame_pos, 1, _len_frame_b);
		const auto     bit_pos = compute_bit_pos (fp_next);
		const auto     mask    = uint32_t (1) << bit_pos._bit;

		const auto     flag = ((_data_out [bit_pos._chn] & mask) != 0);
		_cb_ptr->set_pin (I2sPin_SDOUT, flag);
	}

	// Prepares for the next clock edge
	++ _clock_pos;
	if (_clock_pos >= _len_frame_e)
	{
		_clock_pos = 0;
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Returns the current position in bits in [0 ; _len_frame_b[
int	I2sClient::compute_frame_pos () const noexcept
{
	const int      frame_pos =
		shift_pos (_clock_pos >> 1, -_transfer_lag, _len_frame_b);

	return frame_pos;
}



I2sClient::BitPos	I2sClient::compute_bit_pos (int frame_pos) const noexcept
{
	BitPos           res;

	if (frame_pos >= _bitdepth)
	{
		++ res._chn;
		frame_pos -= _bitdepth;
	}

	res._bit = _bitdepth - 1 - frame_pos;

	assert (res._bit >= 0);
	assert (res._bit < _bitdepth);
	assert (res._chn >= 0);
	assert (res._chn < _nbr_chn);

	return res;
}



int	I2sClient::shift_pos (int pos, int ofs, int val_wrap) noexcept
{
	assert (val_wrap > 0);
	assert (pos + ofs >= -val_wrap);
	assert (pos + ofs < val_wrap * 2);

	pos += ofs;
	if (pos < 0)
	{
		pos += val_wrap;
	}
	else if (pos >= val_wrap)
	{
		pos -= val_wrap;
	}

	assert (pos >= 0);
	assert (pos < val_wrap);

	return pos;
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
