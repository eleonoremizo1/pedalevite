/*****************************************************************************

        TestRpiBcm.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "fstb/def.h"
#include "mfx/adrv/CbInterface.h"
#include "test/TestRpiBcm.h"

#if (PV_RPI_VER_MAJOR >= 3 && PV_RPI_VER_MAJOR < 5)
	#include "mfx/adrv/DPvabDirectBcm.h"
#endif

#include <chrono>
#include <thread>

#include <cassert>
#include <cstdio>



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestRpiBcm_CbDPvabDirect
:	public mfx::adrv::CbInterface
{
public:
	TestRpiBcm_CbDPvabDirect () = default;
	virtual ~TestRpiBcm_CbDPvabDirect () = default;
protected:
	virtual void   do_process_block (float * const * dst_arr, const float * const * src_arr, int nbr_spl) noexcept
	{
		// Copies input to output
		fstb::copy_no_overlap (dst_arr [0], src_arr [0], nbr_spl);
		fstb::copy_no_overlap (dst_arr [1], src_arr [1], nbr_spl);
	}
	virtual void   do_notify_dropout () noexcept
	{
		// Nothing
	}
	virtual void   do_request_exit () noexcept
	{
		// Nothing
	}
};



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestRpiBcm::test_adrv_dpvab ()
{
#if (PV_RPI_VER_MAJOR >= 3 && PV_RPI_VER_MAJOR < 5)

	constexpr int dur = 600;
	printf ("Testing mfx::adrv::DPvabDirectBcm for %d seconds...\n", dur);

	mfx::adrv::DPvabDirectBcm  pvab;
	TestRpiBcm_CbDPvabDirect   cb;

	double         sample_freq;
	int            max_block_size;
	pvab.init (sample_freq, max_block_size, cb, nullptr, 0, 0);

	pvab.start ();
	std::this_thread::sleep_for (std::chrono::seconds (dur));
	pvab.stop ();

	printf ("Done.\n");

#endif // LINUX

	return 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
