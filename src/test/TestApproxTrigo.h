/*****************************************************************************

        TestApproxTrigo.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestApproxTrigo_HEADER_INCLUDED)
#define TestApproxTrigo_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestApproxTrigo
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestApproxTrigo ()                               = delete;
	               TestApproxTrigo (const TestApproxTrigo &other)   = delete;
	               TestApproxTrigo (TestApproxTrigo &&other)        = delete;
	TestApproxTrigo &
	               operator = (const TestApproxTrigo &other)        = delete;
	TestApproxTrigo &
	               operator = (TestApproxTrigo &&other)             = delete;
	bool           operator == (const TestApproxTrigo &other) const = delete;
	bool           operator != (const TestApproxTrigo &other) const = delete;

}; // class TestApproxTrigo



//#include "test/TestApproxTrigo.hpp"



#endif // TestApproxTrigo_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
