/*****************************************************************************

        TestPwmMgr.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Hash.h"
#include "mfx/FileOpWav.h"
#include "mfx/PwmMgr.h"
#include "test/TestPwmMgr.h"

#include <algorithm>
#include <vector>

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestPwmMgr::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing mfx::PwmMgr...\n");

	// Kernel scheduler jitter data
	constexpr auto jit_mu     = 50; // Average jitter, us
	const auto     jit_mu_log = logf (float (jit_mu));
	const auto     jit_sigma  = 0.5f; // Sigma for the log-normal distribution

	// State for the random generator
	uint32_t       rnd_state  = 0;

	mfx::PwmMgr    pwm_mgr;
	pwm_mgr.set_nbr_chn (1);
	pwm_mgr.set_period (1'000);
	pwm_mgr.set_max_sleep (273);

	std::vector <Segment> roadmap;
	const uint32_t    p = 48'000; // us
	roadmap.push_back ({ p, 1.f   , false });
	roadmap.push_back ({ p, 0.f   , false });
	roadmap.push_back ({ p, 0.5f  , false });
	roadmap.push_back ({ p, 0.25f , false });
	roadmap.push_back ({ p, 0.75f , false });
	roadmap.push_back ({ p, 0.009f, false });
	roadmap.push_back ({ p, 1.f   , true  });
	roadmap.push_back ({ p, 0.f   , true  });

	// Current "real-time" timestamp, us
	uint32_t       ts_cur  = 0;

	// Scheduled timestamp of the current segment beginning, us
	uint32_t       ts_seg  = 0;

	// Current PWM value
	float          pwm_val = 0;

	// Output data
	std::vector <float>  dst;

	// Iterates on the segments
	for (const auto &seg : roadmap)
	{
		const auto     pwm_val_beg = pwm_val;
		const auto     ts_end = uint32_t (ts_seg + seg._duration);

		while (ts_cur < ts_end)
		{
			// Updates the value, depending if it's a constant or a ramp
			float          val = seg._val;
			if (seg._ramp_flag)
			{
				const auto     ratio =
					float (ts_cur - ts_seg) / float (seg._duration);
				val = pwm_val_beg + (seg._val - pwm_val_beg) * ratio;
			}
			pwm_mgr.set_level (0, val);

			// Let's modulate
			const auto     result = pwm_mgr.process (ts_cur);

			// Collects data
			const auto     state_val  = int (pwm_mgr.get_state (0));
			const auto     chg_val    = int (result._change_mask & 1);

			// Makes sure we don't stop too long after the segment end.
			auto           dur_sleep =
				std::min (result._sleep_time, int (ts_end - ts_cur));

			// Simulates jitter on the sleep() value
			const auto     jitter =
				fstb::round_int (gen_log_norm (rnd_state, jit_mu_log, jit_sigma));
			rnd_state += 2;
			dur_sleep += jitter;
			dur_sleep  = std::max (dur_sleep, 1);

			// Simulates sleep()
			const auto     sleep_end = ts_cur + dur_sleep;
			dst.resize (sleep_end);
			const auto     spl = float (state_val * 2 - 1);
			dst [ts_cur] = spl * (1 + float (chg_val) * 0.25f) + 0.125f;
			for (int pos = 1; pos < dur_sleep; ++pos)
			{
				dst [ts_cur + pos] = spl;
			}
			ts_cur += dur_sleep;
		}

		// Gets ready for the next segment
		ts_seg  = ts_end;
		pwm_val = seg._val;
	}

	mfx::FileOpWav::save ("results/pwmmgr1.wav", dst, double (p), 0.5f);

	printf ("Done.\n\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Generates a random number with a normal (gaussian) distribution.
// The gaussian curve is actually truncated.
// Consumes two consecutive rnd_state
float	TestPwmMgr::gen_norm_trunc (uint32_t rnd_state) noexcept
{
	// Sums 6 10-bit random numbers, as an Irwin-Hall distribution
	// The output range is [-4.243 ; 4.243], keeping the variable extent
	// limited.
	// https://en.wikipedia.org/wiki/Irwin%E2%80%93Hall_distribution
	constexpr auto nbr  = 6;
	constexpr auto res  = 10;
	constexpr auto m    = (uint32_t (1) << res) - 1;
	constexpr auto avg  = int (m * nbr / 2);
	const auto     r0   = fstb::Hash::hash (rnd_state    );
	const auto     r1   = fstb::Hash::hash (rnd_state + 1);
	const auto     rsu  =
		  (r0 & m) + ((r0 >> res) & m) + ((r0 >> (2 * res)) & m)
		+ (r1 & m) + ((r1 >> res) & m) + ((r1 >> (2 * res)) & m);
	const auto     rss  = int (rsu) - avg;

	// Adjusts the standard deviation
	// scale = sqrt (12 / nbr)
	constexpr auto std_scale = float (fstb::SQRT2);
	constexpr auto mul  = std_scale / float (m);
	const auto     norm = float (rss) * mul;

	return norm;
}



// Generates a random number with a log-normal distribution.
// Consumes two consecutive rnd_state
float	TestPwmMgr::gen_log_norm (uint32_t rnd_state, float mu_log, float sigma) noexcept
{
	assert (mu_log >= -80); // Reasonable values for exp float
	assert (mu_log <= +80);
	assert (sigma >= 0);

	const auto     norm = gen_norm_trunc (rnd_state);
	const auto     val  = expf (mu_log + norm * sigma);

	return val;
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
