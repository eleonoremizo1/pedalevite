/*****************************************************************************

        TestBitTrace.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/BitTrace.h"
#include "test/TestBitTrace.h"

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestBitTrace::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing BitTrace...\n");

	printf ("1 trace:\n");
	BitTrace       simple (1);
	simple.set_name (0, "Some signal");
	feed_traces_as_string (simple, { " # ##   # ### # ### ##  # #  ##" });
	auto           str = simple.print (64);
	printf ("%s", str.c_str ());

	printf ("3 traces, multiple pages:\n");
	BitTrace       triple (3);
	triple.set_name (0, "Clock");
	triple.set_name (1, "Trace 1");
	triple.set_name (2, "Trace 2");
	feed_traces_as_string (triple, {
		"# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ",
		" # ##   # ### # ### ##  # #  ## ###  ## ## # #     ####  ##   ####",
		"# #  ## ###  ## ## # # ####  ##   ####     # ##   # ### # ### ##  "
	});
	str = triple.print (32);
	printf ("%s", str.c_str ());

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename T>
void	TestBitTrace::feed_traces_as_string (BitTrace &bt, std::initializer_list <T> l)
{
	std::vector <const char *> str_0_arr (l);

	int            pos = 0;
	bool           cont_flag = true;
	do
	{
		for (int trace_idx = 0; trace_idx < int (l.size ()) && cont_flag; ++trace_idx)
		{
			const auto  c = str_0_arr [trace_idx] [pos];
			if (c == '\0')
			{
				cont_flag = false;
			}
			else
			{
				bt.set_pin (trace_idx, (c != ' ' && c != '0' && c != '_'));
			}
		}

		if (cont_flag)
		{
			bt.tick (1);
			++ pos;
		}
	}
	while (cont_flag);
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
