/*****************************************************************************

        PatchSetup.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/SerRText.h"
#include "mfx/doc/SerWText.h"
#include "mfx/doc/Setup.h"
#include "mfx/pi/peq/Param.h"
#include "mfx/pi/peq/PEqDesc.h"
#include "mfx/pi/peq/PEqType.h"
#include "mfx/FileIOInterface.h"
#include "test/PatchSetup.h"

#include <memory>
#include <string>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	PatchSetup::patch_setup_file ()
{
	int            ret_val = 0;

	printf ("Patching config file...\n");

	const std::string pathname_load = "../../../etc/config/current"; // Put the settings file here
	const std::string pathname_save = pathname_load + ".patched";

	printf (
		"source: \"%s\", destination: \"%s\"\n",
		pathname_load.c_str (), pathname_save.c_str ()
	);

	std::string    content;
	std::unique_ptr <mfx::doc::Setup> sss_uptr;

	// Loading
	ret_val = mfx::FileIOInterface::read_txt_file_direct (
		pathname_load, content
	);

	if (ret_val == 0)
	{
		mfx::doc::SerRText   ser_r;
		ser_r.start (content);
		sss_uptr = std::make_unique <mfx::doc::Setup> ();
		sss_uptr->ser_read (ser_r);
		ret_val = ser_r.terminate ();
	}

	// Put the code to change the settings here
	if (ret_val == 0)
	{
		fix_peq_2017_02 (*sss_uptr);
	}

	// Saving
	mfx::doc::SerWText   ser_w;
	if (ret_val == 0)
	{
		assert (sss_uptr.get () != nullptr);
		ser_w.clear ();
		sss_uptr->ser_write (ser_w);
		ret_val = ser_w.terminate ();
	}
	if (ret_val == 0)
	{
		content = ser_w.use_content ();
		ret_val = mfx::FileIOInterface::write_txt_file_direct (
			pathname_save, content
		);
	}

	if (ret_val == 0)
	{
		fprintf (stderr, "Patched settings successfully.\n");
	}
	else
	{
		fprintf (stderr, "*** An error occured when patching the settings ***\n");
	}

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// In all PEq settings, divides by 2 the frequencies for all active
// bell curves to preserve the tones after bugfix from 2017-01-31
// b995bbb2212314856e46a86bb2da9505f25f55d0
void	PatchSetup::fix_peq_2017_02 (mfx::doc::Setup &setup)
{
	mfx::pi::peq::PEqDesc <4> desc_pi;
	const mfx::piapi::ParamDescInterface & desc_freq =
		desc_pi.get_param_info (
			mfx::piapi::ParamCateg_GLOBAL, mfx::pi::peq::Param_FREQ
		);
	const mfx::piapi::ParamDescInterface & desc_type =
		desc_pi.get_param_info (
			mfx::piapi::ParamCateg_GLOBAL, mfx::pi::peq::Param_TYPE
		);
	const mfx::piapi::ParamDescInterface & desc_gain =
		desc_pi.get_param_info (
			mfx::piapi::ParamCateg_GLOBAL, mfx::pi::peq::Param_GAIN
		);

	for (auto &bank : setup._bank_arr)
	{
		for (auto &prog : bank._prog_arr)
		{
			for (auto &slot_node : prog._layer._slot_map)
			{
				if (slot_node.second.get () != nullptr)
				{
					auto &         slot = *(slot_node.second);
					if (slot._pi_model == "peq")
					{
						mfx::doc::PluginSettings * settings_ptr =
							slot.test_and_get_settings (mfx::PiType_MAIN);
						if (settings_ptr != nullptr)
						{
							fix_peq_freq  (
								*settings_ptr, desc_freq, desc_type, desc_gain
							);
						}
					}
				}
			}
		}
	}

	auto           it_peq = setup._map_plugin_settings.find ("peq");
	if (it_peq != setup._map_plugin_settings.end ())
	{
		for (auto &settings_sptr : it_peq->second._cell_arr)
		{
			if (settings_sptr.get () != nullptr)
			{
				fix_peq_freq  (
					settings_sptr->_main, desc_freq, desc_type, desc_gain
				);
			}
		}
	}
}



void	PatchSetup::fix_peq_freq (mfx::doc::PluginSettings &settings, const mfx::piapi::ParamDescInterface &desc_freq, const mfx::piapi::ParamDescInterface &desc_type, const mfx::piapi::ParamDescInterface &desc_gain)
{
	const int      nbr_param = int (settings._param_list.size ());
	const int      nbr_bands = nbr_param / mfx::pi::peq::Param_NBR_ELT;

	for (int b_cnt = 0; b_cnt < nbr_bands; ++b_cnt)
	{
		const int	   index_base = b_cnt * mfx::pi::peq::Param_NBR_ELT;
		const int      index_type = index_base + mfx::pi::peq::Param_TYPE;
		const int      index_freq = index_base + mfx::pi::peq::Param_FREQ;
		const int      index_gain = index_base + mfx::pi::peq::Param_GAIN;

		const float    type_nrm = settings._param_list [index_type];
		const double   type_nat = desc_type.conv_nrm_to_nat (type_nrm);
		const mfx::pi::peq::PEqType   type =
			mfx::pi::peq::PEqType (fstb::round_int (type_nat));

		if (type == mfx::pi::peq::PEqType_PEAK)
		{
			const float    gain_nrm = settings._param_list [index_gain];
			const double   gain_nat = desc_gain.conv_nrm_to_nat (gain_nrm);
			if (! fstb::is_eq_rel (gain_nat, 1.0, 1e-3))
			{
				float          freq_nrm = settings._param_list [index_freq];
				double         freq_nat = desc_freq.conv_nrm_to_nat (freq_nrm);

				freq_nat *= 0.5;

				freq_nrm = float (desc_freq.conv_nat_to_nrm (freq_nat));
				settings._param_list [index_freq] = freq_nrm;
			}
		}
	}
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
