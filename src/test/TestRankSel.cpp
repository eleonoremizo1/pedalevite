/*****************************************************************************

        TestRankSel.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/fir/RankSelA.h"
#include "mfx/dsp/fir/RankSelL.h"
#include "test/GenSig.h"
#include "test/TestRankSel.h"

#include <vector>
#include <algorithm>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestRankSel::perform_test ()
{
	printf ("Testing mfx::dsp::fir::RankSel*...\n");

	const int      med_len = 63;
	const int      rank    = med_len / 2;
	std::vector <float> med_buf (med_len);
	mfx::dsp::fir::RankSelA	med_flt_a;
	mfx::dsp::fir::RankSelL	med_flt_l;

	med_flt_a.set_len (med_len, 0);
	med_flt_l.set_len (med_len, 0);

	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr;

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);
	if (ret_val == 0)
	{
		const size_t   len     = chn_arr [0].size ();
		const float *  src_ptr = chn_arr [0].data ();
		for (size_t pos = 0; pos < len && ret_val == 0; ++pos)
		{
			// Naive median filter (reference)
			for (size_t idx = 0; idx < med_len; ++idx)
			{
				if (idx > pos)
				{
					med_buf [idx] = 0;
				}
				else
				{
					med_buf [idx] = src_ptr [pos - idx];
				}
			}
			std::sort (med_buf.begin (), med_buf.end ());
			const float    med_ref = med_buf [rank];
			const float    med_a   = med_flt_a.process_sample (src_ptr [pos]);
			const float    med_l   = med_flt_l.process_sample (src_ptr [pos]);

			if (med_a != med_ref)
			{
				assert (false);
				ret_val = -1;
			}
			if (med_l != med_ref)
			{
				assert (false);
				ret_val = -1;
			}
		}
	}

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
