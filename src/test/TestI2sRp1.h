/*****************************************************************************

        TestI2sRp1.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestI2sRp1_HEADER_INCLUDED)
#define TestI2sRp1_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/adrv/DPvabI2sCommon.h"



class TestI2sRp1
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _pin_bclk = mfx::adrv::DPvabI2sCommon::_pin_bclk;
	static constexpr int _pin_lrck = mfx::adrv::DPvabI2sCommon::_pin_lrck;
	static constexpr int _pin_din  = mfx::adrv::DPvabI2sCommon::_pin_din;
	static constexpr int _pin_dout = mfx::adrv::DPvabI2sCommon::_pin_dout;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ~TestI2sRp1 ()                              = delete;
	               TestI2sRp1 ()                               = delete;
	               TestI2sRp1 (const TestI2sRp1 &other)        = delete;
	               TestI2sRp1 (TestI2sRp1 &&other)             = delete;
	TestI2sRp1 &   operator = (const TestI2sRp1 &other)        = delete;
	TestI2sRp1 &   operator = (TestI2sRp1 &&other)             = delete;
	bool           operator == (const TestI2sRp1 &other) const = delete;
	bool           operator != (const TestI2sRp1 &other) const = delete;

}; // class TestI2sRp1



//#include "test/TestI2sRp1.hpp"



#endif   // TestI2sRp1_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
