/*****************************************************************************

        TestI2sClient.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestI2sClient_HEADER_INCLUDED)
#define TestI2sClient_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

class TestI2sClient
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static int     test_software ();
#if defined (PV_RPI_VER_MAJOR)
	static int     test_gpio ();
#endif // PV_RPI_VER_MAJOR



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ~TestI2sClient ()                              = delete;
	               TestI2sClient ()                               = delete;
	               TestI2sClient (const TestI2sClient &other)     = delete;
	               TestI2sClient (TestI2sClient &&other)          = delete;
	TestI2sClient& operator = (const TestI2sClient &other)        = delete;
	TestI2sClient& operator = (TestI2sClient &&other)             = delete;
	bool           operator == (const TestI2sClient &other) const = delete;
	bool           operator != (const TestI2sClient &other) const = delete;

}; // class TestI2sClient



//#include "test/TestI2sClient.hpp"



#endif   // TestI2sClient_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
