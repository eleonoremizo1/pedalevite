/*****************************************************************************

        TestDistPmChannel.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestDistPmChannel_HEADER_INCLUDED)
#define TestDistPmChannel_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestDistPmChannel
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static int     single_test (bool chelou_flag, float feedback);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestDistPmChannel ()                               = delete;
	               TestDistPmChannel (const TestDistPmChannel &other) = delete;
	               TestDistPmChannel (TestDistPmChannel &&other)      = delete;
	TestDistPmChannel &
	               operator = (const TestDistPmChannel &other)        = delete;
	TestDistPmChannel &
	               operator = (TestDistPmChannel &&other)             = delete;
	bool           operator == (const TestDistPmChannel &other) const = delete;
	bool           operator != (const TestDistPmChannel &other) const = delete;

}; // class TestDistPmChannel



//#include "test/TestDistPmChannel.hpp"



#endif // TestDistPmChannel_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
