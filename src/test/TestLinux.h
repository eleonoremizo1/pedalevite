/*****************************************************************************

        TestLinux.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestLinux_HEADER_INCLUDED)
#define TestLinux_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



class TestLinux
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     test_file_write_fs_ro ();
	static int     test_gettimeofday ();
	static int     test_nanosleep_latency ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static inline int64_t
	               get_tod_us () noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestLinux ()                               = delete;
	               TestLinux (const TestLinux &other)         = delete;
	               TestLinux (TestLinux &&other)              = delete;
	TestLinux &    operator = (const TestLinux &other)        = delete;
	TestLinux &    operator = (TestLinux &&other)             = delete;
	bool           operator == (const TestLinux &other) const = delete;
	bool           operator != (const TestLinux &other) const = delete;

}; // class TestLinux



//#include "test/TestLinux.hpp"



#endif // TestLinux_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
