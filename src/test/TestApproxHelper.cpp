/*****************************************************************************

        TestApproxHelper.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/txt/neutral/fnc.h"
#include "test/EPSPlot.h"
#include "test/Gridaxis.h"
#include "test/TestApproxHelper.h"

#include <set>

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	TestApproxHelper::draw_errors_to_files (std::string fnc_name, const ErrStateMap <1> &e_map, const TestSpec &tst_spec)
{
	std::string      fnc_name_file =
		fstb::txt::neutral::replace_char (fnc_name, " <>|:*\\/\?\"", '_');
	const std::string base ("results/approx-" + fnc_name_file + "-");

	constexpr Point   margin { 16, 16 };
	constexpr Point   sz { 576, 384 };

	for (int type_idx = 0; type_idx < int (ErrType::_nbr_elt); ++type_idx)
	{
		const auto     type = ErrType (type_idx);
		if (tst_spec.is_checked (type))
		{
			const auto     err_type  = std::string (_err_type_0_arr [type_idx]);
			const auto     pathname  = base + err_type.substr (0, 3) + ".eps";
			const auto     data_type = ErrDataType (type_idx);
			const auto     it_err    = e_map.find (data_type);
			assert (it_err != e_map.end ());
			const auto &   err       = it_err->second;
			Boundaries     bnd       = find_boundaries (err._cloud);

			// If we have to display the ULP, make sure they fit
			const auto     it_ulp    = e_map.find (ErrDataType::_ulp);
			if (type == ErrType::_rel && it_ulp != e_map.end ())
			{
				Boundaries     bnd_ulp = find_boundaries (it_ulp->second._cloud);
				bnd._cmin [1] = std::min (bnd._cmin [1], bnd_ulp._cmin [1]);
				bnd._cmax [1] = std::max (bnd._cmax [1], bnd_ulp._cmax [1]);
			}

			EPSPlot        plot (pathname.c_str (), 0, 0, sz [0], sz [1]);

			// Computes the tick size
			const auto     tick = compute_tick_size (bnd);

			Gridaxis			grid;
			grid.set_plot (plot, margin [0] * 2, margin [1] * 1.5f);
			grid.set_sci_mode (true);
			grid.set_size (sz [0] - margin [0] * 3, sz [1] - margin [1] * 3);
			grid.set_grid (true);
			for (int di = 0; di < Gridaxis::Direction_NBR_ELT; ++di)
			{
				const auto     d    = Gridaxis::Direction (di);
				auto &         axis = grid.use_axis (d);
				axis.set_scale (bnd._cmin [di], bnd._cmax [di]);
				axis.set_tick_dist (tick [di]);
				axis.activate_tick (true);
			}
			char           txt_0 [63+1];
			fstb::snprintf4all (txt_0, sizeof (txt_0), "|max| = %.3g", err._max);
			grid.set_title (fnc_name + ", " + err_type + " error. " + txt_0);

			plot.setFontSize (8.f);
			plot.setLineWidth (0.25f);
			plot.setLineCap (1); // Round
			plot.setLineJoin (1); // Round

			plot.setRGBColor (0.8f, 0.8f, 0.9f);
			draw_surface (grid, err);

			grid.render_background ();

			plot.setRGBColor (0, 0, 0.5f);
			draw_single_curve (
				grid, err,
				[] (const ErrState <1>::Range &r) { return r._e_min; }
			);
			draw_single_curve (
				grid, err,
				[] (const ErrState <1>::Range &r) { return r._e_max; }
			);

			if (type == ErrType::_rel && it_ulp != e_map.end ())
			{
				plot.setLineWidth (1.f);
				plot.setRGBColor (0, 0, 0);
				draw_ulp (grid, it_ulp->second);
			}
		}
	}
}



void	TestApproxHelper::draw_surface (Gridaxis &grid, const ErrState <1> &err)
{
	const auto     nbr_points = err._cloud.size ();
	std::vector <double> x_arr (nbr_points * 2);
	std::vector <double> y_arr (nbr_points * 2);

	for (size_t pos = 0; pos < nbr_points; ++pos)
	{
		const auto &   e = err._cloud [pos];
		x_arr [pos    ] = e._val [0];
		y_arr [pos    ] = e._e_max;
		const auto     pos_alt = nbr_points * 2 - 1 - pos;
		x_arr [pos_alt] = e._val [0];
		y_arr [pos_alt] = e._e_min;
	}

	grid.render_curve (x_arr.data (), y_arr.data (), int (x_arr.size ()), true);
}



template <typename G>
void	TestApproxHelper::draw_single_curve (Gridaxis &grid, const ErrState <1> &err, G get_y)
{
	const auto     nbr_points = err._cloud.size ();
	std::vector <double> x_arr (nbr_points);
	std::vector <double> y_arr (nbr_points);

	for (size_t pos = 0; pos < nbr_points; ++pos)
	{
		x_arr [pos] = err._cloud [pos]._val [0];
		y_arr [pos] = get_y (err._cloud [pos]);
	}

	grid.render_curve (x_arr.data (), y_arr.data (), int (x_arr.size ()));
}



void	TestApproxHelper::draw_ulp (Gridaxis &grid, const ErrState <1> &err)
{
	const auto     nbr_points = err._cloud.size ();
	std::vector <double> x_arr (nbr_points * 2);
	std::vector <double> y_arr (nbr_points * 2);

	for (size_t pos = 0; pos < nbr_points; ++pos)
	{
		const auto &   e = err._cloud [pos];
		x_arr [pos    ] = e._val [0];
		y_arr [pos    ] = e._e_max;
		const auto     pos_alt = pos + nbr_points;
		x_arr [pos_alt] = e._val [0];
		y_arr [pos_alt] = e._e_min;
	}

	grid.render_point_set (x_arr.data (), y_arr.data (), int (x_arr.size ()), 0);
}



// Prepare data for display. Quantizes the horizontal axis to keep resolution
// under a minimum threshold.
// Assumes cloud is sorted by ascending ._val [0]
void	TestApproxHelper::requantize_results (ErrState <1>::RangeArray &cloud)
{
	assert (! cloud.empty ());

	const auto     xmin = cloud.front ()._val [0];
	const auto     xmax = cloud.back ()._val [0];
	assert (xmin <= xmax);

	if (xmin == xmax)
	{
		return;
	}

	// Maximum number of different sampling positions on the horizontal axis
	constexpr auto graph_resol = 2500;

	const double   step = (xmax - xmin) / float (graph_resol);

	// Starts with an empty set we're going to fill
	ErrState <1>::RangeArray res;

	ErrState <1>::Range cur;
	cur._val [0] = xmin;
	bool           full_flag = false;

	for (const auto &range : cloud)
	{
		if (range._val [0] >= cur._val [0] + step && full_flag)
		{
			res.push_back (cur);
			full_flag = false;
		}
		if (range._val [0] >= xmin && range._val [0] <= xmax)
		{
			if (! full_flag)
			{
				cur       = range;
				full_flag = true;
			}
			else
			{
				cur._e_max = std::max (cur._e_max, range._e_max);
				cur._e_min = std::min (cur._e_min, range._e_min);
			}
		}
	}
	if (full_flag)
	{
		res.push_back (cur);
	}

	std::swap (cloud, res);
}



// cloud .val [0] density should be approximately homogenous across the whole
// range so the automatic clipping works correctly
TestApproxHelper::Boundaries	TestApproxHelper::find_boundaries (const ErrState <1>::RangeArray &cloud) noexcept
{
	assert (! cloud.empty ());

	Boundaries     bnd;

	// In some data sets, we may have spikes near the ends one ore more orders
	// of magnitude above the average value. This makes the global graph
	// unreadable. In this case we ignore the top quantile of the minima and
	// maxima to trim the spikes.
	const double   quantile = 0.01; // Discards 1 % of the min/max values
	const auto     top_size = size_t (
		std::max (fstb::ceil_int (double (cloud.size ()) * quantile), 1)
	);
	typedef std::multiset <float> TopSet; // Limited to top_size elements

	// Scans data
	TopSet         top_min;          // Stores the sign-opposite values
	TopSet         top_max;
	float          mi_full = +1e30f; // Min and max for the full data set
	float          ma_full = -1e30f;
	for (const auto &range : cloud)
	{
		auto           insert_max = [top_size] (TopSet &s, float val)
		{
			if (s.size () < top_size || val >= *(s.begin ()))
			{
				s.insert (val);
				if (s.size () > top_size)
				{
					s.erase (s.begin ());
				}
			}
		};

		// Statistics
		insert_max (top_min, -range._e_min);
		insert_max (top_max, +range._e_max);

		// Horizontal
		const auto     x = range._val [0];
		bnd._cmin [0] = std::min (bnd._cmin [0], x);
		bnd._cmax [0] = std::max (bnd._cmax [0], x);

		// Vertical
		mi_full = std::min (mi_full, range._e_min);
		ma_full = std::max (ma_full, range._e_max);
	}

	const auto     mi_clip = -*(top_min.begin ());
	const auto     ma_clip = +*(top_max.begin ());
	const auto     dif_clp = ma_clip - mi_clip;

	// Checks some kind of "crest factor" based on the ratio between the
	// actual peak-to-peak amplitude and the clipped signal.
	// If the crest factor is nice, we can just use the limits we found first.
	constexpr auto cf_limit = 10.0;
	const auto     dif_full = ma_full - mi_full;
	if (dif_full < dif_clp * cf_limit)
	{
		bnd._cmin [1] = mi_full;
		bnd._cmax [1] = ma_full;
	}

	// Otherwise we will have to clip the peaks.
	else
	{
		bnd._cmin [1] = mi_clip;
		bnd._cmax [1] = ma_clip;
	}

	refine_boundaries (bnd);

	return bnd;
}



void	TestApproxHelper::refine_boundaries (Boundaries &bnd) noexcept
{
	// Makes sure boundaries make a non-zero surface
	constexpr auto eps = 1e-30f;
	bnd._cmin [0] -= eps;
	bnd._cmax [0] += eps;
	bnd._cmin [1] -= eps;
	bnd._cmax [1] += eps;

	// Now add small margins for the vertical range, so the graph curve doesn't
	// hit the frame.
	constexpr auto ratio  = 1.f / 16;
	const auto     dif    = bnd._cmax [1] - bnd._cmin [1];
	const auto     margin = dif * ratio;
	bnd._cmin [1] -= margin;
	bnd._cmax [1] += margin;
}



TestApproxHelper::Point	TestApproxHelper::compute_tick_size (const Boundaries &bnd) noexcept
{
	Point          tick;

	for (int i = 0; i < int (tick.size ()); ++i)
	{
		const auto     vmin = bnd._cmin [i];
		const auto     vmax = bnd._cmax [i];
		const auto     vdif = double (vmax) - double (vmin);
		const auto     vdl  = log10 (vdif);
		const auto     vdli = int (floor (vdl));
		const auto     vds  = pow (10.0, double (vdli));
		const auto     vdr  = vdif / vds;
		const auto     tick_base =
			  (vdr < 2.01) ? 0.1
			: (vdr < 4.01) ? 0.2
			:                0.5;
		tick [i] = float (tick_base * vds);
	}

	return tick;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
