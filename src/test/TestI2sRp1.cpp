/*****************************************************************************

        TestI2sRp1.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/HigepioLinux.h"
#include "mfx/hw/MmapPtr.h"
#include "mfx/hw/rp1i2s.h"
#include "test/I2sEnd.h"
#include "test/TestI2sRp1.h"

#include <fcntl.h>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestI2sRp1::perform_test ()
{
	int            ret_val = 0;

// Before enabling this test, the following pins from the 40-pin header need
// to be connected together:
// 12 - 11 (SCLK  - GPIO17)
// 35 - 36 (LRCK  - GPIO16)
// 38 - 32 (SDIN  - GPIO12)
// 40 - 31 (SDOUT - GPIO6)
#if 0
	printf ("Testing I2S, direct access, manual transfers...\n");

	const char *   rp1_file_0 = "/sys/bus/pci/devices/0000:01:00.0/resource1";
	constexpr int  mem_flags  = O_RDWR | O_SYNC;

	mfx::hw::MmapPtr  pcm_mptr (
		// We use I2S1 here (same physical pins as I2S0 but clock slave)
		mfx::hw::rp1i2s::_i2s1_ofs,
		mfx::hw::rp1i2s::_i2s_blk_len,
		rp1_file_0, mem_flags
	);
	mfx::hw::MmapPtr  gpio_mptr (
		0xD0000, 0x30000, // Starts at io_bank0 and includes pads_bank0
		rp1_file_0, mem_flags
	);

	// Activates the I2S function on the GPIO pins
	auto           enable_i2s = [&gpio_mptr] (int gpio)
	{
		// Pad: +IE -OD
		constexpr uint32_t pad_ofs = 0xF0000 - 0xD0000;
		const auto     padr = 0x004 + (gpio << 2) + pad_ofs;
		gpio_mptr.set_field (
			padr,
			(1 << 6) | (1 << 7),
			(1 << 6) | (0 << 7)
		);

		// CTRL: FUNCSEL, OUTOVER, OEOVER
		const auto     cadr = 0x004 + (gpio << 3);
		gpio_mptr.set_field (
			cadr,
			0x1F | (3 << 12) | (3 << 14),
			0x04 | (0 << 12) | (0 << 14)
		);
	};

	enable_i2s (_pin_bclk);
	enable_i2s (_pin_lrck);
	enable_i2s (_pin_din);
	enable_i2s (_pin_dout);

	using namespace mfx::hw::rp1i2s;
	[[maybe_unused]] volatile uint32_t dummy;

	constexpr int  prefill = 4;

	// Prepare the I2S module
	{
		// First, disables transfers and channels
		pcm_mptr.at (_cer) = 0;
		pcm_mptr.at (_rer) = 0;
		pcm_mptr.at (_ter) = 0;

		// Sample resolution: 32 bits
		pcm_mptr.at (_rcr) = _xcr_ws_32;
		pcm_mptr.at (_tcr) = _xcr_ws_32;

		// FIFO capacity thresholds
		pcm_mptr.at (_rfcr) = 0;
		pcm_mptr.at (_tfcr) = prefill - 1;

		// Enables channels
		pcm_mptr.at (_rer) = _xer_en;
		pcm_mptr.at (_ter) = _xer_en;

		// Disables DMA for the first pair of channels
		pcm_mptr.clr (_dmacr, _dmacr_txch0 | _dmacr_rxch0);

		// Sample resolution: 32 bits
		pcm_mptr.at (_ccr) = _ccr_res_32;

		// Disables DMA globally
		pcm_mptr.clr (_dmacr, _dmacr_tx | _dmacr_rx);

		// Enables playback and capture
		pcm_mptr.at (_rxffr) = _rxffr_en;
		pcm_mptr.at (_txffr) = _txffr_en;

		// Enables the device
		pcm_mptr.at (_ier) = _ier_dev_en;

		// Interrupts
		pcm_mptr.set_field (
			_imr, _imr_tx_mask | _imr_rx_mask, _imr_tx_ei | _imr_rx_ei
		);

		// Enables interrupts for the channel pair
		pcm_mptr.at (_irer) = _irer_int_en;
		pcm_mptr.at (_iter) = _iter_int_en;

		// Clears the capture FIFOs
		for (int k = 0; k < _fifo_depth; ++k)
		{
			dummy = pcm_mptr.at (_lrbr_lthr);
			dummy = pcm_mptr.at (_rrbr_rthr);
		}

		// Prefills the playback FIFOs
		for (int k = 0; k < prefill; ++k)
		{
			pcm_mptr.at (_lrbr_lthr) = k * 2 + 100;
			pcm_mptr.at (_rrbr_rthr) = k * 2 + 100 + 1;
		}
	}

	I2sEnd         cli_rem (true, 32, 1, false, true); // Master
	mfx::hw::HigepioLinux io;
	cli_rem.set_io (io);

	// Aknowledge any interrupt
	dummy = pcm_mptr.at (_ror) + pcm_mptr.at (_tor);

	// Start
	pcm_mptr.at (_cer) = _cer_trans;

	int            nbr_tx  = 0;
	int            pos_spl = 0;
	int            t       = 0;
	do
	{
		pos_spl = cli_rem.run_step (t);
		const auto     isr = pcm_mptr.at (_isr);
		dummy = pcm_mptr.at (_ror) + pcm_mptr.at (_tor);
		if ((isr & _isr_rxda) != 0)
		{
			const uint32_t dl = pcm_mptr.at (_lrbr_lthr);
			const uint32_t dr = pcm_mptr.at (_rrbr_rthr);
			printf ("Data is available. LRBR_LTHR: %u, RRBR_RTHR: %u\n", dl, dr);
		}
		if ((isr & _isr_txfe) != 0)
		{
			const auto     dl = 20000 + nbr_tx * 2;
			const auto     dr = dl + 1;
			printf ("Data is requested. Sending: %u, %u\n", dl, dr);
			pcm_mptr.at (_lrbr_lthr) = dl;
			pcm_mptr.at (_rrbr_rthr) = dr;
			++ nbr_tx;
		}

		printf (".");

		io.sleep (1'000);
		++ t;
	}
	while (pos_spl < 20);

	// Stop
	pcm_mptr.at (_irer) = 0;
	pcm_mptr.at (_iter) = 0;
	pcm_mptr.set_field (
		_imr, _imr_tx_mask | _imr_rx_mask, _imr_tx_di | _imr_rx_di
	);
	pcm_mptr.at (_cer) = 0;
	pcm_mptr.at (_ier) = 0;
	printf ("\n");

	cli_rem.dump_traces ();

	printf ("Done.\n");
#endif

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
