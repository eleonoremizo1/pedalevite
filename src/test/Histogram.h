/*****************************************************************************

        Histogram.h
        Author: Laurent de Soras, 2024

This class builds an histogram from a data set and displays it as ASCII art.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (Histogram_HEADER_INCLUDED)
#define Histogram_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <string>
#include <tuple>
#include <vector>



class Histogram
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           reset (double v_min, double v_max, int sz, std::string name);
	template <typename IT>
	void           reset_and_fill (IT first, IT last, int sz, std::string name);

	void           insert (double val) noexcept;
	template <typename IT>
	void           insert (IT first, IT last) noexcept;

	std::string    print (int h = 10, bool log_flag = false) const;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	double         compute_scale () const noexcept;

	template <typename IT>
	static std::tuple <double, double>
	               find_min_max (IT first, IT last) noexcept;

	double         _v_min = 0;
	double         _v_max = 1;
	int            _sz    = 79;
	std::vector <int>
	               _hist { _sz, 0 };
	std::string    _name;

	// Pre-calculated data
	double         _scale = compute_scale ();



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Histogram &other) const = delete;
	bool           operator != (const Histogram &other) const = delete;

}; // class Histogram



#include "test/Histogram.hpp"



#endif   // Histogram_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
