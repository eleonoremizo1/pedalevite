/*****************************************************************************

        I2sEnd.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (I2sEnd_HEADER_INCLUDED)
#define I2sEnd_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#if defined (PV_RPI_VER_MAJOR)
# include "mfx/hw/GpioPin.h"
#endif // PV_RPI_VER_MAJOR
#include "test/BitTrace.h"
#include "test/I2sClient.h"
#include "test/I2sClientCbInterface.h"

#include <array>



#if defined (PV_RPI_VER_MAJOR)

namespace mfx
{
namespace hw
{
	class HigepioLinux;
}
}

#endif // PV_RPI_VER_MAJOR



class I2sEnd
:	public I2sClient
,	public I2sClientCbInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       I2sEnd (bool master_flag, int bitdepth, int transfer_lag, bool left_high_flag, bool rec_flag);
	               ~I2sEnd ();

	void           set_other (I2sEnd &other);
#if defined (PV_RPI_VER_MAJOR)
	void           set_io (mfx::hw::HigepioLinux &io);
#endif // PV_RPI_VER_MAJOR
	void           set_pin (I2sPin p, bool state_flag) noexcept;
	int            run_step (int timestamp);
	void           dump_traces () const;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// I2sClientCbInterface
	void           do_proc_audio (SampleFrame &data_bidir) final;
	void           do_set_pin (I2sPin p, bool state_flag) final;
	void           do_notify_clock_error () final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

#if defined (PV_RPI_VER_MAJOR)
	void           write_gpio (I2sPin p, bool state_flag);
	bool           read_gpio (I2sPin p) const;
#endif // PV_RPI_VER_MAJOR

	bool           _master_flag = false;
	bool           _rec_flag  = false;
	const char *   _name_0    = nullptr;
	I2sEnd *       _other_ptr = nullptr; // Not set: use GPIO
	int            _timestamp = 0;
	int            _count     = 0;
	// Used only when clock slave
	BitTrace       _bt;
#if defined (PV_RPI_VER_MAJOR)
	mfx::hw::HigepioLinux *
	               _io_ptr    = nullptr;

	static constexpr std::array <std::array <int, I2sPin_NBR_ELT>, 2>
	               _pin_arr =
	{{
		{{ // Slave
			mfx::hw::GpioPin::_snd_bclk, mfx::hw::GpioPin::_snd_lrck,
			mfx::hw::GpioPin::_snd_din, mfx::hw::GpioPin::_snd_dout
		}},
		{{ 17, 16, 6, 12 }} // Master
	}};
#endif // PV_RPI_VER_MAJOR



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               I2sEnd ()                               = delete;
	               I2sEnd (const I2sEnd &other)            = delete;
	               I2sEnd (I2sEnd &&other)                 = delete;
	I2sEnd &       operator = (const I2sEnd &other)        = delete;
	I2sEnd &       operator = (I2sEnd &&other)             = delete;
	bool           operator == (const I2sEnd &other) const = delete;
	bool           operator != (const I2sEnd &other) const = delete;

}; // class I2sEnd



//#include "test/I2sEnd.hpp"



#endif   // I2sEnd_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
