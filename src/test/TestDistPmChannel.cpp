/*****************************************************************************

        TestDistPmChannel.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/dsp/osc/SweepingSin.h"
#include "mfx/pi/distpm/DistPmChannel.h"
#include "mfx/FileOpWav.h"
#include "test/Benchmark.h"
#include "test/TestDistPmChannel.h"

#include <tuple>
#include <vector>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestDistPmChannel::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing mfx::pi::distpm::DistPmChannel...\n");

	typedef std::tuple <bool, float> V;
	for (auto [chelou_flag, feedback] : {
		V (false, 0.f),
		V (false, 1.f),
		V (true , 0.f),
		V (true , 1.f)
	})
	{
		if (ret_val == 0)
		{
			ret_val = single_test (chelou_flag, feedback);
		}
	}

	printf ("Done.\n\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestDistPmChannel::single_test (bool chelou_flag, float feedback)
{
	const float    amount = 1.f;

	const double   sample_freq = 44100;
	const int      len         = fstb::round_int (sample_freq * 16);
	std::vector <float> src (len, 0.f);
	std::vector <float> dst (len);

	mfx::dsp::osc::SweepingSin ssin (sample_freq, 110.f, 110.f * 64);
	ssin.generate (src.data (), len);

#if 0

	const int      max_dly = fstb::ceil_int (sample_freq / 40.0); // 40 Hz
	float          amt     = max_dly * amount;

	const auto     c_lpf = float ((50.0 / sample_freq) * 2 * fstb::PI);
	float          y1 = 0.f;
	dst [0] = src [0];
	dst [1] = src [1];
	float          fdbk = 0;
	for (int pos = 2; pos < len; ++pos)
	{
		fdbk += (y1 - fdbk) * c_lpf;
		const auto     fdbk_out = (fdbk + 1) * 0.5f; // Scales range [-1 ; 1] -> [0 ; 1]
		float          dly = fdbk_out * amt;
		dly = fstb::limit (dly, 0.f, float (max_dly));
		auto           pos_s = pos - dly;
		pos_s = std::max (pos_s, 0.f);
		const auto     pos_s_i = fstb::floor_int (pos_s);
		const auto     pos_s_f = pos_s - float (pos_s_i);
		const auto     x0 = src [pos_s_i    ];
		const auto     x1 = src [pos_s_i + 1];
		const auto     y  = x0 + pos_s_f * (x1 - x0);
		
		dst [pos] = y;
		y1 = y;
	}

	constexpr char *  pathname_0 = "results/fmfeedback_0.wav";

#else

	constexpr int     max_buf_len = 256;
	mfx::pi::distpm::DistPmChannel dist;
	mfx::pi::distpm::DistPmChannel::BufAlign buf_o1 (max_buf_len * mfx::pi::distpm::DistPmChannel::_ovrspl1);
	mfx::pi::distpm::DistPmChannel::BufAlign buf_o2 (max_buf_len * mfx::pi::distpm::DistPmChannel::_ovrspl_tot);
	[[maybe_unused]] int latency;
	dist.reset (sample_freq, latency, buf_o1, buf_o2);
	dist.set_modulation_amount (amount);
	dist.set_mode (chelou_flag);
	dist.set_feedback_amount (feedback);
	int            pos = 0;
	
	Benchmark  ben;

	ben.start_single ();
	do
	{
		const int      work_len = std::min (len - pos, max_buf_len);
		dist.process_block (&dst [pos], &src [pos], work_len);
		pos += work_len;
	}
	while (pos < len);
	ben.stop_single ();

	const double   spl_per_s = ben.compute_rate (len);
	const double   mega_sps  = spl_per_s / 1e6;
	printf (
		"%s, f=%3d%%: %12.3f Mop/s.\n",
		(chelou_flag) ? "che" : "std",
		fstb::round_int (feedback * 100),
		mega_sps
	);

	char           pathname_0 [1023+1];
	fstb::snprintf4all (pathname_0, sizeof (pathname_0),
		"results/distpmchannel_%s_f%03d.wav",
		(chelou_flag) ? "che" : "std",
		fstb::round_int (feedback * 100)
	);

#endif

	int            ret_val = mfx::FileOpWav::save (
		pathname_0, dst, sample_freq, 0.5f
	);

	return ret_val;
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
