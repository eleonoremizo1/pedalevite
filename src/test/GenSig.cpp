/*****************************************************************************

        GenSig.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/dsp/nz/WhiteFast.h"
#include "mfx/FileOpWav.h"
#include "test/GenSig.h"

#include <algorithm>

#include <cassert>
#include <cmath>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	GenSig::gen_sample (double &sample_freq, std::vector <std::vector <float> > &chn_arr)
{
	return mfx::FileOpWav::load (
		"../../../src/test/samples/"
#if 1
		"guitar-01.wav"
#else
		"guitar-02.wav"
#endif
		, chn_arr, sample_freq
	);
}



int	GenSig::gen_sine_decay (double &sample_freq, std::vector <std::vector <float> > &chn_arr)
{
	sample_freq = 44100;
	chn_arr.resize (1);
	const size_t   len = size_t (sample_freq * 10);
	auto &         dst = chn_arr.front ();
	dst.resize (len);
	const double   freq = 200;
	const double   muls = fstb::PI * 2 * freq / sample_freq;
	const double   mule = -1 / sample_freq;
	for (size_t i = 0; i < len; ++i)
	{
		dst [i] = float (
			  sin (double (i) * muls)
			* exp (double (i) * mule)
		);
	}

	return 0;
}



int	GenSig::gen_noise_w (double &sample_freq, std::vector <std::vector <float> > &chn_arr)
{
	sample_freq = 44100;
	if (chn_arr.empty ())
	{
		chn_arr.resize (1);
	}
	const size_t   len = size_t (sample_freq * 10);
	mfx::dsp::nz::WhiteFast gen;
	gen.set_rough_level (0.75f);
	for (auto &chn : chn_arr)
	{
		chn.resize (len);
		gen.process_block (&chn [0], int (len));
	}

	return 0;
}



// duration and spike_len in seconds
int	GenSig::gen_spikes (double &sample_freq, std::vector <std::vector <float> > &chn_arr, double duration, double spike_len)
{
	assert (duration > 0);
	assert (spike_len > 0);

	sample_freq = 44100;
	if (chn_arr.empty ())
	{
		chn_arr.resize (1);
	}
	const size_t   len = size_t (sample_freq * duration);
	for (auto &chn : chn_arr)
	{
		chn.clear ();
		chn.resize (len, 0.f);
		for (double t = 0; t < duration; t += spike_len)
		{
			const size_t   pos = size_t (t * sample_freq);
			assert (pos < len);
			chn [pos] = 1;
		}
	}

	return 0;
}



// duration, sine_duration and fade in seconds
int	GenSig::gen_short_sine (double &sample_freq, std::vector <std::vector <float> > &chn_arr, double duration, double sine_duration, double freq, double vol, double fade)
{
	assert (duration > 0);
	assert (sine_duration > 0);
	assert (sine_duration <= duration);
	assert (freq > 0);
	assert (fade * 2 <= sine_duration);

	sample_freq = 44100;
	if (chn_arr.empty ())
	{
		chn_arr.resize (1);
	}
	const size_t   len    = size_t (sample_freq *      duration);
	const size_t   len_s  = size_t (sample_freq * sine_duration);
	const double   inv_fs = 1.0 / sample_freq;
	for (auto &chn : chn_arr)
	{
		chn.clear ();
		chn.resize (len, 0.f);
		for (size_t k = 0; k < len_s; ++k)
		{
			float          env = 1;
			const double   t   = double (k) * inv_fs;
			auto           make_fade =
				[fade] (double x)
				{
					return 0.5f - float (cos (fstb::PI * x / fade)) * 0.5f;
				};
			if (t < fade)
			{
				env *= make_fade (t);
			}
			else if (t > sine_duration - fade)
			{
				env *= make_fade (sine_duration - t);
			}
			const float    vv  = float (vol) * env;
			chn [k] = float (sin (2 * fstb::PI * freq * inv_fs * double (k))) * vv;
		}
	}

	return 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
