/*****************************************************************************

        TestApproxTrans.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Approx.h"
#include "test/TestApproxHelper.h"
#include "test/TestApproxTrans.h"

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestApproxTrans::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing fstb::Approx (trans)...\n");

	typedef fstb::Vf32 Vf32;
	typedef TestApproxHelper Helper;
//	constexpr auto notol = Helper::_notol;
//	constexpr auto pi    = fstb::PI;

	// log2

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return logf (x); },
		"std::log (float)", 1e-3, 1e3
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return log (x); },
		"std::log (double)", 1e-3, 1e3
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return log2 (x); },
		[] (float x) { return fstb::Approx::log2_crude (x); },
		[] (Vf32 x) { return fstb::Approx::log2_crude (x); },
		"log2_crude", 1e-3, 1e3, { { 0.045, 0.0 }, true, { 1.0, 2.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return log2 (x); },
		[] (double x) { return fstb::Approx::log2_crude (x); },
		"log2_crude", 1e-3, 1e3, { { 0.045, 0.0 }, true, { 1.0, 2.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return log2 (x); },
		[] (float x) { return fstb::Approx::log2_crude2 (x); },
		[] (Vf32 x) { return fstb::Approx::log2_crude2 (x); },
		"log2_crude2", 1e-3, 1e3, { { 0.004, 0.0 }, true, { 1.0, 2.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return log2 (x); },
		[] (double x) { return fstb::Approx::log2_crude2 (x); },
		"log2_crude2", 1e-3, 1e3, { { 0.004, 0.0 }, true, { 1.0, 2.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return log2 (x); },
		[] (float x) { return fstb::Approx::log2 (x); },
		[] (Vf32 x) { return fstb::Approx::log2 (x); },
		"log2", 1e-3, 1e3, { { 0.01, 0.0 }, true, { 1.0, 2.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return log2 (x); },
		[] (double x) { return fstb::Approx::log2 (x); },
		"log2", 1e-3, 1e3, { { 0.01, 0.0 }, true, { 1.0, 2.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return log2 (x); },
		[] (float x) { return fstb::Approx::log2_5th (x); },
		[] (Vf32 x) { return fstb::Approx::log2_5th (x); },
		"log2_5th", 1e-3, 1e3, { { 0.0002, 0.0 }, true, { 1.0, 2.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return log2 (x); },
		[] (double x) { return fstb::Approx::log2_5th (x); },
		"log2_5th", 1e-3, 1e3, { { 0.0002, 0.0 }, true, { 1.0, 2.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return log2 (x); },
		[] (float x) { return fstb::Approx::log2_7th (x); },
		[] (Vf32 x) { return fstb::Approx::log2_7th (x); },
		"log2_7th", 1e-3, 1e3, { { 3e-06, 0.0 }, true, { 1.0, 2.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return log2 (x); },
		[] (double x) { return fstb::Approx::log2_7th (x); },
		"log2_7th", 1e-3, 1e3, { { 3e-06, 0.0 }, true, { 1.0, 2.0 } }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return log2 (x); },
		[] (Vf32 x) { return fstb::log2 (x); },
		"log2", 1e-3, 1e3, { { 1e-06, 0.0 }, true, { 1.0, 2.0 } }
	);

	// exp, small range

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return expf (x); },
		"std::exp (float)", -20, 20
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return exp (x); },
		"std::exp (double)", -20, 20
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return exp (x); },
		[] (float x) { return fstb::Approx::exp_m <10> (x); },
		[] (Vf32 x) { return fstb::Approx::exp_m <10> (x); },
		"exp_m <10>", -2, 2, { { 0, 2.1e-3 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp (x); },
		[] (double x) { return fstb::Approx::exp_m <10> (x); },
		"exp_m <10>", -2, 2, { { 0, 2.1e-3 }, true }
	);

	// exp2

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return exp2 (x); },
		[] (float x) { return fstb::Approx::exp2_crude (x); },
		[] (Vf32 x) { return fstb::Approx::exp2_crude (x); },
		"exp2_crude", -20.0, 20.0, { { 0.0, 0.031 }, true, { 0.0, 1.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp2 (x); },
		[] (double x) { return fstb::Approx::exp2_crude (x); },
		"exp2_crude", -20.0, 20.0, { { 0, 0.03 }, true, { 0.0, 1.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return exp2 (x); },
		[] (float x) { return fstb::Approx::exp2_crude2 (x); },
		[] (Vf32 x) { return fstb::Approx::exp2_crude2 (x); },
		"exp2_crude2", -20.0, 20.0, { { 0.0, 0.0063 }, true, { 0.0, 1.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp2 (x); },
		[] (double x) { return fstb::Approx::exp2_crude2 (x); },
		"exp2_crude2", -20.0, 20.0, { { 0, 0.0063 }, true, { 0.0, 1.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return exp2 (x); },
		[] (float x) { return fstb::Approx::exp2 (x); },
		[] (Vf32 x) { return fstb::Approx::exp2 (x); },
		"exp2_approx", -20.0, 20.0, { { 0.0, 0.0035 }, true, { 0.0, 1.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp2 (x); },
		[] (double x) { return fstb::Approx::exp2 (x); },
		"exp2_approx", -20.0, 20.0, { { 0.0, 0.0035 }, true, { 0.0, 1.0 } }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return exp2 (x); },
		[] (float x) { return fstb::Approx::exp2_5th (x); },
		[] (Vf32 x) { return fstb::Approx::exp2_5th (x); },
		"exp2_5th", -20.0, 20.0, { { 0.0, 1e-6 }, true, { 0.0, 1.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp2 (x); },
		[] (double x) { return fstb::Approx::exp2_5th (x); },
		"exp2_5th", -20.0, 20.0, { { 0.0, 1e-6 }, true, { 0.0, 1.0 } }
	);

	Helper::test_op1_all_flt  (ret_val,
		[] (double x) { return exp2 (x); },
		[] (float x) { return fstb::Approx::exp2_7th (x); },
		[] (Vf32 x) { return fstb::Approx::exp2_7th (x); },
		"exp2_7th", -20.0, 20.0, { { 0.0, 1e-6 }, true, { 0.0, 1.0 } }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return exp2 (x); },
		[] (double x) { return fstb::Approx::exp2_7th (x); },
		"exp2_7th", -20.0, 20.0, { { 0.0, 1e-6 }, true, { 0.0, 1.0 } }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return exp2 (x); },
		[] (Vf32 x) { return fstb::exp2 (x); },
		"exp2", -20.0, 20.0, { { 0.0, 1e-6 }, true, { 0.0, 1.0 } }
	);

	// pow

	Helper::TestFncSpeed <float, 0>::test_op2 (
		[] (float x, float y) { return powf (x, y); },
		"std::pow (float)", 0.1, 10.0, -10.0, 10.0
	);
	Helper::TestFncSpeed <double, 0>::test_op2 (
		[] (double x, double y) { return pow (x, y); },
		"std::pow (double)", 0.1, 10.0, -10.0, 10.0
	);

	Helper::test_op2_all_flt (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (float x, float y) { return fstb::Approx::pow_crude (x, y); },
		[] (Vf32 x, Vf32 y) { return fstb::Approx::pow_crude (x, y); },
		"pow_crude", 0.1, 10.0, -10.0, 10.0, { { 0.0, 0.6 } }
	);
	Helper::test_op2_all_s <double> (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (double x, double y) { return fstb::Approx::pow_crude (x, y); },
		"pow_crude", 0.1, 10.0, -10.0, 10.0, { { 0.0, 0.6 } }
	);

	Helper::test_op2_all_flt (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (float x, float y) { return fstb::Approx::pow_3 (x, y); },
		[] (Vf32 x, Vf32 y) { return fstb::Approx::pow_3 (x, y); },
		"pow_3", 0.1, 10.0, -10.0, 10.0, { { 0.0, 0.08 } }
	);
	Helper::test_op2_all_s <double> (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (double x, double y) { return fstb::Approx::pow_3 (x, y); },
		"pow_3", 0.1, 10.0, -10.0, 10.0, { { 0.0, 0.08 } }
	);

	Helper::test_op2_all_flt (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (float x, float y) { return fstb::Approx::pow_5 (x, y); },
		[] (Vf32 x, Vf32 y) { return fstb::Approx::pow_5 (x, y); },
		"pow_5", 0.1, 10.0, -10.0, 10.0, { { 0.0, 1.3e-3 } }
	);
	Helper::test_op2_all_s <double> (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (double x, double y) { return fstb::Approx::pow_5 (x, y); },
		"pow_5", 0.1, 10.0, -10.0, 10.0, { { 0.0, 1.3e-3 } }
	);

	Helper::test_op2_all_flt (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (float x, float y) { return fstb::Approx::pow_7 (x, y); },
		[] (Vf32 x, Vf32 y) { return fstb::Approx::pow_7 (x, y); },
		"pow_7", 0.1, 10.0, -10.0, 10.0, { { 0.0, 2e-5 } }
	);
	Helper::test_op2_all_s <double> (ret_val,
		[] (double x, double y) { return pow (x, y); },
		[] (double x, double y) { return fstb::Approx::pow_7 (x, y); },
		"pow_7", 0.1, 10.0, -10.0, 10.0, { { 0.0, 2e-5 } }
	);

	// atan2
	// Make sure we do not test (0, 0) as the angle is not defined and
	// the results between the approx and the reference may differ.

	Helper::TestFncSpeed <float, 0>::test_op2 (
		[] (float x, float y) { return atan2f (x, y); },
		"std::atan2 (float)", -10.001, 10.0, -10.0, 10.0
	);
	Helper::TestFncSpeed <double, 0>::test_op2 (
		[] (double x, double y) { return atan2 (x, y); },
		"std::atan2 (double)", -10.001, 10.0, -10.0, 10.0
	);

	Helper::test_op2_all_s <float> (ret_val,
		[] (double x, double y) { return atan2 (y, x); },
		[] (float x, float y) { return fstb::Approx::atan2_3th (y, x); },
		"atan2_3th", -10.001, 10.0, -10.0, 10.0, { { 0.007, 0.0 }, false }
	);
	Helper::test_op2_all_s <float> (ret_val,
		[] (double x, double y) { return atan2 (y, x); },
		[] (float x, float y) { return fstb::Approx::atan2_7th (y, x); },
		"atan2_7th", -10.001, 10.0, -10.0, 10.0, { { 0.00012, 0.0 }, false }
	);

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
