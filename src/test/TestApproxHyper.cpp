/*****************************************************************************

        TestApproxHyper.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Approx.h"
#include "test/TestApproxHelper.h"
#include "test/TestApproxHyper.h"

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestApproxHyper::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing fstb::Approx (hyper)...\n");

	typedef fstb::Vf32 Vf32;
	typedef TestApproxHelper Helper;
	constexpr auto notol = Helper::_notol;
//	constexpr auto pi    = fstb::PI;

	// tanh

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return tanhf (x); },
		"std::tanh (float)", -5.0, 5.0
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return tanh (x); },
		"std::tanh (double)", -5.0, 5.0
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tanh (x); },
		[] (float x) { return fstb::Approx::tanh_bigtick_modlds (x); },
		[] (Vf32 x) { return fstb::Approx::tanh_bigtick_modlds (x); },
		"tanh_bigtick_modlds", -5.0, 5.0, { { 0.025, 0.04 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tanh (x); },
		[] (double x) { return fstb::Approx::tanh_bigtick_modlds (x); },
		"tanh_bigtick_modlds", -5.0, 5.0, { { 0.025, 0.04 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tanh (x); },
		[] (float x) { return fstb::Approx::tanh_mystran (x); },
		[] (Vf32 x) { return fstb::Approx::tanh_mystran (x); },
		"tanh_mystran", -5.0, 5.0, { { 0.004, 0.004 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tanh (x); },
		[] (double x) { return fstb::Approx::tanh_mystran (x); },
		"tanh_mystran", -5.0, 5.0, { { 0.004, 0.004 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tanh (x); },
		[] (float x) { return fstb::Approx::tanh_andy (x); },
		[] (Vf32 x) { return fstb::Approx::tanh_andy (x); },
		"tanh_andy", -5.0, 5.0, { { 7e-4, 7e-4 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tanh (x); },
		[] (double x) { return fstb::Approx::tanh_andy (x); },
		"tanh_andy", -5.0, 5.0, { { 7e-4, 7e-4 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tanh (x); },
		[] (float x) { return fstb::Approx::tanh_urs (x); },
		[] (Vf32 x) { return fstb::Approx::tanh_urs (x); },
		"tanh_urs", -10.0, 10.0, { { 1.5e-05, 1.5e-05 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tanh (x); },
		[] (double x) { return fstb::Approx::tanh_urs (x); },
		"tanh_urs", -10.0, 10.0, { { 1.5e-05, 1.5e-05 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tanh (x); },
		[] (float x) { return fstb::Approx::tanh_2dat (x); },
		[] (Vf32 x) { return fstb::Approx::tanh_2dat (x); },
		"tanh_2dat", -10.0, 10.0, { { 1e-06, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tanh (x); },
		[] (double x) { return fstb::Approx::tanh_2dat (x); },
		"tanh_2dat", -10.0, 10.0, { { 1e-06, notol }, true }
	);

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
