/*****************************************************************************

        Classname.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (Classname_CODEHEADER_INCLUDED)
#define Classname_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <algorithm>
#include <limits>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: reset_and_fill
Description:
	Initialises the histogram and fills the data set.
Input parameters:
	- first, last: iterators on the first and one-past-last elements to add.
		New values can be added subsequently, but valid data range is calculated
		from this set.
	- sz: number of segments in the histogram. >= 2.
		Corresponds to the number of text columns for ASCII display.
	- name: title of the histogram.
Throws: std::vector related exception
==============================================================================
*/

template <typename IT>
void	Histogram::reset_and_fill (IT first, IT last, int sz, std::string name)
{
	const auto       [mi, ma] = find_min_max (first, last);
	reset (mi, ma, sz, name);
	insert (first, last);
}



/*
==============================================================================
Name: insert
Description:
	Add a set of new value to the histogram data set
Input parameters:
	- first, last: iterators on the first and one-past-last elements to add.
		Elements are first converted to double before insertion.
==============================================================================
*/

template <typename IT>
void	Histogram::insert (IT first, IT last) noexcept
{
	std::for_each (first, last, [this] (const auto &x) {
		this->insert (double (x));
	});
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename IT>
std::tuple <double, double>	Histogram::find_min_max (IT first, IT last) noexcept
{
	double         mi = std::numeric_limits <double>::max ();
	double         ma = std::numeric_limits <double>::lowest ();

	std::for_each (first, last, [&mi, &ma] (const auto &x) {
		const auto     d = double (x);
		mi = std::min (mi, d);
		ma = std::max (ma, d);
	});

	// Handles edge cases (empty set, unique value...)
	if (ma <= mi)
	{
		mi = std::min (mi, 0.0);
		ma = std::max (ma, 1.0);
	}

	return { mi, ma };
}



#endif   // Classname_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
