/*****************************************************************************

        TestBitTrace.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestBitTrace_HEADER_INCLUDED)
#define TestBitTrace_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <initializer_list>



class BitTrace;

class TestBitTrace
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	template <typename T>
	static void    feed_traces_as_string (BitTrace &bt, std::initializer_list <T> l);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ~TestBitTrace ()                              = delete;
	               TestBitTrace ()                               = delete;
	               TestBitTrace (const TestBitTrace &other)      = delete;
	               TestBitTrace (TestBitTrace &&other)           = delete;
	TestBitTrace & operator = (const TestBitTrace &other)        = delete;
	TestBitTrace & operator = (TestBitTrace &&other)             = delete;
	bool           operator == (const TestBitTrace &other) const = delete;
	bool           operator != (const TestBitTrace &other) const = delete;

}; // class TestBitTrace



//#include "test/TestBitTrace.hpp"



#endif   // TestBitTrace_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
