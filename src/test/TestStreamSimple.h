/*****************************************************************************

        TestStreamSimple.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestStreamSimple_HEADER_INCLUDED)
#define TestStreamSimple_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/SplDataRetrievalInterface.h"



class TestStreamSimple
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class SplFromBuffer
	:	public mfx::dsp::SplDataRetrievalInterface
	{
	public:
		explicit       SplFromBuffer (const float buf_ptr [], int len);
	protected:
		virtual void	do_get_data (float *chn_data_ptr_arr [], int64_t pos, int len, bool backward_flag) noexcept final;
	private:
		const float *  _buf_ptr = nullptr;
		int            _buf_len = 0;
	};



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestStreamSimple ()                               = delete;
	               TestStreamSimple (const TestStreamSimple &other)  = delete;
	               TestStreamSimple (TestStreamSimple &&other)       = delete;
	TestStreamSimple &
	               operator = (const TestStreamSimple &other)        = delete;
	TestStreamSimple &
	               operator = (TestStreamSimple &&other)             = delete;
	bool           operator == (const TestStreamSimple &other) const = delete;
	bool           operator != (const TestStreamSimple &other) const = delete;

}; // class TestStreamSimple



//#include "test/TestStreamSimple.hpp"



#endif // TestStreamSimple_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
