/*****************************************************************************

        I2sClientCbInterface.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/I2sClientCbInterface.h"

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: proc_audio
Description:
	Reports incoming audio data and provides with new output data.
	Called as soon as the last incoming bit of the frame is received.
	Data are right-justified according to the sepcified bitdepth.
	If the transmitted data resolution is lower (for example 24-bit samples
	conveyed in 32-bit data), the content of the unused least significant bits
	is unspecified and could contain garbage.
Input/output parameters:
	- data_bidir: A 2-channel sample frame: input data just decoded by the
		I2S object on the function input, and new output data to be send by the
		I2S object on the function output.
Throws: unspecified
==============================================================================
*/

void	I2sClientCbInterface::proc_audio (SampleFrame &data_bidir)
{
	do_proc_audio (data_bidir);
}



/*
==============================================================================
Name: set_pin
Description:
	Notifies that the I2S object has made a change on a signal (typically
	on SDOUT) that should be propagated to the other end.
	If the object is clock master, this may be clock signals too.
Input parameters:
	- p: pin that changed. Only pins on ouput.
	- state_flag: new state of the signal (false = low, true = high).
Throws: unspecified
==============================================================================
*/

void	I2sClientCbInterface::set_pin (I2sPin p, bool state_flag)
{
	assert (p != I2sPin_SDIN);

	do_set_pin (p, state_flag);
}



/*
==============================================================================
Name: notify_clock_error
Description:
	In slave mode, indicates that the LRCK clock signal is inconsistent with
	the current state of the communication.
	Internal synchronisation is shifted towards the new LRCK.
	However frame data overlapping this unexpected edge may be lost or
	corrupted.
Throws: unspecified
==============================================================================
*/

void	I2sClientCbInterface::notify_clock_error ()
{
	do_notify_clock_error ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
