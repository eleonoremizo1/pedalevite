/*****************************************************************************

        TestRpiBcm.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestRpiBcm_HEADER_INCLUDED)
#define TestRpiBcm_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestRpiBcm
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     test_adrv_dpvab ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestRpiBcm ()                               = delete;
	               TestRpiBcm (const TestRpiBcm &other)        = delete;
	               TestRpiBcm (TestRpiBcm &&other)             = delete;
	TestRpiBcm &   operator = (const TestRpiBcm &other)        = delete;
	TestRpiBcm &   operator = (TestRpiBcm &&other)             = delete;
	bool           operator == (const TestRpiBcm &other) const = delete;
	bool           operator != (const TestRpiBcm &other) const = delete;

}; // class TestRpiBcm



//#include "test/TestRpiBcm.hpp"



#endif // TestRpiBcm_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
