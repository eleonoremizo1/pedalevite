/*****************************************************************************

        TestBbdLine.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/dsp/dly/BbdLine.h"
#include "mfx/dsp/mix/Generic.h"
#include "mfx/dsp/rspl/InterpolatorHermite43.h"
#include "mfx/FileOpWav.h"
#include "test/GenSig.h"
#include "test/TestBbdLine.h"

#include <algorithm>
#include <vector>

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestBbdLine::perform_test ()
{
	mfx::dsp::mix::Generic::setup ();

	printf ("Testing mfx::dsp::dly::BbdLine <>...\n");

	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr;

#if 1
	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);
	float          fdbk    = 0.75f;
#elif 0
	int            ret_val = GenSig::gen_spikes (sample_freq, chn_arr, 86.6667, 0.1);
	float          fdbk    = 0.75f;
#else
	int            ret_val = GenSig::gen_spikes (sample_freq, chn_arr, 20, 2.6973);
	float          fdbk    = 1;
#endif

	mfx::dsp::dly::BbdLine <> delay;
	if (ret_val == 0)
	{
		chn_arr.resize (1);

		mfx::dsp::rspl::InterpolatorHermite43  interp;

		int            bbd_size  = 4096;
		delay.init (bbd_size, interp, 0);
		delay.clear_buffers ();

		const int      buf_len   =
			bbd_size << -mfx::dsp::dly::BbdLine <>::_min_speed_l2;
		std::vector <float>  buf (buf_len);
		const float    spd_min   = 1.0f / (1 << -mfx::dsp::dly::BbdLine <>::_min_speed_l2);
		const float    spd_max   = 64;

		float          dly_time  = float (bbd_size);
		float          speed     = 1;
		double         osc_freq  = 0.1; // Hz

		const int      nbr_spl   = int (chn_arr [0].size ());
		int            block_pos = 0;
		while (block_pos < nbr_spl)
		{
			const float    c =
				float (cos (osc_freq * 2 * fstb::PI * block_pos / sample_freq));
			speed = spd_min + (1 - c) * 0.5f * (spd_max - spd_min);

			delay.set_speed (speed);

			const int      rem_len   = nbr_spl - block_pos;
			const int      max_len   =
				delay.estimate_max_one_shot_proc_w_feedback (dly_time);
			const int      max_len2  = 999999999; // 64;
			const int      block_len =
				std::min (std::min (std::min (rem_len, max_len), buf_len), max_len2);

			delay.read_block (&buf [0], block_len, dly_time, dly_time, 0);
			mfx::dsp::mix::Generic::scale_1_v (&buf [0], block_len, fdbk);
			if (block_pos < nbr_spl * 3 / 4)
			{
				mfx::dsp::mix::Generic::mix_1_1 (
					&buf [0],
					&chn_arr [0] [block_pos],
					block_len
				);
			}
			else
			{
				fdbk = 1.0f;
			}
			mfx::dsp::mix::Generic::copy_1_1 (
				&chn_arr [0] [block_pos],
				&buf [0],
				block_len
			);
			delay.push_block (&buf [0], block_len);

			block_pos += block_len;
		}

		ret_val = mfx::FileOpWav::save (
			"results/bbdline0.wav", chn_arr, sample_freq, 1
		);
	}

	if (ret_val == 0)
	{
		printf ("Done.\n");
	}
	else
	{
		printf ("*** An error occured ***\n");
	}

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
