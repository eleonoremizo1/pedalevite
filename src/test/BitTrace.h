/*****************************************************************************

        BitTrace.h
        Author: Laurent de Soras, 2024

Displays as ASCII-art a list of binary signals.
Signals are assumed sampled at an arbitrary period (fixed or not), time is
just measured as ticks.
All signals are stored in memory from the beginning (1 bit per tick).

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (BitTrace_HEADER_INCLUDED)
#define BitTrace_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <string>
#include <vector>



class BitTrace
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       BitTrace (int nbr_traces);

	void           reset ();
	void           set_name (int pin, const std::string &name);

	void           set_pin (int pin, bool state_flag) noexcept;
	void           tick (int duration = 1);

	std::string    print (int page_duration) const;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef std::vector <bool> Stream;
	typedef std::vector <Stream> StreamArray;
	typedef std::vector <std::string> NameArray;

	// Current state in a single row
	Stream         _state;

	NameArray      _name_arr;

	// One row = 1 stream for a single bit accross the time
	StreamArray    _stream_arr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const BitTrace &other) const = delete;
	bool           operator != (const BitTrace &other) const = delete;

}; // class BitTrace



//#include "test/BitTrace.hpp"



#endif   // BitTrace_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
