/*****************************************************************************

        TestPlugins.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/pi/dist1/DistoSimple.h"
#include "mfx/pi/dist1/DistoSimpleDesc.h"
#include "mfx/pi/dist1/Param.h"
#include "mfx/pi/dly2/Delay2.h"
#include "mfx/pi/dly2/Delay2Desc.h"
#include "mfx/pi/dly2/Param.h"
#include "mfx/pi/nzbl/NoiseBleach.h"
#include "mfx/pi/nzbl/NoiseBleachDesc.h"
#include "mfx/pi/nzbl/Param.h"
#include "mfx/pi/nzcl/NoiseChlorine.h"
#include "mfx/pi/nzcl/NoiseChlorineDesc.h"
#include "mfx/pi/nzcl/Param.h"
#include "mfx/pi/osdet/OnsetDetect.h"
#include "mfx/pi/osdet/OnsetDetectDesc.h"
#include "mfx/pi/phase1/Param.h"
#include "mfx/pi/phase1/Phaser.h"
#include "mfx/pi/phase1/PhaserDesc.h"
#include "mfx/pi/testgen/TestGen.h"
#include "mfx/pi/testgen/TestGenDesc.h"
#include "mfx/pi/testgen/Param.h"
#include "mfx/piapi/ProcInfo.h"
#include "mfx/FileOpWav.h"
#include "mfx/PluginPoolHostMini.h"
#include "test/GenSig.h"
#include "test/PiProc.h"
#include "test/TestPlugins.h"

#include <array>
#include <vector>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestPlugins::test_testgen ()
{
	printf ("Testing plug-in testgen...\n");

	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr;

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::testgen::TestGen  plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::testgen::TestGenDesc> ()
			)
		);
		pi_proc.setup (plugin, 1, 1, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		std::array <float *, 1> dst_arr = {{ pi_proc.use_buf_list_dst () [0] }};
		std::array <float *, 1> src_arr = {{ pi_proc.use_buf_list_src () [0] }};
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::testgen::Param_STATE, 1);
		pi_proc.set_param_nat (mfx::pi::testgen::Param_LVL  , 0.9375);
		pi_proc.set_param_nat (mfx::pi::testgen::Param_TYPE , mfx::pi::testgen::Type_SWEEP);
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			fstb::copy_no_overlap (
				src_arr [0],
				&chn_arr [0] [pos],
				block_len
			);

			plugin.process_block (proc_info);
			pi_proc.reset_param ();

			fstb::copy_no_overlap (
				&chn_arr [0] [pos],
				dst_arr [0],
				block_len
			);

			proc_info._nbr_evt = 0;
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/testgensweep0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_dist1 ()
{
	printf ("Testing plug-in dist1...\n");

	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr;

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::dist1::DistoSimple   dist (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::dist1::DistoSimpleDesc> ()
			)
		);
		pi_proc.setup (dist, 1, 1, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		std::array <float *, 1> dst_arr = {{ pi_proc.use_buf_list_dst () [0] }};
		std::array <float *, 1> src_arr = {{ pi_proc.use_buf_list_src () [0] }};
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::dist1::Param_GAIN, 100   );
		pi_proc.set_param_nat (mfx::pi::dist1::Param_BIAS,   0.30);
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			fstb::copy_no_overlap (
				src_arr [0],
				&chn_arr [0] [pos],
				block_len
			);

			dist.process_block (proc_info);
			pi_proc.reset_param ();

			fstb::copy_no_overlap (
				&chn_arr [0] [pos],
				dst_arr [0],
				block_len
			);

			proc_info._nbr_evt = 0;
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/t0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_phase1 ()
{
	printf ("Testing plug-in phase1...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_noise_w (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::phase1::Phaser plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::phase1::PhaserDesc> ()
			)
		);
		pi_proc.setup (plugin, nbr_chn, nbr_chn, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		float * const* dst_arr = pi_proc.use_buf_list_dst ();
		float * const* src_arr = pi_proc.use_buf_list_src ();
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::phase1::Param_SPEED    ,  0.1);
		pi_proc.set_param_nat (mfx::pi::phase1::Param_DEPTH    ,  0);
		pi_proc.set_param_nat (mfx::pi::phase1::Param_AP_DELAY ,  0.001);
		pi_proc.set_param_nat (mfx::pi::phase1::Param_AP_COEF  ,  -0.5);
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					src_arr [chn],
					&chn_arr [chn] [pos],
					block_len
				);
			}

			plugin.process_block (proc_info);

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					&chn_arr [chn] [pos],
					dst_arr [chn],
					block_len
				);
			}

			pi_proc.reset_param ();
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/phaser0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_nzcl ()
{
	printf ("Testing plug-in nzcl...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_noise_w (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::nzcl::NoiseChlorine plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::nzcl::NoiseChlorineDesc> ()
			)
		);
		pi_proc.setup (plugin, nbr_chn, nbr_chn, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		float * const* dst_arr = pi_proc.use_buf_list_dst ();
		float * const* src_arr = pi_proc.use_buf_list_src ();
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::nzcl::Param_LVL    ,  1);
		for (int b = 0; b < mfx::pi::nzcl::Cst::_nbr_notches; ++b)
		{
			const int      base = mfx::pi::nzcl::NoiseChlorineDesc::get_base_notch (b);
			pi_proc.set_param_nat (base + mfx::pi::nzcl::ParamNotch_FREQ, 160 << b);
			pi_proc.set_param_nat (base + mfx::pi::nzcl::ParamNotch_Q,       0.33f);
			pi_proc.set_param_nat (base + mfx::pi::nzcl::ParamNotch_LVL,    16);
		}
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					src_arr [chn],
					&chn_arr [chn] [pos],
					block_len
				);
			}

			plugin.process_block (proc_info);

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					&chn_arr [chn] [pos],
					dst_arr [chn],
					block_len
				);
			}

			pi_proc.reset_param ();
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/noisechlorine0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_nzbl ()
{
	printf ("Testing plug-in nzbl...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_noise_w (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::nzbl::NoiseBleach plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::nzbl::NoiseBleachDesc> ()
			)
		);
		pi_proc.setup (plugin, nbr_chn, nbr_chn, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		float * const* dst_arr = pi_proc.use_buf_list_dst ();
		float * const* src_arr = pi_proc.use_buf_list_src ();
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::nzbl::Param_LVL    ,  1);
		for (int b = 0; b < mfx::pi::nzbl::Cst::_nbr_bands; ++b)
		{
			const int      base = mfx::pi::nzbl::NoiseBleachDesc::get_base_band (b);
			pi_proc.set_param_nat (base + mfx::pi::nzbl::ParamBand_LVL, 1);
		}
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					src_arr [chn],
					&chn_arr [chn] [pos],
					block_len
				);
			}

			plugin.process_block (proc_info);

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					&chn_arr [chn] [pos],
					dst_arr [chn],
					block_len
				);
			}

			pi_proc.reset_param ();
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/noisebleach0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_dly2 ()
{
	printf ("Testing plugin dly2...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

//	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);
	int            ret_val = GenSig::gen_short_sine (sample_freq, chn_arr, 5, 0.9, 110, 0.5, 0.01);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::dly2::Delay2 plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::dly2::Delay2Desc> ()
			)
		);
		pi_proc.setup (plugin, nbr_chn, nbr_chn, sample_freq, max_block_size, latency);
		size_t         pos = 0;
		float * const* dst_arr = pi_proc.use_buf_list_dst ();
		float * const* src_arr = pi_proc.use_buf_list_src ();
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		pi_proc.set_param_nat (mfx::pi::dly2::Delay2Desc::get_line_base (1) + mfx::pi::dly2::ParamLine_VOL, 0);
		pi_proc.set_param_nat (mfx::pi::dly2::Delay2Desc::get_line_base (0) + mfx::pi::dly2::ParamLine_DLY_BASE, 1.0);
		pi_proc.set_param_nat (mfx::pi::dly2::Delay2Desc::get_line_base (0) + mfx::pi::dly2::ParamLine_PITCH, 4.0/12);
		pi_proc.set_param_nat (mfx::pi::dly2::Delay2Desc::get_line_base (0) + mfx::pi::dly2::ParamLine_FDBK, 0.95);
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					src_arr [chn],
					&chn_arr [chn] [pos],
					block_len
				);
			}

			if (pos >= (len >> 1) && pos < (len >> 1) + block_len)
			{
				pi_proc.set_param_nat (mfx::pi::dly2::Delay2Desc::get_line_base (0) + mfx::pi::dly2::ParamLine_DLY_BASE, 1.1);
			}

			plugin.process_block (proc_info);

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					&chn_arr [chn] [pos],
					dst_arr [chn],
					block_len
				);
			}

			pi_proc.reset_param ();
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/delay20.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestPlugins::test_osdet ()
{
	printf ("Testing plug-in osdet...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::PluginPoolHostMini host;
		mfx::pi::osdet::OnsetDetect plugin (host);
		const int      max_block_size = 64;
		int            latency = 0;
		PiProc         pi_proc;
		pi_proc.set_desc (
			std::static_pointer_cast <mfx::piapi::PluginDescInterface> (
				std::make_shared <mfx::pi::osdet::OnsetDetectDesc> ()
			)
		);
		pi_proc.setup (plugin, nbr_chn, 0, sample_freq, max_block_size, latency);
		size_t         pos = 0;
//		float * const* dst_arr = pi_proc.use_buf_list_dst ();
		float * const* sig_arr = pi_proc.use_buf_list_sig ();
		float * const* src_arr = pi_proc.use_buf_list_src ();
		mfx::piapi::ProcInfo &   proc_info = pi_proc.use_proc_info ();
		pi_proc.reset_param ();
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));

			proc_info._nbr_spl = block_len;

			for (int chn = 0; chn < nbr_chn; ++chn)
			{
				fstb::copy_no_overlap (
					src_arr [chn],
					&chn_arr [chn] [pos],
					block_len
				);
			}
			plugin.process_block (proc_info);
			for (int k = 0; k < block_len; ++k)
			{
				chn_arr [0] [pos + k] = sig_arr [0] [0] - sig_arr [1] [0];
			}

			pi_proc.reset_param ();
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/osdet0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
