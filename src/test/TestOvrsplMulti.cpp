/*****************************************************************************

        TestOvrsplMulti.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/pi/cdsp/OvrsplMulti.h"
#include "test/TestOvrsplMulti.h"

#include <algorithm>

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestOvrsplMulti::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing mfx::pi::cdsp::OvrsplMulti...\n");

	// Prepares the source signal
	constexpr int  len = 200'000;
	std::vector <float> src (len);
	constexpr auto w   =
		2 * fstb::PI * mfx::pi::cdsp::OvrsplMulti::_group_delay_ref_freq;
	for (int k = 0; k < len; ++k)
	{
		src [k] = float (sin (k * w));
	}

	mfx::pi::cdsp::OvrsplMulti ovrspl;

	std::vector <float> dst (len);

	constexpr int  xcor_check_len = 64;
	std::vector <double> xcor (xcor_check_len);

	for (int cap_l2 = 0
	;	cap_l2 <= mfx::pi::cdsp::OvrsplMulti::_max_ratio_l2
	;	++ cap_l2)
	{
		ovrspl.cap_ratio_l2 (cap_l2);
		const auto     lat_ref = ovrspl.get_latency ();
		printf ("Capping to %dx, latency = %d samples\n", 1 << cap_l2, lat_ref);

		for (int ratio_l2 = 0; ratio_l2 <= cap_l2; ++ratio_l2)
		{
			ovrspl.set_ratio_l2 (ratio_l2);
			ovrspl.clear_buffers ();

			// Processing
			int            pos         = 0;
			int            blk_len     = 0; // 0 = sample/sample processing
			constexpr int  blk_len_max = 300;
			constexpr int  blm_ovr     =
				blk_len_max * mfx::pi::cdsp::OvrsplMulti::_max_ratio_l2;
			std::array <float, blm_ovr> buf;
			while (pos < len)
			{
				int            work_len = (blk_len == 0) ? blk_len_max : blk_len;
				work_len = std::min (work_len, len - pos);

				if (blk_len == 0)
				{
					for (int k = 0; k < work_len; ++k)
					{
						ovrspl.upsample_sample (buf.data (), src [pos + k]);
						dst [pos + k] = ovrspl.downsample_sample (buf.data ());
					}
				}
				else
				{
					ovrspl.upsample_block (buf.data (), &src [pos], work_len);
					ovrspl.downsample_block (&dst [pos], buf.data (), work_len);
				}

				pos += work_len;
			}

			// Checks results
			// Computes the cross-correlation between source and result
			constexpr int  win_len = 512;
			double         err_max = 0;
			for (int ofs = len / 8; ofs + xcor_check_len - 1 + win_len <= len; ofs += len / 64)
			{
				compute_xcorrelation (
					xcor, &src [ofs], &dst [ofs], xcor_check_len, win_len
				);

				// Finds the maximum position -> latency
				int            lat_tst = 0;
				for (int k = 1; k < xcor_check_len; ++k)
				{
					if (xcor [k] > xcor [lat_tst])
					{
						lat_tst = k;
					}
				}
				const auto     err = 1 - xcor [lat_tst];

				if (lat_tst != lat_ref)
				{
					printf ("*** Error: wrong latency %d samples\n", lat_tst);
					return -1;
				}
				if (err > 1e-6)
				{
					printf ("*** Error: distorted signal, error = %g\n", err);
					return -1;
				}

				err_max = std::copysign (
					std::max (fabs (err_max), fabs (err)), err
				);
			}

			printf (
				"  Ratio %dx: delay = %d, max error = %g\n",
				1 << ratio_l2, lat_ref, err_max
			);
		}
	}

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Warning: no bound check at all
// len = number of offsets to test
// Positive offsets are applied to s2_ptr
// Uses a simple cosine window
void	TestOvrsplMulti::compute_xcorrelation (std::vector <double> &dst, const float s1_ptr [], const float s2_ptr [], int len, int win_len)
{
	assert (s1_ptr != nullptr);
	assert (s2_ptr != nullptr);
	assert (len > 0);
	assert (win_len >= 2);

	dst.resize (len);

	// Builds the window
	std::vector <double> win (win_len);
	const auto     step = 2 * fstb::PI / (win_len - 1);
	for (int pos = 0; pos < win_len; ++pos)
	{
		win [pos] = (1 - cos ((pos + 0.5) * step)) * 0.5;
	}

	// First, auto-correlation at 0 to get the normalisation factor
	double         norm = 0;
	for (int pos = 0; pos < win_len; ++pos)
	{
		const auto     val = fstb::sq (double (s1_ptr [pos])) * win [pos];
		norm += val;
	}
	if (norm == 0)
	{
		norm = 1;
	}

	// Computes the cross-correlation
	for (int ofs = 0; ofs < len; ++ofs)
	{
		double         sum = 0;
		for (int pos = 0; pos < win_len; ++pos)
		{
			const auto     val =
				double (s1_ptr [pos]) * double (s2_ptr [pos + ofs]) * win [pos];
			sum += val;
		}
		dst [ofs] = sum / norm;
	}
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
