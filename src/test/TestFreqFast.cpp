/*****************************************************************************

        TestFreqFast.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/ana/FreqFast.h"
#include "mfx/FileOpWav.h"
#include "test/GenSig.h"
#include "test/TestFreqFast.h"

#include <algorithm>
#include <vector>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestFreqFast::perform_test ()
{
	printf ("Testing mfx::dsp::ana::FreqFast...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		const size_t   len = chn_arr [0].size ();
		mfx::dsp::ana::FreqFast analyser;
		const int      max_block_size = 64;

		analyser.set_sample_freq (sample_freq);

		size_t         pos = 0;
		double         phase = 0;
		do
		{
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));
			const float    freq = analyser.process_block (&chn_arr [0] [pos], block_len);
			for (int k = 0; k < block_len; ++k)
			{
				chn_arr [0] [pos + k] = float (sin (phase * 2 * fstb::PI / sample_freq));
				phase += freq;
			}
			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/freqfast0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
