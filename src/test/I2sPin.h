/*****************************************************************************

        I2sPin.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (I2sPin_HEADER_INCLUDED)
#define I2sPin_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



enum I2sPin
{

	I2sPin_SCLK = 0, // Bit clock
	I2sPin_LRCK,     // Word clock
	I2sPin_SDIN,     // Data in
	I2sPin_SDOUT,    // Data out

	I2sPin_NBR_ELT

}; // enum I2sPin



//#include "test/I2sPin.hpp"



#endif   // I2sPin_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
