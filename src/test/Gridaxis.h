/*****************************************************************************

        Gridaxis.h
        Author: Laurent de Soras, 2003

Handles a graph with its own coordinate system.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (Gridaxis_HEADER_INCLUDED)
#define	Gridaxis_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "Axis.h"
#include "GraphPrim.h"

#include <array>
#include <string>
#include <vector>



class EPSPlot;

class Gridaxis
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	enum Direction
	{
		Direction_H = 0,
		Direction_V,

		Direction_NBR_ELT
	};

	enum DotStyle
	{
		DotStyle_PLUS = 0,
		DotStyle_SQUARE,

		DotStyle_NBR_ELT
	};

	void           set_size (float w, float h);
	void           set_plot (EPSPlot &plot, float x, float y);
	void           set_grid (bool grid_flag);
	void           set_sci_mode (bool sci_flag);
	void           set_title (std::string title);

	EPSPlot &      use_plot ();
	const EPSPlot& use_plot () const;
	Axis &         use_axis (Direction dir);
	const Axis &   use_axis (Direction dir) const;
	GraphPrim &    use_prim ();
	const GraphPrim &
	               use_prim () const;

	void           render_background ();
	void           draw_line (double x_1, double y_1, double x_2, double y_2) const;
	void           draw_point (double x, double y) const;
	void           render_curve (const double x_arr [], const double y_arr [], int nbr_points, bool fill_flag = false) const;
	void           render_point_set (const double x_arr [], const double y_arr [], int nbr_points, double radius = 2) const;
	void           put_annotation (double x, double y, const char txt_0 []) const;
	void           put_annotation_pos (double x, double y, const char txt_0 []) const;

	static void    conv_nbr_2_str (char txt_0 [], double val);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef float PsCoord;
	typedef std::vector <PsCoord> PsCoordArr;

	class Scale
	{
	public:
		               Scale () = default;
		explicit       Scale (const Axis &axis);

		inline double  get_scaler () const noexcept;
		std::string    append_to_legend (std::string legend) const;

	private:

		// Power of 10 of the multiplier. For example "N x 1e-3" => _p10 = -3
		int            _p10 = 0;

		// Equivalent scaling factor, pow (10, _p10)
		double         _scl = 1;
	};

	void           render_grid () const;
	void           render_axis_v ();
	void           render_axis_h ();
	void           convert_coordinates (double &x, double &y) const;
	void           convert_coordinates (PsCoordArr &x_conv, PsCoordArr &y_conv, const double x_arr [], const double y_arr [], int nbr_points) const;
	void           conv_nbr_2_str (char txt_0 [], double val, Direction dir);

	std::array <Axis, Direction_NBR_ELT>
	               _axis;
	GraphPrim      _prim;
	EPSPlot *      _plot_ptr  { nullptr };
	DotStyle       _dot_style { DotStyle_SQUARE };
	PsCoord        _x         { 0 };
	PsCoord        _y         { 0 };
	PsCoord        _w         { -1 };
	PsCoord        _h         { -1 };
	bool           _grid_flag { true };
	std::string    _title;

	bool           _sci_flag  { false }; // Indicates we display values as a*10^b
	std::array <Scale, Direction_NBR_ELT>
	               _scale_arr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

};	// class Gridaxis





//#include	"Gridaxis.hpp"



#endif	// Gridaxis_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
