/*****************************************************************************

        I2sEnd.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#if defined (PV_RPI_VER_MAJOR)
# include "mfx/hw/HigepioLinux.h"
#endif // PV_RPI_VER_MAJOR
#include "test/I2sClient.h"
#include "test/I2sEnd.h"

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



I2sEnd::I2sEnd (bool master_flag, int bitdepth, int transfer_lag, bool left_high_flag, bool rec_flag)
:	I2sClient (master_flag, bitdepth, transfer_lag, left_high_flag)
,	_master_flag (master_flag)
,	_rec_flag (rec_flag)
,	_name_0 (master_flag ? "remote" : "local ")
,	_bt (rec_flag ? I2sPin_NBR_ELT : 0)
{
	set_callback (*this);
	if (master_flag)
	{
		set_offset (0);
	}

	if (rec_flag)
	{
		_bt.set_name (I2sPin_SCLK , "SCLK" );
		_bt.set_name (I2sPin_LRCK , "LRCK" );
		_bt.set_name (I2sPin_SDIN , "SDIN" );
		_bt.set_name (I2sPin_SDOUT, "SDOUT");
	}
}



I2sEnd::~I2sEnd ()
{
#if defined (PV_RPI_VER_MAJOR)
	if (_io_ptr != nullptr)
	{
		// Put all used pins in read mode, it's probably safer if we do other
		// things with the GPIO connector later in the session.
		const auto &   pins = _pin_arr [_master_flag];
		for (auto p : pins)
		{
			_io_ptr->set_pin_dir (p, false);
		}
	}
#endif // PV_RPI_VER_MAJOR
}



void	I2sEnd::set_other (I2sEnd &other)
{
	_other_ptr = &other;
}



#if defined (PV_RPI_VER_MAJOR)

void	I2sEnd::set_io (mfx::hw::HigepioLinux &io)
{
	_io_ptr = &io;
	const auto &   pins = _pin_arr [_master_flag];
	io.set_pin_dir (pins [I2sPin_SCLK ], _master_flag);
	io.set_pin_dir (pins [I2sPin_LRCK ], _master_flag);
	io.set_pin_dir (pins [I2sPin_SDIN ], false);
	io.set_pin_dir (pins [I2sPin_SDOUT], true);
}

#endif // PV_RPI_VER_MAJOR



// Hides inherited definition
void	I2sEnd::set_pin (I2sPin p, bool state_flag) noexcept
{
#if defined (PV_RPI_VER_MAJOR)
	// This function should be called only when GPIO is not used.
	assert (_other_ptr != nullptr);
	if (_other_ptr != nullptr) // For safety
#endif // PV_RPI_VER_MAJOR
	{
		if (_rec_flag)
		{
			_bt.set_pin (int (p), state_flag);
		}
		I2sClient::set_pin (p, state_flag);
	}
}



int	I2sEnd::run_step (int timestamp)
{
	_timestamp = timestamp;

#if defined (PV_RPI_VER_MAJOR)
	// Use GPIO
	if (_other_ptr == nullptr)
	{
		// Reads GPIO and transmits changes
		auto           read_and_transmit = [this] (I2sPin p)
		{
			const bool     flag = read_gpio (p);
			I2sClient::set_pin (p, flag);
			if (_rec_flag)
			{
				_bt.set_pin (int (p), flag);
			}
		};

		read_and_transmit (I2sPin_SDIN);
		if (! _master_flag)
		{
			read_and_transmit (I2sPin_SCLK);
			read_and_transmit (I2sPin_LRCK);
		}
	}
#endif // PV_RPI_VER_MAJOR

	step ();
	if (_rec_flag)
	{
		_bt.tick ();
	}

	return _count;
}



void	I2sEnd::dump_traces () const
{
	assert (_rec_flag);

	const auto     str = _bt.print (128);
	printf ("%s", str.c_str ());
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	I2sEnd::do_proc_audio (SampleFrame &data_bidir)
{
	printf ("t = %4d, %s: received ", _timestamp, _name_0);
	for (const auto &d : data_bidir)
	{
		printf ("%4d, ", int (d));
	}
	printf ("%sing ", _master_flag ? "echo" : "send");
	for (auto &d : data_bidir)
	{
		if (_master_flag)
		{
			d += 1000;
		}
		else
		{
			d = _count + 234;
		}
		printf ("%4d, ", int (d));
		++ _count;
	}
	printf ("\n");
}



void	I2sEnd::do_set_pin (I2sPin p, bool state_flag)
{
	if (_rec_flag)
	{
		_bt.set_pin (int (p), state_flag);
	}

#if defined (PV_RPI_VER_MAJOR)
	// Use GPIO
	if (_other_ptr == nullptr)
	{
		write_gpio (p, state_flag);
	}

	// Direct software connection
	else
#endif // PV_RPI_VER_MAJOR
	{
		if (p == I2sPin_SDOUT)
		{
			p = I2sPin_SDIN;
		}
		else if (p == I2sPin_SDIN)
		{
			p = I2sPin_SDOUT;
		}

		_other_ptr->set_pin (p, state_flag);
	}
}



void	I2sEnd::do_notify_clock_error ()
{
	printf ("%s: inconsistent LRCK\n", _name_0);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



#if defined (PV_RPI_VER_MAJOR)

void	I2sEnd::write_gpio (I2sPin p, bool state_flag)
{
	assert (_io_ptr != nullptr);
	assert (p >= 0);
	assert (p < I2sPin_NBR_ELT);

	const auto &   pins = _pin_arr [_master_flag];
	const auto     gpio = pins [p];
	_io_ptr->write_pin (gpio, (state_flag) ? 1 : 0);
}



bool	I2sEnd::read_gpio (I2sPin p) const
{
	assert (_io_ptr != nullptr);
	assert (p >= 0);
	assert (p < I2sPin_NBR_ELT);

	const auto &   pins = _pin_arr [_master_flag];
	const auto     gpio = pins [p];
	const auto     val  = _io_ptr->read_pin (gpio);

	return (val != 0);
}

#endif // PV_RPI_VER_MAJOR



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
