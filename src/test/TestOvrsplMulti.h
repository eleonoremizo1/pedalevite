
/*****************************************************************************

        TestOvrsplMulti.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestOvrsplMulti_HEADER_INCLUDED)
#define TestOvrsplMulti_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <vector>



class TestOvrsplMulti
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static void    compute_xcorrelation (std::vector <double> &dst, const float s1_ptr [], const float s2_ptr [], int len, int win_len);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ~TestOvrsplMulti ()                              = delete;
	               TestOvrsplMulti ()                               = delete;
	               TestOvrsplMulti (const TestOvrsplMulti &other)   = delete;
	               TestOvrsplMulti (TestOvrsplMulti &&other)        = delete;
	TestOvrsplMulti &
	               operator = (const TestOvrsplMulti &other)        = delete;
	TestOvrsplMulti &
	               operator = (TestOvrsplMulti &&other)             = delete;
	bool           operator == (const TestOvrsplMulti &other) const = delete;
	bool           operator != (const TestOvrsplMulti &other) const = delete;

}; // class TestOvrsplMulti



//#include "test/TestOvrsplMulti.hpp"



#endif   // TestOvrsplMulti_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
