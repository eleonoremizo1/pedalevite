/*****************************************************************************

        BitTrace.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "test/BitTrace.h"

#include <algorithm>
#include <array>

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Description:
	All traces start at a low state.
Input parameters:
	- nbr_traces: number of signals to follow, >= 0
Throws: std::vector and std::string related exceptions
==============================================================================
*/

BitTrace::BitTrace (int nbr_traces)
:	_state (nbr_traces, false)
,	_name_arr (nbr_traces)
,	_stream_arr (nbr_traces)
{
	assert (nbr_traces >= 0);
}



/*
==============================================================================
Name: reset
Description:
	Clears the signals, their name, assumes all states are low, acting as a
	freshly constructed object.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	BitTrace::reset ()
{
	std::fill (_state.begin (), _state.end (), false);
	const auto     nbr_traces = int (_state.size ());
	NameArray (nbr_traces).swap (_name_arr);
	StreamArray (nbr_traces).swap (_stream_arr);
}



/*
==============================================================================
Name: set_name
Description:
	Gives a name to a signal, that will be displayed in front of its trace
	in print().
Input parameters:
	- pin: signal index, in [0 ; N-1]
	- name: Name to be displayed
Throws: std::string related exceptions
==============================================================================
*/

void	BitTrace::set_name (int pin, const std::string &name)
{
	assert (pin >= 0);
	assert (pin < int (_state.size ()));

	_name_arr [pin] = name;
}



/*
==============================================================================
Name: set_pin
Description:
	Sets the new value of a signal.
Input parameters:
	- pin: signal index, in [0 ; N-1]
	- state_flag: new value of the signal (false = low, true = high)
Throws: Nothing
==============================================================================
*/

void	BitTrace::set_pin (int pin, bool state_flag) noexcept
{
	assert (pin >= 0);
	assert (pin < int (_state.size ()));

	_state [pin] = state_flag;
}



/*
==============================================================================
Name: tick
Description:
	Let the time pass for a given duration.
	During this time, all signals are assumed constants.
Input parameters:
	- duration: number of ticks to wait, > 0
Throws:std::vector related exceptions
==============================================================================
*/

void	BitTrace::tick (int duration)
{
	assert (duration > 0);

	const auto     nbr_traces = int (_state.size ());
	for (int trace_idx = 0; trace_idx < nbr_traces; ++trace_idx)
	{
		const auto     bit    = _state [trace_idx];
		auto &         stream = _stream_arr [trace_idx];
		stream.insert (stream.end (), duration, bit);

		assert (stream.size () == _stream_arr.front ().size ());
	}
}



/*
==============================================================================
Name: print
Description:
	Generates a string containing an ASCII art showing the signal history
	from the beginning.
	Each signal occupies 2 character rows.
	If the signal duration is too long, display is split into several pages of
	fixed width.
	There is a header at the left of each page containing the signal names and
	the time index at the beginning of the page.
	Pages end with an empty line.
Input parameters:
	- page_duration: Maximum number of ticks to display on a page, > 0.
Returns:
	The string containing the ASCII-art signals.
Throws: std::string related exceptions
==============================================================================
*/

std::string	BitTrace::print (int page_duration) const
{
	assert (page_duration > 0);

	constexpr int  nbr_rows = 2;

	// Transition order (previous, current): 0-0, 1-0, 0-1, 1-1
	typedef std::array <const char *, 4> CharMapRow;
	const std::array <CharMapRow, nbr_rows> char_map =
	{
#if fstb_SYS == fstb_SYS_WIN
		// CP 850
		CharMapRow { " ", "\xBF", "\xDA", "\xC4" },
		CharMapRow { "\xC4", "\xC0", "\xD9", " " }
#elif fstb_SYS == fstb_SYS_LINUX
		// UTF-8 U+2500, U+250C, U+2510, U+2514, U+2518
		CharMapRow { " ", "\xE2\x94\x90", "\xE2\x94\x8C", "\xE2\x94\x80" },
		CharMapRow { "\xE2\x94\x80", "\xE2\x94\x94", "\xE2\x94\x98", " " }
#else
		// Ordinary ASCII
		CharMapRow { " ", " ", "_", "_" },
		CharMapRow { "_", "_", " ", " " }
#endif
	};

	std::string    str;
	const auto     nbr_traces = int (_state.size ());
	if (nbr_traces <= 0)
	{
		return str;
	}
	const auto     total_len  = int (_stream_arr.front ().size ());

	// Finds the width of the margin containing the names
	int            margin_len = 0;
	for (const auto &name : _name_arr)
	{
		margin_len = std::max (margin_len, int (name.length ()));
	}
	if (margin_len > 0)
	{
		++ margin_len; // +1 for the spacer
	}

	int            page_pos = 0;
	while (page_pos < total_len)
	{
		const auto     page_len =
			std::min (total_len - page_pos, page_duration);
		for (int trace_idx = 0; trace_idx < nbr_traces; ++trace_idx)
		{
			const auto &   stream = _stream_arr [trace_idx];
			for (int row = 0; row < nbr_rows; ++row)
			{
				// Prints the margin with the name
				std::string    margin;
				if (row == 0)
				{
					// Prints the current time on the first trace, if the display
					// spans over several pages.
					if (trace_idx == 0 && page_len < total_len)
					{
						char           txt_0 [255+1];
						fstb::snprintf4all (txt_0, sizeof (txt_0), "%d ", page_pos);
						margin = txt_0;
						// At this point, we may have to update the margin length.
						// Fortunately, we're on the first row of the page.
						margin_len = std::max (margin_len, int (margin.length ()));
					}
				}

				// Second row begins with the signal name.
				else
				{
					assert (row == nbr_rows - 1);
					margin = _name_arr [trace_idx];
				}

				str += margin;
				str.append (margin_len - int (margin.length ()), ' ');

				// Prints the signal
				const auto &   char_map_row = char_map [row];
				const auto     prv_flag	    = stream [std::max (page_pos - 1, 0)];
				auto           trans        = (prv_flag) ? 2 : 0;
				for (int pos = 0; pos < page_len; ++pos)
				{
					const auto     cur_flag = stream [page_pos + pos];
					trans >>= 1;
					trans  += (cur_flag) ? 2 : 0;
					str    += char_map_row [trans];
				}

				str += "\n";
			}
		}

		page_pos += page_len;
		str      += "\n";
	}

	return str;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
