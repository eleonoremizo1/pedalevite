/*****************************************************************************

        TestStreamSimple.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/dsp/rspl/InterpolatorHermite43.h"
#include "mfx/dsp/rspl/StreamSimple.h"
#include "mfx/FileOpWav.h"
#include "test/TestStreamSimple.h"

#include <algorithm>
#include <vector>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestStreamSimple::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing mfx::dsp::rspl::StreamSimple...\n");

	constexpr double  sample_freq = 44100; // Hz
	constexpr double  dur_s       = 20;    // s
	constexpr double  f0          = 1000;  // Hz
	constexpr int     len_s       = int (sample_freq * dur_s);

	constexpr double  dur_d       = 10;    // s
	constexpr int     len_d       = int (sample_freq * dur_d);

	std::vector <float> src (len_s);
	for (int k = 0; k < len_s; ++k)
	{
		src [k] = float (sin (k * (2 * fstb::PI * f0 / sample_freq)));
	}

	mfx::dsp::rspl::StreamSimple strm;
	mfx::dsp::rspl::InterpolatorHermite43 interp;
	strm.set_interpolator (interp);
	strm.set_sample_freq (sample_freq, 0);
	SplFromBuffer     provider (src.data (), len_s);
	strm.set_data (provider, sample_freq);

	std::vector <float>  dst (len_d);
	int               pos_d    = 0;
	constexpr float   oct_beg  = -2;
	constexpr float   oct_end  = +2;
	auto              step_cur = exp2f (oct_beg);
	while (pos_d < len_d)
	{
		int            work_len = 1000 + ((pos_d * 999) % 1243);
		work_len = std::min (work_len, len_d - pos_d);

		const auto     pitch_nxt = fstb::lerp (
			oct_beg, oct_end,
			float (pos_d + work_len) / float (len_d)
		);
		const auto     step_nxt  = exp2f (pitch_nxt);
		strm.set_rate (step_cur, (step_nxt - step_cur) / float (work_len));

		strm.process_block (&dst [pos_d], work_len);

		pos_d   += work_len;
		step_cur = step_nxt;
	}

	mfx::FileOpWav::save ("results/streamsimple1.wav", dst, sample_freq, 0.5f);

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



TestStreamSimple::SplFromBuffer::SplFromBuffer (const float buf_ptr [], int len)
:	_buf_ptr (buf_ptr)
,	_buf_len (len)
{
	assert (buf_ptr != nullptr);
	assert (len >= 0);
}



void	TestStreamSimple::SplFromBuffer::do_get_data (float *chn_data_ptr_arr [], int64_t pos, int len, bool backward_flag) noexcept
{
	fstb::unused (backward_flag);
	assert (! backward_flag); // Not supported

	auto           write_ptr = chn_data_ptr_arr [0];

	if (pos < 0)
	{
		const auto     work_len = int (std::min <int64_t> (-pos, len));
		std::fill (write_ptr, write_ptr + work_len, 0.f);
		pos += work_len;
		len -= work_len;
		write_ptr += work_len;
	}

	if (len > 0 && pos < _buf_len)
	{
		const auto     work_len = int (std::min <int64_t> (_buf_len - pos, len));
		const auto     beg_ptr  = _buf_ptr + pos;
		std::copy (beg_ptr, beg_ptr + work_len, write_ptr);
		pos += work_len;
		len -= work_len;
		write_ptr += work_len;
	}

	if (len > 0 && pos >= _buf_len)
	{
		std::fill (write_ptr, write_ptr + len, 0.f);
	}
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
