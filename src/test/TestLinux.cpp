/*****************************************************************************

        TestLinux.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/hw/ThreadLinux.h"
#include "mfx/FileIOInterface.h"
#include "mfx/Cst.h"
#include "test/Histogram.h"
#include "test/TestLinux.h"

#include <sys/time.h>

#include <algorithm>
#include <array>
#include <limits>
#include <string>
#include <vector>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestLinux::test_file_write_fs_ro ()
{
	printf ("Testing mfx::FileIOInterface::write_txt_file_direct()...\n");

	std::string    pathname = "results/test_file_write_fs_ro.txt";
	std::string    content  = "This is a test.\n";

	std::string    pathname_tmp ("/tmp");
	const size_t   last_delim = pathname.rfind ('/');
	if (last_delim == std::string::npos)
	{
		pathname_tmp += "/";
		pathname_tmp += pathname;
	}
	else
	{
		pathname_tmp += pathname.substr (last_delim);
	}

	int            ret_val = mfx::FileIOInterface::write_txt_file_direct (
		pathname_tmp, content
	);
	if (ret_val == 0)
	{
		std::string    cmd = "sudo ";
		cmd += mfx::Cst::_rw_cmd_script_pathname;
		cmd += " ";
		cmd += "mv \'";
		cmd += pathname_tmp;
		cmd += "\' \'";
		cmd += pathname;
		cmd += "\'";

		const int      ret_val_sys = system (cmd.c_str ());
		if (ret_val_sys != 0)
		{
			ret_val = -1;
		}
	}

	printf ("Done.\n");

	return ret_val;
}



int	TestLinux::test_gettimeofday ()
{
	printf ("Testing gettimeofday()...\n");
	fflush (stdout);

	constexpr int  nbr_tests = 10'000'000 + 1;

	// Real-time thread
	mfx::hw::ThreadLinux::set_priority (0, nullptr);

	// Measurements
	std::vector <int64_t> time_arr (nbr_tests);
	for (int dry_run = 1; dry_run >= 0; --dry_run)
	{
		const int nt = (dry_run == 0) ? nbr_tests : 1'000'000;
		for (int tst_idx = 0; tst_idx < nt; ++tst_idx)
		{
			time_arr [tst_idx] = get_tod_us ();
		}
	}

	// Turns timestamps into deltas
	const auto     duration      = time_arr.back () - time_arr.front ();
	const int      nbr_intervals = int (time_arr.size ());
	for (int itv_idx = 0; itv_idx < nbr_intervals; ++itv_idx)
	{
		time_arr [itv_idx] = time_arr [itv_idx + 1] - time_arr [itv_idx];
	}
	time_arr.pop_back ();

	// Basic statistics
	int64_t        sum2 = 0;
	for (auto t : time_arr)
	{
		sum2 += fstb::sq (t);
	}
	const double   avg   = double (duration) / double (nbr_intervals);
	const double   avg2  = double (sum2    ) / double (nbr_intervals);
	const double   sigma = sqrt (avg2 - fstb::sq (avg));
	printf ("Average: %f us, std dev: %f us", avg, sigma);
	Histogram histo;
	histo.reset_and_fill (
		time_arr.begin (), time_arr.end (), 79, "Delta-t, vert. log scale"
	);
	const auto     s = histo.print (10, true);
	printf ("%s\n", s.c_str ());

	return 0;
}



int	TestLinux::test_nanosleep_latency ()
{
	printf ("Testing nanosleep()...\n");
	fflush (stdout);

	// Real-time thread
	mfx::hw::ThreadLinux::set_priority (0, nullptr);

	// Durations to be tested, in microseconds
	constexpr std::array <int, 8> target_arr =
	{
		1, 10, 25, 50, 75, 100, 150, 200
	};

	for (auto target_us : target_arr)
	{
		const int      nbr_tests = fstb::ceil_int (100'000.0 / sqrt (target_us));
		std::vector <int64_t> time_arr (nbr_tests);

		// Measurements
		const auto     sleep_ns = long (target_us) * 1000;
		::timespec     slp;
		slp.tv_sec  = 0;
		slp.tv_nsec = sleep_ns;

		for (int dry_run = 1; dry_run >= 0; --dry_run)
		{
			const int nt = (dry_run == 0) ? nbr_tests : 1000;
			auto           t_beg = get_tod_us ();
			for (int tst_idx = 0; tst_idx < nt; ++tst_idx)
			{
				nanosleep (&slp, nullptr);
				const auto     t_end = get_tod_us ();
				const auto     dif   = t_end - t_beg;
				time_arr [tst_idx]   = dif;
				t_beg = t_end;
			}
		}

		// Statistics
		int64_t        sum   = 0;
		int64_t        sum2  = 0;
		int            t_min = std::numeric_limits <int>::max ();
		int            t_max = std::numeric_limits <int>::lowest ();
		for (int tst_idx = 0; tst_idx < nbr_tests; ++tst_idx)
		{
			const auto     t = time_arr [tst_idx];
			sum   += t;
			sum2  += fstb::sq (t);
			t_min  = std::min (t_min, int (t));
			t_max  = std::max (t_max, int (t));
		}
		const double   avg   = double (sum ) / double (nbr_tests);
		const double   avg2  = double (sum2) / double (nbr_tests);
		const double   sigma = sqrt (avg2 - fstb::sq (avg));
		const double   ovrhd = avg - double (target_us);
		printf (
			"Target:%4d, avg:%6.1f, min:%5d, max:%5d, stddev:%6.1f, ovrhd:%6.1f\n",
			target_us, avg, t_min, t_max, sigma, ovrhd
		);

		Histogram histo;
		histo.reset_and_fill (
			time_arr.begin (), time_arr.end (), 79, "Sleep time, vert. log scale"
		);
		const auto     s = histo.print (10, true);
		printf ("%s\n", s.c_str ());
	}

	return 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int64_t	TestLinux::get_tod_us () noexcept
{
	timeval        now {};
	gettimeofday (&now, nullptr);
	const auto     t =
		  int64_t (now.tv_sec) * int64_t (1'000'000)
		+ int64_t (now.tv_usec);

	return t;
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
