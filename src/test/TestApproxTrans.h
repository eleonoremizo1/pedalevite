/*****************************************************************************

        TestApproxTrans.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestApproxTrans_HEADER_INCLUDED)
#define TestApproxTrans_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



class TestApproxTrans
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestApproxTrans ()                               = delete;
	               TestApproxTrans (const TestApproxTrans &other)   = delete;
	               TestApproxTrans (TestApproxTrans &&other)        = delete;
	TestApproxTrans &
	               operator = (const TestApproxTrans &other)        = delete;
	TestApproxTrans &
	               operator = (TestApproxTrans &&other)             = delete;
	bool           operator == (const TestApproxTrans &other) const = delete;
	bool           operator != (const TestApproxTrans &other) const = delete;

}; // class TestApproxTrans



//#include "test/TestApproxTrans.hpp"



#endif // TestApproxTrans_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
