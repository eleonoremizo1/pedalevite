/*****************************************************************************

        DrawLfos.h
        Author: Laurent de Soras, 2024

Plots all LFO waveforms with various settings to EPS files.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (DrawLfos_HEADER_INCLUDED)
#define DrawLfos_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/ctrl/lfo/LfoModule.h"

#include <string>
#include <vector>



class DrawLfos
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     draw_all_lfos ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static void    run_lfo (int &ret_val, std::string pathname, int points_per_period, int nbr_periods, mfx::dsp::ctrl::lfo::LfoModule::Type type, double chaos, double ph_dist, double ph_dist_ofs, bool inv_flag, bool unipolar_flag, double var0, double var1, double snh, double smooth, double var0end = -1, double var1end = -1);
	static int     save_lfo_drawing (std::string pathname, const std::vector <double> &data_arr, int nbr_periods);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DrawLfos ()                               = delete;
	               DrawLfos (const DrawLfos &other)          = delete;
	               DrawLfos (DrawLfos &&other)               = delete;
	DrawLfos &     operator = (const DrawLfos &other)        = delete;
	DrawLfos &     operator = (DrawLfos &&other)             = delete;
	bool           operator == (const DrawLfos &other) const = delete;
	bool           operator != (const DrawLfos &other) const = delete;

}; // class DrawLfos



//#include "test/DrawLfos.hpp"



#endif // DrawLfos_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
