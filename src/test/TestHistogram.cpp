/*****************************************************************************

        TestHistogram.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/Histogram.h"
#include "test/TestHistogram.h"

#include <random>
#include <vector>

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestHistogram::perform_test ()
{
	printf ("Testing Histogram...\n");

	std::mt19937 gen;
	std::uniform_real_distribution <> dis (0.0, 1.0);
	std::vector <double> v (10'000);
	for (auto &x : v)
	{
		x = dis (gen) - dis (gen) + dis (gen) - dis (gen);
	}

	Histogram histo;
	histo.reset_and_fill (v.begin (), v.end (), 79, "Irwin-Hall distribution");
	const auto     s = histo.print (10);
	printf ("%s\n", s.c_str ());

	printf ("Done.\n");

	return 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
