/*****************************************************************************

        Benchmark.h
        Author: Laurent de Soras, 2023

Measure execution times by calling start_single() before running the task to
be measured, then stop_single() after.
Repeat as many times you need, all measures are kept.
Then call compute_time() or compute_rate() to retrieve the data.
Returned time is an average of the 4/10 to 6/10 quantiles.

Functions are not RT-safe, but calling reserve() first allows calling start
and stop functions from an RT-thread.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (Benchmark_HEADER_INCLUDED)
#define Benchmark_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "test/TimerAccurate.h"

#include <vector>



class Benchmark
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           reset () noexcept;
	void           reserve (int max_nbr_tests);

	fstb_FORCEINLINE void
	               start_single () noexcept;
	fstb_FORCEINLINE void
	               stop_single () noexcept;

	double         compute_time (double optstop = 0) noexcept;
	double         compute_rate (long long nbr_op, double optstop = 0) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	inline bool    are_results_ready () const noexcept;
	void           prepare_results (double optstop) noexcept;

	std::vector <double>
	               _result_arr;
	TimerAccurate  _tim;
	bool           _run_flag = false;

	double         _res_time = -1; // Negative: results not processed yet



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Benchmark &other) const = delete;
	bool           operator != (const Benchmark &other) const = delete;

}; // class Benchmark



#include "test/Benchmark.hpp"



#endif   // Benchmark_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
