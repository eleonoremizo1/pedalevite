/*****************************************************************************

        TimerAccurate.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/TimerAccurate.h"

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



TimerAccurate::MaxResClock::duration	TimerAccurate::get_best_duration () const noexcept
{
	assert (_best != MaxResClock::duration::max ());

	return _best;
}



double	TimerAccurate::get_best_rate (long nbr_spl) const noexcept
{
	const double   dur  = conv_to_s (get_best_duration ());
	const double   rate = double (nbr_spl) / dur;

	return rate;
}



double	TimerAccurate::conv_to_s (MaxResClock::duration dur) noexcept
{
	constexpr auto   per =
		  double (MaxResClock::duration::period::num)
		/ double (MaxResClock::duration::period::den);

	const double   s = double (dur.count ()) * per;

	return s;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
