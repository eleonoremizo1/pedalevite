
#include "fstb/msg/MsgRet.h"
#include "fstb/msg/QueueRetMgr.h"
#include "fstb/AllocAlign.h"
#include "fstb/DataAlign.h"
#include "fstb/fnc.h"
#include "fstb/Vf32.h"
#include "fstb/ToolsSimd.h"
#include "hiir/Upsampler2xFpu.h"
#include "hiir/PolyphaseIir2Designer.h"
#include "mfx/dsp/dyn/EnvFollowerAHR1LrSimdHelper.h"
#include "mfx/dsp/dyn/EnvFollowerAHR4SimdHelper.h"
#include "mfx/dsp/dyn/EnvHelper.h"
#include "mfx/dsp/fir/RankSelA.h"
#include "mfx/dsp/fir/RankSelL.h"
#include "mfx/dsp/iir/OnePole.h"
#include "mfx/dsp/iir/TransSZBilin.h"
#include "mfx/dsp/mix/Generic.h"
#include "mfx/dsp/rspl/InterpPhaseFpu.h"
#include "mfx/dsp/rspl/InterpPhaseSimd.h"
#include "mfx/Cst.h"
#include "mfx/FileIOInterface.h"
#include "mfx/FileOpWav.h"
#include "test/Benchmark.h"
#include "test/DrawLfos.h"
#include "test/DrawShapers.h"
#include "test/GenSig.h"
#include "test/Histogram.h"
#include "test/PatchSetup.h"
#include "test/Testadrv.h"
#include "test/TestAnalysisFreq.h"
#include "test/TestApprox.h"
#include "test/TestApproxHyper.h"
#include "test/TestApproxTrans.h"
#include "test/TestApproxTrigo.h"
#include "test/TestBbdLine.h"
#include "test/TestBigMuffPi.h"
#include "test/TestBitTrace.h"
#include "test/TestConvolverFft.h"
#include "test/TestDesignElliptic.h"
#include "test/TestDesignPhaseMin.h"
#include "test/TestDiodeClipDAngelo.h"
#include "test/TestDiodeClipJcm.h"
#include "test/TestDiodeClipScreamer.h"
#include "test/TestDistPmChannel.h"
#include "test/TestDkmSimulator.h"
#include "test/TestDsmMgr.h"
#include "test/TestFfft.h"
#include "test/TestFreqFast.h"
#include "test/TestFstbFnc.h"
#include "test/TestHardclipBl.h"
#include "test/TestHash.h"
#include "test/TestHelperDispNum.h"
#include "test/TestHiir.h"
#include "test/TestHiirDesigner.h"
#include "test/TestI2sClient.h"
#include "test/TestInterpFtor.h"
#include "test/TestInterpPhase.h"
#include "test/Testlal.h"
#include "test/TestLatAlgo.h"
#include "test/TestMoogLadderDAngelo.h"
#include "test/TestMoogLadderMystran.h"
#include "test/TestOnsetNinos2.h"
#include "test/TestOscSample.h"
#include "test/TestOscSampleSyncFade.h"
#include "test/TestOscSampleSyncHard.h"
#include "test/TestOscSinCosStable.h"
#include "test/TestOscSinCosStableSimd.h"
#include "test/TestOscWavetable.h"
#include "test/TestOscWavetableSub.h"
#include "test/TestOscWavetableSyncHard.h"
#include "test/TestOvrsplMulti.h"
#include "test/TestPinkShade.h"
#include "test/TestPlugins.h"
#include "test/TestPsu.h"
#include "test/TestPwmMgr.h"
#include "test/TestR128.h"
#include "test/TestRankSel.h"
#include "test/TestRcClipGeneric.h"
#include "test/TestRemez.h"
#include "test/TestRnd.h"
#include "test/TestSampleMipMapper.h"
#include "test/TestShaper.h"
#include "test/TestMnaSimulator.h"
#include "test/TestReverb.h"
#include "test/TestSlidingMax.h"
#include "test/TestSlidingOp.h"
#include "test/TestSmooth.h"
#include "test/TestSpectralFreeze.h"
#include "test/TestSplitAp5.h"
#include "test/TestSplitMultiband.h"
#include "test/TestSplitMultibandBustad.h"
#include "test/TestSplitMultibandLin.h"
#include "test/TestSplitMultibandSimdGen.h"
#include "test/TestSplitThiele8.h"
#include "test/TestStreamSimple.h"
#include "test/TestSvf.h"
#include "test/TestSvfAntisat.h"
#include "test/TestVelvetConv.h"
#include "test/TestWindows.h"

#if fstb_SYS == fstb_SYS_LINUX
	#include "test/TestDisplayFrameBufSimple.h"
	#include "test/TestLinux.h"
#endif

#if defined (PV_RPI_VER_MAJOR)
	#if PV_RPI_VER_MAJOR >= 3 && PV_RPI_VER_MAJOR < 5
		#include "test/TestRpiBcm.h"
	#elif PV_RPI_VER_MAJOR >= 5
		#include "test/TestI2sRp1.h"
	#endif // PV_RPI_VER_MAJOR
	#include "mfx/hw/Higepio.h"
	#include "test/TestLedSimple.h"
	#include "test/TestUserInputRpi.h"
#endif // defined (PV_RPI_VER_MAJOR)

#include <algorithm>
#include <array>
#include <vector>

#include <cassert>
#include <climits>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdio>



int	test_transients ()
{
	printf ("Testing transient design...\n");

	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr;
	std::vector <std::vector <float> >  dst_arr;

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		static const int  order = 2;
		typedef mfx::dsp::dyn::EnvFollowerAHR4SimdHelper <
			fstb::DataAlign <true>,
			fstb::DataAlign <true>,
			fstb::DataAlign <true>,
			order
		> EnvHelper;
		typedef std::vector <
			fstb::Vf32,
			fstb::AllocAlign <fstb::Vf32, 16>
		> BufAlign;

		const size_t   len     = chn_arr [0].size ();
		const int      buf_len = 256;
		BufAlign       buf_src (buf_len);
		BufAlign       buf_dst (buf_len);
		dst_arr.resize (4);
		for (auto &chn : dst_arr)
		{
			chn.resize (len);
		}
		EnvHelper      env;
		const double   min_freq = 50; // Hz
		const int      hold_time = fstb::round_int (sample_freq / min_freq);

		// Attack, fast envelope
		env.set_atk_coef (0, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.0001, order), sample_freq)));
		env.set_rls_coef (0, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.050 , order), sample_freq)));
		env.set_hold_time (0, hold_time);

		// Attack, slow envelope
		env.set_atk_coef (1, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.050 , order), sample_freq)));
		env.set_rls_coef (1, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.050 , order), sample_freq)));
		env.set_hold_time (1, hold_time);

		// Sustain, fast envelope
		env.set_atk_coef (2, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.005 , order), sample_freq)));
		env.set_rls_coef (2, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.200 , order), sample_freq)));
		env.set_hold_time (2, hold_time);

		// Sustain, slow envelope
		env.set_atk_coef (3, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.005 , order), sample_freq)));
		env.set_rls_coef (3, float (mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (mfx::dsp::dyn::EnvHelper::compensate_order (0.600 , order), sample_freq)));
		env.set_hold_time (3, hold_time);

		mfx::dsp::iir::OnePole  hpf;
		const float    bs [2] = { 0.125f, 3 };
		const float    as [2] = { 1     , 1 };
		float          bz [2];
		float          az [2];
		mfx::dsp::iir::TransSZBilin::map_s_to_z_one_pole (
			bz, az, bs, as, 2000, sample_freq
		);
		hpf.set_z_eq (bz, az);

		for (size_t pos = 0; pos < len; ++pos)
		{
			float          a = chn_arr [0] [pos];
			a = hpf.process_sample (a);

			auto           x = fstb::Vf32 (fabsf (a));
			x = env.process_sample (x);
			for (int chn = 0; chn < 4; ++chn)
			{
				dst_arr [chn] [pos] = x.template extract <0> ();
				x = x.template rotate <-1> ();
			}
		}

		ret_val = mfx::FileOpWav::save (
			"results/transients-envelopes.wav", dst_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



int	test_envelope_detector ()
{
	printf ("Testing mfx::dsp::dyn::EnvFollowerAHR1LrSimdHelper...\n");

	static const int  nbr_chn = 1;
	double         sample_freq;
	std::vector <std::vector <float> >  chn_arr (nbr_chn);

	int            ret_val = GenSig::gen_sample (sample_freq, chn_arr);

	if (ret_val == 0)
	{
		static const int  order = 2;
		const size_t   len = chn_arr [0].size ();
		mfx::dsp::dyn::EnvFollowerAHR1LrSimdHelper <fstb::DataAlign <false>, order> env_hlp;
		const int      max_block_size = 64;

		env_hlp.set_atk_coef (float (
			mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (
				mfx::dsp::dyn::EnvHelper::compensate_order (0.001, order),
				sample_freq
			)
		));
		env_hlp.set_rls_coef (float (
			mfx::dsp::dyn::EnvHelper::compute_env_coef_simple (
				mfx::dsp::dyn::EnvHelper::compensate_order (0.100, order),
				sample_freq
			)
		));
		env_hlp.set_hold_time (fstb::round_int (sample_freq * 0.030));

		size_t         pos = 0;
		do
		{
#if 1
			const int      block_len =
				int (std::min (len - pos, size_t (max_block_size)));
			mfx::dsp::mix::Generic::mult_ip_1_1 (&chn_arr [0] [pos], &chn_arr [0] [pos], block_len);
			env_hlp.process_block (&chn_arr [0] [pos], &chn_arr [0] [pos], block_len);
			for (size_t p2 = pos; p2 < pos + block_len; ++p2)
			{
				chn_arr [0] [p2] = sqrtf (chn_arr [0] [p2]);
			}
#else
			const int      block_len = 1;
			const float    x  = chn_arr [0] [pos];
			const float    y2 = env_hlp.process_sample (x * x);
			chn_arr [0] [pos] = sqrtf (y2);
#endif

			pos += block_len;
		}
		while (pos < len);

		ret_val = mfx::FileOpWav::save (
			"results/envdet0.wav", chn_arr, sample_freq, 1
		);
	}

	printf ("Done.\n");

	return ret_val;
}



class QueueRetData
{
public:
	void           set_data (int val)
	{
		assert (val > 0);
		_dat = val;
	}
	int            get_data () const
	{
		assert (_dat > 0);
		return _dat;
	}
	void           clear ()
	{
		if (_dat != 0)
		{
			_dat = 0;
			++ _nbr_clr;
		}
	}
	~QueueRetData ()
	{
		clear ();
	}
	static int     _nbr_clr;
private:
	int            _dat = 0;
};
int QueueRetData::_nbr_clr = 0;

int	test_queue_ret ()
{
	printf ("Testing fstb::msg::QueueRetMgr...\n");

	int            ret_val = 0;

	fstb::msg::QueueRetMgr <fstb::msg::MsgRet <QueueRetData> > queue_mgr;

	const int      idx_base = 1;
	int            idx_s = idx_base;
	int            idx_r = idx_s;
	const int      nbr_a = 1000;
	QueueRetData::_nbr_clr = 0;

	auto           q_sptr = queue_mgr.create_new_ret_queue ();
	for (int i = 0; i < nbr_a && ret_val == 0; ++i)
	{
		auto           cell_ptr = queue_mgr.use_pool ().take_cell (true);
		if (cell_ptr == nullptr)
		{
			assert (false);
		}
		else
		{
			cell_ptr->_val._content.set_data (idx_s);
			queue_mgr.enqueue (*cell_ptr, q_sptr);
		}
		++ idx_s;

		if ((i & 7) == 5)
		{
			queue_mgr.flush_ret_queue (*q_sptr);
		}

		cell_ptr = nullptr;
		do
		{
			cell_ptr = queue_mgr.dequeue ();
			if (cell_ptr != nullptr)
			{
				const int      val = cell_ptr->_val._content.get_data ();
				if (val != idx_r)
				{
					printf (
						"QueueRetMgr: wrong order (received %d, expected %d).\n",
						val, idx_r
					);
					ret_val = -1;
				}
				cell_ptr->_val.ret ();
				++ idx_r;
			}
		}
		while (cell_ptr != nullptr);

	}
	queue_mgr.flush_ret_queue (*q_sptr);
	queue_mgr.kill_ret_queue (q_sptr);

	if (idx_s != idx_r || idx_r != idx_base + nbr_a)
	{
		printf ("QueueRetMgr: messages lost.\n");
		ret_val = -1;
	}
	if (QueueRetData::_nbr_clr != nbr_a)
	{
		printf ("QueueRetMgr: failed to deallocate all message's resources.\n");
		ret_val = -1;
	}

	printf ("Done.\n");

	return ret_val;
}



int	test_conv_int_fast ()
{
	printf ("Testing fstb::conv_int_fast():\n");

	bool           trunc_flag = true;
	bool           round_flag = true;
	bool           rnda0_flag = true;
	bool           rnd20_flag = true;
	bool           floor_flag = true;
	for (int k = -8; k <= 8; ++k)
	{
		const float    v_flt = float (k) * 0.5f;
		const int      v_int = fstb::conv_int_fast (v_flt);
		printf ("%+6.2f -> %+3d\n", v_flt, v_int);

		if (trunc (v_flt) != v_int)
		{
			trunc_flag = false;
		}
		if (int (floor (v_flt + 0.5f)) != v_int)
		{
			round_flag = false;
		}
		if (lround (v_flt) != v_int)
		{
			rnda0_flag = false;
		}
		if (   int (floor (v_flt     + 0.5f)) != v_int
		    && int (floor (v_flt * 2 + 0.5f)) != v_int * 2 + 1)
		{
			rnd20_flag = false;
		}
		if (int (floor (v_flt)) != v_int)
		{
			floor_flag = false;
		}
	}
	printf ("Mode: ");
	if (trunc_flag)
	{
		printf ("trunc\n");
	}
	else if (round_flag)
	{
		printf ("exact round\n");
	}
	else if (rnda0_flag)
	{
		printf ("round and half-way case away from 0\n");
	}
	else if (rnd20_flag)
	{
		printf ("round and half-way case to the nearest even integer\n");
	}
	else if (floor_flag)
	{
		printf ("floor\n");
	}
	else
	{
		printf ("unknown\n");
	}

	return 0;
}



#if fstb_ARCHI == fstb_ARCHI_X86



#define RES __restrict

void multiply1 (std::complex <float> * RES dst_ptr, const std::complex <float> * RES lhs_ptr, const std::complex <float> * RES rhs_ptr, int n)
{
    for (int k = 0; k < n; ++k)
    {
        dst_ptr [k] = lhs_ptr [k] * rhs_ptr [k];
    }
}

void multiply3 (std::complex <float> * RES dst_ptr, const std::complex <float> * RES lhs_ptr, const std::complex <float> * RES rhs_ptr, int n)
{
    for (int k = 0; k < n; ++k)
    {
        dst_ptr [k] = {
              lhs_ptr [k].real () * rhs_ptr [k].real ()
            - lhs_ptr [k].imag () * rhs_ptr [k].imag (),
              lhs_ptr [k].real () * rhs_ptr [k].imag ()
            + lhs_ptr [k].imag () * rhs_ptr [k].real ()
		  };
    }
}

fstb_FORCEINLINE void vecop (float * RES df_ptr, const float * RES lf_ptr, const float * RES rf_ptr)
{
    constexpr int deint0 = (0<<0)|(2<<2)|(0<<4)|(2<<6);
    constexpr int deint1 = (1<<0)|(3<<2)|(1<<4)|(3<<6);
    const auto l0 = _mm_loadu_ps (lf_ptr    );
    const auto l1 = _mm_loadu_ps (lf_ptr + 4);
    const auto r0 = _mm_loadu_ps (rf_ptr    );
    const auto r1 = _mm_loadu_ps (rf_ptr + 4);
    const auto lr = _mm_shuffle_ps (l0, l1, deint0);
    const auto li = _mm_shuffle_ps (l0, l1, deint1);
    const auto rr = _mm_shuffle_ps (r0, r1, deint0);
    const auto ri = _mm_shuffle_ps (r0, r1, deint1);
    const auto dr = _mm_sub_ps (_mm_mul_ps (lr, rr), _mm_mul_ps (li, ri));
    const auto di = _mm_add_ps (_mm_mul_ps (lr, ri), _mm_mul_ps (li, rr));
    const auto d0 = _mm_unpacklo_ps (dr, di);
    const auto d1 = _mm_unpackhi_ps (dr, di);
    _mm_storeu_ps (df_ptr    , d0);
    _mm_storeu_ps (df_ptr + 4, d1);
}

void multiply2 (std::complex <float> * RES dst_ptr, const std::complex <float> * RES lhs_ptr, const std::complex <float> * RES rhs_ptr, int n)
{
    constexpr int blk = 4;
    constexpr int bsh = 2;
    const int nx = n & ~((blk << bsh) - 1);
    float * RES df_ptr = reinterpret_cast <float *> (dst_ptr);
    const float * RES lf_ptr = reinterpret_cast <const float *> (lhs_ptr);
    const float * RES rf_ptr = reinterpret_cast <const float *> (rhs_ptr);
    for (int k = 0; k < nx; k += blk << bsh)
    {
        for (int b = 0; b < 1 << bsh; ++b)
        {
            const int    ofs = k * 2 + b * blk * 2;
            vecop (df_ptr + ofs, lf_ptr + ofs, rf_ptr + ofs);
        }
    }
    multiply1 (dst_ptr + nx, lhs_ptr + nx, rhs_ptr + nx, n - nx);
}

void test_mult_cplx_vect ()
{
	printf ("Testing complex multiplication...\n");

	constexpr int  sz = 256;
	std::vector <std::complex <float> > l (sz);
	std::vector <std::complex <float> > r (sz);
	std::vector <std::complex <float> > d (sz);

	BufferFiller::gen_rnd_non_zero (reinterpret_cast <float *> (l.data ()), sz * 2);
	BufferFiller::gen_rnd_non_zero (reinterpret_cast <float *> (r.data ()), sz * 2);

	constexpr int  nbr_tst = 65536 * 64;

	Benchmark  ben;

	ben.start_single ();
	for (int t = 0; t < nbr_tst; ++t)
	{
		multiply2 (d.data (), l.data (), r.data (), sz);
	}
	ben.stop_single ();

	const double   spl_per_s = ben.compute_rate (sz * nbr_tst, d.back ().real ());
	const double   mega_sps  = spl_per_s / 1e6;
	printf ("Speed: %12.3f Mop/s.\n", mega_sps);
}



#endif // fstb_ARCHI_X86



/*** To do: move this class in a separate file, make it more generic ***/
class TestMulloEpi32
{
public:
	static int perform_test ()
	{
		printf ("Testing fstb::Vu32::operator *...\n");
		fflush (stdout);

		constexpr int  buf_len   = 1 << 8; // Must be a power of 2
		constexpr int  nbr_loops = 1'000'000;
		constexpr int  buf_msk   = buf_len - 1;
		alignas (16) std::array <fstb::Vu32, buf_len> buf_dst;
		alignas (16) std::array <fstb::Vu32, buf_len> buf_s1;
		alignas (16) std::array <fstb::Vu32, buf_len> buf_s2;

		// Init the source buffers
		for (int k = 0; k < buf_len; ++k)
		{
			const auto     kv = fstb::Vu32 (k * 8);
			buf_s1 [k] = fstb::Hash::hash (fstb::Vu32 (1, 2, 3, 4) + kv);
			buf_s2 [k] = fstb::Hash::hash (fstb::Vu32 (5, 6, 7, 8) + kv);
		}

		uint32_t    sum = 0;

		Benchmark  ben;

		const fstb::Vu32 * fstb_RESTRICT s1_ptr  = buf_s1.data ();
		const fstb::Vu32 * fstb_RESTRICT s2_ptr  = buf_s2.data ();
		fstb::Vu32 * fstb_RESTRICT       dst_ptr = buf_dst.data ();

		ben.start_single ();
		for (int t = 0; t < nbr_loops; ++t)
		{
			for (int k = 0; k < buf_len; k += 4)
			{
				const auto     a0 = fstb::Vu32::load (s1_ptr + k    );
				const auto     a1 = fstb::Vu32::load (s1_ptr + k + 1);
				const auto     a2 = fstb::Vu32::load (s1_ptr + k + 2);
				const auto     a3 = fstb::Vu32::load (s1_ptr + k + 3);
				const auto     b0 = fstb::Vu32::load (s2_ptr + k    );
				const auto     b1 = fstb::Vu32::load (s2_ptr + k + 1);
				const auto     b2 = fstb::Vu32::load (s2_ptr + k + 2);
				const auto     b3 = fstb::Vu32::load (s2_ptr + k + 3);
				const auto     p0 = a0 * b0;
				const auto     p1 = a1 * b1;
				const auto     p2 = a2 * b2;
				const auto     p3 = a3 * b3;
				p0.store (dst_ptr + k    );
				p1.store (dst_ptr + k + 1);
				p2.store (dst_ptr + k + 2);
				p3.store (dst_ptr + k + 3);
			}

			sum += uint32_t (buf_dst [t & buf_msk].template extract <0> ());
		}
		ben.stop_single ();

		const double   spl_per_s = ben.compute_rate (buf_len * nbr_loops, sum);
		const double   mega_sps  = spl_per_s / 1e6;
		printf ("Speed: %12.3f Mop/s.\n", mega_sps);
		fflush (stdout);

		return 0;
	}
};



int main (int argc, char *argv [])
{
	fstb::unused (argc, argv);

	mfx::dsp::mix::Generic::setup ();

	// Disable denormals. Calculations are now without headroom in the
	// smallest values. If they work as expected without denormals, there
	// are chances that they work with denormals enabled too.
	fstb::ToolsSimd::disable_denorm ();

	int            ret_val = 0;

#define main_TEST_SPEED 0

#if 1
	if (ret_val == 0) ret_val = TestDistPmChannel::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestShaper::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestStreamSimple::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestOvrsplMulti::perform_test ();
#endif

#if 0
	#if defined (PV_RPI_VER_MAJOR) && PV_RPI_VER_MAJOR >= 5
	if (ret_val == 0) ret_val = TestI2sRp1::perform_test ();
	#endif
#endif

#if 0
	if (ret_val == 0) ret_val = TestI2sClient::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestBitTrace::perform_test ();
#endif

#if 0
	#if fstb_SYS == fstb_SYS_LINUX
	if (ret_val == 0) ret_val = TestLinux::test_nanosleep_latency ();
	#endif
#endif

#if 0
	#if fstb_SYS == fstb_SYS_LINUX
	if (ret_val == 0) ret_val = TestLinux::test_gettimeofday ();
	#endif
#endif

#if 0
	if (ret_val == 0) ret_val = TestHistogram::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPwmMgr::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestDsmMgr::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestFstbFnc::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestHelperDispNum::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestMulloEpi32::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSpectralFreeze::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestR128::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestOnsetNinos2::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestVelvetConv::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestDesignPhaseMin::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestDesignElliptic::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSplitMultibandSimdGen::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSplitMultibandBustad::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSplitMultibandLin::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSplitMultiband::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = Testadrv::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscSinCosStableSimd::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscSinCosStable::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestAnalysisFreq::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSplitAp5::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSplitThiele8::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestReverb::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestWindows::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = Testlal::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestRnd::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestHash::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestFfft::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestHiir::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestHiirDesigner::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestBigMuffPi::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestDkmSimulator::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestMnaSimulator::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestMoogLadderMystran::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestPinkShade::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestHardclipBl::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestMoogLadderDAngelo::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestSvfAntisat::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestRcClipGeneric::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestDiodeClipJcm::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestDiodeClipScreamer::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestDiodeClipDAngelo::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSvf::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSmooth::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = DrawShapers::draw_shapers ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPsu::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = test_conv_int_fast ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestLatAlgo::perform_test ();
#endif

#if 0
	#if fstb_SYS == fstb_SYS_LINUX
	if (ret_val == 0) ret_val = TestLinux::test_file_write_fs_ro ();
	#endif
#endif

#if 0
	#if defined (PV_RPI_VER_MAJOR)
	if (ret_val == 0) ret_val = TestUserInputRpi::perform_test ();
	#endif
#endif

#if 0
	#if defined (PV_RPI_VER_MAJOR)
	if (ret_val == 0) ret_val = TestLedSimple::perform_test ();
	#endif
#endif

#if 0
	#if fstb_SYS == fstb_SYS_LINUX
	if (ret_val == 0) ret_val = TestDisplayFrameBufSimple::perform_test ();
	#endif
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestApprox::perform_test ();
	if (ret_val == 0) ret_val = TestApproxHyper::perform_test ();
	if (ret_val == 0) ret_val = TestApproxTrans::perform_test ();
	if (ret_val == 0) ret_val = TestApproxTrigo::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSlidingMax::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSlidingOp::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscWavetableSyncHard::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscWavetableSub::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscWavetable::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscSampleSyncFade::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscSampleSyncHard::perform_test ();
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestOscSample::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestSampleMipMapper::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestConvolverFft::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestRemez::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestInterpFtor::perform_test ();
#endif

#if 0
	#if fstb_ARCHI == fstb_ARCHI_X86
		if (ret_val == 0) test_mult_cplx_vect ();
	#endif
#endif

#if main_TEST_SPEED
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseFpu <4>, 6>::perform_test ();
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseSimd <4>, 6>::perform_test ();
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseFpu <12>, 6>::perform_test ();
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseSimd <12>, 6>::perform_test ();
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseFpu <64>, 6>::perform_test ();
	if (ret_val == 0) ret_val = TestInterpPhase <mfx::dsp::rspl::InterpPhaseSimd <64>, 6>::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = test_queue_ret ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestRankSel::perform_test ();
#endif

#if 0
	#if PV_RPI_VER_MAJOR >= 3 && PV_RPI_VER_MAJOR < 5
	if (ret_val == 0) ret_val = TestRpiBcm::test_adrv_dpvab ();
	#endif
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_testgen ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_dist1 ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPluign::test_phase1 ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_nzcl ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_nzbl ();
#endif

#if 0
	if (ret_val == 0) ret_val = DrawLfos::draw_all_lfos ();
#endif

#if 0
	if (ret_val == 0) ret_val = test_transients ();
#endif

#if 0
	if (ret_val == 0) ret_val = PatchSetup::patch_setup_file ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestBbdLine::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_dly2 ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestPlugin::test_osdet ();
#endif

#if 0
	if (ret_val == 0) ret_val = TestFreqFast::perform_test ();
#endif

#if 0
	if (ret_val == 0) ret_val = test_envelope_detector ();
#endif

	if (ret_val == 0)
	{
#if 0
		printf ("Everything is OK.\n");
#endif
	}
	else
	{
		printf ("There were errors.\n");
	}

	return ret_val;
}
