/*****************************************************************************

        I2sClient.h
        Author: Laurent de Soras, 2024

Software interface for I2S coding/decoding at the bit level.
Supports clock master and clock slave modes.

Usage:

Configures the callback first.
Then loop:
- call set_pin() to signal a pin value change
- call step() to simulate a wait. The callback functions may be called
if something happened at the beginning of the step().

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (I2sClient_HEADER_INCLUDED)
#define I2sClient_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/I2sClientCbInterface.h"
#include "test/I2sPin.h"

#include <array>



class I2sClient
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_chn = I2sClientCbInterface::_nbr_chn;

	explicit       I2sClient (bool master_flag, int bitdepth, int transfer_lag, bool left_high_flag) noexcept;
	               I2sClient (const I2sClient &other)  = default;
	               I2sClient (I2sClient &&other)       = default;
	               ~I2sClient ()                       = default;

	bool           is_clock_master () const noexcept;

	void           set_callback (I2sClientCbInterface &cb) noexcept;
	void           reset () noexcept;
	void           set_offset (int edge_count) noexcept;

	void           set_pin (I2sPin p, bool state_flag) noexcept;
	void           step ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	typedef std::array <bool, I2sPin_NBR_ELT> PinState;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class BitPos
	{
	public:
		int            _chn = 0;
		int            _bit = 0; // 0 = LSB
	};

	inline int     compute_frame_pos () const noexcept;
	inline BitPos  compute_bit_pos (int frame_pos) const noexcept;
	inline static int
	               shift_pos (int pos, int ofs, int val_wrap) noexcept;

	// Indicates if we are master clock (generating SCLK and LRCK) or
	// slave clock (receiving SCLK and LRCK)
	const bool     _master_flag  = true;

	// Bitdepth of each transferred sample, in [1 ; 32]
	const int      _bitdepth     = 32;

	// Number of clocks the LRCK signal is ahead of the corresponding sample
	// first bit (MSB). Usually 1 for standard I2S.
	const int      _transfer_lag = 1;

	// Polarity of the LRCK clock.
	// false = low  for left channel, high for right (standard I2S)
	// true  = high for left channel, low  for right
	const bool     _left_high_flag = false;

	// Must be set before any call to step()
	I2sClientCbInterface *
	               _cb_ptr       = nullptr;

	// Current position within the sample frame, in SCLK half-ticks (in edges).
	// Origin is the rising SCLK edge sampling an LRCK rising edge.
	// Values in [0 ; _bitdepth * 4 [
	int            _clock_pos    = 0;

	// Current pin state
	PinState       _st_cur {};

	I2sClientCbInterface::SampleFrame
	               _data_in {};
	I2sClientCbInterface::SampleFrame
	               _data_out {};

	// Number of edges of successful synchronisation (clock slave only)
	int            _sync_count  = 0;

	// Precalculated
	const int      _len_spl_e   = 0; // Length of a single sample, in clock edges
	const int      _len_frame_e = 0; // Length of a full frame, in clock edges
	const int      _len_frame_b = 0; // Length of a full frame, in bits



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               I2sClient ()                               = delete;
	I2sClient &    operator = (const I2sClient &other)        = delete;
	I2sClient &    operator = (I2sClient &&other)             = delete;
	bool           operator == (const I2sClient &other) const = delete;
	bool           operator != (const I2sClient &other) const = delete;

}; // class I2sClient



//#include "test/I2sClient.hpp"



#endif   // I2sClient_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
