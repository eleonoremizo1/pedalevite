/*****************************************************************************

        TestI2sClient.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#if defined (PV_RPI_VER_MAJOR)
# include "mfx/hw/HigepioLinux.h"
#endif // PV_RPI_VER_MAJOR
#include "test/I2sEnd.h"
#include "test/TestI2sClient.h"

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestI2sClient::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing I2sClient...\n");

#if 1
	if (ret_val == 0)
	{
		ret_val = test_software ();
	}
#endif

// Connect GPIOs (crossing SDIN and STOUT) before enabling this
#if 0 && defined (PV_RPI_VER_MAJOR)
	if (ret_val == 0)
	{
		ret_val = test_gpio ();
	}
#endif // PV_RPI_VER_MAJOR

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestI2sClient::test_software ()
{
	int            ret_val = 0;

	printf ("Software simulation...\n");

	I2sEnd         cli_rem (true , 32, 1, false, false); // Master
	I2sEnd         cli_loc (false, 32, 1, false, true ); // Slave
	cli_rem.set_other (cli_loc);
	cli_loc.set_other (cli_rem);

	int            pos_spl = 0;
	int            t       = 0;
	do
	{
		cli_rem.run_step (t); // Master runs first to generate the clocks
		pos_spl = cli_loc.run_step (t);
		++ t;
	}
	while (pos_spl < 10);
	cli_loc.dump_traces ();

	return ret_val;
}



#if defined (PV_RPI_VER_MAJOR)

int	TestI2sClient::test_gpio ()
{
	int            ret_val = 0;

	printf ("Simulation using GPIOs...\n");

	I2sEnd         cli_rem (true , 32, 1, false, false); // Master
	I2sEnd         cli_loc (false, 32, 1, false, true ); // Slave
	mfx::hw::HigepioLinux io (
		  (1u << 2) | (1u <<  3)              // SDA1, SCL1 (I2C)
		| (1u << 9) | (1u << 10) | (1u << 11) // MISO, MOSI, SCLK (SPI)
	);
	cli_rem.set_io (io);
	cli_loc.set_io (io);

	bool           cont_flag = true;
	int            t         = 0;
	do
	{
		cli_rem.run_step (t); // Master runs first to generate the clocks
		cont_flag = cli_loc.run_step (t);
		++ t;
	}
	while (cont_flag);
	cli_loc.dump_traces ();

	return ret_val;
}

#endif // PV_RPI_VER_MAJOR



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
