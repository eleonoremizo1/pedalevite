/*****************************************************************************

        TestApproxHelper.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestApproxHelper_HEADER_INCLUDED)
#define TestApproxHelper_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <array>
#include <map>
#include <string>
#include <vector>

#include <cstddef>



class Gridaxis;

class TestApproxHelper
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Big value for the tolerance, when we want to only obtain the figure.
	static constexpr double _notol = 1e30;

	enum class ErrType
	{
		_abs = 0,
		_rel,

		_nbr_elt
	};

	enum class ErrDataType
	{
		_abs = 0,
		_rel,
		_ulp,

		_nbr_elt
	};

	static constexpr std::array <const char * const, size_t (ErrType::_nbr_elt)>
	               _err_type_0_arr = { "absolute", "relative" };

	class TestSpec
	{
	public:
		inline bool    is_checked (ErrType type) const noexcept;
		inline bool    has_custom_plot_range () const noexcept;

		// Tolerance, > 0. 0: not tested.
		std::array <double, size_t (ErrType::_nbr_elt)>
		               _tol_arr;

		bool           _accurate_flag = false;

		// min, max. Same values: same range as the main test range
		std::array <double, 2>
		               _plot_range {};
	};

	template <typename T, int ILL2>
	class TestFncSpeed
	{
		static_assert (ILL2 >= 0, "ILL2 must be in [0 ; 3]");
		static_assert (ILL2 <= 3, "ILL2 must be in [0 ; 3]");
	public:
		template <typename OP>
		static void    test_op1 (const OP &op, const std::string &name, double min_val, double max_val);
		template <typename OP>
		static void    test_op2 (const OP &op, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2);
	};

	template <typename T>
	class TestFncLogic
	{
	public:
		template <typename OPTST, typename OPREF>
		static void    test_op1 (int &ret_val, const OPREF &op_ref, const OPTST &op_tst, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec);

		template <typename OPTST, typename OPREF>
		static void    test_op2 (int &ret_val, const OPREF &op_ref, const OPTST &op_tst, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec);
	};

	template <typename OPREF, typename OPTSTS, typename OPTSTV>
	static void    test_op1_all_flt (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const OPTSTV &op_v, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec);
	template <typename T, typename OPREF, typename OPTSTS>
	static void    test_op1_all_s (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec);
	template <typename OPREF, typename OPTSTV>
	static void    test_op1_all_flt_v (int &ret_val, const OPREF &op_ref, const OPTSTV &op_v, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec);

	template <typename OPREF, typename OPTSTS, typename OPTSTV>
	static void    test_op2_all_flt (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const OPTSTV &op_v, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec);
	template <typename T, typename OPREF, typename OPTSTS>
	static void    test_op2_all_s (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec);
	template <typename OPREF, typename OPTSTV>
	static void    test_op2_all_flt_v (int &ret_val, const OPREF &op_ref, const OPTSTV &op_v, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	template <size_t N>
	class ErrState
	{
	public:
		class Range
		{
		public:
			std::array <float, N>
			               _val   = {};
			float          _e_min = 0;
			float          _e_max = 0;
		};
		typedef std::vector <Range> RangeArray;

		inline void    accumulate (std::array <double, N> val, double err, double weight, const TestSpec &tst_spec) noexcept;

		// General statistics
		double         _max = 0; // Maximum in absolute value
		double         _tot = 0; // Running sum of _max
		double         _wgh = 0; // Number of values accumulated in _tot, weighted

		// Range collection for graph drawing.
		// Should be always sorted by ascending values.
		RangeArray     _cloud;
	};

	template <size_t N>
	using ErrStateMap = std::map <ErrDataType, ErrState <N> >;

	template <typename T>
	class ValIter
	{
	public:
		inline         ValIter (double val_beg, double val_end, int nbr_spl, bool fine_flag);
		inline void    start () noexcept;
		inline bool    is_rem_elt () const noexcept;
		inline ValIter &
		               operator ++ () noexcept;
		inline T       operator * () const noexcept;
		inline double  get_weight () const noexcept;
	private:
		double         _val_beg = 0;
		double         _val_end = 0;
		double         _step    = 0;
		int            _pos     = 0;
		int            _nbr_spl = 0;
		T              _val_cur = 0;
		double         _weight  = 0;
		bool           _fine_flag = false;
	};

	typedef std::array <float, 2> Point; // x, y

	class Boundaries
	{
	public:
		Point          _cmin = { +1e30f, +1e30f };
		Point          _cmax = { -1e30f, -1e30f };
	};

	template <typename T, size_t N>
	static bool    check_val (T val_tst, T val_ref_nat, double val_ref_dbl, std::array <double, N> val_src_arr, double weight, ErrStateMap <N> &e_map, const TestSpec &tst_spec) noexcept;
	template <size_t N>
	static void    print_results (ErrStateMap <N> &e_map, const TestSpec &tst_spec) noexcept;

	static void    draw_errors_to_files (std::string fnc_name, const ErrStateMap <1> &e_map, const TestSpec &tst_spec);
	static void    draw_surface (Gridaxis &grid, const ErrState <1> &err);
	template <typename G>
	static void    draw_single_curve (Gridaxis &grid, const ErrState <1> &err, G get_y);
	static void    draw_ulp (Gridaxis &grid, const ErrState <1> &err);
	static void    requantize_results (ErrState <1>::RangeArray &cloud);
	static Boundaries
	               find_boundaries (const ErrState <1>::RangeArray &cloud) noexcept;
	static void    refine_boundaries (Boundaries &bnd) noexcept;
	static Point   compute_tick_size (const Boundaries &bnd) noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               TestApproxHelper ()                               = delete;
	               TestApproxHelper (const TestApproxHelper &other)  = delete;
	               TestApproxHelper (TestApproxHelper &&other)       = delete;
	TestApproxHelper &
	               operator = (const TestApproxHelper &other)        = delete;
	TestApproxHelper &
	               operator = (TestApproxHelper &&other)             = delete;
	bool           operator == (const TestApproxHelper &other) const = delete;
	bool           operator != (const TestApproxHelper &other) const = delete;

}; // class TestApproxHelper



#include "test/TestApproxHelper.hpp"



#endif // TestApproxHelper_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
