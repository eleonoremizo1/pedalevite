/*****************************************************************************

        TestUserInputRpi.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/GpioPin.h"
#include "mfx/hw/Higepio.h"
#include "mfx/hw/UserInputRpi.h"
#include "mfx/ui/TimeShareThread.h"
#include "test/TestUserInputRpi.h"

#include <chrono>
#include <thread>

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestUserInputRpi::perform_test ()
{
	int            ret_val = 0;

	const int      pin_rst = mfx::hw::GpioPin::_reset;

	mfx::hw::Higepio io (
		  (1u << 2) | (1u <<  3)              // SDA1, SCL1 (I2C)
		| (1u << 9) | (1u << 10) | (1u << 11) // MISO, MOSI, SCLK (SPI)
	);
	io.set_pin_dir (pin_rst, true);
	io.write_pin (pin_rst, 0);
	std::this_thread::sleep_for (std::chrono::milliseconds (100));
	io.write_pin (pin_rst, 1);
	std::this_thread::sleep_for (std::chrono::milliseconds (1));

	typedef mfx::ui::UserInputInterface::MsgQueue MQueue;
	mfx::ui::UserInputInterface::MsgQueue  queue_input;
	mfx::ui::TimeShareThread   thread_spi (std::chrono::milliseconds (10));
	mfx::hw::UserInputRpi   user_input (thread_spi, io);
	user_input.assign_queues_to_input_dev (queue_input, queue_input, queue_input);

	do
	{
		MQueue::CellType * cell_ptr = queue_input.dequeue ();
		if (cell_ptr != nullptr)
		{
			const mfx::ui::UserInputMsg & msg  = cell_ptr->_val;
			const mfx::ui::UserInputType  type = msg.get_type ();
			const int                     idx  = msg.get_index ();
			const float                   val  = msg.get_val ();

			const char *   type_0 = "***unknown***";
			switch (type)
			{
			case mfx::ui::UserInputType_POT_ABS: type_0 = "PotAbs"; break;
			case mfx::ui::UserInputType_POT_REL: type_0 = "PotRel"; break;
			case mfx::ui::UserInputType_SW:      type_0 = "Switch"; break;
			default:
				assert (false);
				break;
			}
			printf ("Type: %s, index: %2d, val: %f\n", type_0, idx, val);

			user_input.return_cell (*cell_ptr);
		}
	}
	while (true);

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
