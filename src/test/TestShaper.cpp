/*****************************************************************************

        TestShaper.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/dsp/shape/PiecewisePoly3.h"
#include "mfx/dsp/shape/PiecewisePoly3Adaa.h"
#include "mfx/dsp/shape/SigmoidAdaa.h"
#include "mfx/dsp/shape/WsTanh.h"
#include "mfx/dsp/osc/SweepingSin.h"
#include "mfx/FileOpWav.h"
#include "test/TestShaper.h"
#include "test/TimerAccurate.h"

#include <cassert>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestShaper::perform_test ()
{
	printf ("Testing mfx::dsp::shape::*...\n");

	int            ret_val = 0;

	float          gain = 10.0f;

	{
		mfx::dsp::shape::PiecewisePoly3 <
			-8, 8, mfx::dsp::shape::WsTanh, 4
		>              shaper;
		test_sweep (shaper, gain, "piecewisepoly3");
	}

	{
		mfx::dsp::shape::PiecewisePoly3Adaa <
			-8, 8, mfx::dsp::shape::WsTanh, 4, false
		>              shaper;
		shaper.clear_buffers ();
		test_sweep (shaper, gain, "piecewisepoly3adaa");
	}

	{
		mfx::dsp::shape::PiecewisePoly3Adaa <
			-8, 8, mfx::dsp::shape::WsTanh, 4, true
		>              shaper;
		shaper.clear_buffers ();
		test_sweep (shaper, gain, "piecewisepoly3adaavic");
	}

	{
		mfx::dsp::shape::SigmoidAdaa <false>   shaper;
		shaper.clear_buffers ();
		test_sweep (shaper, gain, "sigmoidadaa");
	}

	{
		mfx::dsp::shape::SigmoidAdaa <true> shaper;
		shaper.clear_buffers ();
		test_sweep (shaper, gain, "sigmoidadaavic");
	}

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename T>
int	TestShaper::test_sweep (T &shaper, float gain, std::string name)
{
	assert (gain >= 0);
	assert (! name.empty ());

	const double   sample_freq = 44100.0;   // Sample frequency
	const int      len         = fstb::round_int (sample_freq * 10);

	std::vector <float>  src (len);
	mfx::dsp::osc::SweepingSin ssin (sample_freq, 20.0, 20000.0);
	ssin.generate (src.data (), len);

	std::vector <float>  dst (len);

	for (int pos = 0; pos < len; ++pos)
	{
		dst [pos] = shaper.process_sample (src [pos] * gain);
	}

	std::string    filename ("results/");
	filename += name;
	filename += ".wav";

	int            ret_val =
		mfx::FileOpWav::save (filename.c_str (), dst, sample_freq, 0.5f);

	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

	TimerAccurate  tim;
	tim.reset ();

#if defined (NDEBUG)
	const int      nbr_passes = 64;
#else
	const int      nbr_passes = 1;
#endif

	tim.start ();
	for (int p = 0; p < nbr_passes; ++p)
	{
		for (int pos = 0; pos < len; ++pos)
		{
			dst [pos] = shaper.process_sample (src [pos] * gain);
		}
		tim.stop_lap ();
	}

	const double   spl_per_s = tim.get_best_rate (len);
	const double   mega_sps  = spl_per_s / 1e6;
	const double   rt_mul    = spl_per_s / sample_freq;
	printf (
		"%-22s, gain =%6.1f: %12.3f Mspl/s (%9.3fx real-time).\n",
		name.c_str (), gain, mega_sps, rt_mul
	);

	return ret_val;
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
