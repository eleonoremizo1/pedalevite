/*****************************************************************************

        Benchmark.hpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (Benchmark_CODEHEADER_INCLUDED)
#define Benchmark_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Benchmark::start_single () noexcept
{
   assert (! are_results_ready ());
   assert (! _run_flag);

   _run_flag = true;
   _tim.start ();
}



void	Benchmark::stop_single () noexcept
{
   assert (_run_flag);

   const auto     dur_ticks = _tim.stop ();
   const auto     dur_s     = TimerAccurate::conv_to_s (dur_ticks);
   _result_arr.push_back (dur_s);
   _run_flag = false;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	Benchmark::are_results_ready () const noexcept
{
	return (_res_time >= 0);
}



#endif   // Benchmark_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
