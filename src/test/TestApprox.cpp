/*****************************************************************************

        TestApprox.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Approx.h"
#include "test/TestApprox.h"
#include "test/TestApproxHelper.h"

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestApprox::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing fstb::Approx...\n");

	typedef fstb::Vf32 Vf32;
	typedef TestApproxHelper Helper;
//	constexpr auto notol = Helper::_notol;
	constexpr auto pi    = fstb::PI;

	// 1 / x

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return 1.f / x; },
		"1 / x (float)", 1e-9, 4.0
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return 1.0 / x; },
		"1 / x (double)", 1e-9, 4.0
	);

	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (float x) { return fstb::Approx::rcp <0> (x); },
		"rcp <0>", 1e-9, 4.0, { { 0, 6e-2 }, true }
	);
	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (float x) { return fstb::Approx::rcp <1> (x); },
		"rcp <1>", 1e-9, 4.0, { { 0, 1.5e-3 }, true }
	);
	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (float x) { return fstb::Approx::rcp <2> (x); },
		"rcp <2>", 1e-9, 4.0, { { 0, 1.2e-6 }, true }
	);
	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (float x) { return fstb::Approx::rcp <3> (x); },
		"rcp <3>", 1e-9, 4.0, { { 0, 1.5e-7 }, true }
	);

	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (double x) { return fstb::Approx::rcp <0> (x); },
		"rcp <0>", 1e-9, 4.0, { { 0, 6e-2 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (double x) { return fstb::Approx::rcp <1> (x); },
		"rcp <1>", 1e-9, 4.0, { { 0, 1.5e-4 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (double x) { return fstb::Approx::rcp <2> (x); },
		"rcp <2>", 1e-9, 4.0, { { 0, 1e-8 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (double x) { return fstb::Approx::rcp <3> (x); },
		"rcp <3>", 1e-9, 4.0, { { 0, 4e-16 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1.0 / x; },
		[] (double x) { return fstb::Approx::rcp <4> (x); },
		"rcp <4>", 1e-9, 4.0, { { 0, 2.5e-16 }, true }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return 1 / x; },
		[] (Vf32 x) { return x.rcp_approx (); },
		"rcp_approx", 1e-9, 4.0, { { 0.0, 3.5e-4 }, true }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return 1 / x; },
		[] (Vf32 x) { return x.rcp_approx2 (); },
		"rcp_approx2", 1e-9, 4.0, { { 0.0, 2.5e-7 }, true }
	);

	// 1 / sqrt

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return 1 / sqrtf (x); },
		"1 / std::sqrt (float)", 1e-9, pi
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return 1 / sqrt (x); },
		"1 / std::sqrt (double)", 1e-9, 4.0
	);

	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (float x) { return fstb::Approx::rsqrt <0> (x); },
		"rsqrt <0>", 1e-9, 4.0, { { 0.0, 0.035 }, true }
	);
	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (float x) { return fstb::Approx::rsqrt <1> (x); },
		[] (Vf32 x) { return fstb::Approx::rsqrt <1> (x); },
		"rsqrt <1>", 1e-9, 4.0, { { 0.0, 0.001 }, true }
	);
	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (float x) { return fstb::Approx::rsqrt <2> (x); },
		[] (Vf32 x) { return fstb::Approx::rsqrt <2> (x); },
		"rsqrt <2>", 1e-9, 4.0, { { 0.0, 5e-07 }, true }
	);
	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (float x) { return fstb::Approx::rsqrt <3> (x); },
		[] (Vf32 x) { return fstb::Approx::rsqrt <3> (x); },
		"rsqrt <3>", 1e-9, 4.0, { { 0.0, 1.4e-7 }, true }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (Vf32 x) { return x.rsqrt (); },
		"rsqrt", 1e-9, 4.0, { { 0.0, 2e-7 }, true }
	);

	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (double x) { return fstb::Approx::rsqrt <1> (x); },
		"rsqrt <1>", 1e-9, 4.0, { { 0.0, 1e-3 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (double x) { return fstb::Approx::rsqrt <2> (x); },
		"rsqrt <2>", 1e-9, 4.0, { { 0.0, 7e-7 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (double x) { return fstb::Approx::rsqrt <3> (x); },
		"rsqrt <3>", 1e-9, 4.0, { { 0.0, 5e-13 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return 1 / sqrt (x); },
		[] (double x) { return fstb::Approx::rsqrt <4> (x); },
		"rsqrt <4>", 1e-9, 4.0, { { 0.0, 5e-16 }, true }
	);

	// sqrt

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return sqrtf (x); },
		"std::sqrt (float)", 0, 4
	);
	Helper::TestFncSpeed <Vf32, 0>::test_op1 (
		[] (Vf32 x) { return x.sqrt_approx (); },
		"sqrt_approx (Vf32)", 0, 4
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return sqrt (x); },
		"std::sqrt (double)", 0, 4
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sqrt (x); },
		[] (float x) { return fstb::Approx::sqrt_crude (x); },
		[] (Vf32 x) { return fstb::Approx::sqrt_crude (x); },
		"sqrt_crude", 0, 4.0, { { 0, 0.035 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sqrt (x); },
		[] (double x) { return fstb::Approx::sqrt_crude (x); },
		"sqrt_crude", 0, 4.0, { { 0, 0.035 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sqrt (x); },
		[] (float x) { return fstb::Approx::sqrt_crude2 (x); },
		[] (Vf32 x) { return fstb::Approx::sqrt_crude2 (x); },
		"sqrt_crude2", 0x1.0p-125f, 4.0, { { 0, 0.030 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sqrt (x); },
		[] (double x) { return fstb::Approx::sqrt_crude2 (x); },
		"sqrt_crude2", 0x1.0p-1021, 4.0, { { 0, 0.030 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sqrt (x); },
		[] (float x) { return fstb::Approx::sqrt (x); },
		[] (Vf32 x) { return fstb::Approx::sqrt (x); },
		"sqrt", 0x1.0p-121f, 4.0, { { 0, 1e-3 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sqrt (x); },
		[] (double x) { return fstb::Approx::sqrt (x); },
		"sqrt", 0x1.0p-1017, 4.0, { { 0, 1e-3 }, true }
	);

	// Misc

	// Use sparse mode only because of the mismatch between "to nearest
	// integer" and "to nearest even integer".
	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return round (x); },
		[] (Vf32 x) { return round (x); },
		"round", -10.0, 10.0, { { 1e-6, 0.0 }, false }
	);

	printf ("Done.\n");

	return ret_val;
}


/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
