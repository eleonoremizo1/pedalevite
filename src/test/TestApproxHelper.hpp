/*****************************************************************************

        TestApproxHelper.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestApproxHelper_CODEHEADER_INCLUDED)
#define TestApproxHelper_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "fstb/AllocAlign.h"
#include "fstb/bit_cast.h"
#include "fstb/ToolsSimd.h"
#include "test/Benchmark.h"
#include "test/BufferFiller.h"

#include <algorithm>
#include <limits>
#include <type_traits>

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Class defined only for float and double
template <typename T>
class FltRange { };

template <>
class FltRange <float>
{
public:
	static constexpr int       _mbits     = 23;
	static constexpr uint32_t  _signmask  = 1u << 31;
	static constexpr uint32_t  _clrmmask  = ~((1u << _mbits) - 1u);
	static constexpr uint32_t  _expmask   = _clrmmask ^ _signmask;
	static constexpr uint32_t  _lores_thr = 60 << _mbits; // 2^(60-127) ~ 0.68e-20

	static constexpr int       _mbitsq    = 10;
	static constexpr uint32_t  _mqmask    = ~((1u << (_mbits - _mbitsq)) - 1u);
	static constexpr uint32_t  _q_thr     = 117 << _mbits; // 2^(114-127) ~ 0.98e-3

	// Computes the next floating point value to be tested
	static inline float inc (float val) noexcept
	{
		const bool     neg_flag = (val < 0);
		const int32_t  inc      = (neg_flag) ? -1 : 1;

		// Accesses the binary structure of the floating point number
		auto           v_i = fstb::bit_cast <uint32_t> (val);

		// If we're below the threshold in absolute value, use only powers of 2
		// This also ensure no denormal is issued here.
		const auto     i_exp = v_i & _expmask;
		if (i_exp < _lores_thr || (neg_flag && i_exp == _lores_thr))
		{
			v_i = (v_i & _clrmmask) + (inc << _mbits);
		}
		else
		{
			v_i += inc;
		}

		// If we reach -0, turns it into +0
		if (v_i == _signmask)
		{
			v_i = 0;
		}

		// Back to float
		val = fstb::bit_cast <float> (v_i);

		return val;
	}

	// Returns the density associated with the given value.
	// This helps the integration step (to compute average error).
	static inline double get_weight (float val) noexcept
	{
		auto           v_i = fstb::bit_cast <uint32_t> (val);
		v_i = std::max (v_i & _expmask, _lores_thr);
		val = fstb::bit_cast <float> (v_i);

		return double (val);
	}

	// Quantize the mantissa to reduce the number of possible values.
	// The exponent of low values is also clipped (but 0 is left alone).
	static inline float quantize (float val) noexcept
	{
		auto           v_i = fstb::bit_cast <uint32_t> (val);
		v_i &= _mqmask;
		if ((v_i & _expmask) < _q_thr)
		{
			const auto     sign = v_i & _signmask;
			if ((v_i ^ sign) == 0)
			{
				v_i = 0;
			}
			else
			{
				v_i = sign | _q_thr;
			}
		}
		val = fstb::bit_cast <float> (v_i);

		return val;
	}
};

template <>
class FltRange <double>
{
public:
	static constexpr int       _mbits     = 52;
	static constexpr uint64_t  _signmask  = 1ull << 63;
	static constexpr uint64_t  _clrmmask  = ~((1ull << _mbits) - 1ull);
	static constexpr uint64_t  _expmask   = _clrmmask ^ _signmask;
	static constexpr uint64_t  _lores_thr = 956ull << _mbits; // 2^(956-1023) ~ 0.68e-20

	static inline double inc (double val) noexcept
	{
		const bool     neg_flag = (val < 0);

		// Accesses the binary structure of the floating point number
		auto           v_i = fstb::bit_cast <uint64_t> (val);

		// If we're below the threshold in absolute value, use only powers of 2
		// This also ensure no denormal is issued here.
		const uint64_t i_exp = v_i & _expmask;
		if (i_exp < _lores_thr || (neg_flag && i_exp == _lores_thr))
		{
			const int64_t  inc  = (neg_flag) ? -1 : 1;
			v_i = (v_i & _clrmmask) + (inc << _mbits);
		}
		else
		{
			// Trying to travel the whole mantissa range would be much too long.
			// Roughly a 4x better resolution than for float
			// 52 - 23 - 2 = 27, 2^27 = 0x08000000
			constexpr int64_t step = 0x07654321;
			const int64_t     inc  = (neg_flag) ? -step : step;
			v_i += inc;
		}

		// If we reach -0, turns it into +0
		if (v_i == _signmask)
		{
			v_i = 0;
		}

		// Back to float
		val = fstb::bit_cast <double> (v_i);

		return val;
	}

	static inline double get_weight (double val) noexcept
	{
		auto           v_i = fstb::bit_cast <uint64_t> (val);
		v_i = std::max (v_i & _expmask, _lores_thr);
		val = fstb::bit_cast <double> (v_i);

		return val;
	}

	static inline float quantize (double val) noexcept
	{
		return FltRange <float>::quantize (float (val));
	}
};



template <typename T, typename S>
static inline T conv_s_to_t (S x) { return static_cast <T> (x); }

template <>
inline fstb::Vf32 conv_s_to_t (float x)
{
	return fstb::Vf32 (x);
}



template <typename S, typename T>
static inline S conv_t_to_s (T x) { return static_cast <S> (x); }

template <>
inline float conv_t_to_s (fstb::Vf32 x)
{
	return x.template extract <0> ();
}



template <typename S, typename T>
static inline S sum_all_s (T x) { return static_cast <S> (x); }

template <>
inline float sum_all_s (fstb::Vf32 x)
{
	return x.sum_h ();
}



template <typename T, typename S>
static inline S round_to_t (S x)
{
	return conv_t_to_s <S> (conv_s_to_t <T> (x));
}



template <typename T>
static inline bool check_all_elt_same (T x) { fstb::unused (x); return true; }

template <>
inline bool check_all_elt_same (fstb::Vf32 x)
{
	return bool (x.template spread <0> () == x);
}



template <typename T>
struct ToScalar {	typedef T Type; };
template <>
struct ToScalar <fstb::Vf32> { typedef fstb::Vf32::Scalar Type; };



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	TestApproxHelper::TestSpec::is_checked (ErrType type) const noexcept
{
	return (_tol_arr [size_t (type)] > 0);
}



bool	TestApproxHelper::TestSpec::has_custom_plot_range () const noexcept
{
	return (_plot_range [0] != _plot_range [1]);
}



template <typename T, int ILL2>
template <typename OP>
void	TestApproxHelper::TestFncSpeed <T, ILL2>::test_op1 (const OP &op, const std::string &name, double min_val, double max_val)
{
	typedef typename ToScalar <T>::Type S;

	constexpr int  nbr_tests   = 100;
	constexpr int  block_len_s = 1 << 16;
	constexpr int  s_per_t     = sizeof (T) / sizeof (S);
	static_assert ((block_len_s % s_per_t) == 0, "");
	constexpr int  block_len   = block_len_s / s_per_t;
	constexpr int  interleave  = 1 << ILL2;
	static_assert ((block_len % interleave) == 0, "");

	typedef std::vector <T, fstb::AllocAlign <T, 64> > Buffer;

	Buffer         src_arr (block_len);
	Buffer         dst_arr (block_len);

	printf ("Speed test %s, interleave %d... ", name.c_str (), interleave);
	fflush (stdout);
	Benchmark      ben;
	auto           dummy_val = S (0);

	for (int test_cnt = 0; test_cnt < nbr_tests; ++test_cnt)
	{
		BufferFiller::gen_rnd_scaled (
			reinterpret_cast <S *> (&src_arr [0]), block_len_s,
			S (min_val), S (max_val), test_cnt
		);

		ben.start_single ();

		for (int pos = 0; pos < block_len; pos += interleave)
		{
#if defined (_MSC_VER) && (_MSC_VER < 1930) && (fstb_WORD_SIZE == 64)
			// VS2017 in 64-bit mode breaks on a C4789 error...
			T              a [8];
#else
			T              a [interleave];
#endif

			if constexpr (true    ) { a [0] = src_arr [pos    ]; }
			if constexpr (ILL2 > 0) { a [1] = src_arr [pos + 1]; }
			if constexpr (ILL2 > 1) { a [2] = src_arr [pos + 2]; }
			if constexpr (ILL2 > 1) { a [3] = src_arr [pos + 3]; }
			if constexpr (ILL2 > 2) { a [4] = src_arr [pos + 4]; }
			if constexpr (ILL2 > 2) { a [5] = src_arr [pos + 5]; }
			if constexpr (ILL2 > 2) { a [6] = src_arr [pos + 6]; }
			if constexpr (ILL2 > 2) { a [7] = src_arr [pos + 7]; }

			if constexpr (true    ) { dst_arr [pos    ] = op (a [0]); }
			if constexpr (ILL2 > 0) { dst_arr [pos + 1] = op (a [1]); }
			if constexpr (ILL2 > 1) { dst_arr [pos + 2] = op (a [2]); }
			if constexpr (ILL2 > 1) { dst_arr [pos + 3] = op (a [3]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 4] = op (a [4]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 5] = op (a [5]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 6] = op (a [6]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 7] = op (a [7]); }
		}

		ben.stop_single ();

		T sum { 0 };
		for (int pos = 0; pos < block_len; ++pos)
		{
			sum += dst_arr [pos];
		}
		dummy_val += sum_all_s <S> (sum);
	}

	const auto     spl_per_s = ben.compute_rate (block_len_s, dummy_val);
	const double   mega_sps  = spl_per_s / 1'000'000.0;
	printf ("%12.3f Mop/s.\n", mega_sps);
}



template <typename T, int ILL2>
template <typename OP>
void	TestApproxHelper::TestFncSpeed <T, ILL2>::test_op2 (const OP &op, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2)
{
	typedef typename ToScalar <T>::Type S;

	constexpr int  nbr_tests   = 100;
	constexpr int  block_len_s = 1 << 16;
	constexpr int  s_per_t     = sizeof (T) / sizeof (S);
	static_assert ((block_len_s % s_per_t) == 0, "");
	constexpr int  block_len   = block_len_s / s_per_t;
	constexpr int  interleave  = 1 << ILL2;
	static_assert ((block_len % interleave) == 0, "");

	typedef std::vector <T, fstb::AllocAlign <T, 64> > Buffer;

	Buffer          src1_arr (block_len);
	Buffer          src2_arr (block_len);
	Buffer          dst_arr (block_len);

	printf ("Speed test %s, interleave %d... ", name.c_str (), interleave);
	fflush (stdout);
	Benchmark      ben;
	auto           dummy_val = S (0);

	for (int test_cnt = 0; test_cnt < nbr_tests; ++test_cnt)
	{
		BufferFiller::gen_rnd_scaled (
			reinterpret_cast <S *> (&src1_arr [0]), block_len_s,
			S (min_val1), S (max_val1), test_cnt * 2
		);
		BufferFiller::gen_rnd_scaled (
			reinterpret_cast <S *> (&src2_arr [0]), block_len_s,
			S (min_val2), S (max_val2), test_cnt * 2 + 1
		);

		ben.start_single ();

		for (int pos = 0; pos < block_len; pos += interleave)
		{
#if defined (_MSC_VER) && (_MSC_VER < 1930) && (fstb_WORD_SIZE == 64)
			// VS2017 in 64-bit mode breaks on a C4789 error...
			T              a [8];
			T              b [8];
#else
			T              a [interleave];
			T              b [interleave];
#endif

			if constexpr (true    ) { a [0] = src1_arr [pos    ]; b [0] = src2_arr [pos    ]; }
			if constexpr (ILL2 > 0) { a [1] = src1_arr [pos + 1]; b [1] = src2_arr [pos + 1]; }
			if constexpr (ILL2 > 1) { a [2] = src1_arr [pos + 2]; b [2] = src2_arr [pos + 2]; }
			if constexpr (ILL2 > 1) { a [3] = src1_arr [pos + 3]; b [3] = src2_arr [pos + 3]; }
			if constexpr (ILL2 > 2) { a [4] = src1_arr [pos + 4]; b [4] = src2_arr [pos + 4]; }
			if constexpr (ILL2 > 2) { a [5] = src1_arr [pos + 5]; b [5] = src2_arr [pos + 5]; }
			if constexpr (ILL2 > 2) { a [6] = src1_arr [pos + 6]; b [6] = src2_arr [pos + 6]; }
			if constexpr (ILL2 > 2) { a [7] = src1_arr [pos + 7]; b [7] = src2_arr [pos + 7]; }

			if constexpr (true    ) { dst_arr [pos    ] = op (a [0], b [0]); }
			if constexpr (ILL2 > 0) { dst_arr [pos + 1] = op (a [1], b [1]); }
			if constexpr (ILL2 > 1) { dst_arr [pos + 2] = op (a [2], b [2]); }
			if constexpr (ILL2 > 1) { dst_arr [pos + 3] = op (a [3], b [3]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 4] = op (a [4], b [4]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 5] = op (a [5], b [5]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 6] = op (a [6], b [6]); }
			if constexpr (ILL2 > 2) { dst_arr [pos + 7] = op (a [7], b [7]); }
		}

		ben.stop_single ();

		T sum { 0 };
		for (int pos = 0; pos < block_len; ++pos)
		{
			sum += dst_arr [pos];
		}
		dummy_val += sum_all_s <S> (sum);
	}

	const auto     spl_per_s = ben.compute_rate (block_len_s, dummy_val);
	const double   mega_sps  = spl_per_s / 1'000'000.0;
	printf ("%12.3f Mop/s.\n", mega_sps);
}



template <typename OPREF, typename OPTSTS, typename OPTSTV>
void	TestApproxHelper::test_op1_all_flt (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const OPTSTV &op_v, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec)
{
	test_op1_all_s <float> (
		ret_val, op_ref, op_s, name, min_val, max_val, tst_spec
	);
	test_op1_all_flt_v (
		ret_val, op_ref, op_v, name, min_val, max_val, tst_spec
	);
}



template <typename T, typename OPREF, typename OPTSTS>
void	TestApproxHelper::test_op1_all_s (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec)
{
	auto           name_s = name + " (";
	name_s +=
		  std::is_same <T, float >::value ? "float"
		: std::is_same <T, double>::value ? "double"
		:                                   "\?\?\?";
	name_s += ")";
	TestFncLogic <T>::test_op1 (
		ret_val, op_ref, op_s, name_s, min_val, max_val, tst_spec
	);
	if (ret_val == 0)
	{
		TestFncSpeed <T, 0>::test_op1 (
			op_s, name_s, min_val, max_val
		);
		TestFncSpeed <T, 1>::test_op1 (
			op_s, name_s, min_val, max_val
		);
		TestFncSpeed <T, 2>::test_op1 (
			op_s, name_s, min_val, max_val
		);
	}
}



template <typename OPREF, typename OPTSTV>
void	TestApproxHelper::test_op1_all_flt_v (int &ret_val, const OPREF &op_ref, const OPTSTV &op_v, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec)
{
	typedef fstb::Vf32 Vf32;

	const auto     name_v = name + " (Vf32)";
	TestFncLogic <Vf32>::test_op1 (
		ret_val, op_ref, op_v, name_v, min_val, max_val, tst_spec
	);
	if (ret_val == 0)
	{
		TestFncSpeed <Vf32, 0>::test_op1 (
			op_v, name_v, min_val, max_val
		);
		TestFncSpeed <Vf32, 1>::test_op1 (
			op_v, name_v, min_val, max_val
		);
		TestFncSpeed <Vf32, 2>::test_op1 (
			op_v, name_v, min_val, max_val
		);
	}
}



template <typename OPREF, typename OPTSTS, typename OPTSTV>
void	TestApproxHelper::test_op2_all_flt (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const OPTSTV &op_v, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec)
{
	test_op2_all_s <float> (
		ret_val, op_ref, op_s, name,
		min_val1, max_val1, min_val2, max_val2,
		tst_spec
	);
	test_op2_all_flt_v (
		ret_val, op_ref, op_v, name,
		min_val1, max_val1, min_val2, max_val2,
		tst_spec
	);
}



template <typename T, typename OPREF, typename OPTSTS>
void	TestApproxHelper::test_op2_all_s (int &ret_val, const OPREF &op_ref, const OPTSTS &op_s, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec)
{
	auto           name_s = name + " (";
	name_s +=
		  std::is_same <T, float >::value ? "float"
		: std::is_same <T, double>::value ? "double"
		:                                   "\?\?\?";
	name_s += ")";
	TestFncLogic <T>::test_op2 (
		ret_val, op_ref, op_s, name_s,
		min_val1, max_val1, min_val2, max_val2,
		tst_spec
	);
	if (ret_val == 0)
	{
		TestFncSpeed <T, 0>::test_op2 (
			op_s, name_s, min_val1, max_val1, min_val2, max_val2
		);
		TestFncSpeed <T, 1>::test_op2 (
			op_s, name_s, min_val1, max_val1, min_val2, max_val2
		);
		TestFncSpeed <T, 2>::test_op2 (
			op_s, name_s, min_val1, max_val1, min_val2, max_val2
		);
	}
}



template <typename OPREF, typename OPTSTV>
void	TestApproxHelper::test_op2_all_flt_v (int &ret_val, const OPREF &op_ref, const OPTSTV &op_v, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec)
{
	typedef fstb::Vf32 Vf32;

	const auto     name_v = name + " (Vf32)";
	TestFncLogic <Vf32>::test_op2 (
		ret_val, op_ref, op_v, name_v,
		min_val1, max_val1, min_val2, max_val2,
		tst_spec
	);
	if (ret_val == 0)
	{
		TestFncSpeed <Vf32, 0>::test_op2 (
			op_v, name_v, min_val1, max_val1, min_val2, max_val2
		);
		TestFncSpeed <Vf32, 1>::test_op2 (
			op_v, name_v, min_val1, max_val1, min_val2, max_val2
		);
		TestFncSpeed <Vf32, 2>::test_op2 (
			op_v, name_v, min_val1, max_val1, min_val2, max_val2
		);
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <size_t N>
void	TestApproxHelper::ErrState <N>::accumulate (std::array <double, N> val, double err, double weight, const TestSpec &tst_spec) noexcept
{
	const auto     err_a = std::abs (err);
	_max  = std::max (_max, err_a);
	_tot += err_a * weight;
	_wgh += weight;

	if constexpr (N == 1)
	{
		if (   tst_spec.has_custom_plot_range ()
		    && (   val [0] < tst_spec._plot_range [0]
		        || val [0] > tst_spec._plot_range [1]))
		{
			return;
		}

		std::array <float, N> val_f;
		std::transform (
			val.begin (), val.end (), val_f.begin (),
			[] (double x) { return FltRange <double>::quantize (x); }
		);

		const auto     err_f = float (err);
		if (_cloud.empty () || _cloud.back ()._val != val_f)
		{
			_cloud.push_back (Range { val_f, err_f, err_f });
		}
		else
		{
			auto &         range = _cloud.back ();
			range._e_min = std::min (range._e_min, err_f);
			range._e_max = std::max (range._e_max, err_f);
		}
	}
}



template <typename T>
TestApproxHelper::ValIter <T>::ValIter (double val_beg, double val_end, int nbr_spl, bool fine_flag)
:	_val_beg (val_beg)
,	_val_end (val_end)
,	_nbr_spl (nbr_spl)
,	_fine_flag (fine_flag)
{
	assert (val_beg < val_end);
	assert (nbr_spl >= 2);

	_step = (_val_end - val_beg) / double (nbr_spl - 1);
	start ();
}



template <typename T>
void	TestApproxHelper::ValIter <T>::start () noexcept
{
	_pos     = 0;
	_val_cur = T (_val_beg);
	_weight  = _fine_flag ? FltRange <T>::get_weight (_val_cur) : 1;
}



template <typename T>
bool	TestApproxHelper::ValIter <T>::is_rem_elt () const noexcept
{
	return (_val_cur <= _val_end);
}



template <typename T>
TestApproxHelper::ValIter <T> &	TestApproxHelper::ValIter <T>::operator ++ () noexcept
{
	if (_fine_flag)
	{
		_val_cur = FltRange <T>::inc (_val_cur);
		_weight  = FltRange <T>::get_weight (_val_cur);
	}

	else
	{
		++ _pos;

		// Reach exactly _val_end at the last iteration, then set a much larger
		// value to make sure we quit the loop.
		if (_val_cur >= _val_end)
		{
			_val_cur = T (fabs (_val_cur)) * 2 + 1;
		}
		else
		{
			const double   val = _val_beg + _pos * _step;
			_val_cur = T (std::min (val, _val_end));
		}
	}

	return *this;
}



template <typename T>
T	TestApproxHelper::ValIter <T>::operator * () const noexcept
{
	return _val_cur;
}



template <typename T>
double	TestApproxHelper::ValIter <T>::get_weight () const noexcept
{
	return _weight;
}



// Returns true if everything is O.K.
template <typename T, size_t N>
bool	TestApproxHelper::check_val (T val_tst_nat, T val_ref_nat, double val_ref_dbl, std::array <double, N> val_src_arr, double weight, ErrStateMap <N> &e_map, const TestSpec &tst_spec) noexcept
{
	const auto     val_tst_dbl = double (val_tst_nat);
	// Absolute error
	double         err_val = val_tst_dbl - val_ref_dbl;
	auto &         e_abs   = e_map [ErrDataType::_abs];
	e_abs.accumulate (val_src_arr, err_val, weight, tst_spec);

	// Relative error
	// We check it only if both values are non-zero and of the same sign,
	// otherwise relative error has no meaning. Exception when both are 0.
	if (tst_spec.is_checked (ErrType::_rel))
	{
		if (   (      val_ref_nat == 0 && val_tst_nat == 0)
		    || (   ! (val_ref_nat <= 0 && val_tst_nat >= 0)
	           && ! (val_ref_nat >= 0 && val_tst_nat <= 0)))
		{
			if (val_ref_dbl != 0)
			{
				err_val /= val_ref_dbl;
			}
			else
			{
				assert (err_val == 0);
			}
			auto &         e_rel = e_map [ErrDataType::_rel];
			e_rel.accumulate (val_src_arr, err_val, weight, tst_spec);

			float          err_ulp = 0;
			if constexpr (false)
			{
				// Converts ULPs to epsilon
				const auto     d_ulp =
					fstb::calc_dist_ulp (val_tst_nat, val_ref_nat);
				err_ulp = float (d_ulp * std::numeric_limits <T>::epsilon ());
			}
			else
			{
				// Converts ULPs to relative error
				if (val_ref_nat != 0)
				{
					err_ulp = float (
						  (double (val_tst_nat) - double (val_ref_nat))
						/ double (val_ref_nat)
					);
				}
			}
			auto &         e_ulp = e_map [ErrDataType::_ulp];
			e_ulp.accumulate (val_src_arr, err_ulp, weight, tst_spec);
		}
	}

	// Check
	for (int type_idx = 0; type_idx < int (ErrType::_nbr_elt); ++type_idx)
	{
		const auto     type = ErrType (type_idx);
		if (tst_spec.is_checked (type))
		{
			const auto     tol       = tst_spec._tol_arr [type_idx];
			const auto     data_type = ErrDataType (type_idx);
			const auto &   err       = e_map [data_type];

			if (err._max > tol)
			{
				printf (
					"\n*** Error: too large %.3s error %.3g. Input: ",
					_err_type_0_arr [type_idx], err._max
				);
				if (N == 1)
				{
					printf ("%.9g", val_src_arr.front ());
				}
				else
				{
					printf ("(");
					const char *      sep_0 = "";
					for (const auto val_src : val_src_arr)
					{
						printf ("%s%.9g", sep_0, val_src);
						sep_0 = ", ";
					}
					printf (")");
				}
				printf (
					", expected: %.9g, output: %.9g.\n", val_ref_dbl, val_tst_dbl
				);
				return false;
			}
		}
	}

	return true;
}



template <size_t N>
void	TestApproxHelper::print_results (ErrStateMap <N> &e_map, const TestSpec &tst_spec) noexcept
{
	for (int type_idx = 0; type_idx < int (ErrType::_nbr_elt); ++type_idx)
	{
		const auto     type = ErrType (type_idx);
		if (tst_spec.is_checked (type))
		{
			const auto     data_type = ErrDataType (type_idx);
			const auto &   err       = e_map [data_type];
			const double   err_avg   = err._tot / err._wgh;
			printf (
				" Err %.3s max: %.3g, avg: %.3g",
				_err_type_0_arr [type_idx],
				err._max, err_avg
			);
		}
	}

	printf ("\n");
}



// OPREF is always scalar with double as input and output types.
template <typename T>
template <typename OPTST, typename OPREF>
void	TestApproxHelper::TestFncLogic <T>::test_op1 (int &ret_val, const OPREF &op_ref, const OPTST &op_tst, const std::string &name, double min_val, double max_val, const TestSpec &tst_spec)
{
	typedef typename ToScalar <T>::Type S;

	if (ret_val != 0)
	{
		return;
	}

	printf ("Logic test %s... ", name.c_str ());
	fflush (stdout);

	const bool     accurate_flag =
#if ! defined (NDEBUG) || fstb_ARCHI == fstb_ARCHI_ARM
		// This is too slow for debug mode and embedded processors
		false;
#else
		tst_spec._accurate_flag;
#endif // NDEBUG fstb_ARCHI_ARM

	constexpr int  nbr_spl = 234567;
	ValIter <S>    it { min_val, max_val, nbr_spl, accurate_flag };
	ErrStateMap<1> e_map;
	for (it.start (); it.is_rem_elt (); ++it)
	{
		const T        src_t  = conv_s_to_t <T> (*it);
		const double   weight = it.get_weight ();

		// src_t may have a lower resolution than val_src, so we make sure
		// val_src matches the src_t value for reference calculations.
		const auto     val_src     = conv_t_to_s <S> (src_t);
		const auto     val_src_dbl = double (val_src);

		double         val_ref_dbl = op_ref (val_src_dbl);
		const auto     val_ref     = S (val_ref_dbl);

#if 0
		// Matches val_ref with the tested type. This would cancel any error
		// caused by the lower resolution of the target type, which is
		// impossible to cancel otherwise.
		// However this could worsen the results because rounding errors may
		// accumulate instead of cancel each others.
		// Observed effect: better average error, but worse maximum error
		val_ref_dbl = double (round_to_t <T> (S (val_ref_dbl)));
#endif

		const T        dst_t = op_tst (src_t);
		if (! check_all_elt_same (dst_t))
		{
			printf ("\n*** Error: different vector elements.\n");
			ret_val = -1;
			return;
		}
		const auto     val_tst = conv_t_to_s <S> (dst_t);

		const bool     ok_flag = check_val (
			val_tst, val_ref, val_ref_dbl,
			std::array <double, 1> { val_src },
			weight, e_map, tst_spec
		);
		if (! ok_flag)
		{
			ret_val = -1;
			return;
		}
	}

	// Trashes the ULP detailed results if the relative error is too big
	if (tst_spec.is_checked (ErrType::_rel))
	{
		constexpr auto limit_n = 12;
		constexpr auto limit_e = limit_n * std::numeric_limits <S>::epsilon ();
		const auto     rel_max = e_map [ErrDataType::_rel]._max;
		auto           it_ulp  = e_map.find (ErrDataType::_ulp);
		if (rel_max > limit_e && it_ulp != e_map.end ())
		{
			e_map.erase (it_ulp);
		}
	}

	print_results (e_map, tst_spec);

	for (auto &p : e_map)
	{
		auto &         err_state = p.second;
		if (! err_state._cloud.empty ())
		{
			requantize_results (err_state._cloud);
		}
	}
	draw_errors_to_files (name, e_map, tst_spec);
}



template <typename T>
template <typename OPTST, typename OPREF>
void	TestApproxHelper::TestFncLogic <T>::test_op2 (int &ret_val, const OPREF &op_ref, const OPTST &op_tst, const std::string &name, double min_val1, double max_val1, double min_val2, double max_val2, const TestSpec &tst_spec)
{
	assert (! tst_spec.has_custom_plot_range ());

	typedef typename ToScalar <T>::Type S;

	if (ret_val != 0)
	{
		return;
	}

	printf ("Logic test %s...", name.c_str ());
	fflush (stdout);

	constexpr int  nbr_spl =
#if defined (NDEBUG)
		12345;
#else
		2345;
#endif
	ValIter <S>    it1 { min_val1, max_val1, nbr_spl, false };
	ValIter <S>    it2 { min_val2, max_val2, nbr_spl, false };
	ErrStateMap<2> e_map;
	for (it1.start (); it1.is_rem_elt (); ++it1)
	{
		const T        src_t1       = conv_s_to_t <T> (*it1);

		// src_t may have a lower resolution than val_src, so we make sure
		// val_src matches the src_t value for reference calculations.
		const auto     val_src1     = conv_t_to_s <S> (src_t1);
		const auto     val_src1_dbl = double (val_src1);

		for (it2.start (); it2.is_rem_elt (); ++it2)
		{
			const T        src_t2       = conv_s_to_t <T> (*it2);
			const auto     val_src2     = conv_t_to_s <S> (src_t2);
			const auto     val_src2_dbl = double (val_src2);

			double         val_ref_dbl  = op_ref (val_src1_dbl, val_src2_dbl);
			const auto     val_ref      = S (val_ref_dbl);

#if 0
			// Matches val_ref with the tested type. This would cancel any error
			// caused by the lower resolution of the target type, which is
			// impossible to cancel otherwise.
			// However this could worsen the results because rounding errors may
			// accumulate instead of cancel each others.
			// Observed effect: better average error, but worse maximum error
			val_ref_dbl = double (round_to_t <T> (S (val_ref_dbl)));
#endif

			const T        dst_t = op_tst (src_t1, src_t2);
			if (! check_all_elt_same (dst_t))
			{
				printf ("\n*** Error: different vector elements.\n");
				ret_val = -1;
				return;
			}
			const auto     val_tst = conv_t_to_s <S> (dst_t);

			const bool     ok_flag = check_val (
				val_tst, val_ref, val_ref_dbl,
				std::array <double, 2> { val_src1, val_src2 },
				1, e_map, tst_spec
			);
			if (! ok_flag)
			{
				ret_val = -1;
				return;
			}
		}
	}

	print_results (e_map, tst_spec);
}



#endif // TestApproxHelper_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
