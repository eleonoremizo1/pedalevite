/*****************************************************************************

        I2sClientCbInterface.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (I2sClientCbInterface_HEADER_INCLUDED)
#define I2sClientCbInterface_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "test/I2sPin.h"

#include <array>

#include <cstdint>



class I2sClientCbInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_chn = 2;

	typedef std::array <int32_t, _nbr_chn> SampleFrame;

	               I2sClientCbInterface ()  = default;
	virtual        ~I2sClientCbInterface () = default;

	void           proc_audio (SampleFrame &data_bidir);
	void           set_pin (I2sPin p, bool state_flag);
	void           notify_clock_error ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual void   do_proc_audio (SampleFrame &data_bidir) = 0;
	virtual void   do_set_pin (I2sPin p, bool state_flag) = 0;
	virtual void   do_notify_clock_error () = 0;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               I2sClientCbInterface (const I2sClientCbInterface &other) = delete;
	               I2sClientCbInterface (I2sClientCbInterface &&other)      = delete;
	I2sClientCbInterface &
	               operator = (const I2sClientCbInterface &other) = delete;
	I2sClientCbInterface &
	               operator = (I2sClientCbInterface &&other)      = delete;

}; // class I2sClientCbInterface



//#include "test/I2sClientCbInterface.hpp"



#endif   // I2sClientCbInterface_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
