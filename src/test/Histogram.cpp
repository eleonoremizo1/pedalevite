/*****************************************************************************

        Histogram.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "test/Histogram.h"

#include <array>

#include <cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: reset
Description:
	Initialises the histogram. Data set is empty.
Input parameters:
	- v_min: value <= to any value to be entered
	- v_max: value >= to any value to be entered. v_min < v_max
	- sz: number of segments in the histogram. >= 2
		Corresponds to the number of text columns for ASCII display.
	- name: title of the histogram.
Throws: std::vector related exception
==============================================================================
*/

void	Histogram::reset (double v_min, double v_max, int sz, std::string name)
{
	assert (v_min < v_max);
	assert (sz >= 2);

	_v_min = v_min;
	_v_max = v_max;
	_sz    = sz;
	_scale = compute_scale ();

	_hist.clear ();
	_hist.resize (_sz, 0);

	_name = name;
}



/*
==============================================================================
Name: insert
Description:
	Add a new value to the histogram data set
Input parameters:
	- val: value to add. Should be in the range specified during reset(),
		other the value is clipped.
==============================================================================
*/

void	Histogram::insert (double val) noexcept
{
	assert (val >= _v_min);
	assert (val <= _v_max);

	val = fstb::limit (val, _v_min, _v_max);
	const auto     pos_f = (val - _v_min) * _scale;
	const auto     pos_i = fstb::round_int (pos_f);
	assert (pos_i >= 0);
	assert (pos_i < int (_hist.size ()));
	++ _hist [pos_i];
}



/*
==============================================================================
Name: print
Description:
	Generates a string containing an ASCII-art version of the histogram, ready
	to be printed.
Input parameters:
	- h: height of the histogram, in character lines. > 0.
	- log_flag: indicates that the bars are displayed in vertical log scale.
Returns:
	The string to be displayed. Encoding depends on the platform.
Throws: std::string related exceptions
==============================================================================
*/

std::string	Histogram::print (int h, bool log_flag) const
{
	assert (! _hist.empty ());
	assert (h > 0);

	std::string    str;

	const std::array <const char *, 4> char_map =
	{
#if fstb_SYS == fstb_SYS_WIN
		// CP 850
		" ", "_", "\xDC", "\xDB"
#elif fstb_SYS == fstb_SYS_LINUX
		// UTF-8 (U+2584, U+2588)
		" ", "_", "\xE2\x96\x84", "\xE2\x96\x88"
#else
		" ", "_", "w", "#"
#endif
	};

	const int      n_max  = std::max (
		*std::max_element (_hist.begin (), _hist.end ()),
		1
	);
	const int      dh   = (h + 1) << 1;
	const double   mult =
		  (log_flag)
		? dh / log (double (n_max + 1))
		: dh / double (n_max + 1);
	for (int y = h; y >= 0; --y)
	{
		for (int x = 0; x < _sz; ++x)
		{
			auto           v     = double (_hist [x]);
			if (log_flag)
			{
				v = log (v + 1);
			}
			const auto     bar_h = v * mult;
			const auto     c     =
				  (v == 0 && y == 0  ) ? char_map [1]
				: (bar_h >= y * 2 + 1) ? char_map [3]
				: (bar_h >= y * 2    ) ? char_map [2]
				:                        char_map [0];
			str += c;
		}
		str += "\n";
	}

	str += _name + ", range: ";
	char           tmp_0 [1023+1];
	fstb::snprintf4all (tmp_0, sizeof (tmp_0),
		"[%f ; %f]\n",
		_v_min, _v_max
	);
	str += tmp_0;

	return str;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



double	Histogram::compute_scale () const noexcept
{
	assert (_v_min < _v_max);

	return (_sz - 1) / (_v_max - _v_min);
}



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
