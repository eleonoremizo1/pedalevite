/*****************************************************************************

        GenSig.h
        Author: Laurent de Soras, 2024

Functions to quickly generate various test signals.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (GenSig_HEADER_INCLUDED)
#define GenSig_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <vector>



class GenSig
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     gen_sample (double &sample_freq, std::vector <std::vector <float> > &chn_arr);
	static int     gen_sine_decay (double &sample_freq, std::vector <std::vector <float> > &chn_arr);
	static int     gen_noise_w (double &sample_freq, std::vector <std::vector <float> > &chn_arr);
	static int     gen_spikes (double &sample_freq, std::vector <std::vector <float> > &chn_arr, double duration, double spike_len);
	static int     gen_short_sine (double &sample_freq, std::vector <std::vector <float> > &chn_arr, double duration, double sine_duration, double freq, double vol, double fade);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               GenSig ()                               = delete;
	               GenSig (const GenSig &other)            = delete;
	               GenSig (GenSig &&other)                 = delete;
	GenSig &       operator = (const GenSig &other)        = delete;
	GenSig &       operator = (GenSig &&other)             = delete;
	bool           operator == (const GenSig &other) const = delete;
	bool           operator != (const GenSig &other) const = delete;

}; // class GenSig



//#include "test/GenSig.hpp"



#endif // GenSig_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
