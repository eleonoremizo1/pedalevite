/*****************************************************************************

        TestDsmMgr.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (TestDsmMgr_HEADER_INCLUDED)
#define TestDsmMgr_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



class TestDsmMgr
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     perform_test ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class Segment
	{
	public:
		uint32_t        _duration  = 0;
		float           _val       = 0;
		bool            _ramp_flag = false;
	};

	static float   gen_norm_trunc (uint32_t rnd_state) noexcept;
	static float   gen_log_norm (uint32_t rnd_state, float mu_log, float sigma) noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ~TestDsmMgr ()                              = delete;
	               TestDsmMgr ()                               = delete;
	               TestDsmMgr (const TestDsmMgr &other)        = delete;
	               TestDsmMgr (TestDsmMgr &&other)             = delete;
	TestDsmMgr &   operator = (const TestDsmMgr &other)        = delete;
	TestDsmMgr &   operator = (TestDsmMgr &&other)             = delete;
	bool           operator == (const TestDsmMgr &other) const = delete;
	bool           operator != (const TestDsmMgr &other) const = delete;

}; // class TestDsmMgr



//#include "test/TestDsmMgr.hpp"



#endif   // TestDsmMgr_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
