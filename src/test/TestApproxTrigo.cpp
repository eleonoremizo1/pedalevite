/*****************************************************************************

        TestApproxTrigo.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Approx.h"
#include "test/TestApproxHelper.h"
#include "test/TestApproxTrigo.h"

#include <cassert>
#include <cmath>
#include <cstdio>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	TestApproxTrigo::perform_test ()
{
	int            ret_val = 0;

	printf ("Testing fstb::Approx (trigo)...\n");

	typedef fstb::Vf32 Vf32;
	typedef TestApproxHelper Helper;
	constexpr auto notol = Helper::_notol;
	constexpr auto pi    = fstb::PI;

	// sin

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return sinf (x); },
		"std::sin (float)", -pi, pi
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return sin (x); },
		"std::sin (double)", -pi, pi
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (float x) { return fstb::Approx::sin_lds_pi (x); },
		[] (Vf32 x) { return fstb::Approx::sin_lds_pi (x); },
		"sin_lds_pi", -1, 1, { { notol, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x); },
		[] (float x) { return fstb::Approx::sin_nick (x); },
		[] (Vf32 x) { return fstb::Approx::sin_nick (x); },
		"sin_nick", -pi, pi, { { 0.001, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return cos (x * (2 * pi)); },
		[] (float x) { return fstb::Approx::cos_sin_nick_2pi (x) [0]; },
		[] (Vf32 x) { return fstb::Approx::cos_sin_nick_2pi (x) [0]; },
		"cos_sin_nick_2pi cos", -0.5, 0.5, { { 1e-3, notol }, true }
	);
	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * (2 * pi)); },
		[] (float x) { return fstb::Approx::cos_sin_nick_2pi (x) [1]; },
		[] (Vf32 x) { return fstb::Approx::cos_sin_nick_2pi (x) [1]; },
		"cos_sin_nick_2pi sin", -0.5, 0.5, { { 1e-3, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x); },
		[] (float x) { return fstb::Approx::sin_rbj (x); },
		[] (Vf32 x) { return fstb::Approx::sin_rbj (x); },
		"sin_rbj", -0.5 * pi, 0.5 * pi, { { 2e-7, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sin (x); },
		[] (double x) { return fstb::Approx::sin_rbj (x); },
		"sin_rbj", -0.5 * pi, 0.5 * pi, { { 2.5e-8, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return cos (x); },
		[] (float x) { return fstb::Approx::cos_rbj (x); },
		[] (Vf32 x) { return fstb::Approx::cos_rbj (x); },
		"cos_rbj", -pi, pi, { { 2e-7, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return cos (x); },
		[] (double x) { return fstb::Approx::cos_rbj (x); },
		"cos_rbj", -pi, pi, { { 2.5e-8, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (float x) { return fstb::Approx::sin_rbj_pi (x); },
		[] (Vf32 x) { return fstb::Approx::sin_rbj_pi (x); },
		"sin_rbj_pi", -0.5, 1, { { 2.5e-7, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (double x) { return fstb::Approx::sin_rbj_pi (x); },
		"sin_rbj_pi", -0.5, 1, { { 0.001, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * (pi * 0.5f)); },
		[] (float x) { return fstb::Approx::sin_rbj_halfpi (x); },
		[] (Vf32 x) { return fstb::Approx::sin_rbj_halfpi (x); },
		"sin_rbj_halfpi", -1.0, 1.0, { { 2e-07, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sin (x * (pi * 0.5f)); },
		[] (double x) { return fstb::Approx::sin_rbj_halfpi (x); },
		"sin_rbj_halfpi", -1.0, 1.0, { { 2.5e-8, notol }, true }
	);

	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return cos (x); },
		[] (Vf32 x) {
			Vf32 c;
			Vf32 s;
			fstb::Approx::cos_sin_rbj (c, s, x);
			return c;
		},
		"cos_sin_rbj cos", -3 * pi, 3 * pi, { { 1e-6, notol }, true }
	);
	Helper::test_op1_all_flt_v (ret_val,
		[] (double x) { return sin (x); },
		[] (Vf32 x) {
			Vf32 c;
			Vf32 s;
			fstb::Approx::cos_sin_rbj (c, s, x);
			return s;
		},
		"cos_sin_rbj sin", -3 * pi, 3 * pi, { { 1e-6, notol }, true }
	);

	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return sin (x * 0.5 * pi); },
		[] (float x) { return (fstb::Approx::sin_rbj_halfpi_pi (x)) [0]; },
		"sin_rbj_halfpi_pi half", -0.5, 1, { { 2e-7, notol }, true }
	);
	Helper::test_op1_all_s <float> (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (float x) { return (fstb::Approx::sin_rbj_halfpi_pi (x)) [1]; },
		"sin_rbj_halfpi_pi full", -0.5, 1, { { 2e-7, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * (pi * 0.5f)); },
		[] (float x) { return fstb::Approx::sin_andy_halfpi (x); },
		[] (Vf32 x) { return fstb::Approx::sin_andy_halfpi (x); },
		"sin_andy_halfpi", -1.0, 1.0, { { 2e-07, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sin (x * (pi * 0.5f)); },
		[] (double x) { return fstb::Approx::sin_andy_halfpi (x); },
		"sin_andy_halfpi", -1.0, 1.0, { { 2.5e-8, notol }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (float x) { return fstb::Approx::sin_andy_pi (x); },
		[] (Vf32 x) { return fstb::Approx::sin_andy_pi (x); },
		"sin_andy_pi", -1, 1, { { 3e-7, notol }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return sin (x * pi); },
		[] (double x) { return fstb::Approx::sin_andy_pi (x); },
		"sin_andy_pi", -1, 1, { { 5e-9, notol }, true }
	);

	// tan, limited range

	Helper::TestFncSpeed <float, 0>::test_op1 (
		[] (float x) { return tanf (x); },
		"std::tan (float)", -1.0, 1.0
	);
	Helper::TestFncSpeed <double, 0>::test_op1 (
		[] (double x) { return tan (x); },
		"std::tan (double)", -1.0, 1.0
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_taylor5 (x); },
		[] (Vf32 x) { return fstb::Approx::tan_taylor5 (x); },
		"tan_taylor5", -pi/4, pi/4, { { 0, 0.014 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_taylor5 (x); },
		"tan_taylor5", -pi/4, pi/4, { { 0, 0.014 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_7 (x); },
		[] (Vf32 x) { return fstb::Approx::tan_7 (x); },
		"tan_7", -pi/4, pi/4, { { 0, 3e-4 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_7 (x); },
		"tan_7", -pi/4, pi/4, { { 0, 3e-4 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_9 (x); },
		[] (Vf32 x) { return fstb::Approx::tan_9 (x); },
		"tan_9", -pi/4, pi/4, { { 0, 2e-5 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_9 (x); },
		"tan_9", -pi/4, pi/4, { { 0, 2e-5 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_mystran (x); },
		[] (Vf32 x) { return fstb::Approx::tan_mystran (x); },
		"tan_mystran", -pi/4, pi/4, { { 0, 1.2e-6 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_mystran (x); },
		"tan_mystran", -pi/4, pi/4, { { 0, 1e-6 }, true }
	);

	// tan, close to full range

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_pade33 (x); },
		[] (Vf32 x) { return fstb::Approx::tan_pade33 (x); },
		"tan_pade33", -0.4825 * pi, 0.4825 * pi, { { 0, 1.2e-3 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_pade33 (x); },
		"tan_pade33", -0.4825 * pi, 0.4825 * pi, { { 0, 1.2e-3 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_t3a (x); },
		[] (Vf32 x) { return fstb::Approx::tan_t3a (x); },
		"tan_t3a", -0.4995 * pi, 0.4995 * pi, { { 0, 0.175 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_t3a (x); },
		"tan_t3a", -0.499999 * pi, 0.499999 * pi, { { 0, 0.175 }, true }
	);

	Helper::test_op1_all_flt (ret_val,
		[] (double x) { return tan (x); },
		[] (float x) { return fstb::Approx::tan_pade55 (x); },
		[] (Vf32 x) { return fstb::Approx::tan_pade55 (x); },
		"tan_pade55", -0.49965 * pi, 0.49965 * pi, { { 0, 0.012 }, true }
	);
	Helper::test_op1_all_s <double> (ret_val,
		[] (double x) { return tan (x); },
		[] (double x) { return fstb::Approx::tan_pade55 (x); },
		"tan_pade55", -0.49965 * pi, 0.49965 * pi, { { 0, 0.012 }, true }
	);

	printf ("Done.\n");

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
