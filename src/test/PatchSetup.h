/*****************************************************************************

        PatchSetup.h
        Author: Laurent de Soras, 2024

Skeleton to patch the Pedale Vite configuration file, in case of saving errors
made during the development or batch processing.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (PatchSetup_HEADER_INCLUDED)
#define PatchSetup_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
	namespace doc
	{
		class PluginSettings;
		class Setup;
	}
	namespace piapi
	{
		class ParamDescInterface;
	}
}

class PatchSetup
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static int     patch_setup_file ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static void    fix_peq_2017_02 (mfx::doc::Setup &setup);
	static void    fix_peq_freq (mfx::doc::PluginSettings &settings, const mfx::piapi::ParamDescInterface &desc_freq, const mfx::piapi::ParamDescInterface &desc_type, const mfx::piapi::ParamDescInterface &desc_gain);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               PatchSetup ()                               = delete;
	               PatchSetup (const PatchSetup &other)        = delete;
	               PatchSetup (PatchSetup &&other)             = delete;
	PatchSetup &   operator = (const PatchSetup &other)        = delete;
	PatchSetup &   operator = (PatchSetup &&other)             = delete;
	bool           operator == (const PatchSetup &other) const = delete;
	bool           operator != (const PatchSetup &other) const = delete;

}; // class PatchSetup



//#include "test/PatchSetup.hpp"



#endif // PatchSetup_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
