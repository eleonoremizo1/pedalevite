/*****************************************************************************

        Glyph.cpp
        Copyright (c) 2003 Ohm Force

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include	"Glyph.h"

#include	<cassert>



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Glyph::set_size (int w, int h)
{
	assert (w > 0);
	assert (h > 0);

	_w = w;
	_h = h;
	const int		len = w * h;
	_pix_map.resize (len, 0);
}



int	Glyph::get_width () const
{
	return (_w);
}



int	Glyph::get_height () const
{
	return (_h);
}



void	Glyph::set_pix (int x, int y, int c)
{
	assert (x >= 0);
	assert (x < _w);
	assert (y >= 0);
	assert (y < _h);
	assert (c >= 0);
	assert (c <= 255);

	_pix_map.at (_w * y + x) = uint8_t (c);
}



int	Glyph::get_pix (int x, int y) const
{
	assert (x >= 0);
	assert (x < _w);
	assert (y >= 0);
	assert (y < _h);

	return (_pix_map.at (_w * y + x));
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/





/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
