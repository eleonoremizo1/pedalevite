/*****************************************************************************

        BlockSplitter.cpp
        Author: Laurent de Soras, 2005

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "hiir/test/BlockSplitter.h"

#include <cassert>



namespace hiir
{
namespace test
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



BlockSplitter::BlockSplitter (long max_block_len)
:	_max_block_len (max_block_len)
{
	assert (max_block_len > 0);
}



void	BlockSplitter::start (long total_len)
{
	assert (total_len > 0);

	_total_len = total_len;
	_pos       = 0;
	for (auto &len : _cur_len)
	{
		len = 1;
	}
	_len_index = 0;
}



bool	BlockSplitter::is_continuing () const
{
	return (_pos < _total_len);
}



void	BlockSplitter::set_next_block ()
{
	_pos += get_len ();

	++ _len_index;
	if (_len_index >= _nbr_len)
	{
		_len_index = 0;
		bool           cont_flag = true;
		for (int i = 0; i < _nbr_len && cont_flag; ++i)
		{
			++ _cur_len [i];
			if (_cur_len [i] > _max_block_len)
			{
				_cur_len [i] = 1;
			}
			else
			{
				cont_flag = false;
			}
		}
	}
}



long	BlockSplitter::get_pos () const
{
	return _pos;
}



long	BlockSplitter::get_len () const
{
	long           len = _cur_len [_len_index];
	if (_pos + len > _total_len)
	{
		len = _total_len - _pos;
	}
	assert (len > 0);

	return len;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace test
}  // namespace hiir



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
