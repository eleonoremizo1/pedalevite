/*****************************************************************************

        DsmMgr.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/DsmMgr.h"

#include <algorithm>

#include <cassert>



namespace mfx
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: reserve
Description:
	Allocates memory in provision for a given number of DSM channels.
	The actual number of channels is not changed.
	Calling this function helps when set_nbr_chn() has to be called in a real-
	time thread.
Input parameters:
	- nbr_chn: Number of channels to allocate, >= 0
Throws: like std::vector::reserve()
==============================================================================
*/

void	DsmMgr::reserve (int nbr_chn)
{
	assert (nbr_chn >= 0);

	_chn_arr.reserve (nbr_chn);
}



/*
==============================================================================
Name: set_nbr_chn
Description:
	Changes the number of DSM channels.
	New channels are initialised assuming an initial pin value of 0.
Input parameters:
	- nbr_chn: Number of channels, >= 0
Throws: like std::vector::resize()
==============================================================================
*/

void	DsmMgr::set_nbr_chn (int nbr_chn)
{
	assert (nbr_chn >= 0);

	_chn_arr.resize (nbr_chn);
}



/*
==============================================================================
Name: set_resolution
Description:
	Sets the nominal sampling period duration.
	Output signal switches between periods, so shorter periods give a more
	accurate result. However it is difficult for the operating system to
	observe tight timings, so a balance must be found.
	Default is 100.
Input parameters:
	- period: period duration in time units, > 0.
Throws: Nothing
==============================================================================
*/

void	DsmMgr::set_resolution (int period) noexcept
{
	assert (period > 0);

	_period = period;
}



/*
==============================================================================
Name: set_max_sleep
Description:
	Sets the maximum sleep time between two call to process().
	Use this to set a wake-up period when pins are not switching (levels set to
	0 or 1). This also sets an upper bound on the set_level() latency.
	This maximum should be greater or equal to the sampling period (resolution)
	Default is 10000.
Input parameters:
	- duration: maximum sleeping time in time units, > 0.
Throws: Nothing
==============================================================================
*/

void	DsmMgr::set_max_sleep (int duration) noexcept
{
	assert (duration > 0);

	_max_sleep_time = duration;
}



/*
==============================================================================
Name: set_level
Description:
	Sets the analogue DSM level for a given channel.
	The new value will be taken into account at the next process(), not before.
	This function can be called from any thread.
Input parameters:
	- index: index of the channel, in [0 ; nbr_chn-1]
	- val: value to modulate, in [0 ; 1]
Throws: Nothing
==============================================================================
*/

void	DsmMgr::set_level (int index, float val) noexcept
{
	assert (index >= 0);
	assert (index < int (_chn_arr.size ()));
	assert (val >= 0);
	assert (val <= 1);

	_chn_arr [index]._target = val;
}



/*
==============================================================================
Name: process
Description:
	This function has to be called in a loop alternating with thread sleeps.
Input parameters:
	- timestamp: current clock, in time units. Wrapping arount the 32 bits
		should not be a problem, as long as two consecutive calls are distant
		from less than 2^31 (35 min 47 s if the time unit is the microsecond).
Returns: a structure with two elements:
	- _sleep_time: a suggestion in time units of how long to sleep before the
		next call to this function. It is possible to sleep more or less, but
		large deviations towards higher durations may hurt the signal quality.
		Always > 0.
	- _change_mask: each bit corresponds to a channel. A bit set indicates that
		the channel value has changed and should be reflected on the user side.
		Channels out of the mask (index >= 32) have to be polled manually.
Throws: Nothing
==============================================================================
*/

DsmMgr::ProcResult	DsmMgr::process (uint32_t timestamp) noexcept
{
	assert (_max_sleep_time >= uint32_t (_period));

	ProcResult     res;

	const auto     dt = int (timestamp - _ts_prev);

	// Default for sleep time: upper bound
	res._sleep_time = _max_sleep_time;

	// Error scale factor. Corresponds to the number of periods spend since
	// the previous call.
	const auto     err_scale = float (dt) / float (_period);

	for (int chn_idx = 0; chn_idx < int (_chn_arr.size ()); ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];

		// _target may be changed from another thread, so we sample it only once
		// here then use the local variable.
		const auto     target     = chn._target;
		bool           state_flag = chn._state_flag;

		// Computes and accumulates the error from the past period
		auto           error = (chn._tgt_old - float (state_flag)) * err_scale;
		error    = fstb::limit (error, -_max_err, +_max_err);
		chn._a0 += error;

		// Deduces a new output value
		const auto     quant_in = target + chn._a0;
		const auto     new_flag = (quant_in >= 0.5f);

		// Updates the output
		if (new_flag != state_flag)
		{
			if (chn_idx < ProcResult::_max_nbr_chn_mask)
			{
				res._change_mask |= uint32_t (1) << chn_idx;
			}
			chn._state_flag = new_flag;
		}

		constexpr auto margin = 0.01f;
		const bool     short_pulse_flag =
			(target < margin || target > 1 - margin);

		// Estimates the timestamp for the next change
		if (res._sleep_time > _period || short_pulse_flag)
		{
			// Equation to solve:
			// target + (a0 + error) = 0.5
			// with error = (target - state) * err_scale
			// We want err_scale:
			// target + a0 + (target - state) * err_scale = 0.5
			// err_scale = (0.5 - target - a0) / (target - state)
			const auto     den = target - float (new_flag);
			if (den != 0)
			{
				const auto     es         = (0.5f - target - chn._a0) / den;
				assert (es >= 0);
				auto           sleep_time = fstb::round_int (es * float (_period));

				// Keep the sleep time above the sampling period, to make sure
				// the next switch will be distant enough.
				// However we relax this constraint for levels close to 0 or 1, so
				// the integrated level is reached in a shorter time.
				if (! short_pulse_flag)
				{
					sleep_time = std::max (sleep_time, _period);
				}

				// Combines the wake-up times
				res._sleep_time = std::min (res._sleep_time, sleep_time);
			}
		}

		// Prepares for the next frame
		chn._tgt_old = target;
	}

	_ts_prev = timestamp;

	assert (res._sleep_time > 0);

	return res;
}



/*
==============================================================================
Name: get_state
Description:
	Retrieves the binary pin state of a channel.
	This function can be called from any thread.
Input parameters:
	- index: channel index, in [0 ; nbr_chn-1]
Returns: false (0) for a low level or true (1) for a high level.
Throws: Nothing
==============================================================================
*/

bool	DsmMgr::get_state (int index) const noexcept
{
	assert (index >= 0);
	assert (index < int (_chn_arr.size ()));

	return _chn_arr [index]._state_flag;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
