/*****************************************************************************

        GraphDisp.h
        Author: Laurent de Soras, 2024

This class generates structured information from the document in order to
build a display of the audio graph (and signal nodes too).

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_GraphDisp_HEADER_INCLUDED)
#define mfx_GraphDisp_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/cmd/lat/Algo.h"
#include "mfx/doc/LayerType.h"
#include "mfx/piapi/Dir.h"
#include "mfx/ChnMode.h"

#include <array>
#include <map>
#include <optional>
#include <vector>



namespace mfx
{

namespace doc
{
	class Layer;
}



class GraphDisp
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Horizontal resolution for the canvas grid, > 0.
	// This is the minimum pitch between nodes.
	static constexpr int _grid_res = 2;

	enum class NodeType
	{
		slot = 0,
		aud_i,
		aud_o,
		send,
		ret
	};

	enum class CnxType
	{
		aud = 0,
		sig
	};

	enum Move
	{
		Move_INVALID = -1,

		Move_U = 0, // Up
		Move_D,     // Down
		Move_L,     // Left
		Move_R,     // Right

		Move_NBR_ELT
	};
	typedef std::array <int, Move_NBR_ELT> MoveTable;

	void           process (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list);

	bool           is_ready () const noexcept;

	std::array <int, 2>
	               get_canvas_size () const noexcept;

	int            get_nbr_nodes () const noexcept;
	NodeType       get_node_type (int node_idx) const noexcept;
	bool           is_node_signal_slot (int node_idx) const noexcept;
	int            get_node_id (int node_idx) const noexcept;
	std::array <int, 2>
	               get_node_coord (int node_idx) const noexcept;
	const MoveTable &
	               use_move_table (int node_idx) const noexcept;

	int            get_nbr_cnx () const noexcept;
	CnxType        get_cnx_type (int cnx_idx) const noexcept;
	int            get_cnx_end (int cnx_idx, piapi::Dir dir) const noexcept;

	std::optional <int>
	               find_node (doc::CnxEnd::Type type, int slot_id, int pin_idx, piapi::Dir dir) const noexcept;
	std::optional <int>
	               find_node (int slot_id) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// All slots from the documents are mapped. Additionally:
	// - There is one node per audio input and output pin, connected or not.
	// Total depends on the channel config for the document.
	// - Send and return devices are separate nodes, and there is one per pin.
	// Only connected pins are stored here, but always as complete S/R pairs.
	//
	// Possible and optional addition for the future: connected controllers.
	class Node
	{
	public:
		typedef std::vector <int> CnxList;
		typedef std::array <CnxList, piapi::Dir_NBR_ELT> EndList;

		// Makes sure I/O order is the same everywhere
		static_assert (
			   int (NodeType::aud_o) - int (NodeType::aud_i)
			== piapi::Dir_OUT - piapi::Dir_IN
		);
		static_assert (
			   int (NodeType::ret  ) - int (NodeType::send )
			== piapi::Dir_OUT - piapi::Dir_IN
		);

		NodeType       _type;

		// Reference to the document slot_id for slots, and pin number for
		// I/O and send/return
		int            _id   = -1;

		EndList        _end_list;

		// If the node is a slot, indicates if it is a pure signal processor.
		// Otherwise false.
		bool           _sig_slot_flag = false;

		// Vertical position, >= 0, 0 = source (but may be part of a cycle)
		int            _rank = -1;

		// Horizontal position, >= 0
		int            _pos_x = -1;

		// Total number of connections, excluding self-references
		int            _nbr_cnx = -1;

		// Adjascent node index, depending on the direction.
		// For the GUI only.
		MoveTable      _mv_table = fstb::make_array <Move_NBR_ELT> (-1);
	};
	typedef std::vector <Node> NodeList;

	typedef std::array <int, piapi::Dir_NBR_ELT> NodeIndexDir;

	class Cnx
	{
	public:
		typedef std::array <int, piapi::Dir_NBR_ELT> EndArray;

		bool           is_self () const noexcept;

		// Reference to end nodes. -1 = not set
		// Dir_IN  = connected to an input (destination)
		// Dir_OUT = connected to an output (source)
		// It is possible that a node is connected to itself.
		EndArray       _end_arr      = {{ -1, -1 }};

		CnxType        _type;

		// Indicates the edge has been reverted to break loops.
		bool           _reverse_flag = false;
	};
	typedef std::vector <Cnx> CnxList;

	typedef std::map <int, int> MapSlotNode; // [slot_id] -> node index

	enum class Visit : char
	{
		none = 0, // Not visited
		cur,      // Current tree (back edge)
		done      // Done (cross edge)
	};
	typedef std::vector <Visit> VisitedArray;

	class Grid
	{
	public:
		void           make_new (int w, int h);
		int &          at (int x, int y) noexcept;
		const int &    at (int x, int y) const noexcept;
		int            get_beg () const noexcept;
		int            get_end () const noexcept;
	private:
		int            check_x (int x);
		void           insert (int n, bool beg_flag);
		int            _w = 0;
		int            _h = 0;
		int            _x_beg = 0;
		int            _x_end = 0;
		std::vector <int>        // Node index at this pos, < 0: inexisting node
		               _v;
	};

	void           build_graph (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list);
	void           build_node_list (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list);
	void           build_cnx_list (const doc::Layer &layer);
	void           add_cnx (Cnx &&cnx);
	int            find_node (const doc::CnxEnd &end, piapi::Dir dir) const noexcept;
	void           break_cycles (std::vector <int> &ord_node_list) noexcept;
	void           break_cycles_visit_dfs (int node_idx, VisitedArray &visited_arr, std::vector <int> &ord_node_list) noexcept;
	void           reorder_nodes (const std::vector <int> &ord_node_list);
	void           find_ranks ();
	void           setup_layout ();
	void           count_cnx () noexcept;
	void           fix_vertical_overlap ();
	void           retrieve_fwd_cnx (Node::CnxList &cnx_list, int node_idx, piapi::Dir dir) const;
	void           compute_canvas_width_and_fix_pos ();
	void           fill_grid ();
	void           move_node_pushing_others (int x, int y, bool left_flag);
	void           build_move_tables ();

	NodeList       _node_list;
	CnxList        _cnx_list;

	// Valid only at the beginning of the graph construction
	MapSlotNode    _map_slot_node;

	// In/out directions are related here to the whole graph
	// Valid only at the beginning of the graph construction
	NodeIndexDir   _node_aio_beg  = {{ -1, -1 }};
	NodeIndexDir   _node_aio_end  = {{ -1, -1 }};

	// Sorted by pair: +0 = send, +1 = return
	// Valid only at the beginning of the graph construction
	int            _node_sr_beg   = -1;
	int            _node_sr_end   = -1;

	// Number of ranks found, > 0 (highest rank + 1)
	int            _nbr_ranks     = -1;

	// Number of nodes per rank
	std::vector <int>
	               _row_size_arr;

	// Maximum rank size, > 0
	int            _max_row_size  = -1;

	// Canvas width. Related to _max_row_size * _grid_res
	int            _canvas_w      = -1;

	cmd::lat::Algo _lat_algo;
	Grid           _grid;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

#if 0
	               GraphDisp ()                               = delete;
	               GraphDisp (const GraphDisp &other)         = delete;
	               GraphDisp (GraphDisp &&other)              = delete;
	GraphDisp &    operator = (const GraphDisp &other)        = delete;
	GraphDisp &    operator = (GraphDisp &&other)             = delete;
#endif
	bool           operator == (const GraphDisp &other) const = delete;
	bool           operator != (const GraphDisp &other) const = delete;

}; // class GraphDisp



}  // namespace mfx



//#include "mfx/GraphDisp.hpp"



#endif // mfx_GraphDisp_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
