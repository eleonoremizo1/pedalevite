/*****************************************************************************

        AutobindMode.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_AutobindMode_HEADER_INCLUDED)
#define mfx_AutobindMode_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{



enum AutobindMode
{

   AutobindMode_INVALID = -1,

   AutobindMode_OFF = 0,
   AutobindMode_SINGLE,
   AutobindMode_MULTI,
   AutobindMode_MOD,

   AutobindMode_NBR_ELT

}; // enum AutobindMode



}  // namespace mfx



//#include "mfx/AutobindMode.hpp"



#endif // mfx_AutobindMode_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
