/*****************************************************************************

        PotRel.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_PotRel_HEADER_INCLUDED)
#define mfx_PotRel_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{



enum PotRel
{

	PotRel_INVALID = -1,
	PotRel_NBR_GEN = 5, // Number of FX parameters

	// Generic FX parameters
	PotRel_GEN     = 0,

	// Navigation
	PotRel_NAV_VER = PotRel_GEN + PotRel_NBR_GEN,
	PotRel_NAV_HOR,

	PotRel_NBR_ELT

}; // enum PotRel



}  // namespace mfx



//#include "mfx/PotRel.hpp"



#endif // mfx_PotRel_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
