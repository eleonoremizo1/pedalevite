/*****************************************************************************

        PwmMgr.h
        Author: Laurent de Soras, 2024

Class handling several abstract Pulse Width Modulation (PWM) channels.
Channels are characterised only by their binary state, they are not tied to
any physical pin or anything at this level.
Channels are assumed as always running.
They all have the same period, their rising edge is simultaneous.
Physical aspects of the time should be also handled by the caller.
Time unit can be anything (user's choice) but microseconds or nanoseconds fit
well.

Usage:
- Configure the channels and the main pulse period
- Setup an external thread running a loop. This loop should:
	- check the current time accurately
	- call process ()
	- Possibly call get_state() and update physical pin states or whatever
	- sleep the thread for the time indicated by the process () result.
- Call set_level () from any thread to change the DSM values.

When changing the number of channels while the processing loop is running,
the user must synchronise the calls to avoid concurrency.


--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_PwmMgr_HEADER_INCLUDED)
#define mfx_PwmMgr_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <vector>

#include <cstdint>



namespace mfx
{



class PwmMgr
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	class ProcResult
	{
	public:
		static constexpr int _max_nbr_chn_mask = 32;

		// Suggested time to sleep before the next call, in time units.
		int            _sleep_time  = 0;

		// Bitmask of channels whose state has changed.
		uint32_t       _change_mask = 0;
	};

	// Setup
	void           reserve (int nbr_chn);
	void           set_nbr_chn (int nbr_chn);
	void           set_period (int period) noexcept;
	void           set_max_sleep (int duration) noexcept;

	void           set_level (int index, float val) noexcept;

	// Callback from a dedicated thread
	ProcResult     process (uint32_t timestamp) noexcept;
	bool           get_state (int index) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class Channel
	{
	public:
		// Binary output state of the channel.
		// Read by get_state () from another thread.
		volatile bool  _state_flag = false;

		// Target DSM ratio, in [0 ; 1]
		// Set by set_level () from another thread.
		volatile float _target     = 0;

		// Ideal duty cycle duration, in time units. [0 ; _period]
		// Precalculated from the target value (sampled at the rising edge).
		float          _duty       = 0;

		// Current error accumulator for 1st order delta-sigma modulation.
		float          _a0         = 0;

		// Current duty cycle, calculated at the rising edge, in time units.
		int            _dc_cur     = 0;

		// Measured duration of the previous duty cycle, in time units.
		// Calculated on the falling edge.
		int            _dc_prev    = 0;
	};
	typedef std::vector <Channel> ChannelArray;

	ChannelArray   _chn_arr;

	// Cycle duration, in time units. > 0
	int            _period   = 10'000;

	// Theoretical beginning timestamp of the current cycle, in time units
	uint32_t       _cycle_ts = 0;

	// Last rising edge (cycle beginning), measured, in time units
	uint32_t       _prev_ts  = 0;

	// Maximum sleep time in time units. Should be less than 2^31.
	uint32_t       _max_sleep_time = 10'000;

	// Indicates that we are in the first part of the cycle, at the rising edge
	bool           _rising_flag = true;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PwmMgr &other) const = delete;
	bool           operator != (const PwmMgr &other) const = delete;

}; // class PwmMgr



}  // namespace mfx



//#include "mfx/PwmMgr.hpp"



#endif   // mfx_PwmMgr_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
