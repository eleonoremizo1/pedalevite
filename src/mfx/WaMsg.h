/*****************************************************************************

        WaMsg.h
        Author: Laurent de Soras, 2016

Communication message between the audio thread and the command thread.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_WaMsg_HEADER_INCLUDED)
#define mfx_WaMsg_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/LayerType.h"
#include "mfx/doc/ProgSwitchMode.h"
#include "mfx/ControllerType.h"



namespace mfx
{



class ProcessingContext;

class WaMsg
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	enum Sender
	{
		Sender_INVALID = -1,

		Sender_CMD = 0,
		Sender_AUDIO,

		Sender_NBR_ELT
	};

	enum Type
	{
		Type_INVALID = -1,

		Type_CTX = 0,
		Type_LAYER,
		Type_PARAM,
		Type_TEMPO,
		Type_RESET,
		Type_AUTOBIND,
		Type_CTRLBIND,

		Type_NBR_ELT
	};

	class Ctx
	{
	public:
		// Forward: the new context, on return: the context to be recycled.
		const ProcessingContext *
		               _ctx_ptr;

		doc::ProgSwitchMode
		               _prog_switch_mode;
	};

	class Layer
	{
	public:
		// The new layer where to apply modifications
		doc::LayerType _layer;
	};

	class Param
	{
	public:
		int            _plugin_id;
		int            _index;
		float          _val;
	};

	class Tempo
	{
	public:
		float          _bpm; // 0 = refresh
	};

	class Reset
	{
	public:
		int            _plugin_id;
		bool           _steady_flag;
		bool           _full_flag;
	};

	class Autobind
	{
	public:
		bool           _enable_flag;
	};

	class CtrlBind
	{
	public:
		ControllerType _type;  // Members from ControlSource
		int            _index; // --''--
		float          _val;   // Probably not necessary, just to be safe
	};

	union Content
	{
		Ctx            _ctx;
		Layer          _layer;
		Param          _param;
		Tempo          _tempo;
		Reset          _reset;
		Autobind       _autobind;
		CtrlBind       _ctrlbind;
	};

	// Original sender. To know if a message should be recycled.
	Sender         _sender = Sender_INVALID;

	Type           _type   = Type_INVALID;

	Content        _content {};



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const WaMsg &other) const = delete;
	bool           operator != (const WaMsg &other) const = delete;

}; // class WaMsg



}  // namespace mfx



//#include "mfx/WaMsg.hpp"



#endif   // mfx_WaMsg_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
