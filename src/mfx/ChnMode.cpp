/*****************************************************************************

        ChnMode.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ChnMode.h"

#include <array>

#include <cassert>



namespace mfx
{



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



static piapi::Dir	ChnMode_adapt_io_dir_to_layer (piapi::Dir dir, doc::LayerType layer) noexcept
{
	assert (piapi::Dir_is_valid (dir));
	assert (int (layer) >= 0);
	assert (int (layer) <= int (doc::LayerType::NBR_ELT));

	if (layer == doc::LayerType::I)
	{
		dir = piapi::Dir_IN;
	}
	else if (layer == doc::LayerType::O)
	{
		dir = piapi::Dir_OUT;
	}

	return dir;
}



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// When used as an array index, in the get_nbr_* functions
static_assert (piapi::Dir_IN == 0 && piapi::Dir_OUT == 1, "Wrong values");

typedef std::array <
	std::array <int, ChnMode_NBR_ELT>,
	piapi::Dir_NBR_ELT
> ChnMode_Lut;



// Dir refers to the audio graph, so IN = audio input
int	ChnMode_get_nbr_chn (ChnMode mode, piapi::Dir dir)
{
	assert (mode >= 0);
	assert (mode < ChnMode_NBR_ELT);
	assert (piapi::Dir_is_valid (dir));

	ChnMode_Lut    table =
	{{
		{{ 1, 1, 2 }}, // In
		{{ 1, 2, 2 }}  // Out
	}};

	return table [dir] [mode];
}



int	ChnMode_get_nbr_pins (ChnMode mode, piapi::Dir dir)
{
	assert (mode >= 0);
	assert (mode < ChnMode_NBR_ELT);
	assert (piapi::Dir_is_valid (dir));

	ChnMode_Lut    table =
	{{
		{{ 2, 2, 1 }}, // In
		{{ 2, 1, 1 }}  // Out
	}};

	return table [dir] [mode];
}



int	ChnMode_get_layer_nbr_chn (doc::LayerType layer, ChnMode mode, piapi::Dir dir)
{
	assert (int (layer) >= 0);
	assert (int (layer) <= int (doc::LayerType::NBR_ELT));
	assert (mode >= 0);
	assert (mode < ChnMode_NBR_ELT);
	assert (piapi::Dir_is_valid (dir));

	const auto     dir_l = ChnMode_adapt_io_dir_to_layer (dir, layer);

	return ChnMode_get_nbr_chn (mode, dir_l);
}



int	ChnMode_get_layer_nbr_pins (doc::LayerType layer, ChnMode mode, piapi::Dir dir)
{
	assert (int (layer) >= 0);
	assert (int (layer) <= int (doc::LayerType::NBR_ELT));
	assert (mode >= 0);
	assert (mode < ChnMode_NBR_ELT);
	assert (piapi::Dir_is_valid (dir));

	const auto     dir_l = ChnMode_adapt_io_dir_to_layer (dir, layer);

	return ChnMode_get_nbr_pins (mode, dir_l);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
