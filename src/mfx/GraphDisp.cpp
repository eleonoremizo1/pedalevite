/*****************************************************************************

        GraphDisp.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#undef mfx_GraphDisp_TEST_ORDERING



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#if defined (mfx_GraphDisp_TEST_ORDERING)
	#include "fstb/Hash.h"
#endif // mfx_GraphDisp_TEST_ORDERING
#include "mfx/doc/Layer.h"
#include "mfx/Cst.h"
#include "mfx/GraphDisp.h"

#include <algorithm>

#include <cassert>



namespace mfx
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	GraphDisp::process (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list)
{
	assert (int (layer_type) >= 0);
	assert (int (layer_type) < int (doc::LayerType::NBR_ELT));
	assert (chn_mode >= 0);
	assert (chn_mode < ChnMode_NBR_ELT);

	// Not mandatory here, but just we just make sure to invalidate any
	// previous data.
	_node_aio_beg.fill (-1);
	_node_aio_end.fill (-1);
	_node_sr_beg  = -1;
	_node_sr_end  = -1;
	_nbr_ranks    = -1;
	_row_size_arr.clear ();
	_max_row_size = -1;
	_canvas_w     = -1;
	_grid.make_new (0, 0);

	// Main part
	build_graph (layer, layer_type, chn_mode, sig_pi_list);
	find_ranks ();
	setup_layout ();
	compute_canvas_width_and_fix_pos ();

	// Fixes display issues
	count_cnx ();
	fix_vertical_overlap ();

	// For UI navigation
	build_move_tables ();
}



bool	GraphDisp::is_ready () const noexcept
{
	return (_max_row_size > 0);
}



std::array <int, 2>	GraphDisp::get_canvas_size () const noexcept
{
	assert (is_ready ());

	return { _canvas_w, _nbr_ranks };
}



int	GraphDisp::get_nbr_nodes () const noexcept
{
	assert (is_ready ());

	return int (_node_list.size ());
}



GraphDisp::NodeType	GraphDisp::get_node_type (int node_idx) const noexcept
{
	assert (is_ready ());
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());

	return _node_list [node_idx]._type;
}



int	GraphDisp::get_node_id (int node_idx) const noexcept
{
	assert (is_ready ());
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());

	return _node_list [node_idx]._id;
}



bool	GraphDisp::is_node_signal_slot (int node_idx) const noexcept
{
	assert (is_ready ());
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());

	return _node_list [node_idx]._sig_slot_flag;
}



std::array <int, 2>	GraphDisp::get_node_coord (int node_idx) const noexcept
{
	assert (is_ready ());
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());

	return { _node_list [node_idx]._pos_x, _node_list [node_idx]._rank };
}



const GraphDisp::MoveTable &	GraphDisp::use_move_table (int node_idx) const noexcept
{
	assert (is_ready ());
	assert (_canvas_w > 0);
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());

	return _node_list [node_idx]._mv_table;
}



int	GraphDisp::get_nbr_cnx () const noexcept
{
	assert (is_ready ());

	return int (_cnx_list.size ());
}



GraphDisp::CnxType	GraphDisp::get_cnx_type (int cnx_idx) const noexcept
{
	assert (is_ready ());
	assert (cnx_idx >= 0);
	assert (cnx_idx < get_nbr_cnx ());

	return _cnx_list [cnx_idx]._type;
}



int	GraphDisp::get_cnx_end (int cnx_idx, piapi::Dir dir) const noexcept
{
	assert (is_ready ());
	assert (cnx_idx >= 0);
	assert (cnx_idx < get_nbr_cnx ());
	assert (piapi::Dir_is_valid (dir));

	return _cnx_list [cnx_idx]._end_arr [dir];
}



// For audio ports, the direction is related to the node.
std::optional <int>	GraphDisp::find_node (doc::CnxEnd::Type type, int slot_id, int pin_idx, piapi::Dir dir) const noexcept
{
	assert (type >= 0);
	assert (type < doc::CnxEnd::Type_NBR_ELT);
	assert (slot_id >= 0);
	assert (pin_idx >= 0);
	assert (type == doc::CnxEnd::Type_NORMAL || piapi::Dir_is_valid (dir));

	std::optional <int>  node_idx_opt;

	auto           g_type = NodeType::slot;
	auto           g_id   = slot_id;
	if (type == doc::CnxEnd::Type_IO)
	{
		g_type = (dir == piapi::Dir_IN) ? NodeType::aud_o : NodeType::aud_i;
		g_id   = pin_idx;
	}
	else if (type == doc::CnxEnd::Type_RS)
	{
		g_type = (dir == piapi::Dir_IN) ? NodeType::send  : NodeType::ret;
		g_id   = pin_idx;
	}
	else
	{
		assert (type == doc::CnxEnd::Type_NORMAL);
	}

	const auto     it = std::find_if (
		_node_list.begin (), _node_list.end (),
		[g_type, g_id] (const Node &node) noexcept
		{
			return (node._type == g_type && node._id == g_id);
		}
	);
	if (it != _node_list.end ())
	{
		const auto     node_idx = int (std::distance (_node_list.begin (), it));
		assert (node_idx >= 0);
		assert (node_idx < int (_node_list.size ()));
		node_idx_opt.emplace (node_idx);
	}

	return node_idx_opt;
}



std::optional <int>	GraphDisp::find_node (int slot_id) const noexcept
{
	assert (slot_id >= 0);

	return find_node (doc::CnxEnd::Type_NORMAL, slot_id, 0, piapi::Dir_IN);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	GraphDisp::Cnx::is_self () const noexcept
{
	return (_end_arr [0] == _end_arr [1]);
}



void	GraphDisp::Grid::make_new (int w, int h)
{
	assert (w >= 0);
	assert (h >= 0);

	_w = w;
	_h = h;
	_x_beg = 0;
	_x_end = w;
	_v.clear ();
	_v.resize (w * h, -1);
}



// x coordinates out of bounds automatically resize the table.
// Negative x coord are allowed.
int &	GraphDisp::Grid::at (int x, int y) noexcept
{
	assert (y >= 0);
	assert (y < _h);

	const int      xo = check_x (x);

	return _v [y * _w + xo];
}



const int &	GraphDisp::Grid::at (int x, int y) const noexcept
{
	assert (x >= _x_beg);
	assert (x <  _x_end);
	assert (y >= 0);
	assert (y < _h);

	return _v [y * _w + x - _x_beg];
}



int	GraphDisp::Grid::get_beg () const noexcept
{
	return _x_beg;
}



int	GraphDisp::Grid::get_end () const noexcept
{
	return _x_end;
}



int	GraphDisp::Grid::check_x (int x)
{
	if (x >= _x_end)
	{
		insert (x + 1 - _x_end, false);
	}
	x -= _x_beg;
	if (x < 0)
	{
		insert (-x, true);
		x = 0;
	}

	return x;
}



void	GraphDisp::Grid::insert (int n, bool beg_flag)
{
	assert (n > 0);

	_v.resize ((_w + n) * _h);

	const auto     ofs_copy = (beg_flag) ? n : 0;
	const auto     ofs_fill = (beg_flag) ? 0 : _w;
	for (int y = _h - 1; y >= 0; --y)
	{
		const int      row_src =  _w      * y;
		const int      row_dst = (_w + n) * y;
		const auto     dst_beg = _v.begin () + row_dst;
		const auto     dst_end = dst_beg + _w;
		const auto     src_beg = _v.begin () + row_src;
		const auto     src_end = src_beg + _w;
		const auto     dst_fil = dst_beg + ofs_fill;
		std::copy_backward (src_beg, src_end, dst_end + ofs_copy);
		std::fill (dst_fil, dst_fil + n, -1);
	}

	_w += n;
	if (beg_flag)
	{
		_x_beg -= n;
	}
	else
	{
		_x_end += n;
	}
}



void	GraphDisp::build_graph (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list)
{
	assert (int (layer_type) >= 0);
	assert (int (layer_type) < int (doc::LayerType::NBR_ELT));
	assert (chn_mode >= 0);
	assert (chn_mode < ChnMode_NBR_ELT);

	build_node_list (layer, layer_type, chn_mode, sig_pi_list);
	build_cnx_list (layer);

	std::vector <int> ord_node_list;
	break_cycles (ord_node_list);
	reorder_nodes (ord_node_list);
}



void	GraphDisp::build_node_list (const doc::Layer &layer, doc::LayerType layer_type, ChnMode chn_mode, const std::vector <std::string> &sig_pi_list)
{
	assert (int (layer_type) >= 0);
	assert (int (layer_type) < int (doc::LayerType::NBR_ELT));
	assert (chn_mode >= 0);
	assert (chn_mode < ChnMode_NBR_ELT);

	_node_list.clear ();
	_map_slot_node.clear ();

	// Audio I/O
	for (int dir = 0; dir < piapi::Dir_NBR_ELT; ++dir)
	{
		_node_aio_beg [dir] = int (_node_list.size ());
		const auto     nbr_pins =
			ChnMode_get_layer_nbr_pins (layer_type, chn_mode, piapi::Dir (dir));
		for (int pin_idx = 0; pin_idx < nbr_pins; ++ pin_idx)
		{
			_node_list.emplace_back (Node {
				NodeType (int (NodeType::aud_i) + dir),
				pin_idx,
				{}, false, -1, -1
			});
		}
		_node_aio_end [dir] = int (_node_list.size ());
	}

	// Standard nodes. Keeps the signal nodes at the end of the list.
	const int      slot_beg = int (_node_list.size ());
	NodeList       sig_node_list;
	for (const auto &slot_vt : layer._slot_map)
	{
		const auto     slot_id   = slot_vt.first;
		bool           sig_flag  = false;
		if (! doc::Layer::is_slot_empty (slot_vt))
		{
			const auto &   model = slot_vt.second->_pi_model;
			if (   ! model.empty ()
			    &&    std::find (sig_pi_list.begin (), sig_pi_list.end (), model)
			       != sig_pi_list.end ())
			{
				sig_flag = true;
			}
		}
		auto *         n_list_ptr = (sig_flag) ? &sig_node_list : &_node_list;
		n_list_ptr->emplace_back (
			Node { NodeType::slot, slot_id, {}, sig_flag, -1, -1 }
		);
	}

	_node_list.insert (
		_node_list.end (), sig_node_list.begin (), sig_node_list.end ()
	);

#if defined (mfx_GraphDisp_TEST_ORDERING)
	// Randomizes the slot order to stress-test the graph layout algorithm
	auto           rnd_pred =
		[] (const Node &lhs, const Node &rhs) noexcept
		{
			const auto     hl = fstb::Hash::hash (uint32_t (lhs._id));
			const auto     hr = fstb::Hash::hash (uint32_t (rhs._id));
			return (hl < hr);
		};
	std::sort (_node_list.begin () + slot_beg, _node_list.end (), rnd_pred);
#endif // mfx_GraphDisp_TEST_ORDERING

	// Builds a map for easy access from the slot_id
	for (int pos = slot_beg; pos < int (_node_list.size ()); ++pos)
	{
		const auto &   node = _node_list [pos];
		_map_slot_node [node._id] = pos;
	}

	// Send/return
	// First, finds referenced S/R pairs
	std::array <bool, Cst::_max_nbr_send>   sr_mask = { };
	const doc::Routing & routing = layer.use_routing ();
	const auto &   cnx_set = routing._cnx_audio_set;
	auto           it_sr   = cnx_set.begin ();
	while (it_sr != cnx_set.end ())
	{
		it_sr = std::find_if (
			it_sr, cnx_set.end (),
			[] (const doc::Routing::CnxSet::value_type &cnx)
			{
				return (cnx.is_referencing (doc::CnxEnd::Type_RS));
			}
		);
		if (it_sr == cnx_set.end ())
		{
			break;
		}
		for (int dir = 0; dir < piapi::Dir_NBR_ELT; ++dir)
		{
			const doc::CnxEnd &  ce = it_sr->use_end (piapi::Dir (dir));
			if (ce.get_type () == doc::CnxEnd::Type_RS)
			{
				sr_mask [ce.get_pin ()] = true;
			}
		}
		++ it_sr;
	}

	// Adds them to the node list
	_node_sr_beg = int (_node_list.size ());
	for (int pin_idx = 0; pin_idx < Cst::_max_nbr_send; ++ pin_idx)
	{
		if (sr_mask [pin_idx])
		{
			_node_list.emplace_back (
				Node { NodeType::send, pin_idx, {}, false, -1, -1 }
			);
			_node_list.emplace_back (
				Node { NodeType::ret , pin_idx, {}, false, -1, -1 }
			);
		}
	}
	_node_sr_end = int (_node_list.size ());
}



void	GraphDisp::build_cnx_list (const doc::Layer &layer)
{
	_cnx_list.clear ();

	// Standard connections. Includes audio in/out and send/return
	const doc::Routing & routing = layer.use_routing ();
	for (const auto &cnx :routing._cnx_audio_set)
	{
		auto &         src     = cnx.use_src ();
		auto &         dst     = cnx.use_dst ();
		const auto     src_idx = find_node (src, piapi::Dir_OUT);
		const auto     dst_idx = find_node (dst, piapi::Dir_IN );
		add_cnx (Cnx { {{ dst_idx, src_idx }}, CnxType::aud, false });
	}

	// Signals
	// First, build a shortcut map for sources.
	std::map <int, int>  sig_map; // [port index] -> node index
	for (const auto &port_vt : layer._port_map)
	{
		const auto     sig_idx  = port_vt.first;
		const auto     slot_id  = port_vt.second._slot_id;
		const auto     it       = _map_slot_node.find (slot_id);
		assert (it != _map_slot_node.end ());
		const auto     node_idx = it->second;
		sig_map [sig_idx] = node_idx;
	}

	// Then scans all modulations from all slots and looks for signal uses
	for (const auto &slot_vt : layer._slot_map)
	{
		// Inexisting slot
		if (slot_vt.second.get () == nullptr)
		{
			continue;
		}

		const doc::Slot & slot = *(slot_vt.second);

		// Destination node
		const auto     slot_id = slot_vt.first;
		const auto     it_dst  = _map_slot_node.find (slot_id);
		assert (it_dst != _map_slot_node.end ());
		const auto     dst_idx = it_dst->second;

		// Retrieves the settings. We get at least the mixer.
		std::array <const doc::PluginSettings *, 2> settings_list {
			&slot._settings_mixer, nullptr
		};
		if (! slot.is_empty ())
		{
			// Main plug-in settings
			const auto     it = slot._settings_all.find (slot._pi_model);
			settings_list.back () = &(it->second);
		}

		// Each type of existing settings
		for (const auto settings_ptr : settings_list)
		{
			if (settings_ptr == nullptr)
			{
				break;
			}

			// Modulated parameters
			for (const auto &cls_vt : settings_ptr->_map_param_ctrl)
			{
				const doc::CtrlLinkSet &   cls = cls_vt.second;

				auto        check_ctrl =
					[dst_idx, &sig_map, this] (const doc::CtrlLink &cl)
					{
						if (cl._source._type == ControllerType_FX_SIG)
						{
							const auto     sig_idx = cl._source._index;
							const auto     it_src  = sig_map.find (sig_idx);
							const auto     src_idx = it_src->second;
							add_cnx (
								Cnx { {{ dst_idx, src_idx }}, CnxType::sig, false }
							);
						}
					};

				if (cls._bind_sptr.get () != nullptr)
				{
					check_ctrl (*(cls._bind_sptr));
				}

				for (const auto &cl_sptr : cls._mod_arr)
				{
					check_ctrl (*cl_sptr);
				}
			} // Parameters
		} // Settings
	} // Destination slots
}



void	GraphDisp::add_cnx (Cnx &&cnx)
{
	const int      cnx_idx = int (_cnx_list.size ());
	const int      src_idx = cnx._end_arr [piapi::Dir_OUT];
	const int      dst_idx = cnx._end_arr [piapi::Dir_IN ];
	_cnx_list.emplace_back (cnx);
	_node_list [src_idx]._end_list [piapi::Dir_OUT].push_back (cnx_idx);
	_node_list [dst_idx]._end_list [piapi::Dir_IN ].push_back (cnx_idx);
}



int	GraphDisp::find_node (const doc::CnxEnd &end, piapi::Dir dir) const noexcept
{
	assert (piapi::Dir_is_valid (dir));

	const auto     type    = end.get_type ();
	const auto     slot_id = end.get_slot_id ();
	const auto     pin_idx = end.get_pin ();

	if (type == doc::CnxEnd::Type_NORMAL)
	{
		const auto     it = _map_slot_node.find (slot_id);
		assert (it != _map_slot_node.end ());
		return it->second;
	}

	else if (type == doc::CnxEnd::Type_IO)
	{
		// Invert because "audio input" for the whole graph is actually a node
		// output.
		const auto     graph_dir = int (piapi::Dir_invert (dir));
		const auto     base      = _node_aio_beg [graph_dir];
		const auto     node_idx  = base + pin_idx;
		assert (node_idx < _node_aio_end [graph_dir]);
		return node_idx;
	}

	else if (type == doc::CnxEnd::Type_RS)
	{
		for (int node_idx = _node_sr_beg
		;	node_idx < _node_sr_end
		;	node_idx += int (piapi::Dir_NBR_ELT))
		{
			assert (_node_list [node_idx + piapi::Dir_IN]._type == NodeType::send);
			if (_node_list [node_idx]._id == pin_idx)
			{
				return node_idx + int (dir);
			}
		}
		// This must not happen
		assert (false);
	}

	// This must not happen
	assert (false);
	return -1;
}



// Builds a node order from the DFS too
void	GraphDisp::break_cycles (std::vector <int> &ord_node_list) noexcept
{
	VisitedArray   visited_arr (_node_list.size (), Visit::none);
	ord_node_list.clear ();

	// Starts with pure source nodes, then a second pass for remaining
	// nodes that could be isolated loops
	for (int pass = 0; pass < 2; ++pass)
	{
		for (int node_idx = 0; node_idx < int (_node_list.size ()); ++node_idx)
		{
			if (visited_arr [node_idx] == Visit::none)
			{
				const auto &   node = _node_list [node_idx];
				if (pass == 1 || node._end_list [piapi::Dir_IN].empty ())
				{
					break_cycles_visit_dfs (node_idx, visited_arr, ord_node_list);
				}
			}
		}
	}

	// Makes sure we processed all the nodes.
	assert (std::find_if (
		visited_arr.begin (), visited_arr.end (),
		[] (Visit v)
		{
			return (v != Visit::done);
		}
	) == visited_arr.end ());
}



void	GraphDisp::break_cycles_visit_dfs (int node_idx, VisitedArray &visited_arr, std::vector <int> &ord_node_list) noexcept
{
	assert (node_idx >= 0);
	assert (node_idx < int (_node_list.size ()));
	assert (visited_arr.size () == _node_list.size ());
	assert (visited_arr [node_idx] == Visit::none);

	// Node from the current path
	visited_arr [node_idx] = Visit::cur;
	ord_node_list.push_back (node_idx);

	// Explores connections
	const auto &   cnx_list = _node_list [node_idx]._end_list [piapi::Dir_OUT];
	for (const auto cnx_idx : cnx_list)
	{
		auto &         cnx     = _cnx_list [cnx_idx];
		const auto     dst_idx = cnx._end_arr [piapi::Dir_IN];

		// Ignores self-referencing connections (most likely a signal path)
		if (dst_idx != node_idx)
		{
			const auto     status = visited_arr [dst_idx];
			if (status == Visit::none)
			{
				// New node
				break_cycles_visit_dfs (dst_idx, visited_arr, ord_node_list);
			}
			else if (status == Visit::cur)
			{
				// Node from the current path: back edge, this is a cycle
				cnx._reverse_flag = true;
			}
			else
			{
				// Node from another branch: cross edge
				assert (status == Visit::done);
			}
		}
	}

	// We have done the node and its sub-tree
	visited_arr [node_idx] = Visit::done;
}



// ord_node_list: [index new] = index old
void	GraphDisp::reorder_nodes (const std::vector <int> &ord_node_list)
{
	const auto     nbr_nodes = int (_node_list.size ());
	assert (int (ord_node_list.size ()) == nbr_nodes);
	assert (
		  *std::max_element (ord_node_list.begin (), ord_node_list.end ())
		< int (_node_list.size ())
	);

	// Builds the inverse list [index old] = index new
	std::vector <int> inv_list (nbr_nodes, -1);
	for (int new_idx = 0; new_idx < nbr_nodes; ++new_idx)
	{
		const int      old_idx = ord_node_list [new_idx];
		assert (old_idx >= 0);
		assert (old_idx < nbr_nodes);
		inv_list [old_idx] = new_idx;
	}
	// Checks there's no hole
	assert (*std::min_element (inv_list.begin (), inv_list.end ()) == 0);

	// Changes all references in the connections
	for (auto &cnx : _cnx_list)
	{
		for (auto &end : cnx._end_arr)
		{
			end = inv_list [end];
		}
	}

	// Builds a new reorderd node list
	NodeList       node_list_new;
	for (int idx_new = 0; idx_new < nbr_nodes; ++idx_new)
	{
		const auto     idx_old = ord_node_list [idx_new];
		node_list_new.push_back (_node_list [idx_old]);
	}
	_node_list.swap (node_list_new);

	// Now these members are useless and invalid. Makes sure we don't use
	// them anymore.
	_node_aio_beg.fill (-1);
	_node_aio_end.fill (-1);
	_node_sr_beg = -1;
	_node_sr_end = -1;
	_map_slot_node.clear ();
}



void	GraphDisp::find_ranks ()
{
	// Prepares data for the algorithm
	_lat_algo.reset ();

	const auto     nbr_nodes = int (_node_list.size ());
	_lat_algo.set_nbr_elt (nbr_nodes, 0);
	for (int pos = 0; pos < nbr_nodes; ++pos)
	{
		auto &         node = _lat_algo.use_node (pos);
		node.set_latency (1);
	}

	for (const auto &cnx : _cnx_list)
	{
		// Ignores self-referencing connections again
		if (cnx._end_arr [piapi::Dir_IN] != cnx._end_arr [piapi::Dir_OUT])
		{
			cmd::lat::Cnx  cnx_lat;
			for (int dir_idx = 0; dir_idx < piapi::Dir_NBR_ELT; ++dir_idx)
			{
				const auto     dir_lat = piapi::Dir (dir_idx);
				const auto     dir_mod = int (
					(cnx._reverse_flag) ? piapi::Dir_invert (dir_lat) : dir_lat
				);
				cnx_lat.set_node (dir_lat, cnx._end_arr [dir_mod]);
			}
			_lat_algo.add_cnx (cnx_lat);
		}
	}

	// Runs it
	_lat_algo.run ();

	// Extracts the ranks
	int            rank_max = 0;
	int            rank_min = 0;
	for (int pos = 0; pos < nbr_nodes; ++pos)
	{
		auto &         node     = _node_list [pos];
		const auto &   node_lat = _lat_algo.use_node (pos);
		int            rank     = 0;
		if (node_lat.is_timestamp_set ())
		{
			rank = node_lat.get_timestamp ();
		}
#if ! defined (NDEBUG)
		else
		{
			// We may have unknown timestamps for unconnected nodes
			for (const auto &end : node._end_list)
			{
				assert (end.empty ());
			}
		}
#endif
		node._rank = rank;
		rank_min = std::min (rank_min, rank);
		rank_max = std::max (rank_max, rank);
	}

	// Normally, a sink node has a high rank, and here we make sure that audio
	// outputs have the highest rank (bottom of the graph). We send unconnected
	// SEND nodes to the bottom too.
	for (int pos = 0; pos < nbr_nodes; ++pos)
	{
		auto &         node = _node_list [pos];
		if (       node._type == NodeType::aud_o
		    || (   node._type == NodeType::send
		        && node._end_list [piapi::Dir_IN ].empty ()))
		{
			assert (node._end_list [piapi::Dir_OUT].empty ());
			node._rank = rank_max;
		}
		node._rank -= rank_min;
		assert (node._rank >= 0);
	}

	_nbr_ranks = rank_max + 1 - rank_min;
	assert (_nbr_ranks > 0);
}



void	GraphDisp::setup_layout ()
{
	assert (_nbr_ranks > 0);

	_row_size_arr.assign (_nbr_ranks, 0);
	_max_row_size = 0;
	for (auto &node : _node_list)
	{
		auto &         row_size = _row_size_arr [node._rank];
		node._pos_x = row_size * _grid_res;
		++ row_size;
		_max_row_size = std::max (_max_row_size, row_size);
	}
}



/*
Issue to solve: vertically aligned nodes may hide forward connections.
More generally, a node shouldn't be located on a connection path.
This is true for any direction but we focus on vertical paths here.
So we have to break the alignment to show all the connections.
*/

void	GraphDisp::fix_vertical_overlap ()
{
	// We need at least the canvas width and the grid ready
	assert (_canvas_w > 0);

	Node::CnxList  cnx_list;
	cnx_list.reserve (8); // Should be enough for most cases

	std::vector <int> cross_nodes;
	cross_nodes.reserve (16); // Same

	for (int y = 0; y < _nbr_ranks; ++y)
	{
		for (int x = _grid.get_beg (); x < _grid.get_end (); ++x)
		{
			const auto     node_idx = _grid.at (x, y);
			if (node_idx >= 0)
			{
				/*** To do:
				Check if we really need to scan in both directions. Only Dir_OUT
				should be enough.
				***/
				for (int dir_cnt = 0; dir_cnt < piapi::Dir_NBR_ELT; ++dir_cnt)
				{
					const auto     dir      = piapi::Dir (dir_cnt);
					const auto     dir_sign = (dir == piapi::Dir_IN) ? -1 : 1;

					// Checks if there are aligned nodes

					retrieve_fwd_cnx (cnx_list, node_idx, dir);

					// No forward connection
					if (cnx_list.empty ())
					{
						continue;
					}

					// Finds the most distant vertical connection
					int            dist = 0;
					for (auto cnx_idx : cnx_list)
					{
						const auto &   cnx      = _cnx_list [cnx_idx];
						const auto     tgt_idx  =
							cnx._end_arr [piapi::Dir_invert (dir)];
						const auto &   tgt_node = _node_list [tgt_idx];
						if (tgt_node._pos_x == x)
						{
							dist = std::max (dist, std::abs (tgt_node._rank - y));
						}
					}

					// Finds all the nodes on the path, excluding the ends
					cross_nodes.clear ();
					int            n_cnx_cen = 0;
					for (int ofs_y = 1; ofs_y < dist; ++ofs_y)
					{
						const int      y_scan   = y + ofs_y * dir_sign;
						const int      scan_idx = _grid.at (x, y_scan);
						if (scan_idx >= 0)
						{
							cross_nodes.push_back (scan_idx);
							const int      nbr_cnx = _node_list [scan_idx]._nbr_cnx;
							n_cnx_cen = std::max (n_cnx_cen, nbr_cnx);
						}
					}

					// No nodes in the way
					if (cross_nodes.empty ())
					{
						continue;
					}

					// Moves the intermediate node(s) to the right
					for (auto move_idx : cross_nodes)
					{
						const auto     y_move = _node_list [move_idx]._rank;
						move_node_pushing_others (x, y_move, false);
					}
				} // for dir_cnt
			} // if node
		} // for x
	} // for y

	// Makes sure all coordinates are positive and updates the canvas.
	compute_canvas_width_and_fix_pos ();
}



// Gets the node connections excluding self-referencing ones and back edges.
void	GraphDisp::retrieve_fwd_cnx (Node::CnxList &cnx_list, int node_idx, piapi::Dir dir) const
{
	assert (node_idx >= 0);
	assert (node_idx < get_nbr_nodes ());
	assert (piapi::Dir_is_valid (dir));

	cnx_list.clear ();

	const int      dir_sign = (dir == piapi::Dir_IN) ? -1 : 1;
	const auto &   node     = _node_list [node_idx];
	const auto     rank     = node._rank;
	const auto &   cnx_orig = node._end_list [dir];
	for (const auto cnx_idx : cnx_orig)
	{
		const auto &   cnx = _cnx_list [cnx_idx];
		if (cnx.is_self ())
		{
			// Self reference
			continue;
		}

		const auto     tgt_idx  = cnx._end_arr [piapi::Dir_invert (dir)];
		const auto     tgt_node = _node_list [tgt_idx];
		const auto     tgt_rank = tgt_node._rank;
		const auto     rank_dif = tgt_rank - rank;
		assert (rank_dif != 0);

		// Back edge
		if (rank_dif * dir_sign < 0)
		{
			continue;
		}

		// Validated
		cnx_list.push_back (cnx_idx);
	}
}


// Excludes self-referencing connections
void	GraphDisp::count_cnx () noexcept
{
	for (auto &node : _node_list)
	{
		node._nbr_cnx = 0;
	}

	for (auto &cnx : _cnx_list)
	{
		if (! cnx.is_self ())
		{
			for (int dir_cnt = 0; dir_cnt < piapi::Dir_NBR_ELT; ++dir_cnt)
			{
				const auto     node_idx = cnx._end_arr [dir_cnt];
				++ _node_list [node_idx]._nbr_cnx;
			}
		}
	}
}



// Makes sure all horizontal node positions are positive and start at 0
void	GraphDisp::compute_canvas_width_and_fix_pos ()
{
	assert (! _node_list.empty ());

	const auto     [l_it, r_it] = std::minmax_element (
		_node_list.begin (), _node_list.end (),
		[] (const Node &lhs, const Node &rhs) noexcept
		{
			return (lhs._pos_x < rhs._pos_x);
		}
	);
	const auto     x_min = l_it->_pos_x;
	const auto     x_max = r_it->_pos_x;

	// + _grid_res and not + 1 because we want to keep space on the right.
	_canvas_w = x_max - x_min + _grid_res;

	// Fix coordinates
	if (x_min != 0)
	{
		for (auto &node : _node_list)
		{
			node._pos_x -= x_min;
		}
	}

	// Now fills the grid
	fill_grid ();
}



// Fills a grid with nodes so we can easily locate the neighbours
void	GraphDisp::fill_grid ()
{
	assert (_canvas_w > 0);

	_grid.make_new (_canvas_w, _nbr_ranks);
	for (int node_idx = 0; node_idx < int (_node_list.size ()); ++node_idx)
	{
		const auto &   node = _node_list [node_idx];
		_grid.at (node._pos_x, node._rank) = node_idx;
	}
}



void	GraphDisp::move_node_pushing_others (int x, int y, bool left_flag)
{
	assert (_canvas_w > 0);
	assert (x >= _grid.get_beg ());
	assert (x < _grid.get_end ());
	assert (y >= 0);
	assert (y < _nbr_ranks);
	assert (_grid.at (x, y) >= 0);

	const int      dir = (left_flag) ? -1 : 1;
	const int      end = (left_flag) ? (_grid.get_beg () - 1) : _grid.get_end ();
	int            buf = -1; // Content being inserted/pushed
	int            dist = _grid_res;
	for (int pos = x; pos != end; pos += dir)
	{
		if (buf >= 0)
		{
			_node_list [buf]._pos_x = pos;
		}
		std::swap (buf, _grid.at (pos, y));
		if (buf >= 0)
		{
			dist = 0;
		}
		else
		{
			++ dist;

			// If the distance from the last moved node is enough, we stop
			if (dist >= _grid_res)
			{
				return;
			}
		}
	}

	if (buf >= 0)
	{
		assert (dist == 0);
		_node_list [buf]._pos_x = end;
		_grid.at (end, y) = buf;
	}
}



void	GraphDisp::build_move_tables ()
{
	assert (_canvas_w > 0);
	assert (_grid.get_beg () == 0);

	// Helper structure for the search
	class MoveDir
	{
	public:
		int            _step =  0; // +1 or -1
		Move           _hor  = Move_INVALID;
		Move           _ver  = Move_INVALID;
	};
	constexpr std::array <MoveDir, 2> move_dir_arr =
	{{
		{ -1, Move_L, Move_U },
		{ +1, Move_R, Move_D }
	}};

	auto           wrap = [] (int x, int w) noexcept
		{
			assert (w > 0);
			while (x < 0)
			{
				x += w;
			}
			while (x >= w)
			{
				x -= w;
			}
			return x;
		};

	const int      grid_w  = _grid.get_end ();
	const int      grid_wh = fstb::div_ceil (grid_w, 2);
	for (auto &node : _node_list)
	{
		node._mv_table.fill (-1);

		for (const auto &mv : move_dir_arr)
		{
			// Finds the horizontal neighbour
			for (int dx = 1; dx < grid_w; ++dx)
			{
				const auto     x = wrap (node._pos_x + dx * mv._step, grid_w);
				const auto     tgt_idx = _grid.at (x, node._rank);
				if (tgt_idx >= 0)
				{
					node._mv_table [mv._hor] = tgt_idx;
					break;
				}
			}

			// Vertical neighbour
			for (int dy = 1; dy < _nbr_ranks; ++dy)
			{
				const auto     y = wrap (node._rank + dy * mv._step, _nbr_ranks);
				for (int dx = 0; dx <= grid_wh; ++dx)
				{
					for (const auto &mv_x : move_dir_arr)
					{
						const auto     x = wrap (node._pos_x + dx * mv_x._step, grid_w);
						const auto     tgt_idx = _grid.at (x, y);
						if (tgt_idx >= 0)
						{
							node._mv_table [mv._ver] = tgt_idx;
							dx = grid_w;
							dy = _nbr_ranks;
							break;
						}
					} // for mv_x
				} // for dx
			} // for dy
		} // for mv
	} // for node
}



}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
