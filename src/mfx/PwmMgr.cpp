/*****************************************************************************

        PwmMgr.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/PwmMgr.h"

#include <algorithm>

#include <cassert>



namespace mfx
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: reserve
Description:
	Allocates memory in provision for a given number of PWM channels.
	The actual number of channels is not changed.
	Calling this function helps when set_nbr_chn() has to be called in a real-
	time thread.
Input parameters:
	- nbr_chn: Number of channels to allocate, >= 0
Throws: like std::vector::reserve()
==============================================================================
*/

void	PwmMgr::reserve (int nbr_chn)
{
	assert (nbr_chn >= 0);

	_chn_arr.reserve (nbr_chn);
}



/*
==============================================================================
Name: set_nbr_chn
Description:
	Changes the number of PWM channels.
	New channels are initialised assuming an initial pin value of 0.
Input parameters:
	- nbr_chn: Number of channels, >= 0
Throws: like std::vector::resize()
==============================================================================
*/

void	PwmMgr::set_nbr_chn (int nbr_chn)
{
	assert (nbr_chn >= 0);

	_chn_arr.resize (nbr_chn);
}



/*
==============================================================================
Name: set_period
Description:
	Sets the pulse period for all the channels.
	Default is 10'000.
Input parameters:
	- period: period duration in time units, > 0.
Throws: Nothing
==============================================================================
*/

void	PwmMgr::set_period (int period) noexcept
{
	assert (period > 0);

	_period = period;
}



/*
==============================================================================
Name: set_max_sleep
Description:
	Sets the maximum sleep time between two call to process().
	Use this to set a wake-up period when pins are not switching.
	Default is 10000.
Input parameters:
	- duration: maximum sleeping time in time units, > 0.
Throws: Nothing
==============================================================================
*/

void	PwmMgr::set_max_sleep (int duration) noexcept
{
	assert (duration > 0);

	_max_sleep_time = duration;
}



/*
==============================================================================
Name: set_level
Description:
	Sets the analogue PWM level for a given channel.
	The new value will be taken into account at the next process(), not before.
	This function can be called from any thread.
Input parameters:
	- index: index of the channel, in [0 ; nbr_chn-1]
	- val: value to modulate, in [0 ; 1]
Throws: Nothing
==============================================================================
*/

void	PwmMgr::set_level (int index, float val) noexcept
{
	assert (index >= 0);
	assert (index < int (_chn_arr.size ()));
	assert (val >= 0);
	assert (val <= 1);

	_chn_arr [index]._target = val;
}



/*
==============================================================================
Name: process
Description:
	This function has to be called in a loop alternating with thread sleeps.
Input parameters:
	- timestamp: current clock, in time units. Wrapping arount the 32 bits
		should not be a problem, as long as two consecutive calls are distant
		from less than 2^31 (35 min 47 s if the time unit is the microsecond).
Returns: a structure with two elements:
	- _sleep_time: a suggestion in time units of how long to sleep before the
		next call to this function. It is possible to sleep more or less, but
		large deviations towards higher durations may hurt the signal quality.
		Always > 0.
	- _change_mask: each bit corresponds to a channel. A bit set indicates that
		the channel value has changed and should be reflected on the user side.
		Channels out of the mask (index >= 32) have to be polled manually.
Throws: Nothing
==============================================================================
*/

PwmMgr::ProcResult	PwmMgr::process (uint32_t timestamp) noexcept
{
	ProcResult     res;

	// If for some reason we slept too long, or if the clock changed, or if
	// this is the first call to this function after the object construction,
	// we reset the timebase and start at the beginning of a cycle.
	const auto      tdif    = int32_t (timestamp - _cycle_ts);
	const auto      max_gap = _period << 1;
	if (tdif > max_gap || tdif < 0)
	{
		_cycle_ts    = timestamp;
		_prev_ts     = timestamp - _period;
		_rising_flag = true;

		// Resets all channels
		for (auto &chn : _chn_arr)
		{
			chn._duty    = chn._target * float (_period);
			chn._dc_prev = fstb::round_int (chn._duty);
			chn._a0      = 0;
		}
	}

	// Now adjusts the current cycle origin
	while (int32_t (timestamp - _cycle_ts) >= _period)
	{
		_cycle_ts   += _period;
		_rising_flag = true;
	}
	// At this point, _cycle_ts <= timestamp < _cycle_ts + _period

	const auto     elapsed_since_beg = int32_t (timestamp - _prev_ts);

	// Timestamp of the next period will be the upper limit for the pulse end
	const auto     next_ts = uint32_t (_cycle_ts + _period);

	// Available duration up to the next cycle
	const auto     available = int32_t (next_ts - timestamp);
	assert (available > 0);

	// Default for wake-up time: full cycle
	res._sleep_time = available;

	for (int chn_idx = 0; chn_idx < int (_chn_arr.size ()); ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];

		// Operates on a local variable and stores the final result at the end,
		// so if another thread is reading the state, it doesn't get a temporary
		// value.
		const bool     state_orig_flag = chn._state_flag;
		bool           state_flag      = state_orig_flag;

		// Rising edge
		if (_rising_flag)
		{
			// Terminates the previous cycle if it is still running
			if (state_flag)
			{
				chn._dc_prev = elapsed_since_beg;
				state_flag   = false;
			}

			// Computes the delta-sigma error from the previous cycle data
			const auto     error = chn._duty - float (chn._dc_prev);

			// Computes the new high state duration, if the target level has
			// changed
			chn._duty = chn._target * float (_period);

			// Delta-sigma modulation on the pulse width
			chn._a0 += error;
			const auto     quant_in = chn._duty + chn._a0;
			auto           width    = fstb::round_int (quant_in);

			// Limits the pulse width to the available time
			width = fstb::limit (width, 0, int (available));
			chn._dc_cur = width;

			if (width <= 0)
			{
				chn._dc_prev = 0;
			}
			else
			{
				// Wake-up time to handle the falling edge
				res._sleep_time = std::min (res._sleep_time, width);

				state_flag = true;
			}
		}

		// Checks the falling edge
		else if (state_flag)
		{
			const auto     time_before_edge = chn._dc_cur - elapsed_since_beg;

			// We are too early for the falling edge, wait again
			if (time_before_edge > 0)
			{
				res._sleep_time = std::min (res._sleep_time, time_before_edge);
			}

			// Falling edge, when distinct of the rising edge
			else
			{
				chn._dc_prev = elapsed_since_beg;
				state_flag   = false;
			}
		}

		if (   state_flag != state_orig_flag
		    && chn_idx < ProcResult::_max_nbr_chn_mask)
		{
			chn._state_flag   = state_flag;
			res._change_mask |= uint32_t (1) << chn_idx;
		}
	}

	// Stores the measured timestamp for the rising edge of the current cycle
	if (_rising_flag)
	{
		_prev_ts     = timestamp;
		_rising_flag = false;
	}

	res._sleep_time = std::min (res._sleep_time, int (_max_sleep_time));
	assert (res._sleep_time > 0);

	return res;
}



/*
==============================================================================
Name: get_state
Description:
	Retrieves the binary pin state of a channel.
	This function can be called from any thread.
Input parameters:
	- index: channel index, in [0 ; nbr_chn-1]
Returns: false (0) for a low level or true (1) for a high level.
Throws: Nothing
==============================================================================
*/

bool	PwmMgr::get_state (int index) const noexcept
{
	assert (index >= 0);
	assert (index < int (_chn_arr.size ()));

	return _chn_arr [index]._state_flag;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
