/*****************************************************************************

        Cst.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_Cst_HEADER_INCLUDED)
#define mfx_Cst_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/piapi/PluginInterface.h"
#include "mfx/doc/LayerType.h"

#include <array>
#include <chrono>
#include <ratio>
#include <string>



#if (fstb_SYS == fstb_SYS_LINUX)
#define mfx_Cst_ROOT_DIR "/opt/pedalevite/"
#elif defined (_MSC_VER)
#define mfx_Cst_ROOT_DIR "../../../"
#else // For Mingw on Windows
#define mfx_Cst_ROOT_DIR "../../"
#endif



namespace mfx
{



class Cst
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_pedals      =  12;
	static constexpr int _nbr_prog_per_bank = 16;
	static constexpr int _nbr_banks       =  64;  // Per setup

	static constexpr int _nbr_leds        =   3;
	static constexpr int _nbr_pot_abs     =   3;

	static constexpr int _max_input_param =  24; // Maximum number of input parameter for each type (switches, expression pedals, rotary encoders...)
	static constexpr int _max_nbr_buf     = 256; // Number of buffers in the audio graph processing
	static constexpr int _max_nbr_input   = piapi::PluginInterface::_max_nbr_pins; // Per real plug-in (not dry/wet/bypass mixer)
	static constexpr int _max_nbr_output  = piapi::PluginInterface::_max_nbr_pins; // Per real plug-in (not dry/wet/bypass mixer)
	static constexpr int _max_nbr_sig     =   4;
	static constexpr int _max_nbr_sig_ports = 256;
	static constexpr int _max_nbr_plugins = 256;
	static constexpr int _max_nbr_send    =   4;
	static constexpr int _nbr_chn_inout   =   2; // Max of _nbr_chn_in and _nbr_chn_out
	static constexpr int _nbr_chn_in      = _nbr_chn_inout;
	static constexpr int _nbr_chn_out     = _nbr_chn_inout;
	static constexpr float  _clip_lvl     = 0.999f; // We subtract a tiny margin to 0 dB to be safe

	static constexpr int _tempo_min       =  30;
	static constexpr int _tempo_max       = 240;
	static constexpr int _tempo_ref       = 120;
	static constexpr std::chrono::microseconds    // Maximum time between two tempo pedal actions. Microseconds
	               _tempo_detection_max { std::chrono::seconds (2) };
	static constexpr std::chrono::microseconds    // Minimum time, microseconds
	               _tempo_detection_min { std::chrono::milliseconds (100) };

	static constexpr std::chrono::microseconds    // Microseconds
	                  _key_time_hold { std::chrono::milliseconds (500) };
	static constexpr std::chrono::microseconds    // Microseconds
	                  _key_time_repeat { std::chrono::milliseconds (150) };
	static constexpr int  _max_named_targets = 64;// Maximum number of effects that can be addressed simultaneously in the audio thread for a parameter change, for example

	// Reference potentiometer course, in turns. From 7 h to 5 h on a clock.
	typedef std::ratio <10, 12> PotCourse;

	static constexpr int _rotenc_resol =  24; // Detents per turn, indicative

	// Unit step on normalized parameter value
	// full_param_range = nbr_detents_fullrange * step
	// with:
	// full_param_range = 1
	// nbr_detents_fullrange = _rotenc_resol * PotCourse
	// Therefore:
	// step = 1 / (_rotenc_resol * PotCourse)
	static constexpr double _step_param =
		double (PotCourse::den) / double (PotCourse::num * _rotenc_resol);

	// Number of buffers for the a single side of the global audio I/O
	static constexpr int _nbr_buf_io    = _nbr_chn_inout;

	// One buffer zone per side. Index for each side is not fixed, it depends
	// on the current layer.
	static constexpr int _nbr_io_zones  = 2;

	// Number of buffers, per side, for the return device
	static constexpr int _nbr_buf_ret   = _nbr_chn_inout * _max_nbr_send;

	// One send/return buffer zone per layer, because they are persistent
	// from one audio frame to another.
	static constexpr int _nbr_ret_zones = int (doc::LayerType::NBR_ELT);

	static constexpr std::string_view // Dry-wet mix
	               _plugin_dwm   = "\?drywetmix";
	static constexpr std::string_view
	               _plugin_dly   = "\?delay0";
	static constexpr std::string_view
	               _plugin_tuner = "\?tuner";

	static constexpr std::string_view
	               _empty_prog_name = "<Empty prog>";

	static constexpr std::string_view
	               _config_current  = "current";
	static constexpr std::string_view
	               _config_factory  = "factory-default";
	static constexpr std::string_view
	               _config_dir      = mfx_Cst_ROOT_DIR "etc/config";
	static constexpr std::string_view
	               _font_dir        = mfx_Cst_ROOT_DIR "etc/font";
	static constexpr std::string_view
	               _audiodump_dir   = mfx_Cst_ROOT_DIR "var/audiodump";
	static constexpr std::string_view
	               _log_dir         = mfx_Cst_ROOT_DIR "var/log";
	static constexpr std::string_view
	               _d2d_file        = "raw-in-out.wav";
	static constexpr std::string_view
	               _d2d_file_vid    = "video-rec.pvvid";
	static constexpr std::string_view
	               _rw_cmd_script_pathname = mfx_Cst_ROOT_DIR "bin/cmd_rofs.sh";



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	virtual        ~Cst ()                              = delete;
	               Cst ()                               = delete;
	               Cst (const Cst &other)               = delete;
	Cst &          operator = (const Cst &other)        = delete;
	bool           operator == (const Cst &other) const = delete;
	bool           operator != (const Cst &other) const = delete;

}; // class Cst



}  // namespace mfx



#undef mfx_Cst_ROOT_DIR

//#include "mfx/Cst.hpp"



#endif   // mfx_Cst_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
