/*****************************************************************************

        FileWriteActivity.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ui_FileWriteActivity_HEADER_INCLUDED)
#define mfx_ui_FileWriteActivity_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <atomic>
#include <mutex>



namespace mfx
{
namespace ui
{



class LedInterface;

class FileWriteActivity
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	class Guard
	{
	public:
		               Guard (FileWriteActivity &fwa);
		               ~Guard ();
	private:
		FileWriteActivity &
		               _fwa;
	};

	explicit       FileWriteActivity (ui::LedInterface &led);
	               ~FileWriteActivity ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Index of the LED that will be lit when a file is being written. >= 0
	static constexpr int    _led_idx = 1;

	// Lit LED luminance, ]0 ; 1]
	static constexpr float  _led_lum = 1.f;

	void           start ();
	void           stop ();

	ui::LedInterface &
	               _led;
	std::mutex     _led_mutex;

	// 0 = no activity, > 0 = writing file(s)
	std::atomic <int>
	               _count { 0 };



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               FileWriteActivity ()                               = delete;
	               FileWriteActivity (const FileWriteActivity &other) = delete;
	               FileWriteActivity (FileWriteActivity &&other)      = delete;
	FileWriteActivity &
	               operator = (const FileWriteActivity &other)        = delete;
	FileWriteActivity &
	               operator = (FileWriteActivity &&other)             = delete;
	bool           operator == (const FileWriteActivity &other) const = delete;
	bool           operator != (const FileWriteActivity &other) const = delete;

}; // class FileWriteActivity



}  // namespace ui
}  // namespace mfx



//#include "mfx/ui/FileWriteActivity.hpp"



#endif // mfx_ui_FileWriteActivity_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
