/*****************************************************************************

        DisplayInterface.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"

#include <algorithm>
#include <array>

#include <cassert>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: get_width
Returns: display width in pixels, > 0.
==============================================================================
*/

int	DisplayInterface::get_width () const noexcept
{
	const int      w = do_get_width ();
	assert (w > 0);

	return w;
}



/*
==============================================================================
Name: get_height
Returns: display height in pixels, > 0.
==============================================================================
*/

int	DisplayInterface::get_height () const noexcept
{
	const int      h = do_get_height ();
	assert (h > 0);

	return h;
}



/*
==============================================================================
Name: get_stride
Returns: pitch between two rows in the buffer (distance between two neighbour
pixels of the same column), in pixels. Should be greater or equal to the
display width.
==============================================================================
*/

int	DisplayInterface::get_stride () const noexcept
{
	const int      s = do_get_stride ();
	assert (s >= get_width ());

	return s;
}



/*
==============================================================================
Name: use_screen_buf
Description:
	Returns a pointer on a buffer containing the displayed pixels.
	The pointer is located on the top-left pixel.
	They are ordered in rows from top to bottom, which may have a gap between
	them (stride != width).
	Pixels are assumed opaque, the alpha value is unspecified and should not be
	taken into account.
Returns: a pointer on the screen buffer.
==============================================================================
*/

PixArgb *	DisplayInterface::use_screen_buf () noexcept
{
	PixArgb *      buf_ptr = do_use_screen_buf ();
	assert (buf_ptr != nullptr);

	return buf_ptr;
}



const PixArgb *	DisplayInterface::use_screen_buf () const noexcept
{
	const PixArgb *   buf_ptr = do_use_screen_buf ();
	assert (buf_ptr != nullptr);

	return buf_ptr;
}



/*
==============================================================================
Name: refresh
Description:
	Notifies the displaying device that the specified rectangle is made of
	fresh data and should be physically displayed or refreshed.
Input parameters:
	- x, y: coordinates of the top-left corner of the rectangle to be refreshed
		The coordinates should be valid (in the display area).
	- w, h: width and height of the rectangle to be refreshed. All pixels from
		the rectangle should be valid.
Throws: ?
==============================================================================
*/

void	DisplayInterface::refresh (int x, int y, int w, int h)
{
	assert (x >= 0);
	assert (y >= 0);
	assert (w > 0);
	assert (h > 0);
	assert (x + w <= get_width ());
	assert (y + h <= get_height ());

	do_refresh (x, y, w, h);
}



/*
==============================================================================
Name: force_reset
Description:
	Restarts the device and makes sure the current buffer is displayed
	correctly.
Throws: ?
==============================================================================
*/

void	DisplayInterface::force_reset ()
{
	do_force_reset ();
}



/*
==============================================================================
Name: bitblt
Description:
	Copies a picture onto the display buffer. The operation merges the picture
	with the background using the specified blending mode.
	Input pixels are monochromatic and their value specifies the opacity (in
	0-255) applied to the color parameter.
	If the color is not fully opaque, both opacities are multiplied.
	Destination is assumed fully opaque, alpha values are ignored.
	Source data can legitimately be located anywhere relative to the pointer.
Input parameters:
	- xd, yd: top-left coordinates of the destination in pixels. Must be valid
		in the display buffer.
	- src_ptr: pointer on the origin of the source picture.
	- xs, ys: coordinates of the top-left source block relative to the source
		origin, in pixels.
	- ws, hs: size of the source block, in pixels. The block should fit
		entierly in the display buffer.
	- ss: stride of the source, in pixels.
	- mode: blending mode between the source and the background.
	- color: picture color, transparency is valid.
==============================================================================
*/

void	DisplayInterface::bitblt (int xd, int yd, const uint8_t * fstb_RESTRICT src_ptr, int xs, int ys, int ws, int hs, int ss, BlendMode mode, PixArgb color) noexcept
{
	assert (xd >= 0);
	assert (xd + ws <= get_width ());
	assert (yd >= 0);
	assert (yd + hs <= get_height ());
	assert (src_ptr != nullptr);
	assert (ws >= 0);
	assert (hs >= 0);
	assert (ws <= ss);

	if (hs == 0 || ws == 0)
	{
		return;
	}

	const int      sd      = get_stride ();
	PixArgb *      dst_ptr = use_screen_buf ();

	src_ptr += ys * ss + xs;
	dst_ptr += yd * sd + xd;

	// Implements (value * (color_component + 1)) >> 8 to approximate
	// value * color_component / 255 so we get:
	// 255 * 255 -> 255
	//   0 *   ? -> 0
	const std::array <int, 3> bgr {
		color._b + 1,
		color._g + 1,
		color._r + 1
	};
	const auto     alpha    = color._a + 1;
	auto           make_pix = [&bgr] (uint8_t v) noexcept
	{
		return PixArgb {
			uint8_t ((bgr [0] * v) >> 8),
			uint8_t ((bgr [1] * v) >> 8),
			uint8_t ((bgr [2] * v) >> 8),
			255
		};
	};

	for (int y = 0; y < hs; ++y)
	{
		switch (mode)
		{
		case BlendMode_XOR:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], make_pix (src_ptr [x]),
					[] (uint8_t d, uint8_t s) noexcept { return d ^ s; }
				);
			}
			break;
		case BlendMode_MIN:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], make_pix (src_ptr [x]),
					[] (uint8_t d, uint8_t s) noexcept { return std::min (d, s); }
				);
			}
			break;
		case BlendMode_MAX:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], make_pix (src_ptr [x]),
					[] (uint8_t d, uint8_t s) noexcept { return std::max (d, s); }
				);
			}
			break;
		case BlendMode_TRANSP:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], make_pix (src_ptr [x]),
					[alpha] (uint8_t d, uint8_t s) noexcept {
						return d + ((alpha * (s - d)) >> 8);
					}
				);
			}
			break;
		case BlendMode_ALPHA:
			for (int x = 0; x < ws; ++x)
			{
				const auto       v = src_ptr [x] + 1;
				dst_ptr [x] = op2 (
					dst_ptr [x], color,
					[v] (uint8_t d, uint8_t s) noexcept {
						return d + ((v * (s - d)) >> 8);
					}
				);
			}
			break;
		case BlendMode_OPAQUE:
		default:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = make_pix (src_ptr [x]);
			}
			break;
		}

		src_ptr += ss;
		dst_ptr += sd;
	}
}



/*
==============================================================================
Name: bitblt
Description:
	Copies a picture onto the display buffer. The operation merges the picture
	with the background using the specified blending mode.
	Destination is assumed fully opaque, alpha values are ignored.
	ss source stride in pixels
	BlendMode_ALPHA is the same as BlendMode_TRANSP (the latter is preferred)
	Source data can legitimately be located anywhere relative to the pointer.
Input parameters:
	- xd, yd: top-left coordinates of the destination in pixels. Must be valid
		in the display buffer.
	- src_ptr: pointer on the origin of the source picture.
	- xs, ys: coordinates of the top-left source block relative to the source
		origin, in pixels.
	- ws, hs: size of the source block, in pixels. The block should fit
		entierly in the display buffer.
	- ss: stride of the source, in pixels.
	- mode: blending mode between the source and the background.
==============================================================================
*/

void	DisplayInterface::bitblt (int xd, int yd, const PixArgb * fstb_RESTRICT src_ptr, int xs, int ys, int ws, int hs, int ss, BlendMode mode) noexcept
{
	assert (xd >= 0);
	assert (xd + ws <= get_width ());
	assert (yd >= 0);
	assert (yd + hs <= get_height ());
	assert (src_ptr != nullptr);
	assert (ws >= 0);
	assert (hs >= 0);
	assert (ws <= ss);

	if (hs == 0 || ws == 0)
	{
		return;
	}

	const int      sd      = get_stride ();
	PixArgb *      dst_ptr = use_screen_buf ();

	src_ptr += ys * ss + xs;
	dst_ptr += yd * sd + xd;

	if (ws == sd && ws == ss && mode == BlendMode_OPAQUE)
	{
		std::copy (src_ptr, src_ptr + ws * hs, dst_ptr);
		return;
	}

	for (int y = 0; y < hs; ++y)
	{
		switch (mode)
		{
		case BlendMode_XOR:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], src_ptr [x],
					[] (uint8_t d, uint8_t s) noexcept { return d ^ s; }
				);
			}
			break;
		case BlendMode_MIN:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], src_ptr [x],
					[] (uint8_t d, uint8_t s) noexcept { return std::min (d, s); }
				);
			}
			break;
		case BlendMode_MAX:
			for (int x = 0; x < ws; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], src_ptr [x],
					[] (uint8_t d, uint8_t s) noexcept { return std::max (d, s); }
				);
			}
			break;
		case BlendMode_TRANSP:
		case BlendMode_ALPHA:
			for (int x = 0; x < ws; ++x)
			{
				const auto     alpha = src_ptr [x]._a + 1;
				dst_ptr [x] = op2 (
					dst_ptr [x], src_ptr [x],
					[alpha] (uint8_t d, uint8_t s) noexcept {
						return d + ((alpha * (s - d)) >> 8);
					}
				);
			}
			break;
		case BlendMode_OPAQUE:
		default:
			std::copy (src_ptr, src_ptr + ws, dst_ptr);
			break;
		}

		src_ptr += ss;
		dst_ptr += sd;
	}
}



/*
==============================================================================
Name: fill
Description:
	Fills a rectangle of the display buffer with a solid color.
	The operation merges the color with the background using the specified
	blending mode.
	Destination is assumed fully opaque, alpha values are ignored.
Input parameters:
	- xd, yd: top-left coordinates of the rectangle in pixels. Must be valid
		in the display buffer.
	- w, h: size of the block, in pixels. The block should fit entierly in the
		display buffer.
	- color: filling color, transparency is valid
	- mode: blending mode between the source and the background.
==============================================================================
*/

void	DisplayInterface::fill (int xd, int yd, int w, int h, PixArgb color, BlendMode mode) noexcept
{
	assert (xd >= 0);
	assert (xd + w <= get_width ());
	assert (yd >= 0);
	assert (yd + h <= get_height ());
	assert (w >= 0);
	assert (h >= 0);

	if (h == 0 || w == 0)
	{
		return;
	}

	const int      sd      = get_stride ();
	PixArgb *      dst_ptr = use_screen_buf ();

	dst_ptr += yd * sd + xd;

	if (   color._a == 255
	    && (mode == BlendMode_TRANSP || mode == BlendMode_ALPHA))
	{
		mode = BlendMode_OPAQUE;
	}

	const auto     alpha = color._a + 1;

	if (w == sd && mode == BlendMode_OPAQUE)
	{
		std::fill (dst_ptr, dst_ptr + w * h, color);
		return;
	}

	for (int y = 0; y < h; ++y)
	{
		switch (mode)
		{
		case BlendMode_XOR:
			for (int x = 0; x < w; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], color,
					[] (uint8_t d, uint8_t s) noexcept { return d ^ s; }
				);
			}
			break;
		case BlendMode_MIN:
			for (int x = 0; x < w; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], color,
					[] (uint8_t d, uint8_t s) noexcept { return std::min (d, s); }
				);
			}
			break;
		case BlendMode_MAX:
			for (int x = 0; x < w; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], color,
					[] (uint8_t d, uint8_t s) noexcept { return std::max (d, s); }
				);
			}
			break;
		case BlendMode_TRANSP:
		case BlendMode_ALPHA:
			for (int x = 0; x < w; ++x)
			{
				dst_ptr [x] = op2 (
					dst_ptr [x], color,
					[alpha] (uint8_t d, uint8_t s) noexcept {
						return d + ((alpha * (s - d)) >> 8);
					}
				);
			}
			break;
		case BlendMode_OPAQUE:
		default:
			std::fill (dst_ptr, dst_ptr + w, color);
			break;
		}

		dst_ptr += sd;
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename F>
PixArgb	DisplayInterface::op2 (const PixArgb &dst, const PixArgb &src, F f) noexcept
{
	return PixArgb {
		uint8_t (f (dst._b, src._b)),
		uint8_t (f (dst._g, src._g)),
		uint8_t (f (dst._r, src._r)),
		255
	};
}



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
