/*****************************************************************************

        Font.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/ui/Font.h"

#include <cassert>
#include <cstring>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: init
Description:
	Initialises the font from a bitmap in memory. All the stored glyphs
	have the same width. The picture may have several lines of glyphs.
	Pixel are unsigned 8-bit greyscale (0 = transp, max_val = 100% opaque).
	If max_val is 255 and there is no zoom, provided data may be used
	permanently (no local copy in the object).
Input parameters:
	- nbr_char: number of glyphs in the bitmap, > 0. There may be a few
		unused ones at the end of the last char line.
	- char_w: width of a glyph in pixels, > 0
	- char_h: height of a glyph in pixels, > 0
	- char_per_row: number of glyph per row in the picture
	- stride: picture width in pixels, >= char_w * char_per_row.
	- pic_arr: pointer on the pixel data. There should be at least
		char_h * ((nbr_char + char_per_row - 1) / char_per_row) * stride
		pixels. Data may have to be persistent if the function returns false
		(no copy).
		All pixel values should be <= max_val.
	- unicode_arr: pointer on a list of nbr_char UCS-4 code points
		corresponding to each glyph.
	- baseline: ordinate of the baseline in the font, in [1 ; char_h].
		This is the ordinate of the line on which the glyphs are "sitting"
		(not the bottom pixel of the glyphs).
	- max_val: pixel value corresponding to 100% opacity.
	- copy_data_flag: force data copy, even if the picture can remain stored
		externally.
	- zoom_h: horizontal zoom when initialising the font, >= 1.
	- zoom_v: vertical zoom when initialising the font, >= 1.
Returns: true if data has been copied, then the buffer pointed by pic_arr can
	be released. false if pic_arr is just a reference and pointed data will
	be used during the object lifetime.
Throws: std::vector related exceptions
==============================================================================
*/

bool	Font::init (int nbr_char, int char_w, int char_h, int char_per_row, int stride, const uint8_t pic_arr [], const char32_t unicode_arr [], int baseline, int max_val, bool copy_data_flag, int zoom_h, int zoom_v)
{
	assert (nbr_char > 0);
	assert (nbr_char <= 0x7FFF);
	assert (char_w > 0);
	assert (char_h > 0);
	assert (char_per_row > 0);
	assert (stride > 0);
	assert (pic_arr != nullptr);
	assert (unicode_arr != nullptr);
	assert (baseline > 0);
	assert (baseline <= char_h);
	assert (max_val > 0);
	assert (max_val <= 255);
	assert (zoom_h > 0);
	assert (zoom_v > 0);

	_nbr_char   = nbr_char;
	_baseline   = zoom_v * baseline;
	_bold_shift = zoom_h;
	_prop_flag  = false;
	if (copy_data_flag || max_val != 255 || zoom_h != 1 || zoom_v != 1)
	{
		copy_data_flag = true;
		_char_h        = char_h * zoom_v;
		_stride        = char_w * zoom_h;
		_data_arr.resize (_nbr_char * _stride * _char_h);
		int            pos_dst = 0;

		for (int c = 0; c < nbr_char; ++c)
		{
			const int      row     = c / char_per_row;
			const int      col     = c - row * char_per_row;
			int            pos_src = row * char_h * stride + col * char_w;
			for (int cy = 0; cy < char_h; ++cy)
			{
				if (zoom_h == 1)
				{
					for (int cx = 0; cx < char_w; ++cx)
					{
						_data_arr [pos_dst + cx] =
							uint8_t (pic_arr [pos_src + cx] * 255 / max_val);
					}
				}
				else
				{
					for (int cx = 0; cx < char_w; ++cx)
					{
						const uint8_t  v =
							uint8_t (pic_arr [pos_src + cx] * 255 / max_val);
						for (int zx = 0; zx < zoom_h; ++zx)
						{
							_data_arr [pos_dst + cx * zoom_h + zx] = v;
						}
					}
				}

				pos_dst += _stride;
				pos_src +=  stride;

				for (int zy = 1; zy < zoom_v; ++zy)
				{
					memcpy (
						&_data_arr [pos_dst          ],
						&_data_arr [pos_dst - _stride],
						_stride * sizeof (_data_arr [0])
					);
					pos_dst += _stride;
				}
			}
		}

		_data_ptr    = _data_arr.data ();
		char_per_row = 1;
	}
	else
	{
		_char_h      = char_h;
		_stride      = stride;
		PicData ().swap (_data_arr);
		_data_ptr    = pic_arr;
	}

	char_w *= zoom_h;
	char_h *= zoom_v;

	// Fills glyph information
	GlyphInfo      gi;
	gi._width = char_w;
	_glyph_arr.assign (nbr_char, gi);
	for (int c = 0; c < nbr_char; ++c)
	{
		const int      row     = c / char_per_row;
		const int      col     = c - row * char_per_row;
		const int      pos_src = row * char_h * _stride + col * char_w;
		_glyph_arr [c]._data_index = pos_src;
	}

	// Character mapping
	ZoneArray ().swap (_zone_arr);
	for (int c = 0; c < nbr_char; ++c)
	{
		add_char (unicode_arr [c], c);
	}

	return copy_data_flag;
}



/*
==============================================================================
Name: init
Description:
	Initialises the font from a raw uncompressed picture file (no header).
	The picture is made of nbr_char glyphs aligned from left to right.
	There is an extra line at the bottom to indicate the glyph width.
	This line alternates between 0 and 255 at each glyph.
	pic_w is the total width of the picture (sum of all glyph widths), and
	pic_h its height. Therefore the font height is pic_h - 1.
	Pixels are unsigned 8-bit greyscale (0 = transparent, 255 = 100% opaque).
Input parameters:
	- filename: filename, possibly with its full path, of the picture file.
		Encoding is UTF-8.
	- nbr_char: number of glyphs contained in the file, > 0
	- pic_w: picture width in pixels, > 0
	- pic_h: picture height in pixels, > 0
	- unicode_arr: pointer on a list of nbr_char UCS-4 code points
		corresponding to each glyph.
	- baseline: ordinate of the baseline in the font, in [1 ; char_h].
		This is the ordinate of the line on which the glyphs are "sitting"
		(not the bottom pixel of the glyphs).
Returns:
	0 if everything went OK.
	A negative number on error. The font should be assumed not initialised.
Throws: std::vector related exceptions
==============================================================================
*/

int	Font::init (std::string filename, int nbr_char, int pic_w, int pic_h, const char32_t unicode_arr [], int baseline)
{
	assert (! filename.empty ());
	assert (nbr_char > 0);
	assert (nbr_char <= 0x7FFF);
	assert (pic_w >= nbr_char);
	assert (pic_h > 1);
	assert (unicode_arr != nullptr);
	assert (baseline > 0);
	assert (baseline <= pic_h - 1);

	int            ret_val = 0;

	_data_ptr  = nullptr;

	_nbr_char  = nbr_char;
	_baseline  = baseline;
	_prop_flag = true;

	_char_h    = pic_h - 1;
	_stride    = pic_w;
	_data_arr.resize (_stride * _char_h);
	PicData        width_code (_stride);

	// Loads file
	FILE *         file_ptr = fstb::fopen_utf8 (filename.c_str (), "rb");
	if (file_ptr == nullptr)
	{
		ret_val = -1;
	}

	// Main data
	if (ret_val == 0)
	{
		const size_t   len_read = fread (
			_data_arr.data (),
			sizeof (_data_arr [0]),
			_data_arr.size (),
			file_ptr
		);
		if (len_read != _data_arr.size ())
		{
			ret_val = -1;
		}
	}

	// Additional line indicating the width of each character
	if (ret_val == 0)
	{
		const size_t   len_read = fread (
			width_code.data (),
			sizeof (width_code [0]),
			width_code.size (),
			file_ptr
		);
		if (len_read != width_code.size ())
		{
			ret_val = -1;
		}
	}

	if (file_ptr != nullptr)
	{
		fclose (file_ptr);
		file_ptr = nullptr;
	}

	// Builds data
	if (ret_val == 0)
	{
		GlyphInfoArray (nbr_char).swap (_glyph_arr);

		int            pos_dst = 0;
		for (int c = 0; c < nbr_char && pos_dst < _stride; ++c)
		{
			// Finds the character length
			uint8_t        color = width_code [pos_dst];
			int            width = 0;
			while (   pos_dst + width < _stride
			       && width_code [pos_dst + width] == color)
			{
				++ width;
			}

			GlyphInfo &    gi = _glyph_arr [c];
			gi._width      = width;
			gi._data_index = pos_dst;

			pos_dst += width;
		}

		if (pos_dst != _stride)
		{
			ret_val = -1;
		}
	}

	if (ret_val == 0)
	{
		_bold_shift = std::max (pic_w / (nbr_char * 8), 1);
		_data_ptr   = _data_arr.data ();

		// Character mapping
		ZoneArray ().swap (_zone_arr);
		for (int c = 0; c < nbr_char; ++c)
		{
			add_char (unicode_arr [c], c);
		}
	}

	return ret_val;
}



/*
==============================================================================
Name: add_char
Description:
	Manual insertion of a new UCS-4 -> glyph index mapping.
	This possibly replaces an existing entry.
Input parameters:
	- ucs4: UCS-4 code point
	- index: glyph position in the storage, >= 0
Throws: memory allocation related exceptions
==============================================================================
*/

void	Font::add_char (char32_t ucs4, int index)
{
	assert (is_ready ());
	assert (index >= 0);
	assert (index < _nbr_char);

	const size_t   zone_idx = ucs4 >> _zone_bits;
	if (zone_idx >= _zone_arr.size ())
	{
		_zone_arr.resize (zone_idx + 1);
	}
	Zone *         zone_ptr = _zone_arr [zone_idx].get ();
	if (zone_ptr == nullptr)
	{
		_zone_arr [zone_idx] = std::make_unique <Zone> ();
		zone_ptr = _zone_arr [zone_idx].get ();
		for (size_t i = 0; i < zone_ptr->size (); ++i)
		{
			(*zone_ptr) [i] = _not_found;
		}
	}

	const int      loc = ucs4 & _zone_mask;
	(*zone_ptr) [loc] = int16_t (index);
}



/*
==============================================================================
Name: is_ready
Returns: true if the font has been sucessfully initialised.
==============================================================================
*/

bool	Font::is_ready () const noexcept
{
	return (_data_ptr != nullptr);
}



/*
==============================================================================
Name: is_existing
Description:
	Check if the glyph corresponding to a given UCS-4 code point is present.
Input parameters:
	- ucs4: UCS-4 code point
Returns: true if it exists
==============================================================================
*/

bool	Font::is_existing (char32_t ucs4) const noexcept
{
	assert (is_ready ());

	return (get_char_pos (ucs4) >= 0);
}



/*
==============================================================================
Name: get_baseline
Returns: the baseline ordinate, in pixels, > 0.
==============================================================================
*/

int	Font::get_baseline () const noexcept
{
	assert (is_ready ());

	return _baseline;
}



/*
==============================================================================
Name: get_char_h
Returns: glyph height in pixels, > 0.
==============================================================================
*/

int	Font::get_char_h () const noexcept
{
	assert (is_ready ());

	return _char_h;
}



/*
==============================================================================
Name: get_char_w
Description:
	Retrieve the width of a UCS-4 character in pixels.
Input parameters:
	- ucs4: UCS-4 code point
Returns: width in pixels, > 0
==============================================================================
*/

int	Font::get_char_w (char32_t ucs4) const noexcept
{
	assert (is_ready ());

	const int      g_idx = get_char_pos_no_fail (ucs4);
	const GlyphInfo & gl_info = _glyph_arr [g_idx];
	const int      w     = gl_info._width;
	assert (w > 0);

	return w;
}



/*
==============================================================================
Name: get_bold_shift
Returns: the offset in pixels for duplicating a glyph in order to make the
	font bold. > 0.
==============================================================================
*/

int	Font::get_bold_shift () const noexcept
{
	assert (is_ready ());

	return _bold_shift;
}



/*
==============================================================================
Name: render_char
Description:
	Renders a character in a 8-bit pixel buffer.
	Previous content is erased on the character area.
	0 is transparent and 255 full opacity.
	std::codecvt <char32_t, char, std::mbstate_t> can be used to convert from
	UTF-8.
Input parameters:
	- ucs4: character code point in UCS-4.
	- dst_stride: offset between lines in pixels.
Input/output parameters:
	- buf_ptr: pointer on the buffer. Should be large enough to host the glyph.
==============================================================================
*/

void	Font::render_char (uint8_t *buf_ptr, char32_t ucs4, int dst_stride) const noexcept
{
	assert (is_ready ());
	assert (buf_ptr != nullptr);

	const int      c       = get_char_pos_no_fail (ucs4);
	const GlyphInfo & gi   = _glyph_arr [c];
	const int      char_w  = gi._width;
	assert (dst_stride >= char_w);
	const uint8_t* src_ptr = _data_ptr + gi._data_index;

	for (int y = 0; y < _char_h; ++y)
	{
		for (int x = 0; x < char_w; ++x)
		{
			buf_ptr [x] = src_ptr [x];
		}
		buf_ptr += dst_stride;
		src_ptr += _stride;
	}
}



/*
==============================================================================
Name: render_char
Description:
	Renders a character in a 8-bit pixel buffer with magnification.
	Previous content is erased on the character area.
	0 is transparent and 255 full opacity.
	Magnification is done by pixel duplication.
	std::codecvt <char32_t, char, std::mbstate_t> can be used to convert from
	UTF-8.
Input parameters:
	- ucs4: character code point in UCS-4.
	- dst_stride: offset between lines in pixels.
	- mag_x: horizontal magnification, >= 1
	- mag_y: vertical magnification, >= 1
Input/output parameters:
	- buf_ptr: pointer on the buffer. Should be large enough to host the
		magnified glyph.
==============================================================================
*/

void	Font::render_char (uint8_t *buf_ptr, char32_t ucs4, int dst_stride, int mag_x, int mag_y) const noexcept
{
	assert (is_ready ());
	assert (buf_ptr != nullptr);
	assert (mag_x > 0);
	assert (mag_y > 0);

	if (mag_x == 1 && mag_y == 1)
	{
		render_char (buf_ptr, ucs4, dst_stride);
	}
	else
	{
		const int      c       = get_char_pos_no_fail (ucs4);
		const GlyphInfo & gi   = _glyph_arr [c];
		const int      char_w  = gi._width;
		assert (dst_stride >= char_w);
		const uint8_t* src_ptr = _data_ptr + gi._data_index;

		for (int y = 0; y < _char_h; ++y)
		{
			for (int x = 0; x < char_w; ++x)
			{
				const uint8_t  v      = src_ptr [x];
				const int      x_base = x * mag_x;
				for (int x2 = 0; x2 < mag_x; ++x2)
				{
					buf_ptr [x_base + x2] = v;
				}
			}
			buf_ptr += dst_stride;
			src_ptr += _stride;
			for (int y2 = 1; y2 < mag_y; ++y2)
			{
				for (int x = 0; x < char_w * mag_x; ++x)
				{
					buf_ptr [x] = buf_ptr [x - dst_stride];
				}
				buf_ptr += dst_stride;
			}
		}
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Returns -1 if not found
int	Font::get_char_pos (char32_t ucs4) const noexcept
{
	assert (is_ready ());

	int            pos = _not_found;

	const size_t   zone_idx = ucs4 >> _zone_bits;
	if (zone_idx < _zone_arr.size ())
	{
		const Zone &   zone = *(_zone_arr [zone_idx]);
		const int      loc  = ucs4 & _zone_mask;
		pos = zone [loc];
	}

	return pos;
}



// Replaces unmapped characters with the one at position 0
int	Font::get_char_pos_no_fail (char32_t ucs4) const noexcept
{
	return std::max (get_char_pos (ucs4), 0);
}



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
