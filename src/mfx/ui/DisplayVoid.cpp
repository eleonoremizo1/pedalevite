/*****************************************************************************

        DisplayVoid.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayVoid.h"

#include <cassert>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	DisplayVoid::do_get_width () const noexcept
{
	return _scr_w;
}



int	DisplayVoid::do_get_height () const noexcept
{
	return _scr_h;
}



int	DisplayVoid::do_get_stride () const noexcept
{
	return _scr_w;
}



ui::PixArgb *	DisplayVoid::do_use_screen_buf () noexcept
{
	return _buffer.data ();
}



const ui::PixArgb *	DisplayVoid::do_use_screen_buf () const noexcept
{
	return _buffer.data ();
}



void	DisplayVoid::do_refresh (int /*x*/, int /*y*/, int /*w*/, int /*h*/)
{
	// Nothing
}



void   DisplayVoid::do_force_reset ()
{
	// Nothing
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
