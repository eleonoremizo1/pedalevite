/*****************************************************************************

        FileWriteActivity.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/FileWriteActivity.h"
#include "mfx/ui/LedInterface.h"

#include <thread>

#include <cassert>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



FileWriteActivity::Guard::Guard (FileWriteActivity &fwa)
:	_fwa (fwa)
{
	_fwa.start ();
}



FileWriteActivity::Guard::~Guard ()
{
	_fwa.stop ();
}



FileWriteActivity::FileWriteActivity (ui::LedInterface &led)
:	_led (led)
{
	assert (_led_idx < _led.get_nbr_led ());
}



FileWriteActivity::~FileWriteActivity ()
{
	assert (_count.load () == 0);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	FileWriteActivity::start ()
{
	std::lock_guard <std::mutex>  led_guard (_led_mutex);

	assert (_count.load () >= 0);
	++ _count;
	if (_count.load () == 1)
	{
		_led.set_led (_led_idx, _led_lum);
	}
}



void	FileWriteActivity::stop ()
{
	std::lock_guard <std::mutex>  led_guard (_led_mutex);

	assert (_count.load () > 0);
	-- _count;
	if (_count == 0)
	{
		_led.set_led (_led_idx, 0.f);
	}
}



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
