/*****************************************************************************

        PotEndlessDelta.h
        Author: Laurent de Soras, 2024

Wrapper around PotEndless to get filtered delta values.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ui_PotEndlessDelta_HEADER_INCLUDED)
#define mfx_ui_PotEndlessDelta_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/ctrl/Hysteresis.h"
#include "mfx/dsp/ctrl/Smooth.h"
#include "mfx/ui/PotEndless.h"



namespace mfx
{
namespace ui
{



class PotEndlessDelta
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr float _delta_min = 1e-4f;
	static constexpr int   _resol     = PotEndless::_resol;

	void           set_sample_freq (float fs) noexcept;
	void           set_sensitivity (float sens) noexcept;
	void           set_hysteresis (float hyst) noexcept;
	void           set_safety_margin (uint16_t thr) noexcept;
	void           set_delta_min (float delta_min) noexcept;

	float          process_sample (const PotEndless::PotVal &sval) noexcept;

	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Input range representing a single full turn.
	static constexpr float  _scale = float (PotEndless::_range);

	PotEndless     _pot;
	mfx::dsp::ctrl::Smooth
	               _smoother;
	mfx::dsp::ctrl::Hysteresis <float>
	               _hysteresis;

	// Below this value, output is truncated to 0 (scale: 1 = full turn)
	float          _delta_min_raw = 1e-4f * _scale;

	// Latest absolute position we use as reference to compute the delta.
	float          _y_old         = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PotEndlessDelta &other) const = delete;
	bool           operator != (const PotEndlessDelta &other) const = delete;

}; // class PotEndlessDelta



}  // namespace ui
}  // namespace mfx



//#include "mfx/ui/PotEndlessDelta.hpp"



#endif // mfx_ui_PotEndlessDelta_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
