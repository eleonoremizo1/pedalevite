/*****************************************************************************

        PotEndlessDelta.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/ui/PotEndlessDelta.h"

#include <cassert>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_sample_freq
Description:
	Sets the ADC sampling rate. This is actually the rate at which the
	process_sample() function is called.
Input parameters:
	- fs: Sampling rate in Hz, > 0.
==============================================================================
*/

void	PotEndlessDelta::set_sample_freq (float fs) noexcept
{
	assert (fs > 0);

	_smoother.set_base_freq (2.f);
	_smoother.set_sample_freq (fs);
}



/*
==============================================================================
Name: set_sensitivity
Description:
	Sets the filter adaptivity to changes. See mfx::dsp::ctrl::Smooth for
	details.
Input parameters:
	- sens: Sensitivity, >= 0.
==============================================================================
*/

void	PotEndlessDelta::set_sensitivity (float sens) noexcept
{
	assert(sens >= 0);

	_smoother.set_sensitivity (sens);
}



/*
==============================================================================
Name: set_hysteresis
Description:
	Sets the hysteresis amount. Corresponds to something just above the peak-
	to-peak value of the remaining noise after dynamic filtering.
	This value may be obtained by automatic calibration.
Input parameters:
	- hyst: Amount of hysteresis, in the raw input scale. >= 0.
==============================================================================
*/

void	PotEndlessDelta::set_hysteresis (float hyst) noexcept
{
	assert (hyst >= 0);

	_hysteresis.set_hyst (hyst);
}



/*
==============================================================================
Name: set_safety_margin
Description:
	Sets the range where the potentiometer values are considered inaccurate
	for position detection. See detailed description in PotEndless.
Input parameters:
- thr: threshold above/below which potentiometer 16-bit values are considered
	valid. Must be in [0 ; 0x4000]
==============================================================================
*/

void	PotEndlessDelta::set_safety_margin (uint16_t thr) noexcept
{
	_pot.set_safety_margin (thr);
}



/*
==============================================================================
Name: set_delta_min
Description:
	Sets the threshold below which we ignore the smallest changes.
	This helps reducing the trail of the smoothing filter, and acts as an
	additional filter for noise that could pass through the smoother and
	hysteresis.
	Note that the multiple small changes that are ignored in process_sample()
	accumulate internally, there is no loss of information, just "grouping".
Input parameters:
	- delta_min: the minimum angular change, in turns.
==============================================================================
*/

void	PotEndlessDelta::set_delta_min (float delta_min) noexcept
{
	assert (delta_min >= 0);
	assert (delta_min <= 1);

	_delta_min_raw = delta_min * _scale;
}



/*
==============================================================================
Name: process_sample
Description:
	Sets the new quadrature potentiometer values, processes the angular value
	(smoothing and hysteresis) and returns a possible movement (delta) relative
	to the position of a previous call.
Input parameters:
	- sval: raw potentiometer values, in [0 ; 0xFFFF].
	See PotEndless::set_dualpot_val () for more details.
Returns: the delta, in turns.
==============================================================================
*/

float	PotEndlessDelta::process_sample (const PotEndless::PotVal &sval) noexcept
{
	// Gets the full position with all the turns
	_pot.set_dualpot_val (sval);
	const auto     pos = _pot.get_pos ();
	const auto     x   = pos._frac + (pos._idx << PotEndless::_resol);

	// Smoothing and hysteresis
	auto           y   = _smoother.process_sample (float (x));
	y = _hysteresis.process_sample (y);

	// Converts to relative movements
	auto           delta = y - _y_old;

	// Makes sure the delta is significant (cuts the trail of the smoothing)
	if (fstb::is_null (delta, _delta_min_raw))
	{
		delta = 0;
	}
	else
	{
		_y_old = y;
	}

	// Scales the result to get the value in turns
	constexpr auto scale_inv = 1.f / _scale;
	const auto     out = delta * scale_inv;

	return out;
}



/*
==============================================================================
Name: clear_buffers
Description:
==============================================================================
*/

void	PotEndlessDelta::clear_buffers () noexcept
{
	_smoother.clear_buffers ();
	_y_old = _hysteresis.get_val ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
