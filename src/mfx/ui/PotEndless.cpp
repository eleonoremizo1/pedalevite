/*****************************************************************************

        PotEndless.cpp
        Author: Laurent de Soras, 2022

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/PotEndless.h"

#include <algorithm>

#include <cassert>
#include <cstdlib>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: operator +=
Description:
	Adjust the current logical position with a increment.
Input parameters:
- delta: fractional delta, can be negative. 1<<16 is a full round forward.
==============================================================================
*/

PotEndless::Pos &	PotEndless::Pos::operator += (int delta) noexcept
{
	delta += _frac;
	_idx  += delta >> _resol;
	if constexpr (_resol < 16)
	{
		delta &= _range - 1;
	}
	_frac  = uint16_t (delta);

	return *this;
}



/*
==============================================================================
Name: set_safety_margin
Description:
	Sets the range where the potentiometer values are considered inaccurate
	for position detection (top and bottom of the full 16-bit value range).
	A good value is about 1/16 (0x1000), giving a long transition between
	both tapers with a slightly mismatched quadrature or lag caused by the
	circuit capacitance, and fixing small voltage boundary errors.
	If the margin is 0, the weighting is bypassed.
Input parameters:
- thr: threshold above/below which potentiometer 16-bit values are considered
	valid. Must be in [0 ; 0x4000]
==============================================================================
*/

void	PotEndless::set_safety_margin (uint16_t thr) noexcept
{
	assert (thr <= _range_q);

	_thr = thr;
	_rng = compute_confidence_range (_thr);
	_mul = compute_confidence_mult (_rng);
}



/*
==============================================================================
Name: set_dualpot_val
Description:
	Sets the new quadrature potentiometer values.
	Updates the angular position; it is valid only when step between two
	updates is smaller than a half round.
	It is probably better to feed this function with raw, unsmoothed values.
	Smoothing the output angle can be done as a second step if required.
Input parameters:
- val: potentiometer values, in [0 ; 0xFFFF], showing a rough triangular
	waveform for a full pot round. It is not necessary that the actual pot
	course spans on the whole range, small margins are tolerated.
	_v1 should be approximately equal to _v0 + 0x8000 when _v0 is small, that
	is, v1 should be a quarter round ahead. A small angular difference error is
	tolerated too.
==============================================================================
*/

void	PotEndless::set_dualpot_val (const PotVal &val) noexcept
{
	_frac_prev = _frac_cur;
	_frac_cur  = compute_monotonic_pos (val);
	const auto     delta = get_delta ();
	_pos_cur  += delta;
}



/*
==============================================================================
Name: set_pos
Description:
	Sets the current logical angular position and associates it with the
	current physical angular position of the pot.
Input parameters:
- pos: new current logical angular position.
==============================================================================
*/

void	PotEndless::set_pos (Pos pos) noexcept
{
	_pos_cur = pos;
}



/*
==============================================================================
Name: get_pos
Description:
	Gets the current logical angular position.
Returns: the position
==============================================================================
*/

PotEndless::Pos	PotEndless::get_pos () const noexcept
{
	return _pos_cur;
}



/*
==============================================================================
Name: get_pos_phys
Description:
	Gets the current physical position. Angle 0 corresponds to pot 0 at mid
	range and pot 1 at the minimum value.
Returns: the position, in range [0 ; 0xFFFF] mapping to [0 ; 1) round
==============================================================================
*/

uint16_t	PotEndless::get_pos_phys () const noexcept
{
	return _frac_cur;
}



/*
==============================================================================
Name: clear_buffers
Description:
	Aligns the logical position with the current physical position.
==============================================================================
*/

void	PotEndless::clear_buffers () noexcept
{
	set_pos (Pos { 0, get_pos_phys () });
	_frac_prev = _frac_cur;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: compute_monotonic_pos
Description:
	Postprocessing: calculates a monotonic continuous position from quadrature
	potentiometer values.
Input parameters:
- pot_val: potentiometer values, with _v1 ~ _v0 + 0x4000 (wrapped)
Returns: an absolute position, as fraction of a round, in range [0 ; 0xFFFF]
==============================================================================
*/

uint16_t	PotEndless::compute_monotonic_pos (const PotVal &pot_val) const noexcept
{
	const auto     pos_raw = compute_raw_pos (pot_val._v0, pot_val._v1);

	// Simple version averaging both tapers without extra correction
	if (_thr == 0)
	{
		return uint16_t ((uint32_t (pos_raw._c0) + uint32_t (pos_raw._c1)) >> 1);
	}

	const auto     conf_0  = compute_confidence (pot_val._v0);
	const auto     conf_1  = compute_confidence (pot_val._v1);

	// Forumla using real values in [0 ; 1]:
	// pos = 0.5 * (c0 + c1 + (conf (v1) - conf (v0)) * (c1 - c0))
	//     = c0 + 0.5 * (1 + conf (v1) - conf (v0)) * (c1 - c0)

	// Merges both confidences, fixed point u2.16 [0 ; 1 << 17]
	const auto     conf_2x = uint32_t (_range) + conf_1 - conf_0;

	// 0.5 * (1 + conf (v1) - conf (v0)) as u1.14 [0 ; 1 << _shf]
	const auto     conf_sc = int (conf_2x >> (_resol + 1 - _shf));

	// Interpolate between both values using the mixed v1 confidence
	const auto     dif_raw = int (pos_raw._c1) - int (pos_raw._c0); // s17
	const auto     lerp_u  = uint16_t ((dif_raw * conf_sc) >> _shf);
	const auto     pos     = uint16_t (pos_raw._c0 + lerp_u);

	return pos;
}



/*
==============================================================================
Name: compute_confidence
Description:
	Computes the confidence coef associated to a raw potentiometer value.
	Confidence curve has a trapeze shape, maximum at the middle of the pot
	range, minimum at the ends of the range.
Input parameters:
- v: raw potentiometer value, in [0 ; 0xFFFF]
Returns: confidence value, 0-1 mapped in [0 ; 1<<16].
==============================================================================
*/

uint32_t	PotEndless::compute_confidence (uint16_t v) const noexcept
{
	// Formula with real values in [0 ; 1]:
	// conf = limit ((1 - abs (2 * v - 1) - thr) / (1 - 2 * thr), 0, 1)

	// Starts from a triangluar shape
	const auto     tri = _range - _thr - std::abs (int (v) * 2 - _range);

	// Clips it to form a trapeze, then scales it to fill the full range.
	const auto     num = std::max (std::min (tri, int (_rng)), 0);
	const auto     res = uint32_t (num * _mul) >> _shf;

	return res;
}



/*
==============================================================================
Name: get_delta
Description:
	Gets the angular difference between the the current postion and the
	previous set_dualpot_val() call.
	Note: this value is not exposed in the public interface because it is
	highly unreliable in presence of noise on the measured voltages.
Returns:
	The angular difference, scaled to 16 bits: 1<<16 = one full round forward.
	Can be negative if the pot is turned backwards.
==============================================================================
*/

int	PotEndless::get_delta () const noexcept
{
	// Makes it signed
	return int16_t (_frac_cur - _frac_prev);
}



/*
==============================================================================
Name: compute_raw_pos
Description:
	Converts a pair of quadrature (pi/2 phase-shift) potentiometer values into
	two linear positions.
	Depending on the input values, one of the output positions may be
	inaccurate. This happens when the potentiometer value is close to its
	taper ends.
	Further processing should be done to extract the meaningful data from both
	outputs and generate a continuous and monotonic position.
	If both potentiometers are swapped round, the resulting position is
	inverted.
Input parameters:
- v0_raw: first potentiometer value, in [0 ; 0xFFFF].
- v1_raw: second potentiometer value, in [0 ; 0xFFFF]. Should be a quarter
	round ahead of v0_raw, so v0_raw ~ v1_raw + 0x8000 modulo 0x10000.
	This imitates roughly v0 ~ cos, v1 ~ sin.
Returns:
	A pair of absolute positions deduced from each raw values. The range [0 ;
	0xFFFF] is a whole round. Both positions are synchronised (similar values,
	most likely not identical), but:
	- _c0 is invalid or highly inaccurate near 0x0000 and 0x8000.
	- _c1 is invalid or highly inaccurate near 0x4000 and 0xC000.
==============================================================================
*/

constexpr PotEndless::PosRawDual	PotEndless::compute_raw_pos (uint16_t v0_raw, uint16_t v1_raw) noexcept
{
	constexpr auto h   = uint16_t (_range_h);
	constexpr auto q   = uint16_t (_range_q);
	const auto     v0h = v0_raw >> 1;
	const auto     v1h = v1_raw >> 1;
	const auto     c0  = uint16_t (((v1_raw < h) ?  v0h : -v0h) + h);
	const auto     c1  = uint16_t (((v0_raw < h) ? -v1h :  v1h) - q);

	return PosRawDual { c0, c1 };
}



}  // namespace ui
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
