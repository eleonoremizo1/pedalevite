/*****************************************************************************

        DisplayInterface.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ui_DisplayInterface_HEADER_INCLUDED)
#define mfx_ui_DisplayInterface_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/ui/PixArgb.h"

#include <cstdint>



namespace mfx
{
namespace ui
{



class DisplayInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	enum BlendMode
	{
		BlendMode_OPAQUE = 0,
		BlendMode_XOR,
		BlendMode_MIN,
		BlendMode_MAX,
		BlendMode_TRANSP, // Uses the opacity (alpha) part of the color
		BlendMode_ALPHA,  // Pixel value is an opacity for the external color

		BlendMode_NBR_ELT
	};

	               DisplayInterface ()                              = default;
	               DisplayInterface (const DisplayInterface &other) = default;
	               DisplayInterface (DisplayInterface &&other)      = default;
	virtual        ~DisplayInterface ()                             = default;

	DisplayInterface &
	               operator = (const DisplayInterface &other)       = default;
	DisplayInterface &
	               operator = (DisplayInterface &&other)            = default;

	int            get_width () const noexcept;
	int            get_height () const noexcept;
	int            get_stride () const noexcept;
	PixArgb *      use_screen_buf () noexcept;
	const PixArgb* use_screen_buf () const noexcept;

	void           refresh (int x, int y, int w, int h);
	void           force_reset ();

	// Utility functions
	void           bitblt (int xd, int yd, const uint8_t * fstb_RESTRICT src_ptr, int xs, int ys, int ws, int hs, int ss, BlendMode mode, PixArgb color) noexcept;
	void           bitblt (int xd, int yd, const PixArgb * fstb_RESTRICT src_ptr, int xs, int ys, int ws, int hs, int ss, BlendMode mode) noexcept;

	void           fill (int xd, int yd, int w, int h, PixArgb color, BlendMode mode) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual int    do_get_width () const noexcept = 0;
	virtual int    do_get_height () const noexcept = 0;
	virtual int    do_get_stride () const noexcept = 0;
	virtual PixArgb *
	               do_use_screen_buf () noexcept = 0;
	virtual const PixArgb *
	               do_use_screen_buf () const noexcept = 0;

	virtual void   do_refresh (int x, int y, int w, int h) = 0;
	virtual void   do_force_reset () = 0;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	template <typename F>
	fstb_FLATINLINE static PixArgb
	               op2 (const PixArgb &dst, const PixArgb &src, F f) noexcept;



}; // class DisplayInterface



}  // namespace ui
}  // namespace mfx



//#include "mfx/ui/DisplayInterface.hpp"



#endif   // mfx_ui_DisplayInterface_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
