/*****************************************************************************

        PotEndless.hpp
        Author: Laurent de Soras, 2022

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ui_PotEndless_CODEHEADER_INCLUDED)
#define mfx_ui_PotEndless_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace ui
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



constexpr uint32_t	PotEndless::compute_confidence_range (uint16_t thr) noexcept
{
	assert (thr <= 0x4000);

	// rng in [0x8000 ; 0x10000]
	const auto     rng = 0x10000 - 2 * thr;

	return rng;
}



constexpr uint32_t	PotEndless::compute_confidence_mult (uint32_t rng) noexcept
{
	assert (rng >=  0x8000);
	assert (rng <= 0x10000);

	// mul in [1<<_shf ; 2<<_shf]
	const auto     mul = uint32_t ((1 << (_shf + 16)) / rng);

	return mul;
}



}  // namespace ui
}  // namespace mfx



#endif // mfx_ui_PotEndless_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
