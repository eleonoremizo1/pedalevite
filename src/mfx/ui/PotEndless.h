/*****************************************************************************

        PotEndless.h
        Author: Laurent de Soras, 2022

This class handles an "endless potentiometer", a potentiometer with two
outputs in phase quadrature. It is similar to a rotational encoder, but gives
absolute and continuous positions.

The pair of potentiometer values are first converted into a physical angular
value. This value is processed to be as monotonic and continuous as possible
assuming the sensor inaccuracies and noise are not too high.
Model, formulas and graphs: https://www.desmos.com/calculator/8myyw1lrq3
        _         
       / \        
      /   \       
V0         \     /
            \   / 
             \_/  
           _      
          / \     
         /   \    
V1      /     \   
       /       \  
      /         \_

Angle 0----0.5----1

Then a logical angular position is computed, allowing turns to be counted and
tracked accurately.

To get relative positions (delta), you should first filter the logical angular
position to attenuate or cancel the noise, then compute the difference.
Otherwise small variations will be buried into the noise.

Input values are assumed unsmoothed. It is possible to use capacitors on the
hardware side, but keep the cutoff frequency significantly above the variation
rate, because an excessive lag in the voltage readings may cause errors in the
decoding.

Output values are not temporally filtered, but intermediate results are
weighted. Weighting does the following:

- Smoothes the effect of flat tops and bottoms shown on the taper resistance
vs. angle graph. These clipped corners cause a change in the output derivative
but no continuity issue.

- Restores continuity when the voltage reads don't reach exactly the end of
the range. Indeed there is a mirroring operation on each level depending on
the position of the other one.

- Fixes misc taper mismatches related to their shape, including quadrature
errors.

- More consistent result in presence of hardware filtering.

The most important thing is the output continuity. It can be achieved without
weighting by scaling the input (ADC result) at the clipping limit if the ADC
doesn't output values on the full range for some reasons. It's not important
if the signals clip slightly, but if they don't, this causes a discontinuity.
The pros of bypassing the weighting are faster calculations and reduced noise
on the whole angle range (-3 dB obtained by simple averaging). But if you
cannot or don't want to calibrate your device, using the weighting (default)
is better.

Input are unsigned 16 bit values, ranging from 0 to 65535 whatever the real
ADC resolution.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ui_PotEndless_HEADER_INCLUDED)
#define mfx_ui_PotEndless_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace ui
{



class PotEndless
{
	static_assert (
		sizeof (int) >= sizeof (int32_t),
		"int must be at least 32 bits, for temporary calculations"
	);
	static_assert (
		(-2 >> 1) == -1,
		"operator >> must be implemented as an arithmetic shift"
	);

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Range for both the ADC and full turn output (as unsigned integers)
	static constexpr int      _resol   = 16;
	static constexpr int32_t  _range   = 1 << _resol;
	static constexpr int32_t  _range_h = _range >> 1; // Half
	static constexpr int32_t  _range_q = _range >> 2; // Quarter

	// Logical angular position of the potentiometer
	struct Pos
	{
		// Number of complete rounds. Can be positive (forward) or negative
		// (backward). The direction is assumed trigonometric if the pot was
		// wired with { cos, sin }-like inputs in this order.
		// Overflows are not handled, however the rotation life of most parts
		// makes it physically impossible.
		int            _idx  = 0;

		// Fractional round (forward). [0 ; 0xFFFF] -> [0 ; +1)
		uint16_t       _frac = 0;

		Pos &          operator += (int delta) noexcept;
	};

	// Pair of potentiometer values, as read from the ADC and scaled to 16 bits.
	// Assumes _v0 is cos-like and _v1 is sin-like for standard trigonometric
	// rotation. If the inputs are swapped, the rotation is inverted.
	struct PotVal
	{
		uint16_t       _v0 = uint16_t (_range_h);
		uint16_t       _v1 = 0;
	};

	void           set_safety_margin (uint16_t thr) noexcept;

	void           set_dualpot_val (const PotVal &sval) noexcept;

	void           set_pos (Pos pos) noexcept;
	Pos            get_pos () const noexcept;
	uint16_t       get_pos_phys () const noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Resolution for fixed-point 16-bit multiplications
	// s32 or u32 temporaries should not overflow for s17 * (1 << _shf) and
	// u17 * (2 << _shf).
	static constexpr int _shf = 14;

	struct PosRawDual
	{
		uint16_t       _c0 = 0;
		uint16_t       _c1 = 0;
	};

	uint16_t       compute_monotonic_pos (const PotVal &pot_val) const noexcept;
	uint32_t       compute_confidence (uint16_t v) const noexcept;
	inline int     get_delta () const noexcept;

	static constexpr PosRawDual
	               compute_raw_pos (uint16_t v0_raw, uint16_t v1_raw) noexcept;
	static constexpr inline uint32_t
	               compute_confidence_range (uint16_t thr) noexcept;
	static constexpr inline uint32_t
	               compute_confidence_mult (uint32_t rng) noexcept;

	// Safety margin. We assume pot values are accurate only in the range
	// [_thr ; (1 << 16) - _thr]. Valid range: [0 ; 0x4000]
	// "Accurate" means that a given rotation is reflected by a proportional
	// variation of the voltage.
	uint16_t       _thr = uint16_t (_range >> 4);

	// Precalculated temporary values to compute the confidence coefficient
	uint32_t       _rng = compute_confidence_range (_thr);
	uint32_t       _mul = compute_confidence_mult (_rng);

	// State
	Pos            _pos_cur;       // Logical
	uint16_t       _frac_cur  = 0; // Physical
	uint16_t       _frac_prev = 0; // Physical



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PotEndless &other) const = delete;
	bool           operator != (const PotEndless &other) const = delete;

}; // class PotEndless



}  // namespace ui
}  // namespace mfx



#include "mfx/ui/PotEndless.hpp"



#endif // mfx_ui_PotEndless_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
