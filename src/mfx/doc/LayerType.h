/*****************************************************************************

        LayerType.h
        Author: Laurent de Soras, 2022

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_LayerType_HEADER_INCLUDED)
#define mfx_doc_LayerType_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace doc
{



enum class LayerType
{
   INVALID = -1,

   // Not defined as IN nor OUT because these symbols may collide
   // with the ones defined in minwindef.h from the Windows Kit.
   I = 0, // Input
   M,     // Main
   O,     // Output

   NBR_ELT

}; // enum class LayerType



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/LayerType.hpp"



#endif // mfx_doc_LayerType_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
