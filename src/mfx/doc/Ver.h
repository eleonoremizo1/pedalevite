/*****************************************************************************

        Ver.h
        Author: Laurent de Soras, 2024

This enumeration describes the file format versions

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_Ver_HEADER_INCLUDED)
#define mfx_doc_Ver_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace doc
{



enum Ver
{
	Ver_UNDEFINED = -1,

	Ver_INITIAL = 1,

	// 2016-09-21
	// Added a version number to the file format
	Ver_FORMAT_NUMBER,

	// 2016-09-27
	// Added a routing object and slot identifiers
	Ver_EXPLICIT_ROUTING,

	// 2016-10-08
	// Added I/O of the signal type (in addition to audio)
	Ver_IO_SIGNALS,

	// 2016-12-16
	// Added effect presets and reset-on-program-change option
	Ver_PRESETS_RST_PC,

	// 2017-02-18
	// Added optional smooth transition between programs
	Ver_SMOOTH_TRANS,

	// 2018-05-30
	// FX change: added Initial delay and State to the Ramp effect
	Ver_FX_RAMP_DLY_STATE,

	// 2018-08-20
	// Added clipping and linear mapping to the parameter modulators
	Ver_PARAM_MOD_CLIPPING,

	// 2019-04-18
	// Changed the pedal footswitch numbering (row swap)
	Ver_FOOTSWITCH_NUMBERING,

	// 2020-01-08
	// Routing is now a graph
	Ver_ROUTING_GRAPH,

	// 2020-01-21
	// Added automatic rel pot assignment to parameters in the program page
	Ver_AUTO_PARAM_ASSIGN,

	// 2021-05-24
	// Added send/return, allowing loops in the graph
	Ver_SEND_RETURN,

	// 2024-03-28
	// Changed the relative potentiometer scale
	Ver_RELPOT_SCALE,

	// 2024-07-04
	// Added program layers
	Ver_LAYERS,

	// 2024-07-28
	// Added notch radius for modulations
	Ver_MOD_NOTCH,

	Ver_NEXT, // Silly trick to get Ver_CURRENT automatically updated
	Ver_CURRENT = Ver_NEXT - 1

}; // enum Ver



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/Ver.hpp"



#endif // mfx_doc_Ver_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
