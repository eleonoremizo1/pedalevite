/*****************************************************************************

        ParamPresentation.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/doc/ParamPresentation.h"
#include "mfx/doc/SerRInterface.h"
#include "mfx/doc/SerWInterface.h"
#include "mfx/piapi/ParamDescInterface.h"

#include <tuple>

#include <cassert>



namespace mfx
{
namespace doc
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	ParamPresentation::operator == (const ParamPresentation &other) const
{
	return (
		   _disp_mode == other._disp_mode
		&& _ref_beats == other._ref_beats
	);
}



bool	ParamPresentation::operator != (const ParamPresentation &other) const
{
	return ! (*this == other);
}



bool	ParamPresentation::is_similar (const ParamPresentation &other) const noexcept
{
	const float    tol = 1e-5f;

	bool           same_flag = (_disp_mode == other._disp_mode);
	same_flag &= (has_tempo_sync () == other.has_tempo_sync ());
	if (same_flag && has_tempo_sync ())
	{
		same_flag = fstb::is_eq_rel (_ref_beats, other._ref_beats, tol);
	}

	return same_flag;
}



bool	ParamPresentation::has_tempo_sync () const noexcept
{
	return (_ref_beats >= 0);
}



void	ParamPresentation::ser_write (SerWInterface &ser) const
{
	ser.begin_list ();

	ser.write (_disp_mode);
	ser.write (_ref_beats);

	ser.end_list ();
}



void	ParamPresentation::ser_read (SerRInterface &ser)
{
	ser.begin_list ();

	ser.read (_disp_mode);
	ser.read (_ref_beats);

	ser.end_list ();
}



bool	ParamPresentation::is_disp_mode_compatible (DispMode disp_mode, const piapi::ParamDescInterface &desc) noexcept
{
	assert (disp_mode >= 0);
	assert (disp_mode < DispMode_NBR_ELT);

	if (disp_mode == DispMode_DEFAULT)
	{
		return true;
	}

	const auto     categ   = desc.get_categ ();
	const double   val_min = desc.get_nat_min ();

	switch (categ)
	{
	case piapi::ParamDescInterface::Categ_UNDEFINED:
		return false;

	case piapi::ParamDescInterface::Categ_TIME_S:
		return (
			    disp_mode == DispMode_MS
			|| (disp_mode == DispMode_BEATS && val_min >= 0)
			|| val_min > 0
		);

	case piapi::ParamDescInterface::Categ_TIME_HZ:
	case piapi::ParamDescInterface::Categ_FREQ_HZ:
		return (
			   disp_mode == DispMode_HZ
			|| val_min > 0
		);

	default:
		assert (false);
		break;
	}

	return false;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ GLOBAL OPERATORS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	operator < (const ParamPresentation &lhs, const ParamPresentation &rhs)
{
	return (
		  std::tie (lhs._disp_mode, lhs._ref_beats)
		< std::tie (rhs._disp_mode, rhs._ref_beats)
	);
}



}  // namespace doc
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
