/*****************************************************************************

        ParamPresentation.h
        Author: Laurent de Soras, 2016

Display modes for the time/frequency parameters.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_ParamPresentation_HEADER_INCLUDED)
#define mfx_doc_ParamPresentation_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{

namespace piapi
{
	class ParamDescInterface;
}

namespace doc
{



class SerRInterface;
class SerWInterface;

class ParamPresentation
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Time/freq display modes
	enum DispMode
	{
		DispMode_DEFAULT = 0,
		DispMode_MS,
		DispMode_HZ,
		DispMode_NOTE,
		DispMode_BEATS,

		DispMode_NBR_ELT
	};

	bool           operator == (const ParamPresentation &other) const;
	bool           operator != (const ParamPresentation &other) const;
	bool           is_similar (const ParamPresentation &other) const noexcept;
	bool           has_tempo_sync () const noexcept;

	void           ser_write (SerWInterface &ser) const;
	void           ser_read (SerRInterface &ser);

	static bool    is_disp_mode_compatible (DispMode disp_mode, const piapi::ParamDescInterface &desc) noexcept;

	DispMode       _disp_mode = DispMode_DEFAULT;

	// Parameter time value, in beats. >= 0. May exceed the internal parameter
	// range. Negative = tempo sync not activated for this parameter.
	float          _ref_beats = -1;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

}; // class ParamPresentation



/*\\\ GLOBAL OPERATORS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	operator < (const ParamPresentation &lhs, const ParamPresentation &rhs);



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/ParamPresentation.hpp"



#endif   // mfx_doc_ParamPresentation_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
