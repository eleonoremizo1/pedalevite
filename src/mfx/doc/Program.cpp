/*****************************************************************************

        Program.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/Program.h"
#include "mfx/doc/SerRInterface.h"
#include "mfx/doc/SerWInterface.h"
#include "mfx/doc/Ver.h"

#include <cassert>



namespace mfx
{
namespace doc
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Program::ser_write (SerWInterface &ser) const
{
	ser.begin_list ();

	ser.write (_name);
	ser.write (_prog_switch_mode);
	ser.write (_target);

	_layout.ser_write (ser);
	_layer.ser_write (ser);

	ser.end_list ();
}



void	Program::ser_read (SerRInterface &ser)
{
	const auto     doc_ver = Ver (ser.get_doc_version ());

	ser.begin_list ();

	ser.read (_name);

	_prog_switch_mode = ProgSwitchMode::DIRECT;
	if (doc_ver >= Ver_SMOOTH_TRANS)
	{
		ser.read (_prog_switch_mode);
	}
	_target = LayerType::M;
	if (doc_ver >= Ver_LAYERS)
	{
		ser.read (_target);
	}

	_layout.ser_read (ser);
	_layer.ser_read (ser);

	ser.end_list ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace doc
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
