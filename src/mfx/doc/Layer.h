/*****************************************************************************

        Layer.h
        Author: Laurent de Soras, 2022

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_Layer_HEADER_INCLUDED)
#define mfx_doc_Layer_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/Routing.h"
#include "mfx/doc/SignalPort.h"
#include "mfx/doc/Slot.h"

#include <map>
#include <memory>



namespace mfx
{
namespace doc
{



class SerRInterface;
class SerWInterface;

class Layer
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef std::shared_ptr <Slot> SlotSPtr;
	typedef std::map <int, SlotSPtr> SlotMap; // [slot_id] = SlotSPtr
	typedef std::map <int, SignalPort> PortMap; // [port_index] = SignalPort

	               Layer ();
	               Layer (const Layer &other);
	               Layer (Layer &&other)           = default;
	Layer &        operator = (const Layer &other);
	Layer &        operator = (Layer &&other)      = default;

	void           ser_write (SerWInterface &ser) const;
	void           ser_read (SerRInterface &ser);

	bool           is_slot_empty (int slot_id) const;
	bool           is_slot_empty (SlotMap::const_iterator it) const;
	Slot &         use_slot (int slot_id);
	const Slot &   use_slot (int slot_id) const;
	int            gen_slot_id () const;
	int            find_free_port () const;
	bool           check_routing (const Routing &routing) const;
	void           set_routing (const Routing &routing);
	const Routing& use_routing () const;

	static bool    is_slot_empty (const SlotMap::value_type &vt) noexcept;

	// /!\ SlotSPtr pointers may be null here. Always test before use, even
	// with is_slot_empty().
	/*** To do: change this to something safer and more consistent ***/
	SlotMap        _slot_map;

	PortMap        _port_map;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	void           duplicate_slot_list ();
	bool           check_routing_cnx_audio_end (const CnxEnd &cnx_end) const;
	void           fix_routing_converted_from_chain ();

	static bool    is_plugin_requiring_routing_fix (const std::string &pi_model);

	Routing        _routing;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Layer &other) const = delete;
	bool           operator != (const Layer &other) const = delete;

}; // class Layer



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/Layer.hpp"



#endif // mfx_doc_Layer_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
