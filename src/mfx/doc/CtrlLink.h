/*****************************************************************************

        CtrlLink.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_CtrlLink_HEADER_INCLUDED)
#define mfx_doc_CtrlLink_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ControlCurve.h"
#include "mfx/ControlSource.h"

#include <set>



namespace mfx
{
namespace doc
{



class SerRInterface;
class SerWInterface;

class CtrlLink
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// In absolute value.
	static constexpr float _max_amp = 4.f;

	bool           operator == (const CtrlLink &other) const;
	bool           operator != (const CtrlLink &other) const;
	bool           is_similar (const CtrlLink &other) const;

	void           ser_write (SerWInterface &ser) const;
	void           ser_read (SerRInterface &ser);

	ControlSource  _source;

	// Speed multiplier, for relative modes (incremental encoders). > 0
	float          _spd_mult = 1;

	ControlCurve   _curve    = ControlCurve_LINEAR;

	// Normalized value, for absolute mode
	float          _base     = 0;

	// Normalized scale, for all modes. Can be negative.
	float          _amp      = 1;

	// Unipolar to bipolar conversion (0...1 -> -1...1)
	bool           _u2b_flag = false;

	bool           _clip_flag     = false;

	// Minimum value from the modulator source
	float          _clip_src_beg  = -1;

	// Maximum value from the modulator source. beg < end
	float          _clip_src_end  =  1;

	// Value on which src_beg is mapped
	float          _clip_dst_beg  = -1;

	// Value on which src_end is mapped. beg < end
	float          _clip_dst_end  =  1;

	// Normalized values. Applies on the final value
	std::set <float>
	               _notch_list;

	// Radius around the notches. Out of its perimeter, the notch isn't active
	// and doesn't snap the value. > 0.
	float          _notch_radius  = 1;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

}; // class CtrlLink



/*\\\ GLOBAL OPERATORS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	operator < (const CtrlLink &lhs, const CtrlLink &rhs);



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/CtrlLink.hpp"



#endif   // mfx_doc_CtrlLink_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
