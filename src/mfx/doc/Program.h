/*****************************************************************************

        Program.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_doc_Program_HEADER_INCLUDED)
#define mfx_doc_Program_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/Layer.h"
#include "mfx/doc/LayerType.h"
#include "mfx/doc/PedalboardLayout.h"
#include "mfx/doc/ProgSwitchMode.h"

#include <string>



namespace mfx
{
namespace doc
{



class SerRInterface;
class SerWInterface;

class Program
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               Program ()                        = default;
	               Program (const Program &other)    = default;
	               Program (Program &&other)         = default;
	               ~Program ()                       = default;

	Program &      operator = (const Program &other) = default;
	Program &      operator = (Program &&other)      = default;

	void           ser_write (SerWInterface &ser) const;
	void           ser_read (SerRInterface &ser);

	std::string    _name;
	PedalboardLayout
	               _layout;
	ProgSwitchMode _prog_switch_mode = ProgSwitchMode::DIRECT;
	Layer          _layer;
	LayerType      _target           = LayerType::M;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Program &other) const = delete;
	bool           operator != (const Program &other) const = delete;

}; // class Program



}  // namespace doc
}  // namespace mfx



//#include "mfx/doc/Program.hpp"



#endif   // mfx_doc_Program_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
