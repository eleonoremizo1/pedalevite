/*****************************************************************************

        ChnMode.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_ChnMode_HEADER_INCLUDED)
#define mfx_ChnMode_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/LayerType.h"
#include "mfx/piapi/Dir.h"



namespace mfx
{



enum ChnMode
{

	ChnMode_2M_2M = 0,   // 2 mono chains
	ChnMode_2M_1S,       // 2 mono to 1 stereo
	ChnMode_1S_1S,       // Stereo to stereo

	ChnMode_NBR_ELT

}; // class ChnMode



int	ChnMode_get_nbr_chn (ChnMode mode, piapi::Dir dir);
int	ChnMode_get_nbr_pins (ChnMode mode, piapi::Dir dir);

int	ChnMode_get_layer_nbr_chn (doc::LayerType layer, ChnMode mode, piapi::Dir dir);
int	ChnMode_get_layer_nbr_pins (doc::LayerType layer, ChnMode mode, piapi::Dir dir);



}  // namespace mfx



//#include "mfx/ChnMode.hpp"



#endif   // mfx_ChnMode_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
