/*****************************************************************************

        features.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_features_HEADER_INCLUDED)
#define mfx_features_HEADER_INCLUDED



#if ! defined (PV_VERSION)
	#error PV_VERSION should be defined.
#endif



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"



// Display
// Assumes mfx_features_DISP_FB is 800x480 min resolution, true color
#define mfx_features_DISP_ST7920  (1)
#define mfx_features_DISP_FB      (2)
#if PV_VERSION == 1
	#define mfx_features_DISP mfx_features_DISP_ST7920
#else
	#define mfx_features_DISP mfx_features_DISP_FB
#endif

// Audio API
#define mfx_features_API_JACK   (1)
#define mfx_features_API_ALSA   (2)
#define mfx_features_API_ASIO   (3)
#define mfx_features_API_MANUAL (4)
#define mfx_features_API_PVAB   (5)
#if 0 // For debugging complex audio things
	#define mfx_features_API mfx_features_API_MANUAL
#elif fstb_SYS == fstb_SYS_LINUX
	#if PV_VERSION == 1
//		#define mfx_features_API mfx_features_API_JACK
		#define mfx_features_API mfx_features_API_ALSA
	#else
		#define mfx_features_API mfx_features_API_PVAB
	#endif
#else // fstb_SYS
	#define mfx_features_API mfx_features_API_ASIO
#endif // fstb_SYS

// Relative potentiometers for parameters
// Discrete = rotary encoders, continous = endless potentiometers
#define mfx_features_POTREL_DISCRETE  (1)
#define mfx_features_POTREL_CONTINOUS (2)
#if PV_VERSION < 3
	#define mfx_features_POTREL mfx_features_POTREL_DISCRETE
#else
	#define mfx_features_POTREL mfx_features_POTREL_CONTINOUS
#endif


//#include "mfx/features.hpp"



#endif // mfx_features_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
