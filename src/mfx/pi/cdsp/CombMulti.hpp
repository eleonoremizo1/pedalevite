/*****************************************************************************

        CombMulti.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_cdsp_CombMulti_CODEHEADER_INCLUDED)
#define mfx_pi_cdsp_CombMulti_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace pi
{
namespace cdsp
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_nbr_combs
Description:
	Sets the number of comb filters.
	Mandatory call before processing anything.
Input parameters:
	- nbr_combs: number of filters, > 0.
Throws: std::vector related exceptions
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::set_nbr_combs (int nbr_combs)
{
	assert (nbr_combs > 0);

	_comb_arr.resize (nbr_combs);
}



/*
==============================================================================
Name: set_delay_max
Description:
	Sets the delay upper limit on a comb filter.
	Mandatory call on all comb filters before processing anything.
Input parameters:
	- comb_idx: index of the filter to initialize, [0 ; nbr_combs - 1]
	- delay: maximum comb delay in samples, not necessarily integer.
Throws: std::vector related exceptions
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::set_delay_max (int comb_idx, float delay)
{
	assert (! _comb_arr.empty ());
	assert (comb_idx >= 0);
	assert (comb_idx < int (_comb_arr.size ()));
	assert (delay >= Comb::_delay_min);

	const int      max_delay = fstb::ceil_int (delay);
	Comb &         comb      = _comb_arr [comb_idx];
	comb.set_max_len (max_delay);
}



/*
==============================================================================
Name: set_delay
Description:
	Initialise a comb filter.
	Mandatory call on all comb filters before processing anything.
Input parameters:
	- comb_idx: index of the filter to initialize, [0 ; nbr_combs - 1]
	- delay: comb delay in samples, not necessarily integer.
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::set_delay (int comb_idx, float delay) noexcept
{
	assert (! _comb_arr.empty ());
	assert (comb_idx >= 0);
	assert (comb_idx < int (_comb_arr.size ()));
	assert (delay >= Comb::_delay_min);

	Comb &         comb = _comb_arr [comb_idx];
	comb.set_delay_flt (delay);
}



/*
==============================================================================
Name: set_coefs
Description:
	Sets the coefficients for a single comb filter
Input parameters:
	- comb_idx: index of the filter to configure, [0 ; nbr_combs - 1]
	- b0: direct feedforward coefficient
	- bm: delayed feedforward coefficient
	- am: delayed feedback coefficient
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::set_coefs (int comb_idx, float b0, float bm, float am) noexcept
{
	assert (! _comb_arr.empty ());
	assert (comb_idx >= 0);
	assert (comb_idx < int (_comb_arr.size ()));

	Comb &         comb = _comb_arr [comb_idx];
	comb.set_b0 (b0);
	comb.set_bm (bm);
	comb.set_am (am);
}



/*
==============================================================================
Name: process_sample
Description:
	Generate all comb outputs from a single input sample.
Input parameters:
	- x: input sample
Output parameters:
	- dst_arr: pointer on an array of nbr_combs outputs to be written.
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::process_sample (T dst_arr [], T x) noexcept
{
	assert (! _comb_arr.empty ());
	assert (dst_arr != nullptr);

	const auto     nbr_combs = int (_comb_arr.size ());
	for (int comb_idx = 0; comb_idx < nbr_combs; ++comb_idx)
	{
		dst_arr [comb_idx] = process_sample_single (comb_idx, x);
	}
}



/*
==============================================================================
Name: process_sample_single
Description:
	Processes a sample for a single comb filter.
Input parameters:
	- comb_idx: index of the filter to configure, [0 ; nbr_combs - 1]
	- x: input sample
Returns: the output sample
==============================================================================
*/

template <typename T, int NPL2>
T	CombMulti <T, NPL2>::process_sample_single (int comb_idx, T x) noexcept
{
	assert (! _comb_arr.empty ());
	assert (comb_idx >= 0);
	assert (comb_idx < int (_comb_arr.size ()));

	Comb &         comb = _comb_arr [comb_idx];
	const auto     y    = comb.process_sample (x);

	return y;
}



/*
==============================================================================
Name: process_block
Description:
	Processes a block of input sample and generates all the output blocks.
Input parameters:
	- src_ptr: pointer on a block of input samples.
	- nbr_spl: number of input samples to process, > 0.
Output parameters:
	- dst_ptr_arr: array of pointers on output sample blocks, one for each
		comb filter.
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::process_block (T *dst_ptr_arr [], const T src_ptr [], int nbr_spl) noexcept
{
	assert (! _comb_arr.empty ());
	assert (dst_ptr_arr != nullptr);
	assert (src_ptr     != nullptr);
	assert (nbr_spl > 0);

	const auto     nbr_combs = int (_comb_arr.size ());
	for (int comb_idx = 0; comb_idx < nbr_combs; ++comb_idx)
	{
		auto *         dst_ptr = dst_ptr_arr [comb_idx];
		assert (dst_ptr != nullptr);
		process_block_single (dst_ptr, src_ptr, nbr_spl);
	}
}



/*
==============================================================================
Name: process_block_single
Description:
	Processes a block of input sample in a single comb filter.
Input parameters:
	- comb_idx: index of the filter to configure, [0 ; nbr_combs - 1]
	- src_ptr: pointer on a block of input samples.
	- nbr_spl: number of input samples to process, > 0.
Output parameters:
	- dst_ptr: pointer on the output sample block
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::process_block_single (int comb_idx, T dst_ptr [], const T src_ptr [], int nbr_spl) noexcept
{
	assert (! _comb_arr.empty ());
	assert (comb_idx >= 0);
	assert (comb_idx < int (_comb_arr.size ()));
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (nbr_spl > 0);

	Comb &         comb = _comb_arr [comb_idx];
	comb.process_block (dst_ptr, src_ptr, nbr_spl);
}



/*
==============================================================================
Name: clear_buffers
Description:
	Clears all the filters.
==============================================================================
*/

template <typename T, int NPL2>
void	CombMulti <T, NPL2>::clear_buffers () noexcept
{
	for (auto &comb : _comb_arr)
	{
		comb.clear_buffers ();
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#endif // mfx_pi_cdsp_CombMulti_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
