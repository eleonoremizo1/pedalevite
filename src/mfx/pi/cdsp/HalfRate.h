/*****************************************************************************

        HalfRate.h
        Author: Laurent de Soras, 2023

This class implements down- and up-sampling for processing at half the main
sample rate. It handles issues related to odd block lengths.

The delay is 1 sample (at the standard rate) + possible delay from the
antialiasing filters.

      Mem prev  Cur block   Mem next

In        #___  # # # # #
              \ / \ / \ /
Rate/2         #_  #_  #_____
               \ \ \ \ \     \
Out             # # # # #     #

Usage:

For each block, call these functions in this order:
start_block ();
process_block_down ();
[subsampled data processing]
process_block_up ();

Template parameters:

- DS: downsampling filter (single channel), using the same interface as the
	HIIR downsamplers.

- US: upsampling filter (single channel), same interface as HIIR.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_cdsp_HalfRate_HEADER_INCLUDED)
#define mfx_pi_cdsp_HalfRate_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace pi
{
namespace cdsp
{



template <typename DS, typename US>
class HalfRate
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef DS DSpl;
	typedef US USpl;

	inline const DS &
	               use_ds () const noexcept;
	inline DS &    use_ds () noexcept;
	inline const US &
	               use_us () const noexcept;
	inline US &    use_us () noexcept;

	int            start_block (int nbr_spl) noexcept;
	void           process_block_down (float dst_ptr [], const float src_ptr []) noexcept;
	void           process_block_up (float dst_ptr [], const float src_ptr []) noexcept;

	void           clear_buffers () noexcept;
	template <typename HR>
	void           sync_to (const HR &other) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class Mem
	{
	public:
		bool           _flag = false;

		// Stored value
		float          _val  = 0;
	};

	DSpl           _ds;
	USpl           _us;

	// Only 0 or 1.
	// If 0, there is a memory at the beginning of the input stream.
	int            _block_pos = 0;

	// Current block length, standard rate. 0 = block not started.
	// If _block_pos + _block_len is odd, there will be an excess data
	// at the end of the output stream
	int            _block_len = 0;

	// Memories for the input and output streams (standard rate).
	// Their actual content depends on the variables above.
	float          _mem_i = 0;
	float          _mem_o = 0;

	// Indicates if there is a stored value (odd boundary) at:
	// - the beginning of the block for the downsampler (remaining from the
	// previous block),
	// - the end of the block for the upsampler (excess output that should be
	// consumed on the next block).



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const HalfRate &other) const = delete;
	bool           operator != (const HalfRate &other) const = delete;

}; // class HalfRate



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#include "mfx/pi/cdsp/HalfRate.hpp"



#endif   // mfx_pi_cdsp_HalfRate_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

