/*****************************************************************************

        OvrsplMulti.h
        Author: Laurent de Soras, 2024

This object must be aligned on a 16-byte boundary!

Oversampling (up- and downsampling) for a single audio channel

Fixed coefficients
	Min attenuation: 94 dB
	Min bandwidth  : 0.4327 * Fs
Selectable ratio at runtime: x1 (no oversampling), x2, x4, x8

Latency (group delay at 0.09 * Fs) remains fixed and integer in samples,
whatever the ratio. This helps achieving constant latency, independent of
the parameters. It is possible to cap the runtime oversampling ratio to
reduce the latency upper bound.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_cdsp_OvrsplMulti_HEADER_INCLUDED)
#define mfx_pi_cdsp_OvrsplMulti_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "hiir/coef/C2x.h"
#include "hiir/coef/C4x.h"
#include "hiir/coef/C8x.h"
#include "mfx/dsp/iir/Downsampler2xSimd.h"
#include "mfx/dsp/iir/Downsampler4xSimd.h"
#include "mfx/dsp/iir/Downsampler8xSimd.h"
#include "mfx/dsp/iir/Upsampler2xSimd.h"
#include "mfx/dsp/iir/Upsampler4xSimd.h"
#include "mfx/dsp/iir/Upsampler8xSimd.h"

#include <algorithm>
#include <array>



namespace mfx
{
namespace pi
{
namespace cdsp
{



class OvrsplMulti
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _max_ratio_l2 = 3;

	// Reference frequency for the group delay (latency), as a fraction of the
	// standard sampling rate
	static constexpr double _group_delay_ref_freq = 4000.0 / 44100.0;

	               OvrsplMulti () noexcept;
	               OvrsplMulti (const OvrsplMulti &other) = default;
	               OvrsplMulti (OvrsplMulti &&other)      = default;
	               ~OvrsplMulti ()                        = default;

	OvrsplMulti &  operator = (const OvrsplMulti &other)  = default;
	OvrsplMulti &  operator = (OvrsplMulti &&other)       = default;

	void           cap_ratio_l2 (int ratio_l2) noexcept;
	void           set_ratio_l2 (int ratio_l2) noexcept;
	inline int     get_latency () const noexcept;

	void           clear_buffers () noexcept;

	void           upsample_sample (float dst_ptr [], float x) noexcept;
	void           upsample_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;
	float          downsample_sample (const float src_ptr []) noexcept;
	void           downsample_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef hiir::coef::C2x094 C2; // 2x, Attn: 94 dB, BW: 0.4656
	typedef hiir::coef::C4x099 C4; // 4x, Attn: 99 dB, BW: 0.4327
	typedef hiir::coef::C8x096 C8; // 8x, Attn: 96 dB, BW: 0.4456

	// Latencies (group delays) for the x1, x2, x4, x8 cascades, in samples
	typedef std::array <int, _max_ratio_l2 + 1> LatencyArray;
	static constexpr LatencyArray _latency_arr =
	{
		0, C2::_delay, C4::_delay, C8::_delay
	};

	// Maximum possible latency
	static constexpr int _latency_max =
		*std::max_element (_latency_arr.begin (), _latency_arr.end ());

	template <int N>
	class UpDown
	{
	public:
		template <int M>
		using CArr = std::array <double, M>;

		void           set_coefs (const CArr <N> &coef_arr) noexcept;
		void           clear_buffers () noexcept;
		dsp::iir::Upsampler2xSimd <N>
		               _us;
		dsp::iir::Downsampler2xSimd <N>
		               _ds;
	};

	// Ring buffer size for the delay. At least 2 elements, power of 2
	static constexpr int _dly_size_l2 =
		std::max (fstb::get_next_pow_2_ce (_latency_max + 1), 1);
	static constexpr int _dly_size    = 1 << _dly_size_l2;

	// AND mask for quick position wrapping in the ring buffer
	static constexpr int _dly_mask    = _dly_size - 1;

	// Size of a temporary buffer on the stack, in samples. >= 8
	static constexpr int _tmp_buf_len = 256;

	inline int     compute_delay_time_from_latency () const noexcept;
	template <typename DS>
	inline void    downsample_x1 (DS &ds, float dst_ptr [], float src_ptr [], float tmp_ptr [], int nbr_spl) noexcept;
	inline void    process_delay (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;
	inline int     read_delay (float dst_ptr [], int nbr_spl) const noexcept;
	inline void    write_delay (const float src_ptr [], int nbr_spl) noexcept;

	UpDown <C2::X2::_nbr_coef> _rspl10;
	UpDown <C4::X2::_nbr_coef> _rspl20;
	UpDown <C4::X4::_nbr_coef> _rspl21;
	UpDown <C8::X2::_nbr_coef> _rspl30;
	UpDown <C8::X4::_nbr_coef> _rspl31;
	UpDown <C8::X8::_nbr_coef> _rspl32;

	// Log2 of the maximum ratio set for the session, range [0 ; _max_ratio_l2]
	int            _ratio_l2_cap = _max_ratio_l2;

	// Log2 of the current oversampling ratio, range [0 ; _ratio_l2_cap]
	int            _ratio_l2     = 0;

	// Buffer for the delay compensation, applied on the downsampler output
	std::array <float, _dly_size>
	               _dly_buf {};

	// Writing position, range [0 ; _dly_size - 1]
	int            _dly_pos      = 0;

	// Delay time in samples, range [0 ; _latency_max]
	int            _dly_time     = compute_delay_time_from_latency ();



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const OvrsplMulti &other) const = delete;
	bool           operator != (const OvrsplMulti &other) const = delete;

}; // class OvrsplMulti



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#include "mfx/pi/cdsp/OvrsplMulti.hpp"



#endif   // mfx_pi_cdsp_OvrsplMulti_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
