/*****************************************************************************

        OvrsplMulti.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/cdsp/OvrsplMulti.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace cdsp
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Description:
	Default mode: x1 (no oversampling), capped to the maximum possible
Throws: Nothing
==============================================================================
*/

OvrsplMulti::OvrsplMulti () noexcept
{
	_rspl10.set_coefs (C2::X2::_coef_list);
	_rspl20.set_coefs (C4::X2::_coef_list);
	_rspl21.set_coefs (C4::X4::_coef_list);
	_rspl30.set_coefs (C8::X2::_coef_list);
	_rspl31.set_coefs (C8::X4::_coef_list);
	_rspl32.set_coefs (C8::X8::_coef_list);
	cap_ratio_l2 (_max_ratio_l2);
	set_ratio_l2 (0);
	clear_buffers ();
}



/*
==============================================================================
Name: cap_ratio_l2
Description:
	Sets a cap for the oversampling ratio. Limiting the dynamic ratio change
	helps reducing the overall latency, which is constant and related to the
	maximum ratio.
	If the capped ratio is below the current ratio, the latter is automatically
	capped.
Input parameters:
	- ratio_l2: log2 of the capped ratio, in [0 ; _max_ratio_l2].
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::cap_ratio_l2 (int ratio_l2) noexcept
{
	assert (ratio_l2 >= 0);
	assert (ratio_l2 <= _max_ratio_l2);

	_ratio_l2_cap = ratio_l2;
	set_ratio_l2 (std::min (_ratio_l2, _ratio_l2_cap));
}



/*
==============================================================================
Name: set_ratio_l2
Description:
	Sets the current oversampling ratio.
Input parameters:
	- ratio_l2: log2 of the ratio, >= 0. Must be <= to the ratio cap
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::set_ratio_l2 (int ratio_l2) noexcept
{
	assert (ratio_l2 >= 0);
	assert (ratio_l2 <= _ratio_l2_cap);

	_ratio_l2 = ratio_l2;
	_dly_time = compute_delay_time_from_latency ();
}



/*
==============================================================================
Name: clear_buffers
Description:
	Resets all the processor states.
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::clear_buffers () noexcept
{
	_rspl10.clear_buffers ();
	_rspl20.clear_buffers ();
	_rspl21.clear_buffers ();
	_rspl30.clear_buffers ();
	_rspl31.clear_buffers ();
	_rspl32.clear_buffers ();
	_dly_buf.fill (0);
	_dly_pos = 0;
}



/*
==============================================================================
Name: upsample_sample
Description:
	Upsamples a single sample.
Input parameters:
	- x: input sample
Output parameters:
	- dst_ptr: pointer on a buffer where the output samples are written.
		Must be big enough to collect all the output samples (size = ratio).
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::upsample_sample (float dst_ptr [], float x) noexcept
{
	assert (dst_ptr != nullptr);

	std::array <float, 4>   tmp;

	switch (_ratio_l2)
	{
	case 0:
		dst_ptr [0] = x;
		break;
	case 1:
		_rspl10._us.process_sample (dst_ptr [0], dst_ptr [1], x);
		break;
	case 2:
		_rspl20._us.process_sample (tmp [0], tmp [1], x);
		_rspl21._us.process_sample (dst_ptr [0], dst_ptr [1], tmp [0]);
		_rspl21._us.process_sample (dst_ptr [2], dst_ptr [3], tmp [1]);
		break;
	case 3:
		_rspl30._us.process_sample (tmp [0], tmp [2], x);
		_rspl31._us.process_sample (tmp [0], tmp [1], tmp [0]);
		_rspl31._us.process_sample (tmp [2], tmp [3], tmp [2]);
		_rspl32._us.process_block (dst_ptr, tmp.data (), 4);
		break;
	default:
		assert (false);
		break;
	}
}



/*
==============================================================================
Name: upsample_block
Description:
	Upsamples a block of samples
Input parameters:
	- src_ptr: pointer on the block to upsample
	- nbr_spl: Number of input samples
Output parameters:
	- dst_ptr: pointer on the block of output samples. Must be big enough
		to receive the resulting data (ratio times nbr_spl)
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::upsample_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (dst_ptr != nullptr);
	assert (dst_ptr != src_ptr);
	assert (nbr_spl > 0);

	switch (_ratio_l2)
	{
	case 0:
		std::copy (src_ptr, src_ptr + nbr_spl, dst_ptr);
		break;
	case 1:
		_rspl10._us.process_block (dst_ptr, src_ptr, nbr_spl);
		break;
	case 2:
		{
			std::array <float, _tmp_buf_len> x2;
			int            pos = 0;
			do
			{
				const auto     work_len =
					std::min (nbr_spl - pos, int (_tmp_buf_len >> 1));
				const auto     d_ptr    = &dst_ptr [pos << 2];
				_rspl20._us.process_block (x2.data (), &src_ptr [pos], work_len);
				_rspl21._us.process_block (d_ptr, x2.data (), work_len << 1);
				pos += work_len;
			}
			while (pos < nbr_spl);
		}
		break;
	case 3:
		{
			std::array <float, _tmp_buf_len> x4;
			int            pos = 0;
			do
			{
				const auto     work_len =
					std::min (nbr_spl - pos, int (_tmp_buf_len >> 2));
				const auto     d_ptr    = &dst_ptr [pos << 3];
				_rspl30._us.process_block (d_ptr, &src_ptr [pos], work_len);
				_rspl31._us.process_block (x4.data (), d_ptr, work_len << 1);
				_rspl32._us.process_block (d_ptr, x4.data (), work_len << 2);
				pos += work_len;
			}
			while (pos < nbr_spl);
		}
		break;
	default:
		assert (false);
		break;
	}
}



/*
==============================================================================
Name: downsample_sample
Description:
	Downsamples a small block of data to produce a single output sample.
	Compensation delay may be applied at this stage.
Input parameters:
	- src_ptr: Pointer on the data to downsample. The size of block in samples
		is equal to ratio.
Returns: the downsampled sample.
Throws: Nothing
==============================================================================
*/

float	OvrsplMulti::downsample_sample (const float src_ptr []) noexcept
{
	assert (src_ptr != nullptr);

	std::array <float, 4>   tmp;
	float          y;

	// Downsampling
	switch (_ratio_l2)
	{
	case 0:
		y = src_ptr [0];
		break;
	case 1:
		y = _rspl10._ds.process_sample (src_ptr);
		break;
	case 2:
		tmp [0] = _rspl21._ds.process_sample (src_ptr);
		tmp [1] = _rspl21._ds.process_sample (src_ptr + 2);
		y = _rspl20._ds.process_sample (tmp.data ());
		break;
	case 3:
		_rspl32._ds.process_block (tmp.data (), src_ptr, 4);
		tmp [0] = _rspl31._ds.process_sample (&tmp [0]);
		tmp [1] = _rspl31._ds.process_sample (&tmp [2]);
		y = _rspl30._ds.process_sample (tmp.data ());
		break;
	default:
		assert (false);
		return 0;
	}

	// Compensation delay
	_dly_buf [_dly_pos] = y;
	const auto     d = _dly_buf [(_dly_pos - _dly_time) & _dly_mask];
	_dly_pos = (_dly_pos + 1) & _dly_mask;

	return d;
}



/*
==============================================================================
Name: downsample_block
Description:
	Downsamples a block of samples.
	Compensation delay may be applied at this stage.
Input parameters:
	- src_ptr: Pointer on the block of input samples. Its size must be
		nbr_spl * ratio.
	- nbr_spl: Number of output samples.
Output parameters:
	- dst_ptr: Pointer on the block of output samples. Should be big enough to
		receive nbr_spl samples.
Throws: Nothing
==============================================================================
*/

void	OvrsplMulti::downsample_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (dst_ptr != nullptr);
	assert (nbr_spl > 0);

	switch (_ratio_l2)
	{
	case 0:
		process_delay (dst_ptr, src_ptr, nbr_spl);
		break;

	case 1:
		if (_dly_time == 0)
		{
			_rspl10._ds.process_block (dst_ptr, src_ptr, nbr_spl);
			write_delay (dst_ptr, nbr_spl);
		}
		else
		{
			std::array <float, _tmp_buf_len> x1;
			int            pos = 0;
			do
			{
				const auto     work_len = std::min (nbr_spl - pos, _tmp_buf_len);
				_rspl10._ds.process_block (
					x1.data (), &src_ptr [pos << 1], work_len
				);
				process_delay (&dst_ptr [pos], x1.data (), work_len);
				pos += work_len;
			}
			while (pos < nbr_spl);
		}
		break;

	case 2:
		{
			std::array <float,  _tmp_buf_len      > x2;
			std::array <float, (_tmp_buf_len >> 1)> x1;
			int            pos = 0;
			do
			{
				const auto     work_len =
					std::min (nbr_spl - pos, _tmp_buf_len >> 1);
				_rspl21._ds.process_block (
					x2.data (), &src_ptr [pos << 2], work_len << 1
				);
				downsample_x1 (
					_rspl20._ds, &dst_ptr [pos], x2.data (), x1.data (), work_len
				);
				pos += work_len;
			}
			while (pos < nbr_spl);
		}
		break;

	case 3:
		{
			// Buffer shared with x1 and x4 data
			std::array <float,  _tmp_buf_len      > x4_x1;
			std::array <float, (_tmp_buf_len >> 1)> x2;
			int            pos = 0;
			do
			{
				const auto     work_len =
					std::min (nbr_spl - pos, _tmp_buf_len >> 2);
				_rspl32._ds.process_block (
					x4_x1.data (), &src_ptr [pos << 3], work_len << 2
				);
				_rspl31._ds.process_block (
					x2.data (), x4_x1.data (), work_len << 1
				);
				downsample_x1 (
					_rspl30._ds, &dst_ptr [pos], x2.data (), x4_x1.data (), work_len
				);
				pos += work_len;
			}
			while (pos < nbr_spl);
		}
		break;

	default:
		assert (false);
		break;
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <int N>
void	OvrsplMulti::UpDown <N>::set_coefs (const CArr <N> &coef_arr) noexcept
{
	_us.set_coefs (coef_arr.data ());
	_ds.set_coefs (coef_arr.data ());
}



template <int N>
void	OvrsplMulti::UpDown <N>::clear_buffers () noexcept
{
	_us.clear_buffers ();
	_ds.clear_buffers ();
}



template <typename DS>
void	OvrsplMulti::downsample_x1 (DS &ds, float dst_ptr [], float src_ptr [], float tmp_ptr [], int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (tmp_ptr != nullptr);
	assert (nbr_spl > 0);

	const auto     x1_ptr = (_dly_time == 0) ? dst_ptr : tmp_ptr;
	ds.process_block (x1_ptr, src_ptr, nbr_spl);
	if (_dly_time == 0)
	{
		write_delay (dst_ptr, nbr_spl);
	}
	else
	{
		process_delay (dst_ptr, tmp_ptr, nbr_spl);
	}
}



void	OvrsplMulti::process_delay (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (dst_ptr != nullptr);
	assert (dst_ptr != src_ptr);
	assert (nbr_spl > 0);

	const int      pos = read_delay (dst_ptr, nbr_spl);
	assert (pos <= nbr_spl);
	std::copy (src_ptr, src_ptr + nbr_spl - pos, dst_ptr + pos);
	write_delay (src_ptr, nbr_spl);
}



// Returns the number of samples actually read, in [0 ; nbr_spl]
int	OvrsplMulti::read_delay (float dst_ptr [], int nbr_spl) const noexcept
{
	assert (dst_ptr != nullptr);
	assert (nbr_spl > 0);

	const auto     work_len = std::min (nbr_spl, _dly_time);
	const auto     read_beg = _dly_pos - _dly_time; // Can be negative
	for (int pos = 0; pos < work_len; ++pos)
	{
		dst_ptr [pos] = _dly_buf [(read_beg + pos) & _dly_mask];
	}

	return work_len;
}



// Updates the writing position.
void	OvrsplMulti::write_delay (const float src_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (nbr_spl > 0);

	if (nbr_spl >= _dly_size)
	{
		const auto     pos = nbr_spl - _dly_size;
		std::copy (src_ptr + pos, src_ptr + nbr_spl, _dly_buf.data ());
		assert ((_dly_size & _dly_mask) == 0);
		_dly_pos = 0;
	}
	else
	{
		for (int pos = 0; pos < nbr_spl; ++pos)
		{
			_dly_buf [_dly_pos] = src_ptr [pos];
			_dly_pos = (_dly_pos + 1) & _dly_mask;
		}
	}
}



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
