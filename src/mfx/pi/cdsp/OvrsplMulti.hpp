/*****************************************************************************

        OvrsplMulti.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_pi_cdsp_OvrsplMulti_CODEHEADER_INCLUDED)
#define mfx_pi_cdsp_OvrsplMulti_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace pi
{
namespace cdsp
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: get_latency
Description:
	Returns the processing latency, in samples, the delay between input and
	output.
	As oversamplers for different ratios have different latencies, there is an
	additional compensation delay to match the various latencies. This delay is
	added at the end of the downsampling stage.
Returns: The latency, in samples. >= 0.
Throws: Nothing
==============================================================================
*/

int	OvrsplMulti::get_latency () const noexcept
{
	return _latency_arr [_ratio_l2_cap];
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	OvrsplMulti::compute_delay_time_from_latency () const noexcept
{
	const auto     latency    = get_latency ();
	const auto     delay_time = latency - _latency_arr [_ratio_l2];

	return delay_time;
}



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#endif   // mfx_pi_cdsp_OvrsplMulti_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
