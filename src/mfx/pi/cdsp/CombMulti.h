/*****************************************************************************

        CombMulti.h
        Author: Laurent de Soras, 2024

A parallel comb filter with sub-sample accuracy

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_cdsp_CombMulti_HEADER_INCLUDED)
#define mfx_pi_cdsp_CombMulti_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/dsp/spat/UniComb.h"

#include <vector>



namespace mfx
{
namespace pi
{
namespace cdsp
{



template <typename T, int NPL2>
class CombMulti
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           set_nbr_combs (int nbr_combs);
	void           set_delay_max (int comb_idx, float delay);

	void           set_delay (int comb_idx, float delay) noexcept;
	void           set_coefs (int comb_idx, float b0, float bm, float am) noexcept;

	fstb_FORCEINLINE void
	               process_sample (T dst_arr [], T x) noexcept;
	fstb_FORCEINLINE T
	               process_sample_single (int comb_idx, T x) noexcept;
	fstb_FORCEINLINE void
	               process_block (T *dst_ptr_arr [], const T src_ptr [], int nbr_spl) noexcept;
	fstb_FORCEINLINE void
	               process_block_single (int comb_idx, T dst_ptr [], const T src_ptr [], int nbr_spl) noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef dsp::spat::UniComb <T, NPL2, false> Comb;
	typedef std::vector <Comb> CombArray;

	CombArray      _comb_arr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const CombMulti &other) const = delete;
	bool           operator != (const CombMulti &other) const = delete;

}; // class CombMulti



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#include "mfx/pi/cdsp/CombMulti.hpp"



#endif // mfx_pi_cdsp_CombMulti_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
