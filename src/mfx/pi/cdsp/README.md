﻿# Common DSP

Various helper classes shared by the effects.

- **`CombMulti`**: a parallel comb filter with sub-sample accuracy.
- **`FftParam`**: helps FFT configuration and operations when processing a signal using overlapping windows.
- **`FFTRealRange`**: Wrapper around ffft::FFTRealFixLen allowing to select different lengths at runtime.
- **`FreqShift`**: A frequency shifter processor
- **`FreqSplitter5`**: Crossover filter using 5th-order IIR
- **`FreqSplitter8`**: Crossover filter using 8th-order IIR (Thiele/Linkwitz-Riley)
- **`HalfRate`**: Handles half-rate processing
- **`OvrsplMulti`**: Oversampling (up- and downsampling) for a single audio channel, variable ratio, constant latency
- **`PhaseHalfPi`**: Wrapper over hiir::PhaseHalfPi using SIMD for π/2 dephasing
- **`ShaperBag`**: Various stateless waveshapers
