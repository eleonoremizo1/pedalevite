/*****************************************************************************

        HalfRate.hpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_cdsp_HalfRate_CODEHEADER_INCLUDED)
#define mfx_pi_cdsp_HalfRate_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace pi
{
namespace cdsp
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename DS, typename US>
const DS &	HalfRate <DS, US>::use_ds () const noexcept
{
	return _ds;
}

template <typename DS, typename US>
DS &	HalfRate <DS, US>::use_ds () noexcept
{
	return _ds;
}



template <typename DS, typename US>
const US &	HalfRate <DS, US>::use_us () const noexcept
{
	return _us;
}

template <typename DS, typename US>
US &	HalfRate <DS, US>::use_us () noexcept
{
	return _us;
}



// nbr_spl: number of samples at the standard sampling rate.
// Returns the number of samples at the half rate. It can be 0.
template <typename DS, typename US>
int	HalfRate <DS, US>::start_block (int nbr_spl) noexcept
{
	assert (_block_len == 0);
	assert (nbr_spl > 0);

	_block_len = nbr_spl;
	const int      len_half = (1 - _block_pos + nbr_spl) >> 1;

	return len_half;
}



// dst_ptr: half rate
// src_ptr: standard rate
template <typename DS, typename US>
void	HalfRate <DS, US>::process_block_down (float dst_ptr [], const float src_ptr []) noexcept
{
	assert (_block_len > 0);

	int            len = _block_len;

	// If there is a memory, combines it with the first sample of the input
	// buffer.
	// Here, we assume a memory (at virtual time -1) if the block pos is 0.
	if (_block_pos == 0)
	{
		const std::array <float, 2>   buf_i { _mem_i, src_ptr [0] };
		dst_ptr [0] = _ds.process_sample (buf_i.data ());
		-- len;
		++ dst_ptr;
		++ src_ptr;
	}

	// Processes an even number of input samples
	const int      nbr_pairs = len >> 1;
	if (nbr_pairs > 0)
	{
		_ds.process_block (dst_ptr, src_ptr, nbr_pairs);
	}

	// If there is one remaining, stores it
	if ((len & 1) != 0)
	{
		_mem_i = src_ptr [len - 1];
	}
}



// dst_ptr: standard rate
// src_ptr: half rate
template <typename DS, typename US>
void	HalfRate <DS, US>::process_block_up (float dst_ptr [], const float src_ptr []) noexcept
{
	assert (_block_len > 0);

	int            len = _block_len;

	// If there is a memory, stores it at the beginning of the output buffer
	if (_block_pos != 0)
	{
		dst_ptr [0] = _mem_o;
		++ dst_ptr;
		-- len;
	}

	// Processes an even number of output samples
	const int      nbr_pairs = len >> 1;
	if (nbr_pairs > 0)
	{
		_us.process_block (dst_ptr, src_ptr, nbr_pairs);
	}

	// Remaining sample?
	if ((len & 1) != 0)
	{
		_us.process_sample (dst_ptr [len - 1], _mem_o, src_ptr [nbr_pairs]);
	}

	// Next block
	_block_pos = (_block_len + _block_pos) & 1;
	_block_len = 0;
}



template <typename DS, typename US>
void	HalfRate <DS, US>::clear_buffers () noexcept
{
	_ds.clear_buffers ();
	_us.clear_buffers ();
	_block_pos = 0;
	_block_len = 0;
	_mem_i     = 0;
}



template <typename DS, typename US>
template <typename HR>
void	HalfRate <DS, US>::sync_to (const HR &other) noexcept
{
	_block_pos = other._block_pos;
	_block_len = other._block_len;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace cdsp
}  // namespace pi
}  // namespace mfx



#endif // mfx_pi_cdsp_HalfRate_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
