/*****************************************************************************

        FilterBank.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_FilterBank_HEADER_INCLUDED)
#define mfx_pi_vocod_FilterBank_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/DataAlign.h"
#include "fstb/VecAlign.h"
#include "fstb/Vf32.h"
#include "mfx/dsp/iir/Biquad4SimdMorph.h"
#include "mfx/pi/vocod/Cst.h"
#include "mfx/piapi/PluginInterface.h"

#include <array>



namespace mfx
{
namespace pi
{
namespace vocod
{



class FilterBank
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               FilterBank ();
	               ~FilterBank ()                       = default;
	               FilterBank (const FilterBank &other) = default;
	               FilterBank (FilterBank &&other)      = default;
	FilterBank &   operator = (const FilterBank &other) = default;
	FilterBank &   operator = (FilterBank &&other)      = default;

	void           set_gain_comp (bool gain_flag) noexcept;
	void           set_nbr_chn (int nbr_chn) noexcept;
	void           set_sample_freq (double sample_freq) noexcept;

	void           set_transpose (double mult) noexcept;
	void           set_nbr_bands_per_oct (int nbpo) noexcept;
	int            get_nbr_bands_per_oct () const noexcept;
	int            get_nbr_groups () const noexcept;
	int            get_nbr_bands () const noexcept;
	int            get_nbr_chn () const noexcept;
	void           set_extra_filter_steepness (bool steep_flag) noexcept;
	void           set_band_width (double val) noexcept;

	// Ouput data must be aligned
	void           process_block (float * const dst_ptr_arr [], const float * const src_ptr_arr [], int nbr_spl, int pos_in_beg) noexcept;

	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:




/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// 8th-order band-pass filters
	// Number of stages must be even so we can halve the order (gentle slope).
	static constexpr int   _max_nbr_stages = 4;
	static_assert ((_max_nbr_stages & 1) == 0, "_max_nbr_stages must be even");

	typedef dsp::iir::Biquad4SimdMorph <
		fstb::DataAlign <true>,
		fstb::DataAlign <true>,
		fstb::DataAlign <true>
	> BiqMorph;

	// Biquads of a group of 4 bands
	typedef std::array <BiqMorph, _max_nbr_stages> Band4Filter;

	typedef std::array <Band4Filter, Cst::_max_nbr_groups> ChnFilter;
	typedef fstb::VecAlign <ChnFilter, fstb_SIMD128_ALIGN> FullFilter;

	// Filter prototype, single 2nd oder section, s-plane.
	// There are 3 band models, using LPF, BPF or HPF.
	// The normalised frequency (1 rad/s) is located on the band center
	// frequency.
	class ProtoS
	{
	public:
		std::array <double, 3>
		               _b { 1, 0, 0 };
		std::array <double, 3>
		               _a { 1, 0, 0 };
	};
	typedef std::array <ProtoS, _max_nbr_stages> ProtoBand;

	// Prototypes for a group of biquads (several bands). Can be s or z plane.
	class ProtoGroup
	{
	public:
		using Vf32 = fstb::Vf32;
		std::array <Vf32, 3>
		               _b { Vf32::zero (), Vf32::zero (), Vf32::zero () };
		std::array <Vf32, 3>
		               _a { Vf32 (1)     , Vf32::zero (), Vf32::zero () };
	};

	void           clear_buffers_biq () noexcept;
	void           update_filter () noexcept;
	void           build_basic_bands (ProtoBand &bpf, ProtoBand &lpf, ProtoBand &hpf) const noexcept;
	double         compute_w_rel () const noexcept;
	void           build_basic_bpf (ProtoBand &bpf, double bw_rel) const noexcept;
	void           build_basic_lpf_hpf (ProtoBand &lpf, ProtoBand &hpf, double w_rel_sup, double w_rel_inf) const noexcept;
	void           build_all_bands (const ProtoBand &bpf, const ProtoBand &lpf, const ProtoBand &hpf) noexcept;

	FullFilter     _biq_arr     = FullFilter (piapi::PluginInterface::_max_nbr_chn);

	// Sample frequency, Hz, > 0
	double         _sample_freq = 44100;
	float          _inv_fs      = float (1 / _sample_freq);

	// Ratio relative to the band spacing, > 0
	double         _band_width  = 1;

	// Frequency multiplier, > 0.
	double         _tune_mult   = 1;

	// Number of channels, > 0
	int            _nbr_chn     = 1;

	// Bands per octave, > 0
	int            _nbr_bpo     = 3;

	// Indicates filter coefficients must be recalculated
	bool           _dirty_flag  = true;

	// Filter slope steepness. When false, the order is halved.
	bool           _steep_flag  = false;

	// Gain compensation, depending on the number of bands
	bool           _gain_flag   = false;

	int            _nbr_bands   = 0;
	int            _nbr_groups  = 0;

	// Biquads per band. Must be even.
	int            _nbr_stages  = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const FilterBank &other) const = delete;
	bool           operator != (const FilterBank &other) const = delete;

}; // class FilterBank



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/FilterBank.hpp"



#endif   // mfx_pi_vocod_FilterBank_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
