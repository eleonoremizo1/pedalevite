/*****************************************************************************

        Cst.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_Cst_HEADER_INCLUDED)
#define mfx_pi_vocod_Cst_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/dsp/iir/Biquad4SimdData.h"



namespace mfx
{
namespace pi
{
namespace vocod
{



class Cst
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Structural parameters

	// Number of octaves covered by the vocoder bands, > 0
	static constexpr int _nbr_oct           = 6;

	// Maximum number of bands per octave (spectral resolution), > 0
	static constexpr int _max_bands_per_oct = 12;

	// Center frequency of the lowest band
	// Tuned here on F#: 440 * exp2 ((3+6) / 12 - 3)
	static constexpr double _base_freq      = 92.49860567790859973342;

	// Linear gain applied on the modulator envelopes to bring the long-term
	// RMS volume in the range of 0 dBFS. This helps fighting the global
	// volume reduction when activating the effect. For practical reasons, the
	// gain is actually applied on the carrier filters, not the envelopes.
	static constexpr double _global_gain    = 4;



	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Precalculated constants

	// Maximum number of bands, total
	static constexpr int _max_nbr_bands   = _nbr_oct * _max_bands_per_oct;

	// Number of processing paths per SIMD vector
	static constexpr int _biq_per_pack_l2 = dsp::iir::Biquad4SimdData::_nbr_units_l2;
	static constexpr int _biq_per_pack    = 1 << _biq_per_pack_l2;

	// Maxium number of SIMD vectors per channel
	static constexpr int _max_nbr_groups  =
		fstb::div_ceil (_max_nbr_bands, _biq_per_pack);

	// Level below the modulator pass-through is totally disabled, base 2 log
	static constexpr float _thru_thr_off  = -16; // About -96 dB



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               Cst ()                               = delete;
	               Cst (const Cst &other)               = delete;
	               Cst (Cst &&other)                    = delete;
	Cst &          operator = (const Cst &other)        = delete;
	Cst &          operator = (Cst &&other)             = delete;
	bool           operator == (const Cst &other) const = delete;
	bool           operator != (const Cst &other) const = delete;

}; // class Cst



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/Cst.hpp"



#endif   // mfx_pi_vocod_Cst_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
