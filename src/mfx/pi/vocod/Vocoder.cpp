/*****************************************************************************

        Vocoder.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Approx.h"
#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/dsp/iir/DesignEq2p.h"
#include "mfx/dsp/iir/TransSZBilin.h"
#include "mfx/dsp/mix/Align.h"
#include "mfx/pi/vocod/Param.h"
#include "mfx/pi/vocod/Vocoder.h"
#include "mfx/piapi/Dir.h"
#include "mfx/piapi/Err.h"
#include "mfx/piapi/EventParam.h"
#include "mfx/piapi/EventTs.h"
#include "mfx/piapi/EventType.h"
#include "mfx/piapi/ProcInfo.h"

#include <algorithm>

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace vocod
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



Vocoder::Vocoder (piapi::HostInterface &host)
:	_host (host)
{
	dsp::mix::Align::setup ();

	if (_sf_a1_coef_arr.back () == 0)
	{
		for (int stage_idx = 0; stage_idx < _nbr_stages_sf; ++stage_idx)
		{
			const auto    a1 = dsp::iir::DesignEq2p::compute_butter_coef_a1 (
				_nbr_stages_sf * 2, stage_idx
			);
			_sf_a1_coef_arr [stage_idx] = float (a1);
		}
	}

	const ParamDescSet & desc_set = _desc.use_desc_set ();
	_state_set.init (piapi::ParamCateg_GLOBAL, desc_set);

	_state_set.set_val_nat (desc_set, Param_RESOL   , 3);
	_state_set.set_val_nat (desc_set, Param_BAND_W  , 1);
	_state_set.set_val_nat (desc_set, Param_SLOPE   , 0);
	_state_set.set_val_nat (desc_set, Param_TRANSP  , 0);
	_state_set.set_val_nat (desc_set, Param_TUNE    , 0);
	_state_set.set_val_nat (desc_set, Param_EF_RLS  , 0.010);
	_state_set.set_val_nat (desc_set, Param_COMP    , 0);
	_state_set.set_val_nat (desc_set, Param_THRU    , Cst::_thru_thr_off);
	_state_set.set_val_nat (desc_set, Param_SIBILANT, 1);
	_state_set.set_val_nat (desc_set, Param_DIST    , 0);
	_state_set.set_val_nat (desc_set, Param_PAN     , 0);

	_state_set.add_observer (Param_RESOL   , _param_change_flag_bank);
	_state_set.add_observer (Param_BAND_W  , _param_change_flag_bank);
	_state_set.add_observer (Param_SLOPE   , _param_change_flag_bank);
	_state_set.add_observer (Param_TUNE    , _param_change_flag_bank);
	_state_set.add_observer (Param_TRANSP  , _param_change_flag_env);
	_state_set.add_observer (Param_EF_RLS  , _param_change_flag_env);
	_state_set.add_observer (Param_COMP    , _param_change_flag_env);
	_state_set.add_observer (Param_THRU    , _param_change_flag_env);
	_state_set.add_observer (Param_SIBILANT, _param_change_flag_proc);
	_state_set.add_observer (Param_DIST    , _param_change_flag_proc);
	_state_set.add_observer (Param_PAN     , _param_change_flag_proc);

	// The distortion parameter has a long update rate because of the
	// curvature of non-linear operations on the value, causing a click when
	// internal gains ramp between values to far away from each other and
	// become mismatched in the middle of the course.
	// Example: lerp (beg, end) * lerp (1/beg, 1/end) != 1
	_state_set.set_ramp_time (Param_SIBILANT, 0.010);
	_state_set.set_ramp_time (Param_DIST    , 0.050);
	_state_set.set_ramp_time (Param_PAN     , 0.010);
	_state_set.set_ramp_time (Param_TRANSP  , 0.010);

	_param_change_flag_bank.add_observer (_param_change_flag);
	_param_change_flag_env .add_observer (_param_change_flag);
	_param_change_flag_proc.add_observer (_param_change_flag);

	for (auto &chn : _chn_arr)
	{
		chn._rate_halver.use_us ().set_coefs (Coef::X2::_coef_list.data ());
		chn._rate_halver.use_ds ().set_coefs (Coef::X2::_coef_list.data ());
		chn._dspl_voice.set_coefs (Coef::X2::_coef_list.data ());
	}

	_fb_voice.set_gain_comp (false);
	_fb_music.set_gain_comp (true);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInterface::State	Vocoder::do_get_state () const
{
	return _state;
}



double	Vocoder::do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const
{
	fstb::unused (categ, note_id);
	assert (categ == piapi::ParamCateg_GLOBAL);

	return _state_set.use_state (index).get_val_tgt ();
}



int	Vocoder::do_reset (double sample_freq, int max_buf_len, int &latency)
{
	latency = 0;

	_sample_freq = float (    sample_freq);
	_inv_fs      = float (1 / sample_freq);
	_max_buf_len = max_buf_len;
	auto           fs_band = _sample_freq;
	if (_halfrate_flag)
	{
		fs_band *= 0.5f;
	}

	_state_set.set_sample_freq (sample_freq);
	_state_set.clear_buffers ();

	_fb_music.set_sample_freq (fs_band);
	_fb_voice.set_sample_freq (fs_band);
	_env_voice.set_sample_freq (fs_band);
	_env_music.set_sample_freq (fs_band);
	config_sibilant_filter ();

	/*** To do:
	Change the time depending on the band frequency, so we can have a shorter
	release time in general a longer attack time for low frequencies, creating
	lower distortion/wobble on modulated things.
	***/
	_env_music.set_atk_time (0.0005);
	_env_music.set_rls_time (0.100);

	update_buffers ();

	_param_change_flag_bank.set ();
	_param_change_flag_env .set ();
	_param_change_flag_proc.set ();

	update_param (true);
	_param_proc.req_steady ();

	clear_buffers ();

	_state = State_ACTIVE;

	return piapi::Err_OK;
}



void	Vocoder::do_process_block (piapi::ProcInfo &proc)
{
	const int      nbr_chn_src = proc._dir_arr [piapi::Dir_IN ]._nbr_chn;
	const int      nbr_chn_dst = proc._dir_arr [piapi::Dir_OUT]._nbr_chn;
	assert (nbr_chn_src <= nbr_chn_dst);
	const int      nbr_chn_proc = std::min (nbr_chn_src, nbr_chn_dst);
	if (nbr_chn_proc != _nbr_chn)
	{
		set_nbr_chn (nbr_chn_proc);
	}

	const int      nbr_spl = proc._nbr_spl;

	// Events
	_param_proc.handle_msg (proc);

	// Parameters
	_state_set.process_block (nbr_spl);
	update_param ();
	if (_param_proc.is_full_reset ())
	{
		clear_buffers ();
	}

	_sibilant_lvl.tick (nbr_spl);
	_pan_amt.tick (nbr_spl);
	_transpose.tick (nbr_spl);
	_comp.tick (nbr_spl);
	_thru.tick (nbr_spl);

	// Signal processing
	const float * const * src_v_ptr_arr  = proc._src_arr;
	const float * const * src_m_ptr_arr  = proc._src_arr + nbr_chn_src;

	const float * const * sc_src_ptr_arr = src_m_ptr_arr;
	const float * const * io_src_ptr_arr = src_v_ptr_arr;
	float * const *       io_dst_ptr_arr = proc._dst_arr;

	// First, extracts the sibilant data
	extract_sibilant (src_v_ptr_arr, nbr_spl);

	// Downsampling. Retrieves the number of samples at samplerate/2.
	int            work_len = nbr_spl;
	if (_halfrate_flag)
	{
		sc_src_ptr_arr = _buf_ptr_arr_ds_sc.data ();
		io_src_ptr_arr = _buf_ptr_arr_ds_io.data ();
		io_dst_ptr_arr = _buf_ptr_arr_ds_io.data ();
		work_len = process_block_downsample (
			src_m_ptr_arr, src_v_ptr_arr, nbr_spl
		);
	}

	if (work_len > 0)
	{
		// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		// music (carrier)

		// Band split
		_fb_music.process_block (
			_buf_ptr_arr_music.data (),
			sc_src_ptr_arr,
			work_len, 0
		);

		constexpr auto comp_thr  = 1e-4f;
		const bool     comp_flag =
			(   _comp.get_beg () > comp_thr
			 || _comp.get_end () > comp_thr);

		constexpr auto thru_thr  = Cst::_thru_thr_off + 0.1f;
		const bool     thru_flag =
			(   _thru.get_beg () > thru_thr
			 || _thru.get_end () > thru_thr);

		// Carrier dynamics analysis
		if (comp_flag || thru_flag)
		{
			// Extract envelopes
			_env_music.process_block (
				_buf_ptr_arr_env_mus.data (),
				_buf_ptr_arr_music.data (),
				work_len
			);

			// Compression
			compress_block (work_len);
		}

		// Adds distortion
		_distortion.process_block (_buf_ptr_arr_music.data (), _fb_music, work_len);

		// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		// Voice (modulator)

		// Band split
		_fb_voice.process_block (
			_buf_ptr_arr_voice.data (),
			io_src_ptr_arr,
			work_len, 0
		);

		// Extracts envelopes
		_env_voice.process_block (
			_buf_ptr_arr_env_voc.data (),
			_buf_ptr_arr_voice.data (),
			work_len
		);

		// Applies the voice envelopes to the music signals
		combine_bands_envelopes (work_len);
		process_mix_pan (io_dst_ptr_arr, work_len);

		// Pass-through for the voice
		if (thru_flag)
		{
			mix_voice_signal (io_dst_ptr_arr, work_len);
		}
	}

	// Upsampling
	if (_halfrate_flag)
	{
		process_block_upsample (proc._dst_arr);
	}

	// Sibilant mixing
	merge_sibilant (proc._dst_arr, nbr_spl);

	// Duplicates the remaining output channels
	for (int chn_idx = nbr_chn_proc; chn_idx < nbr_chn_dst; ++chn_idx)
	{
		dsp::mix::Align::copy_1_1 (
			proc._dst_arr [chn_idx],
			proc._dst_arr [0],
			nbr_spl
		);
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	Vocoder::clear_buffers () noexcept
{
	_sibilant_lvl.clear_buffers ();
	_pan_amt.clear_buffers ();
	_transpose.clear_buffers ();
	_comp.clear_buffers ();

	_fb_voice.clear_buffers ();
	_fb_music.clear_buffers ();
	_env_voice.clear_buffers ();
	_env_music.clear_buffers ();
	_sibilant_filter.clear_buffers ();

	for (auto &chn : _chn_arr)
	{
		chn._rate_halver.clear_buffers ();
		chn._dspl_voice.clear_buffers ();
	}
}



void	Vocoder::update_param (bool force_flag) noexcept
{
	if (_param_change_flag (true) || force_flag)
	{
		if (_param_change_flag_bank (true) || force_flag)
		{
			update_param_bank ();
		}
		if (_param_change_flag_env (true) || force_flag)
		{
			update_param_env ();
		}
		if (_param_change_flag_proc (true) || force_flag)
		{
			update_param_proc ();
		}
	}
}



void	Vocoder::update_param_bank () noexcept
{
	const auto     steep_flag = _state_set.get_val_bool (Param_SLOPE);
	_fb_music.set_extra_filter_steepness (steep_flag);
	_fb_voice.set_extra_filter_steepness (steep_flag);

	const auto     resol = _state_set.get_val_int (Param_RESOL);
	_fb_music.set_nbr_bands_per_oct (resol);
	_fb_voice.set_nbr_bands_per_oct (resol);
	_distortion.set_nbr_bands_per_oct (resol);

	// Update groups on envelope detectors
	const int      nbr_grp_v = _fb_voice.get_nbr_groups ();
	_env_voice.set_nbr_groups (nbr_grp_v);
	const int      nbr_grp_m = _fb_music.get_nbr_groups ();
	_env_music.set_nbr_groups (nbr_grp_m);

	const auto     width = float (_state_set.get_val_end_nat (Param_BAND_W));
	_fb_music.set_band_width (width);

	constexpr auto cents_to_oct = 1.f / float (12 * 100);
	const auto     cents = float (_state_set.get_val_end_nat (Param_TUNE));
	const auto     mult  = fstb::Approx::exp2 (cents * cents_to_oct);
	_fb_music.set_transpose (mult);
	_fb_voice.set_transpose (mult);
}



void	Vocoder::update_param_env () noexcept
{
	const auto     t_rls = _state_set.get_val_end_nat (Param_EF_RLS);

	// Automatically sets the attack time from the release time.
	// Gives very sharp attack for low release values, then catches up
	// to match the release time at the highest values.
	const auto     t_atk = t_rls * fstb::limit (t_rls * t_rls, 0.01, 0.25);

	_env_voice.set_rls_time (t_rls);
	_env_voice.set_atk_time (t_atk);

	const auto     transp = float (_state_set.get_val_end_nat (Param_TRANSP));
	_transpose.set_val (transp);

	const auto     comp = float (_state_set.get_val_end_nat (Param_COMP));
	_comp.set_val (comp);

	const auto     thru = float (_state_set.get_val_end_nat (Param_THRU));
	_thru.set_val (thru);
}



void	Vocoder::update_param_proc () noexcept
{
	const auto     sib_lvl  = float (_state_set.get_val_end_nat (Param_SIBILANT));
	_sibilant_lvl.set_val (sib_lvl);

	const auto     dist_amt = float (_state_set.get_val_end_nat (Param_DIST));
	_distortion.set_amount (dist_amt);

	const auto     pan_amt  = float (_state_set.get_val_end_nat (Param_PAN));
	_pan_amt.set_val (pan_amt);
}



void	Vocoder::set_nbr_chn (int nbr_chn) noexcept
{
	assert (nbr_chn > 0);

	const auto     nbr_chn_old = _nbr_chn;
	_nbr_chn = nbr_chn;

	_fb_voice.set_nbr_chn (_nbr_chn);
	_fb_music.set_nbr_chn (_nbr_chn);
	_env_voice.set_nbr_chn (_nbr_chn);
	_env_music.set_nbr_chn (_nbr_chn);
	_sibilant_filter.set_config (_nbr_stages_sf, _nbr_chn);

	config_sibilant_filter ();

	// Cleans up new channels
	for (int chn_idx = nbr_chn_old; chn_idx < _nbr_chn; ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];
		chn._rate_halver.clear_buffers ();
		chn._rate_halver.sync_to (_chn_arr.front ()._rate_halver);
		chn._dspl_voice.clear_buffers ();
	}
}



void	Vocoder::update_buffers ()
{
	// 2x-downsampled data, rounded to +oo
	int            max_buf_len_band = _max_buf_len;
	if (_halfrate_flag)
	{
		max_buf_len_band = fstb::div_ceil (max_buf_len_band, 2);
	}
	_buf_len_band = max_buf_len_band * Cst::_biq_per_pack;
	const int      buf_zone_len = _max_nbr_buf * _buf_len_band;

	_buf_band_music.resize (buf_zone_len);
	_buf_band_voice.resize (buf_zone_len);
	_buf_band_mix.resize (buf_zone_len);
	_buf_env_mus.resize (buf_zone_len);
	_buf_env_voc.resize (buf_zone_len);

	update_buffer_pointers (_buf_ptr_arr_music, _buf_band_music);
	update_buffer_pointers (_buf_ptr_arr_voice, _buf_band_voice);
	update_buffer_pointers (_buf_ptr_arr_mix, _buf_band_mix);
	update_buffer_pointers (_buf_ptr_arr_env_mus, _buf_env_mus);
	update_buffer_pointers (_buf_ptr_arr_env_voc, _buf_env_voc);

	for (int chn_idx = 0; chn_idx < _max_nbr_chn; ++chn_idx)
	{
		auto &         chn   = _chn_arr [chn_idx];

		chn._buf_sib.resize (_max_buf_len);
		_buf_ptr_arr_sib [chn_idx] = chn._buf_sib.data ();

		chn._buf_ds_io.resize (_buf_len_band);
		_buf_ptr_arr_ds_io [chn_idx] = chn._buf_ds_io.data ();

		chn._buf_ds_sc.resize (_buf_len_band);
		_buf_ptr_arr_ds_sc [chn_idx] = chn._buf_ds_sc.data ();

		chn._buf_spread.resize (_buf_len_band);
	}
}



void	Vocoder::update_buffer_pointers (BufPtrArray &buf_ptr_arr, BufAlign &buf_zone) noexcept
{
	assert (! buf_zone.empty ());

	const int      nbr_buf = _max_nbr_chn * Cst::_max_nbr_groups;
	for (int buf_idx = 0; buf_idx < nbr_buf; ++buf_idx)
	{
		const int      offset = buf_idx * _buf_len_band;
		buf_ptr_arr [buf_idx] = &(buf_zone [offset]);
		assert (buf_ptr_arr [buf_idx] != nullptr);
	}
}



/*** To do:
- Adjust the cutoff with the tuning
- Use an approximation for the transform
***/
void	Vocoder::config_sibilant_filter () noexcept
{
	const double   f0 = Cst::_base_freq * double (1L << Cst::_nbr_oct);

	std::array <float, 3>   bz {};
	std::array <float, 3>   az {};
	for (int stage_idx = 0; stage_idx < _nbr_stages_sf; ++stage_idx)
	{
		// Simple high-pass filter
		const std::array <float, 3> bs = { 0, 0, 1 };
		const std::array <float, 3> as = { 1, _sf_a1_coef_arr [stage_idx], 1 };
		dsp::iir::TransSZBilin::map_s_to_z (
			bz.data (), az.data (),
			bs.data (), as.data (),
			f0, _sample_freq
		);

		for (int chn = 0; chn < _nbr_chn; ++chn)
		{
			_sibilant_filter.set_biquad (
				stage_idx, chn, bz.data (), az.data (), false
			);
		}
	}
}



int	Vocoder::process_block_downsample (const float * const music_ptr_arr [], const float * const voice_ptr_arr [], int nbr_spl) noexcept
{
	assert (_halfrate_flag);
	assert (music_ptr_arr != nullptr);
	assert (music_ptr_arr [0] != nullptr);
	assert (music_ptr_arr [_nbr_chn - 1] != nullptr);
	assert (voice_ptr_arr != nullptr);
	assert (voice_ptr_arr [0] != nullptr);
	assert (voice_ptr_arr [_nbr_chn - 1] != nullptr);
	assert (nbr_spl > 0);
	assert (nbr_spl <= _max_buf_len);

	int            nsh = 0;

	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];
		const auto     nsh_tmp = chn._rate_halver.start_block (nbr_spl);
		assert (chn_idx == 0 || nsh_tmp == nsh);
		assert (nsh_tmp >= 0);
		assert (nsh_tmp <= _buf_len_band);
		nsh = nsh_tmp;

		chn._rate_halver.process_block_down (
			chn._buf_ds_sc.data (), music_ptr_arr [chn_idx]
		);
		chn._dspl_voice.process_block (
			chn._buf_ds_io.data (), voice_ptr_arr [chn_idx],
			nsh
		);
	}

	return nsh;
}



void	Vocoder::process_block_upsample (float * const dst_ptr_arr []) noexcept
{
	assert (_halfrate_flag);
	assert (dst_ptr_arr != nullptr);

	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];
		chn._rate_halver.process_block_up (
			dst_ptr_arr [chn_idx], chn._buf_ds_io.data ()
		);
	}
}



void	Vocoder::extract_sibilant (const float * const voice_ptr_arr [], int nbr_spl) noexcept
{
	assert (voice_ptr_arr != nullptr);
	assert (voice_ptr_arr [0] != nullptr);
	assert (voice_ptr_arr [_nbr_chn - 1] != nullptr);
	assert (nbr_spl > 0);
	assert (nbr_spl <= _max_buf_len);

	_sibilant_filter.process_block (
		_buf_ptr_arr_sib.data (), voice_ptr_arr, 0, nbr_spl
	);
}



void	Vocoder::merge_sibilant (float * const dst_ptr_arr [], int nbr_spl) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (dst_ptr_arr [0] != nullptr);
	assert (dst_ptr_arr [_nbr_chn - 1] != nullptr);

	const auto     gain_beg = _sibilant_lvl.get_beg ();
	const auto     gain_end = _sibilant_lvl.get_end ();
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];
		dsp::mix::Align::mix_1_1_vlrauto (
			dst_ptr_arr [chn_idx],
			chn._buf_sib.data (),
			nbr_spl,
			gain_beg,
			gain_end
		);
	}
}



/*** To do: compute the envelope w/o square root and compensate in the
gain formula ***/
void	Vocoder::compress_block (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);
	assert (nbr_spl <= _max_buf_len);

	// Compression formula:
	// https://www.desmos.com/calculator/o8p6wv0aee

	// Neutral level, linear.
	constexpr auto lvl_n    = 0.5f;

	// Knee threshold, linear (-80 dB)
	constexpr auto thr_f    = 1e-4f;

	const auto     thr2     = fstb::Vf32 (fstb::sq (thr_f));
	const auto     lvl_n_l2 = fstb::Vf32 (fstb::Approx::log2 (lvl_n));
	const auto     thr_l2   = fstb::Vf32 (fstb::Approx::log2 (thr_f));
	const auto     gain_ofs = lvl_n_l2 - thr_l2;

	// Ramp variables for the compression amount
	fstb::Vf32     c_beg;
	fstb::Vf32     c_step;
	fstb::ToolsSimd::start_lerp (
		c_beg, c_step, _comp.get_beg (), _comp.get_end (), nbr_spl
	);

	const int      nbr_groups = _fb_music.get_nbr_groups ();
	int            buf_index  = 0;
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		for (int group_idx = 0; group_idx < nbr_groups; ++group_idx)
		{
			auto           spl_ptr =
				fstb::Vf32::ptr_v (_buf_ptr_arr_music [buf_index]);
			auto           env_ptr =
				fstb::Vf32::ptr_v (_buf_ptr_arr_env_mus [buf_index]);
			auto           c_cur   = c_beg;

			for (int pos = 0; pos < nbr_spl; ++pos)
			{
				auto           x       = fstb::Vf32::load (spl_ptr + pos);
				const auto     env     = fstb::Vf32::load (env_ptr + pos);
				const auto     env_l2  = fstb::Approx::log2 (max (env, thr2));
				const auto     gain_l2_pre = gain_ofs - abs ((env_l2 - thr_l2));
				const auto     gain_l2 = c_cur * gain_l2_pre;
				const auto     gain    = fstb::Approx::exp2 (gain_l2);
				x *= gain;
				x.store (spl_ptr + pos);
				c_cur += c_step;
			}

			++ buf_index;
		}
	}
}



void	Vocoder::combine_bands_envelopes (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	const int      buf_len    = nbr_spl * Cst::_biq_per_pack;

	const int      nbr_bands  = _fb_voice.get_nbr_bands ();
	const int      nbr_groups = _fb_voice.get_nbr_groups ();
	const int      nbr_bpo    = _fb_voice.get_nbr_bands_per_oct ();

	const auto     transp_beg = _transpose.get_beg ();
	const auto     transp_end = _transpose.get_end ();

	const int		band_shift_beg = fstb::limit (
		fstb::round_int (-transp_beg * float (nbr_bpo)),
		1 - nbr_bands,
		nbr_bands - 1
	);
	const int		band_shift_end = fstb::limit (
		fstb::round_int (-transp_end * float (nbr_bpo)),
		1 - nbr_bands,
		nbr_bands - 1
	);

	// Simple stuff, no transpose
	if (band_shift_beg == 0 && band_shift_end == 0)
	{
		const int      nbr_buf = nbr_groups * _nbr_chn;
		for (int buf_idx = 0; buf_idx < nbr_buf; ++buf_idx)
		{
			float *        music_ptr = _buf_ptr_arr_music [buf_idx];
			const float *  voice_ptr = _buf_ptr_arr_env_voc [buf_idx];
			dsp::mix::Align::mult_ip_1_1 (music_ptr, voice_ptr, buf_len);
		}
	}

	// Static shift
	else
	{
		for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
		{
			for (int band = 0; band < nbr_bands; ++band)
			{
				const int      buf_base = nbr_groups * chn_idx;

				auto           [group, subgroup] = find_band (band);
				float * fstb_RESTRICT   music_ptr =
					_buf_ptr_arr_music [buf_base + group] + subgroup;

				// Static
				if (band_shift_beg == band_shift_end)
				{
					// Source envelope
					const int      band_env = band + band_shift_beg;

					if (band_env < 0 || band_env >= nbr_bands)
					{
						dsp::mix::Generic::clear_nim (
							music_ptr, nbr_spl, 1, Cst::_biq_per_pack - 1
						);
					}

					else
					{
						std::tie (group, subgroup) = find_band (band_env);
						const float * fstb_RESTRICT   voice_1_ptr =
							_buf_ptr_arr_env_voc [buf_base + group] + subgroup;

						for (int pos = 0; pos < buf_len; pos += Cst::_biq_per_pack)
						{
							music_ptr [pos] *= voice_1_ptr [pos];
						}
					}
				}

				// Transition
				else
				{
					// Source envelope
					const int      band_env_beg = band + band_shift_beg;
					const int      band_env_end = band + band_shift_end;

					const float    zero = 0;
					const float * fstb_RESTRICT   voice_1_ptr = &zero;
					const float * fstb_RESTRICT   voice_2_ptr = &zero;

					int            pos_1_inc = 0;
					int            pos_2_inc = 0;

					if (band_env_beg >= 0 && band_env_beg < nbr_bands)
					{
						std::tie (group, subgroup) = find_band (band_env_beg);
						voice_1_ptr = _buf_ptr_arr_env_voc [buf_base + group] + subgroup;
						pos_1_inc = Cst::_biq_per_pack;
					}
					if (band_env_end >= 0 && band_env_end < nbr_bands)
					{
						std::tie (group, subgroup) = find_band (band_env_end);
						voice_2_ptr = _buf_ptr_arr_env_voc [buf_base + group] + subgroup;
						pos_2_inc = Cst::_biq_per_pack;
					}

					// Source bands are completely out of range
					if (pos_1_inc == 0 && pos_2_inc == 0)
					{
						dsp::mix::Generic::clear_nim (
							music_ptr, nbr_spl, 1, Cst::_biq_per_pack - 1
						);
					}

					// Source bands are partially or totally in range
					else
					{
						int            pos_1 = 0;
						int            pos_2 = 0;
						float	         lerp  = 0;
						const auto     lerp_step = fstb::rcp_uint <float> (nbr_spl);
						for (int pos_d = 0; pos_d < buf_len; pos_d += Cst::_biq_per_pack)
						{
							const float    val_1 = voice_1_ptr [pos_1];
							const float    val_2 = voice_2_ptr [pos_2];
							const float    val = val_1 + lerp * (val_2 - val_1);
							music_ptr [pos_d] *= val;
							lerp  += lerp_step;
							pos_1 += pos_1_inc;
							pos_2 += pos_2_inc;
						}
					}
				}
			}
		}
	}
}



/*
Formula, for 2 channels:

Lout = LERP (pan, sum (L), sum (L+R of even bands))
Rout = LERP (pan, sum (R), sum (L+R of odd bands))
*/
void	Vocoder::process_mix_pan (float * const dst_ptr_arr [], int nbr_spl) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (nbr_spl > 0);

	const float    pan_pos_beg = _pan_amt.get_beg ();
	const float    pan_pos_end = _pan_amt.get_end ();

	const bool		spread_flag = (pan_pos_beg > 0 || pan_pos_end > 0);
	const bool		direct_flag = (pan_pos_beg < 1 || pan_pos_end < 1);

	if (spread_flag)
	{
		// Mix bands to mono into the voice buffers, channel 0
		process_mix_pan_part_mono (nbr_spl);

		spread_bands_to_chn (nbr_spl);
	}
	if (direct_flag)
	{
		// Straight band mix for each channel, music buffer
		process_mix_pan_part_direct (nbr_spl);
	}

	// At this point we have 2 buffers per channel, stored in the music
	// buffers:
	// - 0: direct channel data
	// - 1: spread data (a specific set of bands for each channel)
	// By blending both, we can set the stereo spread.
	const int		nbr_groups = _fb_voice.get_nbr_groups ();
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		auto &         chn        = _chn_arr [chn_idx];
		const int      buf_base   = chn_idx * nbr_groups;
		float *        dst_ptr    = dst_ptr_arr [chn_idx];
		assert (dst_ptr != nullptr);
		const float *  direct_ptr = _buf_ptr_arr_music [buf_base];
		const float *  spread_ptr = chn._buf_spread.data ();

		if (spread_flag && direct_flag)
		{
			dsp::mix::Align::copy_xfade_2_1_vlrauto (
				dst_ptr, direct_ptr, spread_ptr,
				nbr_spl, pan_pos_beg, pan_pos_end
			);
		}
		else if (spread_flag)
		{
			dsp::mix::Align::copy_1_1 (dst_ptr, spread_ptr, nbr_spl);
		}
		else
		{
			assert (direct_flag);
			dsp::mix::Align::copy_1_1 (dst_ptr, direct_ptr, nbr_spl);
		}
	}
}



// Mixes each band from the music buffer into mono.
// The bands are stored (temporarily) in the mix buffers of channel 0
// The sample structure is still "packed per group"
void	Vocoder::process_mix_pan_part_mono (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	const int      nbr_groups = _fb_voice.get_nbr_groups ();
	const int      buf_len    = nbr_spl * Cst::_biq_per_pack;
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		const int		buf_base = chn_idx * nbr_groups;
		for (int group = 0; group < nbr_groups; ++group)
		{
			const float *  src_ptr = _buf_ptr_arr_music [group + buf_base];
			float *        dst_ptr = _buf_ptr_arr_mix [group];

			if (chn_idx == 0)
			{
				dsp::mix::Align::copy_1_1 (dst_ptr, src_ptr, buf_len);
			}
			else
			{
				dsp::mix::Align::mix_1_1 (dst_ptr, src_ptr, buf_len);
			}
		}
	}
}



// Collects every N mono band (previously mixed to the voice buffers) and sums
// them together into the _buf_spread for each channel. In a stereo setup,
// even bands are mixed to L and odd bands to R.
void	Vocoder::spread_bands_to_chn (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	const int      nbr_bands  = _fb_voice.get_nbr_bands ();
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		auto &         chn      = _chn_arr [chn_idx];
		float *        dest_ptr = chn._buf_spread.data ();

		// Always collects first and last bands
		const float *  src_ptr = _buf_ptr_arr_music [0];
		dsp::mix::Align::copy_ni1_1 (
			dest_ptr, src_ptr, nbr_spl, Cst::_biq_per_pack
		);
		auto           [group, lane] = find_band (nbr_bands - 1);
		src_ptr = _buf_ptr_arr_music [group] + lane;
		dsp::mix::Align::mix_ni1_1 (
			dest_ptr, src_ptr, nbr_spl, Cst::_biq_per_pack
		);

		// Collects every N bands
		for (int band = chn_idx + 1; band < nbr_bands - 1; band += _nbr_chn)
		{
			std::tie (group, lane) = find_band (band);
			src_ptr = _buf_ptr_arr_mix [group] + lane;
			if (band == chn_idx)
			{
				dsp::mix::Align::copy_ni1_1 (
					dest_ptr, src_ptr, nbr_spl, Cst::_biq_per_pack
				);
			}
			else
			{
				dsp::mix::Align::mix_ni1_1 (
					dest_ptr, src_ptr, nbr_spl, Cst::_biq_per_pack
				);
			}
		}
	}
}



// Mixes all the bands of each channel.
// The music buffer 0 of each channel contains the sum of the bands.
// The sample structure is a flat buffer
void	Vocoder::process_mix_pan_part_direct (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	const int      buf_len    = nbr_spl * Cst::_biq_per_pack;
	const int      nbr_groups = _fb_voice.get_nbr_groups ();
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		const int      buf_base = chn_idx * nbr_groups;
		float *        dst_ptr  = _buf_ptr_arr_music [buf_base];

		// First sums all the lanes in parallel
		for (int group = 1; group < nbr_groups; ++group)
		{
			const float *  src_ptr = _buf_ptr_arr_music [buf_base + group];
			dsp::mix::Align::mix_1_1 (dst_ptr, src_ptr, buf_len);
		}

		// Finally, sums the lanes in place
		dsp::mix::Align::copy_4i_1 (dst_ptr, dst_ptr, nbr_spl);
	}
}



// When a carrier (music) band is below the threshold, mixes the modulator
// (voice) content to the output.
// Uses _chn_arr[]._buf_spread as a temporary accumulation buffer
/*** To do:
- -3 dB/oct slope on the thru threshold.
- Factorize the log2 on the envelope detection for _buf_ptr_arr_env_mus, so we
can even skip the square root step.
***/
void	Vocoder::mix_voice_signal (float * const dst_ptr_arr [], int nbr_spl) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (nbr_spl > 0);

	const auto     knee       = 1.f; // Transition radius, in log2
	const auto     thru_beg   = _thru.get_beg () + knee;
	const auto     thru_end   = _thru.get_end () + knee;
	fstb::Vf32     thru_beg_v;
	fstb::Vf32     thru_step;
	fstb::ToolsSimd::start_lerp (
		thru_beg_v, thru_step, thru_beg, thru_end, nbr_spl
	);

	const auto     thr_log2   = fstb::Vf32 (1e-15f); // Ensures non-zero values
	const auto     scale      = fstb::Vf32 (0.5f / knee);

	const auto     zero       = fstb::Vf32::zero ();
	const auto     one        = fstb::Vf32 (1.f);

	const int		nbr_groups = _fb_voice.get_nbr_groups ();
	for (int chn_idx = 0; chn_idx < _nbr_chn; ++chn_idx)
	{
		auto &         chn     = _chn_arr [chn_idx];
		float *        tmp_ptr = chn._buf_spread.data ();

		dsp::mix::Align::clear (tmp_ptr, nbr_spl * Cst::_biq_per_pack);

		const int      buf_base = chn_idx * nbr_groups;
		for (int grp_idx = 0; grp_idx < nbr_groups; ++grp_idx)
		{
			const auto     buf_idx = buf_base + grp_idx;
			const float *  src_ptr = _buf_ptr_arr_voice [buf_idx];
			const float *  env_ptr = _buf_ptr_arr_env_mus [buf_idx];

			auto           thru_cur = thru_beg_v;
			for (int pos = 0; pos < nbr_spl; ++pos)
			{
				const auto     pos_v  = pos << Cst::_biq_per_pack_l2;
				auto           x      = fstb::Vf32::load (src_ptr + pos_v);
				auto           y      = fstb::Vf32::load (tmp_ptr + pos_v);
				const auto     env    = fstb::Vf32::load (env_ptr + pos_v);
				const auto     env_l2 = fstb::Approx::log2 (max (env, thr_log2));
				const auto     c      = (thru_cur - env_l2) * scale;
				const auto     gain   = fstb::limit (c, zero, one);
				x *= gain;
				y += x;
				y.store (tmp_ptr + pos_v);

				thru_cur += thru_step;
			}
		}

		/*** To do: write a mix_4i_1 function ***/
		dsp::mix::Align::copy_4i_1 (tmp_ptr, tmp_ptr, nbr_spl);
		float *        dst_ptr = dst_ptr_arr [chn_idx];
		assert (dst_ptr != nullptr);
		dsp::mix::Align::mix_1_1 (dst_ptr, tmp_ptr, nbr_spl);
	}
}



// Returns: group index, lane index
std::tuple <int, int>	Vocoder::find_band (int band_idx) noexcept
{
	const auto   group_idx = band_idx >> Cst::_biq_per_pack_l2;
	const auto   lane_idx  = band_idx & (Cst::_biq_per_pack - 1);

	return { group_idx, lane_idx };
}



std::array <float, Vocoder::_nbr_stages_sf>	Vocoder::_sf_a1_coef_arr {};



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
