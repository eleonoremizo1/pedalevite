/*****************************************************************************

        DistVocBand.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Approx.h"
#include "fstb/DataAlign.h"
#include "fstb/fnc.h"
#include "fstb/def.h"
#include "fstb/Vf32.h"
#include "mfx/pi/vocod/Cst.h"
#include "mfx/pi/vocod/DistVocBand.h"
#include "mfx/pi/vocod/FilterBank.h"

#include <cassert>

#include <cmath>



namespace mfx
{
namespace pi
{
namespace vocod
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DistVocBand::set_nbr_bands_per_oct (int nbr_bands) noexcept
{
	assert (nbr_bands >= 1);

	// No use at the moment.
	fstb::unused (nbr_bands);
}



void	DistVocBand::set_amount (float amt) noexcept
{
	assert (amt >= 0);
	assert (amt <= 1);

	_gain_end.set_from_amount (amt);
}



/*
The distortion amount is ramped from its previous value to the new value,
reached at the end of the block.

Distortion as a 3rd order sigmoid with clipping at local maxima, where the
derivates are 0.
The overall shape is scaled down with increasing distortion amount.
Derivates at 0 is kept to unity until the distortion amount goes over
1/_gain_post_min. After that, the gain increases and the shape remains the
same.

The distortion gain is scaled with bands. It starts at nominal value on the
first band, and is attenuated from 1/4 (-12 dB) on the last band.

Distortion process:
- pre-gain
- clipping, so the subsequent sigmoid is clipped when 1st derivative is 0
- sigmoid: x - x^3
- post-gain so the derivative at 0 is 1 at low gain, but hard clipping
output is no less than _gain_post_min.

If the amount is near the minimum, the distortion process is bypassed.

Sample layout: see Vocoder::_buf_band_music
*/

void	DistVocBand::process_block (float * spl_ptr_arr [], const FilterBank &fb, int nbr_spl) noexcept
{
	assert (spl_ptr_arr != nullptr);
	assert (nbr_spl > 0);

	if (_gain_beg.is_distorting () || _gain_end.is_distorting ())
	{
		const int      nbr_groups  = fb.get_nbr_groups ();
		const int      nbr_bands   = fb.get_nbr_bands ();
		const int      nbr_chn     = fb.get_nbr_chn ();

		// Gain ratio between two consecutive bands
		const float    ba_l2 = _top_atten_l2 / float (nbr_bands);
		const float    ba    = fstb::Approx::exp2 (ba_l2);
		const float    ba2   = ba  * ba;
		const float    ba3   = ba2 * ba;
		const float    ba4   = ba2 * ba2;

		// We need a ratio for both gains, pre and post
		auto           ba_pre        = fstb::Vf32 (1.f, ba, ba2, ba3);
		const auto     ba_pre_step   = fstb::Vf32 (ba4);

		auto           ba_post       = ba_pre.rcp_approx2 ();
		const auto     ba_post_step  = ba_pre_step.rcp_approx2 ();

		const auto     val_mi        = fstb::Vf32 (-_clip_in);
		const auto     val_ma        = fstb::Vf32 (+_clip_in);
		const auto     gain_post_min = fstb::Vf32 (_gain_post_min);

		const auto     step_spl = fstb::Vf32 (fstb::rcp_uint <float> (nbr_spl));

		for (int group = 0; group < nbr_groups; ++group)
		{
			const auto     gain_pre_beg  =
				fstb::Vf32 (_gain_beg._pre) * ba_pre;
			const auto     gain_post_beg =
				max (fstb::Vf32 (_gain_beg._post) * ba_post, gain_post_min);

			const auto     gain_pre_end  =
				fstb::Vf32 (_gain_end._pre) * ba_pre;
			const auto     gain_post_end =
				max (fstb::Vf32 (_gain_end._post) * ba_post, gain_post_min);

			const auto     gain_pre_step  =
				(gain_pre_end  - gain_pre_beg ) * step_spl;
			const auto     gain_post_step =
				(gain_post_end - gain_post_beg) * step_spl;

			for (int chn_idx = 0; chn_idx < nbr_chn; ++chn_idx)
			{
				const int      buf_index = chn_idx * nbr_groups + group;
				float *        data_ptr  = spl_ptr_arr [buf_index];
				assert (fstb::DataAlign <true>::check_ptr (data_ptr));

				auto           gain_pre  = gain_pre_beg;
				auto           gain_post = gain_post_beg;

				for (int pos = 0; pos < nbr_spl; ++pos)
				{
					const auto     pos_scalar = pos << Cst::_biq_per_pack_l2;
					auto           x = fstb::Vf32::load (data_ptr + pos_scalar);
					x *= gain_pre;

					x  = limit (x, val_mi, val_ma);
					x -= x * x * x;

					x *= gain_post;
					x.store (data_ptr + pos_scalar);

					gain_pre  += gain_pre_step;
					gain_post += gain_post_step;
				}
			}

			ba_pre  *= ba_pre_step;
			ba_post *= ba_post_step;
		} // group
	}

	_gain_beg = _gain_end;
}



void	DistVocBand::clear_buffers () noexcept
{
	_gain_beg = _gain_end;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DistVocBand::Gain::set_from_amount (float amt) noexcept
{
	// 1 to 1000 ratio between _gain_base and maximum gain.
	constexpr auto scale = float (3 * fstb::LOG2_10);
	_pre  = _gain_base * fstb::Approx::exp2 (amt * scale);
	_post = 1.f / _pre;
}



bool	DistVocBand::Gain::is_distorting () const noexcept
{
	return (_pre >= _gain_base * 1.0625f);
}



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
