/*****************************************************************************

        VocoderDesc.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/vocod/Cst.h"
#include "mfx/pi/vocod/VocoderDesc.h"
#include "mfx/pi/vocod/Param.h"
#include "mfx/pi/param/Simple.h"
#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplInt.h"
#include "mfx/pi/param/TplLin.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/pi/param/TplSq.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace vocod
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



VocoderDesc::VocoderDesc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	_info._unique_id = "vocoder1";
	_info._name      = "Vocoder\nVocod";
	_info._tag_list  = { piapi::Tag::_vocal_0, piapi::Tag::_spectral_0 };
	_info._chn_pref  = piapi::ChnPref::NONE;

	// Resolution
	auto           int_uptr = std::make_unique <param::TplInt> (
		1, Cst::_max_bands_per_oct,
		"Resolution\nResol\nRes",
		"b/oct",
		0,
		"%.0f"
	);
	_desc_set.add_glob (Param_RESOL, int_uptr);

	// Band width
	auto           lin_uptr = std::make_unique <param::TplLin> (
		0, 2,
		"Band width\nWidth\nBW",
		"%",
		0,
		"%3.0f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_BAND_W, lin_uptr);

	// Filter slope
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		"Gentle\nSteep",
		"Filter slope\nFilt slope\nSlope\nFSl",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_SLOPE, enu_uptr);

	// Tune
	lin_uptr = std::make_unique <param::TplLin> (
		-300, +300,
		"Tune\nTn",
		"cents",
		0,
		"%+4.0f"
	);
	_desc_set.add_glob (Param_TUNE, lin_uptr);

	// Transpose
	lin_uptr = std::make_unique <param::TplLin> (
		-Cst::_nbr_oct, +Cst::_nbr_oct,
		"Transpose\nTransp\nTrs",
		"oct",
		0,
		"%+5.2f"
	);
	_desc_set.add_glob (Param_TRANSP, lin_uptr);

	// Release time
	auto           log_uptr = std::make_unique <param::TplLog> (
		0.001, 1.0,
		"Release time\nRls time\nRelease\nRls",
		"ms",
		param::HelperDispNum::Preset_FLOAT_MILLI,
		0,
		"%4.0f"
	);
	log_uptr->set_categ (piapi::ParamDescInterface::Categ_TIME_S);
	_desc_set.add_glob (Param_EF_RLS, log_uptr);

	// Compression
	auto           sim_ptr = std::make_unique <param::Simple> (
		"Compression\nCompress\nComp\nCmp"
	);
	_desc_set.add_glob (Param_COMP, sim_ptr);

	// Pass-through ceiling
	lin_uptr = std::make_unique <param::TplLin> (
		Cst::_thru_thr_off, 0,
		"Pass-through ceiling\nThrough ceiling\nThrough\nThru\nThr",
		"dB",
		0,
		"%+5.1f"
	);
	lin_uptr->use_disp_num ().set_scale (fstb::LOG10_2 * 20);
	_desc_set.add_glob (Param_THRU, lin_uptr);

	// Sibilant level
	auto           sq_uptr = std::make_unique <param::TplSq <false> > (
		0.0, 4.0,
		"Sibilant level\nSibilant lvl\nSibilant\nSibil\nSib",
		"dB",
		param::HelperDispNum::Preset_DB,
		0,
		"%+5.1f"
	);
	_desc_set.add_glob (Param_SIBILANT, sq_uptr);

	// Band distortion
	sim_ptr = std::make_unique <param::Simple> (
		"Band distortion\nDistortion\nDistort\nDist\nDst"
	);
	_desc_set.add_glob (Param_DIST, sim_ptr);

	// Pan split
	sim_ptr = std::make_unique <param::Simple> (
		"Pan split\nPan"
	);
	_desc_set.add_glob (Param_PAN, sim_ptr);
}



ParamDescSet &	VocoderDesc::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInfo	VocoderDesc::do_get_info () const
{
	return _info;
}



void	VocoderDesc::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	// Order: voice (modulator), music (carrier)
	nbr_i = 2;
	nbr_o = 1;
	nbr_s = 0;
}



int	VocoderDesc::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



const piapi::ParamDescInterface &	VocoderDesc::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
