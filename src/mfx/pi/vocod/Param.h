/*****************************************************************************

        Param.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_Param_HEADER_INCLUDED)
#define mfx_pi_vocod_Param_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace pi
{
namespace vocod
{



enum Param
{
	Param_RESOL = 0,
	Param_BAND_W,
	Param_SLOPE,
	Param_TUNE,
	Param_TRANSP,
	Param_EF_RLS,
	Param_COMP,
	Param_THRU,
	Param_SIBILANT,
	Param_DIST,
	Param_PAN,

	Param_NBR_ELT

}; // enum Param



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/Param.hpp"



#endif   // mfx_pi_vocod_Param_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
