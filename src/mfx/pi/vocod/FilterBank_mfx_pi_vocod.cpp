/*****************************************************************************

        FilterBank.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



// Define this macro to dump the filter z-plane coefficients to the debugger
// output console at each change. Works only on Windows + MSVC. Do not use in
// production code.
#undef mfx_pi_vocod_FilterBank_COEF_DUMP



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Approx.h"
#include "fstb/Vs32.h"
#include "fstb/Vx32_conv.hpp"
#include "mfx/dsp/iir/TransS.h"
#include "mfx/dsp/iir/TransSZBilin.h"
#include "mfx/pi/vocod/FilterBank.h"

#if defined (mfx_pi_vocod_FilterBank_COEF_DUMP) && ! defined (NDEBUG) && defined (_MSC_VER)
# define NOMINMAX
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include <debugapi.h>
#endif

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace vocod
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



FilterBank::FilterBank ()
{
	for (auto &chn_filt : _biq_arr)
	{
		for (auto &band4 : chn_filt)
		{
			for (auto &biq : band4)
			{
				biq.set_ramp_time (64);
			}
		}
	}

	set_nbr_bands_per_oct (3);
}



void	FilterBank::set_gain_comp (bool gain_flag) noexcept
{
	_gain_flag  = gain_flag;
	_dirty_flag = true;
}



void	FilterBank::set_nbr_chn (int nbr_chn) noexcept
{
	assert (nbr_chn >= 1);

	const int      nbr_chn_old = _nbr_chn;
	_nbr_chn    = nbr_chn;

	// Cleans up new channels
	for (int chn_idx = nbr_chn_old; chn_idx < _nbr_chn; ++chn_idx)
	{
		ChnFilter &    chn_filt = _biq_arr [chn_idx];
		for (auto &band4 : chn_filt)
		{
			for (auto &biq : band4)
			{
				biq.clear_buffers ();
			}
		}
	}

	_dirty_flag = true;
}



void	FilterBank::set_sample_freq (double sample_freq) noexcept
{
	assert (sample_freq > 0);

	_sample_freq =            sample_freq;
	_inv_fs      = float (1 / sample_freq);
	_dirty_flag = true;
}



void	FilterBank::set_transpose (double mult) noexcept
{
	assert (mult > 0);

	_tune_mult = mult;
	_dirty_flag = true;
}



void	FilterBank::set_nbr_bands_per_oct (int nbpo) noexcept
{
	assert (nbpo > 0);
	assert (nbpo <= Cst::_max_bands_per_oct);

	_nbr_bpo    = nbpo;
	_nbr_bands  = Cst::_nbr_oct * nbpo;
	_nbr_groups = fstb::div_ceil (_nbr_bands, Cst::_biq_per_pack);

	_dirty_flag = true;
}



int	FilterBank::get_nbr_bands_per_oct () const noexcept
{
	return _nbr_bpo;
}



int	FilterBank::get_nbr_groups () const noexcept
{
	assert (_nbr_groups > 0);

	return _nbr_groups;
}



int	FilterBank::get_nbr_bands () const noexcept
{
	assert (_nbr_groups > 0);

	return _nbr_bands;
}



int	FilterBank::get_nbr_chn () const noexcept
{
	assert (_nbr_groups > 0);

	return _nbr_chn;
}



void	FilterBank::set_extra_filter_steepness (bool steep_flag) noexcept
{
	_steep_flag = steep_flag;
	_dirty_flag = true;
}



void	FilterBank::set_band_width (double val) noexcept
{
	assert (val >= 0);

	_band_width = val;
	_dirty_flag = true;
}



// Input : array of channels
// Output : arrays of channels, made of arrays of groups of bands,
// indexed as chn * _nbr_groups + group (a group = 4 bands in a Vect128)
void	FilterBank::process_block (float * const dst_ptr_arr [], const float * const src_ptr_arr [], int nbr_spl, int pos_in_beg) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (dst_ptr_arr [0] != nullptr);
	assert (dst_ptr_arr [_nbr_chn * _nbr_groups - 1] != nullptr);
	assert (src_ptr_arr != nullptr);
	assert (src_ptr_arr [0] != nullptr);
	assert (src_ptr_arr [_nbr_chn - 1] != nullptr);
	assert (nbr_spl > 0);
	assert (pos_in_beg >= 0);

	if (_dirty_flag)
	{
		update_filter ();
	}

	int            out_index = 0;
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		ChnFilter &          chn_filter = _biq_arr [chn];
		const float * const  in_ptr     = src_ptr_arr [chn] + pos_in_beg;

		for (int group = 0; group < _nbr_groups; ++group)
		{
			fstb::Vf32 * const   out_ptr  = 
				fstb::Vf32::ptr_v (dst_ptr_arr [out_index]);
			assert (fstb::DataAlign <true>::check_ptr (out_ptr));
			Band4Filter &  group_filter   = chn_filter [group];

			BiqMorph &     stage_filter_1 = group_filter [0];
			stage_filter_1.process_block_parallel (out_ptr, in_ptr, nbr_spl);

			for (int stage = 1; stage < _nbr_stages; ++stage)
			{
				BiqMorph &     stage_filter = group_filter [stage];
				stage_filter.process_block_parallel (out_ptr, out_ptr, nbr_spl);
			}

			++ out_index;
		}
	}
}



void	FilterBank::clear_buffers () noexcept
{
	if (_dirty_flag)
	{
		update_filter ();
	}

	clear_buffers_biq ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	FilterBank::clear_buffers_biq () noexcept
{
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		ChnFilter &    chn_filt = _biq_arr [chn];
		for (int group = 0; group < _nbr_groups; ++group)
		{
			Band4Filter &  band4 = chn_filt [group];
			for (int stage = 0; stage < _nbr_stages; ++stage)
			{
				BiqMorph &     biq = band4 [stage];
				biq.clear_buffers ();
			}
		}
	}
}



void	FilterBank::update_filter () noexcept
{
	_dirty_flag = false;

	static_assert ((_max_nbr_stages & 1) == 0, "_max_nbr_stages must be even");
	_nbr_stages = (_steep_flag) ? _max_nbr_stages : _max_nbr_stages / 2;

	// First, designs the basic prototypes for a generic band
	// The LPF and HPF are used at each end of the filter bank
	// The BPF is for standard bands.
	ProtoBand      bpf;
	ProtoBand      lpf;
	ProtoBand      hpf;
	build_basic_bands (bpf, lpf, hpf);

	// Shift and wraps the pole/zero frequencies for each band.
	build_all_bands (bpf, lpf, hpf);
}



void	FilterBank::build_basic_bands (ProtoBand &bpf, ProtoBand &lpf, ProtoBand &hpf) const noexcept
{
	assert ((_nbr_stages & 1) == 0);

	const double   w_rel_sup = FilterBank::compute_w_rel ();
	const double   w_rel_inf = 1 / w_rel_sup;
	const double   bw_rel    = w_rel_sup - w_rel_inf;

	build_basic_bpf (bpf, bw_rel);
	build_basic_lpf_hpf (lpf, hpf, w_rel_sup, w_rel_inf);
}



// Calculates the relative bandwidth from the user bandwidth, the band
// resolution and the filter steepness.
// Returns the ratio of the upper edge frequency with the center frequency.
double	FilterBank::compute_w_rel () const noexcept
{
	const double   max_overlap = (_steep_flag) ? 0.907 : 0.79;
	const double   min_bw_r    = 0.06125;
	const double   max_bw_r    = 1.0;
	const double   real_bw_r   = min_bw_r + _band_width * (max_bw_r - min_bw_r);
	const double   bpo_rcp     = fstb::rcp_uint <double> (_nbr_bpo);
	const double   w_rel       = exp2 (0.5 * max_overlap * real_bw_r * bpo_rcp);

	return w_rel;
}



// First a LPF model is generated, half the order.
// Then a transform is applied on poles to build a BPF from the LPF.
void	FilterBank::build_basic_bpf (ProtoBand &bpf, double bw_rel) const noexcept
{
	// Actually pairs of conjugate poles.
	// 1 pair = 2 poles LPF -> 4 poles BPF = 2 biquads (or stages)
	const int      nbr_lp_pole_pairs = _nbr_stages >> 1;

	const auto     angle_mul =
		-fstb::PI * 0.5 * fstb::rcp_uint <double> (nbr_lp_pole_pairs);

	for (int pole_index = 0; pole_index < nbr_lp_pole_pairs; ++pole_index)
	{
		ProtoS &       eq_0  = bpf [pole_index * 2    ];
		ProtoS &       eq_1  = bpf [pole_index * 2 + 1];

		using TS = dsp::iir::TransS;

		// Butterworth pole location
		const double   angle = angle_mul * (pole_index + 0.5);
		const double   re    = sin (angle);
		const double   im    = cos (angle);
		const TS::Cplx lp_pole (re, im);

		// Makes two band pass biquads from the low-pass
		// pole (and its implicit conjugate)
		TS::conv_lp_ap_to_pb_biq (
			eq_0._b.data (), eq_0._a.data (),
			eq_1._b.data (), eq_1._a.data (),
			lp_pole, bw_rel
		);
	}
}



void	FilterBank::build_basic_lpf_hpf (ProtoBand &lpf, ProtoBand &hpf, double w_rel_sup, double w_rel_inf) const noexcept
{
	assert (fstb::is_eq_rel (w_rel_inf * w_rel_sup, 1.0, 1e-5));

	const int      nbr_pole_pairs = _nbr_stages; // Pairs of conjugate poles
	const int      order          = _nbr_stages * 2;
	const auto     angle_mul      = fstb::PI * fstb::rcp_uint <double> (order);
	const auto     wri2           = fstb::sq (w_rel_inf);
	const auto     wrs2           = fstb::sq (w_rel_sup);

	for (int biq_idx = 0; biq_idx < nbr_pole_pairs; ++biq_idx)
	{
		// Butterwoth pole location
		const double   angle = angle_mul * (biq_idx + 0.5);
		const double   a1    = 2 * sin (angle);

		// The scale is a1 / freq, so we use the reciprocal of the target
		// frequency, which swaps inf and sup.
		ProtoS &       eq_lpf = lpf [biq_idx];
		eq_lpf._b = { 1, 0, 0 };
		eq_lpf._a = { 1, a1 * w_rel_inf, wri2 };

		ProtoS &       eq_hpf = hpf [biq_idx];
		eq_hpf._b = { 0, 0, wrs2 };
		eq_hpf._a = { 1, a1 * w_rel_sup, wrs2 };
	}
}



// Note: excess biquads in the last group after the last band are not handled
// specifically. They are still designed like standard bands, with a cutoff
// frequency limited to a valid value. This shouldn't cause much problem, but
// more tests have to be conducted to ensure no div-by-0 or similar errors.
void	FilterBank::build_all_bands (const ProtoBand &bpf, const ProtoBand &lpf, const ProtoBand &hpf) noexcept
{
	float          comp_gain = float (Cst::_global_gain);
	if (_gain_flag)
	{
		// Power loss caused by user bandwidth reduction
		if (_band_width < 1)
		{
			// Actually depends on both the band width and the band resolution.
			const float       gain_margin = 0.40f + 0.03f * float (_nbr_bpo);
			comp_gain *= (1 + gain_margin) / (float (_band_width) + gain_margin);
		}

		// Power loss caused by the spectral division in multiple bands
		const auto     nb_sqrt = sqrtf (float (_nbr_bands));
		comp_gain *= nb_sqrt;
	}
	const auto     comp_gain_v = fstb::Vf32 (comp_gain);

	// Lowest band frequency and step-multiplier
	auto           f_0    = float (Cst::_base_freq * _tune_mult);
	const auto     f_mul  = exp2f (fstb::rcp_uint <float> (_nbr_bpo));

	// Vectorises the band frequencies
	const auto     f_mul2 = f_mul * f_mul;
	auto           f0v    =
		fstb::Vf32 (f_0, f_0 * f_mul, f_0 * f_mul2, f_0 * f_mul2 * f_mul);
	const auto     f_mul4 = f_mul2 * f_mul2;
	const auto     f_step = fstb::Vf32 (f_mul4);

	// f_max is relative to fs. It's a security check for bands out of range.
	const auto     f_max  = fstb::Vf32 (0.49f);

	// On the last group, we have to 0 the filter output to make sure
	// their output is silent (both modulator and carrier).
	// last_group_idx is purposely out of range if the last group is actually
	// full.
	const int      end_bands     = _nbr_bands & (Cst::_biq_per_pack - 1);
	const int      end_group_idx = _nbr_bands >> Cst::_biq_per_pack_l2;
	const auto     end_mask      = fstb::bit_cast <fstb::Vf32> (
		fstb::Vs32 (end_bands) > fstb::Vs32 (0, 1, 2, 3)
	);

	// Last band, where we replace the BPF with a HPF
	const int      last_band     = (_nbr_bands - 1) & (Cst::_biq_per_pack - 1);
	const auto     last_mask     = fstb::bit_cast <fstb::Vf32> (
		fstb::Vs32 (last_band) == fstb::Vs32 (0, 1, 2, 3)
	);

	ProtoGroup     eq_s;
	ProtoGroup     eq_z;
	constexpr int  eq_len = int (eq_s._b.size ());

	for (int grp_idx = 0; grp_idx < _nbr_groups; ++grp_idx)
	{
		const auto     f = f0v * fstb::Vf32 (_inv_fs);
		const auto     k =
			dsp::iir::TransSZBilin::compute_k_approx (min (f, f_max));

		for (int stg_idx = 0; stg_idx < _nbr_stages; ++stg_idx)
		{
			// First initialises the s equation for the group with the single-
			// band prototype, because of subsequent in-place modifications
			for (int i = 0; i < eq_len; ++i)
			{
				auto           b = fstb::Vf32 (bpf [stg_idx]._b [i]);
				auto           a = fstb::Vf32 (bpf [stg_idx]._a [i]);
				if (grp_idx == 0)
				{
					const auto &   biq_src = lpf [stg_idx];
					b = b.template insert <0> (float (biq_src._b [i]));
					a = a.template insert <0> (float (biq_src._a [i]));
				}
				if (grp_idx == _nbr_groups - 1)
				{
					const auto &   biq_src = hpf [stg_idx];
					b = select (last_mask, fstb::Vf32 (biq_src._b [i]), b);
					a = select (last_mask, fstb::Vf32 (biq_src._a [i]), a);
				}
				eq_s._b [i] = b;
				eq_s._a [i] = a;
			}

			/*
			The code below address several issues:

			After the tranform of the analogue prototype from LPF to BPF in the
			build_basic_band() function, we get multiple biquads (second-order
			sections). Each of them is actually a pure band-pass filter. It is
			important to note that the cutoff frequency of these new filters are
			not the main BPF frequency. They have different frequencies, the
			low-Q ones close to the main BPF freq to get a flat passband, and the
			high-Q ones on the edge of the band, to create steep slopes. But all
			these biquads are referenced to the same frequency (here, unity).

			Enters frequency warping.

			The s to z plane mapping distorts the frequencies to make +inf Hz in
			the s plane match the Nyquist frequency in the z plane. If we apply
			this naively to the biquads, the resulting shape and bandpass center
			is calculated as expected. However the filter bandwidth shrinks as
			its the center frequency increases! This is not what we want. The
			reason is simple: poles of a single band filter have different
			frequencies, but the mapping only distorts the center frequency.
			The relationship between frequencies remains the same, whereas their
			mutual ratio should be changed according to the mapping slope.

			Ideally we should map each elementary band-pass filter individually
			and use a shape-preserving transform, like the one of those proposed
			by Vicanek or Orfanidis. But they are quite calculation-intensive,
			and we don't need all the precision they offer when the response
			crosses the Nyquist frequency.

			So we are doing two things:
			- Stretch the internal, relative frequencies of the biquad poles so
			they match better their analogue target after the main frequency is
			mapped. The function is adapted from TransSZBilin::prewarp_biquad().
			- Stretch the bandwidth. This will be an approximation here too,
			based on the ratio between the wrapped frequency and the unwrapped
			one.
			There are some mysterious things still happening. The fix works best
			when we do not change the 1st-order coeffients for the frequency
			stretch (so we scale Q with the frequency too). Also we square the
			ratio applied to the bandwidth. Why is squaring needed? Is this a
			derivative-related thingy? More math invesigation needed.
			
			Anyway the result is still not perfect (the larger the bandwidth, the
			larger the error), so the theoretically ideal solution is probably
			different.
			*/

			// Frequency adaptation
			const auto     fr = sqrt (eq_s._a [2] / eq_s._a [0]);
			const auto     fb = fr * f;
			const auto     kb =
				fr * dsp::iir::TransSZBilin::compute_k_approx (min (fb, f_max));
			const auto     m  = k / kb;
			const auto     m2 = m * m;

			// Bandwidth scaling
			// (1 / (f * k)) ^ 2
#if 0
			// Using a div
			const auto     bw_scale =
				fstb::sq (f * k * fstb::Vf32 (fstb::PI)).rcp_approx ();
#else
			// Using a polynomial approximation (total guess)
			// This doesn't look much faster, but it's way more fun.
			// However this is accurate only below 0.38 * Fs
			constexpr auto c2 = fstb::ipowpc <2> (fstb::PI) * 1.05 * 1.0 / 3;
			constexpr auto c6 = fstb::ipowpc <6> (fstb::PI) * 0.21;
			const auto     f2 = f  * f;
			const auto     f4 = f2 * f2;
			const auto     bw_scale = fstb::sq (
				(fstb::Vf32 (c6) * f4 + fstb::Vf32 (c2)) * f2 + fstb::Vf32 (1)
			);
#endif

			// On the BPF, we apply the bandwidth fix. Otherwise (LPF and HPF),
			// we apply only the frequency fix.
			auto           scale_c1 = bw_scale;
			if (grp_idx == 0)
			{
				scale_c1 = scale_c1.template insert <0> (m.template extract <0> ());
			}
			if (grp_idx == _nbr_groups - 1)
			{
				scale_c1 = select (last_mask, m, scale_c1);
			}

			// Applies the modifications on the coefficients
			eq_s._b [1] *= scale_c1;
			eq_s._a [1] *= scale_c1;
			eq_s._b [2] *= m2;
			eq_s._a [2] *= m2;

			// Final transform
			dsp::iir::TransSZBilin::map_s_to_z_approx (
				eq_z._b.data (), eq_z._a.data (),
				eq_s._b.data (), eq_s._a.data (),
				k
			);

			// Make-up gain
			if (_gain_flag && stg_idx == 0)
			{
				auto           gain = comp_gain_v;
				if (grp_idx == 0)
				{
					gain = gain.template insert <0> (float (Cst::_global_gain));
				}
				if (grp_idx == _nbr_groups - 1)
				{
					gain = select (last_mask, fstb::Vf32 (Cst::_global_gain), gain);
				}
				eq_z._b [0] *= gain;
				eq_z._b [1] *= gain;
				eq_z._b [2] *= gain;
			}

			// Clears bands out of range
			if (grp_idx == end_group_idx)
			{
				eq_z._b [0] &= end_mask;
				eq_z._b [1] &= end_mask;
				eq_z._b [2] &= end_mask;
			}

			for (int chn = 0; chn < _nbr_chn; ++chn)
			{
				ChnFilter &    chn_filter = _biq_arr [chn];
				Band4Filter &  biq_group  = chn_filter [grp_idx];
				BiqMorph &     biq_pack   = biq_group [stg_idx];
				biq_pack.set_z_eq (
					reinterpret_cast <const BiqMorph::VectFlt4 *> (eq_z._b.data ()),
					reinterpret_cast <const BiqMorph::VectFlt4 *> (eq_z._a.data ()),
					true
				);
			}
		}

		f0v *= f_step;
	}

#if defined (mfx_pi_vocod_FilterBank_COEF_DUMP) && ! defined (NDEBUG) && defined (_MSC_VER)

	if (_gain_flag)
	{
		char           txt_0 [1023+1] {};
		char           coord_0 [127+1] {};

		fstb::snprintf4all (txt_0, sizeof (txt_0),
			"%% Bands: %d, slope: %s, bandwidth: %5.1f %%\n",
			_nbr_bands, _steep_flag ? "steep" : "gentle", _band_width * 100
		);
		::OutputDebugStringA (txt_0);

		for (int band_idx = 0; band_idx < _nbr_bands; ++band_idx)
		{
			fstb::snprintf4all (txt_0, sizeof (txt_0),
				"%% Band %d\n", band_idx + 1
			);
			::OutputDebugStringA (txt_0);
			const int      grp_idx  = band_idx >> Cst::_biq_per_pack_l2;
			const int      lane_idx = band_idx & (Cst::_biq_per_pack - 1);
			for (int stg_idx = 0; stg_idx < _nbr_stages; ++stg_idx)
			{
				fstb::snprintf4all (coord_0, sizeof (coord_0),
					"(%2d, %d, :)", band_idx + 1, stg_idx + 1
				);
				ChnFilter &    chn_filter = _biq_arr [0];
				Band4Filter &  biq_group  = chn_filter [grp_idx];
				BiqMorph &     biq_pack   = biq_group [stg_idx];
				std::array <float, 3> b {};
				std::array <float, 3> a {};
				biq_pack.get_z_eq_one_final (lane_idx, b.data (), a.data ());
				fstb::snprintf4all (txt_0, sizeof (txt_0),
					"b %s = [%.10g %.10g %.10g]; a %s = [%.10g %.10g %.10g];\n",
					coord_0, double (b [2]), double (b [1]), double (b [0]),
					coord_0, double (a [2]), double (a [1]), 1.0
				);
				::OutputDebugStringA (txt_0);
			}
		}
	}

#endif
}



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
