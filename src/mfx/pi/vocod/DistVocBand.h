/*****************************************************************************

        DistVocBand.h
        Author: Laurent de Soras, 2023

Applies a x - x^3 curve to the signal, with pre-gain, pre-clipping and
post gain.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_DistVocBand_HEADER_INCLUDED)
#define mfx_pi_vocod_DistVocBand_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace pi
{
namespace vocod
{



class FilterBank;

class DistVocBand
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               DistVocBand ()                         = default;
	               ~DistVocBand ()                        = default;
	               DistVocBand (const DistVocBand &other) = default;
	               DistVocBand (DistVocBand &&other)      = default;
	DistVocBand &  operator = (const DistVocBand &other)  = default;
	DistVocBand &  operator = (DistVocBand &&other)       = default;

	void           set_nbr_bands_per_oct (int nbr_bands) noexcept;
	void           set_amount (float amt) noexcept;
	void           process_block (float * spl_ptr_arr [], const FilterBank &fb, int nbr_spl) noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Minimum linear gain. The clipping level is at 1 / _gain_base
	static constexpr float  _gain_base     = 0.06125f;

	// Minimum distortion clipping level, after gain compensation
	static constexpr float  _gain_post_min = 1.0f;

	// Input clipping, after pre-gain.
	// 1 / sqrt (3), this is where x - x^3 is maximum (and its derivate is 0).
	static constexpr float  _clip_in       = 0.57735026918962576451f;

	// Log base 2 of the gain attenuation at the highest band
	static constexpr float  _top_atten_l2  = -2.f;

	class Gain
	{
	public:
		void           set_from_amount (float amt) noexcept;
		bool           is_distorting () const noexcept;

		float          _pre  = 0.01f;
		float          _post = 1.f / _pre; // Before gain clipping
	};

	Gain           _gain_beg;
	Gain           _gain_end;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const DistVocBand &other) const = delete;
	bool           operator != (const DistVocBand &other) const = delete;

}; // class DistVocBand



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/DistVocBand.hpp"



#endif   // mfx_pi_vocod_DistVocBand_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
