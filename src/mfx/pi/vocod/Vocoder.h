/*****************************************************************************

        Vocoder.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_Vocoder_HEADER_INCLUDED)
#define mfx_pi_vocod_Vocoder_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/VecAlign.h"
#include "fstb/util/NotificationFlag.h"
#include "fstb/util/NotificationFlagCascadeSingle.h"
#include "fstb/AllocAlign.h"
#include "hiir/coef/C2x.h"
#include "mfx/dsp/ctrl/VarBlock.h"
#include "mfx/dsp/iir/BiquadPackSimd.h"
#include "mfx/dsp/iir/Downsampler2xSimd.h"
#include "mfx/dsp/iir/Upsampler2xSimd.h"
#include "mfx/pi/cdsp/HalfRate.h"
#include "mfx/pi/vocod/DistVocBand.h"
#include "mfx/pi/vocod/EnvBank.h"
#include "mfx/pi/vocod/FilterBank.h"
#include "mfx/pi/vocod/VocoderDesc.h"
#include "mfx/pi/ParamProcSimple.h"
#include "mfx/pi/ParamStateSet.h"
#include "mfx/piapi/PluginInterface.h"

#include <array>
#include <tuple>
#include <vector>



namespace mfx
{
namespace pi
{
namespace vocod
{



class Vocoder final
:	public piapi::PluginInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       Vocoder (piapi::HostInterface &host);
	               ~Vocoder () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::piapi::PluginInterface
	State          do_get_state () const final;
	double         do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const final;
	int            do_reset (double sample_freq, int max_buf_len, int &latency) final;
	void           do_process_block (piapi::ProcInfo &proc) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef hiir::coef::C2x094 Coef;

	// Maximum number of buffers for a processing block, with all the filters,
	// bands, channels maxed out.
	static constexpr int _max_nbr_buf   = _max_nbr_chn * Cst::_max_nbr_groups;

	// Number of biquad stages for the sibilant filter
	static constexpr int _nbr_stages_sf = 4;

	typedef fstb::VecAlign <float, fstb_SIMD128_ALIGN> BufAlign;
	typedef std::array <float *, _max_nbr_buf> BufPtrArray;
	typedef std::array <BufAlign, _max_nbr_chn> SplBufArray;

	typedef dsp::iir::Downsampler2xSimd <Coef::X2::_nbr_coef> DSpl;
	typedef dsp::iir::Upsampler2xSimd <Coef::X2::_nbr_coef> USpl;

	typedef dsp::iir::BiquadPackSimd <
		fstb::DataAlign <true>,
		fstb::DataAlign <true>
	> SibilantFilter;

	class Channel
	{
	public:
		// Rate change for the music (carrier)
		cdsp::HalfRate <DSpl, USpl>
		               _rate_halver;

		// Downsampler for the voice (modulator)
		DSpl           _dspl_voice;

		// Storage for the sibilant part, extracted at the beginning
		BufAlign       _buf_sib;

		// Buffer for the input/output.
		// During the input stage: contains the voice, then its envelope
		// During the output stage: fully processed signal
		// Half rate
		BufAlign       _buf_ds_io;

		// Buffer for the side channel (music, carrier)
		// Half rate
		BufAlign       _buf_ds_sc;

		// Temporary buffer for the spread data
		// Half rate
		BufAlign       _buf_spread;
	};
	typedef fstb::VecAlign <Channel> ChannelArray;

	void           clear_buffers () noexcept;
	void           update_param (bool force_flag = false) noexcept;
	void           update_param_bank () noexcept;
	void           update_param_env () noexcept;
	void           update_param_proc () noexcept;

	void           set_nbr_chn (int nbr_chn) noexcept;
	void           update_buffers ();
	void           update_buffer_pointers (BufPtrArray &buf_ptr_arr, BufAlign &buf_zone) noexcept;
	void           config_sibilant_filter () noexcept;

	int            process_block_downsample (const float * const music_ptr_arr [], const float * const voice_ptr_arr [], int nbr_spl) noexcept;
	void           process_block_upsample (float * const dst_ptr_arr []) noexcept;

	void           extract_sibilant (const float * const voice_ptr_arr [], int nbr_spl) noexcept;
	void           merge_sibilant (float * const dst_ptr_arr [], int nbr_spl) noexcept;

	void           compress_block (int nbr_spl) noexcept;

	void           combine_bands_envelopes (int nbr_spl) noexcept;
	void           process_mix_pan (float * const dst_ptr_arr [], int nbr_spl) noexcept;
	void           process_mix_pan_part_mono (int nbr_spl) noexcept;
	void           spread_bands_to_chn (int nbr_spl) noexcept;
	void           process_mix_pan_part_direct (int nbr_spl) noexcept;
	void           mix_voice_signal (float * const dst_ptr_arr [], int nbr_spl) noexcept;

	static inline std::tuple <int, int>
	               find_band (int band_idx) noexcept;

	piapi::HostInterface &
	               _host;
	State          _state = State_CREATED;

	VocoderDesc    _desc;
	ParamStateSet  _state_set;
	ParamProcSimple
	               _param_proc { _state_set };
	float          _sample_freq = 0;    // Hz, > 0. <= 0: not initialized
	float          _inv_fs      = 0;    // 1 / _sample_freq
	int            _max_buf_len = 0;    // Maximum number of samples per buffer. 0 = not set.

	fstb::util::NotificationFlag
	               _param_change_flag;
	fstb::util::NotificationFlagCascadeSingle
	               _param_change_flag_bank;
	fstb::util::NotificationFlagCascadeSingle
	               _param_change_flag_env;
	fstb::util::NotificationFlagCascadeSingle
	               _param_change_flag_proc;

	ChannelArray   _chn_arr = ChannelArray (_max_nbr_chn);
	int            _nbr_chn = 0; // Current number of processing channels. 0 = not initialised
	int            _buf_len_band = 0; // Buffer size for a band in sample, possibly half rate, for a full group. 0 = not initialised

	// Indicates that the effect works at half-rate, saving CPU cycles but
	// losing the top of the spectrum @ Fs < 80 kHz.
	bool           _halfrate_flag = true;

	FilterBank     _fb_music;
	FilterBank     _fb_voice;

	EnvBank        _env_voice;
	EnvBank        _env_music;

	// 1 buffer = packed data, half rate, for a group of 4 bands. Repeat for
	// all active groups, then for all active channels.
	// So the loop order is, from inner to outer:
	// - bands from 1 group
	// - sample index in the block
	// - groups of 1 channel
	// - all channels
	BufAlign       _buf_band_music;
	BufAlign       _buf_band_voice;
	BufAlign       _buf_band_mix;

	// Buffers for the envelopes
	BufAlign       _buf_env_mus;
	BufAlign       _buf_env_voc;

	BufPtrArray    _buf_ptr_arr_music = {};
	BufPtrArray    _buf_ptr_arr_voice = {};
	BufPtrArray    _buf_ptr_arr_mix   = {};
	BufPtrArray    _buf_ptr_arr_sib   = {};

	// For 2x downsampled.
	BufPtrArray    _buf_ptr_arr_ds_io = {};
	BufPtrArray    _buf_ptr_arr_ds_sc = {};

	BufPtrArray    _buf_ptr_arr_env_voc = {};
	BufPtrArray    _buf_ptr_arr_env_mus = {};

	DistVocBand    _distortion;
	SibilantFilter _sibilant_filter;

	dsp::ctrl::VarBlock
	               _sibilant_lvl { 0 };
	dsp::ctrl::VarBlock
	               _pan_amt      { 0 };
	dsp::ctrl::VarBlock                 // Octaves
	               _transpose    { 0 };
	dsp::ctrl::VarBlock
	               _comp         { 0 };
	dsp::ctrl::VarBlock                 // Threshold, log2 scale
	               _thru         { Cst::_thru_thr_off };

	// Coefficients for the sibilant filter. Computed once.
	static std::array <float, _nbr_stages_sf>
	               _sf_a1_coef_arr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               Vocoder ()                               = delete;
	               Vocoder (const Vocoder &other)           = delete;
	               Vocoder (Vocoder &&other)                = delete;
	Vocoder &      operator = (const Vocoder &other)        = delete;
	Vocoder &      operator = (Vocoder &&other)             = delete;
	bool           operator == (const Vocoder &other) const = delete;
	bool           operator != (const Vocoder &other) const = delete;

}; // class Vocoder



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/Vocoder.hpp"



#endif   // mfx_pi_vocod_Vocoder_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
