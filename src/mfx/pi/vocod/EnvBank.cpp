/*****************************************************************************

        EnvBank.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/vocod/EnvBank.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace vocod
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



EnvBank::EnvBank ()
{
	update_sample_freq ();
}



void	EnvBank::set_sample_freq (double sample_freq) noexcept
{
	assert (sample_freq > 0);

	_sample_freq = sample_freq;
	update_sample_freq ();
}



void	EnvBank::set_nbr_chn (int nbr_chn) noexcept
{
	assert (nbr_chn > 0);

	const auto        nbr_chn_old = _nbr_chn;
	_nbr_chn    = nbr_chn;

	// Cleans up new channels
	for (int chn_idx = nbr_chn_old; chn_idx < _nbr_chn; ++chn_idx)
	{
		EnvChn &       chn = _env_chn_arr [chn_idx];
		for (auto &group : chn)
		{
			group.clear_buffers ();
		}
	}

	update_sample_freq ();
	_dirty_flag = true;
}



void	EnvBank::set_nbr_groups (int nbr_groups) noexcept
{
	assert (nbr_groups > 0);

	_nbr_groups = nbr_groups;
	_dirty_flag = true;
}



void	EnvBank::set_atk_time (double at) noexcept
{
	assert (at >= 0);

	_atk_time   = at;
	_dirty_flag = true;
}



void	EnvBank::set_rls_time (double rt) noexcept
{
	assert (rt >= 0);

	_rls_time   = rt;
	_dirty_flag = true;
}



void	EnvBank::clear_buffers () noexcept
{
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		EnvChn &       env_chn = _env_chn_arr [chn];
		for (auto &group : env_chn)
		{
			group.clear_buffers ();
		}
	}
}



// 1 sample = block of 4 voices
void	EnvBank::process_block (float * const dst_ptr_arr [], const float * const src_ptr_arr [], int nbr_spl) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (dst_ptr_arr [0] != nullptr);
	assert (dst_ptr_arr [_nbr_chn * _nbr_groups - 1] != nullptr);
	assert (src_ptr_arr != nullptr);
	assert (src_ptr_arr [0] != nullptr);
	assert (src_ptr_arr [_nbr_chn * _nbr_groups - 1] != nullptr);
	assert (nbr_spl > 0);

	if (_dirty_flag)
	{
		update_env_param ();
	}

	int            buf_index = 0;
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		EnvChn &       env_chn = _env_chn_arr [chn];
		for (int group_idx = 0; group_idx < _nbr_groups; ++group_idx)
		{
			EnvGroup &		group   = env_chn [group_idx];
			auto           src_ptr = fstb::Vf32::ptr_v (src_ptr_arr [buf_index]);
			auto           dst_ptr = fstb::Vf32::ptr_v (dst_ptr_arr [buf_index]);
			group.process_block (dst_ptr, src_ptr, nbr_spl);

			++ buf_index;
		}
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	EnvBank::update_sample_freq () noexcept
{
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		EnvChn &       env_chn = _env_chn_arr [chn];
		for (auto &group : env_chn)
		{
			group.set_sample_freq (_sample_freq);
		}
	}
}



void	EnvBank::update_env_param () noexcept
{
	for (int chn = 0; chn < _nbr_chn; ++chn)
	{
		EnvChn &       env_chn = _env_chn_arr [chn];
		for (int group_idx = 0; group_idx < _nbr_groups; ++group_idx)
		{
			EnvGroup &     group = env_chn [group_idx];
			/*** To do: compute the coefficients once and copy them ***/
			group.set_times_same (float (_atk_time), float (_rls_time));
		}
	}

	_dirty_flag = false;
}



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
