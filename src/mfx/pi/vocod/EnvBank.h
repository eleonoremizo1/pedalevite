/*****************************************************************************

        EnvBank.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_vocod_EnvBank_HEADER_INCLUDED)
#define mfx_pi_vocod_EnvBank_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/DataAlign.h"
#include "fstb/VecAlign.h"
#include "mfx/dsp/dyn/EnvFollowerRms4Simd.h"
#include "mfx/pi/vocod/Cst.h"
#include "mfx/piapi/PluginInterface.h"

#include <array>



namespace mfx
{
namespace pi
{
namespace vocod
{



class EnvBank
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               EnvBank ();
	               ~EnvBank ()                       = default;
	               EnvBank (const EnvBank &other)    = default;
	               EnvBank (EnvBank &&other)         = default;
	EnvBank &      operator = (const EnvBank &other) = default;
	EnvBank &      operator = (EnvBank &&other)      = default;

	void				set_sample_freq (double sample_freq) noexcept;
	void				set_nbr_chn (int nbr_chn) noexcept;

	void				set_nbr_groups (int nbr_groups) noexcept;
	void				set_atk_time (double at) noexcept;
	void				set_rls_time (double rt) noexcept;

	void				clear_buffers () noexcept;
	void				process_block (float * const dst_ptr_arr [], const float * const src_ptr_arr [], int nbr_spl) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:




/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef dsp::dyn::EnvFollowerRms4Simd <
		fstb::DataAlign <true>,
		fstb::DataAlign <true>,
		fstb::DataAlign <true>,
		1
	> EnvGroup;
	typedef std::array <EnvGroup, Cst::_max_nbr_groups> EnvChn;
	typedef fstb::VecAlign <EnvChn, fstb_SIMD128_ALIGN> EnvChnArray;

	void				update_sample_freq () noexcept;
	void				update_env_param () noexcept;

	EnvChnArray		_env_chn_arr = EnvChnArray (piapi::PluginInterface::_max_nbr_chn);
	double			_sample_freq       = 44100; // Hz, > 0
	double			_atk_time          = 0.001; // s
	double			_rls_time          = 0.100; // s
	int				_nbr_chn           = 0; // > 0
	int				_nbr_groups        = 0; // > 0
	bool				_dirty_flag        = true;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const EnvBank &other) const = delete;
	bool           operator != (const EnvBank &other) const = delete;

}; // class EnvBank



}  // namespace vocod
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/vocod/EnvBank.hpp"



#endif   // mfx_pi_vocod_EnvBank_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
