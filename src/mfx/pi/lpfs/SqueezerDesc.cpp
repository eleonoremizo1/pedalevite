/*****************************************************************************

        SqueezerDesc.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/lpfs/Param.h"
#include "mfx/pi/lpfs/SqueezerDesc.h"
#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplLin.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace lpfs
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



SqueezerDesc::SqueezerDesc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	_info._unique_id = "lpfs";
	_info._name      = "Filter Squeezer\nSqueezer";
	_info._tag_list  = { piapi::Tag::_eq_filter_0 };
	_info._chn_pref  = piapi::ChnPref::NONE;

	// Cutoff Frequency
	auto           log_uptr = std::make_unique <param::TplLog> (
		20, 20 * 1024,
		"F\nFreq\nFrequency\nCutoff Frequency",
		"Hz",
		param::HelperDispNum::Preset_FLOAT_STD,
		0,
		"%5.0f"
	);
	log_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
	log_uptr->set_flags (piapi::ParamDescInterface::Flags_AUTOLINK);
	_desc_set.add_glob (Param_FREQ, log_uptr);

	// Resonance
	auto           lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"R\nReso\nResonance",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_RESO, lin_uptr);

	// Color
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"C\nColor",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_COLOR, lin_uptr);

	// Drive
	log_uptr = std::make_unique <param::TplLog> (
		1.0 / 8, 32,
		"D\nDrive",
		"dB",
		param::HelperDispNum::Preset_DB,
		0,
		"%+5.1f"
	);
	_desc_set.add_glob (Param_DRIVE, log_uptr);

	// Type
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		"Hard\nSoft\nNone",
		"F\nFold\nFoldback",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_TYPE, enu_uptr);
}



ParamDescSet &	SqueezerDesc::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInfo	SqueezerDesc::do_get_info () const
{
	return _info;
}



void	SqueezerDesc::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	nbr_i = 1;
	nbr_o = 1;
	nbr_s = 0;
}



int	SqueezerDesc::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



const piapi::ParamDescInterface &	SqueezerDesc::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace lpfs
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
