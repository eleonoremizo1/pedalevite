/*****************************************************************************

        DistSmooth.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_distsm_DistSmooth_HEADER_INCLUDED)
#define mfx_pi_distsm_DistSmooth_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/util/NotificationFlag.h"
#include "fstb/util/NotificationFlagCascadeSingle.h"
#include "fstb/VecAlign.h"
#include "mfx/pi/distsm/DistSmoothDesc.h"
#include "mfx/pi/distsm/SplitDist.h"
#include "mfx/pi/ParamProcSimple.h"
#include "mfx/pi/ParamStateSet.h"
#include "mfx/piapi/PluginInterface.h"

#include <array>
#include <vector>



namespace mfx
{
namespace pi
{
namespace distsm
{



class DistSmooth final
:	public piapi::PluginInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       DistSmooth (piapi::HostInterface &host);
	               ~DistSmooth () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::piapi::PluginInterface
	State          do_get_state () const final;
	double         do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const final;
	int            do_reset (double sample_freq, int max_buf_len, int &latency) final;
	void           do_process_block (piapi::ProcInfo &proc) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static_assert (
		_max_nbr_chn <= SplitDist::_max_nbr_chn,
		"Maximum number of channel mismatch"
	);

	typedef fstb::VecAlign <float, 16> BufAlign;

	void           clear_buffers ();
	void           update_param (bool force_flag = false);

	piapi::HostInterface &
	               _host;
	State          _state = State_CREATED;

	DistSmoothDesc _desc;
	ParamStateSet  _state_set;
	ParamProcSimple
	               _param_proc { _state_set };
	float          _sample_freq = 0;    // Hz, > 0. <= 0: not initialized
	float          _inv_fs      = 0;    // 1 / _sample_freq

	fstb::util::NotificationFlag
	               _param_change_flag;
	fstb::util::NotificationFlagCascadeSingle
	               _param_change_flag_comb;
	fstb::util::NotificationFlagCascadeSingle
	               _param_change_flag_misc;

	SplitDist      _splitter;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DistSmooth ()                               = delete;
	               DistSmooth (const DistSmooth &other)        = delete;
	               DistSmooth (DistSmooth &&other)             = delete;
	DistSmooth &   operator = (const DistSmooth &other)        = delete;
	DistSmooth &   operator = (DistSmooth &&other)             = delete;
	bool           operator == (const DistSmooth &other) const = delete;
	bool           operator != (const DistSmooth &other) const = delete;

}; // class DistSmooth



}  // namespace distsm
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/distsm/DistSmooth.hpp"



#endif   // mfx_pi_distsm_DistSmooth_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
