/*****************************************************************************

        SplitDist.h
        Author: Laurent de Soras, 2024

Sebastian Laguerre, Gary P. Scavone,
Simulating A Hexaphonic Pickup Using Parallel Comb Filters for Guitar Distortion,
Proceedings of the 23rd International Conference on Digital Audio Effects,
pp. 105-112, 2021

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_distsm_SplitDist_HEADER_INCLUDED)
#define mfx_pi_distsm_SplitDist_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/DataAlign.h"
#include "fstb/def.h"
#include "fstb/VecAlign.h"
#include "mfx/dsp/ctrl/VarBlock.h"
#include "mfx/dsp/dyn/EnvFollowerRmsSimple.h"
#include "mfx/dsp/iir/DcKiller2p.h"
#include "mfx/pi/cdsp/CombMulti.h"
#include "mfx/pi/cdsp/OvrsplMulti.h"

#include <array>



namespace mfx
{
namespace pi
{
namespace distsm
{



class SplitDist
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _max_nbr_chn = 2;

	void           reset (double sample_freq, int max_blk_size, int &latency);

	void           set_q (float q) noexcept;
	void           set_gain (float g) noexcept;
	void           set_blend (float b) noexcept;

	void           process_block (float * const * dst_ptr_arr, const float * const * src_ptr_arr, int nbr_spl, int nbr_chn) noexcept;
	void           clear_buffers () noexcept;
	void           clear_param_ramps () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int   _nbr_combs    = 12; // Total number of filters
	static constexpr float _comb_per_oct = 12; // Filter spacing (per octave)
	static constexpr bool  _constant_q   = false; // Constant Q or bandwidth
	static constexpr float _q_min        = 2;
	static constexpr int   _ovrspl_l2    = 2;  // Log2 of the oversampling rate

	typedef cdsp::CombMulti <float, 6> Splitter;
	typedef fstb::VecAlign <float, fstb_SIMD128_ALIGN> BufAlign;

	class Channel
	{
	public:
		dsp::iir::DcKiller2p // Standard rate
		               _dc_kill;

		cdsp::OvrsplMulti
		               _ovrspl;
		Splitter       _splitter; // Oversampled

		// Distorted input (for volume measurement purpose)
		BufAlign       _buf_mono; // Oversampled

		// 1. Accumulates distorted comb filter outputs
		// 2. Blend output
		BufAlign       _buf_sum;  // Oversampled
	};
	typedef std::array <Channel, _max_nbr_chn> ChannelArray;

	class FilterParam
	{
	public:
		float          compute_mag (float frel) const noexcept;
		float          _delay = 0; // M
		float          _b0    = 0; // = bM
		float          _am    = 0;
	};

	typedef dsp::dyn::EnvFollowerRmsSimple Follower;

	typedef std::array <const float *, _max_nbr_chn> BufEnvPtrArray;

	void           reset_filters ();
	void           configure_filters () noexcept;
	float          compute_f0_base () const noexcept;
	FilterParam    compute_filter_param (float f0, float q) noexcept;
	void           distort_block (BufAlign &dst, const BufAlign &src, int nbr_spl) noexcept;

	static inline int
	               align_simd_len_up (int len) noexcept;
	static inline int
	               align_simd_len_down (int len) noexcept;

	// Sampling rate, Hz, > 0. 0 = invalid
	float          _sample_freq = 0;
	float          _fs_ovr      = 0; // Oversampled

	ChannelArray   _chn_arr;

	// MIDI pitch of the lowest filter.
	float          _base_pitch = 40; // E at ~82 Hz

	// Filter selectivity (for the lowest frequency if constant bandwidth)
	// Should be >= 2 for any filter.
	float          _filter_q   = 20;

	dsp::ctrl::VarBlock
	               _gain { 1 };
	dsp::ctrl::VarBlock
	               _blend { 1 };

	// Input signal with gain applied and DC killed.
	BufAlign       _buf_gain; // Standard rate

	// Oversampled input (equiv to _buf_gain)
	BufAlign       _buf_ovr;  // Oversampled

	// Temporary for:
	// 1. Comb distortion
	// 2. Comb envelope
	BufAlign       _buf_comb; // Oversampled

	// Temporary for reference envelope
	BufAlign       _buf_ref;  // Oversampled

	Follower       _env_ref;
	Follower       _env_comb;

	BufEnvPtrArray _env_ref_ptr_arr;  // Points on each _buf_mono
	BufEnvPtrArray _env_comb_ptr_arr; // Points on each _buf_sum



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const SplitDist &other) const = delete;
	bool           operator != (const SplitDist &other) const = delete;

}; // class SplitDist



}  // namespace distsm
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/distsm/SplitDist.hpp"



#endif // mfx_pi_distsm_SplitDist_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
