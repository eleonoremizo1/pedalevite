/*****************************************************************************

        SplitDist.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Approx.h"
#include "fstb/DataAlign.h"
#include "fstb/def.h"
#include "fstb/Vf32.h"
#include "mfx/dsp/dyn/EnvHelper.h"
#include "mfx/dsp/dyn/SCPower.h"
#include "mfx/dsp/mix/Align.h"
#include "mfx/pi/distsm/SplitDist.h"

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace distsm
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	SplitDist::reset (double sample_freq, int max_blk_size, int &latency)
{
	assert (sample_freq > 0);

	dsp::mix::Align::setup ();

	const auto     fs_ovr = sample_freq * (1 << _ovrspl_l2);

	_sample_freq = float (sample_freq);
	_fs_ovr      = float (fs_ovr);

	const auto     mbs_align = align_simd_len_up (max_blk_size);
	_buf_gain.resize (mbs_align);

	const auto     mbs_ovr_align =
		align_simd_len_up (max_blk_size << _ovrspl_l2);
	_buf_ovr.resize (mbs_ovr_align);
	_buf_comb.resize (mbs_ovr_align);
	_buf_ref.resize (mbs_ovr_align);

	for (int chn_idx = 0; chn_idx < _max_nbr_chn; ++chn_idx)
	{
		auto &         chn = _chn_arr [chn_idx];
		chn._ovrspl.cap_ratio_l2 (_ovrspl_l2);
		chn._ovrspl.set_ratio_l2 (_ovrspl_l2);
		chn._dc_kill.set_sample_freq (sample_freq);
		chn._dc_kill.set_cutoff_freq (5);
		chn._splitter.set_nbr_combs (_nbr_combs);

		chn._buf_mono.resize (mbs_ovr_align);
		chn._buf_sum.resize (mbs_ovr_align);
		_env_ref_ptr_arr [chn_idx]  = chn._buf_mono.data ();
		_env_comb_ptr_arr [chn_idx] = chn._buf_sum.data ();
	}

	_env_ref.set_time (0.020f);
	_env_comb.set_time (0.020f);

	reset_filters ();
	configure_filters ();

	latency += _chn_arr [0]._ovrspl.get_latency ();
}



void	SplitDist::set_q (float q) noexcept
{
	assert (q >= _q_min);

	_filter_q = q;
	configure_filters ();
}



void	SplitDist::set_gain (float g) noexcept
{
	_gain.set_val (g);
}



void	SplitDist::set_blend (float b) noexcept
{
	assert (b >= 0);
	assert (b <= 1);

	_blend.set_val (b);
}



// src_ptr_arr [i] should be 16-byte aligned
void	SplitDist::process_block (float * const * dst_ptr_arr, const float * const * src_ptr_arr, int nbr_spl, int nbr_chn) noexcept
{
	assert (dst_ptr_arr != nullptr);
	assert (src_ptr_arr != nullptr);
	assert (nbr_spl > 0);
	assert (nbr_chn > 0);
	assert (nbr_chn <= _max_nbr_chn);

	const int      nbr_spl_ovr = nbr_spl << _ovrspl_l2;

	_gain.tick (nbr_spl);
	_blend.tick (nbr_spl);

	const auto     gain_beg = _gain.get_beg ();
	const auto     gain_end = _gain.get_end ();

	// Distortion
	for (int chn_idx = 0; chn_idx < nbr_chn; ++chn_idx)
	{
		auto &         chn     = _chn_arr [chn_idx];
		const float *  src_ptr = src_ptr_arr [chn_idx];
		assert (fstb::DataAlign <true>::check_ptr (src_ptr));

		// Gain and DC killing
		dsp::mix::Align::copy_1_1_vlrauto (
			_buf_gain.data (), src_ptr, nbr_spl, gain_beg, gain_end
		);
		chn._dc_kill.process_block (
			_buf_gain.data (), _buf_gain.data (), nbr_spl
		);

		// Oversamping
		chn._ovrspl.upsample_block (
			_buf_ovr.data (), _buf_gain.data (), nbr_spl
		);

		// Reference distortion (mono)
		distort_block (chn._buf_mono, _buf_ovr, nbr_spl_ovr);

		// Comb distortion
		auto           fnc_mix = dsp::mix::Align::copy_1_1;
		for (int comb_idx = 0; comb_idx < _nbr_combs; ++comb_idx)
		{
			chn._splitter.process_block_single (
				comb_idx, _buf_comb.data (), _buf_ovr.data (), nbr_spl_ovr
			);
			distort_block (_buf_comb, _buf_comb, nbr_spl_ovr);

			fnc_mix (chn._buf_sum.data (), _buf_comb.data (), nbr_spl_ovr);
			fnc_mix = dsp::mix::Align::mix_1_1;
		}
	}

	// Volume analysis
	dsp::dyn::SCPower <> pwr;

	pwr.prepare_env_input (
		_buf_ref.data (), _env_ref_ptr_arr.data (), nbr_chn, 0, nbr_spl_ovr
	);
	_env_ref.process_block_raw (
		_buf_ref.data (), _buf_ref.data (), nbr_spl_ovr
	);

	pwr.prepare_env_input (
		_buf_comb.data (), _env_comb_ptr_arr.data (), nbr_chn, 0, nbr_spl_ovr
	);
	_env_comb.process_block_raw (
		_buf_comb.data (), _buf_comb.data (), nbr_spl_ovr
	);

	const auto     blend_beg = _blend.get_beg ();
	const auto     blend_end = _blend.get_end ();
	for (int chn_idx = 0; chn_idx < nbr_chn; ++chn_idx)
	{
		auto &         chn = _chn_arr [chn_idx];
		float * fstb_RESTRICT   spl_ptr = chn._buf_sum.data ();

		// Gain correction
		int            v_end = 0;

		constexpr float   epsilon = 1e-10f;

#if defined (fstb_HAS_SIMD)

		const auto     eps_v = fstb::Vf32 (epsilon);
		v_end = align_simd_len_down (nbr_spl_ovr);
		for (int pos = 0; pos < v_end; pos += fstb::Vf32::_length)
		{
			auto           x     = fstb::Vf32::load (spl_ptr           + pos);
			const auto     ref2  = fstb::Vf32::load (_buf_ref.data ()  + pos);
			const auto     comb2 = fstb::Vf32::load (_buf_comb.data () + pos);
			const auto     den   = max (comb2 + eps_v, ref2);
			const auto     gain  = sqrt (ref2) * den.rsqrt ();
			x *= gain;
			x.store (spl_ptr + pos);
		}

#endif

		for (int pos = v_end; pos < nbr_spl_ovr; ++pos)
		{
			// gain = sqrt (ref^2 / max (comb^2, ref^2))
			const auto     ref2  = _buf_ref [pos];
			const auto     comb2 = _buf_comb [pos];
			const auto     den   = std::max (comb2 + epsilon, ref2);
			const auto     gain  = sqrtf (ref2 / den);

			spl_ptr [pos] *= gain;
		}

		// Blends between the mono distortion and the comb distortion sum
		constexpr auto    thr = 1 - 1e-3f;
		if (blend_beg < thr || blend_end < thr)
		{
			const float *     ref_ptr = _chn_arr [chn_idx]._buf_mono.data ();
			dsp::mix::Align::copy_xfade_2_1_vlrauto (
				spl_ptr, ref_ptr, spl_ptr, nbr_spl_ovr, blend_beg, blend_end
			);
		}

		// Downsampling
		float *        dst_ptr = dst_ptr_arr [chn_idx];
		assert (dst_ptr != nullptr);
		chn._ovrspl.downsample_block (dst_ptr, spl_ptr, nbr_spl);
	}
}



void	SplitDist::clear_buffers () noexcept
{
	clear_param_ramps ();
	for (auto &chn : _chn_arr)
	{
		chn._ovrspl.clear_buffers ();
		chn._splitter.clear_buffers ();
	}
	_env_ref.clear_buffers ();
	_env_comb.clear_buffers ();
}



void	SplitDist::clear_param_ramps () noexcept
{
	_gain.clear_buffers ();
	_blend.clear_buffers ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Returns the comb filter magnitude at frel = f0 / Fs.
//    z     = exp (-j*w)
//  H(z)    = (b0 + bM * z^-M) / (1 - aM * z^-M)
// |H(z)|^2 =   ((b0 + bM * cos (-M*w))^2 + (bM * sin (-M*w))^2)
//            / ((1  - aM * cos (-M*w))^2 + (aM * sin (-M*w))^2)
/*** To do: not used. Move this elsewhere, in the UniComb class for example ***/
float	SplitDist::FilterParam::compute_mag (float frel) const noexcept
{
	assert (frel >= 0);
	assert (frel < 0.5f);

	const auto     w   = frel * float (fstb::PI * 2);

	const auto     a   = -_delay * w;
	const auto     c   = cosf (a);
	const auto     s   = sinf (a);

	const auto     bm  = _b0;
	const auto     nr  = _b0 +  bm * c;
	const auto     ni  =        bm * s;
	const auto     dr  = 1   - _am * c;
	const auto     di  =       _am * s;

	const auto     sq  = (nr * nr + ni * ni) / (dr * dr + di * di);
	const auto     mag = sqrtf (sq);

	return mag;
}



void	SplitDist::reset_filters ()
{
	const auto       f0_base   = compute_f0_base ();

	// Arbitrary scaling. We'll see later if we need to take account some
	// margin for detuning.
	const auto       delay_max = 1.25f * _fs_ovr / f0_base;
	for (auto &chn : _chn_arr)
	{
		for (int comb_idx = 0; comb_idx < _nbr_combs; ++comb_idx)
		{
			chn._splitter.set_delay_max (comb_idx, delay_max);
		}
	}
}



void	SplitDist::configure_filters () noexcept
{
	const auto       f0_base = compute_f0_base ();
	const auto       ratio   = exp2f (1.f / _comb_per_oct);

	float            f0  = f0_base;
	float            q   = _filter_q;
	for (int comb_idx = 0; comb_idx < _nbr_combs; ++comb_idx)
	{
		const auto      fp = compute_filter_param (f0, q);
		for (auto &chn : _chn_arr)
		{
			chn._splitter.set_delay (comb_idx, fp._delay);
			chn._splitter.set_coefs (comb_idx, fp._b0, fp._b0, fp._am);
		}

		// Next filter
		f0 *= ratio;
		if (! _constant_q)
		{
			q *= ratio;
		}
	}
}



float	SplitDist::compute_f0_base () const noexcept
{
	const auto       f0_base = 440 * exp2f (float (_base_pitch - 69) / 12);

	return f0_base;
}



SplitDist::FilterParam	SplitDist::compute_filter_param (float f0, float q) noexcept
{
	assert (f0 > 0);
	assert (f0 < _fs_ovr * 0.5f);
	assert (q >= _q_min);

	// Eq. (9)
	const auto       m    = _fs_ovr / f0;

	// Eq. (14)
	const auto       beta = tanf (float (fstb::PI) / (2 * q));

	// Eq. (10)
	const auto       b0   = beta / (1 + beta);
	const auto       am   = 1 - 2 * b0; // = (1 - beta) / (1 + beta)
	assert (am >= 0);

	return { m, b0, am };
}



// Soft clipping in [-a ; a]: f(x) = a * x / sqrt (a^2 + x^2)
void	SplitDist::distort_block (BufAlign &dst, const BufAlign &src, int nbr_spl) noexcept
{
	assert (nbr_spl > 0);
	assert (nbr_spl <= int (dst.size ()));
	assert (nbr_spl <= int (src.size ()));

	const float * fstb_RESTRICT   src_ptr = src.data ();
	float * fstb_RESTRICT         dst_ptr = dst.data ();

	constexpr float   scale = 0.125f;

#if defined (fstb_HAS_SIMD)

	const auto     a  = fstb::Vf32 (scale);
	const auto     a2 = fstb::Vf32 (scale * scale);
	for (int pos = 0; pos < nbr_spl; pos += fstb::Vf32::_length)
	{
		const auto        x    = fstb::Vf32::load (src_ptr + pos);
		const auto        x2   = x * x;
		const auto        num  = x * a;
		const auto        rden = (a2 + x2).rsqrt_approx ();
		const auto        y    = num * rden;
		y.store (dst_ptr + pos);
	}

#else // Reference implementation

	const auto     a2 = scale * scale;
	for (int pos = 0; pos < nbr_spl; ++ pos)
	{
		const auto        x    = src_ptr [pos];
		const auto        x2   = x * x;
		const auto        num  = x * scale;
		const auto        rden = fstb::Approx::rsqrt <1> (a2 + x2);
		const auto        y    = num * rden;
		dst_ptr [pos] = y;
	}

#endif
}



int	SplitDist::align_simd_len_up (int len) noexcept
{
	assert (len >= 0);

	const auto       vlm1  = fstb::Vf32::_length - 1;
	const auto       len_a = (len + vlm1) & ~vlm1;

	return len_a;
}



int	SplitDist::align_simd_len_down (int len) noexcept
{
	assert (len >= 0);

	const auto       vlm1  = fstb::Vf32::_length - 1;
	const auto       len_a = len & ~vlm1;

	return len_a;
}



}  // namespace distsm
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
