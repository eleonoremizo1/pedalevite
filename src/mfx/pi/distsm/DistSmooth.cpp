/*****************************************************************************

        DistSmooth.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/dsp/mix/Align.h"
#include "mfx/pi/distsm/Param.h"
#include "mfx/pi/distsm/DistSmooth.h"
#include "mfx/piapi/Dir.h"
#include "mfx/piapi/Err.h"
#include "mfx/piapi/EventParam.h"
#include "mfx/piapi/EventTs.h"
#include "mfx/piapi/EventType.h"
#include "mfx/piapi/ProcInfo.h"

#include <algorithm>

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace distsm
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DistSmooth::DistSmooth (piapi::HostInterface &host)
:	_host (host)
{
	dsp::mix::Align::setup ();

	const ParamDescSet & desc_set = _desc.use_desc_set ();
	_state_set.init (piapi::ParamCateg_GLOBAL, desc_set);

	_state_set.set_val_nat (desc_set, Param_GAIN ,  1);
	_state_set.set_val_nat (desc_set, Param_Q    , 10);
	_state_set.set_val_nat (desc_set, Param_BLEND,  1);

	_state_set.add_observer (Param_GAIN , _param_change_flag_misc);
	_state_set.add_observer (Param_Q    , _param_change_flag_comb);
	_state_set.add_observer (Param_BLEND, _param_change_flag_misc);

	_param_change_flag_misc.add_observer (_param_change_flag);
	_param_change_flag_comb.add_observer (_param_change_flag);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInterface::State	DistSmooth::do_get_state () const
{
	return _state;
}



double	DistSmooth::do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const
{
	fstb::unused (categ, note_id);
	assert (categ == piapi::ParamCateg_GLOBAL);

	return _state_set.use_state (index).get_val_tgt ();
}



int	DistSmooth::do_reset (double sample_freq, int max_buf_len, int &latency)
{
	_sample_freq = float (    sample_freq);
	_inv_fs      = float (1 / sample_freq);

	_state_set.set_sample_freq (sample_freq);
	_state_set.clear_buffers ();

	_splitter.reset (sample_freq, max_buf_len, latency);

	_param_change_flag_comb.set ();
	_param_change_flag_misc.set ();

	update_param (true);
	_param_proc.req_steady ();

	clear_buffers ();

	_state = State_ACTIVE;

	return piapi::Err_OK;
}



void	DistSmooth::do_process_block (piapi::ProcInfo &proc)
{
	const int      nbr_chn_src = proc._dir_arr [piapi::Dir_IN ]._nbr_chn;
	const int      nbr_chn_dst = proc._dir_arr [piapi::Dir_OUT]._nbr_chn;
	assert (nbr_chn_src <= nbr_chn_dst);
	const int      nbr_chn_proc = std::min (nbr_chn_src, nbr_chn_dst);

	// Events
	_param_proc.handle_msg (proc);

	// Parameters
	_state_set.process_block (proc._nbr_spl);
	update_param ();
	if (_param_proc.is_full_reset ())
	{
		clear_buffers ();
	}
	else if (_param_proc.is_req_steady_state ())
	{
		_splitter.clear_param_ramps ();
	}

	// Signal processing
	_splitter.process_block (
		proc._dst_arr, proc._src_arr, proc._nbr_spl, nbr_chn_proc
	);

	// Duplicates the remaining output channels
	for (int chn_idx = nbr_chn_proc; chn_idx < nbr_chn_dst; ++chn_idx)
	{
		dsp::mix::Align::copy_1_1 (
			proc._dst_arr [chn_idx],
			proc._dst_arr [0],
			proc._nbr_spl
		);
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DistSmooth::clear_buffers ()
{
	_splitter.clear_buffers ();
}



void	DistSmooth::update_param (bool force_flag)
{
	if (_param_change_flag (true) || force_flag)
	{
		if (_param_change_flag_comb (true) || force_flag)
		{
			const auto     q = float (_state_set.get_val_end_nat (Param_Q));
			_splitter.set_q (q);
		}

		if (_param_change_flag_misc (true) || force_flag)
		{
			const auto     gain =
				float (_state_set.get_val_end_nat (Param_GAIN));
			_splitter.set_gain (gain);

			const auto     blend =
				float (_state_set.get_val_end_nat (Param_BLEND));
			_splitter.set_blend (blend);
		}
	}
}



}  // namespace distsm
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
