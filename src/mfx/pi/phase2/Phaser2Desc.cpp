/*****************************************************************************

        Phaser2Desc.cpp
        Author: Laurent de Soras, 2017

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/param/HelperDispNum.h"
#include "mfx/pi/param/MapPiecewiseLinLog.h"
#include "mfx/pi/param/MapS.h"
#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplInt.h"
#include "mfx/pi/param/TplLin.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/pi/param/TplMapped.h"
#include "mfx/pi/phase2/LfoType.h"
#include "mfx/pi/phase2/Param.h"
#include "mfx/pi/phase2/Phaser2Desc.h"
#include "mfx/pi/ParamMapFdbkBipolar.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace phase2
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



Phaser2Desc::Phaser2Desc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	_info._unique_id = "phaser2";
	_info._name      = "Phaser AP";
	_info._tag_list  = { piapi::Tag::_modulation_0 };
	_info._chn_pref  = piapi::ChnPref::STEREO;

	typedef param::TplMapped <param::MapPiecewiseLinLog> TplPll;
	typedef param::TplMapped <param::MapS <false> > TplMaps;
	typedef param::TplMapped <ParamMapFdbkBipolar> TplFdbk;

	// Speed
	auto           log_uptr = std::make_unique <param::TplLog> (
		0.01, 100,
		"Sp\nSpd\nSpeed",
		"Hz",
		param::HelperDispNum::Preset_FLOAT_STD,
		0,
		"%7.3f"
	);
	log_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
	_desc_set.add_glob (Param_SPEED, log_uptr);

	// Mix
	auto           maps_uptr = std::make_unique <TplMaps> (
		0, 1,
		"Phase Mix\nPh Mix\nMix\nM",
		"%",
		param::HelperDispNum::Preset_FLOAT_PERCENT,
		0,
		"%5.1f"
	);
	maps_uptr->use_mapper ().config (
		maps_uptr->get_nat_min (),
		maps_uptr->get_nat_max ()
	);
	_desc_set.add_glob (Param_MIX, maps_uptr);

	// Feedback
	auto           fbi_uptr = std::make_unique <TplFdbk> (
		TplFdbk::Mapper::get_nat_min (),
		TplFdbk::Mapper::get_nat_max (),
		"Feedback level\nFdbk level\nFeedback\nFdbk\nF",
		"%",
		param::HelperDispNum::Preset_FLOAT_PERCENT,
		0,
		"%+6.1f"
	);
	_desc_set.add_glob (Param_FEEDBACK, fbi_uptr);

	// Number of stages
	auto           int_uptr = std::make_unique <param::TplInt> (
		1, 20,
		"Number of stages\nStages\nStg",
		"",
		0,
		"%.0f"
	);
	int_uptr->use_disp_num ().set_scale (2);
	_desc_set.add_glob (Param_NBR_STAGES, int_uptr);

	// Minimum frequency
	auto           pll_uptr = std::make_unique <TplPll> (
		20, 20480,
		"Minimum frequency\nMin freq\nMinF\nMF",
		"Hz",
		param::HelperDispNum::Preset_FLOAT_STD,
		0,
		"%4.0f"
	);
	pll_uptr->use_mapper ().gen_log (10);
	pll_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
	_desc_set.add_glob (Param_FREQ_MIN, pll_uptr);

	// Maximum frequency
	pll_uptr = std::make_unique <TplPll> (
		20, 20480,
		"Maximum frequency\nMax freq\nMaxF\nMF",
		"Hz",
		param::HelperDispNum::Preset_FLOAT_STD,
		0,
		"%4.0f"
	);
	pll_uptr->use_mapper ().gen_log (10);
	pll_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
	_desc_set.add_glob (Param_FREQ_MAX, pll_uptr);

	// Feedback source
	int_uptr = std::make_unique <param::TplInt> (
		1, 40,
		"Feedback source\nFdbk source\nFdbk src\nFSrc\nFS",
		"",
		0,
		"%.0f"
	);
	_desc_set.add_glob (Param_FDBK_POS, int_uptr);

	// Waveform
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		"Sine\nTriangle\nSquare\nSaw\nParabola\nBiphase\nN-Phase\nVarislope"
		"\nNoise",
		"W\nWavef\nWaveform",
		"",
		0,
		"%s"
	);
	assert (enu_uptr->get_nat_max () == LfoType_NBR_ELT - 1);
	_desc_set.add_glob (Param_WAVEFORM, enu_uptr);

	// Sample and hold
	auto           lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"SnH\nSplHold\nSample & hold\nSample and hold",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_SNH, lin_uptr);

	// Smoothing
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"Sm\nSmooth\nSmoothing",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_SMOOTH, lin_uptr);

	// Chaos amount
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"C\nChaos\nChaos amt\nChaos amount",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_CHAOS, lin_uptr);

	// Phase distortion amount
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PDA\nPhDistA\nPhDistAmt\nPhase dist amt\nPhase dist amount"
		"\nPhase distortion amount",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_PH_DIST_AMT, lin_uptr);

	// Phase distortion offset
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PDO\nPhDistO\nPhDistOfs\nPhase dist ofs\nPhase dist offset"
		"\nPhase distortion offset",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_PH_DIST_OFS, lin_uptr);

	// Sign
	enu_uptr = std::make_unique <param::TplEnum> (
		"Normal\nInvert",
		"Si\nSign",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_SIGN, enu_uptr);

	// Polarity
	enu_uptr = std::make_unique <param::TplEnum> (
		"Bipolar\nUnipolar",
		"Po\nPolar\nPolarity",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_POLARITY, enu_uptr);

	// Variation 1
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"V1\nVar 1\nVariation 1",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_VAR1, lin_uptr);

	// Variation 2
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"V2\nVar 2\nVariation 2",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_VAR2, lin_uptr);

	// Phase set
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PS\nPh set\nPhase set",
		"\xC2\xB0", // U+00B0 DEGREE SIGN
		0,
		"%3.0f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_STD
	);
	lin_uptr->use_disp_num ().set_scale (360);
	_desc_set.add_glob (Param_PHASE_SET, lin_uptr);
}



ParamDescSet &	Phaser2Desc::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInfo	Phaser2Desc::do_get_info () const
{
	return _info;
}



void	Phaser2Desc::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	nbr_i = 1;
	nbr_o = 1;
	nbr_s = 0;
}



int	Phaser2Desc::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



const piapi::ParamDescInterface &	Phaser2Desc::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace phase2
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
