/*****************************************************************************

        FlanchoDesc.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/flancho/FlanchoDesc.h"
#include "mfx/pi/flancho/Param.h"
#include "mfx/pi/flancho/Cst.h"
#include "mfx/pi/param/MapPiecewiseLinLog.h"
#include "mfx/pi/param/MapS.h"
#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplInt.h"
#include "mfx/pi/param/TplLin.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/pi/param/TplMapped.h"
#include "mfx/pi/ParamMapFdbkBipolar.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace flancho
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



FlanchoDesc::FlanchoDesc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	_info._unique_id = "flancho";
	_info._name      = "FlanCho";
	_info._tag_list  = { piapi::Tag::_modulation_0 };
	_info._chn_pref  = piapi::ChnPref::STEREO;

	typedef param::TplMapped <ParamMapFdbkBipolar> TplFdbk;
	typedef param::TplMapped <param::MapPiecewiseLinLog> TplPll;
	typedef param::TplMapped <param::MapS <false> > TplMaps;

	// Speed
	auto           pll_uptr = std::make_unique <TplPll> (
		0.01, 1000,
		"Speed",
		"Hz",
		param::HelperDispNum::Preset_FLOAT_STD,
		0,
		"%7.3f"
	);
	pll_uptr->use_mapper ().set_first_value (     0.01);
	pll_uptr->use_mapper ().add_segment (0.75,   10, true);
	pll_uptr->use_mapper ().add_segment (1   , 1000, true);
	pll_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
	_desc_set.add_glob (Param_SPEED, pll_uptr);

	// Depth
	auto           lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"D\nDpt\nDepth",
		"%",
		0,
		"%5.2f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_DEPTH, lin_uptr);

	// Delay
	auto           log_uptr = std::make_unique <param::TplLog> (
		Cst::_delay_min / 1e6, Cst::_delay_max / 1e6,
		"D\nDly\nDelay",
		"ms",
		param::HelperDispNum::Preset_FLOAT_MILLI,
		0,
		"%5.2f"
	);
	log_uptr->set_categ (piapi::ParamDescInterface::Categ_TIME_S);
	_desc_set.add_glob (Param_DELAY, log_uptr);

	// Feedback
	auto           fbi_uptr = std::make_unique <TplFdbk> (
		TplFdbk::Mapper::get_nat_min (),
		TplFdbk::Mapper::get_nat_max (),
		"F\nFdbk\nFeedback",
		"%",
		param::HelperDispNum::Preset_FLOAT_PERCENT,
		0,
		"%+6.1f"
	);
	_desc_set.add_glob (Param_FDBK, fbi_uptr);

	// Waveform Type
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		"Sine\nTriangle\nParabola\nInv.para\nRamp up\nRamp down\nRandom",
		"T\nWf.T\nWaveform\nWaveform type",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_WF_TYPE, enu_uptr);

	// Waveform Shape
	lin_uptr = std::make_unique <param::TplLin> (
		-1, 1,
		"S\nWf.S\nShape\nWaveform shape",
		"%",
		0,
		"%+6.1f"
	);
	lin_uptr->use_disp_num ().set_preset (param::HelperDispNum::Preset_FLOAT_PERCENT);
	_desc_set.add_glob (Param_WF_SHAPE, lin_uptr);

	// Number of voices
	auto           int_uptr = std::make_unique <param::TplInt> (
		1, int (Cst::_max_nbr_voices),
		"#\nVc#\nNumber of voices",
		"",
		0,
		"%1.0f"
	);
	_desc_set.add_glob (Param_NBR_VOICES, int_uptr);

	// Mix
	auto           maps_uptr = std::make_unique <TplMaps> (
		0, 1,
		"M\nMix",
		"%",
		param::HelperDispNum::Preset_FLOAT_PERCENT,
		0,
		"%5.1f"
	);
	maps_uptr->use_mapper ().config (
		maps_uptr->get_nat_min (),
		maps_uptr->get_nat_max ()
	);
	_desc_set.add_glob (Param_MIX, maps_uptr);

	// Phase set
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"P\nPh.Set\nPhase set",
		"\xC2\xB0", // U+00B0 DEGREE SIGN
		0,
		"%3.0f"
	);
	lin_uptr->use_disp_num ().set_preset (param::HelperDispNum::Preset_FLOAT_STD);
	lin_uptr->use_disp_num ().set_scale (360);
	_desc_set.add_glob (Param_PHASE_SET, lin_uptr);

	// Negative
	enu_uptr = std::make_unique <param::TplEnum> (
		"Add\nSub",
		"M\nMix\nMix Mode",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_NEGATIVE, enu_uptr);

	// Oversampling
	enu_uptr = std::make_unique <param::TplEnum> (
		"\xC3\x97" "1\n" "\xC3\x97" "4", // U+00D7 multiplication sign (UTF-8 C3 97)
		"O\nOvr\nOvrspl\nOversamp\nOversampling\nOversampling rate",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_OVRSPL, enu_uptr);
}



ParamDescSet &	FlanchoDesc::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInfo	FlanchoDesc::do_get_info () const
{
	return _info;
}



void	FlanchoDesc::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	nbr_i = 1;
	nbr_o = 1;
	nbr_s = 0;
}



int	FlanchoDesc::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



const piapi::ParamDescInterface &	FlanchoDesc::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace flancho
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
