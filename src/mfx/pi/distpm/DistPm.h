/*****************************************************************************

        DistPm.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_distpm_DistPm_HEADER_INCLUDED)
#define mfx_pi_distpm_DistPm_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/util/NotificationFlag.h"
#include "fstb/AllocAlign.h"
#include "fstb/VecAlign.h"
#include "mfx/pi/distpm/DistPmChannel.h"
#include "mfx/pi/distpm/DistPmDesc.h"
#include "mfx/pi/ParamProcSimple.h"
#include "mfx/pi/ParamStateSet.h"
#include "mfx/piapi/PluginInterface.h"

#include <vector>



namespace mfx
{
namespace pi
{
namespace distpm
{



class DistPm final
:	public piapi::PluginInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       DistPm (piapi::HostInterface &host);
	               ~DistPm () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::piapi::PluginInterface
	State          do_get_state () const final;
	double         do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const final;
	int            do_reset (double sample_freq, int max_buf_len, int &latency) final;
	void           do_process_block (piapi::ProcInfo &proc) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef fstb::VecAlign <float, 16> BufAlign;

	typedef std::vector <
		DistPmChannel,
		fstb::AllocAlign <DistPmChannel, 16>
	> ChannelArray;

	void           clear_buffers ();
	void           update_param (bool force_flag = false);

	piapi::HostInterface &
	               _host;
	State          _state = State_CREATED;

	DistPmDesc     _desc;
	ParamStateSet  _state_set;
	ParamProcSimple
	               _param_proc { _state_set };
	float          _sample_freq = 0;    // Hz, > 0. <= 0: not initialized
	float          _inv_fs      = 0;    // 1 / _sample_freq

	fstb::util::NotificationFlag
	               _param_change_flag;

	ChannelArray   _chn_arr { _max_nbr_chn };

	// 1st oversampling level
	BufAlign       _buf_o1;

	// 2nd oversampling level
	BufAlign       _buf_o2;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DistPm ()                               = delete;
	               DistPm (const DistPm &other)            = delete;
	               DistPm (DistPm &&other)                 = delete;
	DistPm &       operator = (const DistPm &other)        = delete;
	DistPm &       operator = (DistPm &&other)             = delete;
	bool           operator == (const DistPm &other) const = delete;
	bool           operator != (const DistPm &other) const = delete;

}; // class DistPm



}  // namespace distpm
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/distpm/DistPm.hpp"



#endif   // mfx_pi_distpm_DistPm_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
