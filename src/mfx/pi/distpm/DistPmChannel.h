/*****************************************************************************

        DistPmChannel.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_distpm_DistPmChannel_HEADER_INCLUDED)
#define mfx_pi_distpm_DistPmChannel_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "hiir/coef/C4x.h"
#include "fstb/VecAlign.h"
#include "mfx/dsp/ctrl/VarBlock.h"
#include "mfx/dsp/dly/DelayLineData.h"
#include "mfx/dsp/dyn/EnvFollowerPeak.h"
#include "mfx/dsp/fir/DownsamplerLerp.h"
#include "mfx/dsp/fir/UpsamplerLerp.h"
#include "mfx/dsp/iir/DcKiller1p.h"
#include "mfx/dsp/iir/Downsampler4xSimd.h"
#include "mfx/dsp/iir/Lpf1p.h"
#include "mfx/dsp/iir/Upsampler4xSimd.h"



namespace mfx
{
namespace pi
{
namespace distpm
{



class DistPmChannel
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef hiir::coef::C4x099 Coef;

	// First oversampling, IIR filter
	static constexpr int _ovrspl1 = 1 << Coef::_ovr_l2;

	// Second oversampling, linear interpolation
	static constexpr int _ovrspl2 = 4;

	static constexpr int _ovrspl_tot = _ovrspl1 * _ovrspl2;

	typedef fstb::VecAlign <float, 16> BufAlign;

	int            reset (double sample_freq, int &latency, BufAlign &buf_o1, BufAlign &buf_o2);
	void           set_mode (bool chelou_flag) noexcept;
	void           set_modulation_amount (float amt) noexcept;
	void           set_feedback_amount (float amt) noexcept;
	void           process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;
	void           set_steady_steate () noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class ZeroNyquist
	{
	public:
		inline void    clear_buffers () noexcept;
		inline float   process_sample (float x) noexcept;
	private:
		float          _x_old = 0;
	};

	// Maximum delay, s, reference for the +1.0 modulator excursion
	static constexpr float  _dly_max = 0.010f;

	typedef dsp::iir::Upsampler4xSimd <
		Coef::X4::_nbr_coef, Coef::X2::_nbr_coef
	> UpSpl;
	typedef dsp::iir::Downsampler4xSimd <
		Coef::X4::_nbr_coef, Coef::X2::_nbr_coef
	> DwSpl;

	inline void    update_dly_scale () noexcept;
	void           upsample_block (int nbr_spl) noexcept;
	void           downsample_block (int nbr_spl) noexcept;
	void           process_block_upsampled (int nbr_spl) noexcept;

	// Sampling rate, Hz, > 0. 0 = not set yet
	float          _sample_freq = 0;

	// Amount of modulation, [0 ; 1]
	float          _amount = 0;

	// Distortion mode. true = uses a LPF on the feedback.
	bool           _chelou_flag = false;

	// Feedback amount in the modulator, [0 ; 1]
	dsp::ctrl::VarBlock
	               _fdbk_amt { 1 };

	// Scaling factor to convert the normalized delay to a delay in samples,
	// taking the modulation amount into account.
	// Gives a negative result (< 0)
	// Ticks at the final oversampled rate
	dsp::ctrl::VarBlock
	               _dly_scale { 0 };

	// First oversampling
	UpSpl          _us;
	DwSpl          _ds;

	// Second oversampling
	dsp::fir::UpsamplerLerp <_ovrspl2>
						_us2;
	dsp::fir::DownsamplerLerp <_ovrspl2>
						_ds2;

	// Maximum delay for the modulation, samples, > 0. 0 = not set
	int            _dly_max_spl = 0;

	// Fully oversampled rate. Size is a power of 2.
	std::vector <float>
	               _ring_buf;
	int            _rb_mask = 0; // size - 1
	int            _rb_pos  = 0; // Writing position, [0 ; size - 1]

	// Envelope follower for the modulator limiter.
	// Runs at 1x rate
	dsp::dyn::EnvFollowerPeak
	               _env;
	float          _mod_mult_old = 0;
	float          _feedback = 0;

	// LPF in the high mids for standard mode, before compression
	dsp::iir::Lpf1p <float>
	               _fdbk_lpf_pre;

	// Post-compression LP filtering
	dsp::iir::Lpf1p <float>
	               _fdbk_lpf;
	ZeroNyquist    _fdbk_zero;

	dsp::iir::DcKiller1p
	               _dc_kill_pre;
	dsp::iir::DcKiller1p
	               _dc_kill_post;

	// Intermediate buffer for the oversampling (between 1st and 2nd passes)
	BufAlign *     _buf_o1_ptr  = nullptr;

	// Buffer for full rate data.
	// Also contains standard rate data (DC killing steps)
	BufAlign *     _buf_o2_ptr  = nullptr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const DistPmChannel &other) const = delete;
	bool           operator != (const DistPmChannel &other) const = delete;

}; // class DistPmChannel



}  // namespace distpm
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/distpm/DistPmChannel.hpp"



#endif // mfx_pi_distpm_DistPmChannel_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
