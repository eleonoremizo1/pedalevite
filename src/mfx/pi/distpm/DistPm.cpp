/*****************************************************************************

        DistPm.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/dsp/mix/Align.h"
#include "mfx/pi/distpm/Param.h"
#include "mfx/pi/distpm/DistPm.h"
#include "mfx/piapi/Dir.h"
#include "mfx/piapi/Err.h"
#include "mfx/piapi/EventParam.h"
#include "mfx/piapi/EventTs.h"
#include "mfx/piapi/EventType.h"
#include "mfx/piapi/ProcInfo.h"

#include <algorithm>

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace distpm
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DistPm::DistPm (piapi::HostInterface &host)
:	_host (host)
{
	dsp::mix::Align::setup ();

	const ParamDescSet & desc_set = _desc.use_desc_set ();
	_state_set.init (piapi::ParamCateg_GLOBAL, desc_set);

	_state_set.set_val_nat (desc_set, Param_AMOUNT  , 0);
	_state_set.set_val_nat (desc_set, Param_MODE    , 0);
	_state_set.set_val_nat (desc_set, Param_FEEDBACK, 0);

	_state_set.add_observer (Param_AMOUNT  , _param_change_flag);
	_state_set.add_observer (Param_MODE    , _param_change_flag);
	_state_set.add_observer (Param_FEEDBACK, _param_change_flag);

	_state_set.set_ramp_time (Param_AMOUNT  , 0.010);
	_state_set.set_ramp_time (Param_FEEDBACK, 0.010);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInterface::State	DistPm::do_get_state () const
{
	return _state;
}



double	DistPm::do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const
{
	fstb::unused (categ, note_id);
	assert (categ == piapi::ParamCateg_GLOBAL);

	return _state_set.use_state (index).get_val_tgt ();
}



int	DistPm::do_reset (double sample_freq, int max_buf_len, int &latency)
{
	_sample_freq = float (    sample_freq);
	_inv_fs      = float (1 / sample_freq);

	_state_set.set_sample_freq (sample_freq);
	_state_set.clear_buffers ();

	const auto     mbl_align = (max_buf_len + 3) & ~3;
	const auto     mbl_o1    = mbl_align * DistPmChannel::_ovrspl1;
	const auto     mbl_o2    = mbl_align * DistPmChannel::_ovrspl_tot;

	_buf_o1.resize (mbl_o1);
	_buf_o2.resize (mbl_o2);

	for (auto &chn : _chn_arr)
	{
		int             lat_chn = 0;
		int             ret_val =
			chn.reset (sample_freq, lat_chn, _buf_o1, _buf_o2);
		if (ret_val != piapi::Err_OK)
		{
			return ret_val;
		}
		latency = lat_chn;
	}

	_param_change_flag.set ();

	update_param (true);
	_param_proc.req_steady ();

	clear_buffers ();

	_state = State_ACTIVE;

	return piapi::Err_OK;
}



void	DistPm::do_process_block (piapi::ProcInfo &proc)
{
	const int      nbr_chn_src = proc._dir_arr [piapi::Dir_IN ]._nbr_chn;
	const int      nbr_chn_dst = proc._dir_arr [piapi::Dir_OUT]._nbr_chn;
	assert (nbr_chn_src <= nbr_chn_dst);
	const int      nbr_chn_proc = std::min (nbr_chn_src, nbr_chn_dst);

	// Events
	_param_proc.handle_msg (proc);

	// Parameters
	_state_set.process_block (proc._nbr_spl);
	update_param ();
	if (_param_proc.is_full_reset ())
	{
		clear_buffers ();
	}
	else if (_param_proc.is_req_steady_state ())
	{
		// Clears the parameter ramps
		for (auto &chn : _chn_arr)
		{
			chn.set_steady_steate ();
		}
	}

	// Signal processing
	for (int chn_idx = 0; chn_idx < nbr_chn_proc; ++chn_idx)
	{
		auto &         chn = _chn_arr [chn_idx];

		chn.process_block (
			proc._dst_arr [chn_idx],
			proc._src_arr [chn_idx],
			proc._nbr_spl
		);
	}

	// Duplicates the remaining output channels
	for (int chn_idx = nbr_chn_proc; chn_idx < nbr_chn_dst; ++chn_idx)
	{
		dsp::mix::Align::copy_1_1 (
			proc._dst_arr [chn_idx],
			proc._dst_arr [0],
			proc._nbr_spl
		);
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DistPm::clear_buffers ()
{
	for (auto &chn : _chn_arr)
	{
		chn.clear_buffers ();
	}
}



void	DistPm::update_param (bool force_flag)
{
	if (_param_change_flag (true) || force_flag)
	{
		const auto     amount =
			float (_state_set.get_val_end_nat (Param_AMOUNT));

		const auto     mode = _state_set.get_val_int (Param_MODE);
		const bool     chelou_flag = (mode != 0);

		const auto     fdbk =
			float (_state_set.get_val_end_nat (Param_FEEDBACK));

		for (auto &chn : _chn_arr)
		{
			chn.set_modulation_amount (amount);
			chn.set_mode (chelou_flag);
			chn.set_feedback_amount (fdbk);
		}
	}
}



}  // namespace distpm
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
