/*****************************************************************************

        DistPmChannel.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/piapi/Err.h"
#include "mfx/pi/distpm/DistPmChannel.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace distpm
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: reset
Description:
	Initializes the object.
	Mandatory before calling any other function.
	Provided buffers are not checked here, only their references are kept (so
	they can be empty during this call).
Input parameters:
	- sample_freq: sampling rate in Hz
	- buf_o1: a temporary buffer used in process_block(). At call time, its
		length should be greater or equal to the block length * _ovrspl1.
	- buf_o2: another temporary buffer used in process_block(). At call time,
		its length should be greater or equal to the block length * _ovrspl_tot.
Output parameters:
	- latency: processing latency in samples
Returns: 0 (always O.K.)
Throws: memory allocation exceptions
==============================================================================
*/

int	DistPmChannel::reset (double sample_freq, int &latency, BufAlign &buf_o1, BufAlign &buf_o2)
{
	assert (sample_freq > 0);

	_sample_freq = float (sample_freq);
	_buf_o1_ptr  = &buf_o1;
	_buf_o2_ptr  = &buf_o2;

	_us.set_coefs (
		Coef::X4::_coef_list.data (), Coef::X2::_coef_list.data ()
	);
	_ds.set_coefs (
		Coef::X4::_coef_list.data (), Coef::X2::_coef_list.data ()
	);

	latency = hiir::coef::C4x099::_delay;

	// Delay ring buffer
	_dly_max_spl = fstb::round_int (_dly_max * _ovrspl_tot * sample_freq);
	const auto     storage_req = _dly_max_spl + 1; // +1 for LERP
	const auto     rb_len      = 1 << fstb::get_next_pow_2 (storage_req);
	_ring_buf.resize (rb_len);
	_rb_mask = rb_len - 1;
	if (_rb_pos >= rb_len)
	{
		_rb_pos = 0;
	}

	// Envelope detector
	_env.set_sample_freq (sample_freq * _ovrspl1);
	_env.set_times (0.001f, 0.100f);

	// Deduces the feedback LPF cutoff frequency from the maximum delay.
	// This makes the delay fixed for high frequencies fundamentals whatever
	// the maximum delay is, whereas delay for low frequencies may be limited.
	// Constant is arbitrary and was chosen empirically from other parameters
	// during prototyping stage.
	constexpr auto lpf_freq = 1.25 / _dly_max;

	// 1st order approximation for a 1-pole LPF, valid only for the lowest
	// frequencies
	const auto     c_lpf = float (
		lpf_freq * 2 * fstb::PI / (_sample_freq * _ovrspl_tot)
	);
	_fdbk_lpf.set_coef (c_lpf);

	const auto     c_lpf_pre = float (
		2000.0 * 2 * fstb::PI / (_sample_freq * _ovrspl_tot)
	);
	_fdbk_lpf_pre.set_coef (c_lpf_pre);

	// DC killers
	constexpr auto hpf_freq = 5.f; // Hz
	_dc_kill_pre.set_sample_freq (sample_freq);
	_dc_kill_pre.set_cutoff_freq (hpf_freq);
	_dc_kill_post.set_sample_freq (sample_freq);
	_dc_kill_pre.set_cutoff_freq (hpf_freq);

	return piapi::Err_OK;
}



/*
==============================================================================
Name: set_mode
Description:
	Selects between standard distortion mode and chelou mode.
Input parameters:
	- chelou_flag: true for chelou, false for standard.
==============================================================================
*/

void	DistPmChannel::set_mode (bool chelou_flag) noexcept
{
	_chelou_flag = chelou_flag;
	update_dly_scale ();
}



/*
==============================================================================
Name: set_modulation_amount
Description:
	Sets the modulation amount.
Input parameters:
	- amt: modulation amount, in [0 ; 1]
==============================================================================
*/

void	DistPmChannel::set_modulation_amount (float amt) noexcept
{
	assert (amt >= 0);
	assert (amt <= 1);

	_amount = amt;
	update_dly_scale ();
}



/*
==============================================================================
Name: set_feedback_amount
Description:
	Sets the feedback amount in the modulator.
Input parameters:
	- amt: feedback amount, in [0 ; 1]
==============================================================================
*/

void	DistPmChannel::set_feedback_amount (float amt) noexcept
{
	assert (amt >= 0);
	assert (amt <= 1);

	const auto     u     = fstb::ipowpc <6> (1.f - amt);
	const auto     v     = fstb::ipowpc <3> (      amt);
	const auto     remap = (1 - u + amt + 2 * v) * 0.25f;

	_fdbk_amt.set_val (remap);
}



/*
==============================================================================
Name: process_block
Description:
	Processes a block of samples.
	Make sure the temporary buffers are big enough.
Input parameters:
	- src_ptr: pointer on the input sample buffer, containing nbr_spl samples.
	- nbr_spl: number of samples to process, > 0.
Output parameters:
	- dst_ptr: pointer on the output sample buffer, receives nbr_spl samples.
==============================================================================
*/

void	DistPmChannel::process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (_sample_freq > 0);
	assert (_buf_o1_ptr  != nullptr);
	assert (_buf_o2_ptr  != nullptr);
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (nbr_spl > 0);

	assert (nbr_spl * _ovrspl1    <= int (_buf_o1_ptr->size ()));
	assert (nbr_spl * _ovrspl_tot <= int (_buf_o2_ptr->size ()));

	_dc_kill_pre.process_block (_buf_o2_ptr->data (), src_ptr, nbr_spl);

	// Upsampling
	upsample_block (nbr_spl);

	// Processing
	process_block_upsampled (nbr_spl); 

	// Downsampling
	downsample_block (nbr_spl);

	_dc_kill_post.process_block (dst_ptr, _buf_o2_ptr->data (), nbr_spl);
}



/*
==============================================================================
Name: set_steady_steate
Description:
	Makes sure all the internally ramping parameters become stable, reaching
	their target.
==============================================================================
*/

void	DistPmChannel::set_steady_steate () noexcept
{
	_fdbk_amt.clear_buffers ();
	_dly_scale.clear_buffers ();
}



/*
==============================================================================
Name: clear_buffers
Description:
	Sets the object state like if it was fed with silence for an infinite time.
==============================================================================
*/

void	DistPmChannel::clear_buffers () noexcept
{
	set_steady_steate ();
	_us.clear_buffers ();
	_ds.clear_buffers ();
	_us2.clear_buffers ();
	_ds2.clear_buffers ();
	std::fill (_ring_buf.begin (), _ring_buf.end (), 0.f);
	_rb_pos = 0;
	_env.clear_buffers ();
	_mod_mult_old = 0;
	_feedback     = 0;
	_fdbk_lpf.clear_buffers ();
	_fdbk_lpf_pre.clear_buffers ();
	_fdbk_zero.clear_buffers ();
	_dc_kill_pre.clear_buffers ();
	_dc_kill_post.clear_buffers ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DistPmChannel::ZeroNyquist::clear_buffers () noexcept
{
	_x_old = 0;
}



float	DistPmChannel::ZeroNyquist::process_sample (float x) noexcept
{
	const auto     y = (x + _x_old) * 0.5f;
	_x_old = x;

	return y;
}



void	DistPmChannel::update_dly_scale () noexcept
{
	assert (_dly_max_spl > 0);

	auto           remap = _amount;
	if (! _chelou_flag)
	{
		constexpr auto scale = 0.5f;
		remap *= (scale * 0.25f) + (scale * 0.75f) * fstb::sq (remap);
	}
	_dly_scale.set_val (-float (_dly_max_spl) * remap);
}



// In-place in _buf_o2_ptr, uses _buf_o1_ptr as temporary
void	DistPmChannel::upsample_block (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	auto &         buf_o1     = *_buf_o1_ptr;
	auto &         buf_o2     = *_buf_o2_ptr;
	const int      nbr_spl_o1 = nbr_spl * _ovrspl1;
	_us.process_block (buf_o1.data (), buf_o2.data (), nbr_spl);
	_us2.process_block (buf_o2.data (), buf_o1.data (), nbr_spl_o1);
}



// In-place in _buf_o2_ptr, uses _buf_o1_ptr as temporary
void	DistPmChannel::downsample_block (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	auto &         buf_o1     = *_buf_o1_ptr;
	auto &         buf_o2     = *_buf_o2_ptr;
	const int      nbr_spl_o1 = nbr_spl * _ovrspl1;
	_ds2.process_block (buf_o1.data (),buf_o2.data (), nbr_spl_o1);
	_ds.process_block (buf_o2.data (), buf_o1.data (), nbr_spl);
}



// In-place in _buf_o2_ptr
void	DistPmChannel::process_block_upsampled (int nbr_spl) noexcept
{
	assert (nbr_spl > 0);

	const auto     nbr_spl_ot = nbr_spl * _ovrspl_tot;
	assert (nbr_spl_ot <= int (_buf_o2_ptr->size ()));

	float * fstb_RESTRICT   spl_ptr = _buf_o2_ptr->data ();
	constexpr auto step_mul2 = 1.f / float (_ovrspl2);

	auto           mod_mult_cur  = _mod_mult_old;
	auto           feedback      = _feedback;

	_fdbk_amt.tick (nbr_spl_ot);
	auto           fdbk_amt      = _fdbk_amt.get_beg ();
	const auto     fdbk_amt_step = _fdbk_amt.get_step ();

	_dly_scale.tick (nbr_spl_ot);
	auto           dly_scale     = _dly_scale.get_beg ();
	const auto     dly_scl_step  = _dly_scale.get_step ();

	for (int pos = 0; pos < nbr_spl_ot; pos += _ovrspl2)
	{
		// Level normalisation on the feedback. Target range: +/-0.5
		const auto     lvl_tgt  = 0.5f;
		const auto     env_val  = _env.process_sample (feedback);
		const auto     min_lvl  = 1e-5f; // To avoid div by 0 on small signals
		const auto     mod_mult = lvl_tgt / std::max (env_val, min_lvl);
		const auto     mm_step  = (mod_mult - mod_mult_cur) * step_mul2;

		for (int k = 0; k < _ovrspl2; ++k)
		{
			const auto     x = spl_ptr [pos + k];
			_ring_buf [_rb_pos] = x;

			const auto     compressed = feedback * mod_mult;

			// LPF on the compressed feedback
			auto           filtered = compressed;
			if (_chelou_flag)
			{
				filtered = _fdbk_lpf.process_sample (filtered);
			}
			else
			{
				filtered = _fdbk_zero.process_sample (filtered);
			}

			// Delay calculations
			const auto     dly_nrm  = filtered + 0.5f; // Nominal in [0 ; 1]
			auto           ofs_read = dly_nrm * dly_scale; // Negative
			constexpr auto dly_min  = 0.f;
			const auto     dly_max  = float (_dly_max_spl);
			ofs_read = fstb::limit (ofs_read, -dly_max, -dly_min);
			const auto     ofs_int  = fstb::floor_int (ofs_read);
			const auto     pos_frac = ofs_read - float (ofs_int);
			assert (ofs_int < 0 || (ofs_int == 0 && pos_frac <= 0));
			const auto     pos_read = _rb_pos + ofs_int;

			// Read + LERP
			const auto     x0 = _ring_buf [ pos_read      & _rb_mask];
			const auto     x1 = _ring_buf [(pos_read + 1) & _rb_mask];
			const auto     y  = fstb::lerp (x0, x1, pos_frac);

			// Writes result
			spl_ptr [pos + k] = y;

			feedback = fstb::lerp (x, y, fdbk_amt);

			// Next
			_rb_pos       = (_rb_pos + 1) & _rb_mask;
			mod_mult_cur += mm_step;
			fdbk_amt     += fdbk_amt_step;
			dly_scale    += dly_scl_step;

			if (! _chelou_flag)
			{
				feedback = _fdbk_lpf_pre.process_sample (feedback);
			}
		}
	}

	_mod_mult_old = mod_mult_cur;
	_feedback     = feedback;
}



}  // namespace distpm
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
