/*****************************************************************************

        BazzFz.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/dsp/mix/Align.h"
#include "mfx/pi/bfz/Param.h"
#include "mfx/pi/bfz/BazzFz.h"
#include "mfx/piapi/Dir.h"
#include "mfx/piapi/Err.h"
#include "mfx/piapi/EventParam.h"
#include "mfx/piapi/EventTs.h"
#include "mfx/piapi/EventType.h"
#include "mfx/piapi/ProcInfo.h"

#include <algorithm>

#include <cassert>
#include <cmath>



namespace mfx
{
namespace pi
{
namespace bfz
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



BazzFz::BazzFz (piapi::HostInterface &host)
:	_host (host)
,	_chn_arr (_max_nbr_chn)
,	_ovrspl (Ovrspl_X1)
,	_ovrspl_rate (1)
{
	dsp::mix::Align::setup ();

	const ParamDescSet & desc_set = _desc.use_desc_set ();
	_state_set.init (piapi::ParamCateg_GLOBAL, desc_set);

	_state_set.set_val_nat (desc_set, Param_GAIN  , 1);
	_state_set.set_val_nat (desc_set, Param_OVRSPL, Ovrspl_X1);

	_state_set.add_observer (Param_GAIN  , _param_change_flag);
	_state_set.add_observer (Param_OVRSPL, _param_change_flag);

	_state_set.set_ramp_time (Param_GAIN, 0.010);

	for (auto &chn : _chn_arr)
	{
		chn._ovr.cap_ratio_l2 (int (Ovrspl_NBR_ELT) - 1);
		chn._ovr.set_ratio_l2 (int (_ovrspl));
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInterface::State	BazzFz::do_get_state () const
{
	return _state;
}



double	BazzFz::do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const
{
	fstb::unused (categ, note_id);
	assert (categ == piapi::ParamCateg_GLOBAL);

	return _state_set.use_state (index).get_val_tgt ();
}



int	BazzFz::do_reset (double sample_freq, int max_buf_len, int &latency)
{
	_sample_freq = float (    sample_freq);
	_inv_fs      = float (1 / sample_freq);
	// Sampling rate will be set in the circuit simulator during oversampling
	// parameter update.

	_state_set.set_sample_freq (sample_freq);
	_state_set.clear_buffers ();

	const int      mbs_align = (max_buf_len + 3) & ~3;
	_buf_tmp.resize (mbs_align);
	_buf_ovr.resize (mbs_align * _ovrspl_ratio_max);

	update_param (true);
	_param_proc.req_steady ();

	clear_buffers ();

	_state = State_ACTIVE;

	latency = _chn_arr.front ()._ovr.get_latency ();

	return piapi::Err_OK;
}



void	BazzFz::do_process_block (piapi::ProcInfo &proc)
{
	const int      nbr_chn_src = proc._dir_arr [piapi::Dir_IN ]._nbr_chn;
	const int      nbr_chn_dst = proc._dir_arr [piapi::Dir_OUT]._nbr_chn;
	assert (nbr_chn_src <= nbr_chn_dst);
	const int      nbr_chn_proc = std::min (nbr_chn_src, nbr_chn_dst);

	// Events
	_param_proc.handle_msg (proc);

	// Parameters
	_state_set.process_block (proc._nbr_spl);
	update_param ();
	if (_param_proc.is_full_reset ())
	{
		clear_buffers ();
	}
	else if (_param_proc.is_req_steady_state ())
	{
		_gain.clear_buffers ();
	}

	// Signal processing
	const int      nbr_spl     = proc._nbr_spl;
	const int      nbr_spl_ovr = nbr_spl * _ovrspl_rate;

	_gain.tick (nbr_spl);

	for (int chn_idx = 0; chn_idx < nbr_chn_proc; ++chn_idx)
	{
		Channel &      chn = _chn_arr [chn_idx];

		// Gain, pre
		const float *  src_ptr = proc._src_arr [chn_idx];
		float *        dst_ptr = _buf_tmp.data ();
		dsp::mix::Align::copy_1_1_vlrauto (
			dst_ptr, src_ptr, proc._nbr_spl, _gain.get_beg (), _gain.get_end ()
		);
		src_ptr = dst_ptr;

		// Upsampling
		dst_ptr = _buf_ovr.data ();
		chn._ovr.upsample_block (dst_ptr, src_ptr, nbr_spl);
		src_ptr = dst_ptr;

		// Main processing
		chn._bfz.process_block (dst_ptr, src_ptr, nbr_spl_ovr);

		// Downsampling
		dst_ptr = proc._dst_arr [chn_idx];
		chn._ovr.downsample_block (dst_ptr, src_ptr, nbr_spl);

		// Gain, post
		if (_sig_scale != 1)
		{
			dsp::mix::Align::scale_1_v (dst_ptr, nbr_spl, 1.f / _sig_scale);
		}
	}

	// Duplicates the remaining output channels
	for (int chn_idx = nbr_chn_proc; chn_idx < nbr_chn_dst; ++chn_idx)
	{
		dsp::mix::Align::copy_1_1 (
			proc._dst_arr [chn_idx],
			proc._dst_arr [0],
			proc._nbr_spl
		);
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	BazzFz::clear_buffers ()
{
	for (auto &chn : _chn_arr)
	{
		chn._bfz.clear_buffers ();
		chn._ovr.clear_buffers ();
	}
	_gain.clear_buffers ();
}



void	BazzFz::update_param (bool force_flag)
{
	if (_param_change_flag (true) || force_flag)
	{
		const auto     gain = float (_state_set.get_val_end_nat (Param_GAIN));
		_gain.set_val (gain);

		_ovrspl = Ovrspl (fstb::round_int (
			_state_set.get_val_tgt_nat (Param_OVRSPL)
		));
		update_oversampling ();
	}
}



void	BazzFz::update_oversampling ()
{
	const auto     rate_l2 = int (_ovrspl);
	_ovrspl_rate = 1 << rate_l2;
	for (auto &chn : _chn_arr)
	{
		chn._ovr.set_ratio_l2 (rate_l2);
		chn._bfz.set_sample_freq (_sample_freq * double (_ovrspl_rate));
	}
}



}  // namespace bfz
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
