/*****************************************************************************

        BazzFz.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_pi_bfz_BazzFz_HEADER_INCLUDED)
#define mfx_pi_bfz_BazzFz_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/util/NotificationFlag.h"
#include "fstb/VecAlign.h"
#include "mfx/dsp/ctrl/VarBlock.h"
#include "mfx/dsp/va/BazzFuzz.h"
#include "mfx/pi/bfz/BazzFzDesc.h"
#include "mfx/pi/bfz/Ovrspl.h"
#include "mfx/pi/cdsp/OvrsplMulti.h"
#include "mfx/pi/ParamProcSimple.h"
#include "mfx/pi/ParamStateSet.h"
#include "mfx/piapi/PluginInterface.h"

#include <array>
#include <vector>



namespace mfx
{
namespace pi
{
namespace bfz
{



class BazzFz final
:	public piapi::PluginInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       BazzFz (piapi::HostInterface &host);
	               ~BazzFz () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::piapi::PluginInterface
	State          do_get_state () const final;
	double         do_get_param_val (piapi::ParamCateg categ, int index, int note_id) const final;
	int            do_reset (double sample_freq, int max_buf_len, int &latency) final;
	void           do_process_block (piapi::ProcInfo &proc) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _ovrspl_ratio_max = 1 << (Ovrspl_NBR_ELT - 1);

	// Signal scale: input is amplified by X and output attenuated by 1/X
	static constexpr float  _sig_scale     = 4.f;

	typedef fstb::VecAlign <float, 16> BufAlign;

	class Channel
	{
	public:
		dsp::va::BazzFuzz
		               _bfz;
		cdsp::OvrsplMulti
		               _ovr;
	};
	typedef fstb::VecAlign <Channel, 16> ChannelArray;

	void           clear_buffers ();
	void           update_param (bool force_flag = false);
	void           update_oversampling ();

	piapi::HostInterface &
	               _host;
	State          _state = State_CREATED;

	BazzFzDesc     _desc;
	ParamStateSet  _state_set;
	ParamProcSimple
	               _param_proc { _state_set };
	float          _sample_freq = 0;    // Hz, > 0. <= 0: not initialized
	float          _inv_fs      = 0;    // 1 / _sample_freq

	fstb::util::NotificationFlag
	               _param_change_flag;

	ChannelArray   _chn_arr;
	Ovrspl         _ovrspl;
	int            _ovrspl_rate;
	dsp::ctrl::VarBlock                 // Cumulates _sig_scale with the gain
	               _gain;

	BufAlign       _buf_tmp;
	BufAlign       _buf_ovr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               BazzFz ()                               = delete;
	               BazzFz (const BazzFz &other)            = delete;
	               BazzFz (BazzFz &&other)                 = delete;
	BazzFz &       operator = (const BazzFz &other)        = delete;
	BazzFz &       operator = (BazzFz &&other)             = delete;
	bool           operator == (const BazzFz &other) const = delete;
	bool           operator != (const BazzFz &other) const = delete;

}; // class BazzFz



}  // namespace bfz
}  // namespace pi
}  // namespace mfx



//#include "mfx/pi/bfz/BazzFz.hpp"



#endif   // mfx_pi_bfz_BazzFz_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
