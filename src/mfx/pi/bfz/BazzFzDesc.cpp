/*****************************************************************************

        BazzFzDesc.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/bfz/BazzFzDesc.h"
#include "mfx/pi/bfz/Ovrspl.h"
#include "mfx/pi/bfz/Param.h"
#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace bfz
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



BazzFzDesc::BazzFzDesc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	_info._unique_id = "bfz";
	_info._name      = "Bazz Fuzz\nBazzFuzz\nBazzFz\nBFz";
	_info._tag_list  = { piapi::Tag::_distortion_0 };
	_info._chn_pref  = piapi::ChnPref::NONE;

	// Gain
	auto           log_uptr = std::make_unique <param::TplLog> (
		0.1, 10.0,
		"Gain\nGa",
		"dB",
		param::HelperDispNum::Preset_DB,
		0,
		"%+5.1f"
	);
	_desc_set.add_glob (Param_GAIN, log_uptr);

	// Oversampling rate
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		// U+00D7 multiplication sign (UTF-8 C3 97)
		"\xC3\x97" "1\n" "\xC3\x97" "2\n" "\xC3\x97" "4",
		"Oversampling rate\nOversampling\nOversamp\nOvrspl\nOvrs\nOS",
		"",
		0,
		"%s"
	);
	assert (int (enu_uptr->get_nat_max ()) == Ovrspl_NBR_ELT - 1);
	_desc_set.add_glob (Param_OVRSPL, enu_uptr);
}



ParamDescSet &	BazzFzDesc::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



piapi::PluginInfo	BazzFzDesc::do_get_info () const
{
	return _info;
}



void	BazzFzDesc::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	nbr_i = 1;
	nbr_o = 1;
	nbr_s = 0;
}



int	BazzFzDesc::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



const piapi::ParamDescInterface &	BazzFzDesc::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace bfz
}  // namespace pi
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
