/*****************************************************************************

        LfoDesc.hpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_pi_lfo1_LfoDesc_CODEHEADER_INCLUDED)
#define mfx_pi_lfo1_LfoDesc_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/pi/param/TplEnum.h"
#include "mfx/pi/param/TplLin.h"
#include "mfx/pi/param/TplLog.h"
#include "mfx/pi/lfo1/LfoType.h"
#include "mfx/pi/lfo1/Param.h"
#include "mfx/piapi/Tag.h"

#include <cassert>



namespace mfx
{
namespace pi
{
namespace lfo1
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <bool SLOW>
LfoDesc <SLOW>::LfoDesc ()
:	_desc_set (Param_NBR_ELT, 0)
,	_info ()
{
	if (SLOW)
	{
		_info._unique_id = "lfo1slow";
		_info._name      = "VLFO";

		// Period
		auto           log_uptr = std::make_unique <param::TplLog> (
			0.1, 1000,
			"Period\nPer\nP",
			"s",
			param::HelperDispNum::Preset_FLOAT_STD,
			0,
			"%7.2f"
		);
		log_uptr->set_categ (piapi::ParamDescInterface::Categ_TIME_S);
		_desc_set.add_glob (Param_SPEED, log_uptr);
	}
	else
	{
		_info._unique_id = "lfo1";
		_info._name      = "LFO";

		// Speed
		auto           log_uptr = std::make_unique <param::TplLog> (
			0.01, 100,
			"Sp\nSpd\nSpeed",
			"Hz",
			param::HelperDispNum::Preset_FLOAT_STD,
			0,
			"%7.3f"
		);
		log_uptr->set_categ (piapi::ParamDescInterface::Categ_FREQ_HZ);
		_desc_set.add_glob (Param_SPEED, log_uptr);
	}
	_info._tag_list = { piapi::Tag::_control_gen_0 };
	_info._chn_pref = piapi::ChnPref::NONE;

	// Amplitude
	auto           lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"A\nAmp\nAmplitude",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_AMP, lin_uptr);

	// Waveform
	auto           enu_uptr = std::make_unique <param::TplEnum> (
		"Sine\nTriangle\nSquare\nSaw\nParabola\nBiphase\nN-Phase\nVarislope"
		"\nNoise",
		"W\nWf\nWavef\nWaveform",
		"",
		0,
		"%s"
	);
	assert (enu_uptr->get_nat_max () == LfoType_NBR_ELT - 1);
	_desc_set.add_glob (Param_WAVEFORM, enu_uptr);

	// Sample and hold
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"SnH\nSplHold\nSample & hold\nSample and hold",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_SNH, lin_uptr);

	// Smoothing
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"Sm\nSmooth\nSmoothing",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_SMOOTH, lin_uptr);

	// Chaos amount
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"C\nKos\nChaos\nChaos Amt\nChaos amount",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_CHAOS, lin_uptr);

	// Phase distortion amount
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PDA\nPhDistA\nPhDistAmt\nPhase dist amt\nPhase dist amount\nPhase distortion amount",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_PH_DIST_AMT, lin_uptr);

	// Phase distortion offset
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PDO\nPhDistO\nPhDistOfs\nPhase dist ofs\nPhase dist offset\nPhase distortion offset",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_PH_DIST_OFS, lin_uptr);

	// Sign
	enu_uptr = std::make_unique <param::TplEnum> (
		"Normal\nInvert",
		"Si\nSig\nSign",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_SIGN, enu_uptr);

	// Polarity
	enu_uptr = std::make_unique <param::TplEnum> (
		"Bipolar\nUnipolar",
		"Po\nPol\nPolar\nPolarity",
		"",
		0,
		"%s"
	);
	_desc_set.add_glob (Param_POLARITY, enu_uptr);

	// Variation 1
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"V1\nVar 1\nVariation 1",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_VAR1, lin_uptr);

	// Variation 2
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"V2\nVar 2\nVariation 2",
		"%",
		0,
		"%5.1f"
	);
	lin_uptr->use_disp_num ().set_preset (
		param::HelperDispNum::Preset_FLOAT_PERCENT
	);
	_desc_set.add_glob (Param_VAR2, lin_uptr);

	// Phase set
	lin_uptr = std::make_unique <param::TplLin> (
		0, 1,
		"PS\nPhS\nPh set\nPhase set",
		"\xC2\xB0", // U+00B0 DEGREE SIGN
		0,
		"%3.0f"
	);
	lin_uptr->use_disp_num ().set_preset (param::HelperDispNum::Preset_FLOAT_STD);
	lin_uptr->use_disp_num ().set_scale (360);
	_desc_set.add_glob (Param_PHASE_SET, lin_uptr);
}



template <bool SLOW>
ParamDescSet &	LfoDesc <SLOW>::use_desc_set ()
{
	return _desc_set;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <bool SLOW>
piapi::PluginInfo	LfoDesc <SLOW>::do_get_info () const
{
	return _info;
}



template <bool SLOW>
void	LfoDesc <SLOW>::do_get_nbr_io (int &nbr_i, int &nbr_o, int &nbr_s) const
{
	nbr_i = 0;
	nbr_o = 0;
	nbr_s = 1;
}



template <bool SLOW>
int	LfoDesc <SLOW>::do_get_nbr_param (piapi::ParamCateg categ) const
{
	return _desc_set.get_nbr_param (categ);
}



template <bool SLOW>
const piapi::ParamDescInterface &	LfoDesc <SLOW>::do_get_param_info (piapi::ParamCateg categ, int index) const
{
	return _desc_set.use_param (categ, index);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace lfo1
}  // namespace pi
}  // namespace mfx



#endif   // mfx_pi_lfo1_LfoDesc_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
