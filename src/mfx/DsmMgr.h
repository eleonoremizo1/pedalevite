/*****************************************************************************

        DsmMgr.h
        Author: Laurent de Soras, 2024

Class handling several abstract Delta-Sigma Modulation (DSM) channels.
They are equivalent to PWM signals once low-pass filtered, but without well-
defined cycle period. Their switching rate is higher, but they don't have
high-energy harmonics (noise is more evenly spread on the spectrum).
Channels are characterised only by their binary state, they are not tied to
any physical pin or anything at this level.
Channels are assumed as always running.
Physical aspects of the time should be also handled by the caller.
Time unit can be anything (user's choice) but microseconds or nanoseconds fit
well.

Usage:
- Configure the channels and timings
- Setup an external thread running a loop. This loop should:
	- check the current time accurately
	- call process ()
	- Possibly call get_state() and update physical pin states or whatever
	- sleep the thread for the time indicated by the process () result.
- Call set_level () from any thread to change the DSM values.

When changing the number of channels while the processing loop is running,
the user must synchronise the calls to avoid concurrency.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_DsmMgr_HEADER_INCLUDED)
#define mfx_DsmMgr_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <vector>

#include <cstdint>



namespace mfx
{



class DsmMgr
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	class ProcResult
	{
	public:
		static constexpr int _max_nbr_chn_mask = 32;

		// Suggested time to sleep before the next call, in time units.
		int            _sleep_time  = 0;

		// Bitmask of channels whose state has changed.
		uint32_t       _change_mask = 0;
	};

	// Setup
	void           reserve (int nbr_chn);
	void           set_nbr_chn (int nbr_chn);
	void           set_resolution (int period) noexcept;
	void           set_max_sleep (int duration) noexcept;

	void           set_level (int index, float val) noexcept;

	// Callback from a dedicated thread
	ProcResult     process (uint32_t timestamp) noexcept;
	bool           get_state (int index) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Maximum error generated in a single processing frame (value units). >= 1.
	// Prevents too long recovery time when the time distance between two
	// process() calls is too long for an unexpected reason, or at startup
	// when the initial timestamp is random (far from 0).
	static constexpr float _max_err = 100;

	class Channel
	{
	public:
		// Binary state of the channel.
		// Read by get_state () from another thread.
		volatile bool  _state_flag = false;

		// Target DSM ratio, in [0 ; 1]
		// Set by set_level () from another thread.
		volatile float _target     = 0;

		// Current error accumulator for 1st order delta-sigma modulation
		float          _a0         = 0;

		// Target from the past period
		float          _tgt_old    = 0;
	};
	typedef std::vector <Channel> ChannelArray;

	ChannelArray   _chn_arr;

	// Nominal sampling period, in time units
	int            _period         = 100;

	// Maximum sleep time in time units. Should be less than 2^31.
	uint32_t       _max_sleep_time = 10'000;

	// Previous process() timestamp, in time units
	uint32_t       _ts_prev        = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const DsmMgr &other) const = delete;
	bool           operator != (const DsmMgr &other) const = delete;

}; // class DsmMgr



}  // namespace mfx



//#include "mfx/DsmMgr.hpp"



#endif   // mfx_DsmMgr_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
