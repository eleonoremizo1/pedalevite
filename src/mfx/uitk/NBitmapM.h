/*****************************************************************************

        NBitmapM.h
        Author: Laurent de Soras, 2016

Monochromatic bitmap

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_NBitmapM_HEADER_INCLUDED)
#define mfx_uitk_NBitmapM_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"
#include "mfx/uitk/NBitmapBase.h"

#include <cstdint>



namespace mfx
{
namespace uitk
{



class NBitmapM
:	public NBitmapBase <uint8_t>
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef NBitmapBase <uint8_t> Inherited;

	using Inherited::Inherited;

	NBitmapM &     operator = (const NBitmapM &other) = default;
	NBitmapM &     operator = (NBitmapM &&other)      = default;

	void           set_color (ui::PixArgb col) noexcept;
	ui::PixArgb    get_color () const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::NodeInterface via mfx::uitk::NodeBase
	void           do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord) override;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	ui::PixArgb    _color { 255, 255, 255, 255 };



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const NBitmapM &other) const = delete;
	bool           operator != (const NBitmapM &other) const = delete;

}; // class NBitmapM



}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/NBitmapM.hpp"



#endif   // mfx_uitk_NBitmapM_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
