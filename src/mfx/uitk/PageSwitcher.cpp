/*****************************************************************************

        PageSwitcher.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/Page.h"
#include "mfx/uitk/PageSwitcher.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



PageSwitcher::PageSwitcher (Page &page_mgr)
:	_page_mgr (page_mgr)
{
	// Nothing
}



void	PageSwitcher::add_page (pg::PageType page_id, PageInterface &page)
{
	assert (page_id >= 0);
	assert (_page_map.find (page_id) == _page_map.end ());

	_page_map [page_id] = &page;
}



void	PageSwitcher::switch_to (pg::PageType page_id, void *usr_ptr)
{
	// Checks if the page_id is in the stack to keep the outer calls
	// Otherwise, the stack is cleared.
	while (! _call_stack.empty ())
	{
		const PagePos        page_pos = _call_stack.back ();
		_call_stack.pop_back ();
		if (page_pos._page_id == page_id)
		{
			break;
		}
	}

	_return_flag = false;
	switch_to (page_id, usr_ptr, -1);
}



// node_id = cursor location in the initial page
void	PageSwitcher::call_page (pg::PageType page_id, void *usr_ptr, int node_id)
{
	assert (_cur_page >= 0);

	_call_stack.push_back (PagePos { _cur_page, _usr_ptr, node_id });
	_return_flag = false;
	switch_to (page_id, usr_ptr, -1);
}



void	PageSwitcher::return_page ()
{
	assert (! _call_stack.empty ());

	if (_call_stack.empty ())
	{
		// This shouldn't happen, but just in case...
		switch_to (_root, nullptr);
	}
	else
	{
		const PagePos        page_pos = _call_stack.back ();
		_call_stack.pop_back ();
		_return_flag = true;
		switch_to (page_pos._page_id, page_pos._usr_ptr, page_pos._node_id);
	}
}



void	PageSwitcher::return_or_switch_to (pg::PageType page_id, void *usr_ptr)
{
	if (_call_stack.empty ())
	{
		switch_to (page_id, usr_ptr);
	}
	else
	{
		return_page ();
	}
}



bool	PageSwitcher::is_returning () const noexcept
{
	return _return_flag;
}



int	PageSwitcher::get_return_node () const
{
	int            node_id = -1;

	if (_call_stack.empty ())
	{
		assert (false);
	}
	else
	{
		return _call_stack.back ()._node_id;
	}

	return node_id;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	PageSwitcher::switch_to (pg::PageType page_id, void *usr_ptr, int node_id)
{
	assert (page_id >= 0);
	assert (_page_map.find (page_id) != _page_map.end ());

	if (_root == pg::PageType_INVALID)
	{
		assert (usr_ptr == nullptr);
		_root = page_id;
	}

	PageInterface *   page_ptr = _page_map [page_id];
	_cur_page = page_id;
	_usr_ptr  = usr_ptr;
	_page_mgr.set_page_content (*page_ptr, usr_ptr);
	// Makes sure we're still on the same page before jumping, because the page
	// switch could trigger another switch.
	if (page_ptr == _page_mgr.get_page_content () && node_id >= 0)
	{
		_page_mgr.jump_to (node_id);
	}
}



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
