/*****************************************************************************

        RenderCtx.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/grap/RenderCtx.h"

#include <algorithm>

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace grap
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Input parameters:
	- sz: size in pixel of the area covered by the buffer. Must be at least
		1 pixel.
	- stride: step in pixels (or bytes?) between 2 lines. Must be positive and
		greater of equal to the active area width.
	- buf_ptr: pointer on the pixel buffer, on the coordinates (0, 0).
		Must contain at least (sz [1] - 1) * stride + sz [0] pixels.
		Addresses inbetween the lines (when stride > sz [0]) must be valid but
		their content is and will remain undefined.
==============================================================================
*/

RenderCtx::RenderCtx (uint8_t *buf_ptr, const Vec2d &sz, int stride) noexcept
:	_buf_ptr (buf_ptr)
,	_sz (sz)
,	_stride (stride)
{
	assert (buf_ptr != nullptr);
	assert (sz [0] > 0);
	assert (sz [1] > 0);
	assert (stride >= sz [0]);
}



/*
==============================================================================
Name: fill
Description:
	Fills the buffer with a solid color
Input parameters:
	- c: the color, [0 ; 255]
==============================================================================
*/

void	RenderCtx::fill (uint8_t c) noexcept
{
	if (_stride == _sz [0])
	{
		std::fill (_buf_ptr, _buf_ptr + _sz [1] * _stride, c);
	}
	else
	{
		auto           ptr = _buf_ptr;
		for (int y = 0; y < _sz [1]; ++y)
		{
			std::fill (ptr, ptr + _sz [0], c);
			ptr += _stride;
		}
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



constexpr std::array <uint8_t, 256>	RenderCtx::make_lut_l2g () noexcept
{
	std::array <uint8_t, 256>   table { 0 };
	for (int v = 1; v < 256; ++v)
	{
		const auto     x = 255.0 * double (v);

		// sqrt() approximation using Newton-Raphson convergence.
		// 10 iterations should be enough for 8 bits of accuracy.
		// Initial guess: 1 << (bitdepth / 2)
		auto           y = double (1 << 8);
		for (int k = 0; k < 10; ++k)
		{
			y = 0.5 * (y + x / y);
		}

		table [v] = uint8_t (std::min (y + 0.5, 255.0));
	}

	return table;
}



const std::array <uint8_t, 256>	RenderCtx::_lut_l2g = make_lut_l2g ();



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
