/*****************************************************************************

        PrimBezierCubic.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "fstb/Poly.h"
#include "mfx/uitk/grap/PrimBezierCubic.h"
#include "mfx/uitk/grap/PrimLine.h"
#include "mfx/uitk/grap/RenderCtx.h"

#include <algorithm>

#include <cassert>
#include <cmath>



namespace mfx
{
namespace uitk
{
namespace grap
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: draw
Description:
	Draws a single-pixel thick cubic bezier curve a rendering context.
	Coordinates or any part of the curve can be located out of the context,
	the function properly handles the clipping.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1, y1: Coordinates of the control point associated with the first end
	- x2, y2: Coordinates of the control point associated with the last end
	- x3, y3: Coordinates of the last end
	- c: color of the line (pixel intensity), [0 ; 255]
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimBezierCubic::draw (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, uint8_t c) noexcept
{
	draw_common <false> (
		ctx, x0, y0, x1, y1, x2, y2, x3, y3,
		[c] (
			RenderCtx &ctx0, int x00, int y00, double x01, double y01,
			double x02, double y02, int x03, int y03
		) noexcept
		{
			draw_seg (ctx0, x00, y00, x01, y01, x02, y02, x03, y03, c);
		}
	);
}



/*
==============================================================================
Name: draw_aa
Description:
	Draws a cubic bezier curve of selectable tickness in a rendering context.
	Coordinates or any part of the curve can be located out of the context,
	the function properly handles the clipping.
	Line color is fixed to 255. Pixels intensities < 255 are written only if
	they are greater than the background.
	Note: the path followed is not exactly a cubic bezier.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1, y1: Coordinates of the control point associated with the first end
	- x2, y2: Coordinates of the control point associated with the last end
	- x3, y3: Coordinates of the last end
	- th: line thickness in pixels, >= 1.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimBezierCubic::draw_aa (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, double th) noexcept
{
	assert (th >= 1);

	draw_common <true> (
		ctx, x0, y0, x1, y1, x2, y2, x3, y3,
		[th] (
			RenderCtx &ctx0, int x00, int y00, double x01, double y01,
			double x02, double y02, int x03, int y03
		) noexcept
		{
			draw_seg_aa (ctx0, x00, y00, x01, y01, x02, y02, x03, y03, th);
		}
	);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <bool INFL, typename F>
void	PrimBezierCubic::draw_common (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, F fnc_draw_seg)
{
	const int      xc = x0 + x1 - x2 - x3;
	const int      xa = xc - 4 * (x1 - x2);
	const int      xb = x0 - x1 - x2 + x3;
	const int      xd = xb + 4 * (x1 + x2);
	const int      yc = y0 + y1 - y2 - y3;
	const int      ya = yc - 4 * (y1 - y2);
	const int      yb = y0 - y1 - y2 + y3;
	const int      yd = yb + 4 * (y1 + y2);
	int            n  = 0;
	double         t [(INFL ? 6 : 4) + 1];

	// Sub-divide curve at gradient sign changes

	// Horizontal
	auto           t1 = double (xb * xb - xa * xc);
	if (xa == 0)
	{
		// One change
		if (std::abs (xc) < 2 * std::abs (xb))
		{
			t [n] = double (xc) / double (2 * xb);
			++ n;
		}
	}
	else if (t1 > 0.0)
	{                                      
		// Two changes
		const auto     t2 = sqrt (t1);
		t1 = (double (xb) - t2) / double (xa);
		if (fabs (t1) < 1.0)
		{
			t [n] = t1;
			++ n;
		}
		t1 = (double (xb) + t2) / double (xa);
		if (fabs (t1) < 1.0)
		{
			t [n] = t1;
			++ n;
		}
	}

	// Vertical
	t1 = double (yb * yb - ya * yc);
	if (ya == 0)
	{
		// One change
		if (std::abs (yc) < 2 * std::abs (yb))
		{
			t [n] = double (yc) / double (2 * yb);
			++ n;
		}
	}
	else if (t1 > 0.0)
	{
		// Two changes
		const auto     t2 = sqrt (t1);
		t1 = (double (yb) - t2) / double (ya);
		if (fabs (t1) < 1.0)
		{
			t [n] = t1;
			++ n;
		}
		t1 = (double (yb) + t2) / double (ya);
		if (fabs (t1) < 1.0)
		{
			t [n] = t1;
			++ n;
		}
	}

	if constexpr (INFL)
	{
		// Divide at inflection point
		t1 = double (2 * (xa * yb - xb * ya));
		const auto     t2 = double (xa * yc - xc * ya);
		auto           i  = t2 * t2 - t1 * double (2 * (xb * yc - xc * yb));
		if (i > 0)
		{
			// t1 == 0 when the 4 points make a crossed parallelogram (hourglass
			// shape). This case was not handled in the original algorithm.
			if (t1 == 0)
			{
				t [n] = 0;
				++ n;
			}
			else
			{
				i = sqrt (i);
				t [n] = (t2 + i) / t1;
				if (fabs (t [n]) < 1.0)
				{
					++ n;
				}
				t [n] = (t2 - i) / t1;
				if (fabs (t [n]) < 1.0)
				{
					++ n;
				}
			}
		}
	}

	// Bubble sort of n points
	for (int i = 1; i < n; i++)
	{
		if (t [i - 1] > t [i])
		{
			std::swap (t [i - 1], t [i]);
			i = 0;
		}
	}

	// Begin / end point
	t1    = -1.0;
	t [n] =  1.0;

	// Plot each segment separately
	double         fx0 = x0;
	double         fy0 = y0;
	for (int i = 0; i <= n; i++)
	{
		// Sub-divide at t [i-1], t [i]
		const auto     t2  = t [i];
		auto           fx1 =
			(t1 * (t1*xb - 2*xc) - t2 * (t1 * (t1*xa - 2*xb) + xc) + xd) / 8 - fx0;
		auto           fy1 =
			(t1 * (t1*yb - 2*yc) - t2 * (t1 * (t1*ya - 2*yb) + yc) + yd) / 8 - fy0;
		auto           fx2 =
			(t2 * (t2*xb - 2*xc) - t1 * (t2 * (t2*xa - 2*xb) + xc) + xd) / 8 - fx0;
		auto           fy2 =
			(t2 * (t2*yb - 2*yc) - t1 * (t2 * (t2*ya - 2*yb) + yc) + yd) / 8 - fy0;
		auto           fx3 = (t2 * (t2*(3*xb - t2*xa) - 3*xc) + xd) / 8;
		auto           fy3 = (t2 * (t2*(3*yb - t2*ya) - 3*yc) + yd) / 8;
		fx0 -= fx3;
		fy0 -= fy3;

		// Scale bounds to int
		x3 = fstb::round_int (fx3);
		y3 = fstb::round_int (fy3);

		if (fx0 != 0.0)
		{
			fx0  = double (x0 - x3) / fx0;
			fx1 *= fx0;
			fx2 *= fx0;
		}
		if (fy0 != 0.0)
		{
			fy0  = double (y0 - y3) / fy0;
			fy1 *= fy0;
			fy2 *= fy0;
		}

		// Segment t1 - t2
		if (x0 != x3 || y0 != y3)
		{
			fnc_draw_seg (
				ctx,
				x0, y0,
				double (x0) + fx1, double (y0) + fy1,
				double (x0) + fx2, double (y0) + fy2,
				x3, y3
			);
		}

		x0  = x3;
		y0  = y3;
		fx0 = fx3;
		fy0 = fy3;
		t1  = t2;
	}
}



// Plot limited cubic Bezier segment
void	PrimBezierCubic::draw_seg (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, uint8_t c) noexcept
{
	// Step direction
	int            sx = (x0 < x3) ? 1 : -1;
	int            sy = (y0 < y3) ? 1 : -1;

	double         xc = -fabs (x0 + x1 - x2 - x3);
	double         yc = -fabs (y0 + y1 - y2 - y3);
	double         xa = xc - 4 * sx * (x1 - x2);
	double         ya = yc - 4 * sy * (y1 - y2);
	double         xb = sx * (x0 - x1 - x2 + x3);
	double         yb = sy * (y0 - y1 - y2 + y3);
	const double   ep = 0.01;

	// Check for curve restrains
	// slope P0-P1 == P2-P3     and  (P0-P3 == P1-P2      or   no slope change)
	assert ((x1-x0)*(x2-x3) < ep && ((x3-x0)*(x1-x2) < ep || xb*xb < xa*xc+ep));
	assert ((y1-y0)*(y2-y3) < ep && ((y3-y0)*(y1-y2) < ep || yb*yb < ya*yc+ep));

	// Quadratic Bezier
	if (xa == 0 && ya == 0)
	{
		// New midpoint
		const auto     xm = fstb::round_int ((3 * x1 - x0) * 0.5);
		const auto     ym = fstb::round_int ((3 * y1 - y0) * 0.5);
		draw_seg_quad (ctx, x0, y0, xm, ym, x3, y3, c);
		return;
	}

	// Line lengths
	x1 = (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + 1;
	x2 = (x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3) + 1;

	// Loop over both ends
	for (int leg = 1; leg >= 0; --leg)
	{
		double         ab = xa * yb - xb * ya;
		double         ac = xa * yc - xc * ya;
		double         bc = xb * yc - xc * yb;

		// P0 part of self-intersection loop?
		double         ex = ab * (ab + ac - 3 * bc) + ac * ac;

		/// Calculate resolution
		const int      f =
			  (ex > 0)
			? 1
			: fstb::floor_int (sqrt (1 + 1024.0 / x1));

		// Increase resolution
		ab *= f;
		ac *= f;
		bc *= f;
		ex *= f * f;

		// Init differences of 1st degree
		double        xy = 9*(ab+ac+bc)/8;
		double        cb = 8*(xa-ya);
		double        dx =
			27*(8*ab*(yb*yb-ya*yc)+ex*(ya+2*yb+yc))/64-ya*ya*(xy-ya);
		double        dy =
			27*(8*ab*(xb*xb-xa*xc)-ex*(xa+2*xb+xc))/64-xa*xa*(xy+xa);

		// Init differences of 2nd degree
		double        xx =
			3*(3*ab*(3*yb*yb-ya*ya-2*ya*yc)-ya*(3*ac*(ya+yb)+ya*cb))/4;
		double        yy =
			3*(3*ab*(3*xb*xb-xa*xa-2*xa*xc)-xa*(3*ac*(xa+xb)+xa*cb))/4;
		xy = xa*ya*(6*ab+6*ac-3*bc+cb);
		ac = ya*ya;
		cb = xa*xa;
		xy = 3*(xy+9*f*(cb*yb*yc-xb*xc*ac)-18*xb*yb*ab)/8;

		// Negate values if inside self-intersection loop
		if (ex < 0)
		{
			dx = -dx;
			dy = -dy;
			xx = -xx;
			yy = -yy;
			xy = -xy;
			ac = -ac;
			cb = -cb;
		}

		// Init differences of 3rd degree
		ab =  6 * ya * ac;
		ac = -6 * xa * ac;
		bc =  6 * ya * cb;
		cb = -6 * xa * cb;

		// Error of 1st step
		dx += xy;
		ex  = dx + dy;
		dy += xy;

		int            fx = f;
		int            fy = f;
		for (const auto *xy_ptr = &xy; x0 != x3 && y0 != y3; )
		{
			// Plot curve
			ctx.set_pix (x0, y0, c);

			// Move sub-steps of one pixel
			do
			{
				// Confusing values
				if (dx > *xy_ptr || dy < *xy_ptr)
				{
					goto exit;
				}
				// Save value for test of y step
				y1 = 2 * ex - dy;

				// x sub-step
				if (2 * ex >= dx)
				{
					-- fx;
					dx += xx;
					ex += dx;
					xy += ac;
					dy += xy;
					yy += bc;
					xx += ab;
				}

				// y sub-step
				if (y1 <= 0)
				{
					-- fy;
					dy += yy;
					ex += dy;
					xy += bc;
					dx += xy;
					xx += ac;
					yy += cb;
				}
			}
			while (fx > 0 && fy > 0); // pixel complete?

			// x step
			if (2 * fx <= f)
			{
				x0 += sx;
				fx += f;
			}

			// y step
			if (2 * fy <= f)
			{
				y0 += sy;
				fy += f;
			}

			// Pixel ahead valid
			if (xy_ptr == &xy && dx < 0 && dy > 0)
			{
				xy_ptr = &ep;
			}
		}
	exit:

		// Swap legs
		xx =  x0;
		x0 =  x3;
		x3 =  fstb::round_int (xx); // round or trunc?
		sx = -sx;
		xb = -xb;
      yy =  y0;
		y0 =  y3;
		y3 =  fstb::round_int (yy); // round or trunc?
		sy = -sy;
		yb = -yb;
		x1 =  x2;
   } // Try other end

	// Remaining part in case of cusp or crunode
	PrimLine::draw (ctx, x0, y0, x3, y3, c, true);
}



// Plot a limited quadratic Bezier segment
void	PrimBezierCubic::draw_seg_quad (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, uint8_t c) noexcept
{
	int            sx = x2 - x1;
	int            sy = y2 - y1;

	// Relative values for checks
	int            xx = x0 - x1;
	int            yy = y0 - y1;

	// Curvature
	double         cur = xx * sy - yy * sx;

	// Sign of gradient must not change
	assert (xx * sx <= 0 && yy * sy <= 0);

	// Begin with longer part
	if (sx * sx + sy * sy > xx * xx + yy * yy)
	{
		// Swap P0 P2
		x2  = x0;
		x0  = sx + x1;
		y2  = y0;
		y0  = sy + y1;
		cur = -cur;
	}

	// No straight line
	if (cur != 0)
	{
		// x step direction
		xx += sx;
		sx  = (x0 < x2) ? 1 : -1;
		xx *= sx;

		// y step direction
		yy += sy;
		sy  = (y0 < y2) ? 1 : -1;
		yy *= sy;

		// Differences 2nd degree
		int            xy = 2 * xx * yy;
		xx *= xx;
		yy *= yy;

		// Negated curvature?
		if (cur * sx * sy < 0)
		{
			xx  = -xx;
			yy  = -yy;
			xy  = -xy;
			cur = -cur;
		}

		// Differences 1st degree
		double         dx = 4.0 * sy * cur * (x1 - x0) + xx - xy;
		double         dy = 4.0 * sx * cur * (y0 - y1) + yy - xy;
		xx += xx;
		yy += yy;

		// Error 1st step
		double         err = dx + dy + xy;

		do
		{
			// Plot curve
			ctx.set_pix (x0, y0, c);

			// Last pixel -> curve finished
			if (x0 == x2 && y0 == y2)
			{
				return;
			}

			// Save value for test of y step
			const bool     y_step_flag = (2 * err < dx);

			// x step
			if (2 * err > dy)
			{
				x0  += sx;
				dx  -= xy;
				dy  += yy;
				err += dy;
			}

			// y step
			if (y_step_flag)
			{
				y0  += sy;
				dy  -= xy;
				dx  += xx;
				err += dx;
			}
		}
		while (dy < 0 && dx > 0); // Gradient negates -> algorithm fails
	}

	// Plot remaining part to end
	PrimLine::draw (ctx, x0, y0, x2, y2, c, true);
}



// If the segment is long, split it in two parts before using the quadratic
// trick, so the overall shape is better approximated.
void	PrimBezierCubic::draw_seg_aa (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double th) noexcept
{
	assert (th >= 1);

	constexpr int  dist_thr = 32;

#if 0 // Nearly vertical curves sometimes look quite aliased. A bug?
	if (th == 1)
	{
		draw_seg_aa_1pix (ctx, x0, y0, x1, y1, x2, y2, x3, y3);
	}
	else
#endif
	if (fstb::sq (x3 - x0) + fstb::sq (y3 - y0) < fstb::sq (dist_thr))
	{
		draw_seg_aa_final (ctx, x0, y0, x1, y1, x2, y2, x3, y3, th);
	}
	else
	{
		const auto     [x00, y00, x01, y01, x02, y02, x03, y03] =
			divide_cubic (x0, y0, x1, y1, x2, y2, x3, y3, -1, 0);
		draw_seg_aa_final (ctx, x00, y00, x01, y01, x02, y02, x03, y03, th);

		const auto     [x10, y10, x11, y11, x12, y12, x13, y13] =
			divide_cubic (x0, y0, x1, y1, x2, y2, x3, y3, 0, 1);
		draw_seg_aa_final (ctx, x10, y10, x11, y11, x12, y12, x13, y13, th);
	}
}



// Plot limited anti-aliased cubic Bezier segment
void	PrimBezierCubic::draw_seg_aa_1pix (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3) noexcept
{
	// Step direction
	int            sx = x0 < x3 ? 1 : -1;
	int            sy = y0 < y3 ? 1 : -1;

	double         xc = -fabs (x0 + x1 - x2 - x3);
	double         yc = -fabs (y0 + y1 - y2 - y3);
	double         xa = xc - 4 * sx * (x1 - x2);
	double         ya = yc - 4 * sy * (y1 - y2);
	double         xb = sx * (x0 - x1 - x2 + x3);
	double         yb = sy * (y0 - y1 - y2 + y3);

	[[maybe_unused]] constexpr auto ep = 0.01;

	// Check for curve restrains
	// slope P0-P1 == P2-P3     and  (P0-P3 == P1-P2      or  no slope change)
	assert((x1-x0)*(x2-x3) < ep && ((x3-x0)*(x1-x2) < ep || xb*xb < xa*xc+ep));
	assert((y1-y0)*(y2-y3) < ep && ((y3-y0)*(y1-y2) < ep || yb*yb < ya*yc+ep));

	// Quadratic Bezier
	if (xa == 0 && ya == 0)
	{
		// New midpoint
		const auto     xm = fstb::round_int ((3 * x1 - x0) * 0.5);
		const auto     ym = fstb::round_int ((3 * y1 - y0) * 0.5);
		draw_seg_quad_rat_aa (ctx, x0, y0, xm, ym, x3, y3, 1, 1);
		return;
	}

	// Line lengths
	x1 = (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + 1;
	x2 = (x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3) + 1;

	// Loop over both ends
	for (int leg = 1; leg >= 0; --leg)
	{
		double         ab = xa * yb - xb * ya;
		double         ac = xa * yc - xc * ya;
		double         bc = xb * yc - xc * yb;

		// P0 part of self-intersection loop?
		double         ex = ab * (ab + ac - 3 * bc) + ac * ac;

		// Calculate resolution
		const int      f =
			  (ex > 0)
			? 1
			: fstb::floor_int (sqrt (1 + 1024.0 / x1));

		// Increase resolution
		ab *= f;
		ac *= f;
		bc *= f;
		ex *= f * f;

		// Init differences of 1st degree
		double         xy = 9*(ab+ac+bc)/8;
		double         ba = 8*(xa-ya);
		double         dx =
			27*(8*ab*(yb*yb-ya*yc)+ex*(ya+2*yb+yc))/64-ya*ya*(xy-ya);
		double         dy =
			27*(8*ab*(xb*xb-xa*xc)-ex*(xa+2*xb+xc))/64-xa*xa*(xy+xa);

		// Init differences of 2nd degree
		double         xx =
			3*(3*ab*(3*yb*yb-ya*ya-2*ya*yc)-ya*(3*ac*(ya+yb)+ya*ba))/4;
		double         yy =
			3*(3*ab*(3*xb*xb-xa*xa-2*xa*xc)-xa*(3*ac*(xa+xb)+xa*ba))/4;
		xy = xa*ya*(6*ab+6*ac-3*bc+ba);
		ac = ya*ya;
		ba = xa*xa;
		xy = 3*(xy+9*f*(ba*yb*yc-xb*xc*ac)-18*xb*yb*ab)/8;

		// Negate values if inside self-intersection loop
		if (ex < 0)
		{
			dx = -dx;
			dy = -dy;
			xx = -xx;
			yy = -yy;
			xy = -xy;
			ac = -ac;
			ba = -ba;
		}

		// Init differences of 3rd degree
		ab =  6 * ya * ac;
		ac = -6 * xa * ac;
		bc =  6 * ya * ba;
		ba = -6 * xa * ba;

		// Error of 1st step
		dx += xy;
		ex  = dx + dy;
		dy += xy;

		int            fx  = f;
		int            fy  = f;
		int            y2s = 0;
		double         px  = 0;
		double         py  = 0;
		double         ed  = 0;
		while (x0 != x3 && y0 != y3)
		{
			y1 = std::min (fabs (xy - dx), fabs (dy - xy));

			// Approximate error distance
			ed = std::max (fabs (xy - dx), fabs (dy - xy));
			ed = f * (ed + 2 * ed * y1 * y1 / (4 * ed * ed + y1 * y1));

			y1 = 255 * fabs (
				  ex
				- (f - fx + 1) * dx
				- (f - fy + 1) * dy
				+  f           * xy
			) / ed;

			if (y1 < 255)
			{
				// Plot curve
				ctx.max_linear (x0, y0, uint8_t (255 - fstb::round_int (y1)));
			}

			// pixel intensity x move
			px = fabs (ex - (f - fx + 1) * dx + (fy - 1) * dy);

			// pixel intensity y move
			py = fabs (ex - (f - fy + 1) * dy + (fx - 1) * dx);

			// move sub-steps of one pixel
			y2s = y0;
			do
			{
				// two x or y steps
				if (dx + xx > xy || dy + yy < xy)
				{
					goto exit;
				}

				// Save value for test of y step
				const auto     ystep_test = 2 * ex + dx;

				// x sub-step
				if (2 * ex + dy > 0)
				{
					-- fx;
					dx += xx;
					ex += dx;
					xy += ac;
					dy += xy;
					yy += bc;
					xx += ab;
				}

				// Tiny nearly cusp
				else if (ystep_test > 0)
				{
					goto exit;
				}

				// y sub-step
				if (ystep_test <= 0)
				{
					-- fy;
					dy += yy;
					ex += dy;
					xy += bc;
					dx += xy;
					xx += ac;
					yy += ba;
				}
			}
			while (fx > 0 && fy > 0); // pixel complete?

			// x+ anti-aliasing pixel
			if (2 * fy <= f)
			{
				if (py < ed)
				{
					// Plot curve
					ctx.max_linear (
						x0 + sx, y0,
						uint8_t (255 - fstb::round_int (255 * py / ed))
					);
				}

				// y step
				y0 += sy;
				fy += f;
			}

			// y+ anti-aliasing pixel
			if (2 * fx <= f)
			{
				if (px < ed)
				{
					// Plot curve
					ctx.max_linear (
						x0, y2s + sy,
						uint8_t (255 - fstb::round_int (255 * px / ed))
					);
				}

				// x step
				x0 += sx;
				fx += f;
			}
		}
		break; // Finish curve by line

	exit:

		// Round x+ approximation pixel
		if (2 * ex < dy && 2 * fy <= f + 2)
		{
			if (py < ed)
			{
				// Plot curve
				ctx.max_linear (
					x0 + sx, y0,
					uint8_t (255 - fstb::round_int (255 * py / ed))
				);
			}
			y0 += sy;
		}

		// Round y+ approximation pixel
		if (2 * ex > dx && 2 * fx <= f + 2)
		{
			if (px < ed)
			{
				// Plot curve
				ctx.max_linear (
					x0, y2s + sy,
					uint8_t (255 - fstb::round_int (255 * px / ed))
				);
			}
			x0 += sx;
		}

		// swap legs
		xx =  x0;
		x0 =  x3;
		x3 =  fstb::round_int (xx); // round or trunc?
		sx = -sx;
		xb = -xb;
		yy =  y0;
		y0 =  y3;
		y3 =  fstb::round_int (yy); // round or trunc?
		sy = -sy;
		yb = -yb;
		x1 =  x2;
	}

	// Remaining part in case of cusp or crunode
	PrimLine::draw_aa (ctx, x0, y0, x3, y3, 1);
}



void	PrimBezierCubic::draw_seg_aa_final (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double th) noexcept
{
	assert (th >= 1);

	// Split cubic Bezier segment in two quadratic segments
	const auto     x  = fstb::round_int ((x0 + 3 * (x1 + x2) + x3) / 8);
	const auto     y  = fstb::round_int ((y0 + 3 * (y1 + y2) + y3) / 8);
	const auto     xa = fstb::round_int ((x0 + 3 *  x1)            / 4);
	const auto     ya = fstb::round_int ((y0 + 3 *  y1)            / 4);
	const auto     xb = fstb::round_int ((     3 *       x2  + x3) / 4);
	const auto     yb = fstb::round_int ((     3 *       y2  + y3) / 4);
	draw_seg_quad_rat_aa (ctx, x0, y0, xa, ya, x, y, 1.0, th);
	draw_seg_quad_rat_aa (ctx, x, y, xb, yb, x3, y3, 1.0, th);
}



// Plot a limited rational Bezier segment of thickness th, squared weight
void	PrimBezierCubic::draw_seg_quad_rat_aa (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, double w, double th) noexcept
{
	assert (th >= 1);

	// Relative values for checks
	int            sx = x2 - x1;
	int            sy = y2 - y1;

	double         dx = x0 - x2;
	double         dy = y0 - y2;
	double         xx = x0 - x1;
	double         yy = y0 - y1;

	// Curvature
	double         xy  = xx * sy + yy * sx;
	double         cur = xx * sy - yy * sx;
	double         err;
	double         e2;
	double         ed;

	// Sign of gradient must not change
	assert (xx * sx <= 0.0 && yy * sy <= 0.0);

	// No straight line
	if (cur != 0.0 && w > 0.0)
	{
		// Begin with longer part
		if (sx * sx + sy * sy > xx * xx + yy * yy)
		{
			// Swap P0 P2
			std::swap (x0, x2);
			std::swap (y0, y2);
			cur = -cur;
		}

		// Differences 2nd degree
		xx = 2.0 * (4.0 * w * sx * xx + dx * dx);
		yy = 2.0 * (4.0 * w * sy * yy + dy * dy);

		sx = (x0 < x2) ? 1 : -1; // x step direction
		sy = (y0 < y2) ? 1 : -1; // y step direction
		xy = -2.0 * sx * sy * (2.0 * w * xy + dx * dy);

		// Negated curvature?
		if (cur * sx * sy < 0)
		{
			xx  = -xx;
			yy  = -yy;
			xy  = -xy;
			cur = -cur;
		}

		// Differences 1st degree
		dx = 4.0 * w * (x1 - x0) * sy * cur + xx / 2.0;
		dy = 4.0 * w * (y0 - y1) * sx * cur + yy / 2.0;

		// Flat ellipse, algo fails
		if (w < 0.5 && (dx + xx <= 0 || dy + yy >= 0))
		{
			cur = (w + 1.0) / 2.0;
			w   = sqrt (w);
			xy  = 1.0 / (w + 1.0);

			// Subdivide curve
			sx = fstb::round_int ((x0 + 2.0 * w * x1 + x2) * xy / 2.0);
			sy = fstb::round_int ((y0 + 2.0 * w * y1 + y2) * xy / 2.0);

			// Plot separately
			draw_seg_quad_rat_aa (
				ctx,
				x0, y0,
				fstb::round_int ((w*x1+x0)*xy), fstb::round_int ((y1*w+y0)*xy),
				sx, sy,
				cur, th
			);
			draw_seg_quad_rat_aa (
				ctx,
				sx, sy,
				fstb::round_int ((w*x1+x2)*xy), fstb::round_int ((y1*w+y2)*xy),
				x2, y2,
				cur, th
			);
			return;
		}

		// Loop of steep/flat curve
		for (err = 0; dy + 2 * yy < 0 && dx + 2 * xx > 0; )
		{
			// Steep curve
			if (dx + dy + xy < 0)
			{
				do
				{
					// Approximate sqrt
					ed = -dy - 2 * dy * dx * dx / (4.0 * dy * dy + dx * dx);

					// Scale line width
					w = (th - 1) * ed;

					// Start offset
					x1 = fstb::floor_int ((err - ed - w / 2) / dy);

					// Error value at offset
					e2 = err - x1 * dy - w / 2;

					// Start point
					x1 = x0 - x1 * sx;

					// Aliasing pre-pixel
					ctx.max_linear (
						x1, y0,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					// Pixel on thick line
					for (e2 = -w - dy - e2; e2 - dy < ed; e2 -= dy)
					{
						x1 += sx;
						ctx.set_pix (x1, y0, 255);
					}

					// Aliasing post-pixel
					ctx.max_linear (
						x1 + sx, y0,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					// Last pixel -> curve finished
					if (y0 == y2)
					{
						return;
					}

					// y step
					y0  += sy;
					dy  += xy;
					err += dx;
					dx  += xx;

					// e_x+e_xy > 0
					if (2 * err + dy > 0)
					{
						// x step
						x0  += sx;
						dx  += xy;
						err += dy;
						dy  += yy;
					}

					if (x0 != x2 && (dx + 2 * xx <= 0 || dy + 2 * yy >= 0))
					{
						if (std::abs (y2 - y0) > std::abs (x2 - x0))
						{
							goto fail;
						}
						else
						{
							// Other curve near
							break;
						}
					}
				}
				while (dx + dy + xy < 0); // Gradient still steep?

				// Change from steep to flat curve
				y1 = y0;
				for (cur = err - dy - w / 2; cur < ed; cur += dx)
				{
					// Pixel on thick line
					x1 = x0;
					for (e2 = cur; e2 - dy < ed; e2 -= dy)
					{
						x1 -= sx;
						ctx.set_pix (x1, y1, 255);
					}

					// Aliasing post-pixel
					ctx.max_linear (
						x1 - sx, y1,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					y1 += sy;
				}
			}

			// Flat curve
			else
			{
				do
				{
					// Approximate sqrt
					ed = dx + 2 * dx * dy * dy / (4.0 * dx * dx + dy * dy);

					// Scale line width
					w = (th - 1) * ed;

					// Start offset
					y1 = fstb::floor_int ((err + ed + w / 2) / dx);

					// Error value at offset
					e2 = y1 * dx - w / 2 - err;

					// Start point
					y1 = y0 - y1 * sy;

					// Aliasing pre-pixel
					ctx.max_linear (
						x0, y1,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					// Pixel on thick line
					for (e2 = dx - e2 - w; e2 + dx < ed; e2 += dx)
					{
						y1 += sy;
						ctx.set_pix (x0, y1, 255);
					}

					// Aliasing post-pixel
					ctx.max_linear (
						x0, y1 + sy,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					// Last pixel -> curve finished
					if (x0 == x2)
					{
						return;
					}

					// x step
					x0  += sx;
					dx  += xy;
					err += dy;
					dy  += yy;

					// e_y+e_xy < 0
					if (2 * err + dx < 0)
					{
						// y step
						y0  += sy;
						dy  += xy;
						err += dx;
						dx  += xx;
					}

					if (y0 != y2 && (dx + 2 * xx <= 0 || dy + 2 * yy >= 0))
					{
						if (std::abs (y2 - y0) <= std::abs(x2 - x0))
						{
							goto fail;
						}
						else
						{
							// Other curve near
							break;
						}
					}
				}
				while (dx + dy + xy >= 0); // Gradient still flat?

				// Change from flat to steep curve
				x1 = x0;
				for (cur = -err + dx - w / 2; cur < ed; cur -= dy)
				{
					// Pixel on thick line
					y1 = y0;
					for (e2 = cur; e2 + dx < ed; e2 += dx)
					{
						y1 -= sy;
						ctx.set_pix (x1, y1, 255);
					}

					// Aliasing post-pixel
					ctx.max_linear (
						x1, y1 - sy,
						uint8_t (255 - fstb::round_int (255 * e2 / ed))
					);

					x1 += sx;
				}
			}
		}
	fail:
		if constexpr (false) {} // Makes the compiler happy after the label.
	}

	// Confusing error values
	PrimLine::draw_aa (ctx, x0 , y0, x2, y2, th);
}



std::tuple <int, int, double, double, double, double, int, int>	PrimBezierCubic::divide_cubic (int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double t1, double t2) noexcept
{
	assert (t1 >= -1);
	assert (t1 < t2);
	assert (t2 <= 1);

	// Eq. 37
	auto           z0new =
		[] (double za, double zb, double zc, double zd, double t) noexcept
		{
			const auto     p = fstb::Poly::eval (t, zd, -3 * zc, 3 * zb, -za);
			return fstb::round_int (p * 0.125);
		};
	auto           z1new =
		[] (double za, double zb, double zc, double zd, double ta, double tb) noexcept
		{
			const auto     p1 = fstb::Poly::eval (ta, zd, -2 * zc, zb);
			const auto     p2 = fstb::Poly::eval (ta, zc, -2 * zb, za);
			return (p1 - tb * p2) * 0.125;
		};

	// Eq. 26
	auto           za =
		[] (int z0, double z1, double z2, int z3) noexcept
		{
			return double (z0) + 3 * (z2 - z1) - double (z3);
		};
	auto           zb =
		[] (int z0, double z1, double z2, int z3) noexcept
		{
			return double (z0) - z1 - z2 + double (z3);
		};
	auto           zc =
		[] (int z0, double z1, double z2, int z3) noexcept
		{
			return double (z0) + z1 - z2 - double (z3);
		};
	auto           zd =
		[] (int z0, double z1, double z2, int z3) noexcept
		{
			return double (z0) + 3 * (z2 + z1) + double (z3);
		};

	const auto     xa = za (x0, x1, x2, x3);
	const auto     ya = za (y0, y1, y2, y3);
	const auto     xb = zb (x0, x1, x2, x3);
	const auto     yb = zb (y0, y1, y2, y3);
	const auto     xc = zc (x0, x1, x2, x3);
	const auto     yc = zc (y0, y1, y2, y3);
	const auto     xd = zd (x0, x1, x2, x3);
	const auto     yd = zd (y0, y1, y2, y3);

	return std::make_tuple (
		z0new (xa, xb, xc, xd, t1    ),
		z0new (ya, yb, yc, yd, t1    ),
		z1new (xa, xb, xc, xd, t1, t2),
		z1new (ya, yb, yc, yd, t1, t2),
		z1new (xa, xb, xc, xd, t2, t1),
		z1new (ya, yb, yc, yd, t2, t1),
		z0new (xa, xb, xc, xd, t2    ),
		z0new (ya, yb, yc, yd, t2    )
	);
}



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
