/*****************************************************************************

        RenderCtx.hpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_uitk_grap_RenderCtx_CODEHEADER_INCLUDED)
#define mfx_uitk_grap_RenderCtx_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <algorithm>

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace grap
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



uint8_t *	RenderCtx::use_buf () noexcept
{
	return _buf_ptr;
}



const uint8_t*	RenderCtx::use_buf () const noexcept
{
	return _buf_ptr;
}



const Vec2d &	RenderCtx::use_size () const noexcept
{
	return _sz;
}



int	RenderCtx::get_w () const noexcept
{
	return _sz [0];
}



int	RenderCtx::get_h () const noexcept
{
	return _sz [1];
}



int	RenderCtx::get_stride () const noexcept
{
	return _stride;
}



uint8_t &	RenderCtx::at (int x, int y) noexcept
{
	assert (x >= 0);
	assert (x < _sz [0]);
	assert (y >= 0);
	assert (y < _sz [1]);

	return _buf_ptr [y * _stride + x];
}



const uint8_t &	RenderCtx::at (int x, int y) const noexcept
{
	assert (x >= 0);
	assert (x < _sz [0]);
	assert (y >= 0);
	assert (y < _sz [1]);

	return _buf_ptr [y * _stride + x];
}



/*
==============================================================================
Name: set_pix
Description:
	Sets a pixel only if its coordinates are in the render area (clipping).
Input parameters:
	- x, y: pixel coordinates
	- c: pixel value, [0 ; 255]
==============================================================================
*/

void	RenderCtx::set_pix (int x, int y, uint8_t c) noexcept
{
	// Checks clipping
	if (x < 0 || y < 0 || x >= get_w () || y >= get_h ())
	{
		return;
	}

	// Writes pixel value
	at (x, y) = c;
}



/*
==============================================================================
Name: max_linear
Description:
	If the given coordinates are in the render area, the given linear value
	is converted to gamma-compressed and compared with the existing pixel.
	Then the maximum luminance is retained.
	This function is useful for anti-aliased drawing because output values from
	the algorithm are usually in linear light. The max() helps when drawings
	overlap.
Input parameters:
	- x, y: pixel coordinates
	- lin: pixel value in linear light, [0 ; 255]
==============================================================================
*/

void	RenderCtx::max_linear (int x, int y, uint8_t lin) noexcept
{
	// Checks clipping
	if (x < 0 || y < 0 || x >= get_w () || y >= get_h ())
	{
		return;
	}

	// Converts to gamma-compressed
	const auto     gam = _lut_l2g [lin];

	// Writes pixel value
	auto &         pix = at (x, y);
	pix = std::max (pix, gam);
}



/*
==============================================================================
Name: use_lut_l2g
Description:
	Returns the linear to gamma look-up table, unsigned 8 bits to 8 bits.
	This is an approximation of the sRGB gamma transfer curve, using a pure
	gamma of 2.0 instead of a more complex curve equivalent to gamma 2.2.
	However this should be good enough for basic anti-aliasing algorithms.
Returns: the LUT
==============================================================================
*/

const RenderCtx::Lut &	RenderCtx::use_lut_l2g () noexcept
{
	return _lut_l2g;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



#endif   // mfx_uitk_grap_RenderCtx_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
