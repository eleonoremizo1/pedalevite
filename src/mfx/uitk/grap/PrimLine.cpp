/*****************************************************************************

        PrimLine.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/uitk/grap/PrimLine.h"
#include "mfx/uitk/grap/RenderCtx.h"

#include <algorithm>

#include <cassert>
#include <cmath>
#include <cstdlib>



namespace mfx
{
namespace uitk
{
namespace grap
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: draw
Description:
	Draws a single-pixel thick line in a rendering context.
	Coordinates or any part of the line can be located out of the context,
	the function properly handles the clipping.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1, y1: Coordinates of the last end
	- c: color of the line (pixel intensity), [0 ; 255]
	- term_flag: indicates if we have to draw the last pixel in (x1, y1) or
		if it should be left untouched.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimLine::draw (RenderCtx &ctx, int x0, int y0, int x1, int y1, uint8_t c, bool term_flag) noexcept
{
	const int      w = ctx.get_w ();
	const int      h = ctx.get_h ();
	if (   x0 >= 0 && x0 < w
	    && x1 >= 0 && x1 < w
	    && y0 >= 0 && y0 < h
	    && y1 >= 0 && y1 < h)
	{
		draw_no_clip (ctx, x0, y0, x1, y1, c, term_flag);
	}
	else
	{
		const int      stride = ctx.get_stride ();
		const int      dx     =  std::abs (x1 - x0);
		const int      dy     = -std::abs (y1 - y0);
		const int      sx     = (x0 < x1) ? 1 : -1;
		const int      sy     = (y0 < y1) ? 1 : -1;
		const int      sy_buf = sy * stride;
		int            err    = dx + dy;
		uint8_t *             cur_ptr = ctx.use_buf () + y0 * stride + x0;
		const uint8_t * const end_ptr = ctx.use_buf () + y1 * stride + x1;
		bool           clip_h_flag = (x0 >= 0 && x0 < w);
		bool           clip_v_flag = (y0 >= 0 && y0 < h);

		while (cur_ptr != end_ptr)
		{
			if (clip_h_flag && clip_v_flag)
			{
				*cur_ptr = c;
			}
			const int      e2 = err * 2;
			if (e2 >= dy)
			{
				err     += dy;
				x0      += sx;
				cur_ptr += sx;
				clip_h_flag = (x0 >= 0 && x0 < w);
			}
			if (e2 <= dx)
			{
				err     += dx;
				y0      += sy;
				cur_ptr += sy_buf;
				clip_v_flag = (y0 >= 0 && y0 < h);
			}
		}

		if (term_flag)
		{
			assert (end_ptr == cur_ptr);
			if (clip_h_flag && clip_v_flag)
			{
				*cur_ptr = c;
			}
		}
	}
}



/*
==============================================================================
Name: draw_h
Description:
	Draws a single-pixel thick horizontal line in a rendering context.
	Coordinates or any part of the line can be located out of the context,
	the function properly handles the clipping.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1: abscissa of the second end
	- c: color of the line (pixel intensity), [0 ; 255]
	- term_flag: indicates if we have to draw the last pixel in (x1, y0) or
		if it should be left untouched.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimLine::draw_h (RenderCtx &ctx, int x0, int y0, int x1, uint8_t c, bool term_flag) noexcept
{
	if (term_flag)
	{
		x1 += (x1 < x0) ? -1 : 1;
	}
	if (x1 < x0)
	{
		std::swap (x1, x0);
		++ x1;
		++ x0;
	}
	const int      w = ctx.get_w ();
	const int      h = ctx.get_h ();
	if (   (y0 >= 0 && y0 <  h)
	    && (x0 >= 0 || x1 >  0)
	    && (x0 <  w || x1 <= w))
	{
		x0 = std::max (x0, 0);
		x1 = std::min (x1, w);

		uint8_t *      buf_ptr = &ctx.at (x0, y0);
		std::fill (buf_ptr, buf_ptr + x1 - x0, c);
	}
}



/*
==============================================================================
Name: draw_v
Description:
	Draws a single-pixel thick vertical line in a rendering context.
	Coordinates or any part of the line can be located out of the context,
	the function properly handles the clipping.
Input parameters:
	- x0, y0: Coordinates of the first end
	- y1: Ordinate of the second end
	- c: color of the line (pixel intensity), [0 ; 255]
	- term_flag: indicates if we have to draw the last pixel in (x0, y1) or
		if it should be left untouched.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimLine::draw_v (RenderCtx &ctx, int x0, int y0, int y1, uint8_t c, bool term_flag) noexcept
{
	if (term_flag)
	{
		y1 += (y1 < y0) ? -1 : 1;
	}
	if (y1 < y0)
	{
		std::swap (y1, y0);
		++ y1;
		++ y0;
	}
	const int      w = ctx.get_w ();
	const int      h = ctx.get_h ();
	if (   (x0 >= 0 && x0 <  w)
	    && (y0 >= 0 || y1 >  0)
	    && (y0 <  h || y1 <= h))
	{
		y0 = std::max (y0, 0);
		y1 = std::min (y1, h);

		uint8_t *      buf_ptr = &ctx.at (x0, y0);
		const int      stride  = ctx.get_stride ();
		for (int y = y0; y < y1; ++y)
		{
			*buf_ptr = c;
			buf_ptr += stride;
		}
	}
}



/*
==============================================================================
Name: draw_no_clip
Description:
	Draws a single-pixel thick line in a rendering context.
	Both end coordinates must be located in the rendering context.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1, y1: Coordinates of the last end
	- c: color of the line (pixel intensity), [0 ; 255]
	- term_flag: indicates if we have to draw the last pixel in (x1, y1) or
		if it should be left untouched.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimLine::draw_no_clip (RenderCtx &ctx, int x0, int y0, int x1, int y1, uint8_t c, bool term_flag) noexcept
{
	assert (x0 >= 0);
	assert (x1 >= 0);
	assert (y0 >= 0);
	assert (y1 >= 0);

	const int      w = ctx.get_w ();
	const int      h = ctx.get_h ();
	fstb::unused (w, h);
	assert (x0 < w);
	assert (x1 < w);
	assert (y0 < h);
	assert (y1 < h);

	const int      stride  = ctx.get_stride ();
	const int      dx      =  std::abs (x1 - x0);
	const int      dy      = -std::abs (y1 - y0);
	const int      sx      = (x0 < x1) ? 1      : -1;
	const int      sy_buf  = (y0 < y1) ? stride : -stride;
	int            err     = dx + dy;
	uint8_t *             cur_ptr = &ctx.at (x0, y0);
	const uint8_t * const end_ptr = &ctx.at (x1, y1);

	while (cur_ptr != end_ptr)
	{
		*cur_ptr = c;
		const int      e2 = err * 2;
		if (e2 >= dy)
		{
			err     += dy;
			cur_ptr += sx;
		}
		if (e2 <= dx)
		{
			err     += dx;
			cur_ptr += sy_buf;
		}
	}

	if (term_flag)
	{
		assert (end_ptr == cur_ptr);
		*cur_ptr = c;
	}
}



/*
==============================================================================
Name: draw_aa
Description:
	Draws an antialiased line of selectable tickness in a rendering context.
	Coordinates or any part of the line can be located out of the context,
	the function properly handles the clipping.
	Line color is fixed to 255. Pixels intensities < 255 are written only if
	they are greater than the background.
Input parameters:
	- x0, y0: Coordinates of the first end
	- x1, y1: Coordinates of the last end
	- th: line thickness in pixels, >= 1.
Input/output parameters:
	- ctx: the rendering context where to draw the shape.
==============================================================================
*/

void	PrimLine::draw_aa (RenderCtx &ctx, int x0, int y0, int x1, int y1, double th) noexcept
{
	assert (th >= 1);

 	int            dx  = std::abs (x1 - x0);
	int            dy  = std::abs (y1 - y0);
	int            sx  = (x0 < x1) ? 1 : -1;
	int            sy  = (y0 < y1) ? 1 : -1;

	// Length
	double         e2  = sqrt (double (dx * dx + dy * dy));
	if (e2 == 0)
	{
		ctx.set_pix (x0, y0, 255);
		return;
	}

	// Scale values
	dx = fstb::round_int (dx * 255 / e2);
	dy = fstb::round_int (dy * 255 / e2);
	const int      thi = fstb::round_int (255 * (th - 1));

	// Steep line
	if (dx < dy)
	{
		// Start offset
		x1 = fstb::round_int ((e2 + thi * 0.5) / double (dy));

		// Shift error value to offset width
		int            err = x1 * dy - thi / 2;

		x0 -= x1 * sx;
		while (true)
		{
			x1 = x0;

			// Aliasing pre-pixel
			ctx.max_linear (x1, y0, uint8_t (255 - err));

			// Pixel on the line
			int            e3 = dy - err - thi;
			for ( ; e3 + dy < 255; e3 += dy)
			{
				x1 += sx;
				ctx.set_pix (x1, y0, 255);
			}

			// Aliasing post-pixel
			ctx.max_linear (x1 + sx, y0, uint8_t (255 - e3));

			if (y0 == y1)
			{
				break;
			}

			// y-step
			err += dx;

			// x-step 
			if (err > 255)
			{
				err -= dy;
				x0  += sx;
			}

			y0 += sy;
		}
	}

	// Flat line
	else
	{
		// Start offset
		y1 = fstb::round_int ((e2 + thi * 0.5) / double (dx));

		// Shift error value to offset width
		int            err = y1 * dx - thi / 2;

		y0 -= y1 * sy;
		while (true)
		{
			y1 = y0;

			// Aliasing pre-pixel
			ctx.max_linear (x0, y1, uint8_t (255 - err));

			// Pixel on the line
			int            e3 = dx - err - thi;
			for (; e3 + dx < 255; e3 += dx)
			{
				y1 += sy;
				ctx.set_pix (x0, y1, 255);
			}

			// Aliasing post-pixel
			ctx.max_linear (x0, y1 + sy, uint8_t (255 - e3));

			if (x0 == x1)
			{
				break;
			}

			// x-step 
			err += dy;

			// y-step
			if (err > 255)
			{
				err -= dx;
				y0  += sy;
			}

			x0 += sx;
		} 
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
