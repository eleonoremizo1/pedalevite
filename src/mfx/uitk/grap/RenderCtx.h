/*****************************************************************************

        RenderCtx.h
        Author: Laurent de Soras, 2019

A class wrapping a 8-bit 1-channel pixel buffer.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_grap_RenderCtx_HEADER_INCLUDED)
#define mfx_uitk_grap_RenderCtx_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/uitk/Vec2d.h"

#include <array>

#include <cstdint>



namespace mfx
{
namespace uitk
{
namespace grap
{



class RenderCtx
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       RenderCtx (uint8_t *buf_ptr, const Vec2d &sz, int stride) noexcept;
	               RenderCtx (const RenderCtx &other)  = default;
	               RenderCtx (RenderCtx &&other)       = default;

	virtual        ~RenderCtx ()                       = default;

	RenderCtx &    operator = (const RenderCtx &other) = default;
	RenderCtx &    operator = (RenderCtx &&other)      = default;

	inline uint8_t *
	               use_buf () noexcept;
	inline const uint8_t *
	               use_buf () const noexcept;
	inline const Vec2d &
	               use_size () const noexcept;
	inline int     get_w () const noexcept;
	inline int     get_h () const noexcept;
	inline int     get_stride () const noexcept;

	inline uint8_t &
	               at (int x, int y) noexcept;
	inline const uint8_t &
	               at (int x, int y) const noexcept;

	// Utility things
	typedef std::array <uint8_t, 256> Lut;
	void           fill (uint8_t c) noexcept;
	fstb_FLATINLINE void
	               set_pix (int x, int y, uint8_t c) noexcept;
	fstb_FLATINLINE void
	               max_linear (int x, int y, uint8_t lin) noexcept;

	inline static const Lut &
	               use_lut_l2g () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr Lut
	               make_lut_l2g () noexcept;

	uint8_t *      _buf_ptr;
	Vec2d          _sz;
	int            _stride;

	// Linear to gamma-compressed luminance, 8 bits
	static const Lut
	               _lut_l2g;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               RenderCtx ()                               = delete;
	bool           operator == (const RenderCtx &other) const = delete;
	bool           operator != (const RenderCtx &other) const = delete;

}; // class RenderCtx



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



#include "mfx/uitk/grap/RenderCtx.hpp"



#endif   // mfx_uitk_grap_RenderCtx_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
