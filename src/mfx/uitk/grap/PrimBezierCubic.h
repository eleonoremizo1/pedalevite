/*****************************************************************************

        PrimBezierCubic.h
        Author: Laurent de Soras, 2024

Algorithm from:
Alois Zingl, A Rasterizing Algorithm for Drawing Curves, 2016
https://zingl.github.io/bresenham.html

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_grap_PrimBezierCubic_HEADER_INCLUDED)
#define mfx_uitk_grap_PrimBezierCubic_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"

#include <tuple>

#include <cstdint>



namespace mfx
{
namespace uitk
{
namespace grap
{



class RenderCtx;

class PrimBezierCubic
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static void    draw (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, uint8_t c) noexcept;
	static void    draw_aa (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, double th) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	template <bool INFL, typename F>
	static void    draw_common (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, F fnc_draw_seg);
	static void    draw_seg (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, uint8_t c) noexcept;
	static void    draw_seg_quad (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, uint8_t c) noexcept;
	static void    draw_seg_aa (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double th) noexcept;
	static void    draw_seg_aa_1pix (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3) noexcept;
	static void    draw_seg_aa_final (RenderCtx &ctx, int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double th) noexcept;
	static void    draw_seg_quad_rat_aa (RenderCtx &ctx, int x0, int y0, int x1, int y1, int x2, int y2, double w, double th) noexcept;

	fstb_FLATINLINE static std::tuple <int, int, double, double, double, double, int, int>
	               divide_cubic (int x0, int y0, double x1, double y1, double x2, double y2, int x3, int y3, double t1, double t2) noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               PrimBezierCubic ()                               = delete;
	               PrimBezierCubic (const PrimBezierCubic &other)   = delete;
	               PrimBezierCubic (PrimBezierCubic &&other)        = delete;
	PrimBezierCubic &
	               operator = (const PrimBezierCubic &other)        = delete;
	PrimBezierCubic &
	               operator = (PrimBezierCubic &&other)             = delete;
	bool           operator == (const PrimBezierCubic &other) const = delete;
	bool           operator != (const PrimBezierCubic &other) const = delete;

}; // class PrimBezierCubic



}  // namespace grap
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/grap/PrimBezierCubic.hpp"



#endif // mfx_uitk_grap_PrimBezierCubic_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
