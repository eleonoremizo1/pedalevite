/*****************************************************************************

        NText.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/txt/utf8/Codec8.h"
#include "fstb/Err.h"
#include "fstb/fnc.h"
#include "mfx/pi/param/Tools.h"
#include "mfx/ui/Font.h"
#include "mfx/uitk/NText.h"
#include "mfx/uitk/ParentInterface.h"

#include <algorithm>
#include <limits>

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_frame
Description:
	Sets the minimum frame occupied by the text.
	It is possible to add a margin so the text doesn't start at the top-left
	of the frame.
Input parameters:
	- size_max: frame size in pixels. If a component is 0, the frame size is
		not set in the corresponding dimension. Any component >= 0.
	- margin: position of the text relative to the top-left corner of the
		frame, in pixels. The value is given for top-left justification.
		Any component in [0 ; size_max component].
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_frame (Vec2d size_max, Vec2d margin)
{
	const Rect     zone_old (get_bounding_box () + get_coord ());

	assert (size_max [0] >= 0);
	assert (size_max [1] >= 0);
	assert (margin [0] >= 0);
	assert (margin [1] >= 0);
	assert (margin [0] <= size_max [0]);
	assert (margin [1] <= size_max [1]);

	_frame_size = size_max;
	_margin     = margin;
	update_text (zone_old);
}



/*
==============================================================================
Name: set_text_m
Description:
	Sets the displayed text, as a multilabel string (concatenated labels of
	descending lengths, separated by '\n').
	It uses the frame size as refrence. If the frame size is not defined,
	the longest label is used.
	The multilabel string has precendences and replaces the single text field.
Input parameters:
	- multilabel: a multilabel string encoded in UTF-8.
		The different label versions are separated by a '\n' character.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_text_m (std::string multilabel)
{
	const Rect     zone_old (get_bounding_box () + get_coord ());

	_txt_multi_opt.emplace (multilabel);
	update_text (zone_old);
}



/*
==============================================================================
Name: set_text
Description:
	Sets te displayed text.
	Calling this function automatically removes the multilabel text version.
Input parameters:
	- txt: single-line text encoded in UTF-8
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_text (std::string txt)
{
	const Rect     zone_old (get_bounding_box () + get_coord ());

	_txt_multi_opt.reset ();
	_txt = txt;
	update_text (zone_old);
}



/*
==============================================================================
Name: get_text
Returns: the actual displayed text.
	If the text is a multilabel string, this is the selected label.
Throws: std::string related exceptions
==============================================================================
*/

std::string	NText::get_text () const
{
	return _txt;
}



/*
==============================================================================
Name: set_font
Description:
	Sets the font for the text.
	The font must be set to display anything.
Input parameters:
	- fnt: a reference on the font. The object should be persistent until
		the font is changed.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_font (const ui::Font &fnt)
{
	const Rect     zone_old (get_bounding_box () + get_coord ());

	_font_ptr = &fnt;
	update_text (zone_old);
}



/*
==============================================================================
Name: set_mag
Description:
	Sets the magnification factor, allowing to display large characters.
	Magnification is done by simple pixel replication.
Input parameters:
	- x: Horizontal scale factor, >= 1
	- y: Vertical scale factor, >= 1
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_mag (int x, int y)
{
	assert (x > 0);
	assert (y > 0);

	const Rect     zone_old (get_bounding_box () + get_coord ());

	_mag_arr [0] = x;
	_mag_arr [1] = y;
	update_text (zone_old);
}



/*
==============================================================================
Name: set_justification
Description:
	Sets the text justification, meaning where the reference coordinates is
	located within the text block. The text block includes frame margins at
	the top-left.
	(0, 0) is top-left of the text and (1, 1) is bottom-right. (0.5, 0.5) is
	center-jusitified.
	It is possible to align the text on the font baseline.
Input parameters:
	- x: horizontal position of the reference point, in [0 ; 1].
	- y: vertical position of the reference point, in [0 ; 1].
	- baseline_flag: vertical justification on the font baseline. Overrides
		the y value.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_justification (float x, float y, bool baseline_flag)
{
	assert (x >= 0);
	assert (x <= 1);
	assert (y >= 0);
	assert (y <= 1);

	const Rect     zone_old (get_bounding_box () + get_coord ());

	_justification [0] = x;
	_justification [1] = y;
	_baseline_flag     = baseline_flag;
	update_content ();

	ParentInterface * parent_ptr = get_parent ();
	if (parent_ptr != nullptr)
	{
		parent_ptr->invalidate (zone_old);
	}
}



/*
==============================================================================
Name: set_bold
Description:
	Uses a (fake) bold face by duplicating a pixel column for each character.
Input parameters:
	- bold_flag: indicates that bold face is activated.
	- space_flag: when using bold, adds one or more pixels after each character
		to compensate for the extra column and maintain the kerning. If false,
		the global text width is the same as the regular font face.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_bold (bool bold_flag, bool space_flag)
{
	const Rect     zone_old (get_bounding_box () + get_coord ());

	_bold_flag  = bold_flag;
	_space_flag = space_flag;
	update_text (zone_old);
}



/*
==============================================================================
Name: is_bold
Returns: true if the text is dispayed in bold face.
==============================================================================
*/

bool	NText::is_bold () const noexcept
{
	return _bold_flag;
}



/*
==============================================================================
Name: has_space_in_bold
Returns: true if the pitch between characters has an extra pixel when the font
	is in bold. The returned value is not dependent on the actual bold setting.
==============================================================================
*/

bool	NText::has_space_in_bold () const noexcept
{
	return _space_flag;
}



/*
==============================================================================
Name: set_vid_inv
Description:
	Displays the text in video inverse by default.
	When the cursor is active on a video inverted text, only the frame outline
	is inverted.
Input parameters:
	- vid_inv_flag: true for video inversion.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_vid_inv (bool vid_inv_flag)
{
	_vid_inv_flag = vid_inv_flag;

	update_content ();
}



/*
==============================================================================
Name: set_underline
Description:
	Activates or deactivate text underline. This is a single-pixel thick
	horizontal line at the bottom of the case.
Input parameters:
	- underline_flag: true for underlining.
Throws: std::vector and std::string related exceptions
==============================================================================
*/

void	NText::set_underline (bool underline_flag)
{
	_underline_flag = underline_flag;

	update_content ();
}



/*
==============================================================================
Name: get_char_width
Description:
	Computes the width of a character in displayed pixels.
	Requires the font to be set.
	A default value is returned for unmapped characters, depending on the font
	implementation.
Input parameters:
	- c: character coded in UCS-4.
Returns: character width in pixels, >= 0.
==============================================================================
*/

int	NText::get_char_width (char32_t c) const noexcept
{
	assert (_font_ptr != nullptr);

	int            len = _font_ptr->get_char_w (c);
	if (_bold_flag && _space_flag)
	{
		len += _font_ptr->get_bold_shift ();
	}
	len *= _mag_arr [0];

	return len;
}



/*
==============================================================================
Name: get_active_width
Returns: the width of the frame in pixels, without the margin.
==============================================================================
*/

int	NText::get_active_width () const noexcept
{
	return (_frame_size - _margin) [0];
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NText::redraw_cursor (ui::DisplayInterface &disp, Rect bitmap_abs)
{
	fstb::unused (disp, bitmap_abs);

	// Bypasses the default implementation. Here, the cursor is directly
	// rendered in the cached bitmap by update_content().
}



Rect	NText::do_get_bounding_box () const noexcept
{
	return Inherited::do_get_bounding_box () + _origin;
}



void	NText::do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d node_coord)
{
	Inherited::do_redraw (disp, clipbox, node_coord + _origin);
}



MsgHandlerInterface::EvtProp	NText::do_handle_evt (const NodeEvt &evt)
{
	const auto       ret_val = Inherited::do_handle_evt (evt);

	if (evt.get_target () == get_id ())
	{
		const NodeEvt::Type  type = evt.get_type ();
		if (type == NodeEvt::Type_CURSOR)
		{
			update_content ();
		}
	}

	return ret_val;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NText::update_text (const Rect &zone_old)
{
	update_full_text ();
	update_txt_ucs4 ();
	update_content ();

	ParentInterface * parent_ptr = get_parent ();
	if (parent_ptr != nullptr)
	{
		parent_ptr->invalidate (zone_old);
	}
}



void	NText::update_full_text ()
{
	if (_txt_multi_opt && _font_ptr != nullptr)
	{
		auto           width = size_t (get_active_width ());
		if (width <= 0)
		{
			width = std::numeric_limits <size_t>::max ();
		}
		else
		{
			width = fstb::div_ceil (width, size_t (_mag_arr [0]));
		}

		const auto &   txt_multi = *_txt_multi_opt;
		_txt = pi::param::Tools::print_name_bestfit (
			width, txt_multi.c_str (), *this, &NText::get_char_width
		);
	}
}



void	NText::update_txt_ucs4 ()
{
	_txt_ucs4.clear ();
	size_t         pos_utf8 = 0;
	int            ret_val  = fstb::Err_OK;
	while (pos_utf8 < _txt.size () && ret_val == fstb::Err_OK)
	{
		int            len_utf8 = 1;
		ret_val = fstb::txt::utf8::Codec8::get_char_seq_len_utf (
			len_utf8, _txt [pos_utf8]
		);

		if (ret_val == fstb::Err_OK)
		{
			char32_t       c_ucs4;
			ret_val = fstb::txt::utf8::Codec8::decode_char (
				c_ucs4, _txt.c_str () + pos_utf8
			);

			_txt_ucs4 += c_ucs4;
			pos_utf8  += len_utf8;
		}
	}

	assert (ret_val == fstb::Err_OK);
}



void	NText::update_content ()
{
	// First, finds the string size in pixels
	int            w_pix = 0;
	int            h_pix = 0;
	if (_font_ptr != nullptr)
	{
		w_pix = compute_width_pix ();
		h_pix = _font_ptr->get_char_h () * _mag_arr [1];
	}

	// The frame size
	const int      fw_pix = std::max (w_pix + _margin [0], _frame_size [0]);
	const int      fh_pix = std::max (h_pix + _margin [1], _frame_size [1]);

	if (fw_pix == 0 || fh_pix == 0)
	{
		set_size (Vec2d ());
		return;
	}

	set_size (Vec2d (fw_pix, fh_pix));

	// Sets the origin
	_origin = Vec2d (
		-fstb::round_int (float (fw_pix) * _justification [0]),
		-fstb::round_int (float (fh_pix) * _justification [1])
	);
	if (_font_ptr != nullptr && _baseline_flag)
	{
		_origin [1] = -_font_ptr->get_baseline () * _mag_arr [1];
	}

	// Margins from the top-left corner
	const int      margin_x = fstb::round_int (
		  float (_margin [0]   ) * (1 - _justification [0])
		+ float (fw_pix - w_pix) *      _justification [0]
	);
	const int      margin_y = fstb::round_int (
		  float (_margin [1]   ) * (1 - _justification [1])
		+ float (fh_pix - h_pix) *      _justification [1]
	);

	uint8_t *      buf_ptr = use_buffer ();
	const int      stride  = get_stride ();

	// Cleans the buffer if required
	if (   (_bold_flag && _space_flag)
	    || fw_pix > w_pix
	    || fh_pix > h_pix)
	{
		std::fill (buf_ptr, buf_ptr + stride * fh_pix, uint8_t (0));
	}

	if (_font_ptr != nullptr)
	{
		// Renders the string
		{
			uint8_t *      buf2_ptr = buf_ptr + stride * margin_y;
			int            x = margin_x;
			for (auto c : _txt_ucs4)
			{
				_font_ptr->render_char (
					buf2_ptr + x, c, stride, _mag_arr [0], _mag_arr [1]
				);
				x += get_char_width (c);
			}
		}

		// Bold
		if (_bold_flag)
		{
			int            b_sh     = _font_ptr->get_bold_shift ();
			const int      nbr_dup  = (b_sh >= 2) ? 2 : 1;
			for (int dup = 0; dup < nbr_dup; ++dup)
			{
				const int      mag_x    = _mag_arr [0];
				const int      shift_x  = b_sh * mag_x * (dup + 1) / nbr_dup;
				uint8_t *      buf2_ptr = buf_ptr + stride * margin_y;
				for (int y = 0; y < h_pix; ++y)
				{
					for (int x = margin_x + w_pix - 1; x >= margin_x + shift_x; -- x)
					{
						buf2_ptr [x] = std::max (buf2_ptr [x], buf2_ptr [x - shift_x]);
					}
					buf2_ptr += stride;
				}
			}
		}

		// Underline
		if (_underline_flag)
		{
			const int      mag_y     = _mag_arr [1];
			const int      thickness = (mag_y + 1) >> 1;
			const int      y         = margin_y + h_pix - thickness;
			const auto     zone_ptr  = buf_ptr + y * w_pix + margin_x;
			std::fill (zone_ptr, zone_ptr + stride * thickness, uint8_t (255));
		}
	}

	// Video inverse
	if (_vid_inv_flag != is_cursor_displayed ())
	{
		NodeBase::invert_zone (buf_ptr, fw_pix, fh_pix, stride);
	}

	// Double video inverse: inverts just the outline
	else if (   _vid_inv_flag && is_cursor_displayed ()
	         && fw_pix >= 2 && fh_pix >= 2)
	{
		// Top
		NodeBase::invert_zone (buf_ptr, fw_pix, 1, stride);

		// Left
		NodeBase::invert_zone (buf_ptr + stride, 1, fh_pix - 2, stride);

		// Right
		NodeBase::invert_zone (buf_ptr + stride + fw_pix - 1, 1, fh_pix - 2, stride);

		// Bottom
		NodeBase::invert_zone (buf_ptr + stride * (fh_pix - 1), fw_pix, 1, stride);
	}

	invalidate_all ();
}



// Returns the text width in pixels
int	NText::compute_width_pix () const
{
	int            w_pix = 0;
	for (auto c : _txt_ucs4)
	{
		w_pix += get_char_width (c);
	}

	return w_pix;
}



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
