/*****************************************************************************

        NodeBase.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/bit_cast.h"
#include "fstb/def.h"
#include "mfx/uitk/ParentInterface.h"
#include "mfx/uitk/NodeBase.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Input parameters:
	- node_id: a unique identifier in the tree, >= 0.
==============================================================================
*/

NodeBase::NodeBase (int node_id) noexcept
:	_node_id (node_id)
{
	assert (node_id >= 0);
}



/*
==============================================================================
Name: set_node_id
Description:
	Sets or changed the node unique identifier.
Input parameters:
	- node_id: a unique identifier in the tree, >= 0.
==============================================================================
*/

void	NodeBase::set_node_id (int node_id) noexcept
{
	assert (node_id >= 0);

	_node_id = node_id;
}



/*
==============================================================================
Name: get_parent
Returns: A pointer on the attached parent or nullptr if the leaf is detached.
==============================================================================
*/

ParentInterface *	NodeBase::get_parent () const noexcept
{
	return _parent_ptr;
}



/*
==============================================================================
Name: set_coord
Description:
	Sets the node reference coordinates, relative to its parent.
Input parameters:
	- pos: coordinates.
Throws: ?
==============================================================================
*/

void	NodeBase::set_coord (Vec2d pos)
{
	const Rect     bbox (get_bounding_box ());
	const Rect     zone_old (bbox + _coord);
	_coord = pos;
	const Rect     zone_new (bbox + _coord);

	if (_parent_ptr != nullptr)
	{
		_parent_ptr->invalidate (zone_old);
		_parent_ptr->invalidate (zone_new);
	}
}



/*
==============================================================================
Name: invalidate_all
Description:
	Indicates to the parent that the area covered by the node should be
	redrawn.
Throws: ?
==============================================================================
*/

void	NodeBase::invalidate_all ()
{
	ParentInterface * parent_ptr = get_parent ();
	if (parent_ptr != nullptr)
	{
		Rect           zone (do_get_bounding_box ());
		zone += get_coord ();
		parent_ptr->invalidate (zone);
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// stride in pixels
void	NodeBase::invert_zone (uint8_t *buf_ptr, int w, int h, int stride)
{
	assert (w >= 0);
	assert (h >= 0);

	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			buf_ptr [x] = uint8_t (~buf_ptr [x]);
		}

		buf_ptr += stride;
	}
}



// stride in pixels
void	NodeBase::invert_zone (ui::PixArgb *buf_ptr, int w, int h, int stride)
{
	assert (w >= 0);
	assert (h >= 0);

	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
#if 0 // Reference code
			buf_ptr [x]._b = uint8_t (~buf_ptr [x]._b);
			buf_ptr [x]._g = uint8_t (~buf_ptr [x]._g);
			buf_ptr [x]._r = uint8_t (~buf_ptr [x]._r);
#else
			buf_ptr [x] = fstb::bit_cast <ui::PixArgb> (
				fstb::bit_cast <int32_t> (buf_ptr [x]) ^ 0x00FFFFFF
			);
#endif
		}

		buf_ptr += stride;
	}
}



void	NodeBase::do_notify_attachment (ParentInterface *cont_ptr)
{
	_parent_ptr = cont_ptr;
}



int	NodeBase::do_get_id () const noexcept
{
	return _node_id;
}



Vec2d	NodeBase::do_get_coord () const noexcept
{
	return _coord;
}



MsgHandlerInterface::EvtProp	NodeBase::do_handle_evt (const NodeEvt &evt)
{
	fstb::unused (evt);

	return EvtProp_PASS;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
