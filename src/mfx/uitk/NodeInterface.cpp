/*****************************************************************************

        NodeInterface.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/NodeInterface.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: notify_attachment
Description:
	Informs the node that it has been attached to a parent node, or detached.
Input/output parameters:
	- cont_ptr: pointer on the new parent, or nullptr if it has been detached.
Throws: ?
==============================================================================
*/

void	NodeInterface::notify_attachment (ParentInterface *cont_ptr)
{
	do_notify_attachment (cont_ptr);
}



/*
==============================================================================
Name: get_id
Returns: the unique node identifier, >= 0.
==============================================================================
*/

int	NodeInterface::get_id () const noexcept
{
	const int      ret_val = do_get_id ();
	assert (ret_val >= 0);

	return ret_val;
}



/*
==============================================================================
Name: get_coord
Returns: the node reference coordinates, relative to the parent.
==============================================================================
*/

Vec2d	NodeInterface::get_coord () const noexcept
{
	return do_get_coord ();
}



/*
==============================================================================
Name: get_bounding_box
Returns: a rectangle that should include the whole area covered by the node.
	The rectangle is relative to the internal coordinates given by get_coord().
==============================================================================
*/

Rect	NodeInterface::get_bounding_box () const noexcept
{
	return do_get_bounding_box ();
}



/*
==============================================================================
Name: redraw
Description:
	Draws the node content onto the provided background.
Input parameters:
	- clipbox: area from disp that should be redrawn.
	- parent_coord: absolute coordinates of the parent node (relative to disp).
Input/output parameters:
	- disp: where to display the node.
Throws: ?
==============================================================================
*/

void	NodeInterface::redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord)
{
	do_redraw (disp, clipbox, parent_coord);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
