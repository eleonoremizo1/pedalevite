/*****************************************************************************

        NBitmapM.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"
#include "mfx/uitk/NBitmapM.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/Rect.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NBitmapM::set_color (ui::PixArgb col) noexcept
{
	_color = col;
}



ui::PixArgb	NBitmapM::get_color () const noexcept
{
	return _color;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NBitmapM::do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord)
{
	redraw_base (
		disp, clipbox, parent_coord,
		[&] (Rect bitmap_abs, Rect bitmap_rel, Vec2d disp_size)
		{
			disp.bitblt (
				bitmap_abs [0] [0], bitmap_abs [0] [1],
				use_buffer (),
				bitmap_rel [0] [0], bitmap_rel [0] [1],
				disp_size [0], disp_size [1],
				get_size () [0],
				get_blend_mode (), get_color ()
			);
		}
	);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
