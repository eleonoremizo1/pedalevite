/*****************************************************************************

        NBitmap.h
        Author: Laurent de Soras, 2024

Color bitmap

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_NBitmap_HEADER_INCLUDED)
#define mfx_uitk_NBitmap_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"
#include "mfx/ui/PixArgb.h"
#include "mfx/uitk/NBitmapBase.h"



namespace mfx
{
namespace uitk
{



class NBitmap
:	public NBitmapBase <ui::PixArgb>
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef NBitmapBase <ui::PixArgb> Inherited;

	using Inherited::Inherited;

	NBitmap &      operator = (const NBitmap &other) = default;
	NBitmap &      operator = (NBitmap &&other)      = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::NodeInterface via mfx::uitk::NodeBase
	void           do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord) override;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const NBitmap &other) const = delete;
	bool           operator != (const NBitmap &other) const = delete;

}; // class NBitmap



}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/NBitmap.hpp"



#endif // mfx_uitk_NBitmap_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
