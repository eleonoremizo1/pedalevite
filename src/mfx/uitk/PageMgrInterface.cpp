/*****************************************************************************

        PageMgrInterface.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/PageMgrInterface.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	PageMgrInterface::set_nav_layout (const NavLocList &nav_list, int jump_node_id)
{
	do_set_nav_layout (nav_list, jump_node_id);
}



void	PageMgrInterface::jump_to (int node_id)
{
	assert (node_id >= 0);

	do_jump_to (node_id);
}



void	PageMgrInterface::set_timer (int node_id, bool enable_flag)
{
	assert (node_id >= 0);

	do_set_timer (node_id, enable_flag);
}



bool	PageMgrInterface::get_shift (Shift key) const
{
	return do_get_shift (key);
}



void	PageMgrInterface::set_page_step (int step)
{
	assert (step > 0);

	do_set_page_step (step);
}



void	PageMgrInterface::reset_display ()
{
	do_reset_display ();
}



void	PageMgrInterface::add_nav (NavLocList &nll, int node_id)
{
	nll.push_back (NavLoc (node_id));
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
