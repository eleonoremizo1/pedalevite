/*****************************************************************************

        NBitmapBase.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_NBitmapBase_CODEHEADER_INCLUDED)
#define mfx_uitk_NBitmapBase_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/Rect.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_size
Description:
	Sets the bitmap size in pixels.
Input parameters:
	- sz: Size in pixels, any component >= 0. If a component is 0, there is
		nothing to display but this is valid.
Throws: std::vector related exceptions
==============================================================================
*/

template <typename PT>
void	NBitmapBase <PT>::set_size (Vec2d sz)
{
	assert (sz [0] >= 0);
	assert (sz [1] >= 0);

	_size = sz;
	_buffer.resize (_size [0] * _size [1]);

	// Don't invalidate anything now because the buffer may contain garbage
}



/*
==============================================================================
Name: get_size
Returns: the bitmap size in pixels, any component >= 0.
==============================================================================
*/

template <typename PT>
Vec2d	NBitmapBase <PT>::get_size () const noexcept
{
	return _size;
}



/*
==============================================================================
Name: show
Description:
	Activates or deactivates bitmap display. Cursor is dependent on this
	parameter.
Input parameters:
	- flag: the bitmap is displayed if true.
Throws: ?
==============================================================================
*/

template <typename PT>
void	NBitmapBase <PT>::show (bool flag)
{
	_show_flag   = flag;
	_cursor_flag = false;
	invalidate_all ();
}



/*
==============================================================================
Name: set_blend_mode
Description:
	Sets how the bitmap is merged with the background during display.
	Opaque is the default.
Input parameters:
	- mode: one of the valid ui::DisplayInterface::BlendMode values.
Throws: ?
==============================================================================
*/

template <typename PT>
void	NBitmapBase <PT>::set_blend_mode (ui::DisplayInterface::BlendMode mode)
{
	assert (mode >= 0);
	assert (mode < ui::DisplayInterface::BlendMode_NBR_ELT);

	_blend_mode = mode;
	invalidate_all ();
}



/*
==============================================================================
Name: get_blend_mode
Returns: the current blend mode.
==============================================================================
*/

template <typename PT>
ui::DisplayInterface::BlendMode	NBitmapBase <PT>::get_blend_mode () const noexcept
{
	return _blend_mode;
}



/*
==============================================================================
Name: use_buffer
Description:
	Returns a pointer on the buffer containing the picture, on the top-left
	pixel.
	Buffer is arranged in rows from top to bottom, the pitch between
	consecutive pixels of each column is obtained with get_stride().
Returns: pointer on the buffer
==============================================================================
*/

template <typename PT>
const PT *	NBitmapBase <PT>::use_buffer () const noexcept
{
	assert (_size [0] > 0);
	assert (_size [1] > 0);

	return _buffer.data ();
}



template <typename PT>
PT *	NBitmapBase <PT>::use_buffer () noexcept
{
	assert (_size [0] > 0);
	assert (_size [1] > 0);

	return _buffer.data ();
}



/*
==============================================================================
Name: get_stride
Returns: the distance between two consecutive pixels of the same column, in
	pixels. It is greater or equal to the picture width.
==============================================================================
*/

template <typename PT>
int	NBitmapBase <PT>::get_stride () const noexcept
{
	assert (_size [0] > 0);
	assert (_size [1] > 0);

	return _size [0];
}



/*
==============================================================================
Name: has_cursor
Description:
	Indicates that the cursor is currently located on this widget.
Returns: true if the widget hosts the cursor, even if not displayed.
==============================================================================
*/

template <typename PT>
bool	NBitmapBase <PT>::has_cursor () const noexcept
{
	return _cursor_flag;
}



/*
==============================================================================
Name: show_cursor
Description:
	Enable or disable cursor display.
Input parameters:
	- flag: true if the cursor should be rendered.
Throws: ?
==============================================================================
*/

template <typename PT>
void	NBitmapBase <PT>::show_cursor (bool flag)
{
	std::swap (_show_cursor_flag, flag);
	if (_cursor_flag && flag)
	{
		invalidate_all ();
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename PT>
bool	NBitmapBase <PT>::is_cursor_displayed () const noexcept
{
	return (_cursor_flag && _show_cursor_flag);
}



template <typename PT>
void	NBitmapBase <PT>::redraw_cursor (ui::DisplayInterface &disp, Rect bitmap_abs)
{
	invert_zone (disp, bitmap_abs);
}



template <typename PT>
void	NBitmapBase <PT>::do_notify_attachment (ParentInterface *cont_ptr)
{
	Inherited::do_notify_attachment (cont_ptr);
	if (cont_ptr == nullptr)
	{
		_cursor_flag = false;
	}
}



template <typename PT>
Rect	NBitmapBase <PT>::do_get_bounding_box () const noexcept
{
	return Rect (Vec2d (), _size);
}



template <typename PT>
template <typename F>
void	NBitmapBase <PT>::redraw_base (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord, F blit_fnc)
{
	if (_show_flag)
	{
		Rect           bitmap_abs (Vec2d (), _size);
		const Vec2d    node_coord (parent_coord + get_coord ());
		bitmap_abs += node_coord;
		bitmap_abs.intersect (clipbox);
		if (! bitmap_abs.is_empty ())
		{
			assert (! _buffer.empty ());

			const Rect     bitmap_rel (bitmap_abs - node_coord);
			const Vec2d    disp_size (bitmap_abs.get_size ());

			blit_fnc (bitmap_abs, bitmap_rel, disp_size);

			if (is_cursor_displayed ())
			{
				redraw_cursor (disp, bitmap_abs);
			}
		}
	}
}



template <typename PT>
MsgHandlerInterface::EvtProp	NBitmapBase <PT>::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	if (evt.get_target () == get_id ())
	{
		const NodeEvt::Type  type = evt.get_type ();
		if (type == NodeEvt::Type_CURSOR)
		{
			const NodeEvt::Curs  curs = evt.get_cursor ();
			_cursor_flag = (curs == NodeEvt::Curs_ENTER);
			if (_show_cursor_flag)
			{
				invalidate_all ();
			}

			ret_val = EvtProp_CATCH;
		}
	}

	return ret_val;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename PT>
void	NBitmapBase <PT>::invert_zone (ui::DisplayInterface &disp, Rect zone)
{
	const Vec2d    disp_size (zone.get_size ());
	const int      stride  = disp.get_stride ();
	ui::PixArgb *  dst_ptr = disp.use_screen_buf ();

	dst_ptr += zone [0] [1] * stride + zone [0] [0];

	NodeBase::invert_zone (dst_ptr, disp_size [0], disp_size [1], stride);
}



}  // namespace uitk
}  // namespace mfx



#endif // mfx_uitk_NBitmapBase_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
