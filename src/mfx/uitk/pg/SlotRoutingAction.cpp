/*****************************************************************************

        SlotRoutingAction.cpp
        Author: Laurent de Soras, 2020

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/piapi/PluginDescInterface.h"
#include "mfx/ui/Font.h"
#include "mfx/uitk/pg/SlotRoutingAction.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/PageSwitcher.h"
#include "mfx/LocEdit.h"
#include "mfx/Model.h"
#include "mfx/View.h"

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



SlotRoutingAction::SlotRoutingAction (PageSwitcher &page_switcher, LocEdit &loc_edit)
:	_page_switcher (page_switcher)
,	_loc_edit (loc_edit)
,	_model_ptr (nullptr)
,	_view_ptr (nullptr)
,	_page_ptr (nullptr)
,	_page_size ()
,	_fnt_ptr (nullptr)
,	_arg_ptr (nullptr)
,	_menu_sptr (std::make_shared <NWindow> (Entry_WINDOW ))
,	_tit_sptr ( std::make_shared <NText  > (Entry_TITLE  ))
,	_del_sptr ( std::make_shared <NText  > (Entry_DEL    ))
,	_rep_sptr ( std::make_shared <NText  > (Entry_REPLACE))
,	_cnx_end_arr ()
{
	_tit_sptr->set_justification (0.5, 0, false);
	_del_sptr->set_text ("Delete");
	_rep_sptr->set_text ("Replace with:");

	_menu_sptr->set_autoscroll (true);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	SlotRoutingAction::do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt)
{
	assert (usr_ptr != nullptr);

	_model_ptr = &model;
	_view_ptr  = &view;
	_page_ptr  = &page;
	_page_size = page_size;
	_fnt_ptr   = &fnt._m;
	_arg_ptr   = static_cast <const RoutCom::Arg *> (usr_ptr);

	const int      scr_w = _page_size [0];

	_menu_sptr->set_size (_page_size, Vec2d ());
	_menu_sptr->set_disp_pos (Vec2d ());

	switch (_arg_ptr->_ed_type)
	{
	case RoutCom::Arg::EdType_CNX:
		_tit_sptr->set_text ("Edit connection");
		break;
	case RoutCom::Arg::EdType_PIN:
		_tit_sptr->set_text ("Add connection");
		break;
	default:
		assert (false);
		break;
	}

	_tit_sptr->set_font (fnt._l);
	_tit_sptr->set_coord (Vec2d (scr_w >> 1, 0));
	_tit_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
	_page_ptr->push_back (_menu_sptr);

	update_display ();
}



void	SlotRoutingAction::do_disconnect ()
{
	_arg_ptr = nullptr;
}



PageInterface::EvtProp	SlotRoutingAction::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const int      node_id = evt.get_target ();

	if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			ret_val = EvtProp_CATCH;
			if (   node_id >= RoutCom::_entry_tgt_base
			    && node_id <  RoutCom::_entry_tgt_base
			                + int (_cnx_end_arr.size ()))
			{
				assert (_loc_edit._slot_id >= 0);
				const ToolsRouting::Node   graph_node (
					doc::CnxEnd::Type_NORMAL, _loc_edit._slot_id
				);
				const auto     slot_id_opt = RoutCom::add_or_replace_cnx (
					node_id, graph_node, _cnx_end_arr,
					*_arg_ptr, *_model_ptr, *_view_ptr
				);
				if (node_id == RoutCom::_entry_new_slot)
				{
					assert (slot_id_opt.has_value ());
					_loc_edit._slot_id = *slot_id_opt;
					_page_switcher.switch_to (PageType_SLOT_MENU, nullptr);
				}
			}
			else
			{
				switch (node_id)
				{
				case Entry_DEL:
					RoutCom::del_cnx (*_arg_ptr, *_model_ptr, *_view_ptr);
					break;
				default:
					ret_val = EvtProp_PASS;
					break;
				}
			}
			break;
		case Button_E:
			_page_switcher.switch_to (PageType_SLOT_ROUTING, nullptr);
			break;
		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



void	SlotRoutingAction::do_activate_prog (int index)
{
	fstb::unused (index);

	_page_switcher.switch_to (pg::PageType_PROG_EDIT, nullptr);
}



void	SlotRoutingAction::do_set_chn_mode (ChnMode mode)
{
	fstb::unused (mode);

	// Available I/O pins might change
	update_display ();
}



void	SlotRoutingAction::do_set_layer_edit (doc::LayerType layer_edit)
{
	fstb::unused (layer_edit);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	SlotRoutingAction::do_remove_slot (int slot_id)
{
	if (slot_id == _loc_edit._slot_id)
	{
		_loc_edit._slot_id = -1;
		_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
	}
	else
	{
		update_display ();
	}
}



void	SlotRoutingAction::do_set_routing (const doc::Routing &routing)
{
	fstb::unused (routing);

	_page_switcher.switch_to (pg::PageType_SLOT_ROUTING, nullptr);
}



void	SlotRoutingAction::do_set_plugin (int slot_id, const PluginInitData &pi_data)
{
	fstb::unused (pi_data);

	if (slot_id == _loc_edit._slot_id)
	{
		_page_switcher.switch_to (pg::PageType_SLOT_ROUTING, nullptr);
	}
	else
	{
		update_display ();
	}
}



void	SlotRoutingAction::do_remove_plugin (int slot_id)
{
	if (slot_id == _loc_edit._slot_id)
	{
		_page_switcher.switch_to (pg::PageType_SLOT_ROUTING, nullptr);
	}
	else
	{
		update_display ();
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	SlotRoutingAction::update_display ()
{
	assert (_arg_ptr != nullptr);

	PageMgrInterface::NavLocList  nav_list;

	const int      scr_w = _page_size [0];
	const int      h_m   = _fnt_ptr->get_char_h ();
	const auto     blend_mode = ui::DisplayInterface::BlendMode_OPAQUE;

	_menu_sptr->clear_all_nodes ();
	_menu_sptr->push_back (_tit_sptr);

	int            pos_y = h_m * 2;
	if (_arg_ptr->_ed_type == RoutCom::Arg::EdType_CNX)
	{
		_del_sptr->set_font (*_fnt_ptr);
		_rep_sptr->set_font (*_fnt_ptr);

		_del_sptr->set_coord (Vec2d (0, pos_y));
		_rep_sptr->set_coord (Vec2d (0, pos_y + h_m));

		_del_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		_rep_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());

		_menu_sptr->push_back (_del_sptr);
		_menu_sptr->push_back (_rep_sptr);

		PageMgrInterface::add_nav (nav_list, Entry_DEL);

		pos_y += h_m * 2;
	}

	assert (_loc_edit._slot_id >= 0);
	const auto     start =
		ToolsRouting::Node (doc::CnxEnd::Type_NORMAL, _loc_edit._slot_id);

	RoutCom::build_possible_cnx_set (
		_cnx_end_arr, *_menu_sptr, nav_list, pos_y, start,
		*_arg_ptr, *_fnt_ptr, scr_w, blend_mode, *_model_ptr, *_view_ptr
	);

	_page_ptr->set_nav_layout (nav_list);

	_menu_sptr->invalidate_all ();
}



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
