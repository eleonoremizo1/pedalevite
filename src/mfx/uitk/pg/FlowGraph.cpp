/*****************************************************************************

        FlowGraph.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "mfx/pi/param/Tools.h"
#include "mfx/ui/Font.h"
#include "mfx/uitk/grap/PrimBox.h"
#include "mfx/uitk/grap/PrimLine.h"
#include "mfx/uitk/grap/PrimBezierCubic.h"
#include "mfx/uitk/grap/RenderCtx.h"
#include "mfx/uitk/pg/FlowGraph.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/uitk/PageSwitcher.h"
#include "mfx/LocEdit.h"
#include "mfx/Model.h"
#include "mfx/View.h"

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



FlowGraph::FlowGraph (PageSwitcher &page_switcher, LocEdit &loc_edit)
:	_page_switcher (page_switcher)
,	_loc_edit (loc_edit)
,	_win_graph_sptr (std::make_shared <NWindow > (Entry_WIN_GRAPH))
,	_win_nodes_sptr (std::make_shared <NWindow > (Entry_WIN_NODES))
,	_win_menu_ctn_sptr (std::make_shared <NWindow> (Entry_WIN_MENU_CTN))
,	_win_menu_sptr  (std::make_shared <NWindow > (Entry_WIN_MENU ))
,	_cnx_aud_m_sptr (std::make_shared <NBitmapM> (Entry_CNX_AUD_M))
,	_cnx_aud_l_sptr (std::make_shared <NBitmapM> (Entry_CNX_AUD_L))
,	_cnx_aud_r_sptr (std::make_shared <NBitmapM> (Entry_CNX_AUD_R))
,	_cnx_sig_sptr (  std::make_shared <NBitmapM> (Entry_CNX_SIG  ))
,	_cursor_sptr (   std::make_shared <NBitmapM> (Entry_CURS     ))
,	_highlight_sptr (std::make_shared <NBitmapM> (Entry_HILITE   ))
,	_menu_bkg_sptr  (std::make_shared <NBitmapM> (Entry_MENU_BKG ))
,	_node_name_sptr (std::make_shared <NText   > (Entry_NODE_NAME))
,	_edit_fx_sptr (  std::make_shared <NText   > (Entry_EDIT_FX  ))
,	_del_cnx_sptr (  std::make_shared <NText   > (Entry_DEL_CNX  ))
,	_ins_slot_sptr ( std::make_shared <NText   > (Entry_INS_SLOT ))
,	_rep_cnx_sptr (  std::make_shared <NText   > (Entry_REP_CNX  ))
{
	_cnx_aud_m_sptr->set_color (_col_vlgrey);
	_cnx_aud_l_sptr->set_color (_col_red);
	_cnx_aud_r_sptr->set_color (_col_blue);
	_cnx_sig_sptr  ->set_color (_col_green);

	_cursor_sptr   ->set_color (_col_white);
	_highlight_sptr->set_color (_col_magenta);
	_highlight_sptr->show (false);

	// Fixed hyerarchy
	for (const auto &cnx_sptr :
		{ _cnx_sig_sptr, _cnx_aud_r_sptr, _cnx_aud_l_sptr, _cnx_aud_m_sptr }
	)
	{
		// Adds the layers to the window (order is important)
		_win_graph_sptr->push_back (cnx_sptr);
	}
	_win_graph_sptr->push_back (_highlight_sptr);
	_win_graph_sptr->push_back (_win_nodes_sptr);
	_win_graph_sptr->push_back (_cursor_sptr);

	_win_menu_ctn_sptr->push_back (_menu_bkg_sptr);
	_win_menu_ctn_sptr->push_back (_win_menu_sptr);

	_node_name_sptr->set_justification (0.5f, 0, false);
	_node_name_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_node_name_sptr->set_color (_col_grey);

	_edit_fx_sptr->set_text_m ("Edit FX\xE2\x80\xA6\nEdit\xE2\x80\xA6");
	_edit_fx_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_del_cnx_sptr->set_text_m ("Delete\nDel");
	_del_cnx_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_ins_slot_sptr->set_text_m ("Insert new FX\nInsert FX\nIns FX");
	_ins_slot_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_rep_cnx_sptr->set_text_m ("Replace with:\nReplace:\nRepl:");
	_rep_cnx_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_rep_cnx_sptr->set_color (_col_grey);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	FlowGraph::do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt)
{
	fstb::unused (usr_ptr);

	_model_ptr = &model;
	_view_ptr  = &view;
	_page_ptr  = &page;
	_page_size = page_size;
	_fs_ptr    = &fnt;

	_ui_state  = UiState::graph;

	build_cursor ();
	build_highlight ();

	auto           win_pos = Vec2d (0, 0);
	_win_graph_sptr->set_size (_page_size - win_pos, Vec2d ());
	_win_graph_sptr->set_coord (win_pos);
	_win_graph_sptr->set_disp_pos (Vec2d ());

	_win_nodes_sptr->set_size (_page_size - win_pos, Vec2d ());
	_win_nodes_sptr->set_coord (Vec2d ());
	_win_nodes_sptr->set_disp_pos (Vec2d ());

	for (const auto &cnx_sptr :
		{ _cnx_sig_sptr, _cnx_aud_r_sptr, _cnx_aud_l_sptr, _cnx_aud_m_sptr }
	)
	{
		cnx_sptr->set_size (_page_size);
		cnx_sptr->set_coord (Vec2d ());
		cnx_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	}

	_page_ptr->push_back (_win_graph_sptr);

	win_pos = Vec2d (_page_size [0] * 2 / 3, 0);
	const auto     win_size = _page_size - win_pos;
	_win_menu_ctn_sptr->set_size (win_size, Vec2d ());
	_win_menu_ctn_sptr->set_coord (win_pos);
	_win_menu_ctn_sptr->set_disp_pos (Vec2d ());
	_win_menu_ctn_sptr->set_autoscroll (false);

	_win_menu_sptr->set_size (win_size, Vec2d ());
	_win_menu_sptr->set_coord (Vec2d ());
	_win_menu_sptr->set_disp_pos (Vec2d ());

	_menu_bkg_sptr->set_size (win_size);
	_menu_bkg_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
	_menu_bkg_sptr->set_color (_col_dgreen);
	_menu_bkg_sptr->set_coord (Vec2d ());
	grap::RenderCtx   ctx (
		_menu_bkg_sptr->use_buffer (),
		_menu_bkg_sptr->get_size (),
		_menu_bkg_sptr->get_stride ()
	);
	ctx.fill (192); // Not totally opaque

	_node_name_sptr->set_font (_fs_ptr->_m);
	_edit_fx_sptr  ->set_font (_fs_ptr->_m);
	_del_cnx_sptr  ->set_font (_fs_ptr->_m);
	_ins_slot_sptr ->set_font (_fs_ptr->_m);
	_rep_cnx_sptr  ->set_font (_fs_ptr->_m);

	_page_ptr->push_back (_win_menu_ctn_sptr);

	_node_cur = -1;
	update_display ();
}



void	FlowGraph::do_disconnect ()
{
	// Nothing
}



MsgHandlerInterface::EvtProp	FlowGraph::do_handle_evt (const NodeEvt &evt)
{
	switch (_ui_state)
	{
	case UiState::graph:
		return handle_evt_graph (evt);

	case UiState::menu_node:
		return handle_evt_node_menu (evt);

	case UiState::menu_cnx:
		return handle_evt_cnx_menu (evt);
	}

	return EvtProp_PASS;
}



void	FlowGraph::do_set_prog (int bank_index, int prog_index, const doc::Program &prog)
{
	fstb::unused (bank_index, prog_index, prog);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_activate_prog (int index)
{
	fstb::unused (index);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_layer_edit (doc::LayerType layer_edit)
{
	fstb::unused (layer_edit);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_chn_mode (ChnMode mode)
{
	fstb::unused (mode);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_add_slot (int slot_id)
{
	fstb::unused (slot_id);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_remove_slot (int slot_id)
{
	fstb::unused (slot_id);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_routing (const doc::Routing &routing)
{
	fstb::unused (routing);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_plugin (int slot_id, const PluginInitData &pi_data)
{
	fstb::unused (slot_id, pi_data);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_remove_plugin (int slot_id)
{
	fstb::unused (slot_id);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_plugin_mono (int slot_id, bool mono_flag)
{
	fstb::unused (slot_id, mono_flag);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_plugin_reset (int slot_id, bool reset_flag)
{
	fstb::unused (slot_id, reset_flag);
	_ui_state = UiState::graph;
	update_display ();
}



void	FlowGraph::do_set_param_ctrl (int slot_index, PiType type, int index, const doc::CtrlLinkSet &cls)
{
	fstb::unused (slot_index, type, index, cls);
	update_display ();
}



void	FlowGraph::do_set_signal_port (int port_id, const doc::SignalPort &port)
{
	fstb::unused (port_id, port);
	update_display ();
}



void	FlowGraph::do_clear_signal_port (int port_id)
{
	fstb::unused (port_id);
	update_display ();
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	FlowGraph::build_cursor ()
{
	const auto     curs_size = _node_rad * 4;
	_cursor_sptr->set_size (curs_size);
	_cursor_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);

	grap::RenderCtx   ctx (
		_cursor_sptr->use_buffer (),
		_cursor_sptr->get_size (),
		_cursor_sptr->get_stride ()
	);
	ctx.fill (0);

	// A square outline with only the corners
	//  __    __
	// |        |
	// 
	// 
	// |__    __|
	const auto     [w,  h ]  = curs_size;
	const auto     [sx, sy]  = curs_size / 4;
	constexpr int  thickness = 2; // Pixels
	for (int k = 0; k < thickness; ++k)
	{
		for (int y : { k, h - 1 - k })
		{
			grap::PrimLine::draw_h (ctx, k     , y, sx   , 255, false);
			grap::PrimLine::draw_h (ctx, w - sx, y, w - k, 255, false);
		}
		for (int x : { k, w - 1 - k })
		{
			grap::PrimLine::draw_v (ctx, x, k     , sy   , 255, false);
			grap::PrimLine::draw_v (ctx, x, h - sy, h - k, 255, false);
		}
	}
}



void	FlowGraph::build_highlight ()
{
	const auto     hl_size = _node_rad [0] * 6;
	const auto     rad     = double (_node_rad [0]) * 1.75;
	_highlight_sptr->set_size (Vec2d (hl_size, hl_size));
	_highlight_sptr->set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);

	grap::RenderCtx   ctx (
		_highlight_sptr->use_buffer (),
		_highlight_sptr->get_size (),
		_highlight_sptr->get_stride ()
	);
	ctx.fill (0);

	// A square-disc (L4 norm)
	const auto     sz  = hl_size;
	const auto     cc  = hl_size / 2 - 0.5; // -0.5 because diameter is even
	constexpr auto sig = 8.0;               // Pseudo-sigma (border blurring)
	constexpr int  n   = 4;                 // Gaussian gradient sharpness, > 0
	const auto     sn  = fstb::ipowpc <n> (sig);
	constexpr int  col = 192;               // In [0 ; 255]. Not totally opaque
	for (int y = 0; y < sz; ++y)
	{
		for (int x = 0; x < sz; ++x)
		{
			// Distance to the center (norm 4 for rounded square disc)
			const auto     dc = sqrt (sqrt (double (
				fstb::ipowpc <4> (x - cc) + fstb::ipowpc <4> (y - cc)
			)));

			// Distance to the "disc", raised to the power N
			const auto     dpn = fstb::ipowpc <n> (std::max (dc - rad, 0.));

			// Small gradient for the border blur
			const auto     g = exp (-dpn / sn);
			const auto     v = uint8_t (fstb::round_int (g * col));
			ctx.at (x, y) = v;
		}
	}
}



void	FlowGraph::update_display ()
{
	update_graph ();

	switch (_ui_state)
	{
	case UiState::graph:
		_win_menu_ctn_sptr->enable (false);
		_highlight_sptr->show (false);
		break;
	case UiState::menu_node:
		update_menu_node ();
		break;
	case UiState::menu_cnx:
		update_menu_cnx ();
		break;
	}
}



void	FlowGraph::update_graph ()
{
	const auto &   prog  = _view_ptr->use_prog_cur ();
	const auto &   layer = prog._layer;

	// Updates the map [slot_id] -> NodeEntry
	Tools::extract_slot_list (_slot_list, layer, *_model_ptr);
	_slot_map.clear ();
	for (const auto &slot : _slot_list)
	{
		_slot_map [slot._slot_id] = slot;
	}

	// Builds the display layout for the graph
	_layer_type = _view_ptr->get_layer_edit ();
	_chn_mode   = _view_ptr->use_setup ()._chn_mode;
	const auto &   sig_pi_list = _model_ptr->use_sig_pi_list ();
	_graph_disp.process (layer, _layer_type, _chn_mode, sig_pi_list);

	update_graph_nodes ();
	update_graph_connections ();

	// Builds the navigation thing
	if (_ui_state == UiState::graph)
	{
		update_nav ();
	}

	_win_graph_sptr->invalidate_all ();
}



void	FlowGraph::update_nav ()
{
	_nav_graph.clear ();

	constexpr std::array <
		std::pair <Button, GraphDisp::Move>,
		GraphDisp::Move_NBR_ELT
	>              move_translate =
	{{
		{ Button_U, GraphDisp::Move_U },
		{ Button_D, GraphDisp::Move_D },
		{ Button_L, GraphDisp::Move_L },
		{ Button_R, GraphDisp::Move_R }
	}};

	constexpr auto entry_nav = EntryNode_ICON;
	const int      nbr_nodes = _graph_disp.get_nbr_nodes ();
	for (int node_idx = 0; node_idx < nbr_nodes; ++node_idx)
	{
		const auto &   mv_table = _graph_disp.use_move_table (node_idx);
		const int      e_base   = node_idx_to_entry (node_idx);
		NavLoc         nav { e_base + entry_nav };

		for (const auto [bu, mv] : move_translate)
		{
			int            entry   = NavLoc::OvrAction_STOP;
			const auto     tgt_idx = mv_table [mv];
			if (tgt_idx >= 0)
			{
				const auto     tgt_base = node_idx_to_entry (tgt_idx);
				entry = tgt_base + entry_nav;
			}
			nav._action_arr [bu] = entry;
		}

		_nav_graph.push_back (nav);
	}

	if (_node_cur < 0 || _node_cur >= nbr_nodes)
	{
		_node_cur = 0;
		if (_loc_edit._slot_id >= 0)
		{
			const auto     node_idx_opt =
				_graph_disp.find_node (_loc_edit._slot_id);
			if (node_idx_opt)
			{
				_node_cur = *node_idx_opt;
			}
		}
	}
	const int      node_id = node_idx_to_entry (_node_cur);
	_page_ptr->set_nav_layout (_nav_graph, node_id);
}



void	FlowGraph::update_graph_nodes ()
{
	// Size-related constants
	constexpr auto wh   = _node_rad [0];
	constexpr auto hh   = _node_rad [1];
	constexpr int  ic_w = wh * 2;
	constexpr int  ic_h = hh * 2;
	const int      w_t  = _fs_ptr->_t.get_char_w (U'0');

	// Builds the nodes widgets
	const int      nbr_nodes = _graph_disp.get_nbr_nodes ();
	const auto     [sz_x, sz_y] = _graph_disp.get_canvas_size ();
	_node_gfx_arr.clear ();
	_node_gfx_arr.resize (nbr_nodes);
	for (int node_idx = 0; node_idx < nbr_nodes; ++node_idx)
	{
		const auto     type   = _graph_disp.get_node_type (node_idx);
		const auto     node_c = calc_node_coord (node_idx);
		const auto     max_txt_width = std::max (
			_page_size [0] * GraphDisp::_grid_res / sz_x - ic_w,
			96
		);

		NodeGfx &      widget = _node_gfx_arr [node_idx];
		const int      e_base = node_idx_to_entry (node_idx);

		// Finds the color depending on the node type
		ui::PixArgb    col = _col_vlgrey;
		switch (type)
		{
		case GraphDisp::NodeType::slot:
			if (_graph_disp.is_node_signal_slot (node_idx))
			{
				col = _col_green;
			}
			break;
		case GraphDisp::NodeType::aud_i: col = _col_yellow; break;
		case GraphDisp::NodeType::aud_o: col = _col_orange; break;
		case GraphDisp::NodeType::ret:   col = _col_purple; break;
		case GraphDisp::NodeType::send:  col = _col_violet; break;
		}

		// Icon
		widget._icon_sptr = std::make_shared <NBitmap> (e_base + EntryNode_ICON);
		auto &         ic = *(widget._icon_sptr);
		ic.set_coord (node_c - Vec2d (wh, hh));
		ic.set_size (Vec2d (ic_w, ic_h));
		ic.set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
		ic.show_cursor (false);
		for (int y = 0; y < ic_h; ++y)
		{
			ui::PixArgb *  ptr = ic.use_buffer () + y * ic.get_stride ();
			std::fill (ptr, ptr + ic_w, col);
		}

		// Name
		widget._name_sptr = std::make_shared <NText> (e_base + EntryNode_NAME);
		auto &         na = *(widget._name_sptr);
		na.set_font (_fs_ptr->_t);
		na.set_coord (node_c + Vec2d (wh + (w_t >> 1), 1));
		na.set_justification (0, 0.5f, false);
		na.set_color (col);
		na.set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
		const auto     multilabel = get_node_name (node_idx);
		const std::string txt = pi::param::Tools::print_name_bestfit (
			max_txt_width, multilabel.c_str (), na, &NText::get_char_width
		);
		na.set_text (txt);

		// Once name is done, adds a black outline to the text.
		widget._outline_sptr =
			std::make_shared <NBitmapM> (e_base + EntryNode_OUTLINE);
		auto &         ol = (*widget._outline_sptr);
		ol.set_blend_mode (ui::DisplayInterface::BlendMode_ALPHA);
		ol.set_color (_col_black);
	}

	// Add the widgets to the window
	_win_nodes_sptr->clear_all_nodes ();
	for (const auto &widget : _node_gfx_arr)
	{
		_win_nodes_sptr->push_back (widget._icon_sptr);
	}
	for (const auto &widget : _node_gfx_arr)
	{
		update_outline (*(widget._outline_sptr), *(widget._name_sptr));
		_win_nodes_sptr->push_back (widget._outline_sptr);
		_win_nodes_sptr->push_back (widget._name_sptr);
	}
}



// Returns the name as multilabel string
std::string	FlowGraph::get_node_name (int node_idx) const
{
	assert (node_idx >= 0);
	assert (node_idx < _graph_disp.get_nbr_nodes ());

	const auto     type    = _graph_disp.get_node_type (node_idx);
	const auto     node_id = _graph_disp.get_node_id (node_idx);

	std::string    multilabel;
	switch (type)
	{
	case GraphDisp::NodeType::slot:
		{
			const auto     it = _slot_map.find (node_id);
			assert (it != _slot_map.end ());
			multilabel = Tools::build_slot_name_with_index (it->second);
		}
		break;
	case GraphDisp::NodeType::aud_i:
		multilabel = "In";
		break;
	case GraphDisp::NodeType::aud_o:
		multilabel = "Out";
		break;
	case GraphDisp::NodeType::ret:
		multilabel = "Return\nRet";
		break;
	case GraphDisp::NodeType::send:
		multilabel = "Send";
		break;
	}

	if (type != GraphDisp::NodeType::slot)
	{
		char           txt_0 [63+1];
		fstb::snprintf4all (txt_0, sizeof (txt_0), " %d", node_id + 1);
		multilabel = pi::param::Tools::join_strings_multi (
			multilabel.c_str (), '\n', "", txt_0
		);
	}

	return multilabel;
}



void	FlowGraph::update_graph_connections ()
{
	// Size-related constants
	constexpr auto wh = _node_rad [0];
	constexpr auto hh = _node_rad [1];

	// Clears all the connection layers
	for (const auto &cnx_sptr :
		{ _cnx_sig_sptr, _cnx_aud_r_sptr, _cnx_aud_l_sptr, _cnx_aud_m_sptr }
	)
	{
		grap::RenderCtx   ctx (
			cnx_sptr->use_buffer (),
			cnx_sptr->get_size (),
			cnx_sptr->get_stride ()
		);
		ctx.fill (0);
	}

	// Draws the connections
	const auto     nbr_cnx = _graph_disp.get_nbr_cnx ();
	for (int cnx_idx = 0; cnx_idx < nbr_cnx; ++ cnx_idx)
	{
		const auto     src_idx =
			_graph_disp.get_cnx_end (cnx_idx, piapi::Dir_OUT);
		const auto     dst_idx =
			_graph_disp.get_cnx_end (cnx_idx, piapi::Dir_IN );
		const auto     c_type  = _graph_disp.get_cnx_type (cnx_idx);
		const auto     src_c   = calc_node_coord (src_idx);
		const auto     dst_c   = calc_node_coord (dst_idx);
		int            ofs_x   = 0;

		// Selects the drawing layer(s)
		// We possibly have to draw 2 curves, for stereo
		std::array <NBitmapM *, 2> gfx_ptr_arr { nullptr, nullptr };

		if (c_type == GraphDisp::CnxType::sig)
		{
			ofs_x = 4;
			gfx_ptr_arr [0] = _cnx_sig_sptr.get ();
		}
		else
		{
			const int      nbr_chn = get_cnx_chn (cnx_idx);
			if (nbr_chn == 2)
			{
				ofs_x           = -2;
				gfx_ptr_arr [0] = _cnx_aud_l_sptr.get ();
				gfx_ptr_arr [1] = _cnx_aud_r_sptr.get ();
			}
			else
			{
				gfx_ptr_arr [0] = _cnx_aud_m_sptr.get ();
			}
		}

		// Actual drawing
		for (auto gfx_ptr : gfx_ptr_arr)
		{
			if (gfx_ptr != nullptr)
			{
				grap::RenderCtx   ctx (
					gfx_ptr->use_buffer (),
					gfx_ptr->get_size (),
					gfx_ptr->get_stride ()
				);
				draw_connection (
					ctx,
					src_c [0] + ofs_x, src_c [1] + hh,
					dst_c [0] + ofs_x, dst_c [1] - hh - 1,
					wh, hh, 1
				);
			}
			ofs_x += 4;
		}
	}
}



// x0, y0: source point
// y1, x1: sink point
// wh, hh: radius of the connected nodes
// thickness in pixels, >= 1
void	FlowGraph::draw_connection (grap::RenderCtx &ctx, int x0, int y0, int x1, int y1, int wh, int hh, double thickness) noexcept
{
	assert (wh > 0);
	assert (hh > 0);
	assert (thickness >= 1);

	const auto     dist_x = std::abs (x1 - x0);

	// A single Bezier segment for a smooth curve going down
	if (y1 - y0 >= hh || (y1 >= y0 && dist_x <= wh))
	{
		const int      dy_min = fstb::limit (dist_x, 1, 2 * hh);
		int            dy_t   = std::max (std::abs (y1 - y0) / 2, dy_min);
		grap::PrimBezierCubic::draw_aa (
			ctx,
			x0, y0,
			x0, y0 + dy_t,
			x1, y1 - dy_t,
			x1, y1,
			thickness
		);
	}

	// Two Bezier segments for going up, making a S or C shape
	else
	{
		// Connection goes the same side of both nodes if they are horizontally
		// close
		const bool     same_side_flag = (dist_x < 3 * wh);

		int            x2    = (x0 + x1) / 2;
		int            y2    = (y0 + y1) / 2;
		int            dx_t2 = ((x0 > x1) ? -3 * wh : 3 * wh) + x0 - x2;
		int            ofs_dn = 0;
		int            ofs_up = 0;
		if (y1 > y0 && dx_t2 * (x1 - x2) > 0)
		{
			// Avoids the curve going horizontally backwards.
			// This happens when nodes are very close.
			dx_t2 = 0;
		}
		int            dy_t2 = 2 * hh + std::max ((y0 - y1) / 2, 0);
		if (same_side_flag)
		{
			x2    = 2 * wh + std::max (x0, x1);
			dx_t2 = 0;
			if (y1 >= y0 - hh)
			{
				ofs_dn = y1 - (y0 - hh);
				if (x0 > x1 + wh)
				{
					ofs_up = ofs_dn * 2;
				}
			}
		}

		grap::PrimBezierCubic::draw_aa (
			ctx,
			x0, y0,
			x0, y0 + 2 * hh + ofs_dn,
			x2 + dx_t2, y2 + dy_t2 + ofs_dn - ofs_up,
			x2, y2 + ofs_dn * 2 - ofs_up * 2,
			thickness
		);
		grap::PrimBezierCubic::draw_aa (
			ctx,
			x2, y2 + ofs_dn * 2 - ofs_up * 2,
			x2 - dx_t2, y2 - dy_t2 + ofs_dn - ofs_up,
			x1, y1 - 2 * hh - ofs_up,
			x1, y1,
			thickness
		);
	}
}



Vec2d	FlowGraph::calc_node_coord (int node_idx) const noexcept
{
	assert (node_idx >= 0);

	const auto     [sz_x, sz_y] = _graph_disp.get_canvas_size ();
	const auto     [ n_x,  n_y] = _graph_disp.get_node_coord (node_idx);

	// Nodes are centered on the canvas cells
	const auto     xc = _page_size [0] * (n_x * 2 + 1) / (sz_x * 2);
	const auto     yc = _page_size [1] * (n_y * 2 + 1) / (sz_y * 2);

	return Vec2d (xc, yc);
}



int	FlowGraph::get_cnx_chn (int cnx_idx) const noexcept
{
	assert (cnx_idx >= 0);
	assert (_graph_disp.get_cnx_type (cnx_idx) == GraphDisp::CnxType::aud);

	int            nbr_chn = 1;

	const auto     src_idx  =
		_graph_disp.get_cnx_end (cnx_idx, piapi::Dir_OUT);
	const auto     src_type = _graph_disp.get_node_type (src_idx);
	bool           check_dst_flag = false;
	int            slot_id = -1;
	std::optional <cmd::PluginRoutingInfo> pri_opt;
	switch (src_type)
	{
	case GraphDisp::NodeType::slot:
		slot_id = _graph_disp.get_node_id (src_idx);
		pri_opt = _model_ptr->get_plugin_routing_info (slot_id);
		if (pri_opt)
		{
			nbr_chn = pri_opt->_nbr_chn_o;
		}
		break;
	case GraphDisp::NodeType::aud_i:
		nbr_chn = ChnMode_get_layer_nbr_chn (
			_layer_type, _chn_mode, piapi::Dir_IN
		);
		break;
	case GraphDisp::NodeType::ret:
		check_dst_flag = true;
		break;

	case GraphDisp::NodeType::aud_o:
	case GraphDisp::NodeType::send:
		assert (false);
		break;
	}

	if (check_dst_flag)
	{
		const auto     dst_idx  =
			_graph_disp.get_cnx_end (cnx_idx, piapi::Dir_IN);
		const auto     dst_type = _graph_disp.get_node_type (dst_idx);
		if (dst_type == GraphDisp::NodeType::slot)
		{
			slot_id = _graph_disp.get_node_id (dst_idx);
			pri_opt = _model_ptr->get_plugin_routing_info (slot_id);
			if (pri_opt)
			{
				nbr_chn = pri_opt->_nbr_chn_i;
			}
		}
		else if (dst_type == GraphDisp::NodeType::aud_o)
		{
			nbr_chn = ChnMode_get_layer_nbr_chn (
				_layer_type, _chn_mode, piapi::Dir_OUT
			);
		}
		/*** To do: what if it's a send? ***/
	}

	return nbr_chn;
}



// Builds an outlined shadow (grow mask)
// Could be optimised.
void	FlowGraph::update_outline (NBitmapM &dst, const NBitmapM &src)
{
	constexpr int  grow   = 1;
	const auto &   bb_src = src.get_bounding_box ();
	const auto     sz_src = bb_src.get_size ();
	const auto     sz_dst = sz_src + Vec2d (grow * 2, grow * 2);
	if (dst.get_size () != sz_dst)
	{
		dst.set_size (sz_dst);
	}
	dst.set_coord (src.get_coord () + bb_src [0] - Vec2d (grow, grow));
	const int      stride_src  = src.get_stride ();
	const auto *   src_buf_ptr = src.use_buffer ();


	auto           get =
		[sz_src] (int x, int y, const uint8_t *src_ptr) noexcept
		{
			if (x < 0 || y < 0 || x >= sz_src [0] || y >= sz_src [1])
			{
				return uint8_t (0);
			}
			return src_ptr [x];
		};

	grap::RenderCtx   ctx_dst (
		dst.use_buffer (),
		dst.get_size (),
		dst.get_stride ()
	);

	const uint8_t *   s2_ptr = src_buf_ptr;
	const uint8_t *   s1_ptr = s2_ptr - stride_src;
	const uint8_t *   s0_ptr = s1_ptr - stride_src;
	for (int y = 0; y < sz_dst [1]; ++ y)
	{
		for (int x = 0; x < sz_dst [0]; ++ x)
		{
			const auto     c0 = get (x - 1 - grow, y - 1 - grow, s0_ptr);
			const auto     c1 = get (x     - grow, y - 1 - grow, s0_ptr);
			const auto     c2 = get (x + 1 - grow, y - 1 - grow, s0_ptr);
			const auto     c3 = get (x - 1 - grow, y     - grow, s1_ptr);
			const auto     c4 = get (x     - grow, y     - grow, s1_ptr);
			const auto     c5 = get (x + 1 - grow, y     - grow, s1_ptr);
			const auto     c6 = get (x - 1 - grow, y + 1 - grow, s2_ptr);
			const auto     c7 = get (x     - grow, y + 1 - grow, s2_ptr);
			const auto     c8 = get (x + 1 - grow, y + 1 - grow, s2_ptr);
			const auto     c  = std::max (
				{ c0, c1, c2, c3, c4, c5, c6, c7, c8 }
			);
			ctx_dst.at (x, y) = c;
		}
		s0_ptr  = s1_ptr;
		s1_ptr  = s2_ptr;
		s2_ptr += stride_src;
	}

	dst.invalidate_all ();
}



int	FlowGraph::node_idx_to_entry (int node_idx) const noexcept
{
	assert (node_idx >= 0);
	assert (node_idx < _graph_disp.get_nbr_nodes ());

	return Entry_NODES + node_idx * EntryNode_NBR_ELT;
}



int	FlowGraph::node_entry_to_idx (int node_id) const noexcept
{
	assert (node_id >= Entry_NODES);

	const int        node_idx = (node_id - Entry_NODES) / EntryNode_NBR_ELT;
	assert (node_idx >= 0);
	assert (node_idx < _graph_disp.get_nbr_nodes ());

	return node_idx;
}



// Graph and cached data should be valid at this point
void	FlowGraph::update_menu_node ()
{
	assert (_node_cur >= 0);

	PageMgrInterface::NavLocList  nav_list;
	_win_menu_sptr->clear_all_nodes ();

	const auto     win_size = _win_menu_sptr->get_bounding_box () [1];
	const auto     scr_w    = win_size [0];

	const int      h_m      = _fs_ptr->_m.get_char_h ();
	int            pos_y    = 0;

	// Title: node name
	const auto     name_multi = get_node_name (_node_cur);
	_node_name_sptr->set_coord (Vec2d (scr_w >> 1, pos_y));
	_node_name_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
	_node_name_sptr->set_text_m (name_multi);
	_win_menu_sptr->push_back (_node_name_sptr);
	pos_y += (h_m * 3) >> 1;

	const auto     node_type = _graph_disp.get_node_type (_node_cur);
	if (node_type == GraphDisp::NodeType::slot)
	{
		// Edit FX
		_edit_fx_sptr->set_coord (Vec2d (0, pos_y));
		_edit_fx_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		_win_menu_sptr->push_back (_edit_fx_sptr);
		pos_y += (h_m * 3) >> 1;
		PageMgrInterface::add_nav (nav_list, Entry_EDIT_FX);
	}

	// Finds the physical ports of the current node, if any
	const PinInfo  pin_info = get_pin_info (_node_cur);

	// Graph connections
	int            slot_id    = 0;
	auto           n_type_doc = doc::CnxEnd::Type_INVALID;
	switch (node_type)
	{
	case GraphDisp::NodeType::slot:
		n_type_doc = doc::CnxEnd::Type_NORMAL;
		slot_id    = _graph_disp.get_node_id (_node_cur);
		break;
	case GraphDisp::NodeType::aud_i:
	case GraphDisp::NodeType::aud_o:
		n_type_doc = doc::CnxEnd::Type_IO;
		break;
	case GraphDisp::NodeType::send:
	case GraphDisp::NodeType::ret:
		n_type_doc = doc::CnxEnd::Type_RS;
		break;
	}
	int            dir_beg = 0;
	int            dir_end = piapi::Dir_NBR_ELT;
#if 0
	switch (node_type)
	{
	case GraphDisp::NodeType::slot:
		// Nothing, default
		break;
	case GraphDisp::NodeType::aud_o:
	case GraphDisp::NodeType::send:
		dir_beg = piapi::Dir_IN;
		dir_end = dir_beg + 1;
		break;
	case GraphDisp::NodeType::aud_i:
	case GraphDisp::NodeType::ret:
		dir_beg = piapi::Dir_OUT;
		dir_end = dir_beg + 1;
		break;
	}
#endif
	const ToolsRouting::NodeMap & graph = _view_ptr->use_graph ();
	auto           it_node   =
		graph.find (ToolsRouting::Node { n_type_doc, slot_id });
	const bool     node_flag = (it_node != graph.end ());
	for (int dir = dir_beg; dir < dir_end; ++dir)
	{
		auto &         side     = _side_arr [dir];
		const int      nbr_pins_phy = pin_info._nbr_pins_arr_phy [dir];
		int            nbr_pins     = nbr_pins_phy;
		int            nbr_pins_gra = 0;
		if (node_flag)
		{
			const ToolsRouting::CnxPinList &	side_node = it_node->second [dir];
			nbr_pins_gra = int (side_node.size ());
			nbr_pins     = std::max (nbr_pins, nbr_pins_gra);
		}

		int            pin_beg = 0;
		int            pin_end = nbr_pins;

		// Single-pin nodes
		if (pin_info._unique_pin_idx >= 0)
		{
			if (nbr_pins_phy > 0)
			{
				pin_beg = pin_info._unique_pin_idx;
				pin_end = pin_beg + 1;
			}
			else
			{
				pin_beg = 0;
				pin_end = 0;
			}
		}

		side.resize (nbr_pins);
		for (int pin_idx = pin_beg; pin_idx < pin_end; ++pin_idx)
		{
			auto &         pin = side [pin_idx];
			RoutCom::list_pin (
				*_win_menu_sptr, pos_y, nav_list,
				pin, pin_idx, nbr_pins, nbr_pins_gra,
				piapi::Dir (dir), _slot_list,
				pin_info._exist_flag, node_flag, it_node,
				_fs_ptr->_m, scr_w, ui::DisplayInterface::BlendMode_ALPHA
			);
		}
	}

	_win_menu_ctn_sptr->enable (true);
	_page_ptr->set_nav_layout (nav_list, _node_id_menu_node);
	_node_id_top = nav_list.front ()._node_id;
}



FlowGraph::PinInfo	FlowGraph::get_pin_info (int node_idx) const
{
	assert (node_idx >= 0);
	assert (node_idx < _graph_disp.get_nbr_nodes ());

	PinInfo        pin_info;

	const auto     type = _graph_disp.get_node_type (node_idx);
	switch (type)
	{
	case GraphDisp::NodeType::slot:
		{
			const auto &   prog  = _view_ptr->use_prog_cur ();
			const auto &   layer = prog._layer;
			pin_info._nbr_pins_arr_phy.fill (1);
			const int         slot_id = _graph_disp.get_node_id (node_idx);
			[[maybe_unused]]
			int               nbr_sig = 0;
			pin_info._exist_flag = Tools::get_physical_io (
				pin_info._nbr_pins_arr_phy [piapi::Dir_IN],
				pin_info._nbr_pins_arr_phy [piapi::Dir_OUT],
				nbr_sig, slot_id, layer, *_model_ptr
			);
		}
		break;

	// Node dir and audio port dir inverted because related to node or whole
	// graph.
	case GraphDisp::NodeType::aud_i:
		pin_info._nbr_pins_arr_phy [piapi::Dir_OUT] =
			ChnMode_get_layer_nbr_pins (_layer_type, _chn_mode, piapi::Dir_IN );
		break;
	case GraphDisp::NodeType::aud_o:
		pin_info._nbr_pins_arr_phy [piapi::Dir_IN ] =
			ChnMode_get_layer_nbr_pins (_layer_type, _chn_mode, piapi::Dir_OUT);
		break;

	case GraphDisp::NodeType::send:
		pin_info._nbr_pins_arr_phy [piapi::Dir_IN ] = Cst::_max_nbr_send;
		break;
	case GraphDisp::NodeType::ret:
		pin_info._nbr_pins_arr_phy [piapi::Dir_OUT] = Cst::_max_nbr_send;
		break;
	}

	if (type != GraphDisp::NodeType::slot)
	{
		pin_info._unique_pin_idx = _graph_disp.get_node_id (node_idx);
	}

	return pin_info;
}



void	FlowGraph::update_menu_cnx ()
{
	assert (_node_cur >= 0);
	assert (_arg._ed_type != RoutCom::Arg::EdType_INVALID);

	PageMgrInterface::NavLocList  nav_list;
	_win_menu_sptr->clear_all_nodes ();

	const auto     win_size = _win_menu_sptr->get_bounding_box () [1];
	const auto     scr_w    = win_size [0];

	// Title: action type
	std::string    title_multi;
	switch (_arg._ed_type)
	{
	case RoutCom::Arg::EdType_CNX:
		title_multi = "Edit connection\nEdit cnx\nEdit";
		break;
	case RoutCom::Arg::EdType_PIN:
		title_multi = "Add connection\nAdd cnx\nAdd";
		break;
	default:
		assert (false);
		break;
	}
	_node_name_sptr->set_coord (Vec2d (scr_w >> 1, 0));
	_node_name_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
	_node_name_sptr->set_text_m (title_multi);
	_win_menu_sptr->push_back (_node_name_sptr);

	const int      h_m   = _fs_ptr->_m.get_char_h ();
	int            pos_y = (h_m * 3) >> 1;

	if (_arg._ed_type == RoutCom::Arg::EdType_CNX)
	{
		// Delete
		_del_cnx_sptr->set_coord (Vec2d (0, pos_y));
		pos_y += h_m;
		_del_cnx_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		_win_menu_sptr->push_back (_del_cnx_sptr);
		PageMgrInterface::add_nav (nav_list, Entry_DEL_CNX);

		// Insert new FX
		_ins_slot_sptr->set_coord (Vec2d (0, pos_y));
		pos_y += h_m;
		_ins_slot_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		_win_menu_sptr->push_back (_ins_slot_sptr);
		PageMgrInterface::add_nav (nav_list, Entry_INS_SLOT);

		// Replace with:
		_rep_cnx_sptr->set_coord (Vec2d (0, pos_y));
		pos_y += h_m;
		_rep_cnx_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		_win_menu_sptr->push_back (_rep_cnx_sptr);
	}

	// List of potential connections
	const auto     start = conv_node_gd_to_tr (_node_cur);
	RoutCom::build_possible_cnx_set (
		_cnx_end_arr, *_win_menu_sptr, nav_list, pos_y, start,
		_arg, _fs_ptr->_m, scr_w, ui::DisplayInterface::BlendMode_ALPHA,
		*_model_ptr, *_view_ptr
	);

	_win_menu_ctn_sptr->enable (true);
	_page_ptr->set_nav_layout (nav_list);
	_node_id_top = nav_list.front ()._node_id;
}



MsgHandlerInterface::EvtProp	FlowGraph::handle_evt_graph (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const auto     node_id = evt.get_target ();

	if (evt.is_cursor ())
	{
		const auto     evt_curs = evt.get_cursor ();
		if (evt_curs == NodeEvt::Curs_LEAVE)
		{
			_cursor_sptr->show (false);
		}
		else if (evt_curs == NodeEvt::Curs_ENTER)
		{
			_node_cur = node_entry_to_idx (node_id);
			auto           pos = calc_node_coord (_node_cur);
			pos -= _cursor_sptr->get_bounding_box () [1] / 2;
			_cursor_sptr->set_coord (pos);
			_cursor_sptr->show (true);
		}
	}

	else if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			_node_id_menu_node = -1;
			_ui_state = UiState::menu_node;
			update_display ();
			ret_val = EvtProp_CATCH;
			break;

		case Button_E:
			_page_switcher.return_page ();
			ret_val = EvtProp_CATCH;
			break;

		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



MsgHandlerInterface::EvtProp	FlowGraph::handle_evt_node_menu (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const auto     node_id = evt.get_target ();

	if (evt.is_cursor ())
	{
		const auto     evt_curs = evt.get_cursor ();
		if (evt_curs == NodeEvt::Curs_LEAVE)
		{
			_highlight_sptr->show (false);
		}
		else if (evt_curs == NodeEvt::Curs_ENTER)
		{
			// Makes sure we make the top of the menu appear when we get back
			// to the beginning of the list.
			if (node_id == _node_id_top)
			{
				_win_menu_sptr->set_disp_pos (Vec2d ());
			}

			piapi::Dir     dir;
			int            pin_idx;
			int            cnx_idx;
			const auto     type =
				RoutCom::conv_node_id_to_cnx (dir, pin_idx, cnx_idx, node_id);
			if (type == RoutCom::IoType_CNX)
			{
				const auto &   cnx_doc =
					_side_arr [dir] [pin_idx]._cnx_arr [cnx_idx]._cnx;
				const auto &   end = cnx_doc.use_end (dir);
				const auto     node_idx_opt = _graph_disp.find_node (
					end.get_type (), end.get_slot_id (), end.get_pin (),
					piapi::Dir_invert (dir)
				);
				if (! node_idx_opt)
				{
					assert (false);
				}
				else
				{
					const int      node_idx = *node_idx_opt;
					auto           pos = calc_node_coord (node_idx);
					pos -= _highlight_sptr->get_bounding_box () [1] / 2;
					_highlight_sptr->set_coord (pos);
					_highlight_sptr->show (true);
				}
			}
		}
	}

	else if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			if (node_id == Entry_EDIT_FX)
			{
				const doc::Program & prog = _view_ptr->use_prog_cur ();
				assert (_node_cur >= 0);
				assert (
					   _graph_disp.get_node_type (_node_cur)
					== GraphDisp::NodeType::slot
				);
				const int      slot_id = _graph_disp.get_node_id (_node_cur);
				if (slot_id < 0)
				{
					assert (false);
				}
				else
				{
					_loc_edit._slot_id = slot_id;
					if (! prog._layer.is_slot_empty (slot_id))
					{
						_page_switcher.call_page (PageType_PARAM_LIST, nullptr);
					}
					else
					{
						// Empty slot
						_page_switcher.call_page (PageType_SLOT_MENU, nullptr);
					}
					ret_val = EvtProp_CATCH;
				}
			}
			else
			{
				ret_val = sel_pin_cnx (node_id);
			}
			break;

		case Button_E:
			_ui_state = UiState::graph;
			_node_id_menu_node = -1;
			_highlight_sptr->show (false);
			update_display ();
			ret_val = EvtProp_CATCH;
			break;

		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



MsgHandlerInterface::EvtProp	FlowGraph::sel_pin_cnx (int node_id)
{
	assert (node_id >= 0);
	assert (_ui_state == UiState::menu_node);

	EvtProp        ret_val = EvtProp_CATCH;

	// Saves the node for when we return to this menu
	_node_id_menu_node = node_id;

	int            pin_idx = -1;
	int            cnx_idx = -1;
	const auto     type =
		RoutCom::conv_node_id_to_cnx (_arg._dir, pin_idx, cnx_idx, node_id);

	if (type == RoutCom::IoType_NAME)
	{
		_arg._ed_type = RoutCom::Arg::EdType_PIN;
		_arg._pin_idx = pin_idx;
		_ui_state     = UiState::menu_cnx;
	}

	else if (type == RoutCom::IoType_CNX)
	{
		_arg._ed_type = RoutCom::Arg::EdType_CNX;
		_arg._cnx     = _side_arr [_arg._dir] [pin_idx]._cnx_arr [cnx_idx]._cnx;
		_ui_state     = UiState::menu_cnx;
	}

	if (_ui_state == UiState::menu_cnx)
	{
		update_display ();
	}
	else 
	{
		ret_val = EvtProp_PASS;
		assert (false);
	}

	return ret_val;
}



MsgHandlerInterface::EvtProp	FlowGraph::handle_evt_cnx_menu (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	[[maybe_unused]]
	const auto     node_id = evt.get_target ();

	if (evt.is_cursor ())
	{
		const auto     evt_curs = evt.get_cursor ();
		if (evt_curs == NodeEvt::Curs_LEAVE)
		{
			_highlight_sptr->show (false);
		}
		else if (evt_curs == NodeEvt::Curs_ENTER)
		{
			// Makes sure we make the top of the menu appear when we get back
			// to the beginning of the list.
			if (node_id == _node_id_top)
			{
				_win_menu_sptr->set_disp_pos (Vec2d ());
			}

			if (   node_id >= RoutCom::_entry_tgt_beg
			    && node_id <  RoutCom::_entry_tgt_base
                         + int (_cnx_end_arr.size ()))
			{
				const auto &   end =
					_cnx_end_arr [node_id - RoutCom::_entry_tgt_base]._cnx_end;
				const auto     node_idx_opt = _graph_disp.find_node (
					end.get_type (), end.get_slot_id (), end.get_pin (),
					piapi::Dir_invert (_arg._dir)
				);
				if (! node_idx_opt)
				{
					// The target node may not be in the current graph, for example
					// an unused send or return.
					_highlight_sptr->show (false);
				}
				else
				{
					const int      node_idx = *node_idx_opt;
					auto           pos = calc_node_coord (node_idx);
					pos -= _highlight_sptr->get_bounding_box () [1] / 2;
					_highlight_sptr->set_coord (pos);
					_highlight_sptr->show (true);
				}
			}
		}
	}

	else if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			ret_val = EvtProp_CATCH;
			if (   node_id >= RoutCom::_entry_tgt_base
			    && node_id <  RoutCom::_entry_tgt_base
			                + int (_cnx_end_arr.size ()))
			{
				const auto     graph_node = conv_node_gd_to_tr (_node_cur);
				const auto     slot_id_opt = RoutCom::add_or_replace_cnx (
					node_id, graph_node, _cnx_end_arr,
					_arg, *_model_ptr, *_view_ptr
				);
				if (slot_id_opt)
				{
					set_focus_on_slot (*slot_id_opt);
				}
			}
			else
			{
				switch (node_id)
				{
				case Entry_DEL_CNX:
					RoutCom::del_cnx (_arg, *_model_ptr, *_view_ptr);
					break;
				case Entry_INS_SLOT:
					insert_slot_at ();
					break;
				default:
					ret_val = EvtProp_PASS;
					break;
				}
			}
			break;

		case Button_E:
			_ui_state = UiState::menu_node;
			_highlight_sptr->show (false);
			update_display ();
			ret_val = EvtProp_CATCH;
			break;

		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



void	FlowGraph::insert_slot_at ()
{
	const int      slot_id    = _model_ptr->add_slot ();
	const doc::Program & prog = _view_ptr->use_prog_cur ();
	doc::Routing   routing    = prog._layer.use_routing (); // Copy
	ToolsRouting::insert_slot_at (routing._cnx_audio_set, slot_id, _arg._cnx);
	_model_ptr->set_routing (routing);

	// Sets the focus on the new slot
	set_focus_on_slot (slot_id);
}



void	FlowGraph::set_focus_on_slot (int slot_id)
{
	assert (slot_id >= 0);

	const auto     node_idx_opt = _graph_disp.find_node (slot_id);
	if (node_idx_opt)
	{
		_node_cur = *node_idx_opt;
		update_display ();
	}
}



ToolsRouting::Node	FlowGraph::conv_node_gd_to_tr (int node_idx) noexcept
{
	assert (node_idx >= 0);

	const auto     gd_type = _graph_disp.get_node_type (node_idx);
	const auto     gd_id   = _graph_disp.get_node_id (node_idx);
	switch (gd_type)
	{
	case GraphDisp::NodeType::slot:
		return ToolsRouting::Node { doc::CnxEnd::Type_NORMAL, gd_id };
	case GraphDisp::NodeType::aud_i:
	case GraphDisp::NodeType::aud_o:
		return ToolsRouting::Node { doc::CnxEnd::Type_IO, 0 };
	case GraphDisp::NodeType::send:
	case GraphDisp::NodeType::ret:
		return ToolsRouting::Node { doc::CnxEnd::Type_RS, 0 };
	}

	// Shouldn't happen
	assert (false);
	return ToolsRouting::Node {};
}



// 4 bits per component. Order: (r << 8) | (g << 4) | b
constexpr ui::PixArgb	FlowGraph::make12 (int rgb12) noexcept
{
	assert (rgb12 >= 0x000);
	assert (rgb12 <= 0xFFF);

	return ui::PixArgb {
		uint8_t ( rgb12 << 4        ),
		uint8_t ( rgb12       & 0xF0),
		uint8_t ((rgb12 >> 4) & 0xF0),
		0xFF
	};
}



const ui::PixArgb	FlowGraph::_col_black  = make12 (0x000);
const ui::PixArgb	FlowGraph::_col_grey   = make12 (0xAAA);
const ui::PixArgb	FlowGraph::_col_vlgrey = make12 (0xEEE);
const ui::PixArgb	FlowGraph::_col_white  = { 255, 255, 255, 255 };
const ui::PixArgb FlowGraph::_col_dgreen = {  32,  48,  32, 255 };

// Colors from https://iamkate.com/data/12-bit-rainbow/
const ui::PixArgb	FlowGraph::_col_red    = make12 (0xA35 + 0x312); // Brighter
const ui::PixArgb	FlowGraph::_col_blue   = make12 (0x36B + 0x123); //
const ui::PixArgb	FlowGraph::_col_green  = make12 (0x4D8);
const ui::PixArgb	FlowGraph::_col_yellow = make12 (0xED0);
const ui::PixArgb	FlowGraph::_col_orange = make12 (0xE94);
const ui::PixArgb	FlowGraph::_col_purple = make12 (0x817);
const ui::PixArgb	FlowGraph::_col_violet = make12 (0x639);
const ui::PixArgb	FlowGraph::_col_magenta = make12 (0x817);


}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
