/*****************************************************************************

        ParamEdit.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_pg_ParamEdit_HEADER_INCLUDED)
#define mfx_uitk_pg_ParamEdit_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/ParamPresentation.h"
#include "mfx/uitk/NText.h"
#include "mfx/uitk/PageInterface.h"
#include "mfx/uitk/PageMgrInterface.h"

#include <optional>



namespace mfx
{

class LocEdit;

namespace uitk
{

class PageSwitcher;

namespace pg
{



class ParamEdit final
:	public PageInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       ParamEdit (PageSwitcher &page_switcher, LocEdit &loc_edit);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::PageInterface
	void           do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt) final;
	void           do_disconnect () final;

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::PageInterface
	EvtProp        do_handle_evt (const NodeEvt &evt) final;

	// mfx::ModelObserverInterface via mfx::uitk::PageInterface
	void           do_set_tempo (double bpm) final;
	void           do_activate_prog (int index) final;
	void           do_set_layer_edit (doc::LayerType layer_edit) final;
	void           do_set_param (int slot_id, int index, float val, PiType type) final;
	void           do_remove_plugin (int slot_id) final;
	void           do_set_param_pres (int slot_id, PiType type, int index, const doc::ParamPresentation *pres_ptr) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _nbr_steps = 4;

	enum Entry
	{
		Entry_NAME = 0,
		Entry_VALUNIT,
		Entry_STEP,
		Entry_STEP_LAST = Entry_STEP + _nbr_steps - 1,
		Entry_CTRL,
		Entry_DISPUNIT,
		Entry_FOLLOW
	};

	typedef std::shared_ptr <NText> TxtSPtr;

	void           update_display ();
	void           update_param_txt ();
	EvtProp        change_something (int node_id, int dir);
	const piapi::ParamDescInterface &
	               use_param_desc () const;

	PageSwitcher & _page_switcher;
	LocEdit &      _loc_edit;
	Model *        _model_ptr;    // 0 = not connected
	const View *   _view_ptr;     // 0 = not connected
	PageMgrInterface *            // 0 = not connected
	               _page_ptr;
	Vec2d          _page_size;
	const ui::Font *              // 0 = not connected
	               _fnt_ptr;

	TxtSPtr        _name_sptr;
	TxtSPtr        _val_unit_sptr;
	std::array <TxtSPtr, _nbr_steps>
	               _step_sptr_arr;
	TxtSPtr        _controllers_sptr;
	TxtSPtr        _dispunit_sptr;
	TxtSPtr        _follow_sptr;

	int            _step_index;

	// Cached data
	bool           _can_dispunit_flag = false;
	std::optional <doc::ParamPresentation>
	               _pres_opt;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ParamEdit ()                               = delete;
	               ParamEdit (const ParamEdit &other)         = delete;
	               ParamEdit (ParamEdit &&other)              = delete;
	ParamEdit &    operator = (const ParamEdit &other)        = delete;
	ParamEdit &    operator = (ParamEdit &&other)             = delete;
	bool           operator == (const ParamEdit &other) const = delete;
	bool           operator != (const ParamEdit &other) const = delete;

}; // class ParamEdit



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/pg/ParamEdit.hpp"



#endif   // mfx_uitk_pg_ParamEdit_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
