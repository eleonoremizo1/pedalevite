/*****************************************************************************

        ModList.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_pg_ModList_HEADER_INCLUDED)
#define mfx_uitk_pg_ModList_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/pg/CtrlSrcNamed.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NBitmapM.h"
#include "mfx/uitk/NText.h"
#include "mfx/uitk/NWindow.h"
#include "mfx/uitk/PageInterface.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/ControlSource.h"

#include <optional>
#include <memory>
#include <string>
#include <vector>



namespace mfx
{

class LocEdit;

namespace uitk
{

class PageMgrInterface;
class PageSwitcher;

namespace pg
{



class ModList final
:	public PageInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       ModList (PageSwitcher &page_switcher, LocEdit &loc_edit, const std::vector <CtrlSrcNamed> &csn_list);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::PageInterface
	void           do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt) final;
	void           do_disconnect () final;

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::PageInterface
	EvtProp        do_handle_evt (const NodeEvt &evt) final;

	// mfx::ModelObserverInterface via mfx::uitk::PageInterface
	void           do_activate_prog (int index) final;
	void           do_set_layer_edit (doc::LayerType layer_edit) final;
	void           do_add_slot (int slot_id) final;
	void           do_remove_slot (int slot_id) final;
	void           do_set_plugin (int slot_id, const PluginInitData &pi_data) final;
	void           do_remove_plugin (int slot_id) final;
	void           do_set_param_ctrl (int slot_id, PiType type, int index, const doc::CtrlLinkSet &cls) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _badge_w = 16;
	static constexpr int _badge_h = 16;
	static constexpr int _badge_gap = 2;
	typedef std::array <uint8_t, _badge_w * _badge_h> BadgeData;

	enum Entry
	{
		Entry_TITLE_P = 0,
		Entry_TITLE_S,
		Entry_TITLE_A,
		Entry_LIST_BASE, // Should be the last of the list

		Entry_WINDOW = 1'000'000
	};

	typedef std::shared_ptr <NText> TxtSPtr;
	typedef std::shared_ptr <NWindow> WinSPtr;
	typedef std::shared_ptr <NBitmapM> BitmapMSPtr;

	class Mod
	{
	public:
		PiType         _type       = PiType_INVALID;
		int            _param_idx  = -1;
		int            _ctrl_idx   = -1; // Index in the CtrlLinkSet::_mod_arr
		ControlSource  _src;
		float          _amp        = 0;
		bool           _abs_flag   = false;
		bool           _curve_flag = false;
		bool           _clip_flag  = false;
		bool           _notch_flag = false;

		TxtSPtr        _param_sptr;
		TxtSPtr        _src_sptr;
		TxtSPtr        _amp_sptr;

		std::vector <BitmapMSPtr>
		               _badge_arr;
	};

	typedef std::vector <Mod> ModArray;

	class Dev
	{
	public:
		int            _slot_id = -1;
		ModArray       _mod_list;

		TxtSPtr        _txt_sptr;
	};

	typedef std::unique_ptr <Dev> DevUPtr;
	typedef std::vector <DevUPtr> DevArray;

	void           update_display ();
	void           check_and_create_slot (PageMgrInterface::NavLocList &nav_list, int &node_id, int &y, const Tools::NodeEntry &entry, const doc::Slot &slot);
	void           add_source_line (PageMgrInterface::NavLocList &nav_list, int &node_id, int &y, std::optional <std::string> &param_name_multilabel_opt, Dev &dev, const doc::CtrlLink &cl, int ctrl_idx, PiType type, const piapi::PluginDescInterface &desc_pi, int param_idx, int y_slot);
	void           add_badge (Mod &mod_inf, int &node_id, const BadgeData &badge, int y, const ui::PixArgb &color);
	EvtProp        go_to_ctrl_edit (int node_id);
	EvtProp        change_amp (int node_id, int dir);
	void           change_amp (const Dev &dev, const Mod &mod_inf, int dir);

	const std::vector <CtrlSrcNamed> &
	               _csn_list_base;
	PageSwitcher & _page_switcher;
	LocEdit &      _loc_edit;
	Model *        _model_ptr = nullptr;   // 0 = not connected
	const View *   _view_ptr  = nullptr;   // 0 = not connected
	PageMgrInterface *                     // 0 = not connected
	               _page_ptr  = nullptr;
	Vec2d          _page_size;
	const ui::Font *                       // 0 = not connected
	               _fnt_ptr   = nullptr;

	// Display-dependent coordinates

	// Parameter name
	int            _x_pname  = 0;
	int            _w_pname  = 0;

	// Control source name
	int            _x_sname  = 0;
	int            _w_sname  = 0;

	// Modulation amount
	int            _x_amp    = 0;
	int            _w_amp    = 0;

	int            _spacer_v = 0;

	// Contains the mod list
	WinSPtr        _win_sptr     = std::make_shared <NWindow> (Entry_WINDOW);

	TxtSPtr        _title_p_sptr = std::make_shared <NText> (Entry_TITLE_P);
	TxtSPtr        _title_s_sptr = std::make_shared <NText> (Entry_TITLE_S);
	TxtSPtr        _title_a_sptr = std::make_shared <NText> (Entry_TITLE_A);

	// Cached data and UI widgets
	DevArray       _dev_list;
	int            _nid_first = -1; // First navigable widget. -1 = not set

	// Updated with control ports
	std::vector <CtrlSrcNamed>
	               _csn_list_full = _csn_list_base;

	static const BadgeData
	               _badge_clip;
	static const BadgeData
	               _badge_notch;
	static const BadgeData
	               _badge_curve;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               ModList ()                               = delete;
	               ModList (const ModList &other)           = delete;
	               ModList (ModList &&other)                = delete;
	ModList &      operator = (const ModList &other)        = delete;
	ModList &      operator = (ModList &&other)             = delete;
	bool           operator == (const ModList &other) const = delete;
	bool           operator != (const ModList &other) const = delete;

}; // class ModList



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/pg/ModList.hpp"



#endif // mfx_uitk_pg_ModList_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
