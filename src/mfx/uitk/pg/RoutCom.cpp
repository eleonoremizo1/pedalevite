/*****************************************************************************

        RoutCom.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/Font.h"
#include "mfx/uitk/pg/RoutCom.h"
#include "mfx/Model.h"
#include "mfx/View.h"

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Convert a connection or a pin to a UI node_id
int	RoutCom::conv_cnx_to_node_id (IoType type, piapi::Dir dir, int pin_idx, int cnx_idx)
{
	assert (type >= 0);
	assert (type < IoType_NBR_ELT);
	assert (piapi::Dir_is_valid (dir));
	assert (pin_idx >= 0);
	assert (type == IoType_NAME || cnx_idx >= 0);

	const int      cnx_val = (type == IoType_NAME) ? _ofs_name : cnx_idx;

	return _entry_pin_base + (dir << _s_dir) + (pin_idx << _s_pin) + cnx_val;
}



// Returns IoType_INVALID if the node_id cannot be converted to something
// valid.
RoutCom::IoType	RoutCom::conv_node_id_to_cnx (piapi::Dir &dir, int &pin_idx, int &cnx_idx, int node_id)
{
	IoType         type = IoType_INVALID;

	if (node_id >= _entry_pin_base)
	{
		node_id -= _entry_pin_base;

		dir = piapi::Dir (node_id >> _s_dir);
		if (dir >= 0 && dir < piapi::Dir_NBR_ELT)
		{
			pin_idx = (node_id >> _s_pin) & _mask_pin;
			if (pin_idx >= 0)
			{
				cnx_idx = node_id & _mask_cnx;
				if (cnx_idx == _ofs_name)
				{
					cnx_idx = -1;
					type = IoType_NAME;
				}
				else
				{
					type = IoType_CNX;
				}
			}
		}
	}

	return type;
}



void	RoutCom::list_pin (ContainerInterface &menu, int &pos_y, PageMgrInterface::NavLocList &nav_list, Pin &pin, int pin_idx, int nbr_pins, int nbr_pins_gra, piapi::Dir dir, const std::vector <Tools::NodeEntry> &entry_list, bool exist_flag, bool node_flag, ToolsRouting::NodeMap::const_iterator it_node, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode)
{
	// Name
	const int      nid_name = RoutCom::conv_cnx_to_node_id (
		RoutCom::IoType_NAME, piapi::Dir (dir), pin_idx, -1
	);
	char           txt2_0 [255+1];
	txt2_0 [0] = '\0';
	if (exist_flag)
	{
		/*** To do: add an information about the stereo ***/
		fstb::snprintf4all (txt2_0, sizeof (txt2_0), "/%d", nbr_pins);
	}
	char           txt_0 [255+1];
	fstb::snprintf4all (
		txt_0, sizeof (txt_0),
		"%s %d%s:%s", Tools::_dir_txt_arr [dir], pin_idx + 1, txt2_0,
		(nbr_pins_gra == 0) ? " N.C." : ""
	);

	const int      h_m = fnt.get_char_h ();
	pin._name_sptr = std::make_shared <NText> (nid_name);
	pin._name_sptr->set_font (fnt);
	pin._name_sptr->set_coord (Vec2d (0, pos_y));
	pin._name_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
	pin._name_sptr->set_text (txt_0);
	pin._name_sptr->set_blend_mode (blend_mode);
	menu.push_back (pin._name_sptr);
	PageMgrInterface::add_nav (nav_list, nid_name);

	pos_y += h_m;

	// Connections
	if (node_flag && pin_idx < nbr_pins_gra)
	{
		list_pin_cnx (
			menu, pos_y, nav_list, pin, pin_idx, dir,
			entry_list, it_node->second [dir] [pin_idx],
			fnt, scr_w, blend_mode
		);
	}
}



void	RoutCom::build_possible_cnx_set (CnxEndArray &cnx_end_arr, ContainerInterface &menu, PageMgrInterface::NavLocList &nav_list, int &pos_y, const ToolsRouting::Node &start, const Arg &arg, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode, const Model &model, const View &view)
{
	assert (start.is_valid ());

	cnx_end_arr.clear ();

	// We exclude all the nodes on which a connection may cause a loop.
	// For a downstream connection, they are all the upstream nodes,
	// and vice-versa.
	const ToolsRouting::NodeMap & graph = view.use_graph ();
	std::set <ToolsRouting::Node> node_list_u;
	std::set <ToolsRouting::Node> node_list_d;
	ToolsRouting::find_coverage (node_list_u, node_list_d, graph, start);
	std::set <ToolsRouting::Node> & node_excl_list (
		  (arg._dir == piapi::Dir_IN) ? node_list_d : node_list_u
	);

	// Subtracts the exclusion list from the overall slot list
	std::vector <int> slot_arr = view.use_slot_list_aud (); // Copy
	for (int pos = int (slot_arr.size ()) - 1; pos >= 0; --pos)
	{
		const int      slot_id = slot_arr [pos];
		const auto     it      = node_excl_list.find (
			ToolsRouting::Node (doc::CnxEnd::Type_NORMAL, slot_id)
		);
		if (it != node_excl_list.end ())
		{
			slot_arr.erase (slot_arr.begin () + pos);

			// Removes the element, so next calls to find() will become faster.
			node_excl_list.erase (it);
		}
	}

	// Builds a set of additional exclusion targets: the existing connections
	std::set <doc::CnxEnd>  ce_excl_list;
	ToolsRouting::NodeMap::const_iterator it_node = graph.find (start);
	if (it_node != graph.end ())
	{
		int         pin_idx = arg._pin_idx;
		if (arg._ed_type == Arg::EdType_CNX)
		{
			pin_idx = arg._cnx.use_end (arg._dir).get_pin ();
		}

		const auto &   side = it_node->second [arg._dir];

		// Checks the pin index, as the pin may not exist in the graph. For
		// example when trying to add back a connection to a disconnected pin.
		if (pin_idx < int (side.size ()))
		{
			const auto &   pin  = side [pin_idx];
			for (const auto &cnx : pin)
			{
				ce_excl_list.insert (cnx.use_end (arg._dir));
			}
		}
	}

	const piapi::Dir  dir_opp = piapi::Dir_invert (arg._dir);
	const auto &   prog  = view.use_prog_cur ();
	const auto &   layer = prog._layer;
	std::vector <Tools::NodeEntry>   entry_list;
	Tools::extract_slot_list (entry_list, layer, model);

	// To a new empty slot
	{
		const int      h_m = fnt.get_char_h ();
		CnxEndData     c_data;
		c_data._txt_sptr = std::make_shared <NText> (_entry_new_slot);
		const auto     indent = std::string (_indent_0);
		c_data._txt_sptr->set_text_m (
			  indent + "<New FX slot>\n"
			+ indent + "<New FX>\n"
			+ indent + "<New>"
		);
		c_data._txt_sptr->set_font (fnt);
		c_data._txt_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		c_data._txt_sptr->set_coord (Vec2d (0, pos_y));
		c_data._txt_sptr->set_blend_mode (blend_mode);
		cnx_end_arr.push_back (c_data);
		menu.push_back (c_data._txt_sptr);
		PageMgrInterface::add_nav (nav_list, _entry_new_slot);
		pos_y += h_m;
	}

	int            nid = _entry_new_slot + 1;

	// Builds the list of valid connection targets
	for (const int slot_id : slot_arr)
	{
		std::array <int, piapi::Dir_NBR_ELT> nbr_pins_arr = {{ 1, 1 }};

		// Finds physical ports of the associated plug-in, if any
		int            nbr_s = 0;
		Tools::get_physical_io (
			nbr_pins_arr [piapi::Dir_IN], nbr_pins_arr [piapi::Dir_OUT], nbr_s,
			slot_id, layer, model
		);

		// Creates the connection targets
		for (int pin_idx = 0; pin_idx < nbr_pins_arr [dir_opp]; ++pin_idx)
		{
			list_target (
				cnx_end_arr, menu, nav_list, nid, pos_y,
				doc::CnxEnd (doc::CnxEnd::Type_NORMAL, slot_id, pin_idx),
				ce_excl_list, nbr_pins_arr [dir_opp], entry_list,
				arg, fnt, scr_w, blend_mode
			);
		}
	}

	// Adds the audio input/output
	doc::LayerType layer_type = view.get_layer_edit ();
	const auto &   setup      = view.use_setup ();
	const int      nbr_pins   = ChnMode_get_layer_nbr_pins (
		layer_type, setup._chn_mode, arg._dir
	);
	for (int pin_idx = 0; pin_idx < nbr_pins; ++pin_idx)
	{
		list_target (
			cnx_end_arr, menu, nav_list, nid, pos_y,
			doc::CnxEnd (doc::CnxEnd::Type_IO, 0, pin_idx),
			ce_excl_list, nbr_pins, entry_list,
			arg, fnt, scr_w, blend_mode
		);
	}

	// Adds the send/return pins
	for (int pin_idx = 0; pin_idx < Cst::_max_nbr_send; ++pin_idx)
	{
		list_target (
			cnx_end_arr, menu, nav_list, nid, pos_y,
			doc::CnxEnd (doc::CnxEnd::Type_RS, 0, pin_idx),
			ce_excl_list, Cst::_max_nbr_send, entry_list,
			arg, fnt, scr_w, blend_mode
		);
	}
}



// If a new slot has been created, returns its slot_id
std::optional <int>	RoutCom::add_or_replace_cnx (int node_id, const ToolsRouting::Node &graph_node, const CnxEndArray &cnx_end_arr, const Arg &arg, Model &model, const View &view)
{
	std::optional <int>  slot_id_opt;

	// Target end
	doc::CnxEnd    cnx_end_tgt;

	// On a new slot
	if (node_id == _entry_new_slot)
	{
		const auto     slot_id = model.add_slot ();
		cnx_end_tgt.set (doc::CnxEnd::Type_NORMAL, slot_id, 0);
		slot_id_opt.emplace (slot_id);
	}

	// Existing end
	else
	{
		const int      cnx_idx = node_id - _entry_tgt_base;
		assert (cnx_idx >= 1); // 1 because 0 is _entry_new_slot
		assert (cnx_idx < int (cnx_end_arr.size ()));
		cnx_end_tgt = cnx_end_arr [cnx_idx]._cnx_end;
	}

	const doc::Program & prog = view.use_prog_cur ();
	doc::Routing   routing = prog._layer.use_routing (); // Copy

	doc::CnxEnd    cnx_end_org;

	// Replaces connection
	if (arg._ed_type == Arg::EdType_CNX)
	{
		routing._cnx_audio_set.erase (arg._cnx);

		const piapi::Dir  dir_opp = piapi::Dir_invert (arg._dir);
		cnx_end_org = arg._cnx.use_end (dir_opp);
	}

	// Adds connection
	else
	{
		cnx_end_org.set (
			graph_node.get_type (), graph_node.get_slot_id (),
			arg._pin_idx
		);
	}

	const doc::Cnx cnx (
		(arg._dir == piapi::Dir_IN) ? cnx_end_tgt : cnx_end_org,
		(arg._dir == piapi::Dir_IN) ? cnx_end_org : cnx_end_tgt
	);
	routing._cnx_audio_set.insert (cnx);

	model.set_routing (routing);

	return slot_id_opt;
}



void	RoutCom::del_cnx (const Arg &arg, Model &model, const View &view)
{
	const doc::Program & prog = view.use_prog_cur ();
	doc::Routing   routing = prog._layer.use_routing (); // Copy

	routing._cnx_audio_set.erase (arg._cnx);

	model.set_routing (routing);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	RoutCom::list_pin_cnx (ContainerInterface &menu, int &pos_y, PageMgrInterface::NavLocList &nav_list, RoutCom::Pin &pin, int pin_idx, piapi::Dir dir, const std::vector <Tools::NodeEntry> &entry_list, const ToolsRouting::CnxSet &cnx_set, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode)
{
	const int      h_m     = fnt.get_char_h ();
	const int      nbr_cnx = int (cnx_set.size ());
	assert (nbr_cnx <= _max_nbr_cnx);
	int            cnx_idx = 0;
	ToolsRouting::CnxSet::const_iterator it_cnx = cnx_set.begin ();
	pin._cnx_arr.resize (nbr_cnx);
	while (it_cnx != cnx_set.end ())
	{
		const doc::CnxEnd &  cnx_end =
			it_cnx->use_end (dir);

		const int      nid_cnx  =
			conv_cnx_to_node_id (IoType_CNX, dir, pin_idx, cnx_idx);

		Cnx &          cnx_data = pin._cnx_arr [cnx_idx];
		cnx_data._cnx = *it_cnx;
		TxtSPtr &      cnx_sptr = cnx_data._label_sptr;
		cnx_sptr = std::make_shared <NText> (nid_cnx);
		cnx_sptr->set_font (fnt);
		cnx_sptr->set_coord (Vec2d (0, pos_y));
		cnx_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		cnx_sptr->set_blend_mode (blend_mode);
		Tools::print_cnx_name (
			*cnx_sptr, scr_w, entry_list, dir, cnx_end, _indent_0, 0
		);

		menu.push_back (cnx_sptr);
		PageMgrInterface::add_nav (nav_list, nid_cnx);

		pos_y += h_m;
		++ it_cnx;
		++ cnx_idx;
	}
}



void	RoutCom::list_target (CnxEndArray &cnx_end_arr, ContainerInterface &menu, PageMgrInterface::NavLocList &nav_list, int &nid, int &pos_y, const doc::CnxEnd &cnx_end, const std::set <doc::CnxEnd> &ce_excl_list, int nbr_pins, const std::vector <Tools::NodeEntry> &entry_list, const Arg &arg, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode)
{
	if (ce_excl_list.find (cnx_end) == ce_excl_list.end ())
	{
		const int      h_m = fnt.get_char_h ();

		CnxEndData     c_data;
		c_data._cnx_end  = cnx_end;
		c_data._txt_sptr = std::make_shared <NText> (nid);
		c_data._txt_sptr->set_font (fnt);
		c_data._txt_sptr->set_blend_mode (blend_mode);
		c_data._txt_sptr->set_frame (Vec2d (scr_w, 0), Vec2d ());
		c_data._txt_sptr->set_coord (Vec2d (0, pos_y));
		Tools::print_cnx_name (
			*c_data._txt_sptr, scr_w, entry_list,
			arg._dir, c_data._cnx_end, _indent_0, nbr_pins
		);

		cnx_end_arr.push_back (c_data);
		menu.push_back (c_data._txt_sptr);
		PageMgrInterface::add_nav (nav_list, nid);

		++ nid;
		pos_y += h_m;
	}
}



const char *	RoutCom::_indent_0 = "  ";



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
