/*****************************************************************************

        ParamEdit.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/piapi/ParamDescInterface.h"
#include "mfx/piapi/PluginDescInterface.h"
#include "mfx/piapi/PluginInfo.h"
#include "mfx/uitk/pg/ParamEdit.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/uitk/PageSwitcher.h"
#include "mfx/ui/Font.h"
#include "mfx/Cst.h"
#include "mfx/LocEdit.h"
#include "mfx/Model.h"
#include "mfx/ToolsParam.h"
#include "mfx/View.h"

#include <cassert>
#include <cmath>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



ParamEdit::ParamEdit (PageSwitcher &page_switcher, LocEdit &loc_edit)
:	_page_switcher (page_switcher)
,	_loc_edit (loc_edit)
,	_model_ptr (nullptr)
,	_view_ptr (nullptr)
,	_page_ptr (nullptr)
,	_page_size ()
,	_fnt_ptr (nullptr)
,	_name_sptr (       std::make_shared <NText> (Entry_NAME   ))
,	_val_unit_sptr (   std::make_shared <NText> (Entry_VALUNIT))
,	_step_sptr_arr ()
,	_controllers_sptr (std::make_shared <NText> (Entry_CTRL   ))
,	_dispunit_sptr (   std::make_shared <NText> (Entry_DISPUNIT))
,	_follow_sptr (     std::make_shared <NText> (Entry_FOLLOW ))
,	_step_index (0)
{
	Tools::init_multiscale_param_ctrl (
		_step_sptr_arr.data (), int (_step_sptr_arr.size ()), Entry_STEP
	);

	_controllers_sptr->set_text ("Controllers\xE2\x80\xA6");

	_name_sptr       ->set_justification (0.5f, 0   , false);
	_val_unit_sptr   ->set_justification (0.5f, 0.5f, false);
	_controllers_sptr->set_justification (0.5f, 0   , false);
	_dispunit_sptr   ->set_justification (0.5f, 0   , false);
	_follow_sptr     ->set_justification (0.5f, 0   , false);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	ParamEdit::do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt)
{
	fstb::unused (usr_ptr);
	assert (_loc_edit._slot_id >= 0);
	assert (_loc_edit._pi_type >= 0);
	assert (_loc_edit._param_index >= 0);

	_model_ptr = &model;
	_view_ptr  = &view;
	_page_ptr  = &page;
	_page_size = page_size;
	_fnt_ptr   = &fnt._m;

	_name_sptr       ->set_font (*_fnt_ptr);
	_val_unit_sptr   ->set_font (fnt._l);
	_controllers_sptr->set_font (*_fnt_ptr);
	_dispunit_sptr   ->set_font (*_fnt_ptr);
	_follow_sptr     ->set_font (*_fnt_ptr);

	const int      scr_w = _page_size [0];
	const int      x_mid =  scr_w >> 1;
	const int      h_m   = _fnt_ptr->get_char_h ();

	_name_sptr       ->set_coord (Vec2d (x_mid, h_m * 0));
	_val_unit_sptr   ->set_coord (Vec2d (x_mid, h_m * 2));
	_controllers_sptr->set_coord (Vec2d (x_mid, h_m * 4));
	_dispunit_sptr   ->set_coord (Vec2d (x_mid, h_m * 5));
	_follow_sptr     ->set_coord (Vec2d (x_mid, h_m * 6));

	_controllers_sptr->set_frame (Vec2d (scr_w >> 1, 0), Vec2d ());
	_dispunit_sptr   ->set_frame (Vec2d (scr_w >> 1, 0), Vec2d ());
	_follow_sptr     ->set_frame (Vec2d (scr_w >> 1, 0), Vec2d ());

	_page_ptr->push_back (_name_sptr);
	_page_ptr->push_back (_val_unit_sptr);

	Tools::push_multiscale_param_ctrl (
		*_page_ptr, _step_sptr_arr.data (), _nbr_steps,
		Vec2d (0, h_m * 3), scr_w, *_fnt_ptr
	);

	_page_ptr->push_back (_controllers_sptr);
	_page_ptr->push_back (_dispunit_sptr);
	_page_ptr->push_back (_follow_sptr);

	update_display ();
	_page_ptr->jump_to (Entry_STEP);
}



void	ParamEdit::do_disconnect ()
{
	// Nothing
}



MsgHandlerInterface::EvtProp	ParamEdit::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const int      node_id = evt.get_target ();

	if (evt.is_cursor ())
	{
		if (   evt.get_cursor () == NodeEvt::Curs_ENTER
		    && node_id >= Entry_STEP && node_id < Entry_STEP + _nbr_steps)
		{
			_step_index = node_id - Entry_STEP;
		}
	}

	else if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			ret_val = EvtProp_CATCH;
			if (node_id == Entry_CTRL)
			{
				_page_switcher.call_page (
					PageType_PARAM_CONTROLLERS, nullptr, node_id
				);
			}
			else if (node_id == Entry_DISPUNIT || node_id == Entry_FOLLOW)
			{
				ret_val = change_something (node_id, +1);
			}
			break;
		case Button_E:
			_page_switcher.return_page (); // pg::PageType_PARAM_LIST
			ret_val = EvtProp_CATCH;
			break;
		case Button_L:
			ret_val = change_something (node_id, -1);
			break;
		case Button_R:
			ret_val = change_something (node_id, +1);
			break;
		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



void	ParamEdit::do_set_tempo (double bpm)
{
	fstb::unused (bpm);

	if (_pres_opt)
	{
		update_display ();
	}
}



void	ParamEdit::do_activate_prog (int index)
{
	fstb::unused (index);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	ParamEdit::do_set_layer_edit (doc::LayerType layer_edit)
{
	fstb::unused (layer_edit);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	ParamEdit::do_set_param (int slot_id, int index, float val, PiType type)
{
	fstb::unused (val);

	if (   slot_id == _loc_edit._slot_id
	    && type    == _loc_edit._pi_type
	    && index   == _loc_edit._param_index)
	{
		update_param_txt ();
	}
}



void	ParamEdit::do_remove_plugin (int slot_id)
{
	if (slot_id == _loc_edit._slot_id)
	{
		_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
	}
}



void	ParamEdit::do_set_param_pres (int slot_id, PiType type, int index, const doc::ParamPresentation *pres_ptr)
{
	if (   slot_id == _loc_edit._slot_id
	    && type    == _loc_edit._pi_type
	    && index   == _loc_edit._param_index)
	{
		if (pres_ptr == nullptr)
		{
			_pres_opt.reset ();
		}
		else
		{
			assert (_can_dispunit_flag);
			_pres_opt = *pres_ptr;
		}

		update_display ();
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	ParamEdit::update_display ()
{
	const auto &   prog    = _view_ptr->use_prog_cur ();
	const int      slot_id = _loc_edit._slot_id;
	const auto &   slot    = prog._layer.use_slot (slot_id);
	const PiType   type    = _loc_edit._pi_type;
	assert (slot._settings_all.find (slot._pi_model) != slot._settings_all.end ());
	const doc::PluginSettings &  settings = slot.use_settings (type);
	const int      index   = _loc_edit._param_index;

	const auto &   param_desc = use_param_desc ();
	_can_dispunit_flag = param_desc.is_time_freq ();

	// Controllers
	const bool     ctrl_flag = settings.has_ctrl (index);
	_controllers_sptr->set_bold (ctrl_flag, true);

	// Display unit
	if (_can_dispunit_flag)
	{
		const auto     pres_it = settings._map_param_pres.find (index);
		auto           disp_mode = doc::ParamPresentation::DispMode_DEFAULT;
		if (pres_it == settings._map_param_pres.end ())
		{
			_pres_opt.reset ();
		}
		else
		{
			_pres_opt = pres_it->second;
			disp_mode = _pres_opt->_disp_mode;
		}

		constexpr std::array <
			const char *, doc::ParamPresentation::DispMode_NBR_ELT
		>              dispmode_0_arr =
		{
			"Default  ",
			"Time     ",
			"Frequency",
			"Note     ",
			"Beat     "
		};
		static_assert (dispmode_0_arr.back () != nullptr);

		std::string    txt ("Unit: ");
		txt += dispmode_0_arr [int (disp_mode)];
		_dispunit_sptr->set_text (txt);

		// Tempo sync
		txt = "Follow tempo: ";
		txt += (_pres_opt && _pres_opt->has_tempo_sync ()) ? "Yes" : "No ";
		_follow_sptr->set_text (txt);
	}

	_dispunit_sptr->show (_can_dispunit_flag);
	_follow_sptr->show (_can_dispunit_flag);

	PageMgrInterface::NavLocList  nav_list;
	for (int k = 0; k < _nbr_steps; ++k)
	{
		PageMgrInterface::add_nav (nav_list, Entry_STEP + k);
	}
	PageMgrInterface::add_nav (nav_list, Entry_CTRL);
	if (_can_dispunit_flag)
	{
		PageMgrInterface::add_nav (nav_list, Entry_DISPUNIT);
		if (doc::ParamPresentation::is_disp_mode_compatible (
			doc::ParamPresentation::DispMode_BEATS, param_desc
		))
		{
			PageMgrInterface::add_nav (nav_list, Entry_FOLLOW);
		}
	}
	_page_ptr->set_nav_layout (nav_list);

	update_param_txt ();
}



void	ParamEdit::update_param_txt ()
{
	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	Tools::set_param_text (
		*_model_ptr, *_view_ptr, _page_size [0], index, -1, slot_id, type,
		_name_sptr.get (), *_val_unit_sptr, nullptr, nullptr, true
	);
}



MsgHandlerInterface::EvtProp	ParamEdit::change_something (int node_id, int dir)
{
	auto           ret_val = EvtProp_PASS;

	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	if (node_id >= Entry_STEP && node_id < Entry_STEP + _nbr_steps)
	{
		const int      step_scale =
			_page_ptr->get_shift (PageMgrInterface::Shift::R) ? 1 : 0;
		const int      step_index_loc = _step_index + step_scale;
		const float    step    =
			float (Cst::_step_param / pow (10, step_index_loc));

		ret_val = Tools::change_param (
			*_model_ptr, *_view_ptr, slot_id, type,
			index, step, step_index_loc, dir
		);
	}

	else if (node_id == Entry_DISPUNIT)
	{
		assert (_can_dispunit_flag);

		// Gets the current value
		auto           val = doc::ParamPresentation::DispMode_DEFAULT;
		if (_pres_opt)
		{
			val = _pres_opt->_disp_mode;
		}

		// Changes the value
		const auto &   desc = use_param_desc ();
		constexpr auto len  = int (doc::ParamPresentation::DispMode_NBR_ELT);
		do
		{
			val = doc::ParamPresentation::DispMode ((int (val) + dir + len) % len);
		}
		while (! doc::ParamPresentation::is_disp_mode_compatible (val, desc));

		// Sets it to the parameter
		if (! _pres_opt)
		{
			_pres_opt = doc::ParamPresentation ();
		}
		_pres_opt->_disp_mode = val;
		_model_ptr->set_param_pres (slot_id, type, index, &*_pres_opt);

		ret_val = EvtProp_CATCH;
	}

	else if (node_id == Entry_FOLLOW)
	{
		assert (_can_dispunit_flag);
		if (dir != 0)
		{
			if (_pres_opt && _pres_opt->has_tempo_sync ())
			{
				_pres_opt->_ref_beats = -1;
			}
			else
			{
				const auto &   prog    = _view_ptr->use_prog_cur ();
				const auto &   slot    = prog._layer.use_slot (slot_id);
				const doc::PluginSettings &	settings = slot.use_settings (type);
				const auto     val_nrm = settings._param_list [index];
				const auto &   desc    = use_param_desc ();
				const auto     tempo   = _view_ptr->get_tempo ();
				if (! _pres_opt)
				{
					_pres_opt = doc::ParamPresentation ();
				}
				_pres_opt->_ref_beats = float (
					ToolsParam::conv_nrm_to_beats (val_nrm, desc, tempo)
				);
			}

			_model_ptr->set_param_pres (slot_id, type, index, &*_pres_opt);
		}
	}

	return ret_val;
}



const piapi::ParamDescInterface &	ParamEdit::use_param_desc () const
{
	assert (_view_ptr  != nullptr);
	assert (_model_ptr != nullptr);

	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	const auto &   prog = _view_ptr->use_prog_cur ();
	const auto &   slot = prog._layer.use_slot (slot_id);
	const std::string    pi_model =
		(type == PiType_MIX) ? std::string (Cst::_plugin_dwm) : slot._pi_model;
	const piapi::PluginDescInterface &  pi_desc =
		_model_ptr->get_model_desc (pi_model);
	const piapi::ParamDescInterface &   param_desc =
		pi_desc.get_param_info (piapi::ParamCateg_GLOBAL, index);

	return param_desc;
}



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
