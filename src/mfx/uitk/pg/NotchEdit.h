/*****************************************************************************

        NotchEdit.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_pg_NotchEdit_HEADER_INCLUDED)
#define mfx_uitk_pg_NotchEdit_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/NText.h"
#include "mfx/uitk/NWindow.h"
#include "mfx/uitk/PageInterface.h"
#include "mfx/uitk/PageMgrInterface.h"

#include <array>
#include <memory>
#include <vector>



namespace mfx
{

class LocEdit;

namespace uitk
{

class PageSwitcher;

namespace pg
{



class NotchEdit final
:	public PageInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       NotchEdit (PageSwitcher &page_switcher, LocEdit &loc_edit);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::PageInterface
	void           do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt) final;
	void           do_disconnect () final;

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::PageInterface
	EvtProp        do_handle_evt (const NodeEvt &evt) final;

	// mfx::ModelObserverInterface via mfx::uitk::PageInterface
	void           do_set_tempo (double bpm) final;
	void           do_activate_prog (int index) final;
	void           do_set_layer_edit (doc::LayerType layer_edit) final;
	void           do_set_param (int slot_id, int index, float val, PiType type) final;
	void           do_set_param_beats (int slot_id, int index, float beats) final;
	void           do_remove_plugin (int slot_id) final;
	void           do_set_param_ctrl (int slot_id, PiType type, int index, const doc::CtrlLinkSet &cls) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _nbr_steps = 4;

	enum Entry
	{
		Entry_WINDOW = 1000,
		Entry_ADD,
		Entry_VALUNIT,
		Entry_RADIUS,
		Entry_STEP,
		Entry_STEP_LAST = Entry_STEP + _nbr_steps - 1,
		Entry_DELIM,
		Entry_LIST
	};

	typedef std::shared_ptr <NWindow> WinSPtr;
	typedef std::shared_ptr <NText> TxtSPtr;

	void           update_display ();
	void           update_param_val ();
	void           update_ctrl_link ();
	void           add_current_value_as_notch ();
	doc::CtrlLink& use_ctrl_link (doc::CtrlLinkSet &cls) const;
	EvtProp        change_param (int node_id, int dir);
	EvtProp        change_radius (int dir);
	EvtProp        delete_notch (int node_id);
	void           set_cached_control_link ();

	PageSwitcher & _page_switcher;
	LocEdit &      _loc_edit;
	Model *        _model_ptr = nullptr; // 0 = not connected
	const View *   _view_ptr  = nullptr; // 0 = not connected
	PageMgrInterface *                   // 0 = not connected
	               _page_ptr  = nullptr;
	Vec2d          _page_size;
	const ui::Font *                     // 0 = not connected
	               _fnt_ptr   = nullptr;

	WinSPtr        _win_sptr;
	TxtSPtr        _add_sptr;
	TxtSPtr        _val_sptr;            // Value + unit
	std::array <TxtSPtr, _nbr_steps>
	               _step_sptr_arr;
	TxtSPtr        _rad_sptr;
	TxtSPtr        _delim_sptr;
	std::vector <TxtSPtr>
	               _notch_arr;

	int            _step_index = 0;

	// Cached data

	// Updated on connect()
	doc::CtrlLinkSet
	               _cls;

	// Only for display purpose
	doc::CtrlLink  _ctrl_link;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               NotchEdit ()                               = delete;
	               NotchEdit (const NotchEdit &other)         = delete;
	               NotchEdit (NotchEdit &&other)              = delete;
	NotchEdit &    operator = (const NotchEdit &other)        = delete;
	NotchEdit &    operator = (NotchEdit &&other)             = delete;
	bool           operator == (const NotchEdit &other) const = delete;
	bool           operator != (const NotchEdit &other) const = delete;

}; // class NotchEdit



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/pg/NotchEdit.hpp"



#endif // mfx_uitk_pg_NotchEdit_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
