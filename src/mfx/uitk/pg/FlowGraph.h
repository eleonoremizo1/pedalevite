/*****************************************************************************

        FlowGraph.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_pg_FlowGraph_HEADER_INCLUDED)
#define mfx_uitk_pg_FlowGraph_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/PixArgb.h"
#include "mfx/uitk/pg/RoutCom.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NBitmap.h"
#include "mfx/uitk/NBitmapM.h"
#include "mfx/uitk/NText.h"
#include "mfx/uitk/NWindow.h"
#include "mfx/uitk/PageInterface.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/GraphDisp.h"

#include <map>
#include <string>
#include <vector>



namespace mfx
{

class LocEdit;

namespace uitk
{
namespace grap
{
	class RenderCtx;
}

class PageSwitcher;

namespace pg
{



class FlowGraph final
:	public PageInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       FlowGraph (PageSwitcher &page_switcher, LocEdit &loc_edit);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::PageInterface
	void           do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt) final;
	void           do_disconnect () final;

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::PageInterface
	EvtProp        do_handle_evt (const NodeEvt &evt) final;

	// mfx::ModelObserverInterface via mfx::uitk::PageInterface
	void           do_set_prog (int bank_index, int prog_index, const doc::Program &prog) final;
	void           do_activate_prog (int index) final;
	void           do_set_layer_edit (doc::LayerType layer_edit) final;
	void           do_set_chn_mode (ChnMode mode) final;
	void           do_add_slot (int slot_id) final;
	void           do_remove_slot (int slot_id) final;
	void           do_set_routing (const doc::Routing &routing) final;
	void           do_set_plugin (int slot_id, const PluginInitData &pi_data) final;
	void           do_remove_plugin (int slot_id) final;
	void           do_set_plugin_mono (int slot_id, bool mono_flag) final;
	void           do_set_plugin_reset (int slot_id, bool reset_flag) final;
	void           do_set_param_ctrl (int slot_index, PiType type, int index, const doc::CtrlLinkSet &cls) final;
	void           do_set_signal_port (int port_id, const doc::SignalPort &port) final;
	void           do_clear_signal_port (int port_id) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Node radius in pixels. Node size is actually twice the radius (even
	// size)
	static constexpr Vec2d  _node_rad = Vec2d (8, 8);

	enum EntryNode
	{
		EntryNode_ICON = 0,
		EntryNode_NAME,
		EntryNode_OUTLINE,

		EntryNode_NBR_ELT
	};

	enum Entry
	{
		Entry_NODES     = 0,
		Entry_WIN_GRAPH = RoutCom::_entry_tgt_last + 1,
		Entry_WIN_NODES,
		Entry_CNX_AUD_M,
		Entry_CNX_AUD_L,
		Entry_CNX_AUD_R,
		Entry_CNX_SIG,
		Entry_CURS,
		Entry_HILITE,
		Entry_WIN_MENU_CTN,
		Entry_MENU_BKG,
		Entry_WIN_MENU,
		Entry_NODE_NAME,
		Entry_EDIT_FX,
		Entry_DEL_CNX,
		Entry_INS_SLOT,
		Entry_REP_CNX,
	};

	typedef std::shared_ptr <NWindow> WinSPtr;
	typedef std::shared_ptr <NText> TxtSPtr;
	typedef std::shared_ptr <NBitmapM> BitmapMSPtr;
	typedef std::shared_ptr <NBitmap> BitmapSPtr;

	class NodeGfx
	{
	public:
		BitmapSPtr     _icon_sptr;
		TxtSPtr        _name_sptr;
		BitmapMSPtr    _outline_sptr;
	};
	typedef std::vector <NodeGfx> NodeGfxArray;

	typedef std::map <int, Tools::NodeEntry> SlotMap; // [slot_id] -> NodeEntry

	// Current navigation location
	enum class UiState
	{
		graph = 0, // Audio graph
		menu_node, // In the menu related to the selected node
		menu_cnx   // Menu for changing a connection (add or replace/delete)
	};

	// Information about the pin configuration of a node
	class PinInfo
	{
	public:
		// When the node is a slot, indicates if the slot exists
		bool           _exist_flag = true;

		// Pins per direction. Default to 0
		std::array <int, piapi::Dir_NBR_ELT>
		               _nbr_pins_arr_phy {};

		// For audio ports and send/return, indicates that the node is
		// represented as a single-pin device, and which pin is used.
		// Otherwise -1 for standard slots.
		int            _unique_pin_idx = -1;
	};

	void           build_cursor ();
	void           build_highlight ();
	void           update_display ();
	void           update_graph ();
	void           update_nav ();
	void           update_graph_nodes ();
	std::string    get_node_name (int node_idx) const;
	void           update_graph_connections ();
	void           draw_connection (grap::RenderCtx &ctx, int x0, int y0, int x1, int y1, int wh, int hh, double thickness) noexcept;
	Vec2d          calc_node_coord (int node_idx) const noexcept;
	int            get_cnx_chn (int cnx_idx) const noexcept;
	void           update_outline (NBitmapM &dst, const NBitmapM &src);
	int            node_idx_to_entry (int node_idx) const noexcept;
	int            node_entry_to_idx (int node_id) const noexcept;
	void           update_menu_node ();
	PinInfo        get_pin_info (int node_idx) const;
	void           update_menu_cnx ();
	EvtProp        handle_evt_graph (const NodeEvt &evt);
	EvtProp        handle_evt_node_menu (const NodeEvt &evt);
	EvtProp        sel_pin_cnx (int node_id);
	EvtProp        handle_evt_cnx_menu (const NodeEvt &evt);
	void           insert_slot_at ();
	void           set_focus_on_slot (int slot_id);
	ToolsRouting::Node
	               conv_node_gd_to_tr (int node_idx) noexcept;

	static inline constexpr ui::PixArgb
	               make12 (int rgb12) noexcept;

	PageSwitcher & _page_switcher;
	LocEdit &      _loc_edit;
	Model *        _model_ptr = nullptr; // 0 = not connected
	const View *   _view_ptr  = nullptr; // 0 = not connected
	PageMgrInterface *                   // 0 = not connected
	               _page_ptr  = nullptr;
	Vec2d          _page_size;
	const FontSet *                      // 0 = not connected
	               _fs_ptr    = nullptr;

	UiState        _ui_state  = UiState::graph;

	// Navigation list for the graph (cached)
	PageMgrInterface::NavLocList
	               _nav_graph;

	/*
	Widget hierarchy and display order:
	Page
	 +- win_graph
	 |   +- cnx_sig
	 |   +- cnx_aud_r
	 |   +- cnx_aud_l
	 |   +- cnx_aud_m
	 |   +- highlight
	 |   +- win_nodes
	 |   |   +- all node icons
	 |   |   `- all node names + outlines (outline before name)
	 |   `- cursor
	 `- win_menu_ctn (shown when menu is open)
	     +- menu_bkg
	     `- win_menu
	*/

	WinSPtr        _win_graph_sptr;
	WinSPtr        _win_nodes_sptr;
	WinSPtr        _win_menu_ctn_sptr;
	WinSPtr        _win_menu_sptr;
	BitmapMSPtr    _cnx_aud_m_sptr;
	BitmapMSPtr    _cnx_aud_l_sptr;
	BitmapMSPtr    _cnx_aud_r_sptr;
	BitmapMSPtr    _cnx_sig_sptr;
	
	NodeGfxArray   _node_gfx_arr;

	BitmapMSPtr    _cursor_sptr;
	BitmapMSPtr    _highlight_sptr;

	BitmapMSPtr    _menu_bkg_sptr;
	TxtSPtr        _node_name_sptr;
	TxtSPtr        _edit_fx_sptr;
	TxtSPtr        _del_cnx_sptr;
	TxtSPtr        _ins_slot_sptr;
	TxtSPtr        _rep_cnx_sptr;

	GraphDisp		_graph_disp;

	// Cached data

	doc::LayerType _layer_type;
	ChnMode        _chn_mode;

	// These members are updated with the graph and reused in the menus.
	std::vector <Tools::NodeEntry>
	               _slot_list;
	SlotMap        _slot_map;

	// Currently selected node index, >= 0. -1 if none or unavailable
	int            _node_cur = -1;

	// In Pin/connection menu, saves the edited node, so we can return to this
	// entry when exiting the target connection menu.
	int            _node_id_menu_node = -1;

	// Currently edited connection, in UiState::menu_cnx state
	RoutCom::Arg   _arg;

	RoutCom::SideArray
	               _side_arr;

	// First element is the "new slot" entry
	RoutCom::CnxEndArray
	               _cnx_end_arr;

	// First node id of the menu, when one is active.
	int            _node_id_top = -1;

	static const ui::PixArgb _col_black;
	static const ui::PixArgb _col_grey;
	static const ui::PixArgb _col_vlgrey;
	static const ui::PixArgb _col_white;
	static const ui::PixArgb _col_dgreen;
	static const ui::PixArgb _col_magenta_bright;
	static const ui::PixArgb _col_red;
	static const ui::PixArgb _col_blue;
	static const ui::PixArgb _col_green;
	static const ui::PixArgb _col_yellow;
	static const ui::PixArgb _col_orange;
	static const ui::PixArgb _col_purple;
	static const ui::PixArgb _col_violet;
	static const ui::PixArgb _col_magenta;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               FlowGraph ()                               = delete;
	               FlowGraph (const FlowGraph &other)         = delete;
	               FlowGraph (FlowGraph &&other)              = delete;
	FlowGraph &    operator = (const FlowGraph &other)        = delete;
	FlowGraph &    operator = (FlowGraph &&other)             = delete;
	bool           operator == (const FlowGraph &other) const = delete;
	bool           operator != (const FlowGraph &other) const = delete;

}; // class FlowGraph



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/pg/FlowGraph.hpp"



#endif // mfx_uitk_pg_FlowGraph_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
