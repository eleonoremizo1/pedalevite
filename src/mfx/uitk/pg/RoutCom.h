/*****************************************************************************

        RoutCom.h
        Author: Laurent de Soras, 2024

Shared code and data for managing routing menus displaying connections to
audio devices.

There are two distinct menus:
- The menu for pin/connection selection, given a device (slot, audio I/O or
send/return)
- The menu to select a target for a new connection or replace an existing one.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_pg_RoutCom_HEADER_INCLUDED)
#define mfx_uitk_pg_RoutCom_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/Cnx.h"
#include "mfx/piapi/Dir.h"
#include "mfx/piapi/PluginInterface.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NText.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/ToolsRouting.h"

#include <array>
#include <memory>
#include <optional>
#include <vector>



namespace mfx
{
namespace uitk
{
namespace pg
{



class RoutCom
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef std::shared_ptr <NText> TxtSPtr;

	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Pin/connection menu

	class Cnx
	{
	public:
		doc::Cnx       _cnx;
		TxtSPtr        _label_sptr;
	};
	class Pin
	{
	public:
		TxtSPtr        _name_sptr;
		std::vector <Cnx>
		               _cnx_arr;
	};
	typedef std::vector <Pin> Side;
	typedef std::array <Side, piapi::Dir_NBR_ELT> SideArray;

	enum IoType
	{
		IoType_INVALID = -1,

		IoType_NAME = 0, // Pin
		IoType_CNX,      // Connection

		IoType_NBR_ELT
	};

	// Entry coding for the initial pin/connection menu:
	// Base + (direction | pin | cnx_type)
	// direction: Dir       << _s_dir
	// pin      : pin_index << _s_pin
	// cnx_type : _ofs_name for pins, index for connections
	// Connection indexes is the order in ToolsRooting::CnxSet
	static constexpr int _entry_pin_base = 500'000;

	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Connection target menu

	class Arg
	{
	public:
		/*** To do: EdType is similar to IoType, keep only one of them ***/
		enum EdType
		{
			EdType_INVALID = -1,

			EdType_CNX = 0,
			EdType_PIN,

			EdType_NBR_ELT
		};

		EdType         _ed_type = EdType_INVALID;     // I/-, edit connection or pin
		piapi::Dir     _dir     = piapi::Dir_INVALID; // I/-, edited direction (target is the opposite)
		doc::Cnx       _cnx;          // I/-, edited connection
		int            _pin_idx = -1; // I/-, edited pin
	};

	class CnxEndData
	{
	public:
		doc::CnxEnd   _cnx_end;
		TxtSPtr       _txt_sptr;
	};

	typedef std::vector <CnxEndData> CnxEndArray;

	static constexpr int _entry_tgt_base = _entry_pin_base + 500'000;
	static constexpr int _entry_new_slot = _entry_tgt_base;
	static constexpr int _entry_tgt_beg  = _entry_new_slot + 1;
	static constexpr int _entry_tgt_last = _entry_new_slot + 500'000 - 1;

	// Pin/connection menu
	static int     conv_cnx_to_node_id (IoType type, piapi::Dir dir, int pin_idx, int cnx_idx);
	static IoType  conv_node_id_to_cnx (piapi::Dir &dir, int &pin_idx, int &cnx_idx, int node_id);

	static void    list_pin (ContainerInterface &menu, int &pos_y, PageMgrInterface::NavLocList &nav_list, Pin &pin, int pin_idx, int nbr_pins, int nbr_pins_gra, piapi::Dir dir, const std::vector <Tools::NodeEntry> &entry_list, bool exist_flag, bool node_flag, ToolsRouting::NodeMap::const_iterator it_node, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode);

	// Connection target menu
	static void    build_possible_cnx_set (CnxEndArray &cnx_end_arr, ContainerInterface &menu, PageMgrInterface::NavLocList &nav_list, int &y_pos, const ToolsRouting::Node &start, const Arg &arg, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode, const Model &model, const View &view);
	static std::optional <int>
	               add_or_replace_cnx (int node_id, const ToolsRouting::Node &graph_node, const CnxEndArray &cnx_end_arr, const Arg &arg, Model &model, const View &view);
	static void    del_cnx (const Arg &arg, Model &model, const View &view);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Bit position for the direction field
	static constexpr int _s_dir    = 12;

	// Bit position for the pin field
	static constexpr int _s_pin    =  8;
	static_assert (
		   1 << (_s_dir - _s_pin)
		>= piapi::PluginInterface::_max_nbr_pins
	);
	static constexpr int _mask_cnx = ((1 <<           _s_pin)  - 1);
	static constexpr int _mask_pin = ((1 << (_s_dir - _s_pin)) - 1);
	static constexpr int _ofs_name = _mask_cnx;
	static constexpr int _max_nbr_cnx = _ofs_name;

	// Pin/connection menu
	static void    list_pin_cnx (ContainerInterface &menu, int &pos_y, PageMgrInterface::NavLocList &nav_list, RoutCom::Pin &pin, int pin_idx, piapi::Dir dir, const std::vector <Tools::NodeEntry> &entry_list, const ToolsRouting::CnxSet &cnx_set, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode);

	// Connection target menu
	static void    list_target (CnxEndArray &cnx_end_arr, ContainerInterface &menu, PageMgrInterface::NavLocList &nav_list, int &nid, int &pos_y, const doc::CnxEnd &cnx_end, const std::set <doc::CnxEnd> &ce_excl_list, int nbr_pins, const std::vector <Tools::NodeEntry> &entry_list, const Arg &arg, const ui::Font &fnt, int scr_w, ui::DisplayInterface::BlendMode blend_mode);

	static const char *
	               _indent_0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               RoutCom ()                               = delete;
	               RoutCom (const RoutCom &other)           = delete;
	               RoutCom (RoutCom &&other)                = delete;
	RoutCom &      operator = (const RoutCom &other)        = delete;
	RoutCom &      operator = (RoutCom &&other)             = delete;
	bool           operator == (const RoutCom &other) const = delete;
	bool           operator != (const RoutCom &other) const = delete;

}; // class RoutCom



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/pg/RoutCom.hpp"



#endif // mfx_uitk_pg_RoutCom_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
