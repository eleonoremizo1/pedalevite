/*****************************************************************************

        ModList.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/doc/Layer.h"
#include "mfx/pi/param/Tools.h"
#include "mfx/piapi/ParamDescInterface.h"
#include "mfx/piapi/PluginDescInterface.h"
#include "mfx/ui/Font.h"
#include "mfx/uitk/pg/ModList.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/PageSwitcher.h"
#include "mfx/LocEdit.h"
#include "mfx/Model.h"
#include "mfx/View.h"

#include <cassert>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



ModList::ModList (PageSwitcher &page_switcher, LocEdit &loc_edit, const std::vector <CtrlSrcNamed> &csn_list)
:	_csn_list_base (csn_list)
,	_page_switcher (page_switcher)
,	_loc_edit (loc_edit)
{
	_title_a_sptr->set_justification (1, 0, false);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	ModList::do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt)
{
	fstb::unused (usr_ptr);

	_model_ptr = &model;
	_view_ptr  = &view;
	_page_ptr  = &page;
	_page_size = page_size;
	_fnt_ptr   = &fnt._s;

	const int      h_m   = _fnt_ptr->get_char_h ();
	const int      w_m   = _fnt_ptr->get_char_w (U'0');
	const int      win_w = _page_size [0];

	// Indent and amp widths are fixed, then parameter and modulation source
	// names take the remaining.
	_x_pname = w_m * 2;
	_w_amp   = w_m * 9;
	const int      w_pm = win_w - _x_pname - _w_amp;
	_w_pname = w_pm * 3 / 5;
	_w_sname = w_pm - _w_pname;
	_x_sname = _x_pname + _w_pname;
	_x_amp   = _x_sname + _w_sname;

	_spacer_v = h_m >> 2;

	_page_ptr->clear_all_nodes ();

	_title_p_sptr->set_font (*_fnt_ptr);
	_title_s_sptr->set_font (*_fnt_ptr);
	_title_a_sptr->set_font (*_fnt_ptr);

	constexpr auto title_color = ui::PixArgb { 128, 128, 128, 255 };
	_title_p_sptr->set_color (title_color);
	_title_s_sptr->set_color (title_color);
	_title_a_sptr->set_color (title_color);

	_title_p_sptr->set_coord (Vec2d (0              , 0));
	_title_s_sptr->set_coord (Vec2d (_x_sname       , 0));
	_title_a_sptr->set_coord (Vec2d (_x_amp + _w_amp, 0));

	_page_ptr->push_back (_title_p_sptr);
	_page_ptr->push_back (_title_s_sptr);
	_page_ptr->push_back (_title_a_sptr);

	const auto     win_pos = Vec2d (0, h_m + _spacer_v);
	_win_sptr->set_coord (win_pos);
	_win_sptr->set_size (_page_size - win_pos, Vec2d ());
	_win_sptr->set_disp_pos (Vec2d ());

	_page_ptr->push_back (_win_sptr);

	update_display ();
}



void	ModList::do_disconnect ()
{
	// Nothing
}



MsgHandlerInterface::EvtProp	ModList::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const int      node_id = evt.get_target ();
	if (evt.is_cursor ())
	{
		// Makes sure we make the plug-in name appear when we get back to the
		// beginning of the list.
		if (node_id == _nid_first)
		{
			_win_sptr->set_disp_pos (Vec2d ());
		}
	}

	if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			ret_val = go_to_ctrl_edit (node_id);
			break;
		case Button_E:
			_page_switcher.return_page ();
			ret_val = EvtProp_CATCH;
			break;
		case Button_L:
			ret_val = change_amp (node_id, -1);
			break;
		case Button_R:
			ret_val = change_amp (node_id, +1);
			break;
		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



void	ModList::do_activate_prog (int index)
{
	fstb::unused (index);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	ModList::do_set_layer_edit (doc::LayerType layer_edit)
{
	fstb::unused (layer_edit);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	ModList::do_add_slot (int slot_id)
{
	fstb::unused (slot_id);

	update_display ();
}



void	ModList::do_remove_slot (int slot_id)
{
	fstb::unused (slot_id);

	update_display ();
}



void	ModList::do_set_plugin (int slot_id, const PluginInitData &pi_data)
{
	fstb::unused (slot_id, pi_data);

	update_display ();
}



void	ModList::do_remove_plugin (int slot_id)
{
	fstb::unused (slot_id);

	update_display ();
}



void	ModList::do_set_param_ctrl (int slot_id, PiType type, int index, const doc::CtrlLinkSet &cls)
{
	fstb::unused (slot_id, type, index, cls);

	update_display ();
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	ModList::update_display ()
{
	PageMgrInterface::NavLocList  nav_list;

	// Resets everything
	_win_sptr->clear_all_nodes ();
	_dev_list.clear ();

	// Collects data

	// Control port name update
	_csn_list_full = Tools::complete_source_list_w_ports (
		*_model_ptr, *_view_ptr, _csn_list_base
	);

	const doc::Program & prog  = _view_ptr->use_prog_cur ();
	const doc::Layer &   layer = prog._layer;

	// Gets the name of each plug-in with their instance number
	std::vector <Tools::NodeEntry>   entry_list;
	Tools::extract_slot_list (entry_list, layer, *_model_ptr);

	_nid_first = -1;
	int            node_id = Entry_LIST_BASE;
	int            y       = 0;
	for (const auto &entry : entry_list)
	{
		if (! entry._type.empty ())
		{
			const int      slot_id = entry._slot_id;
			assert (! layer.is_slot_empty (slot_id));
			const auto &   slot    = layer.use_slot (slot_id);

			check_and_create_slot (nav_list, node_id, y, entry, slot);
		}
	}

	if (_dev_list.empty ())
	{
		_title_p_sptr->set_text ("No modulation");
		_title_s_sptr->set_text ("");
		_title_a_sptr->set_text ("");
	}
	else
	{
		_title_p_sptr->set_text ("FX / parameter");
		_title_s_sptr->set_text ("Mod source");
		_title_a_sptr->set_text ("Amount");
	}

	_page_ptr->set_nav_layout (nav_list);
	_win_sptr->invalidate_all ();
}



void	ModList::check_and_create_slot (PageMgrInterface::NavLocList &nav_list, int &node_id, int &y, const Tools::NodeEntry &entry, const doc::Slot &slot)
{
	const auto     y_slot = y;
	Dev            dev;

	for (int type_idx = 0; type_idx < PiType_NBR_ELT; ++type_idx)
	{
		const auto     type = PiType (type_idx);
		const doc::PluginSettings &   settings = slot.use_settings (type);

		const std::string    pi_model =
			  (type == PiType_MIX)
			? std::string (Cst::_plugin_dwm)
			: slot._pi_model;
		const piapi::PluginDescInterface &  desc_pi =
			_model_ptr->get_model_desc (pi_model);

		for (const auto &cls_vt : settings._map_param_ctrl)
		{
			const int      param_idx       = cls_vt.first;
			const doc::CtrlLinkSet &   cls = cls_vt.second;

			// We will fetch the parameter name only at the first validated
			// modulation.
			std::optional <std::string>   param_name_multilabel_opt;

			// Scans modulations for the parameter (if any)
			if (cls._bind_sptr.get () != nullptr)
			{
				add_source_line (
					nav_list, node_id, y, param_name_multilabel_opt, dev,
					*(cls._bind_sptr), -1, type, desc_pi, param_idx, y_slot
				);
			}

			for (int ctrl_idx = 0
			;	ctrl_idx < int (cls._mod_arr.size ())
			;	++ ctrl_idx)
			{
				const auto &   cl_sptr = cls._mod_arr [ctrl_idx];
				if (cl_sptr.get () != nullptr)
				{
					add_source_line (
						nav_list, node_id, y, param_name_multilabel_opt, dev,
						*cl_sptr, ctrl_idx, type, desc_pi, param_idx, y_slot
					);
				}
			}
		}
	}

	if (! dev._mod_list.empty ())
	{
		dev._slot_id  = entry._slot_id;
		dev._txt_sptr = std::make_shared <NText> (node_id);
		++ node_id;
		dev._txt_sptr->set_font (*_fnt_ptr);
		dev._txt_sptr->set_coord (Vec2d (0, y_slot));

		// Plug-in name
		const auto     win_bb     = _win_sptr->get_bounding_box ();
		const auto     win_w      = (win_bb [1] - win_bb [0]) [0];
		const auto     multilabel = Tools::build_slot_name_with_index (entry);
		std::string    txt        = pi::param::Tools::print_name_bestfit (
			win_w, multilabel.c_str (), *dev._txt_sptr, &NText::get_char_width
		);
		dev._txt_sptr->set_text (txt);

		_win_sptr->push_back (dev._txt_sptr);
		_dev_list.push_back (std::make_unique <Dev> (std::move (dev)));

		y += _spacer_v;
	}
}



// ctrl_idx < 0: direct link
void	ModList::add_source_line (PageMgrInterface::NavLocList &nav_list, int &node_id, int &y, std::optional <std::string> &param_name_multilabel_opt, Dev &dev, const doc::CtrlLink &cl, int ctrl_idx, PiType type, const piapi::PluginDescInterface &desc_pi, int param_idx, int y_slot)
{
	Mod          mod_inf;
	mod_inf._type       = type;
	mod_inf._param_idx  = param_idx;
	mod_inf._ctrl_idx   = ctrl_idx;
	mod_inf._src        = cl._source;
	mod_inf._amp        = cl._amp;
	mod_inf._abs_flag   = (ctrl_idx < 0);
	mod_inf._curve_flag = (cl._curve != ControlCurve_LINEAR);
	mod_inf._clip_flag  = cl._clip_flag;
	mod_inf._notch_flag = ! cl._notch_list.empty ();

	const int      h_m  = _fnt_ptr->get_char_h ();

	// Makes room for the plug-in name
	if (y == y_slot)
	{
		y += h_m;
	}

	// First, retrieves the parameter name
	if (! param_name_multilabel_opt.has_value ())
	{
		const auto &   desc_param = desc_pi.get_param_info (
			mfx::piapi::ParamCateg_GLOBAL, param_idx
		);
		param_name_multilabel_opt = desc_param.get_name (0);
	}

	// Source name
	const std::string src_name_multilabel =
		Tools::find_ctrl_name (cl._source, _csn_list_full);

	// Amount
	char           txt_0 [127+1] = "Direct";
	if (! mod_inf._abs_flag)
	{
		fstb::snprintf4all (txt_0, sizeof (txt_0),
			"%+7.2f %%", cl._amp * 100
		);
	}

	// Builds the widgets
	const int       node_id_p = node_id; ++ node_id;
	const int       node_id_s = node_id; ++ node_id;
	const int       node_id_a = node_id; ++ node_id;

	mod_inf._param_sptr = std::make_shared <NText> (node_id_p);
	mod_inf._src_sptr   = std::make_shared <NText> (node_id_s);
	mod_inf._amp_sptr   = std::make_shared <NText> (node_id_a);

	// Parameter name
	mod_inf._param_sptr->set_font (*_fnt_ptr);
	mod_inf._param_sptr->set_frame (Vec2d (_w_pname, 0), Vec2d ());
	mod_inf._param_sptr->set_coord (Vec2d (_x_pname, y));
	std::string    pname = pi::param::Tools::print_name_bestfit (
		_w_pname, param_name_multilabel_opt->c_str (),
		*mod_inf._param_sptr, &NText::get_char_width
	);
	mod_inf._param_sptr->set_text (pname);

	// Modulation source name
	mod_inf._src_sptr->set_font (*_fnt_ptr);
	mod_inf._src_sptr->set_frame (Vec2d (_w_sname, 0), Vec2d ());
	mod_inf._src_sptr->set_coord (Vec2d (_x_sname, y));
	std::string    sname = pi::param::Tools::print_name_bestfit (
		_w_sname, src_name_multilabel.c_str (),
		*mod_inf._src_sptr, &NText::get_char_width
	);
	mod_inf._src_sptr->set_text (sname);

	// Modulation amount
	mod_inf._amp_sptr->set_font (*_fnt_ptr);
	mod_inf._amp_sptr->set_justification (1, 0, false);
	mod_inf._amp_sptr->set_frame (Vec2d (_w_amp, 0), Vec2d ());
	mod_inf._amp_sptr->set_coord (Vec2d (_x_amp + _w_amp, y));
	mod_inf._amp_sptr->set_text (txt_0);

	// Registers the text widgets
	_win_sptr->push_back (mod_inf._param_sptr);
	_win_sptr->push_back (mod_inf._src_sptr);
	_win_sptr->push_back (mod_inf._amp_sptr);
	PageMgrInterface::add_nav (nav_list, node_id_a);
	if (_nid_first < 0)
	{
		_nid_first = node_id_a;
	}

	// Badges
	mod_inf._badge_arr.clear ();
	if (mod_inf._curve_flag)
	{
		const auto     color = ui::PixArgb { 255, 255, 0, 255 }; // Cyan
		add_badge (mod_inf, node_id, _badge_curve, y, color);
	}
	if (mod_inf._clip_flag)
	{
		const auto     color = ui::PixArgb { 255, 0, 255, 255 }; // Magenta
		add_badge (mod_inf, node_id, _badge_clip , y, color);
	}
	if (mod_inf._notch_flag)
	{
		const auto     color = ui::PixArgb { 0, 255, 255, 255 }; // Yellow
		add_badge (mod_inf, node_id, _badge_notch, y, color);
	}

	dev._mod_list.push_back (mod_inf);
	y += h_m;
}



void	ModList::add_badge (Mod &mod_inf, int &node_id, const BadgeData &badge, int y, const ui::PixArgb &color)
{
	const int      badge_idx  = int (mod_inf._badge_arr.size ());
	auto           badge_sptr = std::make_shared <NBitmapM> (node_id);
	++ node_id;

	badge_sptr->set_size (Vec2d (_badge_w, _badge_h));

	const int      bl_m  = _fnt_ptr->get_baseline ();
	const int      pos_x = _x_amp - (badge_idx + 1) * (_badge_w + _badge_gap);
	const int      pos_y = y + ((bl_m - _badge_h) >> 1);
	badge_sptr->set_coord (Vec2d (pos_x, pos_y));

	badge_sptr->set_color (color);

	uint8_t *      dst_ptr  = badge_sptr->use_buffer ();
	const uint8_t* src_ptr  = badge.data ();
	const int      stride_d = badge_sptr->get_stride ();
	for (int r = 0; r < _badge_h; ++r)
	{
		std::copy (src_ptr, src_ptr + _badge_w, dst_ptr);
		dst_ptr += stride_d;
		src_ptr += _badge_w;
	}

	mod_inf._badge_arr.push_back (badge_sptr);
	_win_sptr->push_back (badge_sptr);
}



MsgHandlerInterface::EvtProp	ModList::go_to_ctrl_edit (int node_id)
{
	// Locates the modulation from node_id
	for (const auto &dev_sptr : _dev_list)
	{
		const auto &   dev = *dev_sptr;
		for (const auto &mod_inf : dev._mod_list)
		{
			if (node_id == mod_inf._amp_sptr->get_id ())
			{
				_loc_edit._slot_id       = dev._slot_id;
				_loc_edit._pi_type       = mod_inf._type;
				_loc_edit._param_index   = mod_inf._param_idx;
				_loc_edit._ctrl_index    =
					(mod_inf._abs_flag) ? 0 : mod_inf._ctrl_idx;
				_loc_edit._ctrl_abs_flag = mod_inf._abs_flag;

				_page_switcher.call_page (
					pg::PageType_CTRL_EDIT, nullptr, node_id
				);
				return EvtProp_CATCH;
			}
		}
	}

	return EvtProp_PASS;
}



MsgHandlerInterface::EvtProp	ModList::change_amp (int node_id, int dir)
{
	// Locates the modulation from node_id
	for (const auto &dev_sptr : _dev_list)
	{
		const auto &   dev = *dev_sptr;
		for (const auto &mod_inf : dev._mod_list)
		{
			if (node_id == mod_inf._amp_sptr->get_id ())
			{
				// Found, is it tweakable?
				if (! mod_inf._abs_flag)
				{
					change_amp (dev, mod_inf, dir);
					// From there, loop iterators are invalid.
					return EvtProp_CATCH;
				}
				return EvtProp_PASS;
			}
		}
	}

	return EvtProp_PASS;
}



void	ModList::change_amp (const Dev &dev, const Mod &mod_inf, int dir)
{
	assert (! mod_inf._abs_flag);

	const auto     slot_id   = dev._slot_id;
	const auto     type      = mod_inf._type;
	const auto     param_idx = mod_inf._param_idx;

	// Retrieves the current CtrlLinkSet
	const doc::Program & prog  = _view_ptr->use_prog_cur ();
	const doc::Layer &   layer = prog._layer;
	const auto &         slot  = layer.use_slot (slot_id);
	const doc::PluginSettings &   settings =
		slot.use_settings (mod_inf._type);
	auto                 cls   = // Makes a copy
		settings.use_ctrl_link_set (param_idx);

	// Finds the CtrlLink
	assert (mod_inf._ctrl_idx >= 0);
	assert (mod_inf._ctrl_idx < int (cls._mod_arr.size ()));
	auto &            cl_sptr = cls._mod_arr [mod_inf._ctrl_idx];
	assert (cl_sptr.get () != nullptr);
	auto &            cl = *cl_sptr;

	// Computes the new value
	const int      step_index = 0;
	const int      step_scale =
		_page_ptr->get_shift (PageMgrInterface::Shift::R) ? 1 : 0;
	const float    step       =
		float (Cst::_step_param / pow (10, step_index + step_scale));

	cl._amp += step * float (dir);
	cl._amp  = fstb::limit (
		cl._amp,
		-doc::CtrlLink::_max_amp,
		+doc::CtrlLink::_max_amp
	);

	// Sets the value
	_model_ptr->set_param_ctrl (slot_id, type, param_idx, cls);
}



// Define for solid icons, undef for outlined icons
#undef ModList_BADGE_SOLID

const ModList::BadgeData	ModList::_badge_clip =
{{
#if defined (ModList_BADGE_SOLID)
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	255,255,255,255,255,255,255,255,255,183,0,0,0,0,255,255,
	255,255,255,255,255,255,255,255,240,0,255,255,255,255,255,255,
	255,255,255,255,255,255,255,255,112,208,255,255,255,255,255,255,
	255,255,255,255,255,255,255,240,112,255,255,255,255,255,255,255,
	255,255,255,255,255,255,255,112,240,255,255,255,255,255,255,255,
	255,255,255,255,255,255,208,112,255,255,255,255,255,255,255,255,
	255,255,255,255,255,255,0,240,255,255,255,255,255,255,255,255,
	255,255,0,0,0,0,183,255,255,255,255,255,255,255,255,255,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0
#else
	0,64,192,255,255,255,255,255,255,255,255,255,255,255,64,0,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	192,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,0,0,192,255,255,255,255,0,255,
	255,0,0,0,0,0,0,0,64,255,0,0,0,0,0,255,
	255,0,0,0,0,0,0,0,223,128,0,0,0,0,0,255,
	255,0,0,0,0,0,0,64,223,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,223,64,0,0,0,0,0,0,255,
	255,0,0,0,0,0,128,223,0,0,0,0,0,0,0,255,
	255,0,0,0,0,0,255,64,0,0,0,0,0,0,0,255,
	255,0,255,255,255,255,192,0,0,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	0,64,255,255,255,255,255,255,255,255,255,255,255,255,64,0
#endif
}};

const ModList::BadgeData	ModList::_badge_notch =
{{
#if defined (ModList_BADGE_SOLID)
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	255,255,255,255,255,255,224,63,63,224,255,255,255,255,255,255,
	255,255,255,255,255,255,63,0,0,63,255,255,255,255,255,255,
	255,255,255,255,255,255,63,0,0,63,255,255,255,255,255,255,
	255,255,0,0,0,255,224,63,63,224,255,0,0,0,255,255,
	255,255,255,255,0,255,255,255,255,255,255,0,255,255,255,255,
	255,255,255,255,63,192,255,255,255,255,192,63,255,255,255,255,
	255,255,255,255,224,32,192,255,255,192,32,224,255,255,255,255,
	255,255,255,255,255,224,63,0,0,63,224,255,255,255,255,255,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0
#else
	0,64,192,255,255,255,255,255,255,255,255,255,255,255,64,0,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	192,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,192,192,0,0,0,0,0,0,255,
	255,0,0,0,0,0,192,255,255,192,0,0,0,0,0,255,
	255,0,0,0,0,0,192,255,255,192,0,0,0,0,0,255,
	255,0,255,255,255,0,0,192,192,0,0,255,255,255,0,255,
	255,0,0,0,255,0,0,0,0,0,0,255,0,0,0,255,
	255,0,0,0,192,128,0,0,0,0,128,192,0,0,0,255,
	255,0,0,0,64,223,128,0,0,128,223,64,0,0,0,255,
	255,0,0,0,0,64,192,255,255,192,64,0,0,0,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	0,64,255,255,255,255,255,255,255,255,255,255,255,255,64,0
#endif
}};

const ModList::BadgeData	ModList::_badge_curve =
{{
#if defined (ModList_BADGE_SOLID)
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	255,255,255,255,255,255,255,255,255,255,255,255,255,64,255,255,
	255,255,255,255,255,255,255,255,255,255,255,255,218,118,255,255,
	255,255,255,255,255,255,255,255,255,255,255,255,118,202,255,255,
	255,255,255,255,255,255,255,255,255,255,255,232,156,255,255,255,
	255,255,255,255,255,255,255,255,255,255,232,118,226,255,255,255,
	255,255,255,255,255,255,255,255,255,202,118,202,255,255,255,255,
	255,255,255,255,239,226,170,118,0,156,218,255,255,255,255,255,
	255,255,64,0,83,118,156,218,239,255,255,255,255,255,255,255,
	240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,240,
	192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,
	64,223,255,255,255,255,255,255,255,255,255,255,255,255,223,64,
	0,64,192,240,255,255,255,255,255,255,255,255,240,192,64,0
#else
	0,64,192,255,255,255,255,255,255,255,255,255,255,255,64,0,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	192,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,240,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,96,223,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,223,128,0,255,
	255,0,0,0,0,0,0,0,0,0,0,64,192,0,0,255,
	255,0,0,0,0,0,0,0,0,0,64,223,80,0,0,255,
	255,0,0,0,0,0,0,0,0,128,223,128,0,0,0,255,
	255,0,0,0,48,80,176,223,255,192,96,0,0,0,0,255,
	255,0,192,255,240,223,192,96,48,0,0,0,0,0,0,255,
	255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,
	255,128,0,0,0,0,0,0,0,0,0,0,0,0,128,255,
	64,223,128,0,0,0,0,0,0,0,0,0,0,128,223,64,
	0,64,255,255,255,255,255,255,255,255,255,255,255,255,64,0
#endif
}};

#undef ModList_BADGE_SOLID



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
