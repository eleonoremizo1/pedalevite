/*****************************************************************************

        NotchEdit.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/Font.h"
#include "mfx/uitk/pg/NotchEdit.h"
#include "mfx/uitk/pg/Tools.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/PageMgrInterface.h"
#include "mfx/uitk/PageSwitcher.h"
#include "mfx/features.h"
#include "mfx/LocEdit.h"
#include "mfx/Model.h"
#include "mfx/View.h"

#include <cassert>
#include <cmath>



namespace mfx
{
namespace uitk
{
namespace pg
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



NotchEdit::NotchEdit (PageSwitcher &page_switcher, LocEdit &loc_edit)
:	_page_switcher (page_switcher)
,	_loc_edit (loc_edit)
,	_win_sptr (std::make_shared <NWindow> (Entry_WINDOW ))
,	_add_sptr (std::make_shared <NText  > (Entry_ADD    ))
,	_val_sptr (std::make_shared <NText  > (Entry_VALUNIT))
,	_rad_sptr (std::make_shared <NText  > (Entry_RADIUS ))
,	_delim_sptr (std::make_shared <NText  > (Entry_DELIM ))
{
	Tools::init_multiscale_param_ctrl (
		_step_sptr_arr.data (), _nbr_steps, Entry_STEP
	);

	_add_sptr  ->set_justification (0.5f, 0, false);
	_val_sptr  ->set_justification (1.0f, 0, false);
	_rad_sptr  ->set_justification (0.5f, 0, false);
	_delim_sptr->set_justification (0.5f, 0, false);

	_add_sptr->set_text ("Add as notch");
	_delim_sptr->set_text ("---------------------");
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NotchEdit::do_connect (Model &model, const View &view, PageMgrInterface &page, Vec2d page_size, void *usr_ptr, const FontSet &fnt)
{
	fstb::unused (usr_ptr);

	assert (_loc_edit._slot_id >= 0);
	assert (_loc_edit._pi_type >= 0);
	assert (_loc_edit._param_index >= 0);

	_model_ptr = &model;
	_view_ptr  = &view;
	_page_ptr  = &page;
	_page_size = page_size;
	_fnt_ptr   = &fnt._m;

	const int      scr_w = _page_size [0];
	const int      x_mid =  scr_w >> 1;
	const int      h_m   = _fnt_ptr->get_char_h ();

	_add_sptr->set_font (*_fnt_ptr);
	_val_sptr->set_font (*_fnt_ptr);
	_rad_sptr->set_font (*_fnt_ptr);
	_delim_sptr->set_font (*_fnt_ptr);

	_add_sptr->set_coord (Vec2d (x_mid, 0 * h_m));
	_val_sptr->set_coord (Vec2d (x_mid, 1 * h_m));
	Tools::push_multiscale_param_ctrl (
		*_page_ptr, _step_sptr_arr.data (), _nbr_steps,
		Vec2d (0, 2 * h_m), scr_w, *_fnt_ptr
	);
	_rad_sptr->set_coord (Vec2d (x_mid, 3 * h_m));
	_delim_sptr->set_coord (Vec2d (x_mid, 4 * h_m));

	_add_sptr->set_frame (Vec2d (x_mid, 0), Vec2d ());
	_rad_sptr->set_frame (Vec2d (x_mid, 0), Vec2d ());

	_page_ptr->push_back (_add_sptr);
	_page_ptr->push_back (_val_sptr);
	_page_ptr->push_back (_rad_sptr);
	_page_ptr->push_back (_delim_sptr);

	const auto     win_pos = Vec2d (0, 5 * h_m);
	_win_sptr->set_size (_page_size - win_pos, Vec2d ());
	_win_sptr->set_coord (win_pos);
	_win_sptr->set_disp_pos (Vec2d ());

	_page_ptr->push_back (_win_sptr);

	const doc::Program & prog = _view_ptr->use_prog_cur ();
	const doc::Slot &    slot = prog._layer.use_slot (_loc_edit._slot_id);
	const doc::PluginSettings &   settings = slot.use_settings (_loc_edit._pi_type);
	auto           it_cls =
		settings._map_param_ctrl.find (_loc_edit._param_index);
	if (it_cls == settings._map_param_ctrl.end ())
	{
		assert (false);
	}
	else
	{
		_cls = it_cls->second;
	}

	if (_loc_edit._ctrl_index >= 0)
	{
		update_ctrl_link ();
	}

	update_display ();
	_page_ptr->jump_to (Entry_ADD);
}



void	NotchEdit::do_disconnect ()
{
	// Nothing
}



MsgHandlerInterface::EvtProp	NotchEdit::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	const int      node_id = evt.get_target ();

	if (evt.is_cursor ())
	{
		if (   evt.get_cursor () == NodeEvt::Curs_ENTER
		    && node_id >= Entry_STEP && node_id < Entry_STEP + _nbr_steps)
		{
			_step_index = node_id - Entry_STEP;
		}
	}

	else if (evt.is_button_ex ())
	{
		const Button   but = evt.get_button_ex ();
		switch (but)
		{
		case Button_S:
			if (    node_id == Entry_ADD
			    || (node_id >= Entry_STEP && node_id < Entry_STEP + _nbr_steps))
			{
				add_current_value_as_notch ();
				ret_val = EvtProp_CATCH;
			}
			else if (node_id >= Entry_LIST)
			{
				ret_val = delete_notch (node_id);
			}
			break;

		case Button_E:
			_page_switcher.return_page ();
			ret_val = EvtProp_CATCH;
			break;
		case Button_L:
		case Button_R:
			{
				const int      dir = (but == Button_L) ? -1 : +1;
				if (node_id == Entry_RADIUS)
				{
					ret_val = change_radius (dir);
				}
				else
				{
					ret_val = change_param (node_id, dir);
				}
			}
			break;
		default:
			// Nothing
			break;
		}
	}

	return ret_val;
}



void	NotchEdit::do_set_tempo (double bpm)
{
	fstb::unused (bpm);

	update_display ();
}



void	NotchEdit::do_activate_prog (int index)
{
	fstb::unused (index);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	NotchEdit::do_set_layer_edit (doc::LayerType layer_edit)
{
	fstb::unused (layer_edit);

	_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
}



void	NotchEdit::do_set_param (int slot_id, int index, float val, PiType type)
{
	fstb::unused (val);

	if (   slot_id == _loc_edit._slot_id
	    && type    == _loc_edit._pi_type
	    && index   == _loc_edit._param_index)
	{
		update_param_val ();
	}
}



void	NotchEdit::do_set_param_beats (int slot_id, int index, float beats)
{
	fstb::unused (beats);

	if (   slot_id     == _loc_edit._slot_id
	    && PiType_MAIN == _loc_edit._pi_type
	    && index       == _loc_edit._param_index)
	{
		update_display ();
	}
}



void	NotchEdit::do_remove_plugin (int slot_id)
{
	if (slot_id == _loc_edit._slot_id)
	{
		_page_switcher.switch_to (PageType_PROG_EDIT, nullptr);
	}
}



void	NotchEdit::do_set_param_ctrl (int slot_id, PiType type, int index, const doc::CtrlLinkSet &cls)
{
	if (   slot_id == _loc_edit._slot_id
	    && type    == _loc_edit._pi_type
	    && index   == _loc_edit._param_index)
	{
		_cls = cls;

		bool           stay_flag = true;
		if (! _loc_edit._ctrl_abs_flag)
		{
			const int      nbr_mod = int (cls._mod_arr.size ());
			if (_loc_edit._ctrl_index >= nbr_mod)
			{
				stay_flag = false;
			}
		}

		if (stay_flag)
		{
			update_display ();
		}
		else
		{
			_page_switcher.switch_to (PageType_PARAM_CONTROLLERS, nullptr);
		}
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NotchEdit::update_display ()
{
	update_param_val ();

	PageMgrInterface::NavLocList  nav_list;
	PageMgrInterface::add_nav (nav_list, Entry_ADD);
	for (int k = 0; k < _nbr_steps; ++k)
	{
		PageMgrInterface::add_nav (nav_list, Entry_STEP + k);
	}

	const auto     rad_percent = _ctrl_link._notch_radius * 100.f;
	char           txt_0 [127+1];
	fstb::snprintf4all (txt_0, sizeof (txt_0),
		"Radius: %5.1f %%", rad_percent
	);
	_rad_sptr->set_text (txt_0);
	PageMgrInterface::add_nav (nav_list, Entry_RADIUS);

	update_ctrl_link ();

	_win_sptr->clear_all_nodes ();
	_notch_arr.clear ();

	const int      scr_w = _page_size [0];
	const int      h_m   = _fnt_ptr->get_char_h ();

	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	const int      max_nbr_col = 2;
	const auto &   notch_list  = _ctrl_link._notch_list;
	const int      nbr_notches = int (notch_list.size ());

	const int      h_avail_pix = _page_size [1] - _win_sptr->get_coord () [1];
	const int      nbr_full_lines = h_avail_pix / h_m;
	const int      col_h       =
		std::max (fstb::div_ceil (nbr_notches, max_nbr_col), nbr_full_lines);
	const int      nbr_col     =
		std::max (fstb::div_ceil (nbr_notches, col_h), 1);
	const int      col_w_pix   = scr_w / nbr_col;
	int            notch_w     = col_w_pix / 2;
#if mfx_features_DISP == mfx_features_DISP_ST7920
	if (nbr_col > 1)
	{
		notch_w = col_w_pix;
	}
#endif

	int            notch_idx = 0;
	for (const auto &notch : notch_list)
	{
		const int      pos_x = notch_idx / col_h;
		const int      pos_y = notch_idx % col_h;

		auto           notch_sptr =
			std::make_shared <NText> (Entry_LIST + notch_idx);
		_notch_arr.push_back (notch_sptr);

		notch_sptr->set_font (*_fnt_ptr);
		notch_sptr->set_coord (Vec2d (pos_x * col_w_pix + notch_w, pos_y * h_m));
		notch_sptr->set_justification (1.f, 0, false);

		Tools::set_param_text (
			*_model_ptr, *_view_ptr, notch_w, index, notch, slot_id, type,
			nullptr, *notch_sptr, nullptr, nullptr, true
		);

		_win_sptr->push_back (notch_sptr);
		PageMgrInterface::add_nav (nav_list,Entry_LIST + notch_idx);

		++ notch_idx;
	}

	_page_ptr->set_nav_layout (nav_list);
	_win_sptr->invalidate_all ();
}



void	NotchEdit::update_param_val ()
{
	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	const int      width   = _val_sptr->get_coord () [0];
	Tools::set_param_text (
		*_model_ptr, *_view_ptr, width, index, -1, slot_id, type,
		nullptr, *_val_sptr, nullptr, nullptr, true
	);
}



void	NotchEdit::add_current_value_as_notch ()
{
	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;
	const auto &   prog    = _view_ptr->use_prog_cur ();
	const auto &   layer   = prog._layer;
	auto           val_nrm =
		_view_ptr->get_param_val (layer, slot_id, type, index);
	_ctrl_link._notch_list.insert (float (val_nrm));

	set_cached_control_link ();
}



void	NotchEdit::update_ctrl_link ()
{
	_ctrl_link = use_ctrl_link (_cls);
}



doc::CtrlLink &	NotchEdit::use_ctrl_link (doc::CtrlLinkSet &cls) const
{
	assert (_loc_edit._ctrl_index >= 0);

	if (_loc_edit._ctrl_abs_flag)
	{
		assert (cls._bind_sptr.get () != nullptr);
		return *cls._bind_sptr;
	}
	else
	{
		assert (cls._mod_arr [_loc_edit._ctrl_index].get () != nullptr);
		return *(cls._mod_arr [_loc_edit._ctrl_index]);
	}
}



MsgHandlerInterface::EvtProp	NotchEdit::change_param (int node_id, int dir)
{
	EvtProp        ret_val = EvtProp_PASS;

	if (node_id >= Entry_STEP && node_id < Entry_STEP + _nbr_steps)
	{
		const int      slot_id = _loc_edit._slot_id;
		const PiType   type    = _loc_edit._pi_type;
		const int      index   = _loc_edit._param_index;
		const int      step_scale =
			_page_ptr->get_shift (PageMgrInterface::Shift::R) ? 1 : 0;
		const int      step_index_loc = _step_index + step_scale;
		const float    step    =
			float (Cst::_step_param / pow (10, step_index_loc));

		ret_val = Tools::change_param (
			*_model_ptr, *_view_ptr, slot_id, type,
			index, step, dir, false, true
		);
	}

	return ret_val;
}



MsgHandlerInterface::EvtProp	NotchEdit::change_radius (int dir)
{
	assert (dir == -1 || dir == +1);

	constexpr int  l2_min  = -10;
	constexpr auto val_min = fstb::ipowpc <-l2_min> (0.5f);
	constexpr auto val_max = 1.f;

	auto           val = _ctrl_link._notch_radius;
	if (val >= val_min)
	{
		val *= (dir < 0) ? 0.5f : 2.f;
	}
	val = fstb::limit (val, val_min, val_max);

	_ctrl_link._notch_radius = val;
	set_cached_control_link ();

	return EvtProp_CATCH;
}



MsgHandlerInterface::EvtProp	NotchEdit::delete_notch (int node_id)
{
	assert (node_id >= Entry_LIST);

	const int      n_idx = node_id - Entry_LIST;
	assert (n_idx < int (_ctrl_link._notch_list.size ()));

	auto           it = _ctrl_link._notch_list.begin ();
	std::advance (it, n_idx);
	_ctrl_link._notch_list.erase (it);

	set_cached_control_link ();

	return EvtProp_CATCH;
}



void	NotchEdit::set_cached_control_link ()
{
	const int      slot_id = _loc_edit._slot_id;
	const PiType   type    = _loc_edit._pi_type;
	const int      index   = _loc_edit._param_index;

	use_ctrl_link (_cls) = _ctrl_link;
	_model_ptr->set_param_ctrl (slot_id, type, index, _cls);
}



}  // namespace pg
}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
