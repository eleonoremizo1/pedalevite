/*****************************************************************************

        NText.h
        Author: Laurent de Soras, 2016

Implements a single-line text widget as a cached monochrom bitmap, rendered
with a bitmap font.

Specifying the font is mandatory to be able to display anything.

Bitmap size is automatically ajusted by the text widget depending on the
settings.

Cursor is not handled by the bitmap default implementation. Instead, the
cached bitmap directly generates the video inversion. This class knows when
the cursor should be activated by spying cursor messages handled by the
parent bitmap class.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_NText_HEADER_INCLUDED)
#define mfx_uitk_NText_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/uitk/NBitmapM.h"

#include <array>
#include <optional>
#include <string>



namespace mfx
{

namespace ui
{
	class Font;
}

namespace uitk
{



class NText
:	public NBitmapM
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef NBitmapM Inherited;

	using Inherited::Inherited;

	NText &        operator = (const NText &other) = default;
	NText &        operator = (NText &&other)      = default;

	void           set_frame (Vec2d size_min, Vec2d margin);
	void           set_text_m (std::string multilabel);
	void           set_text (std::string txt);
	std::string    get_text () const;
	void           set_font (const ui::Font &fnt);
	void           set_mag (int x, int y);
	void           set_justification (float x, float y, bool baseline_flag);
	void           set_bold (bool bold_flag, bool space_flag);
	bool           is_bold () const noexcept;
	bool           has_space_in_bold () const noexcept;
	void           set_vid_inv (bool vid_inv_flag);
	void           set_underline (bool underline_flag);

	int            get_char_width (char32_t c) const noexcept;
	int            get_active_width () const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// mfx::uitk::NBitmapBase
	void           redraw_cursor (ui::DisplayInterface &disp, Rect bitmap_abs) override;

	// mfx::uitk::NodeInterface via mfx::uitk::NBitmapM
	Rect           do_get_bounding_box () const noexcept override;
	void           do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d node_coord) override;

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::NodeBase
	// Overrides mfx::uitk::NBitmapBase
	EvtProp        do_handle_evt (const NodeEvt &evt) override;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	void           update_text (const Rect &zone_old);
	void           update_full_text ();
	void           update_txt_ucs4 ();
	void           update_content ();
	int            compute_width_pix () const;

	// Standard and multilabel strings. If _txt_multi_opt exists,
	// it overrides _txt.
	std::optional <std::string>
	               _txt_multi_opt;
	std::string    _txt;

	// 0 = not set. Doesn't display anything if not set.
	const ui::Font *
	               _font_ptr = nullptr;

	// The minimum frame size (for video inverse). Includes the margin. >= 0
	Vec2d          _frame_size;

	// Distributed on all sides depending on the justification. >= 0
	Vec2d          _margin;

	// Magnification in 2 directions. > 0
	std::array <int, 2>
	               _mag_arr  = {{ 1, 1 }};

	// [0 ; 1]. 0 = left/top, 0.5 = center, 1.0 = right/bottom
	std::array <float, 2>
	               _justification  = {{ 0, 0 }};

	// Uses the font baseline as reference for the vertical position.
	// Overrides the vertical justification.
	bool           _baseline_flag  = false;

	// Uses a bold face by the text with another rendered 1 (or more) pixels
	// to the right.
	bool           _bold_flag      = false;

	// When bold, insert one column between all characters to compensate the
	// larger faces.
	bool           _space_flag     = false;

	// Default video inversion, in addition to the selection
	bool           _vid_inv_flag   = false;

	// Draws a single-pixel thick line at the bottom of the case.
	bool           _underline_flag = false;

	// Cached

	// Top-left of the textbox relative to the node coord.
	Vec2d          _origin;
	std::u32string _txt_ucs4;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const NText &other) const = delete;
	bool           operator != (const NText &other) const = delete;

}; // class NText



}  // namespace uitk
}  // namespace mfx



//#include "mfx/uitk/NText.hpp"



#endif   // mfx_uitk_NText_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
