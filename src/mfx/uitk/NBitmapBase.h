/*****************************************************************************

        NBitmapBase.h
        Author: Laurent de Soras, 2024

This is a base class for a bitmap node implementation.

do_redraw() must be implemented by the derived class. It can use redraw_base()
which is a helper providing all area calculations and clipping, as well as
cursor display.

The default implementation handles the cursor by video inversion of the
blitted result. It is possible to override this implementation and provide
a custom cursor.

Template parameters:

- PT: pixel type of the contained bitmap, either uint8_t or ui::PixArgb.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_NBitmapBase_HEADER_INCLUDED)
#define mfx_uitk_NBitmapBase_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"
#include "mfx/ui/PixArgb.h"
#include "mfx/uitk/NodeBase.h"

#include <vector>



namespace mfx
{
namespace uitk
{



template <typename PT>
class NBitmapBase
:	public NodeBase
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef NodeBase Inherited;
	typedef PT PixType;

	using NodeBase::NodeBase;

	void           set_size (Vec2d sz);
	Vec2d          get_size () const noexcept;
	void           show (bool flag);
	void           set_blend_mode (ui::DisplayInterface::BlendMode mode);
	ui::DisplayInterface::BlendMode
	               get_blend_mode () const noexcept;
	const PT *     use_buffer () const noexcept;
	PT *           use_buffer () noexcept;
	int            get_stride () const noexcept;
	bool           has_cursor () const noexcept;
	void           show_cursor (bool flag);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	bool           is_cursor_displayed () const noexcept;
	virtual void   redraw_cursor (ui::DisplayInterface &disp, Rect bitmap_abs);

	// mfx::uitk::NodeInterface via mfx::uitk::NodeBase
	void           do_notify_attachment (ParentInterface *cont_ptr) override;
	Rect           do_get_bounding_box () const noexcept override;

	// This function is called by do_redraw() in the derived class
	template <typename F>
	void           redraw_base (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord, F blit_fnc);

	// mfx::uitk::MsgHandlerInterface via mfx::uitk::NodeBase
	EvtProp        do_handle_evt (const NodeEvt &evt) override;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	Vec2d          _size;
	std::vector <PT>
	               _buffer;
	ui::DisplayInterface::BlendMode
	               _blend_mode  = ui::DisplayInterface::BlendMode_OPAQUE;
	bool           _cursor_flag = false;
	bool           _show_flag   = true;
	bool           _show_cursor_flag = true;

	static void    invert_zone (ui::DisplayInterface &disp, Rect zone);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const NBitmapBase &other) const = delete;
	bool           operator != (const NBitmapBase &other) const = delete;

}; // class NBitmapBase



}  // namespace uitk
}  // namespace mfx



#include "mfx/uitk/NBitmapBase.hpp"



#endif // mfx_uitk_NBitmapBase_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
