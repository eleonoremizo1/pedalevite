/*****************************************************************************

        Vec2d.hpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_uitk_Vec2d_CODEHEADER_INCLUDED)
#define mfx_uitk_Vec2d_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



constexpr Vec2d::Vec2d () noexcept
:	Inherited ({{ 0, 0 }})
{
	// Nothing
}



constexpr Vec2d::Vec2d (int x, int y) noexcept
:	Inherited ({{ x, y }})
{
	// Nothing
}



Vec2d &	Vec2d::operator += (const Vec2d &other) noexcept
{
	(*this) [0] += other [0];
	(*this) [1] += other [1];

	return *this;
}



Vec2d &	Vec2d::operator -= (const Vec2d &other) noexcept
{
	(*this) [0] -= other [0];
	(*this) [1] -= other [1];

	return *this;
}



Vec2d &	Vec2d::operator *= (int scale) noexcept
{
	(*this) [0] *= scale;
	(*this) [1] *= scale;

	return *this;
}



Vec2d &	Vec2d::operator /= (int scale) noexcept
{
	(*this) [0] /= scale;
	(*this) [1] /= scale;

	return *this;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace uitk
}  // namespace mfx



mfx::uitk::Vec2d  operator + (const mfx::uitk::Vec2d &lhs, const mfx::uitk::Vec2d &rhs) noexcept
{
	mfx::uitk::Vec2d  res (lhs);
	res += rhs;

	return res;
}



mfx::uitk::Vec2d  operator - (const mfx::uitk::Vec2d &lhs, const mfx::uitk::Vec2d &rhs) noexcept
{
	mfx::uitk::Vec2d  res (lhs);
	res -= rhs;

	return res;
}



mfx::uitk::Vec2d  operator * (const mfx::uitk::Vec2d &lhs, int rhs) noexcept
{
	mfx::uitk::Vec2d  res (lhs);
	res *= rhs;

	return res;
}



mfx::uitk::Vec2d  operator / (const mfx::uitk::Vec2d &lhs, int rhs) noexcept
{
	mfx::uitk::Vec2d  res (lhs);
	res /= rhs;

	return res;
}



#endif   // mfx_uitk_Vec2d_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
