/*****************************************************************************

        Vec2d.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_uitk_Vec2d_HEADER_INCLUDED)
#define mfx_uitk_Vec2d_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <array>



namespace mfx
{
namespace uitk
{



class Vec2d
:	public std::array <int, 2>
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef std::array <int, 2> Inherited;

	using          Inherited::Inherited;
	inline constexpr
	               Vec2d () noexcept;
	inline constexpr
	               Vec2d (int x, int y) noexcept;
	               Vec2d (const Vec2d &other)      = default;
	               ~Vec2d ()                       = default;
	Vec2d &        operator = (const Vec2d &other) = default;

	inline Vec2d & operator += (const Vec2d &other) noexcept;
	inline Vec2d & operator -= (const Vec2d &other) noexcept;
	inline Vec2d & operator *= (int scale) noexcept;
	inline Vec2d & operator /= (int scale) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



}; // class Vec2d



}  // namespace uitk
}  // namespace mfx



inline mfx::uitk::Vec2d  operator + (const mfx::uitk::Vec2d &lhs, const mfx::uitk::Vec2d &rhs) noexcept;
inline mfx::uitk::Vec2d  operator - (const mfx::uitk::Vec2d &lhs, const mfx::uitk::Vec2d &rhs) noexcept;
inline mfx::uitk::Vec2d  operator * (const mfx::uitk::Vec2d &lhs, int rhs) noexcept;
inline mfx::uitk::Vec2d  operator / (const mfx::uitk::Vec2d &lhs, int rhs) noexcept;



// Compatibility with structured binding.
// It seems to work without implementing the get() template functions.
// https://en.cppreference.com/w/cpp/language/structured_binding
// https://devblogs.microsoft.com/oldnewthing/20201015-00/?p=104369
namespace std
{

template <>
struct tuple_size <mfx::uitk::Vec2d>
{
	static constexpr size_t value =
		  sizeof (mfx::uitk::Vec2d)
		/ sizeof (typename mfx::uitk::Vec2d::value_type);
};

template <size_t IDX>
struct tuple_element <IDX, mfx::uitk::Vec2d>
{
	using type = typename mfx::uitk::Vec2d::value_type;
};

} // namespace std



#include "mfx/uitk/Vec2d.hpp"



#endif   // mfx_uitk_Vec2d_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
