/*****************************************************************************

        NWindow.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/uitk/NodeEvt.h"
#include "mfx/uitk/NWindow.h"

#include <cassert>



namespace mfx
{
namespace uitk
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
Input parameters:
	- node_id: a unique identifier in the tree, >= 0.
Throws: std::vector related exception
==============================================================================
*/

NWindow::NWindow (int node_id)
:	_node_id (node_id)
{
	assert (node_id >= 0);
}



/*
==============================================================================
Name: enable
Description:
	Enable or disable the window. When it is disabled, the windows is hidden
	(drawing commands ignored) and messages are not passed to its children,
	acting like it has been virtually removed.
Input parameters:
	- flag: true if the window is enabled
Throws: ?
==============================================================================
*/

void	NWindow::enable (bool flag)
{
	if (flag)
	{
		_en_flag = true;
		invalidate_all ();
	}
	else
	{
		const Rect     bbox (get_bounding_box ());
		const Rect     zone_old (bbox + _coord);
		_en_flag = false;
		if (_parent_ptr != nullptr)
		{
			_parent_ptr->invalidate (zone_old);
		}
	}
}



/*
==============================================================================
Name: set_size
Description:
	Sets the visible area of the window as well as its virtual size.
	The virtual size can be larger than the visible area.
	Default is 0 everywhere, so the call is mandatory to display anything.
Input parameters:
	- disp: visible area size in pixels, any component >= 0.
		If a component is 0, the window is hidden.
	- virt: virtual size of the contained area. It should be bigger than disp.
		If a component of virt is set to 0, it means that the size is undefined
		(infinite) in the associated dimension.
Throws: Nothing at the moment
==============================================================================
*/

void	NWindow::set_size (Vec2d disp, Vec2d virt)
{
	assert (disp [0] >= 0);
	assert (disp [1] >= 0);
	assert (virt [0] == 0 || virt [0] >= disp [0]);
	assert (virt [1] == 0 || virt [1] >= disp [1]);

	_size_disp = disp;
	_size_virt = virt;
}



/*
==============================================================================
Name: set_disp_pos
Description:
	Sets the position of the displayed area within the virtual canvas.
Input parameters:
	- pos: position of the top-left corner of the displayed area within the
		virtual canvas. The position should keep the displayed area in the
		canvas, unless its size is undefined in the given dimension.
Throws: ?
==============================================================================
*/

void	NWindow::set_disp_pos (Vec2d pos)
{
	assert (_size_virt [0] == 0 || pos [0] >= 0);
	assert (_size_virt [1] == 0 || pos [1] >= 0);
	assert (_size_virt [0] == 0 || pos [0] <= _size_virt [0] - _size_disp [0]);
	assert (_size_virt [1] == 0 || pos [1] <= _size_virt [1] - _size_disp [1]);

	_pos_virt = pos;
	invalidate_all ();
}



/*
==============================================================================
Name: set_autoscroll
Description:
	When the autoscroll is activated, the display area follows the cursor
	so the child object located under the cursor is displayed properly.
	Enabled by default.
Input parameters:
	- flag: true to enable the autoscroll.
==============================================================================
*/

void	NWindow::set_autoscroll (bool flag) noexcept
{
	_autoscroll_flag = flag;
}



/*
==============================================================================
Name: set_node_id
Description:
	Sets or changed the window unique identifier.
Input parameters:
	- node_id: a unique identifier in the tree, >= 0.
==============================================================================
*/

void	NWindow::set_node_id (int node_id) noexcept
{
	assert (node_id >= 0);

	_node_id = node_id;
}



/*
==============================================================================
Name: get_parent
Returns: A pointer on the attached parent or nullptr if the leaf is detached.
==============================================================================
*/

ParentInterface *	NWindow::get_parent () const noexcept
{
	return _parent_ptr;
}



/*
==============================================================================
Name: set_coord
Description:
	Sets the window reference coordinates, relative to its parent.
Input parameters:
	- pos: coordinates.
Throws: ?
==============================================================================
*/

void	NWindow::set_coord (Vec2d pos)
{
	const Rect     bbox (get_bounding_box ());
	const Rect     zone_old (bbox + _coord);
	_coord = pos;
	const Rect     zone_new (bbox + _coord);

	if (_parent_ptr != nullptr)
	{
		_parent_ptr->invalidate (zone_old);
		_parent_ptr->invalidate (zone_new);
	}
}



/*
==============================================================================
Name: invalidate_all
Description:
	Indicates to the parent that the area covered by the window should be
	redrawn.
Throws: ?
==============================================================================
*/

void	NWindow::invalidate_all ()
{
	ParentInterface * parent_ptr = get_parent ();
	if (parent_ptr != nullptr)
	{
		Rect           zone (do_get_bounding_box ());
		zone += get_coord ();
		parent_ptr->invalidate (zone);
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NWindow::do_push_back (NodeSPtr node_sptr)
{
	_node_list.push_back (node_sptr);
	node_sptr->notify_attachment (this);
}



void	NWindow::do_set_node (int pos, NodeSPtr node_sptr)
{
	_node_list [pos]->notify_attachment (nullptr);
	_node_list [pos] = node_sptr;
	node_sptr->notify_attachment (this);
}



void	NWindow::do_insert (int pos, NodeSPtr node_sptr)
{
	_node_list.insert (_node_list.begin () + pos, node_sptr);
	node_sptr->notify_attachment (this);
}



void	NWindow::do_erase (int pos)
{
	_node_list [pos]->notify_attachment (nullptr);
	_node_list.erase (_node_list.begin () + pos);
}



int	NWindow::do_get_nbr_nodes () const
{
	return int (_node_list.size ());
}



ContainerInterface::NodeSPtr	NWindow::do_use_node (int pos)
{
	return _node_list [pos];
}



Vec2d	NWindow::do_get_coord_abs () const
{
	Vec2d          coord;

	coord -= _pos_virt;

	if (_parent_ptr != nullptr)
	{
		coord += _parent_ptr->get_coord_abs ();
	}

	return coord;
}



void	NWindow::do_invalidate (const Rect &zone)
{
	if (_en_flag && _parent_ptr != nullptr)
	{
		Rect           zone_parent (zone + _coord - _pos_virt);
		_parent_ptr->invalidate (zone_parent);
	}
}



void	NWindow::do_notify_attachment (ParentInterface *cont_ptr)
{
	_parent_ptr = cont_ptr;
}



int	NWindow::do_get_id () const noexcept
{
	return _node_id;
}



Vec2d	NWindow::do_get_coord () const noexcept
{
	return _coord;
}



Rect	NWindow::do_get_bounding_box () const noexcept
{
	return Rect (Vec2d (), _size_disp);
}



MsgHandlerInterface::EvtProp	NWindow::do_handle_evt (const NodeEvt &evt)
{
	EvtProp        ret_val = EvtProp_PASS;

	if (_en_flag)
	{
		const NodeEvt::Type  type   = evt.get_type ();
		const int            target = evt.get_target ();

		for (auto &node_sptr : _node_list)
		{
			// Ensure that the selected node remains within the visible area
			if (   _autoscroll_flag
			    && type   == NodeEvt::Type_CURSOR
			    && target == node_sptr->get_id ())
			{
				const NodeEvt::Curs  curs = evt.get_cursor ();
				if (curs == NodeEvt::Curs_ENTER)
				{
					keep_node_visible (*node_sptr);
				}
			}

			ret_val = node_sptr->handle_evt (evt);
			if (ret_val == EvtProp_CATCH)
			{
				break;
			}
		}
	}

	return ret_val;
}



void	NWindow::do_redraw (ui::DisplayInterface &disp, Rect clipbox, Vec2d parent_coord)
{
	if (_en_flag)
	{
		Vec2d          this_coord (parent_coord + _coord);
		clipbox.intersect (get_bounding_box () + this_coord);

		for (auto &node_sptr : _node_list)
		{
			node_sptr->redraw (disp, clipbox, this_coord - _pos_virt);
		}
	}
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	NWindow::keep_node_visible (const NodeInterface &node)
{
	const Rect     node_box_orig (node.get_bounding_box ());

	// Relative to the virtual window top-left corner
	Rect           node_box (node_box_orig + node.get_coord ());

	// Clips the object's box to the largest visible area,
	// keeping at least the top-left part
	Vec2d          box_size (node_box.get_size ());
	for (size_t c = 0; c < box_size.size (); ++c)
	{
		box_size [c] = std::min (box_size [c], _size_disp [c]);
	}
	node_box = Rect (node_box [0], node_box [0] + box_size);

	// Distance of the box corners from the corners of the visible area
	const Vec2d    dist_tl (node_box [0] -  _pos_virt              );
	const Vec2d    dist_br (node_box [1] - (_pos_virt + _size_disp));

	// Finds the required displacement
	Vec2d          shift;
	for (size_t c = 0; c < box_size.size (); ++c)
	{
		if (dist_tl [c] < 0)
		{
			shift [c] = dist_tl [c];
		}
		else if (dist_br [c] > 0)
		{
			shift [c] = dist_br [c];
		}
	}

	const Vec2d    pos_virt_old (_pos_virt);
	_pos_virt += shift;
	const Vec2d    pos_virt_max (_size_virt - _size_disp);
	for (size_t c = 0; c < box_size.size (); ++c)
	{
		if (_size_virt [0] != 0)
		{
			_pos_virt [c] = fstb::limit (_pos_virt [c], 0, pos_virt_max [c]);
		}
	}

	if (_pos_virt != pos_virt_old)
	{
		invalidate_all ();
	}
}



}  // namespace uitk
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
