/*****************************************************************************

        DPvabI2sRp1.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/adrv/DPvabI2sRp1.h"

#include <fcntl.h>

#include <thread>

#include <cassert>



namespace mfx
{
namespace adrv
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DPvabI2sRp1::DPvabI2sRp1 (hw::Higepio &io)
:	_mgr (*this, io, _prefill)
,	_io (io)
,	_i2s_mptr (
		// We use I2S1 here (same physical pins as I2S0 but clock slave)
		hw::rp1i2s::_i2s1_ofs,
		hw::rp1i2s::_i2s_blk_len,
		_rp1_file_0, _mem_flags
	)
{
	// Nothing
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	DPvabI2sRp1::do_init (double &sample_freq, int &max_block_size, CbInterface &callback, const char *driver_0, int chn_idx_in, int chn_idx_out) noexcept
{
	fstb::unused (driver_0, chn_idx_in, chn_idx_out);
	assert (chn_idx_in == 0);
	assert (chn_idx_out == 0);

	// Custom pins
	_io.set_pin_dir (_pin_rst,  true);
	_io.set_pin_dir (_pin_freq, true);

	// I2S pins. Requires tweaking the GPIO registers
	try
	{
		hw::MmapPtr    gpio_mptr (
			0xD0000, 0x30000, // Starts at io_bank0 and includes pads_bank0
			_rp1_file_0, _mem_flags
		);

		auto           enable_i2s = [&gpio_mptr] (int gpio)
		{
			// Pad: +IE -OD
			constexpr uint32_t pad_ofs = 0xF0000 - 0xD0000;
			const auto     padr = 0x004 + (gpio << 2) + pad_ofs;
			gpio_mptr.set_field (
				padr,
				(1 << 6) | (1 << 7), // Masks
				(1 << 6) | (0 << 7)  // Values
			);

			// CTRL: FUNCSEL, OUTOVER, OEOVER
			const auto     cadr = 0x004 + (gpio << 3);
			gpio_mptr.set_field (
				cadr,
				0x1F | (3 << 12) | (3 << 14), // Masks
				0x04 | (0 << 12) | (0 << 14)  // Values
			);
		};

		enable_i2s (_pin_bclk);
		enable_i2s (_pin_lrck);
		enable_i2s (_pin_din);
		enable_i2s (_pin_dout);
	}
	catch (...)
	{
		return -1;
	}

	const int      ret_val = _mgr.init (sample_freq, max_block_size, callback);

	return ret_val;
}



int	DPvabI2sRp1::do_start () noexcept
{
	return _mgr.start ();
}



int	DPvabI2sRp1::do_stop () noexcept
{
	return _mgr.stop ();
}



void	DPvabI2sRp1::do_restart () noexcept
{
	do_stop ();
	do_start ();
}



std::string	DPvabI2sRp1::do_get_last_error () const
{
	return "";
}



void	DPvabI2sRp1::do_pcm_prepare ()
{
	using namespace hw::rp1i2s;

	// First, disables transfers and channels
	_i2s_mptr.at (_cer) = 0;
	_i2s_mptr.at (_rer) = 0;
	_i2s_mptr.at (_ter) = 0;

	// Sample resolution: 24 bits
	_i2s_mptr.at (_rcr) = _xcr_ws_24;
	_i2s_mptr.at (_tcr) = _xcr_ws_24;

	// FIFO capacity thresholds
	_i2s_mptr.at (_rfcr) = 0;
	_i2s_mptr.at (_tfcr) = _prefill - 1;

	// Enables channels
	_i2s_mptr.at (_rer) = _xer_en;
	_i2s_mptr.at (_ter) = _xer_en;

	// Disables DMA for the first pair of channels
	_i2s_mptr.clr (_dmacr, _dmacr_txch0 | _dmacr_rxch0);

	// Sample resolution: 24 bits
	_i2s_mptr.at (_ccr) = _ccr_res_24;

	// Disables DMA globally
	_i2s_mptr.clr (_dmacr, _dmacr_tx | _dmacr_rx);

	// Enables playback and capture
	_i2s_mptr.at (_rxffr) = _rxffr_en;
	_i2s_mptr.at (_txffr) = _txffr_en;

	// Enables the device
	_i2s_mptr.at (_ier) = _ier_dev_en;

	// Interrupts
	_i2s_mptr.set_field (
		_imr, _imr_tx_mask | _imr_rx_mask, _imr_tx_ei | _imr_rx_ei
	);

	// Enables interrupts for the channel pair
	_i2s_mptr.at (_irer) = _irer_int_en;
	_i2s_mptr.at (_iter) = _iter_int_en;

	// Clears the capture FIFOs
	for (int k = 0; k < _fifo_depth; ++k)
	{
		[[maybe_unused]] volatile uint32_t dummy;
		dummy = _i2s_mptr.at (_lrbr_lthr);
		dummy = _i2s_mptr.at (_rrbr_rthr);
	}
}



void	DPvabI2sRp1::do_pcm_start () noexcept
{
	using namespace hw::rp1i2s;

	// Aknowledge any existing interrupt
	ack_interrupts ();

	// Starts transfers
	_i2s_mptr.at (_cer) = _cer_trans;
}



void	DPvabI2sRp1::do_pcm_stop () noexcept
{
	_i2s_mptr.at (hw::rp1i2s::_cer) = 0;
}



int32_t	DPvabI2sRp1::do_pcm_read (int chn) noexcept
{
	const auto     adr = get_fifo_adr_for_chn (chn);
	return _i2s_mptr.at (adr);
}



void	DPvabI2sRp1::do_pcm_write (int chn, int32_t x) noexcept
{
	const auto     adr = get_fifo_adr_for_chn (chn);
	_i2s_mptr.at (adr) = x;
}



DPvabI2sPcmInterface::CheckStatusResult	DPvabI2sRp1::do_pcm_check_status (int chn) noexcept
{
	using namespace hw::rp1i2s;

	CheckStatusResult res;

	// Status bits are for a full frame (L + R), so we really check things only
	// for the first channel. For the other one, we assume nothing serious has
	// changed so we can continue the transfer immediately.
	if (chn > 0)
	{
		res._ready_flag = true;
	}

	else
	{
		// Reads the interrupt status register
		const auto     isr = read_isr_ack ();

		// Checks if there is any error
		if ((isr & (_isr_rxfo | _isr_txfo)) != 0)
		{
			res._sync_err_flag = true;
		}

		// Checks if we can both send and retrieve data
		constexpr auto ready_mask = _isr_rxda | _isr_txfe;
		if ((isr & ready_mask) == ready_mask)
		{
			res._ready_flag = true;
		}
	}

	return res;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



uint32_t	DPvabI2sRp1::read_isr_ack () noexcept
{
	const auto     isr = _i2s_mptr.at (hw::rp1i2s::_isr);
	ack_interrupts ();

	return isr;
}



void	DPvabI2sRp1::ack_interrupts () noexcept
{
	[[maybe_unused]] volatile uint32_t dummy =
		  _i2s_mptr.at (hw::rp1i2s::_ror)
		+ _i2s_mptr.at (hw::rp1i2s::_tor);
}



uint32_t	DPvabI2sRp1::get_fifo_adr_for_chn (int chn) noexcept
{
	assert (chn >= 0);
	assert (chn < _nbr_chn);

	using namespace hw::rp1i2s;

	return _lrbr_lthr + chn * (_rrbr_rthr - _lrbr_lthr);
}



const char *	DPvabI2sRp1::_rp1_file_0 =
	"/sys/bus/pci/devices/0000:01:00.0/resource1";
const int	DPvabI2sRp1::_mem_flags = O_RDWR | O_SYNC;



}  // namespace adrv
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
