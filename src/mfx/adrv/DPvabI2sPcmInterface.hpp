/*****************************************************************************

        DPvabI2sPcmInterface.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_adrv_DPvabI2sPcmInterface_CODEHEADER_INCLUDED)
#define mfx_adrv_DPvabI2sPcmInterface_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace adrv
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DPvabI2sPcmInterface::pcm_prepare ()
{
	do_pcm_prepare ();
}



void	DPvabI2sPcmInterface::pcm_start () noexcept
{
	do_pcm_start ();
}



void	DPvabI2sPcmInterface::pcm_stop () noexcept
{
	do_pcm_stop ();
}



int32_t	DPvabI2sPcmInterface::pcm_read (int chn) noexcept
{
	assert (chn >= 0);

	return do_pcm_read (chn);
}



void	DPvabI2sPcmInterface::pcm_write (int chn, int32_t x) noexcept
{
	assert (chn >= 0);

	do_pcm_write (chn, x);
}



DPvabI2sPcmInterface::CheckStatusResult	DPvabI2sPcmInterface::pcm_check_status (int chn) noexcept
{
	assert (chn >= 0);

	return do_pcm_check_status (chn);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace adrv
}  // namespace mfx



#endif   // mfx_adrv_DPvabI2sPcmInterface_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
