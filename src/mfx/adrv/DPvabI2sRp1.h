/*****************************************************************************

        DPvabI2sRp1.h
        Author: Laurent de Soras, 2024

PCM/I2S interface in polled mode

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_adrv_DPvabI2sRp1_HEADER_INCLUDED)
#define mfx_adrv_DPvabI2sRp1_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/adrv/DPvabI2sCommon.h"
#include "mfx/adrv/DPvabI2sPcmInterface.h"
#include "mfx/adrv/DriverInterface.h"
#include "mfx/hw/Higepio.h"
#include "mfx/hw/MmapPtr.h"
#include "mfx/hw/rp1i2s.h"

#include <cstdint>



namespace mfx
{
namespace adrv
{



class DPvabI2sRp1 final
:	public DriverInterface
,	public DPvabI2sPcmInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       DPvabI2sRp1 (hw::Higepio &io);
	               ~DPvabI2sRp1 () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// DriverInterface
	int            do_init (double &sample_freq, int &max_block_size, CbInterface &callback, const char *driver_0, int chn_idx_in, int chn_idx_out) noexcept final;
	int            do_start () noexcept final;
	int            do_stop () noexcept final;
	void           do_restart () noexcept final;
	std::string    do_get_last_error () const final;

	// DPvabI2sPcmInterface
	void           do_pcm_prepare () final;
	void           do_pcm_start () noexcept final;
	void           do_pcm_stop () noexcept final;
	CheckStatusResult
	               do_pcm_check_status (int chn) noexcept final;
	int32_t        do_pcm_read (int chn) noexcept final;
	void           do_pcm_write (int chn, int32_t x) noexcept final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _nbr_chn      = DPvabI2sCommon::_nbr_chn;
	static constexpr int _bits_per_chn = DPvabI2sCommon::_bits_per_chn;
	static constexpr int _resol        = DPvabI2sCommon::_resol;

	static constexpr int _pin_rst      = DPvabI2sCommon::_pin_rst;
	static constexpr int _pin_freq     = DPvabI2sCommon::_pin_freq;
	static constexpr int _pin_bclk     = DPvabI2sCommon::_pin_bclk;
	static constexpr int _pin_lrck     = DPvabI2sCommon::_pin_lrck;
	static constexpr int _pin_din      = DPvabI2sCommon::_pin_din;
	static constexpr int _pin_dout     = DPvabI2sCommon::_pin_dout;

	// Number of sample frames written in advance in the I2S TX queue. >= 1
	// After initial filling, we sync on the RX queue (we try to keep it always
	// empty) and write the same number of read samples on the TX queue.
	static const int  _prefill = 4;
	static_assert (
		_prefill <= hw::rp1i2s::_fifo_depth_tx,
		"Prefill should fit in the TX FIFO."
	);

	inline uint32_t
	               read_isr_ack () noexcept;
	inline void    ack_interrupts () noexcept;

	static inline uint32_t
	               get_fifo_adr_for_chn (int chn) noexcept;

	DPvabI2sCommon _mgr;
	hw::Higepio &  _io;
	hw::MmapPtr    _i2s_mptr;  // Virtual base address for the I2S registers

	static const char *
	               _rp1_file_0;
	static const int
	               _mem_flags;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DPvabI2sRp1 ()                               = delete;
	               DPvabI2sRp1 (const DPvabI2sRp1 &other)       = delete;
	               DPvabI2sRp1 (DPvabI2sRp1 &&other)            = delete;
	DPvabI2sRp1 &  operator = (const DPvabI2sRp1 &other)        = delete;
	DPvabI2sRp1 &  operator = (DPvabI2sRp1 &&other)             = delete;
	bool           operator == (const DPvabI2sRp1 &other) const = delete;
	bool           operator != (const DPvabI2sRp1 &other) const = delete;

}; // class DPvabI2sRp1



}  // namespace adrv
}  // namespace mfx



//#include "mfx/adrv/DPvabI2sRp1.hpp"



#endif   // mfx_adrv_DPvabI2sRp1_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
