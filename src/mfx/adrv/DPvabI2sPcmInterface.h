/*****************************************************************************

        DPvabI2sPcmInterface.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_adrv_DPvabI2sPcmInterface_HEADER_INCLUDED)
#define mfx_adrv_DPvabI2sPcmInterface_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace adrv
{



class DPvabI2sPcmInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	class CheckStatusResult
	{
	public:
		bool           _sync_err_flag = false;
		bool           _ready_flag    = false;
	};

	               DPvabI2sPcmInterface ()                        = default;
	               DPvabI2sPcmInterface (const DPvabI2sPcmInterface &other) = default;
	               DPvabI2sPcmInterface (DPvabI2sPcmInterface &&other)      = default;
	virtual        ~DPvabI2sPcmInterface ()                       = default;

	DPvabI2sPcmInterface &
	               operator = (const DPvabI2sPcmInterface &other) = default;
	DPvabI2sPcmInterface &
	               operator = (DPvabI2sPcmInterface &&other)      = default;

	inline void    pcm_prepare ();
	inline void    pcm_start () noexcept;
	inline void    pcm_stop () noexcept;
	inline int32_t pcm_read (int chn) noexcept;
	inline void    pcm_write (int chn, int32_t x) noexcept;
	inline CheckStatusResult
	               pcm_check_status (int chn) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual void   do_pcm_prepare () = 0;
	virtual void   do_pcm_start () noexcept = 0;
	virtual void   do_pcm_stop () noexcept = 0;
	virtual CheckStatusResult
	               do_pcm_check_status (int chn) noexcept = 0;
	virtual int32_t
	               do_pcm_read (int chn) noexcept = 0;
	virtual void   do_pcm_write (int chn, int32_t x) noexcept = 0;

}; // class DPvabI2sPcmInterface



}  // namespace adrv
}  // namespace mfx



#include "mfx/adrv/DPvabI2sPcmInterface.hpp"



#endif   // mfx_adrv_DPvabI2sPcmInterface_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
