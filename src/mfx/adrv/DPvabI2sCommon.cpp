/*****************************************************************************

        DPvabI2sCommon.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



// Enables use of SIMD instructions in the buffer conversion and copy
#define mfx_adrv_DPvabI2sCommon_USE_SIMD

// RP1 requires data sign extension
#define mfx_adrv_DPvabI2sCommon_REQ_SIGN_EXT (PV_RPI_VER_MAJOR == 5)
#if PV_RPI_VER_MAJOR < 3 || PV_RPI_VER_MAJOR > 5
	#error Unsupported Raspberry Pi version
#endif



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#if defined (mfx_adrv_DPvabI2sCommon_USE_SIMD)
	#include "fstb/Vx32_conv.hpp"
	#include "fstb/Vf32.h"
	#include "fstb/Vs32.h"
#endif
#include "mfx/adrv/CbInterface.h"
#include "mfx/adrv/DPvabI2sCommon.h"
#include "mfx/adrv/DPvabI2sPcmInterface.h"
#include "mfx/hw/cs4272.h"
#include "mfx/hw/ThreadLinux.h"

#include <bcm_host.h>

#include <fcntl.h>

#include <chrono>
#include <stdexcept>
#include <thread>

#include <cassert>



namespace mfx
{
namespace adrv
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DPvabI2sCommon::DPvabI2sCommon (DPvabI2sPcmInterface &pcm, hw::Higepio &io, int prefill)
:	_pcm (pcm)
,	_io (io)
,	_i2c (_i2c_addr, "DPvabI2sCommon: cannot open I2C port")
,	_cb_ptr (nullptr)
,	_state (State_STOP)
,	_exit_flag (false)
,	_proc_ex_flag (false)
,	_cur_buf (0)
,	_buf_int_i (_block_size_a * _nbr_chn * 2)
,	_buf_int_o (_block_size_a * _nbr_chn * 2)
,	_buf_flt_i (_block_size_a * _nbr_chn)
,	_buf_flt_o (_block_size_a * _nbr_chn)
,	_thread_main ()
,	_blk_proc_mtx ()
,	_blk_proc_cv ()
,	_proc_now_flag (false)
,	_prefill (prefill)
{
	if (   ! _exit_flag.is_lock_free ()
	    || ! _proc_ex_flag.is_lock_free ()
	    || ! _cur_buf.is_lock_free ()
	    || ! _proc_now_flag.is_lock_free ())
	{
		throw std::runtime_error (
			"std::atomic is not lock-free on this system."
		);
	}
}



DPvabI2sCommon::State	DPvabI2sCommon::get_state () const noexcept
{
	return _state;
}



bool	DPvabI2sCommon::is_exit_needed () const noexcept
{
	return _exit_flag;
}



// GPIO pins should be configured before calling this function.
int	DPvabI2sCommon::init (double &sample_freq, int &max_block_size, CbInterface &callback) noexcept
{
	sample_freq    = (_fs_code != 0) ? 44100 : 48000;
	max_block_size = _block_size;
	_cb_ptr        = &callback;

	_io.write_pin (_pin_freq, _fs_code);

	// Puts the chip in reset state
	_io.write_pin (_pin_rst, 0);

	return 0;
}



int	DPvabI2sCommon::start () noexcept
{
	if (_state != State_STOP)
	{
		stop ();
	}

	int            ret_val = 0;

	// Puts the chip in reset state
	_io.write_pin (_pin_rst, 0);

	// Waits a few ms for all clocks to be stable
	std::this_thread::sleep_for (std::chrono::milliseconds (5));

	_io.write_pin (_pin_rst, 1);

	// If MCLK is internally generated, waits for it
	std::this_thread::sleep_for (std::chrono::milliseconds (1));

	using namespace hw::cs4272;

	// Mode control 2: sets CPEN and PDN
	write_reg (0x07, _mc2_ctrl_port | _mc2_power_down);

	// Setup -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Master mode and I2S for DAC
	// Table 9, p. 29:  MCLK=Input, Master Mode, Single Speed,
	// MCLK/LRCK = 256, SCLK/LRCK = 64: ratio1 = 0, ratio0 = 0
	write_reg (0x01, _mc1_single | _mc1_master | _mc1_fmt_i2s);

	write_reg (0x02, _dacc_deemph_none);

	write_reg (0x03, _mix_soft_r | _mix_atapi_l_to_l | _mix_atapi_r_to_r);

	write_reg (0x04, 0);
	write_reg (0x05, 0);

	// I2S for ADC
	write_reg (0x06, _adcc_fmt_i2s);
	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

	// Clears the PDN bit for startup
	write_reg (0x07, _mc2_ctrl_port);

	// Actually only 85 us are required
	std::this_thread::sleep_for (std::chrono::milliseconds (1));

	// Initializes threads and stuff
	_exit_flag   = false;
	try
	{
		_thread_main = std::thread (&DPvabI2sCommon::main_loop, this);
		hw::ThreadLinux::set_priority (_thread_main, 0, nullptr);
	}
	catch (...)
	{
		ret_val = -1;
	}

	if (ret_val == 0)
	{
		_state = State_RUN;
	}

	return ret_val;
}



int	DPvabI2sCommon::stop () noexcept
{
	int            ret_val = 0;

	if (_state == State_RUN)
	{
		_exit_flag = true;
		try
		{
			_thread_main.join ();
		}
		catch (...)
		{
			ret_val = -1;
		}
	}

	_state = State_STOP;

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DPvabI2sCommon::main_loop () noexcept
{
	_proc_ex_flag  = false;
	_proc_now_flag = false;
	_cur_buf       = 0;

	try
	{
	std::thread    thread_proc (&DPvabI2sCommon::proc_loop, this);
	hw::ThreadLinux::set_priority (thread_proc, -1, nullptr);

	// Clears integer buffers
	memset (&_buf_int_i [0], 0, sizeof (_buf_int_i [0]) * _buf_int_i.size ());
	memset (&_buf_int_o [0], 0, sizeof (_buf_int_o [0]) * _buf_int_o.size ());

	// Prepares the I2S interface
	_pcm.pcm_prepare ();

	// Writes a few samples in advance
	// Less samples = shorter latency
	// More samples = better protection against thread interruptions
	for (int k = 0; k < _prefill; ++k)
	{
		for (int chn = 0; chn < _nbr_chn; ++chn)
		{
			_pcm.pcm_write (chn, 0);
		}
	}

	// Start
	_pcm.pcm_start ();

	int            buf_idx = _cur_buf;
	int            buf_pos = 0;
	int            chn_pos = 0;
	while (! _exit_flag)
	{
		const auto     cs_res = _pcm.pcm_check_status (chn_pos);

		// Can we read/write something ?
		if (cs_res._ready_flag)
		{
			const int      pos =
				(buf_idx * _block_size_a + buf_pos) * _nbr_chn + chn_pos;

			// Read to the input buffer
			_buf_int_i [pos] = _pcm.pcm_read (chn_pos);

			// Write from the output buffer
			_pcm.pcm_write (chn_pos, _buf_int_o [pos]);

			// Next sample
			++ chn_pos;
			if (chn_pos >= _nbr_chn)
			{
				chn_pos = 0;
				++ buf_pos;

				if (buf_pos >= _block_size)
				{
					buf_pos  = 0;
					buf_idx  = 1 - buf_idx;
					_cur_buf = buf_idx;

					// Buffer done, signals the processing thread
					_proc_now_flag = true;
					_blk_proc_cv.notify_one ();
				}
			}
		}

		else if (cs_res._sync_err_flag)
		{
			_cb_ptr->notify_dropout ();
		}
	}

	_proc_ex_flag = true;
	_blk_proc_cv.notify_one ();

	// Disables the PCM interface
	_pcm.pcm_stop ();

	// Waits for the processing thread to terminate
	thread_proc.join ();
	}
	catch (...)
	{
		// Nothing
		assert (false);
	}
}



void	DPvabI2sCommon::proc_loop ()
{
	std::unique_lock <std::mutex> lock (_blk_proc_mtx);

	float *        dst_arr [2] =
	{
		&_buf_flt_o [0],
		&_buf_flt_o [_block_size_a]
	};
	const float *  src_arr [2] =
	{
		&_buf_flt_i [0],
		&_buf_flt_i [_block_size_a]
	};

	const float    scale_o = float ((1U << (_resol - 1)) - 1);
#if mfx_adrv_DPvabI2sCommon_REQ_SIGN_EXT
	// We convert the sample to signed 32 bits first, and scale it from there.
	const auto     scale_i = 1.0f / (scale_o * float (1U << (32 - _resol)));
#else
	const float    scale_i = 1.0f / scale_o;
#endif
	const float    min_flt = -1.0f;
	const float    max_flt = +1.0f;

#if defined (mfx_adrv_DPvabI2sCommon_USE_SIMD)
	const auto     sc_o_v = fstb::Vf32 (scale_o);
	const auto     sc_i_v = fstb::Vf32 (scale_i);
	const auto     maxf_v = fstb::Vf32 (max_flt);
	const auto     minf_v = fstb::Vf32 (min_flt);
#endif // mfx_adrv_DPvabI2sCommon_USE_SIMD

	float *        buf_flt_i_ptr = &_buf_flt_i [0];
	float *        buf_flt_o_ptr = &_buf_flt_o [0];

	while (! _proc_ex_flag)
	{
		while (! _proc_now_flag && ! _proc_ex_flag)
		{
			_blk_proc_cv.wait (lock);
		}
		_proc_now_flag = false;

		if (_proc_ex_flag)
		{
			break;
		}

		// _cur_buf is sampled now and its value will be blocked for the whole
		// processing duration.
		const int      buf_idx = 1 - _cur_buf;

		const int      ofs     = buf_idx * _block_size_a * _nbr_chn;
		const int32_t* buf_int_i_ptr = &_buf_int_i [ofs];
		int32_t *      buf_int_o_ptr = &_buf_int_o [ofs];

		// Copies acquired data to input buffer
#if defined (mfx_adrv_DPvabI2sCommon_USE_SIMD)
		for (int pos = 0; pos < _block_size; pos += 4)
		{
			auto           x0_int = fstb::Vs32::load (buf_int_i_ptr + pos * 2    );
			auto           x1_int = fstb::Vs32::load (buf_int_i_ptr + pos * 2 + 4);
	#if mfx_adrv_DPvabI2sCommon_REQ_SIGN_EXT
			if constexpr (_resol < 32)
			{
				x0_int <<= 32 - _resol;
				x1_int <<= 32 - _resol;
			}
	#endif
			auto           x0_flt = fstb::Vf32 (x0_int);
			auto           x1_flt = fstb::Vf32 (x1_int);
			x0_flt *= sc_i_v;
			x1_flt *= sc_i_v;
			fstb::Vf32     xl_flt;
			fstb::Vf32     xr_flt;
			std::tie (xl_flt, xr_flt) = fstb::Vf32::deinterleave (x0_flt, x1_flt);
			xl_flt.store (buf_flt_i_ptr +                 pos);
			xr_flt.store (buf_flt_i_ptr + _block_size_a + pos);
		}
#else // mfx_adrv_DPvabI2sCommon_USE_SIMD
		for (int pos = 0; pos < _block_size; ++pos)
		{
			int32_t        xl_int = buf_int_i_ptr [pos * 2    ];
			int32_t        xr_int = buf_int_i_ptr [pos * 2 + 1];
	#if mfx_adrv_DPvabI2sCommon_REQ_SIGN_EXT
			if constexpr (_resol < 32)
			{
				xl_int <<= 32 - _resol;
				xr_int <<= 32 - _resol;
			}
	#endif
			const auto     xl_flt = float (xl_int) * scale_i;
			const auto     xr_flt = float (xr_int) * scale_i;
			buf_flt_i_ptr [                pos] = xl_flt;
			buf_flt_i_ptr [_block_size_a + pos] = xr_flt;
		}
#endif // mfx_adrv_DPvabI2sCommon_USE_SIMD

		// Processing
		_cb_ptr->process_block (dst_arr, src_arr, _block_size);

		// Copies produced data to the output buffer
#if defined (mfx_adrv_DPvabI2sCommon_USE_SIMD)
		for (int pos = 0; pos < _block_size; pos += 4)
		{
			auto           xl_flt =
				fstb::Vf32::load (buf_flt_o_ptr +                 pos);
			auto           xr_flt =
				fstb::Vf32::load (buf_flt_o_ptr + _block_size_a + pos);
			xl_flt  = fstb::limit (xl_flt, minf_v, maxf_v);
			xr_flt  = fstb::limit (xr_flt, minf_v, maxf_v);
			xl_flt *= sc_o_v;
			xr_flt *= sc_o_v;
			fstb::Vf32     x0_flt;
			fstb::Vf32     x1_flt;
			std::tie (x0_flt, x1_flt) = fstb::Vf32::interleave (xl_flt, xr_flt);
			const auto  x0_int = fstb::Vs32 (x0_flt);
			const auto  x1_int = fstb::Vs32 (x1_flt);
			x0_int.store (buf_int_o_ptr + pos * 2    );
			x1_int.store (buf_int_o_ptr + pos * 2 + 4);
		}
#else // mfx_adrv_DPvabI2sCommon_USE_SIMD
		for (int pos = 0; pos < _block_size; ++pos)
		{
			float          xl_flt = buf_flt_o_ptr [                pos];
			float          xr_flt = buf_flt_o_ptr [_block_size_a + pos];
			xl_flt  = fstb::limit (xl_flt, min_flt, max_flt);
			xr_flt  = fstb::limit (xr_flt, min_flt, max_flt);
			xl_flt *= scale_o;
			xr_flt *= scale_o;
			const int32_t  xl_int = fstb::conv_int_fast (xl_flt);
			const int32_t  xr_int = fstb::conv_int_fast (xr_flt);
			buf_int_o_ptr [pos * 2    ] = xl_int;
			buf_int_o_ptr [pos * 2 + 1] = xr_int;
		}
#endif // mfx_adrv_DPvabI2sCommon_USE_SIMD

		if (_proc_now_flag)
		{
			// We must be late
			_cb_ptr->notify_dropout ();
		}
	}
}



void	DPvabI2sCommon::write_reg (uint8_t reg, uint8_t val) noexcept
{
	_i2c.write_reg_8 (reg, val);
}



}  // namespace adrv
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
