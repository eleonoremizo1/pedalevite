/*****************************************************************************

        DPvabI2sCommon.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_adrv_DPvabI2sCommon_HEADER_INCLUDED)
#define mfx_adrv_DPvabI2sCommon_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/AllocAlign.h"
#include "mfx/hw/Higepio.h"
#include "mfx/hw/GpioPin.h"
#include "mfx/hw/I2cLinux.h"

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

#include <cstdint>



namespace mfx
{
namespace adrv
{



class CbInterface;
class DPvabI2sPcmInterface;

class DPvabI2sCommon
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_chn      =  2;
	static constexpr int _bits_per_chn = 32; // Transmitted bits (meaningful + padding)
	static constexpr int _resol        = 24; // Number of meaningful bits.
	static constexpr int _transfer_lag =  1; // I2S data is one clock cycle after the LRCLK edge. >= 0
	static constexpr int _block_size   = 64; // Buffer size in samples, for processing
	static constexpr int _fs_code      =  1; // Sampling rate: 0 = 48 kHz, 1 = 44.1 kHz
	static constexpr int _i2c_addr     = 0x10 + 0;

	// GPIO pins (BCM numbering)
	static constexpr int _pin_rst      = hw::GpioPin::_snd_reset; // W - Reset pin (0 = reset, 1 = working)
	static constexpr int _pin_freq     = hw::GpioPin::_snd_sfreq; // W - Frequency selection (0 = 48 kHz, 1 = 44.1 kHz)
	static constexpr int _pin_bclk     = hw::GpioPin::_snd_bclk;  // R - I2S bit clock
	static constexpr int _pin_lrck     = hw::GpioPin::_snd_lrck;  // R - I2S word selection (0 = L, 1 = R)
	static constexpr int _pin_din      = hw::GpioPin::_snd_din;   // R - I2S data input (codec to cpu)
	static constexpr int _pin_dout     = hw::GpioPin::_snd_dout;  // W - I2S data output (cpu to codec)

	enum State
	{
		State_STOP = 0,
		State_RUN,

		State_NBR_ELT
	};

	explicit       DPvabI2sCommon (DPvabI2sPcmInterface &pcm, hw::Higepio &io, int prefill);
	               ~DPvabI2sCommon () = default;

	State          get_state () const noexcept;
	bool           is_exit_needed () const noexcept;

	int            init (double &sample_freq, int &max_block_size, CbInterface &callback) noexcept;
	int            start () noexcept;
	int            stop () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef std::vector <int32_t, fstb::AllocAlign <int32_t, 16> > BufIntAlign;
	typedef std::vector <float, fstb::AllocAlign <float, 16> > BufFltAlign;

	static const int  _block_size_a = (_block_size + 3) & ~3; // Aligned block size

	void           main_loop () noexcept;
	void           proc_loop ();

	inline void    write_reg (uint8_t reg, uint8_t val) noexcept;

	DPvabI2sPcmInterface &
	               _pcm;
	hw::Higepio &  _io;
	hw::I2cLinux   _i2c;
	CbInterface *  _cb_ptr;       // 0 = not set
	State          _state;

	std::atomic <bool>            // Main loop is required to exit ASAP.
	               _exit_flag;
	std::atomic <bool>            // Processing loop is required to exit ASAP.
	               _proc_ex_flag;
	std::atomic <int>             // Current buffer for I2S transfer, 0 or 1. The other buffer is for processing
	               _cur_buf;
	BufIntAlign    _buf_int_i;    // Double input buffer (interleaved stereo), integer data
	BufIntAlign    _buf_int_o;    // Double output buffer (interleaved stereo), integer data
	BufFltAlign    _buf_flt_i;    // Input buffer (soundchip to software processing), dual mono
	BufFltAlign    _buf_flt_o;    // Output buffer (software processing to soundchip), dual mono
	std::thread    _thread_main;
	std::mutex     _blk_proc_mtx;
	std::condition_variable
	               _blk_proc_cv;
	std::atomic <bool>
	               _proc_now_flag;
	const int      _prefill;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DPvabI2sCommon ()                               = delete;
	               DPvabI2sCommon (const DPvabI2sCommon &other)    = delete;
	               DPvabI2sCommon (DPvabI2sCommon &&other)         = delete;
	DPvabI2sCommon &
	               operator = (const DPvabI2sCommon &other)        = delete;
	DPvabI2sCommon &
	               operator = (DPvabI2sCommon &&other)             = delete;
	bool           operator == (const DPvabI2sCommon &other) const = delete;
	bool           operator != (const DPvabI2sCommon &other) const = delete;

}; // class DPvabI2sCommon



}  // namespace adrv
}  // namespace mfx



//#include "mfx/adrv/DPvabI2sCommon.hpp"



#endif   // mfx_adrv_DPvabI2sCommon_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
