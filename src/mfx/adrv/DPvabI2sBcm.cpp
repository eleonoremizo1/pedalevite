/*****************************************************************************

        DPvabI2sBcm.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/adrv/DPvabI2sBcm.h"

#include <bcm_host.h>

#include <fcntl.h>

#include <thread>

#include <cassert>



namespace mfx
{
namespace adrv
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DPvabI2sBcm::DPvabI2sBcm (hw::Higepio &io)
:	_mgr (*this, io, _prefill)
,	_io (io)
,	_pcm_mptr (
		::bcm_host_get_peripheral_address () + hw::bcm2837pcm::_pcm_ofs,
		hw::bcm2837pcm::_pcm_len,
		"/dev/mem",
		O_RDWR | O_SYNC
	)
{
	// Nothing
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	DPvabI2sBcm::do_init (double &sample_freq, int &max_block_size, CbInterface &callback, const char *driver_0, int chn_idx_in, int chn_idx_out) noexcept
{
	fstb::unused (driver_0, chn_idx_in, chn_idx_out);
	assert (chn_idx_in == 0);
	assert (chn_idx_out == 0);

	// Custom pins
	_io.set_pin_mode (_pin_rst , hw::bcm2837gpio::PinFnc_OUT);
	_io.set_pin_mode (_pin_freq, hw::bcm2837gpio::PinFnc_OUT);

	// I2S pins, mode ALT0 (p. 102)
	_io.set_pin_mode (_pin_bclk, hw::bcm2837gpio::PinFnc_ALT0);
	_io.set_pin_mode (_pin_lrck, hw::bcm2837gpio::PinFnc_ALT0);
	_io.set_pin_mode (_pin_din , hw::bcm2837gpio::PinFnc_ALT0);
	_io.set_pin_mode (_pin_dout, hw::bcm2837gpio::PinFnc_ALT0);

	const int      ret_val = _mgr.init (sample_freq, max_block_size, callback);

	return ret_val;
}



int	DPvabI2sBcm::do_start () noexcept
{
	return _mgr.start ();
}



int	DPvabI2sBcm::do_stop () noexcept
{
	return _mgr.stop ();
}



void	DPvabI2sBcm::do_restart () noexcept
{
	do_stop ();
	do_start ();
}



std::string	DPvabI2sBcm::do_get_last_error () const
{
	return "";
}



void	DPvabI2sBcm::do_pcm_prepare ()
{
	using namespace hw::bcm2837pcm;

	_pcm_mptr.at (_mode_a) =
		  _mode_a_clkm
		| _mode_a_clki
		| _mode_a_fsm
		| _mode_a_fsi
		| ((_bits_per_chn * 2 - 1) << _mode_a_flen )
		| ( _bits_per_chn          << _mode_a_fslen);
	const uint32_t chn_conf_base =
		  _xc_a_en
		| (( (_resol - 8) & 0x0F) << _xc_a_wid)
		| ((((_resol - 8) & 0x10) != 0) ? _xc_a_wex : 0);
	const uint32_t chn_conf_l =
		chn_conf_base | ( _transfer_lag                  << _xc_a_pos);
	const uint32_t chn_conf_r =
		chn_conf_base | ((_transfer_lag + _bits_per_chn) << _xc_a_pos);
	_pcm_mptr.at (_rxc_a) =
		  (chn_conf_l << _xc_a_ch1)
		| (chn_conf_r << _xc_a_ch2);
	_pcm_mptr.at (_txc_a) =
		  (chn_conf_l << _xc_a_ch1)
		| (chn_conf_r << _xc_a_ch2);
	_pcm_mptr.at (_dreq_a) =
		  (0x10 << _dreq_a_tx_panic)
		| (0x30 << _dreq_a_rx_panic)
		| (0x30 << _dreq_a_tx      )
		| (0x20 << _dreq_a_rx      );
	_pcm_mptr.at (_inten_a) = 0;
	_pcm_mptr.at (_intstc_a) =
		  _intstc_a_rxerr
		| _intstc_a_txerr
		| _intstc_a_rxr
		| _intstc_a_txw;
	_pcm_mptr.at (_gray) = 0;

	// Clears the FIFOs and errors, enables the PCM clock, sets the SYNC bit
	// that will be echoed back in the read value after 2 clocks.
	_pcm_mptr.at (_cs_a) =
		  _status_mask
		| _cs_a_sync
		| _cs_a_rxerr
		| _cs_a_txerr
		| _cs_a_rxclr
		| _cs_a_txclr;

	// The FIFO requires 2 clock cycles before being cleared.
#if 0
	// Wait for 1 ms, which should be much more than 2 clock cycles.
	std::this_thread::sleep_for (std::chrono::milliseconds (1));
#else // Is this working now?
	// Waits for the SYNC bit, so the FIFOs are actually cleared
	while ((_pcm_mptr.at (_cs_a) & _cs_a_sync) == 0 && ! _mgr.is_exit_needed ())
	{
		continue;
	}
#endif
}



void	DPvabI2sBcm::do_pcm_start () noexcept
{
	_pcm_mptr.at (hw::bcm2837pcm::_cs_a) = _status_mask_on;
}



void	DPvabI2sBcm::do_pcm_stop () noexcept
{
	_pcm_mptr.at (hw::bcm2837pcm::_cs_a) = 0;
}



int32_t	DPvabI2sBcm::do_pcm_read (int chn) noexcept
{
	fstb::unused (chn);

	return _pcm_mptr.at (hw::bcm2837pcm::_fifo_a);
}



void	DPvabI2sBcm::do_pcm_write (int chn, int32_t x) noexcept
{
	fstb::unused (chn);

	_pcm_mptr.at (hw::bcm2837pcm::_fifo_a) = x;
}



DPvabI2sPcmInterface::CheckStatusResult	DPvabI2sBcm::do_pcm_check_status (int chn) noexcept
{
	fstb::unused (chn);

	using namespace hw::bcm2837pcm;

	CheckStatusResult res;

	uint32_t       status = _pcm_mptr.at (_cs_a);

	if ((status & _cs_a_rxerr) != 0 || (status & _cs_a_txerr) != 0)
	{
#if 0 /*** To do: this test does not work, we have to check exactly why. ***/
		// Possible L/R sync errors, skips a frame to fix them
		if (chn_pos == 0)
		{
			if ((status & _cs_a_rxsync) == 0)
			{
				volatile auto dummy = _pcm_mptr.at (_fifo_a);
			}
			if ((status & _cs_a_txsync) == 0)
			{
				_pcm_mptr.at (_fifo_a) = 0;
			}
		}
#endif

		res._sync_err_flag = true;

		// Clears error at the PCM interface level
		_pcm_mptr.at (_cs_a) = _status_mask_on | _cs_a_rxerr | _cs_a_txerr;

		// Reads the status again
		status = _pcm_mptr.at (_cs_a);
	}

	// Can we read/write something ?
	res._ready_flag =
		(   (status & _cs_a_rxr) != 0
		 && (status & _cs_a_txw) != 0);

	return res;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace adrv
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
