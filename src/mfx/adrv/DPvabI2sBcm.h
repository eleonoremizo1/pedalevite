/*****************************************************************************

        DPvabI2sBcm.h
        Author: Laurent de Soras, 2019

PCM/I2S interface in polled mode

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_adrv_DPvabI2sBcm_HEADER_INCLUDED)
#define mfx_adrv_DPvabI2sBcm_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/adrv/DPvabI2sCommon.h"
#include "mfx/adrv/DPvabI2sPcmInterface.h"
#include "mfx/adrv/DriverInterface.h"
#include "mfx/hw/bcm2837pcm.h"
#include "mfx/hw/Higepio.h"
#include "mfx/hw/MmapPtr.h"

#include <cstdint>



namespace mfx
{
namespace adrv
{



class DPvabI2sBcm final
:	public DriverInterface
,	public DPvabI2sPcmInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       DPvabI2sBcm (hw::Higepio &io);
	               ~DPvabI2sBcm () = default;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// DriverInterface
	int            do_init (double &sample_freq, int &max_block_size, CbInterface &callback, const char *driver_0, int chn_idx_in, int chn_idx_out) noexcept final;
	int            do_start () noexcept final;
	int            do_stop () noexcept final;
	void           do_restart () noexcept final;
	std::string    do_get_last_error () const final;

	// DPvabI2sPcmInterface
	void           do_pcm_prepare () final;
	void           do_pcm_start () noexcept final;
	void           do_pcm_stop () noexcept final;
	int32_t        do_pcm_read (int chn) noexcept final;
	void           do_pcm_write (int chn, int32_t x) noexcept final;
	CheckStatusResult
	               do_pcm_check_status (int chn) noexcept final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _nbr_chn      = DPvabI2sCommon::_nbr_chn;
	static constexpr int _bits_per_chn = DPvabI2sCommon::_bits_per_chn;
	static constexpr int _resol        = DPvabI2sCommon::_resol;
	static constexpr int _transfer_lag = DPvabI2sCommon::_transfer_lag;

	static constexpr int _pin_rst      = DPvabI2sCommon::_pin_rst;
	static constexpr int _pin_freq     = DPvabI2sCommon::_pin_freq;
	static constexpr int _pin_bclk     = DPvabI2sCommon::_pin_bclk;
	static constexpr int _pin_lrck     = DPvabI2sCommon::_pin_lrck;
	static constexpr int _pin_din      = DPvabI2sCommon::_pin_din;
	static constexpr int _pin_dout     = DPvabI2sCommon::_pin_dout;

	// Number of sample frames written in advance in the I2S TX queue. >= 1
	// After initial filling, we sync on the RX queue (we try to keep it always
	// empty) and write the same number of read samples on the TX queue.
	static const int  _prefill = 4;
	static_assert (
		_prefill * _nbr_chn <= 64,
		"Prefill should fit in the TX FIFO."
	);

	// Status register mask for the I2S interface
	static constexpr uint32_t  _status_mask =
		  hw::bcm2837pcm::_cs_a_stby
		| hw::bcm2837pcm::_cs_a_rxsex
		| hw::bcm2837pcm::_cs_a_rxthr_one
		| hw::bcm2837pcm::_cs_a_txthr_ful1
		| hw::bcm2837pcm::_cs_a_en;
	static constexpr uint32_t  _status_mask_on =
		  _status_mask
		| hw::bcm2837pcm::_cs_a_txon
		| hw::bcm2837pcm::_cs_a_rxon;

	DPvabI2sCommon _mgr;
	hw::Higepio &  _io;
	hw::MmapPtr    _pcm_mptr;  // Virtual base address for the PCM registers



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DPvabI2sBcm ()                               = delete;
	               DPvabI2sBcm (const DPvabI2sBcm &other)       = delete;
	               DPvabI2sBcm (DPvabI2sBcm &&other)            = delete;
	DPvabI2sBcm &  operator = (const DPvabI2sBcm &other)        = delete;
	DPvabI2sBcm &  operator = (DPvabI2sBcm &&other)             = delete;
	bool           operator == (const DPvabI2sBcm &other) const = delete;
	bool           operator != (const DPvabI2sBcm &other) const = delete;

}; // class DPvabI2sBcm



}  // namespace adrv
}  // namespace mfx



//#include "mfx/adrv/DPvabI2sBcm.hpp"



#endif   // mfx_adrv_DPvabI2sBcm_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
