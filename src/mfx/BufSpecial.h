/*****************************************************************************

        BufSpecial.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_BufSpecial_HEADER_INCLUDED)
#define mfx_BufSpecial_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/Cst.h"



namespace mfx
{



enum BufSpecial
{

	// Silent input
	BufSpecial_SILENCE = 0,

	// Discarded output
	BufSpecial_TRASH,

	// Zone for layer I/O
	BufSpecial_IO,
	BufSpecial_IO_LAST =
		BufSpecial_IO + Cst::_nbr_io_zones * Cst::_nbr_buf_io - 1,

	// Zones for the layer send/return signals
	BufSpecial_RET,
	BufSpecial_RET_LAST =
		BufSpecial_RET + Cst::_nbr_ret_zones * Cst::_nbr_buf_ret - 1,

	BufSpecial_NBR_ELT

}; // enum BufSpecial



}  // namespace mfx



//#include "mfx/BufSpecial.hpp"



#endif // mfx_BufSpecial_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
