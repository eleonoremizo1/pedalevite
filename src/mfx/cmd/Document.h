/*****************************************************************************

        Document.h
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_cmd_Document_HEADER_INCLUDED)
#define mfx_cmd_Document_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/cmd/Layer.h"
#include "mfx/doc/LayerType.h"
#include "mfx/doc/ProgSwitchMode.h"
//#include "mfx/piapi/Dir.h"
#include "mfx/ChnMode.h"

#include <array>



namespace mfx
{
namespace cmd
{



class Document
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       Document (doc::LayerType cur_layer);
	               Document (const Document &other) = default;
	               Document (Document &&other)      = default;
	               ~Document ()                     = default;

	void           set_layer (doc::LayerType cur_layer) noexcept;

	Plugin &       find_plugin (int pi_id) noexcept;

	bool           is_plugin_in_use (int pi_id) const noexcept;
	std::optional <Layer::PluginLoc>
	               get_slot_pos_if_exist (int pi_id) const noexcept;
	void           set_plugin_loc (int pi_id, Layer::PluginLoc loc);
	void           remove_plugin_loc_if_exist (int pi_id);

	std::vector <Slot> &
	               use_slot_list () noexcept;
	const std::vector <Slot> &
	               use_slot_list () const noexcept;

	void           set_plugin_instance_use (const std::string &model, int pi_id, bool use_flag);
	std::optional <Layer::ModelMap::iterator>
	               find_model (const std::string &model) noexcept;

	bool           has_ctx () const noexcept;
	void           create_ctx ();
	void           destroy_ctx ();
	ProcessingContext &
	               use_ctx () noexcept;
	const ProcessingContext &
	               use_ctx () const noexcept;
	Layer::ContextSPtr
	               get_ctx_sptr () const noexcept;

	Layer::CnxList &
	               use_cnx_list () noexcept;
	const Layer::CnxList &
	               use_cnx_list () const noexcept;

	Layer::PluginAuxList &
	               use_pi_dly_list () noexcept;
	const Layer::PluginAuxList &
	               use_pi_dly_list () const noexcept;

	ChnMode        _chn_mode         = ChnMode_2M_2M;
	float          _master_vol       = 1;
	doc::ProgSwitchMode
	               _prog_switch_mode = doc::ProgSwitchMode::DIRECT;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	doc::LayerType _cur_layer = doc::LayerType::INVALID;

	std::array <Layer, size_t (doc::LayerType::NBR_ELT)>
	               _layer_arr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               Document ()                               = delete;
	Document &     operator = (const Document &other)        = delete;
	Document &     operator = (Document &&other)             = delete;
	bool           operator == (const Document &other) const = delete;
	bool           operator != (const Document &other) const = delete;

}; // class Document



}  // namespace cmd
}  // namespace mfx



//#include "mfx/cmd/Document.hpp"



#endif   // mfx_cmd_Document_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
