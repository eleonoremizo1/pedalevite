/*****************************************************************************

        SlotType.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_cmd_SlotType_HEADER_INCLUDED)
#define mfx_cmd_SlotType_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace cmd
{



enum SlotType
{

		SlotType_INVALID = -1,

		SlotType_NORMAL = 0,
		SlotType_IO,   // Audio In/Out. Input or output depends on the context
		SlotType_RS,   // Return/Send. Same
		SlotType_DLY,

		SlotType_NBR_ELT

}; // enum SlotType



}  // namespace cmd
}  // namespace mfx



//#include "mfx/cmd/SlotType.hpp"



#endif // mfx_cmd_SlotType_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
