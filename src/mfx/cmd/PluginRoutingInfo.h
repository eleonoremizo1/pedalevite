/*****************************************************************************

        PluginRoutingInfo.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_cmd_PluginRoutingInfo_HEADER_INCLUDED)
#define mfx_cmd_PluginRoutingInfo_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace cmd
{



class PluginRoutingInfo
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Number of input channels, > 0. 0 = not set
	int            _nbr_chn_i = 0;

	// Number of output channels, >= _nbr_chn_i. 0 = not set
	int            _nbr_chn_o = 0;

	// Plugin processing latency between input and output, in samples
	int            _latency   = 0;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PluginRoutingInfo &other) const = delete;
	bool           operator != (const PluginRoutingInfo &other) const = delete;

}; // class PluginRoutingInfo



}  // namespace cmd
}  // namespace mfx



//#include "mfx/cmd/PluginRoutingInfo.hpp"



#endif // mfx_cmd_PluginRoutingInfo_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
