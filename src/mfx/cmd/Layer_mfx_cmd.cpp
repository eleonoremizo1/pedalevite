/*****************************************************************************

        Layer_mfx_cmd.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/cmd/Layer.h"

#include <cassert>



namespace mfx
{
namespace cmd
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// Must exist
Plugin &	Layer::find_plugin (int pi_id) noexcept
{
	assert (pi_id >= 0);

	auto           it_loc = _map_id_loc.find (pi_id);
	assert (it_loc != _map_id_loc.end ());

	const int      pos  = it_loc->second._slot_pos;
	const PiType   type = it_loc->second._type;

	Slot &         slot = _slot_list [pos];
	Plugin &       plug = slot._component_arr [type];

	return plug;
}



bool	Layer::is_plugin_in_use (int pi_id) const noexcept
{
	assert (pi_id >= 0);

	const auto     it_loc = _map_id_loc.find (pi_id);

	return (it_loc != _map_id_loc.end ());
}



std::optional <Layer::PluginLoc>	Layer::get_slot_pos_if_exist (int pi_id) const noexcept
{
	assert (pi_id >= 0);

	std::optional <PluginLoc> loc_opt;

	const auto     it_loc = _map_id_loc.find (pi_id);
	if (it_loc != _map_id_loc.end ())
	{
		loc_opt.emplace (it_loc->second);
	}

	return loc_opt;
}



void	Layer::set_plugin_loc (int pi_id, PluginLoc loc)
{
	assert (pi_id >= 0);
	assert (loc._slot_pos >= 0);
	assert (loc._type >= 0);
	assert (loc._type < PiType::PiType_NBR_ELT);

	_map_id_loc [pi_id] = loc;
}



void	Layer::remove_plugin_loc_if_exist (int pi_id)
{
	assert (pi_id >= 0);

	auto           it_loc = _map_id_loc.find (pi_id);
	if (it_loc != _map_id_loc.end ())
	{
		_map_id_loc.erase (it_loc);
	}
}



std::vector <Slot> &	Layer::use_slot_list () noexcept
{
	return _slot_list;
}



const std::vector <Slot> &	Layer::use_slot_list () const noexcept
{
	return _slot_list;
}



void	Layer::set_plugin_instance_use (const std::string &model, int pi_id, bool use_flag)
{
	assert (! model.empty ());
	assert (pi_id >= 0);

	_map_model_id [model] [pi_id] = use_flag;
}



std::optional <Layer::ModelMap::iterator>	Layer::find_model (const std::string &model) noexcept
{
	assert (! model.empty ());

	const auto     it = _map_model_id.find (model);
	if (it != _map_model_id.end ())
	{
		return it;
	}

	return {};
}



bool	Layer::has_ctx () const noexcept
{
	return (_ctx_sptr.get () != nullptr);
}



void	Layer::create_ctx ()
{
	_ctx_sptr = std::make_shared <ProcessingContext> ();
}



void	Layer::destroy_ctx ()
{
	_ctx_sptr.reset ();
}



ProcessingContext &	Layer::use_ctx () noexcept
{
	return *_ctx_sptr;
}



const ProcessingContext &	Layer::use_ctx () const noexcept
{
	return *_ctx_sptr;
}



Layer::ContextSPtr	Layer::get_ctx_sptr () const noexcept
{
	return _ctx_sptr;
}



Layer::CnxList &	Layer::use_cnx_list () noexcept
{
	return _cnx_list;
}



const Layer::CnxList &	Layer::use_cnx_list () const noexcept
{
	return _cnx_list;
}



Layer::PluginAuxList &	Layer::use_pi_dly_list () noexcept
{
	return _plugin_dly_list;
}



const Layer::PluginAuxList &	Layer::use_pi_dly_list () const noexcept
{
	return _plugin_dly_list;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace cmd
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
