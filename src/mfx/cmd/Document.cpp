/*****************************************************************************

        Document.cpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/cmd/Document.h"

#include <cassert>



namespace mfx
{
namespace cmd
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



Document::Document (doc::LayerType cur_layer)
:	_cur_layer (cur_layer)
{
	assert (int (cur_layer) >= 0);
	assert (int (cur_layer) < int (doc::LayerType::NBR_ELT));
}



void	Document::set_layer (doc::LayerType cur_layer) noexcept
{
	assert (int (cur_layer) >= 0);
	assert (int (cur_layer) < int (doc::LayerType::NBR_ELT));

	_cur_layer = cur_layer;
}



// Must exist
Plugin &	Document::find_plugin (int pi_id) noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.find_plugin (pi_id);
}



bool	Document::is_plugin_in_use (int pi_id) const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.is_plugin_in_use (pi_id);
}



std::optional <Layer::PluginLoc>	Document::get_slot_pos_if_exist (int pi_id) const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.get_slot_pos_if_exist (pi_id);
}



void	Document::set_plugin_loc (int pi_id, Layer::PluginLoc loc)
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	layer.set_plugin_loc (pi_id, loc);
}



void	Document::remove_plugin_loc_if_exist (int pi_id)
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	layer.remove_plugin_loc_if_exist (pi_id);
}



std::vector <Slot> &	Document::use_slot_list () noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.use_slot_list ();
}



const std::vector <Slot> &	Document::use_slot_list () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.use_slot_list ();
}



void	Document::set_plugin_instance_use (const std::string &model, int pi_id, bool use_flag)
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	layer.set_plugin_instance_use (model, pi_id, use_flag);
}



std::optional <Layer::ModelMap::iterator>	Document::find_model (const std::string &model) noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.find_model (model);
}



bool	Document::has_ctx () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.has_ctx ();
}



void	Document::create_ctx ()
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	layer.create_ctx ();
}



void	Document::destroy_ctx ()
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	layer.destroy_ctx ();
}



ProcessingContext &	Document::use_ctx () noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.use_ctx ();
}



const ProcessingContext &	Document::use_ctx () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.use_ctx ();
}



Layer::ContextSPtr	Document::get_ctx_sptr () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.get_ctx_sptr ();
}



Layer::CnxList &	Document::use_cnx_list () noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.use_cnx_list ();
}



const Layer::CnxList &	Document::use_cnx_list () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.use_cnx_list ();
}



Layer::PluginAuxList &	Document::use_pi_dly_list () noexcept
{
	auto &         layer = _layer_arr [int (_cur_layer)];

	return layer.use_pi_dly_list ();
}



const Layer::PluginAuxList &	Document::use_pi_dly_list () const noexcept
{
	const auto &   layer = _layer_arr [int (_cur_layer)];

	return layer.use_pi_dly_list ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace cmd
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
