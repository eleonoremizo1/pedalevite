/*****************************************************************************

        Layer.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_cmd_Layer_HEADER_INCLUDED)
#define mfx_cmd_Layer_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/cmd/Cnx.h"
#include "mfx/cmd/PluginAux.h"
#include "mfx/cmd/Slot.h"
#include "mfx/PiType.h"
#include "mfx/ProcessingContext.h"

#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>



namespace mfx
{
namespace cmd
{


class Plugin;

class Layer
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef std::vector <Cnx> CnxList;
	typedef std::vector <PluginAux> PluginAuxList;

	typedef std::map <int, bool> InstanceMap; // [Id] = use
	typedef std::map <std::string, InstanceMap> ModelMap; // [model] = map

	class PluginLoc
	{
	public:
		int            _slot_pos = -1;
		PiType         _type     = PiType::PiType_INVALID;
	};

	typedef std::shared_ptr <ProcessingContext> ContextSPtr;

	               Layer ()                   = default;
	               Layer (const Layer &other) = default;
	               Layer (Layer &&other)      = default;

	Plugin &       find_plugin (int pi_id) noexcept;

	bool           is_plugin_in_use (int pi_id) const noexcept;
	std::optional <PluginLoc>
	               get_slot_pos_if_exist (int pi_id) const noexcept;
	void           set_plugin_loc (int pi_id, PluginLoc loc);
	void           remove_plugin_loc_if_exist (int pi_id);

	std::vector <Slot> &
	               use_slot_list () noexcept;
	const std::vector <Slot> &
	               use_slot_list () const noexcept;

	void           set_plugin_instance_use (const std::string &model, int pi_id, bool use_flag);
	std::optional <ModelMap::iterator>
	               find_model (const std::string &model) noexcept;

	bool           has_ctx () const noexcept;
	void           create_ctx ();
	void           destroy_ctx ();
	ProcessingContext &
	               use_ctx () noexcept;
	const ProcessingContext &
	               use_ctx () const noexcept;
	ContextSPtr    get_ctx_sptr () const noexcept;

	CnxList &      use_cnx_list () noexcept;
	const CnxList& use_cnx_list () const noexcept;

	PluginAuxList& use_pi_dly_list () noexcept;
	const PluginAuxList &
	               use_pi_dly_list () const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	std::vector <Slot>
	               _slot_list;

	// All the existing plug-ins, ordered by model.
	ModelMap       _map_model_id;

	// [plugin_id] = location. Only main/dwm plug-ins, no compensation delay
	std::map <int, PluginLoc>
	               _map_id_loc;

	ContextSPtr    _ctx_sptr;

	// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	// Additions for graph routing

	// Structure, part of the data accessed by Central. Should only contain
	// only SlotType_NORMAL, SlotType_IO
	CnxList        _cnx_list;

	// Additional data created by the routing, for the audio processing

	// Delay plug-ins
	PluginAuxList  _plugin_dly_list;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	Layer &        operator = (const Layer &other)        = delete;
	Layer &        operator = (Layer &&other)             = delete;
	bool           operator == (const Layer &other) const = delete;
	bool           operator != (const Layer &other) const = delete;

}; // class Layer



}  // namespace cmd
}  // namespace mfx



//#include "mfx/cmd/Layer.hpp"



#endif // mfx_cmd_Layer_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
