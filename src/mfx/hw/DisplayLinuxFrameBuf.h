/*****************************************************************************

        DisplayLinuxFrameBuf.h
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_DisplayLinuxFrameBuf_HEADER_INCLUDED)
#define mfx_hw_DisplayLinuxFrameBuf_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/ui/DisplayInterface.h"

#include <linux/fb.h>

#include <string>
#include <vector>



namespace mfx
{
namespace hw
{



class DisplayLinuxFrameBuf final
:	public ui::DisplayInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Display rotation, in quarter turns, counter-clockwise
	static constexpr int _rot = 3; // 270 deg

	explicit       DisplayLinuxFrameBuf (std::string dev_name);
	virtual        ~DisplayLinuxFrameBuf ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// DisplayInterface
	int            do_get_width () const noexcept final;
	int            do_get_height () const noexcept final;
	int            do_get_stride () const noexcept final;
	ui::PixArgb *  do_use_screen_buf () noexcept final;
	const ui::PixArgb *
	               do_use_screen_buf () const noexcept final;

	void           do_refresh (int x, int y, int w, int h) final;
	void           do_force_reset () final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef std::vector <ui::PixArgb> FrameBufInt;

	void           clean_up ();
	void           refresh_argb32 (int x, int y, int w, int h) noexcept;
	void           refresh_rgb16 (int x, int y, int w, int h) noexcept;

	static bool    is_supported_argb32 (const ::fb_var_screeninfo &info_var) noexcept;
	static bool    is_supported_rgb16 (const ::fb_var_screeninfo &info_var) noexcept;

	template <int INV_FLAG, typename T>
	static inline T
	               sgn (T a) noexcept;
	template <int INV_FLAG, typename T, typename U>
	static inline void
	               add (T &a, U b) noexcept;

	std::string    _dev_name;  // Linux output device. For example "/dev/fb0". -1 if not open or failed
	int            _tty_fd;    // File descriptor for the console
	int            _fb_fd;     // File descriptor for the framebuffer
	::fb_fix_screeninfo
	               _info_fix;
	::fb_var_screeninfo
	               _info_var;
	uint8_t *      _mm_fb_ptr; // Address allocated with mmap
	int            _map_len;
	uint8_t *      _pix_fb_ptr;// Pointer on the top-left pixel (system FB)
	int            _stride_s;  // Stride for the system framebuffer, in bytes.
	int            _ss_pix;    // Same, in pixels.
	int            _bypp;      // Bytes per pixel for the framebuf. 2 = RGB16, 4 = RGB32

	int            _disp_w;    // Width in zoomed pixels
	int            _disp_h;    // Height in zoomed pixels

	FrameBufInt    _fb_int;    // Internal frame buffer
	int            _stride_i;  // Stride for the internal framebuffer, in bytes



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               DisplayLinuxFrameBuf ()                               = delete;
	               DisplayLinuxFrameBuf (const DisplayLinuxFrameBuf &other) = delete;
	               DisplayLinuxFrameBuf (DisplayLinuxFrameBuf &&other)   = delete;
	DisplayLinuxFrameBuf &
	               operator = (const DisplayLinuxFrameBuf &other)        = delete;
	DisplayLinuxFrameBuf &
	               operator = (DisplayLinuxFrameBuf &&other)             = delete;
	bool           operator == (const DisplayLinuxFrameBuf &other) const = delete;
	bool           operator != (const DisplayLinuxFrameBuf &other) const = delete;

}; // class DisplayLinuxFrameBuf



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/DisplayLinuxFrameBuf.hpp"



#endif   // mfx_hw_DisplayLinuxFrameBuf_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
