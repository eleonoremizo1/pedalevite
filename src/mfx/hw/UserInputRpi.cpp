/*****************************************************************************

        UserInputRpi.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#if ! defined (PV_VERSION)
	#error PV_VERSION should be defined.
#endif



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/hw/GpioPin.h"
#include "mfx/hw/mcp23017.h"
#if mfx_features_POTREL == mfx_features_POTREL_DISCRETE
	#include "mfx/hw/SpiAccessSimple.h"
#else
	#include "mfx/hw/SpiAccessGpio.h"
#endif // mfx_features_POTREL
#include "mfx/hw/UserInputRpi.h"
#include "mfx/ui/TimeShareThread.h"

#include <unistd.h>

#include <array>
#include <chrono>
#include <memory>
#include <stdexcept>
#include <thread>

#include <cassert>
#include <climits>
#include <ctime>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



const std::chrono::nanoseconds	UserInputRpi::_antibounce_time (
	std::chrono::milliseconds (30)
);

// Slave address, p. 8
const int	UserInputRpi::_i2c_dev_23017_arr [_nbr_dev_23017] =
{
	0x20 + 0,
	0x20 + 1
};

const int	UserInputRpi::_gpio_pin_arr [_nbr_sw_gpio] =
{
	GpioPin::_nav_ok, GpioPin::_nav_cancel
};

const UserInputRpi::SwitchSrc	UserInputRpi::_switch_arr [_nbr_switches] =
{
	{ BinSrc_GPIO    ,    0 },
	{ BinSrc_GPIO    ,    1 },
	// MCP23017 unit 0
	{ BinSrc_PORT_EXP, 0x00 },
	{ BinSrc_PORT_EXP, 0x01 },
	{ BinSrc_PORT_EXP, 0x02 },
	{ BinSrc_PORT_EXP, 0x03 },
	{ BinSrc_PORT_EXP, 0x04 },
	{ BinSrc_PORT_EXP, 0x05 },
	{ BinSrc_PORT_EXP, 0x06 },
	{ BinSrc_PORT_EXP, 0x07 },
	{ BinSrc_PORT_EXP, 0x08 },
	{ BinSrc_PORT_EXP, 0x09 },
	{ BinSrc_PORT_EXP, 0x0A },
	{ BinSrc_PORT_EXP, 0x0B },
	{ BinSrc_PORT_EXP, 0x0C },
	{ BinSrc_PORT_EXP, 0x0D },
	{ BinSrc_PORT_EXP, 0x0E },
	{ BinSrc_PORT_EXP, 0x0F },
	// MCP23017 unit 1
	{ BinSrc_PORT_EXP, 0x1C },
	{ BinSrc_PORT_EXP, 0x1F }
};

const UserInputRpi::RotEncSrc	UserInputRpi::_rotenc_arr [_nbr_rot_enc] =
{
#if (PV_VERSION == 1)
	// Rotary encoder wiring was inverted in Pedale Vite v1
	#define mfx_hw_UserInputRpi_ROTENC_DIR (-1)
#else
	#define mfx_hw_UserInputRpi_ROTENC_DIR (+1)
#endif

#if mfx_features_POTREL == mfx_features_POTREL_DISCRETE
	// Parameters
	{ BinSrc_PORT_EXP, 0x10, 0x11, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel (PotRel_GEN + 0) },
	{ BinSrc_PORT_EXP, 0x12, 0x13, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel (PotRel_GEN + 1) },
	{ BinSrc_PORT_EXP, 0x14, 0x15, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel (PotRel_GEN + 2) },
	{ BinSrc_PORT_EXP, 0x16, 0x17, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel (PotRel_GEN + 3) },
	{ BinSrc_PORT_EXP, 0x18, 0x19, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel (PotRel_GEN + 4) },
#endif // mfx_features_POTREL

	// Navigation
	{ BinSrc_PORT_EXP, 0x1A, 0x1B, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel_NAV_VER },
	{ BinSrc_PORT_EXP, 0x1D, 0x1E, mfx_hw_UserInputRpi_ROTENC_DIR, PotRel_NAV_HOR }

#undef mfx_hw_UserInputRpi_ROTENC_DIR
};

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE

const std::array <UserInputRpi::EndlessPotSrc, UserInputRpi::_nbr_endless_pot>	UserInputRpi::_endless_pot_arr =
{{
	{ 1, {{ 0, 1 }}, PotRel (PotRel_GEN + 0) },
	{ 1, {{ 2, 3 }}, PotRel (PotRel_GEN + 1) },
	{ 1, {{ 4, 5 }}, PotRel (PotRel_GEN + 2) },
	{ 1, {{ 6, 7 }}, PotRel (PotRel_GEN + 3) },
	{ 0, {{ 0, 1 }}, PotRel (PotRel_GEN + 4) }
}};

#endif // mfx_features_POTREL

const std::array <UserInputRpi::PotSrc, Cst::_nbr_pot_abs> UserInputRpi::_pot_arr =
{{
	{ 0, 2 }, { 0, 3 }, { 0, 4 }
}};


// Before calling:
// io.set_pin_dir (_pin_rst, true);
// io.write_pin (_pin_rst, 0); ::delay (100);
// io.write_pin (_pin_rst, 1); ::delay (1);
UserInputRpi::UserInputRpi (ui::TimeShareThread &thread_spi, Higepio &io)
:	_thread_spi (thread_spi)
,	_io (io)
,	_hnd_23017_arr {
		I2cLinux { _i2c_dev_23017_arr [0], "Cannot open I2C for MCP23017 unit 0" },
		I2cLinux { _i2c_dev_23017_arr [1], "Cannot open I2C for MCP23017 unit 1" }
	}
,	_hnd_3008_arr {
#if mfx_features_POTREL == mfx_features_POTREL_DISCRETE
		SpiLinux {
			std::make_unique <SpiAccessSimple> (_spi_port, _spi_rate),
			"Cannot open SPI for MCP3008 dev 0"
		}
#else  // mfx_features_POTREL
		SpiLinux {
			std::make_unique <SpiAccessGpio> (io, GpioPin::_spl_csadc0, _spi_rate),
			"Cannot open SPI for MCP3008 dev 0"
		},
		SpiLinux {
			std::make_unique <SpiAccessGpio> (io, GpioPin::_spl_csadc1, _spi_rate),
			"Cannot open SPI for MCP3008 dev 1"
		}
#endif // mfx_features_POTREL
	}
,	_recip_list ()
,	_switch_state_arr ()
,	_pot_state_arr ()
,	_rotenc_state_arr ()
,	_msg_pool ()
,	_quit_flag (false)
,	_polling_thread ()
,	_polling_count (0)
{
	for (int i = 0; i < _nbr_sw_gpio; ++i)
	{
		const auto     gpio = _gpio_pin_arr [i];
		io.set_pin_dir (gpio, false);
		io.set_pull (gpio, PinPullType::NONE);
	}

	_msg_pool.expand_to (256);
	for (int i = 0; i < ui::UserInputType_NBR_ELT; ++i)
	{
		const int      nbr_dev =
			do_get_nbr_param (static_cast <ui::UserInputType> (i));
		_recip_list [i].resize (nbr_dev, nullptr);
	}

	for (auto &hnd : _hnd_23017_arr)
	{
		hnd.write_reg_8 (mcp23017::cmd_iocona, mcp23017::iocon_mirror);

		// All the pins are set in read mode.
		hnd.write_reg_16 (mcp23017::cmd_iodira, 0xFFFF);
	}

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	for (auto &pot : _pot_endless_state_arr)
	{
		pot.set_sample_freq (_sample_freq_adc);
		pot.set_sensitivity (0.1f);

		// 1 ADC step in the PotEndless input range
		constexpr auto adc_step =
			float (1u << ui::PotEndless::_resol) / float (1u << _res_adc);

		// After the adaptative filtering, a hysteresis of 1 ADC step is
		// probably enough to get rid of most of the remaining noise.
		// Maybe more because the resulting value depends on two readings
		// on two different inputs?
		pot.set_hysteresis (1 * adc_step);

		// Half an ADC step for the delta threshold
		pot.set_delta_min (0.5f / float (1 << _res_adc));

		//pot.set_safety_margin (....);
	}
#endif // mfx_features_POTREL

	// Initial read
	read_data (true);

	_polling_thread = std::thread (&UserInputRpi::polling_loop, this);
	_thread_spi.register_cb (
		*this,
		std::chrono::microseconds (1'000'000 / _sample_freq_adc)
	);
}



UserInputRpi::~UserInputRpi ()
{
	_thread_spi.remove_cb (*this);
	close_everything ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	UserInputRpi::do_get_nbr_param (ui::UserInputType type) const
{
	int            nbr = 0;

	if (type == ui::UserInputType_POT_ABS)
	{
		nbr = Cst::_nbr_pot_abs;
	}
	else if (type == ui::UserInputType_SW)
	{
		nbr = _nbr_switches;
	}
	else if (type == ui::UserInputType_POT_REL)
	{
		nbr = PotRel_NBR_ELT;
	}

	return nbr;
}



void	UserInputRpi::do_set_msg_recipient (ui::UserInputType type, int index, MsgQueue *queue_ptr)
{
	const bool     send_flag =
		(queue_ptr != nullptr &&_recip_list [type] [index] != queue_ptr);

	_recip_list [type] [index] = queue_ptr;

	if (send_flag)
	{
		switch (type)
		{
		case ui::UserInputType_SW:
			{
				// Creates an event only if the switch is ON (assumes OFF by default).
				const SwitchState &  state = _switch_state_arr [index];
				if (state.is_set () && state._flag)
				{
					enqueue_val (
						state._time_last,
						ui::UserInputType_SW,
						index,
						(state._flag) ? 1 : 0
					);
				}
			}
			break;
		case ui::UserInputType_POT_ABS:
			{
				assert (index >= 0);
				assert (index < Cst::_nbr_pot_abs);
				const PotState &  state = _pot_state_arr [index];
				if (state.is_set ())
				{
					const float    val_flt  =
						float (state._cur_val) * (1.0f / ((1 << _res_adc) - 1));
					const std::chrono::nanoseconds   cur_time (read_clock_ns ());
					enqueue_val (
						cur_time,
						ui::UserInputType_POT_ABS,
						index,
						val_flt
					);
				}
			}
			break;
		default:
			// Nothing
			break;
		}
	}
}



void	UserInputRpi::do_return_cell (MsgCell &cell)
{
	_msg_pool.return_cell (cell);
}



std::chrono::microseconds	UserInputRpi::do_get_cur_date () const
{
	return std::chrono::duration_cast <std::chrono::microseconds> (
		read_clock_ns ()
	);
}



bool	UserInputRpi::do_process_timeshare_op ()
{
	const std::chrono::nanoseconds   cur_time (read_clock_ns ());

	// Potentiometers
	for (int i = 0; i < Cst::_nbr_pot_abs; ++i)
	{
		const auto &   src = _pot_arr [i];
		auto &         hnd = _hnd_3008_arr [src._adc_idx];
		const int      val = read_adc (hnd, src._chn_idx);
		if (val >= 0)
		{
			handle_pot (i, val, cur_time);
		}
	}

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	// Endless potentiometers
	for (int i = 0; i < _nbr_endless_pot; ++i)
	{
		const auto &   src = _endless_pot_arr [i];
		auto &         hnd = _hnd_3008_arr [src._adc_idx];
		const auto     v0  = read_adc (hnd, src._chn_idx_arr [0]);
		const auto     v1  = read_adc (hnd, src._chn_idx_arr [1]);
		if (v0 >= 0 && v1 >= 0)
		{
			handle_endless_pot (i, v0, v1, cur_time);
		}
	}
#endif // mfx_features_POTREL

	return false;
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	UserInputRpi::close_everything ()
{
	if (_polling_thread.joinable ())
	{
		_quit_flag = true;
		_polling_thread.join ();
	}
}



void	UserInputRpi::polling_loop ()
{
	while (! _quit_flag)
	{
		const bool     low_freq_flag = ((_polling_count & 15) == 0);
		read_data (low_freq_flag);

		// 1 ms between updates. Not less because of the rotary encoders.
		std::this_thread::sleep_for (std::chrono::milliseconds (1));
		++ _polling_count;
	}

	_quit_flag = false;
}



void	UserInputRpi::read_data (bool low_freq_flag)
{
	typedef unsigned int InputState;
	static_assert (
		sizeof (InputState) * CHAR_BIT >= _nbr_sw_23017 * _nbr_dev_23017,
		"state capacity for MCP23017"
	);
	static_assert (
		sizeof (InputState) * CHAR_BIT >= _nbr_sw_gpio,
		"state capacity for GPIO"
	);

	const std::chrono::nanoseconds   cur_time (read_clock_ns ());

	// Reads all binary inputs first
	InputState     input_state_arr [BinSrc_NBR_ELT] = { 0, 0 };

	static const InputState mask = (1U << _nbr_sw_23017) - 1;
	for (int p = 0; p < _nbr_dev_23017; ++p)
	{
		InputState     dev_state = InputState (
			_hnd_23017_arr [p].read_reg_16 (mcp23017::cmd_gpioa)
		);
		dev_state ^= mask;
		input_state_arr [BinSrc_PORT_EXP] |= dev_state << (p * _nbr_sw_23017);
	}

	if (low_freq_flag)
	{
		for (int p = 0; p < _nbr_sw_gpio; ++p)
		{
			InputState     sw_val = InputState (
				_io.read_pin (_gpio_pin_arr [p]) & 1
			);
			sw_val ^= 1;
			input_state_arr [BinSrc_GPIO] |= sw_val << p;
		}

		// Switches
		for (int s = 0; s < _nbr_switches; ++s)
		{
			const SwitchSrc & src = _switch_arr [s];
			const int      val    = (input_state_arr [src._type] >> src._pos) & 1;
			const bool     flag   = (val != 0);
			handle_switch (s, flag, cur_time);
		}
	}

	// Rotary incremental encoders
	for (int i = 0; i < _nbr_rot_enc; ++i)
	{
		const RotEncSrc & src       = _rotenc_arr [i];
		const InputState  src_state = input_state_arr [src._type];
		const int         v0        = (src_state >> src._pos_0) & 1;
		const int         v1        = (src_state >> src._pos_1) & 1;
		handle_rotenc (i, (v0 != 0), (v1 != 0), cur_time);
	}
}



void	UserInputRpi::handle_switch (int index, bool flag, std::chrono::nanoseconds cur_time)
{
	SwitchState &  sw = _switch_state_arr [index];

	const std::chrono::nanoseconds   dist (cur_time - sw._time_last);
	if (flag != sw._flag && dist >= _antibounce_time)
	{
		sw._flag      = flag;
		sw._time_last = cur_time;
		enqueue_val (cur_time, ui::UserInputType_SW, index, (flag) ? 1 : 0);
	}
	else if (! sw.is_set ())
	{
		// Does noe generate any event when reading the initial value
		sw._flag      = flag;
		sw._time_last = cur_time;
	}
}



void	UserInputRpi::handle_rotenc (int index, bool f0, bool f1, std::chrono::nanoseconds cur_time)
{
	ui::RotEnc &   re     = _rotenc_state_arr [index];
	const auto     pr_idx = int (_rotenc_arr [index]._pr_idx);
	const auto     dir    = _rotenc_arr [index]._dir_mul;
	constexpr auto scale  = float (Cst::_step_param);
	const auto     inc    = float (re.set_new_state (f0, f1) * dir) * scale;
	if (inc != 0)
	{
		enqueue_val (cur_time, ui::UserInputType_POT_REL, pr_idx, inc);
	}
}



void	UserInputRpi::handle_pot (int index, int val, std::chrono::nanoseconds cur_time)
{
	assert (index >= 0);
	assert (index < Cst::_nbr_pot_abs);

	PotState &     pot      = _pot_state_arr [index];

	// Filters out the oscillations between two consecutive values
	bool           new_flag = false;
	const int      d_cur    = std::abs (val - pot._cur_val);
	const int      d_alt    = std::abs (val - pot._alt_val);
	if (d_cur != 0 && d_alt != 0)
	{
		new_flag = true;
		if (d_cur == 1)
		{
			pot._alt_val = pot._cur_val;
		}
		else if (d_alt > 1)
		{
			pot._alt_val = PotState::_val_none;
		}
		pot._cur_val = val;
	}
	else if (d_cur != 0)
	{
		assert (d_alt == 0);
		assert (d_cur == 1);
	}
	else
	{
		assert (d_cur == 0);
	}

	if (new_flag)
	{
		constexpr auto range_full = (1 << _res_adc) - 1;
		constexpr auto range_real = range_full - _dead_zone_hi - _dead_zone_lo;
		static_assert (range_real > 0, "Too large dead zones");
		// We use a mult instead of a div, so we make sure to reach at least 1,
		// not just 0.999... because of f32 rounding errors.
		constexpr auto round_fix  = 5e-7f; // An arbitrary handful of FLT_EPSILON
		constexpr auto scale      = (1.f + round_fix) / float (range_real);
		const auto     val_flt    =
			fstb::limit (float (val - _dead_zone_lo) * scale, 0.f, 1.f);
		enqueue_val (cur_time, ui::UserInputType_POT_ABS, index, val_flt);
	}
}



#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE

void	UserInputRpi::handle_endless_pot (int index, int v0, int v1, std::chrono::nanoseconds cur_time)
{
	assert (index >= 0);
	assert (index < _nbr_endless_pot);

	constexpr int  shift = ui::PotEndlessDelta::_resol - _res_adc;
	static_assert (shift >= 0, "Resolution issue");
	v0 <<= shift;
	v1 <<= shift;

	auto &         pot   = _pot_endless_state_arr [index];
	auto           delta = pot.process_sample (
		ui::PotEndless::PotVal { uint16_t (v0), uint16_t (v1) }
	);

	// Converts the delta in full truns into a delta in pseudo-potentiometer
	// course, which is a bit shorter.
	delta *= float (Cst::PotCourse::den) / float (Cst::PotCourse::num);

	// We can test strict equality with 0 as the thresholded equality is
	// handled by the PotEndlessDelta class.
	if (delta != 0)
	{
		const auto &   src    = _endless_pot_arr [index];
		const auto     pr_idx = int (src._pr_idx);
		enqueue_val (cur_time, ui::UserInputType_POT_REL, pr_idx, delta);
	}
}

#endif // mfx_features_POTREL



// date is in nanoseconds
void	UserInputRpi::enqueue_val (std::chrono::nanoseconds date, ui::UserInputType type, int index, float val)
{
	MsgQueue *     queue_ptr = _recip_list [type] [index];
	if (queue_ptr != nullptr)
	{
		MsgCell *      cell_ptr = _msg_pool.take_cell (true);
		if (cell_ptr == nullptr)
		{
			assert (false);
		}
		else
		{
			cell_ptr->_val.set (
				std::chrono::duration_cast <std::chrono::microseconds> (date),
				type,
				index,
				val
			);
			queue_ptr->enqueue (*cell_ptr);
		}
	}
}



// Returns negative number on error
// Valid results are in range 0-1023
int	UserInputRpi::read_adc (SpiLinux &port, int chn)
{
	assert (chn >= 0);
	assert (chn < _nbr_adc_chn);

	// MCP3008 doc, p. 21

	// Amount of bit shifting, from 0 to 7.
	// Only 2 and 3 are compatible with the 12864ZH (ST7920) Chip Select bug.
	constexpr int  s = 3;

	constexpr auto msg_len  = 3;
	constexpr auto start    = 1 << 4;
	constexpr auto sgl_diff = 1 << 3; // Single-ended reading
	constexpr auto header   = sgl_diff | start;
	const auto     chns4    = (header | chn) << (s + 4);
	std::array <uint8_t, msg_len> buffer {
		uint8_t (chns4 >> 8),
		uint8_t (chns4),
		0
	};

	int            ret_val = port.rw_data (&buffer [0], msg_len);
	if (ret_val == 0)
	{
		auto           flat = (buffer [1] << 8) + buffer [2];
		if constexpr (s == 7)
		{
			flat += buffer [0] << 16;
		}
		ret_val = (flat >> s) & 0x3FF;
	}

	return ret_val;
}



std::chrono::nanoseconds	UserInputRpi::read_clock_ns () const
{
	timespec       tp;
	clock_gettime (CLOCK_REALTIME, &tp);

	constexpr long ns_mul = 1'000'000'000L;

	return std::chrono::nanoseconds (int64_t (tp.tv_sec) * ns_mul + tp.tv_nsec);
}



UserInputRpi::SwitchState::SwitchState ()
:	_flag (false)
,	_time_last (INT64_MIN)
{
	// Nothing
}



bool	UserInputRpi::SwitchState::is_set () const
{
	return (_time_last.count () != INT64_MIN);
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
