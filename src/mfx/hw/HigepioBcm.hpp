/*****************************************************************************

        HigepioBcm.hpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_hw_HigepioBcm_CODEHEADER_INCLUDED)
#define mfx_hw_HigepioBcm_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	HigepioBcm::set_pin_dir (int gpio, bool out_flag) noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	_ga.set_fnc (
		gpio,
		(out_flag) ? bcm2837gpio::PinFnc_OUT : bcm2837gpio::PinFnc_IN
	);
}



void	HigepioBcm::set_pull (int gpio, PinPullType pull) noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	bcm2837gpio::Pull p = bcm2837gpio::Pull_NONE;
	if (pull == PinPullType::UP)
	{
		p = bcm2837gpio::Pull_UP;
	}
	else if (pull == PinPullType::DOWN)
	{
		p = bcm2837gpio::Pull_DOWN;
	}
	_ga.pull (gpio, p);
}



void	HigepioBcm::set_pin_mode (int gpio, bcm2837gpio::PinFnc mode) noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);
	assert (mode >= 0);
	assert (mode < bcm2837gpio::PinFnc_NBR_ELT);

	_ga.set_fnc (gpio, mode);
}



int	HigepioBcm::read_pin (int gpio) const noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	return _ga.read (gpio);
}



void	HigepioBcm::write_pin (int gpio, int val) const noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);
	assert (val >= 0);
	assert (val <= 1);

	_ga.write (gpio, val);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_HigepioBcm_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
