/*****************************************************************************

        bcm2837dma.h
        Author: Laurent de Soras, 2019

Register map and bitfields for the PCM/I2S part of the BCM2837 SoC

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_bcm2837dma_HEADER_INCLUDED)
#define mfx_hw_bcm2837dma_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace hw
{
namespace bcm2837dma
{



// DMA 0-14 registers relative to the base address (p. 39)
static constexpr uint32_t  _dma_ofs     = 0x00007000;
static constexpr uint32_t  _dma_len     = 0x1000;
static constexpr uint32_t  _dma_chn_len = 0x24;        // Bytes
static constexpr uint32_t  _dma_chn_inc = 0x100;       // Bytes

static constexpr uint32_t  _dma_chn_15  = 0x00E05000;

// Register map per DMA channel (p. 41)
static constexpr uint32_t  _cs          = 0x00; // Control and Status
static constexpr uint32_t  _conblk_ad   = 0x04; // Control Block Address
static constexpr uint32_t  _ti          = 0x08; // CB Word 0 (Transfer Information)
static constexpr uint32_t  _source_ad   = 0x0C; // CB Word 1 (Source Address)
static constexpr uint32_t  _dest_ad     = 0x10; // CB Word 2 (Destination Address)
static constexpr uint32_t  _txfr_len    = 0x14; // CB Word 3 (Transfer Length)
static constexpr uint32_t  _stride      = 0x18; // CB Word 4 (2D Stride)
static constexpr uint32_t  _nextconbk   = 0x1C; // CB Word 5 (Next CB Address)
static constexpr uint32_t  _debug       = 0x20; // Debug

// Other registers
static constexpr uint32_t  _int_status  = 0xFE0;   // Interrupt status of each DMA channel
static constexpr uint32_t  _enable      = 0xFF0;   // Global enable bits for each DMA channel

// Control and Status (p. 47)
static constexpr uint32_t  _reset       = 1u << 31; // DMA Channel Reset
static constexpr uint32_t  _abort       = 1u << 30; // Abort DMA
static constexpr uint32_t  _disdebug    = 1u << 29; // Disable debug pause signal
static constexpr uint32_t  _waitfow     = 1u << 28; // Wait for outstanding writes
static constexpr int       _panic_prio  =       20; // 4 bits - AXI Panic Priority Level
static constexpr int       _priority    =       16; // 4 bits - AXI Priority Level
static constexpr uint32_t  _error       = 1u <<  8; // DMA Error
static constexpr uint32_t  _waitingfow  = 1u <<  6; // DMA is Waiting for the Last Write to be Received
static constexpr uint32_t  _dreq_stops  = 1u <<  5; // DMA Paused by DREQ State
static constexpr uint32_t  _paused      = 1u <<  4; // DMA Paused State
static constexpr uint32_t  _dreq        = 1u <<  3; // DREQ State
static constexpr uint32_t  _int         = 1u <<  2; // Interrupt Status
static constexpr uint32_t  _end         = 1u <<  1; // DMA End Flag
static constexpr uint32_t  _active      = 1u <<  0; // Activate the DMA

// Transfer Information (p. 51, p. 56)
static constexpr uint32_t  _no_wide_b   = 1u << 26; // Don't Do wide writes as a 2 beat burst
static constexpr int       _wait        =       21; // 5 bits - Add Wait Cycles
static constexpr int       _permap      =       16; // 5 bits - Peripheral Mapping
static constexpr int       _burst_len   =       12; // 4 bits - Burst Transfer Length
static constexpr uint32_t  _src_ignore  = 1u << 11; // Ignore Reads
static constexpr uint32_t  _src_dreq    = 1u << 10; // Control Source Reads with DREQ
static constexpr uint32_t  _src_width   = 1u <<  9; // Source Transfer Width
static constexpr uint32_t  _src_inc     = 1u <<  8; // Source Address Increment
static constexpr uint32_t  _dest_ignore = 1u <<  7; // Ignore Writes
static constexpr uint32_t  _dest_dreq   = 1u <<  6; // Control Destination Writes with DREQ
static constexpr uint32_t  _dest_width  = 1u <<  5; // Destination Transfer Width
static constexpr uint32_t  _dest_inc    = 1u <<  4; // Destination Address Increment
static constexpr uint32_t  _wait_resp   = 1u <<  3; // Wait for a Write Response
static constexpr uint32_t  _tdmode      = 1u <<  1; // 2D Mode
static constexpr uint32_t  _inten       = 1u <<  0; // Interrupt Enable

// Transfer Length (p. 53, p. 57)
static constexpr int       _ylength     =       16; // 14 bits - Y transfer length in 2D mode
static constexpr int       _wlength     =        0; // 16 bits - Transfer Length in bytes

// 2D Stride (p. 54)
static constexpr int       _d_stride    =       16; // 16 bits - Destination Stride (2D Mode)
static constexpr int       _s_stride    =        0; // 16 bits - Source Stride (2D Mode)

// Debug (p. 55, p. 58)
static constexpr uint32_t  _lite        = 1u << 28; // DMA Lite
static constexpr int       _version     =       25; // 3 bits - DMA Version
static constexpr int       _dma_state   =       16; // 9 bits - DMA State Machine State
static constexpr int       _dma_id      =        8; // 8 bits - DMA ID
static constexpr int       _outstnd_wr  =        4; // 4 bits - DMA Outstanding Writes Counter
static constexpr uint32_t  _read_error  = 1u <<  2; // Slave Read Response Error
static constexpr uint32_t  _fifo_error  = 1u <<  1; // Fifo Error
static constexpr uint32_t  _rlns_error  = 1u <<  0; // Read Last Not Set Error
static constexpr uint32_t  _all_errors  = _read_error | _fifo_error | _rlns_error;

// Peripheral DREQ Signals (p. 61)
enum Dreq
{
	Dreq_ON = 0,
	Dreq_DSI,
	Dreq_PCM_TX,
	Dreq_PCM_RX,
	Dreq_SMI,
	Dreq_PWM,
	Dreq_SPI_TX,
	Dreq_SPI_RX,
	Dreq_BSC_SPI_TX,
	Dreq_BSC_SPI_RX,
	Dreq_UNUSED,
	Dreq_EMMC,
	Dreq_UART_TX,
	Dreq_SD_HOST,
	Dreq_UART_RX,
	Dreq_DSI_2,
	Dreq_SLIMBUS_MCTX,
	Dreq_HDMI,
	Dreq_SLIMBUX_MCRX,
	Dreq_SLIMBUX_DC0,
	Dreq_SLIMBUX_DC1,
	Dreq_SLIMBUX_DC2,
	Dreq_SLIMBUX_DC3,
	Dreq_SLIMBUX_DC4,
	Dreq_SCALER_FIFO0_SMI,
	Dreq_SCALER_FIFO1_SMI,
	Dreq_SCALER_FIFO2_SMI,
	Dreq_SLIMBUX_DC5,
	Dreq_SLIMBUX_DC6,
	Dreq_SLIMBUX_DC7,
	Dreq_SLIMBUX_DC8,
	Dreq_SLIMBUX_DC9,

	Dreq_NBR_ELT
};

class CtrlBlock
{
public:
	uint32_t       _info;    // TI: transfer information
	uint32_t       _src;     // SOURCE_AD
	uint32_t       _dst;     // DEST_AD
	uint32_t       _length;  // TXFR_LEN: transfer length
	uint32_t       _stride;  // 2D stride mode. Not used for DMA lite
	uint32_t       _next;    // NEXTCONBK

	static constexpr int _pad_size = 2;
	uint32_t       _pad [_pad_size]; // Reserved
};

class CtrlBlock4
{
public:
	uint32_t       _info;    // TI: transfer information
	uint32_t       _src;     // SRC
	uint32_t       _srci;    // SRCI
	uint32_t       _dst;     // DEST
	uint32_t       _dsti;    // DESTI
	uint32_t       _length;  // LEN: transfer length
	uint32_t       _next;    // NEXT_CB

	static constexpr int _pad_size = 1;
	uint32_t       _pad [_pad_size]; // Reserved
};


}  // namespace bcm2837dma
}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_bcm2837dma_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
