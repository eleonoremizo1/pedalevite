/*****************************************************************************

        HigepioLinux.h
        Author: Laurent de Soras, 2024

Gives basic access to the GPIO pins using the Linux GPIO ABI v2.
Cannot access GPIO pins >= 28, only physical pins.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_HigepioLinux_HEADER_INCLUDED)
#define mfx_hw_HigepioLinux_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/PinPullType.h"

#include <linux/gpio.h>

#include <cstdint>

#include <array>
#include <string>



namespace mfx
{
namespace hw
{



class HigepioLinux
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Number of GPIOs on the 40-pin connector. Actually the highest index + 1
	static constexpr int   _nbr_gpio = 28;

	explicit       HigepioLinux (uint64_t exclusion_mask);
	               HigepioLinux (HigepioLinux &&other) = default;
	               ~HigepioLinux ();

	HigepioLinux & operator = (HigepioLinux &&other)   = default;

	void           set_pin_dir (int gpio, bool out_flag) noexcept;
	void           set_pull (int gpio, PinPullType pull) noexcept;
	int            read_pin (int gpio) const noexcept;
	void           write_pin (int gpio, int val) const noexcept;

	void           sleep (uint32_t dur_us) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class Line
	{
	public:
		inline bool    is_dir_set (bool out_flag) const noexcept;

		uint64_t       _flags = 0;
		int            _fd    = 0;
	};

	typedef std::array <Line, _nbr_gpio> LineArray;

	void           cleanup () noexcept;
	template <typename F>
	void           modify_flags (int gpio, F f) noexcept;

	static std::string
	               find_gpio_chip ();

	std::string    _chip;
	int            _fd = -1;
	LineArray      _line_arr { };



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               HigepioLinux ()                               = delete;
	               HigepioLinux (const HigepioLinux &other)      = delete;
	HigepioLinux & operator = (const HigepioLinux &other)        = delete;
	bool           operator == (const HigepioLinux &other) const = delete;
	bool           operator != (const HigepioLinux &other) const = delete;

}; // class HigepioLinux



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/HigepioLinux.hpp"



#endif   // mfx_hw_HigepioLinux_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
