/*****************************************************************************

        SpiAccessGpio.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/SpiAccessGpio.h"

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



SpiAccessGpio::SpiAccessGpio (Higepio &io, int gpio_idx, int speed)
:	_io (io)
,	_gpio_idx (gpio_idx)
,	_speed (speed)
{
	assert (gpio_idx >= 0);
	assert (gpio_idx < Higepio::_nbr_gpio);
	assert (speed >= 0);

	_io.set_pin_dir (_gpio_idx, true);
	_io.write_pin (_gpio_idx, 1);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	SpiAccessGpio::do_get_chn () const noexcept
{
	// The returned value is not important, as we don't use the standard
	// CS* pins to select the SPI device.
	return 0;
}



int	SpiAccessGpio::do_get_speed () const noexcept
{
	return _speed;
}



void	SpiAccessGpio::do_start () const noexcept
{
	_io.write_pin (_gpio_idx, 0);
}



void	SpiAccessGpio::do_terminate () const noexcept
{
	_io.write_pin (_gpio_idx, 1);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
