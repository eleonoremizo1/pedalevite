/*****************************************************************************

        MmapPtr.hpp
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_hw_MmapPtr_CODEHEADER_INCLUDED)
#define mfx_hw_MmapPtr_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



volatile uint32_t *	MmapPtr::get () const
{
	return _ptr;
}



volatile uint32_t *	MmapPtr::operator -> () const
{
	return _ptr;
}



volatile uint32_t &	MmapPtr::operator * () const
{
	return *_ptr;
}



const volatile uint32_t &	MmapPtr::at (uint32_t ofs_byte) const
{
	assert ((ofs_byte & 3) == 0);
	assert (ofs_byte < _len);

	return _ptr [ofs_byte >> 2];
}



volatile uint32_t &	MmapPtr::at (uint32_t ofs_byte)
{
	assert ((ofs_byte & 3) == 0);
	assert (ofs_byte < _len);

	return _ptr [ofs_byte >> 2];
}



// Not atomic
void	MmapPtr::set (uint32_t ofs_byte, uint32_t bitmask)
{
	at (ofs_byte) = at (ofs_byte) | bitmask;
}



// Not atomic
void	MmapPtr::clr (uint32_t ofs_byte, uint32_t bitmask)
{
	at (ofs_byte) = at (ofs_byte) & ~bitmask;
}



// Bitfield operation: field is delimited by the non-null mask bits.
// val is the new content of the field (all bits covered by the mask).
// mask and val may be lsb-based and shifted in final position with shift.
// Not atomic
void	MmapPtr::set_field (uint32_t ofs_byte, uint32_t mask, uint32_t val, uint32_t shift)
{
	assert ((mask & val) == val);
	assert (shift < 32);
	assert (((mask << shift) >> shift) == mask);

	mask <<= shift;
	val  <<= shift;
	at (ofs_byte) = (at (ofs_byte) & ~mask) | val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_MmapPtr_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
