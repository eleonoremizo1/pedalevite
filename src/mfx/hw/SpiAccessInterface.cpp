/*****************************************************************************

        SpiAccessInterface.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/SpiAccessInterface.h"

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: get_chn
Description:
	Returns the standard SPI channel for the device.
Returns: the channel, 0 or 1
==============================================================================
*/

int	SpiAccessInterface::get_chn () const noexcept
{
	const auto     chn = do_get_chn ();
	assert (chn >= 0);
	assert (chn < 2);

	return chn;
}



/*
==============================================================================
Name: get_speed
Description:
	Returns the maximum communication speed for the device.
Returns: maximum speed in bits/s, > 0.
==============================================================================
*/

int	SpiAccessInterface::get_speed () const noexcept
{
	const auto     speed = do_get_speed ();
	assert (speed > 0);

	return speed;
}



/*
==============================================================================
Name: start
Description:
	This function is called before a SPI communication.
==============================================================================
*/

void	SpiAccessInterface::start () const noexcept
{
	do_start ();
}



/*
==============================================================================
Name: terminate
Description:
	This function is called after a SPI communication.
==============================================================================
*/

void	SpiAccessInterface::terminate () const noexcept
{
	do_terminate ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
