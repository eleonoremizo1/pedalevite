/*****************************************************************************

        rp1i2s.h
        Author: Laurent de Soras, 2024

Sources:

- Raspberry Pi RP1 Peripherals, draft 2023-11-07
https://datasheets.raspberrypi.com/rp1/rp1-peripherals.pdf

- Synopsys I2S Linux kernel driver
https://github.com/raspberrypi/linux/blob/rpi-6.1.y/sound/soc/dwc/dwc-i2s.c

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_rp1i2s_HEADER_INCLUDED)
#define mfx_hw_rp1i2s_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace hw
{
namespace rp1i2s
{



// Base address for the I2S blocks, relative to the RP1 address space
static constexpr uint32_t _i2s_ofs     = 0xA0000;

// Length of each I2S0, I2S1 and I2S2 block
static constexpr uint32_t _i2s_blk_len = 0x4000;

// Length of each channel pair block
static constexpr uint32_t _chn_blk_len = 0x40;

static constexpr uint32_t _i2s0_ofs    = _i2s_ofs; // Clock master
static constexpr uint32_t _i2s1_ofs    = _i2s_ofs + _i2s_blk_len * 1; // Slave
static constexpr uint32_t _i2s2_ofs    = _i2s_ofs + _i2s_blk_len * 2; // Slave

// Depth for both RX and TX data FIFOs. In samples or frames?
static constexpr int      _fifo_depth    = 16;
static constexpr int      _fifo_depth_rx = _fifo_depth;
static constexpr int      _fifo_depth_tx = _fifo_depth;

// Maximum wordsize, in bits
static constexpr int      _wordsize_rx   = 32;
static constexpr int      _wordsize_tx   = 32;

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
// Global

// Registers. Offsets in bytes, relative to _i2s?_ofs
static constexpr uint32_t _ier           = 0x000;
static constexpr uint32_t _irer          = 0x004;
static constexpr uint32_t _iter          = 0x008;
static constexpr uint32_t _cer           = 0x00C;
static constexpr uint32_t _ccr           = 0x010;
static constexpr uint32_t _rxffr         = 0x014;
static constexpr uint32_t _txffr         = 0x018;
static constexpr uint32_t _rxdma         = 0x1C0;
static constexpr uint32_t _txdma         = 0x1C8;
static constexpr uint32_t _comp_param_2  = 0x1F0;
static constexpr uint32_t _comp_param_1  = 0x1F4;
static constexpr uint32_t _comp_version  = 0x1F8;
static constexpr uint32_t _comp_type     = 0x1FC;
static constexpr uint32_t _dmacr         = 0x200;
static constexpr uint32_t _rslot_tslot   = 0x224;

// Bit fields
static constexpr uint32_t _ier_dev_en    = 1 << 0; // Device enable
static constexpr uint32_t _irer_int_en   = 1 << 0; // Enable interrupts for RX
static constexpr uint32_t _iter_int_en   = 1 << 0; // Enable interrupts for TX
static constexpr uint32_t _cer_trans     = 1 << 0; // Start/stop transfers

// Sample resolution, 2 bits
static constexpr int      _ccr_res       = 3;      // Bit offset
static constexpr uint32_t _ccr_res_mask  = ((1 << 2) - 1) << _ccr_res;
static constexpr uint32_t _ccr_res_16    = 0 << _ccr_res; // 16 bits
static constexpr uint32_t _ccr_res_24    = 1 << _ccr_res; // 24 bits
static constexpr uint32_t _ccr_res_32    = 2 << _ccr_res; // 32 bits

static constexpr uint32_t _rxffr_en      = 1 << 0; // Enable RX
static constexpr uint32_t _txffr_en      = 1 << 0; // Enable TX

static constexpr uint32_t _cp1_rx_en     = 1 << 6; // Set if the peripheral is able to capture
static constexpr uint32_t _cp1_tx_en     = 1 << 5; // Set if the peripheral is able to receive
static constexpr uint32_t _cp1_mode_en   = 1 << 4; // 1 = peripheral is clock master, 0 = clock slave
static constexpr uint32_t _cp1_fifo_dep       = 2; // FIFO_DEPTH_GLOBAL
static constexpr uint32_t _cp1_fifo_dep_mask  = ((1 << 2) - 1) << _cp1_fifo_dep;
static constexpr uint32_t _cp1_fifo_dep_2     = 0 << _cp1_fifo_dep;
static constexpr uint32_t _cp1_fifo_dep_4     = 1 << _cp1_fifo_dep;
static constexpr uint32_t _cp1_fifo_dep_8     = 2 << _cp1_fifo_dep;
static constexpr uint32_t _cp1_fifo_dep_16    = 3 << _cp1_fifo_dep;
static constexpr uint32_t _cp1_apb_width      = 0; // APB_DATA_WIDTH, bus width in bits for the DMA (TXDMA/RXDMA)
static constexpr uint32_t _cp1_apb_width_mask = ((1 << 2) - 1) << _cp1_apb_width;
static constexpr uint32_t _cp1_apb_width_8    = 0 << _cp1_apb_width;
static constexpr uint32_t _cp1_apb_width_16   = 1 << _cp1_apb_width;
static constexpr uint32_t _cp1_apb_width_32   = 2 << _cp1_apb_width;

static constexpr uint32_t _dmacr_tx    = 1 << 17; // Enables TX DMA transfers on selected channel pairs
static constexpr uint32_t _dmacr_rx    = 1 << 16; // Enables RX DMA transfers on selected channel pairs
static constexpr uint32_t _dmacr_txch3 = 1 << 11; // Enables DMA transfer of a given TX channel pair
static constexpr uint32_t _dmacr_txch2 = 1 << 10;
static constexpr uint32_t _dmacr_txch1 = 1 << 9;
static constexpr uint32_t _dmacr_txch0 = 1 << 8;
static constexpr uint32_t _dmacr_rxch3 = 1 << 3;  // Enables DMA transfer of a given RX channel pair
static constexpr uint32_t _dmacr_rxch2 = 1 << 2;
static constexpr uint32_t _dmacr_rxch1 = 1 << 1;
static constexpr uint32_t _dmacr_rxch0 = 1 << 0;

// TX_WORDSIZE and RX_WORDSIZE fields for each channel *pair*
// Gives the maximum bit resolution achievable on the peripheral
static constexpr uint32_t _wordsize_mask = (1 << 3) - 1; // Unshifted
static constexpr uint32_t _wordsize_12   = 0; // Bitdepth
static constexpr uint32_t _wordsize_16   = 1;
static constexpr uint32_t _wordsize_20   = 2;
static constexpr uint32_t _wordsize_24   = 3;
static constexpr uint32_t _wordsize_32   = 4;

// TX_CHANNELS and RX_CHANNELS fields
// Gives the maximum number of channels that the peripheral can handle
static constexpr uint32_t _channels_mask = (1 << 2) - 1; // Unshifted
static constexpr uint32_t _channels_2    = 0;
static constexpr uint32_t _channels_4    = 1;
static constexpr uint32_t _channels_8    = 2; // Unused on RP1

// Bit positions in COMP_PARAM_1
static constexpr int      _cp1_tx_wordsize_0 = 16;
static constexpr int      _cp1_tx_wordsize_1 = 19; // Only for I2S0 and I2S1
static constexpr int      _cp1_tx_wordsize_2 = 22; // Unused on RP1
static constexpr int      _cp1_tx_wordsize_3 = 25; // Unused on RP1
static constexpr int      _cp1_tx_channels   = 9;
static constexpr int      _cp1_rx_channels   = 7;

// Bit positions in COMP_PARAM_2
static constexpr int      _cp2_rx_wordsize_0 =  0;
static constexpr int      _cp2_rx_wordsize_1 =  3; // Only for I2S0 and I2S1
static constexpr int      _cp2_rx_wordsize_2 =  7; // Unused on RP1
static constexpr int      _cp2_rx_wordsize_3 = 10; // Unused on RP1

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
// Channel pair registers

// Registers. Offsets in bytes, relative to _i2s?_ofs + N * _chn_blk_len
static constexpr uint32_t _lrbr_lthr = 0x20; // R/W FIFOs for the left channel
static constexpr uint32_t _rrbr_rthr = 0x24; // R/W FIFOs for the right channel
static constexpr uint32_t _rer       = 0x28;
static constexpr uint32_t _ter       = 0x2C;
static constexpr uint32_t _rcr       = 0x30;
static constexpr uint32_t _tcr       = 0x34;
static constexpr uint32_t _isr       = 0x38; // Interrupt status register
static constexpr uint32_t _imr       = 0x3C; // Interrupt mask register
static constexpr uint32_t _ror       = 0x40; // Reading this register acknowledges the RX interrupt
static constexpr uint32_t _tor       = 0x44; // Reading this register acknowledges the TX interrupt
static constexpr uint32_t _rfcr      = 0x48; // Threshold for TXFE on the RX FIFO
static constexpr uint32_t _tfcr      = 0x4C; // Threshold for RXDA on the TX FIFO
static constexpr uint32_t _rff       = 0x50;
static constexpr uint32_t _tff       = 0x54;

// Bit fields
static constexpr uint32_t _xer_en      = 1 << 0; // Enable channel pair for RX or TX on RER or TER
static constexpr int      _xcr_ws      = 0; // Bit offset for the TX or RX wordsize in bits. 3 bits
static constexpr uint32_t _xcr_ws_mask = ((1 << 3) - 1) << _xcr_ws;
static constexpr uint32_t _xcr_ws_16   = 2 << _xcr_ws;
static constexpr uint32_t _xcr_ws_24   = 4 << _xcr_ws;
static constexpr uint32_t _xcr_ws_32   = 5 << _xcr_ws;
static constexpr uint32_t _isr_txfo    = 1 << 5; // TX FIFO overrun (client failed to provide enough data?)
static constexpr uint32_t _isr_txfe    = 1 << 4; // TX FIFO is empty. Only for the first pair of channels.
static constexpr uint32_t _isr_rxfo    = 1 << 1; // RX FIFO overrun (client failed to retrieve data?)
static constexpr uint32_t _isr_rxda    = 1 << 0; // RX FIFO is not empty (data is available). Only for the first pair of channels.
static constexpr int      _imr_tx      = 4; // Bit offset for the TX interrupt mask, 2 bits
static constexpr uint32_t _imr_tx_mask = ((1 << 2) - 1) << _imr_tx;
static constexpr uint32_t _imr_tx_ei   = 0 << _imr_tx; // Enable interrupts (which events?)
static constexpr uint32_t _imr_tx_di   = 3 << _imr_tx; // Disable interrupts
static constexpr int      _imr_rx      = 0; // Bit offset for the RX interrupt mask, 2 bits
static constexpr uint32_t _imr_rx_mask = ((1 << 2) - 1) << _imr_rx;
static constexpr uint32_t _imr_rx_ei   = 0 << _imr_rx; // Enable interrupts (which events?)
static constexpr uint32_t _imr_rx_di   = 3 << _imr_rx; // Disable interrupts



}  // namespace rp1i2s
}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_rp1i2s_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
