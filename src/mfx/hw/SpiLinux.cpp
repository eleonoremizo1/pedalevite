/*****************************************************************************

        SpiLinux.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/hw/SpiLinux.h"

#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdexcept>
#include <system_error>
#include <utility>

#include <cassert>
#include <climits>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



SpiLinux::SpiLinux (std::unique_ptr <SpiAccessInterface> access_uptr, const char *err_0)
:	_speed (access_uptr->get_speed ())
,	_access_uptr (std::move (access_uptr))
{
	assert (_access_uptr.get () != nullptr);
	assert (_speed >= _speed_min);
	assert (_speed <= _speed_max);

	const auto       chn = _access_uptr->get_chn ();
	assert (chn >= 0);
	assert (chn < 2);

	char           dev_0 [99+1];
	fstb::snprintf4all (dev_0, sizeof (dev_0), "/dev/spidev0.%d", chn);
	_hnd = open (dev_0, O_RDWR);
	if (_hnd < 0)
	{
		if (err_0 == nullptr)
		{
			err_0 = "Cannot open SPI port";
		}
		throw std::system_error (
			std::error_code (errno, std::system_category ()), err_0
		);
	}

	try
	{
		int            mode = 0;
		if (ioctl (_hnd, SPI_IOC_WR_MODE, &mode) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot set SPI mode"
			);
		}

		uint8_t        bits_per_word = CHAR_BIT;
		if (ioctl (_hnd, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot set SPI word size"
			);
		}

		if (ioctl (_hnd, SPI_IOC_WR_MAX_SPEED_HZ, &_speed) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot set SPI maximum speed"
			);
		}
	}

	catch (...)
	{
		clean_up ();
		throw;
	}
}



SpiLinux::~SpiLinux ()
{
	clean_up ();
}



// Returns 0 on success, negative on failure
int   SpiLinux::rw_data (uint8_t *data_ptr, int len) const noexcept
{
	return rw_data (data_ptr, data_ptr, len);
}



// Returns 0 on success, negative on failure
int	SpiLinux::rw_data (uint8_t *dst_ptr, const uint8_t *src_ptr, int len) const noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (len > 0);

	int               ret_val = 0;

	_access_uptr->start ();

	spi_ioc_transfer  msg {}; // Defaults everything to 0
	msg.tx_buf        = reinterpret_cast <intptr_t> (src_ptr);
	msg.rx_buf        = reinterpret_cast <intptr_t> (dst_ptr);
	msg.len           = len;
	msg.speed_hz      = _speed;
	msg.delay_usecs   = 0;
	msg.bits_per_word = CHAR_BIT;
	msg.cs_change     = 0;

	if (ioctl (_hnd, SPI_IOC_MESSAGE (1), &msg) < 0)
	{
		ret_val = -1;
	}

	_access_uptr->terminate ();

	return ret_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void   SpiLinux::clean_up () noexcept
{
	if (_hnd >= 0)
	{
		close (_hnd);
		_hnd = -1;
	}
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
