/*****************************************************************************

        DisplayLinuxFrameBuf.cpp
        Author: Laurent de Soras, 2019

Ref:
https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/fb.h
https://www.kernel.org/doc/html/latest/fb/api.html

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/hw/DisplayLinuxFrameBuf.h"

#if fstb_ARCHI == fstb_ARCHI_X86
	#include <emmintrin.h>
#elif fstb_ARCHI == fstb_ARCHI_ARM
	#include <arm_neon.h>
#else
	#error architecture not defined
#endif

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/kd.h>
#include <fcntl.h>
#include <unistd.h>

#include <algorithm>
#include <stdexcept>
#include <system_error>

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



DisplayLinuxFrameBuf::DisplayLinuxFrameBuf (std::string dev_name)
:	_dev_name (dev_name)
,	_tty_fd (open ("/dev/tty0", O_RDWR))
,	_fb_fd (open (dev_name.c_str (), O_RDWR))
,	_info_fix ()
,	_info_var ()
,	_mm_fb_ptr (nullptr)
,	_map_len (0)
,	_pix_fb_ptr (nullptr)
,	_stride_s (0)
,	_disp_w (0)
,	_disp_h (0)
,	_fb_int ()
,	_stride_i (0)
{
	assert (! dev_name.empty ());

	try
	{
		if (_tty_fd == -1)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot open tty"
			);
		}

		if (_fb_fd == -1)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot open the framebuffer"
			);
		}

		if (ioctl (_tty_fd, KDSETMODE, KD_GRAPHICS) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot set tty to graphic mode"
			);
		}

		if (ioctl (_fb_fd, FBIOGET_FSCREENINFO, &_info_fix) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot get screen info (fixed)"
			);
		}

		if (ioctl (_fb_fd, FBIOGET_VSCREENINFO, &_info_var) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot get screen info (variable)"
			);
		}

		_map_len   = _info_fix.smem_len;
		_mm_fb_ptr = reinterpret_cast <uint8_t *> (mmap (
			nullptr, _map_len, PROT_READ | PROT_WRITE, MAP_SHARED, _fb_fd, 0
		));
		if (_mm_fb_ptr == MAP_FAILED)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot mmap the frame buffer"
			);
		}

		// We don't need the file descriptor anymore.
		close (_fb_fd);
		_fb_fd = -1;

		// Checks the pixel format
		if (_info_fix.type != FB_TYPE_PACKED_PIXELS)
		{
			throw std::runtime_error ("Only packed pixels are supported.");
		}

		if (   _info_fix.visual != FB_VISUAL_TRUECOLOR
		    && _info_fix.visual != FB_VISUAL_DIRECTCOLOR)
		{
			throw std::runtime_error ("Unsupported pixel encoding.");
		}

		if (_info_var.nonstd != 0)
		{
			throw std::runtime_error ("Non standard pixel format.");
		}

		if (   ! is_supported_argb32 (_info_var)
		    && ! is_supported_rgb16 (_info_var))
		{
			throw std::runtime_error ("Unsupported pixel arrangement.");
		}

		if ((_rot & 3) != 0 && _info_var.bits_per_pixel == 32)
		{
			throw std::runtime_error (
				"Display rotation not supported for 32-bit ARGB pixel format."
			);
		}

		_bypp = _info_var.bits_per_pixel >> 3;

		// Gets the resolution information
		_disp_w     = _info_var.xres;
		_disp_h     = _info_var.yres;
		_stride_s   = _info_fix.line_length;
		_ss_pix     = _stride_s / _bypp;

		if (_ss_pix * _bypp != _stride_s)
		{
			throw std::runtime_error (
				"Row stride is not an integer amount of pixels."
			);
		}

		_pix_fb_ptr =
			  _mm_fb_ptr
			+ _info_var.yoffset * _stride_s
			+ _info_var.xoffset * _bypp;

		if constexpr ((_rot & 1) != 0)
		{
			std::swap (_disp_w, _disp_h);
		}

		// Allocates the internal framebuffer
		constexpr auto align_byte = 16;
		constexpr auto align_pix  = align_byte / sizeof (ui::PixArgb);
		static_assert (
			align_pix * sizeof (ui::PixArgb) == align_byte,
			"A source pixel should be coded with a power-of-2 number of bytes"
		);
		constexpr auto amask      = align_pix - 1;
		_stride_i = (_disp_w + amask) & ~amask;
		_fb_int.resize (_stride_i * _disp_h, ui::PixArgb { 0, 0, 0, 255 });
	}

	catch (...)
	{
		clean_up ();
		throw;
	}
}



DisplayLinuxFrameBuf::~DisplayLinuxFrameBuf ()
{
	clean_up ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	DisplayLinuxFrameBuf::do_get_width () const noexcept
{
	return _disp_w;
}



int	DisplayLinuxFrameBuf::do_get_height () const noexcept
{
	return _disp_h;
}



int	DisplayLinuxFrameBuf::do_get_stride () const noexcept
{
	return _stride_i;
}



ui::PixArgb *	DisplayLinuxFrameBuf::do_use_screen_buf () noexcept
{
	return _fb_int.data ();
}



const ui::PixArgb *	DisplayLinuxFrameBuf::do_use_screen_buf () const noexcept
{
	return _fb_int.data ();
}



void	DisplayLinuxFrameBuf::do_refresh (int x, int y, int w, int h)
{
	x = std::max (x, 0);
	y = std::max (y, 0);
	w = std::min (w, _disp_w - x);
	h = std::min (h, _disp_h - y);

	if (w > 0 && h > 0)
	{
		if (_bypp == 2)
		{
			refresh_rgb16 (x, y, w, h);
		}
		else
		{
			refresh_argb32 (x, y, w, h);
		}
	}
}



void	DisplayLinuxFrameBuf::do_force_reset ()
{
	// Nothing
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DisplayLinuxFrameBuf::clean_up ()
{
	if (_mm_fb_ptr != MAP_FAILED && _mm_fb_ptr != nullptr)
	{
		munmap (_mm_fb_ptr, _map_len);
		_mm_fb_ptr = nullptr;
	}

	if (_fb_fd != -1)
	{
		close (_fb_fd);
		_fb_fd = -1;
	}

	if (_tty_fd != -1)
	{
		// Don't bother to check the result. Text mode will be restored upon
		// program termination anyway.
		ioctl (_tty_fd, KDSETMODE, KD_TEXT);

		close (_tty_fd);
		_tty_fd = -1;
	}
}



void	DisplayLinuxFrameBuf::refresh_argb32 (int x, int y, int w, int h) noexcept
{
	const ui::PixArgb * fstb_RESTRICT   src_ptr =
		_fb_int.data () + y * _stride_i + x;
	ui::PixArgb *  dst_ptr =
		  reinterpret_cast <ui::PixArgb *> (_pix_fb_ptr)
		+ y * _ss_pix
		+ x;

	for (int py = 0; py < h; ++py)
	{
		std::copy (src_ptr, src_ptr + w, dst_ptr);
		src_ptr += _stride_i;
		dst_ptr += _stride_s;
	}
}



void	DisplayLinuxFrameBuf::refresh_rgb16 (int x, int y, int w, int h) noexcept
{
	const ui::PixArgb * fstb_RESTRICT src_ptr =
		_fb_int.data () + y * _stride_i + x;

	constexpr int  inv   =     ( _rot      & 2) >> 1;
	constexpr int  inv2  =     ((_rot + 1) & 2) >> 1;
	constexpr int  dir   = 1 - ( _rot      & 2);

	const int      x_ofs = inv2 * (_disp_w - 1);
	const int      y_ofs = inv  * (_disp_h - 1);

	auto           encode_pix = [] (const ui::PixArgb &p) noexcept
	{
		const auto     p_b  = p._b & 0xF8;
		const auto     p_g  = p._g & 0xFC;
		const auto     p_r  = p._r & 0xF8;
		constexpr auto sh_r =   11 - (8-5);
		constexpr auto sh_g =    5 - (8-6);
		constexpr auto sh_b = -( 0 - (8-5));
		const auto     rgb  = (p_r << sh_r) | (p_g << sh_g) | (p_b >> sh_b);
		return uint16_t (rgb);
	};

	// 0 or 180 deg
	if constexpr ((_rot & 1) == 0)
	{
		uint16_t *     dst_ptr =
			  reinterpret_cast <uint16_t *> (_pix_fb_ptr)
			+ (y_ofs + y * dir) * _ss_pix
			+  x_ofs + x * dir;

		for (int py = 0; py < h; ++py)
		{
			for (int px = 0; px < w; ++px)
			{
				const auto &   v = src_ptr [px];
				dst_ptr [sgn <inv> (px)] = encode_pix (v);
			}

			src_ptr += _stride_i;
			add <inv> (dst_ptr, _ss_pix);
		}
	}

	// +/-90 deg (with transposition)
	else
	{
		uint16_t *     dst_ptr =
			  reinterpret_cast <uint16_t *> (_pix_fb_ptr)
			+ (x_ofs + x * -dir) * _ss_pix
			+  y_ofs + y *  dir;

		for (int py = 0; py < h; ++py)
		{
			auto           d2_ptr = dst_ptr;
			for (int px = 0; px < w; ++px)
			{
				const auto &   v = src_ptr [px];
				*d2_ptr = encode_pix (v);
				add <inv2> (d2_ptr, _ss_pix);
			}

			src_ptr += _stride_i;
			add <inv> (dst_ptr, 1);
		}
	}
}



bool	DisplayLinuxFrameBuf::is_supported_argb32 (const ::fb_var_screeninfo &info_var) noexcept
{
	return (
		   info_var.bits_per_pixel  == 32
		&& info_var.red.length      == 8
		&& info_var.red.offset      == 16
		&& info_var.red.msb_right   == 0
		&& info_var.green.length    == 8
		&& info_var.green.offset    == 8
		&& info_var.green.msb_right == 0
		&& info_var.blue.length     == 8
		&& info_var.blue.offset     == 0
		&& info_var.blue.msb_right  == 0
	);
}



bool	DisplayLinuxFrameBuf::is_supported_rgb16 (const ::fb_var_screeninfo &info_var) noexcept
{
	return (
		   info_var.bits_per_pixel  == 16
		&& info_var.red.length      == 5
		&& info_var.red.offset      == 11
		&& info_var.red.msb_right   == 0
		&& info_var.green.length    == 6
		&& info_var.green.offset    == 5
		&& info_var.green.msb_right == 0
		&& info_var.blue.length     == 5
		&& info_var.blue.offset     == 0
		&& info_var.blue.msb_right  == 0
	);
}



template <int INV_FLAG, typename T>
T	DisplayLinuxFrameBuf::sgn (T a) noexcept
{
	if constexpr (INV_FLAG != 0)
	{
		return -a;
	}
	else
	{
		return a;
	}
}



template <int INV_FLAG, typename T, typename U>
void	DisplayLinuxFrameBuf::add (T &a, U b) noexcept
{
	if constexpr (INV_FLAG != 0)
	{
		a -= b;
	}
	else
	{
		a += b;
	}
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
