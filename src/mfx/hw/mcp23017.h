/*****************************************************************************

        mcp23017.h
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_mcp23017_mcp23017_HEADER_INCLUDED)
#define mfx_hw_mcp23017_mcp23017_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace hw
{
namespace mcp23017
{


// MCP23017 registers
// IOCON.BANK = 0
static constexpr int cmd_iodira   = 0x00;
static constexpr int cmd_iodirb   = 0x01;
static constexpr int cmd_ipola    = 0x02;
static constexpr int cmd_ipolb    = 0x03;
static constexpr int cmd_gpintena = 0x04;
static constexpr int cmd_gpintenb = 0x05;
static constexpr int cmd_defvala  = 0x06;
static constexpr int cmd_defvalb  = 0x07;
static constexpr int cmd_intcona  = 0x08;
static constexpr int cmd_intconb  = 0x09;
static constexpr int cmd_iocona   = 0x0A;
static constexpr int cmd_ioconb   = 0x0B;
static constexpr int cmd_gppua    = 0x0C;
static constexpr int cmd_gppub    = 0x0D;
static constexpr int cmd_intfa    = 0x0E;
static constexpr int cmd_intfb    = 0x0F;
static constexpr int cmd_intcapa  = 0x10;
static constexpr int cmd_intcapb  = 0x11;
static constexpr int cmd_gpioa    = 0x12;
static constexpr int cmd_gpiob    = 0x13;
static constexpr int cmd_olata    = 0x14;
static constexpr int cmd_olatb    = 0x15;

static constexpr int iocon_bank   = 0x80;
static constexpr int iocon_mirror = 0x40;
static constexpr int iocon_seqop  = 0x20;
static constexpr int iocon_disslw = 0x10;
static constexpr int iocon_haen   = 0x08;
static constexpr int iocon_odr    = 0x04;
static constexpr int iocon_intpol = 0x02;



}  // namespace mcp23017
}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/mcp23017/mcp23017.hpp"



#endif   // mfx_hw_mcp23017_mcp23017_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
