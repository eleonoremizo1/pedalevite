/*****************************************************************************

        I2cLinux.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_I2cLinux_HEADER_INCLUDED)
#define mfx_hw_I2cLinux_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace hw
{



class I2cLinux
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       I2cLinux (int dev_id, const char *err_0 = nullptr);
	               I2cLinux (I2cLinux &&other)   = default;
	               ~I2cLinux ();

	I2cLinux &     operator = (I2cLinux &&other) = default;

	uint8_t        read_reg_8 (int reg) const noexcept;
	uint16_t       read_reg_16 (int reg) const noexcept;
	void           write_reg_8 (int reg, uint8_t val) const noexcept;
	void           write_reg_16 (int reg, uint16_t val) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// ioctl commands for I2C/smbus
	static constexpr int _ioctl_i2c_slave = 0x0703;
	static constexpr int _ioctl_i2c_smbus = 0x0720;

	// smbus command arguments
	static constexpr uint8_t   _dir_w     = 0;
	static constexpr uint8_t   _dir_r     = 1;
	static constexpr uint8_t   _byte_data = 2;
	static constexpr uint8_t   _word_data = 3;

	union SmbusData
	{
		uint8_t        _u8;
		uint16_t       _u16;
	};

	class SmbusIoctl
	{
	public:
		uint8_t        _rw;
		uint8_t        _cmd;
		uint32_t       _size;
		SmbusData *    _data_ptr;
	};

	void           clean_up () noexcept;
	inline int     access_smbus (int dir, int cmd, int len, SmbusData &data) const noexcept;

	int            _hnd   = 0; // File handle, > 0



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               I2cLinux ()                               = delete;
	               I2cLinux (const I2cLinux &other)          = delete;
	I2cLinux &     operator = (const I2cLinux &other)        = delete;
	bool           operator == (const I2cLinux &other) const = delete;
	bool           operator != (const I2cLinux &other) const = delete;

}; // class I2cLinux



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/I2cLinux.hpp"



#endif   // mfx_hw_I2cLinux_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
