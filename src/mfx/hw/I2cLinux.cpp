/*****************************************************************************

        I2cLinux.cpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/hw/I2cLinux.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdexcept>
#include <system_error>

#include <cassert>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



I2cLinux::I2cLinux (int dev_id, const char *err_0)
:	_hnd (open ("/dev/i2c-1", O_RDWR))
{
	assert (dev_id >= 0);
	assert (dev_id <= 0x7F);

	if (_hnd < 0)
	{
		if (err_0 == nullptr)
		{
			err_0 = "Cannot open I2C port";
		}
		throw std::system_error (
			std::error_code (errno, std::system_category ()), err_0
		);
	}

	try
	{
		if (ioctl (_hnd, _ioctl_i2c_slave, dev_id) < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot select I2C device"
			);
		}
	}
	catch (...)
	{
		clean_up ();
		throw;
	}
}



I2cLinux::~I2cLinux ()
{
	clean_up ();
}



uint8_t  I2cLinux::read_reg_8 (int reg) const noexcept
{
	assert (reg >= 0);
	assert (reg <= 0xFF);

	SmbusData      smbd;
	smbd._u8 = 0;
	const int      ret_val = access_smbus (_dir_r, reg, _byte_data, smbd);
	fstb::unused (ret_val);
	assert (ret_val == 0);

	return smbd._u8;
}



uint16_t I2cLinux::read_reg_16 (int reg) const noexcept
{
	assert (reg >= 0);
	assert (reg <= 0xFF);

	SmbusData      smbd;
	smbd._u16 = 0;
	const int      ret_val = access_smbus (_dir_r, reg, _word_data, smbd);
	fstb::unused (ret_val);
	assert (ret_val == 0);

	return smbd._u16;
}



void  I2cLinux::write_reg_8 (int reg, uint8_t val) const noexcept
{
	assert (reg >= 0);
	assert (reg <= 0xFF);

	SmbusData      smbd;
	smbd._u8 = val;
	const int      ret_val = access_smbus (_dir_w, reg, _byte_data, smbd);
	fstb::unused (ret_val);
	assert (ret_val == 0);
}



void  I2cLinux::write_reg_16 (int reg, uint16_t val) const noexcept
{
	assert (reg >= 0);
	assert (reg <= 0xFF);

	SmbusData      smbd;
	smbd._u16 = val;
	const int      ret_val = access_smbus (_dir_w, reg, _word_data, smbd);
	fstb::unused (ret_val);
	assert (ret_val == 0);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	I2cLinux::clean_up () noexcept
{
	if (_hnd >= 0)
	{
		close (_hnd);
		_hnd = -1;
	}
}



int	I2cLinux::access_smbus (int dir, int cmd, int len, SmbusData &data) const noexcept
{
	SmbusIoctl arg { uint8_t (dir), uint8_t (cmd), uint32_t (len), &data };

	return ioctl (_hnd, _ioctl_i2c_smbus, &arg);
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
