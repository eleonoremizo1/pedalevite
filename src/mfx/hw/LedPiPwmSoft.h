/*****************************************************************************

        LedPiPwmSoft.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_LedPiPwmSoft_HEADER_INCLUDED)
#define mfx_hw_LedPiPwmSoft_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/Higepio.h"
#include "mfx/ui/LedInterface.h"
#include "mfx/Cst.h"
#include "mfx/PwmMgr.h"

#include <atomic>
#include <thread>



namespace mfx
{
namespace hw
{



class LedPiPwmSoft final
:	public ui::LedInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       LedPiPwmSoft (Higepio &io);
	virtual        ~LedPiPwmSoft ();

	static constexpr int _nbr_led = Cst::_nbr_leds;
	static const int     _gpio_pin_arr [_nbr_led];



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// LedInterface
	int            do_get_nbr_led () const final;
	void           do_set_led (int index, float val) final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class LedState
	{
	public:
		volatile float _val_cur  = 0;
		float          _val_prev = 0;
	};

	static constexpr int _pwm_cycle = 10'000; // Microseconds

	void           refresh_loop ();

	Higepio &      _io;
	PwmMgr         _pwm_mgr;

	std::atomic <bool>
	               _quit_flag;
	std::thread    _refresher;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               LedPiPwmSoft ()                               = delete;
	               LedPiPwmSoft (const LedPiPwmSoft &other)      = delete;
	               LedPiPwmSoft (LedPiPwmSoft &&other)           = delete;
	LedPiPwmSoft & operator = (const LedPiPwmSoft &other)        = delete;
	LedPiPwmSoft & operator = (LedPiPwmSoft &&other)             = delete;
	bool           operator == (const LedPiPwmSoft &other) const = delete;
	bool           operator != (const LedPiPwmSoft &other) const = delete;

}; // class LedPiPwmSoft



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/LedPiPwmSoft.hpp"



#endif   // mfx_hw_LedPiPwmSoft_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
