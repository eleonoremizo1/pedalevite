/*****************************************************************************

        HigepioLinux.cpp
        Author: Laurent de Soras, 2024

Reference:
https://www.kernel.org/doc/html/latest/driver-api/gpio/index.html
https://github.com/raspberrypi/linux/blob/rpi-6.6.y/include/uapi/linux/gpio.h

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/HigepioLinux.h"
#include "mfx/hw/SleepLinux.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdexcept>
#include <system_error>

#include <cassert>
#include <cstring>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
	Opens all the available GPIO lines tied to physical pins of the main
	connector.
Input parameters:
	- exclusion_mask: bits set per GPIO line which the object will not try to
		open. Sometimes an open line may implicitly change the GPIO function,
		even if it is not used. For example the I2C lines may be turned into
		simple inputs when closed.
Throws: std::system_error
==============================================================================
*/

HigepioLinux::HigepioLinux (uint64_t exclusion_mask)
:	_chip (find_gpio_chip ())
,	_fd (open (_chip.c_str (), O_RDONLY))
{
	try
	{
		if (_fd < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot open GPIO device file"
			);
		}

		// Open requests for all the GPIO lines tied to physical pins of the
		// main connector.
		// Some pins may be unavailable because already open for other purposes
		// by the system or another application (for example the SPI interface).
		// In this case we silently ignore the error, unless it fails for every
		// pin.
		bool           attempt_flag              = false;
		bool           at_least_one_success_flag = false;
		for (int gpio_idx = 0; gpio_idx < _nbr_gpio; ++ gpio_idx)
		{
			if (((exclusion_mask >> gpio_idx) & 1) == 0)
			{
				attempt_flag    = true;
				gpio_v2_line_request req {}; // Default config: no flag
				req.offsets [0] = gpio_idx;
				req.num_lines   = 1;
				const auto err  = ioctl (_fd, GPIO_V2_GET_LINE_IOCTL, &req);
				if (err >= 0)
				{
					Line &         line = _line_arr [gpio_idx];
					line._fd    = req.fd;
					line._flags = req.config.flags;
					at_least_one_success_flag = true;
				}
			}
		}

		if (attempt_flag && ! at_least_one_success_flag)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot open request for any GPIO line"
			);
		}
	}

	catch (...)
	{
		cleanup ();
		throw;
	}
}



/*
==============================================================================
Name: dtor
Description:
	All the open lines are closed.
==============================================================================
*/

HigepioLinux::~HigepioLinux ()
{
	cleanup ();
}



/*
==============================================================================
Name: set_pin_dir
Description:
	Sets the pin as input or output.
Input parameters:
	- gpio: valid GPIO line index, >= 0
	- out_flag: true for output, false for input
==============================================================================
*/

void	HigepioLinux::set_pin_dir (int gpio, bool out_flag) noexcept
{
	modify_flags (gpio,
		[&] (uint64_t &flags) noexcept
		{
			if (out_flag)
			{
				flags &= ~GPIO_V2_LINE_FLAG_INPUT;
				flags |=  GPIO_V2_LINE_FLAG_OUTPUT;
			}
			else
			{
				flags &= ~GPIO_V2_LINE_FLAG_OUTPUT;
				flags |=  GPIO_V2_LINE_FLAG_INPUT;
			}
		}
	);
}



/*
==============================================================================
Name: set_pull
Description:
	Activate the pull-up or pull-down on the given GPIO line, or deactivate it.
Input parameters:
	- gpio: valid GPIO line index, >= 0
	- pull: the state of the pull (UP, DOWN or NONE)
==============================================================================
*/

void	HigepioLinux::set_pull (int gpio, PinPullType pull) noexcept
{
	modify_flags (gpio,
		[&] (uint64_t &flags) noexcept
		{
			flags &= ~(
				  GPIO_V2_LINE_FLAG_BIAS_PULL_UP
				| GPIO_V2_LINE_FLAG_BIAS_PULL_DOWN
				| GPIO_V2_LINE_FLAG_BIAS_DISABLED
			);

			switch (pull)
			{
			case	PinPullType::NONE:
				flags |= GPIO_V2_LINE_FLAG_BIAS_DISABLED;
				break;
			case	PinPullType::DOWN:
				flags |= GPIO_V2_LINE_FLAG_BIAS_PULL_DOWN;
				break;
			case	PinPullType::UP:
				flags |= GPIO_V2_LINE_FLAG_BIAS_PULL_UP;
				break;
			}
		}
	);
}



/*
==============================================================================
Name: read_pin
Description:
	Reads the state of a GPIO line configured as input
Input parameters:
	- gpio: valid GPIO line index, >= 0. Must have been previously configured
		as input.
Returns: the state of the line: 1 for high, 0 for low.
==============================================================================
*/

int	HigepioLinux::read_pin (int gpio) const noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	const Line &   line = _line_arr [gpio];
	if (line._fd <= 0)
	{
		assert (false);
		return 0;
	}
	else
	{
		assert (line.is_dir_set (false));

		gpio_v2_line_values  v {};
		v.mask = 1;
		const int err = ioctl (line._fd, GPIO_V2_LINE_GET_VALUES_IOCTL, &v);
		if (err < 0)
		{
			assert (false);
		}

		return int (v.bits);
	}
}



/*
==============================================================================
Name: write_pin
Description:
	Sets a GPIO line state. The line must be writeable.
Input parameters:
	- gpio: valid GPIO line index, >= 0. Must have been previously configured
		as output.
	- val: new state, 0 for low, anything else for high.
==============================================================================
*/

void	HigepioLinux::write_pin (int gpio, int val) const noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	const Line &   line = _line_arr [gpio];
	if (line._fd <= 0)
	{
		assert (false);
	}
	else
	{
		assert (line.is_dir_set (true));

		gpio_v2_line_values  v {};
		v.mask = 1;
		v.bits = (val != 0) ? 1 : 0;
		const int err = ioctl (line._fd, GPIO_V2_LINE_SET_VALUES_IOCTL, &v);
		if (err < 0)
		{
			assert (false);
		}
	}
}



/*
==============================================================================
Name: sleep
Description:
	Waits for a given duration.
Input parameters:
	- dur_us: wait duration in microseconds. When dur_us <= 0, the function
		returns immediately.
==============================================================================
*/

void	HigepioLinux::sleep (uint32_t dur_us) const noexcept
{
	SleepLinux::sleep (dur_us);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



bool	HigepioLinux::Line::is_dir_set (bool out_flag) const noexcept
{
	if (out_flag)
	{
		return ((_flags & GPIO_V2_LINE_FLAG_OUTPUT) != 0);
	}

	return ((_flags & GPIO_V2_LINE_FLAG_INPUT) != 0);
}



void	HigepioLinux::cleanup () noexcept
{
	for (auto &line : _line_arr)
	{
		if (line._fd > 0)
		{
			close (line._fd);
			line._fd = 0;
		}
	}

	if (_fd >= 0)
	{
		close (_fd);
		_fd = -1;
	}
}



// void f (uint64_t &flags);
template <typename F> 
void	HigepioLinux::modify_flags (int gpio, F f) noexcept
{
	assert (gpio >= 0);
	assert (gpio < _nbr_gpio);

	Line &         line = _line_arr [gpio];
	if (line._fd <= 0)
	{
		assert (false);
	}
	else
	{
		const auto     old_flags = line._flags;

		f (line._flags);

		gpio_v2_line_config config {};
		config.flags = line._flags;

		const auto     err =
			ioctl (line._fd, GPIO_V2_LINE_SET_CONFIG_IOCTL, &config);
		if (err < 0)
		{
			assert (false);
			line._flags = old_flags;
		}
	}
}



std::string	HigepioLinux::find_gpio_chip ()
{
	// Label of the chip we are looking for
	constexpr char chiplabel_0 [] =
#if PV_RPI_VER_MAJOR == 5
		"pinctrl-rp1"
#elif PV_RPI_VER_MAJOR == 4
		"pinctrl-bcm2711"
#elif PV_RPI_VER_MAJOR >= 1 && PV_RPI_VER_MAJOR <= 3
		"pinctrl-bcm2835"
#else
# error Unknown or unsupported Raspberry Pi model.
#endif
		;

	char           name_0 [] = "/dev/gpiochip0";
	constexpr auto pos_idx = int (sizeof (name_0) - 2);
	do
	{
		// Opens the device files
		auto           fd = open (name_0, O_RDONLY);
		if (fd < 0)
		{
			// File does not exist: end of the enumeration
			if (errno == ENOENT)
			{
				break;
			}
			// Other error: failure
			else
			{
				throw std::system_error (
					std::error_code (errno, std::system_category ()),
					"Cannot open GPIO device file while scanning all files"
				);
			}
		}

		// Gets the device information
		struct ::gpiochip_info   info {};
		const auto     err = ioctl (fd, GPIO_GET_CHIPINFO_IOCTL, &info);
		close (fd);

		if (err < 0)
		{
			throw std::system_error (
				std::error_code (errno, std::system_category ()),
				"Cannot get info for the GPIO device file"
			);
		}

		if (strstr (info.label, chiplabel_0) != nullptr)
		{
			return name_0;
		}

		// Next file to check
		++ name_0 [pos_idx];
	}
	while (name_0 [pos_idx] <= '9');

	throw std::runtime_error ("Couldn\'t find suitable gpiochip file.");
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
