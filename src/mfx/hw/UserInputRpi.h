/*****************************************************************************

        UserInputRpi.h
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_UserInputRpi_HEADER_INCLUDED)
#define mfx_hw_UserInputRpi_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "conc/CellPool.h"
#include "mfx/ui/RotEnc.h"
#include "mfx/ui/TimeShareCbInterface.h"
#include "mfx/ui/UserInputInterface.h"
#include "mfx/hw/Higepio.h"
#include "mfx/hw/I2cLinux.h"
#include "mfx/hw/SpiLinux.h"
#include "mfx/Cst.h"
#include "mfx/features.h"
#include "mfx/PotRel.h"

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	#include "mfx/ui/PotEndlessDelta.h"
#endif

#include <array>
#include <atomic>
#include <mutex>
#include <thread>
#include <vector>



namespace mfx
{

namespace ui
{
	class TimeShareThread;
}

namespace hw
{



class UserInputRpi final
:	public ui::UserInputInterface
,	public ui::TimeShareCbInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	enum BinSrc
	{
		BinSrc_GPIO = 0,
		BinSrc_PORT_EXP,

		BinSrc_NBR_ELT
	};

	class SwitchSrc
	{
	public:
		BinSrc         _type;
		int            _pos;
	};
	class RotEncSrc
	{
	public:
		BinSrc         _type;
		int            _pos_0;
		int            _pos_1;
		int            _dir_mul; // 1 or -1
		PotRel         _pr_idx;  // Index of the relative pot mapped to this source
	};
	class EndlessPotSrc
	{
	public:
		int            _adc_idx;
		std::array <int, 2>
		               _chn_idx_arr;
		PotRel         _pr_idx;  // Index of the relative pot mapped to this source
	};
	class PotSrc
	{
	public:
		int            _adc_idx;
		int            _chn_idx;
	};

	static const std::chrono::nanoseconds               // Nanoseconds
	                  _antibounce_time;

	static constexpr int _nbr_dev_23017   = 2;
	static const     int  _i2c_dev_23017_arr [_nbr_dev_23017];
	static constexpr int _nbr_sw_23017    = 16;            // Inputs per device

	static constexpr int _nbr_sw_gpio     = 2;
	static const     int  _gpio_pin_arr [_nbr_sw_gpio];

	static constexpr int _nbr_rot_enc     = 2 + (
		  (mfx_features_POTREL == mfx_features_POTREL_DISCRETE)
		 ? PotRel_NBR_GEN
		 : 0
	);

	static constexpr int _nbr_dev_adc     = 1 + (
		  (mfx_features_POTREL != mfx_features_POTREL_DISCRETE)
		? 1
		: 0
	);
	static constexpr int _nbr_adc_chn     = 8;             // Per MCP3008 chip
	static constexpr int _res_adc         = 10;            // Bits
	static constexpr int _spi_port        = 0;             // For the ADC
	static constexpr int _spi_rate        = 1'000'000;     // Hz

	static constexpr int _sample_freq_adc = 100;           // Hz

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	static constexpr int _nbr_endless_pot = PotRel_NBR_GEN;
#endif // mfx_features_POTREL

	// ADC: number of digital steps to clip at each end of the range, because
	// the last steps may be unreachable (parasitic resistance preventing to
	// obtain exactly 0 V or Vref at the converter input).
	// Please use the test application to calibrate the following values:
	static constexpr int _dead_zone_lo    = 1; // >= 0
	static constexpr int _dead_zone_hi    = 0; // >= 0

	// The first MCP23017 unit is completely filled with switches, and the
	// second one has only 2 lines for switches.
	static constexpr int _nbr_switches    = _nbr_sw_gpio + _nbr_sw_23017 + 2;
	static const SwitchSrc
	               _switch_arr [_nbr_switches];

	static const RotEncSrc
	               _rotenc_arr [_nbr_rot_enc];

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	static const std::array <EndlessPotSrc, _nbr_endless_pot>
	               _endless_pot_arr;
#endif // mfx_features_POTREL

	// ADC channel -> potentiometer index
	static const std::array <PotSrc, Cst::_nbr_pot_abs>
	               _pot_arr;

	explicit       UserInputRpi (ui::TimeShareThread &thread_spi, Higepio &io);
	virtual        ~UserInputRpi ();



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	// UserInputInterface
	int            do_get_nbr_param (ui::UserInputType type) const final;
	void           do_set_msg_recipient (ui::UserInputType type, int index, MsgQueue *queue_ptr) final;
	void           do_return_cell (MsgCell &cell) final;
	std::chrono::microseconds
	               do_get_cur_date () const final;

	// TimeShareCbInterface
	bool           do_process_timeshare_op () final;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class SwitchState
	{
	public:
		               SwitchState ();
		std::atomic <bool>
			            _flag;
		std::chrono::nanoseconds
		               _time_last; // ns
		bool           is_set () const;
	};
	typedef std::array <SwitchState, _nbr_switches> SwitchStateArray;

	class PotState
	{
	public:
		static constexpr int _val_none = -666;
		std::atomic <int>
		               _cur_val { _val_none };
		int            _alt_val { _val_none };
		bool           is_set () const { return _cur_val != _val_none; }
	};
	typedef std::array <PotState, Cst::_nbr_pot_abs> PotStateArray;

#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	typedef std::array <
		ui::PotEndlessDelta, _nbr_endless_pot
	> PotEndlessStateArray;
#endif // mfx_features_POTREL

	typedef std::array <ui::RotEnc, PotRel_NBR_ELT>   RotEncStateArray;

	typedef conc::CellPool <ui::UserInputMsg> MsgPool;
	typedef std::vector <MsgQueue *> QueueArray;
	typedef std::array <QueueArray, ui::UserInputType_NBR_ELT> RecipientList;

	void           close_everything ();
	void           polling_loop ();
	void           read_data (bool low_freq_flag);
	void           handle_switch (int index, bool flag, std::chrono::nanoseconds cur_time);
	void           handle_rotenc (int index, bool f0, bool f1, std::chrono::nanoseconds cur_time);
	void           handle_pot (int index, int val, std::chrono::nanoseconds cur_time);
#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	void           handle_endless_pot (int index, int v0, int v1, std::chrono::nanoseconds cur_time);
#endif // mfx_features_POTREL
	void           enqueue_val (std::chrono::nanoseconds date, ui::UserInputType type, int index, float val);
	int            read_adc (SpiLinux &port, int chn);
	std::chrono::nanoseconds
	               read_clock_ns () const;

	ui::TimeShareThread &
	               _thread_spi;
	Higepio &      _io;
	std::array <I2cLinux, _nbr_dev_23017>
	               _hnd_23017_arr;      // MCP23017: Port expander
	std::array <SpiLinux, _nbr_dev_adc>
	               _hnd_3008_arr;       // MCP3008 : ADC

	RecipientList  _recip_list;
	SwitchStateArray
	               _switch_state_arr;
	PotStateArray  _pot_state_arr;
	RotEncStateArray
	               _rotenc_state_arr;
#if mfx_features_POTREL != mfx_features_POTREL_DISCRETE
	PotEndlessStateArray
	               _pot_endless_state_arr;
#endif // mfx_features_POTREL


	MsgPool        _msg_pool;

	std::atomic <bool>
	               _quit_flag;
	std::thread    _polling_thread;
	int            _polling_count;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               UserInputRpi ()                               = delete;
	               UserInputRpi (const UserInputRpi &other)      = delete;
	               UserInputRpi (UserInputRpi &&other)           = delete;
	UserInputRpi & operator = (const UserInputRpi &other)        = delete;
	UserInputRpi & operator = (UserInputRpi &&other)             = delete;
	bool           operator == (const UserInputRpi &other) const = delete;
	bool           operator != (const UserInputRpi &other) const = delete;

}; // class UserInputRpi



}  // namespace ui
}  // namespace mfx



//#include "mfx/ui/UserInputRpi.hpp"



#endif   // mfx_ui_UserInputRpi_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
