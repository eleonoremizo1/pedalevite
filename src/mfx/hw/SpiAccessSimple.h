/*****************************************************************************

        SpiAccessSimple.h
        Author: Laurent de Soras, 2024

Standard access to a SPI device, using normal channel selection.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_SpiAccessSimple_HEADER_INCLUDED)
#define mfx_hw_SpiAccessSimple_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/SpiAccessInterface.h"



namespace mfx
{
namespace hw
{



class SpiAccessSimple
:	public SpiAccessInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       SpiAccessSimple (int chn, int speed);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual int    do_get_chn () const noexcept override;
	virtual int    do_get_speed () const noexcept override;

	virtual void   do_start () const noexcept override { }
	virtual void   do_terminate () const noexcept override { }



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// SPI channel, >= 0
	int            _chn   = -1;

	// Maximum transfer speed, bit/s, > 0
	int            _speed = -1;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               SpiAccessSimple ()                               = delete;
	               SpiAccessSimple (const SpiAccessSimple &other)   = delete;
	               SpiAccessSimple (SpiAccessSimple &&other)        = delete;
	SpiAccessSimple &
	               operator = (const SpiAccessSimple &other)        = delete;
	SpiAccessSimple &
	               operator = (SpiAccessSimple &&other)             = delete;
	bool           operator == (const SpiAccessSimple &other) const = delete;
	bool           operator != (const SpiAccessSimple &other) const = delete;

}; // class SpiAccessSimple



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/SpiAccessSimple.hpp"



#endif // mfx_hw_SpiAccessSimple_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
