/*****************************************************************************

        LedPiPwmSoft.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/hw/GpioPin.h"
#include "mfx/hw/LedPiPwmSoft.h"
#include "mfx/hw/SleepLinux.h"

#include <sys/time.h>

#include <thread>

#include <cassert>



#undef mfx_hw_LedPiPwmSoft_REVERSE_ORDER



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



const int	LedPiPwmSoft::_gpio_pin_arr [_nbr_led] =
#if defined (mfx_hw_LedPiPwmSoft_REVERSE_ORDER)
	{ GpioPin::_led_2, GpioPin::_led_1, GpioPin::_led_0 };
#else
	{ GpioPin::_led_0, GpioPin::_led_1, GpioPin::_led_2 };
#endif



LedPiPwmSoft::LedPiPwmSoft (Higepio &io)
:	_io (io)
,	_pwm_mgr ()
,	_quit_flag (false)
,	_refresher ()
{
	_pwm_mgr.set_nbr_chn (_nbr_led);
	_pwm_mgr.set_period (_pwm_cycle);
	_pwm_mgr.set_max_sleep (_pwm_cycle);

	for (auto gpio : _gpio_pin_arr)
	{
		io.set_pin_dir (gpio, true);
		io.write_pin (gpio, 0);
	}

	_refresher = std::thread (&LedPiPwmSoft::refresh_loop, this);
}



LedPiPwmSoft::~LedPiPwmSoft ()
{
	if (_refresher.joinable ())
	{
		_quit_flag = true;
		_refresher.join ();
	}

	for (auto gpio : _gpio_pin_arr)
	{
		_io.write_pin (gpio, 0);
		_io.set_pin_dir (gpio, false);
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



int	LedPiPwmSoft::do_get_nbr_led () const
{
	return _nbr_led;
}



void	LedPiPwmSoft::do_set_led (int index, float val)
{
	_pwm_mgr.set_level (index, val);
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	LedPiPwmSoft::refresh_loop ()
{
	while (! _quit_flag)
	{
		timeval        now {};
		gettimeofday (&now, nullptr);
		const auto     timestamp =
			  uint32_t (now.tv_sec) * uint32_t (1'000'000)
			+ uint32_t (now.tv_usec);

		const auto     res = _pwm_mgr.process (timestamp);

		for (int index = 0; index < _nbr_led; ++index)
		{
			if (((res._change_mask >> index) & 1) != 0)
			{
				const auto     pin_val = int (_pwm_mgr.get_state (index)); 
				_io.write_pin (_gpio_pin_arr [index], pin_val);
			}
		}

		SleepLinux::sleep_accurate (res._sleep_time);
	}

	_quit_flag = false;
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
