/*****************************************************************************

        SpiLinux.h
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_SpiLinux_HEADER_INCLUDED)
#define mfx_hw_SpiLinux_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/SpiAccessInterface.h"

#include <memory>

#include <cstdint>



namespace mfx
{
namespace hw
{



class SpiLinux
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	// Maximum theoretical speed is higher (125 MHz) but the physical pins
	// will not follow.
	// https://forums.raspberrypi.com/viewtopic.php?p=1662572#p1662572
	static constexpr int   _speed_min =     32'000; // bit/s
	static constexpr int   _speed_max = 32'000'000; // bit/s

	explicit       SpiLinux (std::unique_ptr <SpiAccessInterface> access_uptr, const char *err_0 = nullptr);
	               SpiLinux (SpiLinux &&other)   = default;
	               ~SpiLinux ();

	SpiLinux &     operator = (SpiLinux &&other) = default;

	int            rw_data (uint8_t *data_ptr, int len) const noexcept;
	int            rw_data (uint8_t *dst_ptr, const uint8_t *src_ptr, int len) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:




/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	void           clean_up () noexcept;

	int            _hnd   = 0; // File handle, > 0
	int            _speed = 0; // Transmission speed, Hz, > 0
	std::unique_ptr <SpiAccessInterface>
	               _access_uptr;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               SpiLinux ()                               = delete;
	               SpiLinux (const SpiLinux &other)          = delete;
	SpiLinux &     operator = (const SpiLinux &other)        = delete;
	bool           operator == (const SpiLinux &other) const = delete;
	bool           operator != (const SpiLinux &other) const = delete;

}; // class SpiLinux



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/SpiLinux.hpp"



#endif   // mfx_hw_SpiLinux_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
