/*****************************************************************************

        SpiAccessInterface.h
        Author: Laurent de Soras, 2024

With this interface, it is possible to implement custom activation for a SPI
device, which is useful to talk to more than 2 devices on the same bus.

The SpiAccessSimple class implements standard access.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_SpiAccessInterface_HEADER_INCLUDED)
#define mfx_hw_SpiAccessInterface_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace hw
{



class SpiAccessInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	virtual        ~SpiAccessInterface () = default;

	int            get_chn () const noexcept;
	int            get_speed () const noexcept;

	void           start () const noexcept;
	void           terminate () const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual int    do_get_chn () const noexcept = 0;
	virtual int    do_get_speed () const noexcept = 0;

	virtual void   do_start () const noexcept = 0;
	virtual void   do_terminate () const noexcept = 0;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const SpiAccessInterface &other) const = delete;
	bool           operator != (const SpiAccessInterface &other) const = delete;

}; // class SpiAccessInterface



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/SpiAccessInterface.hpp"



#endif // mfx_hw_SpiAccessInterface_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
