/*****************************************************************************

        SpiAccessGpio.h
        Author: Laurent de Soras, 2024

Uses a specific GPIO pin to command the CS pin of the SPI device (instead of
the default CS0 or CS1 built-in GPIO pins).

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_SpiAccessGpio_HEADER_INCLUDED)
#define mfx_hw_SpiAccessGpio_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/Higepio.h"
#include "mfx/hw/SpiAccessInterface.h"



namespace mfx
{
namespace hw
{



class SpiAccessGpio
:	public SpiAccessInterface
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	explicit       SpiAccessGpio (Higepio &io, int gpio_idx, int speed);



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:

	virtual int    do_get_chn () const noexcept override;
	virtual int    do_get_speed () const noexcept override;

	virtual void   do_start () const noexcept override;
	virtual void   do_terminate () const noexcept override;



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	Higepio &      _io;

	// Index of the GPIO pin activating the device when set to low.
	int            _gpio_idx = -1;

	// Maximum transfer speed, bit/s, > 0
	int            _speed    = -1;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               SpiAccessGpio ()                               = delete;
	               SpiAccessGpio (const SpiAccessGpio &other)     = delete;
	               SpiAccessGpio (SpiAccessGpio &&other)          = delete;
	SpiAccessGpio& operator = (const SpiAccessGpio &other)        = delete;
	SpiAccessGpio& operator = (SpiAccessGpio &&other)             = delete;
	bool           operator == (const SpiAccessGpio &other) const = delete;
	bool           operator != (const SpiAccessGpio &other) const = delete;

}; // class SpiAccessGpio



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/SpiAccessGpio.hpp"



#endif // mfx_hw_SpiAccessGpio_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
