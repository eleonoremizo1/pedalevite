/*****************************************************************************

        SleepLinux.h
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_SleepLinux_HEADER_INCLUDED)
#define mfx_hw_SleepLinux_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



struct timeval;

namespace mfx
{
namespace hw
{



class SleepLinux
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static void    sleep (uint32_t dur_us) noexcept;
	static void    sleep_active (uint32_t dur_us) noexcept;
	static void    sleep_accurate (uint32_t dur_us) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	class TSUs
	{
	public:
		static constexpr uint32_t  _us_per_s = 1'000'000;
		explicit inline
		              TSUs (uint32_t dur_us) noexcept;
		uint32_t       _s  = 0; // Seconds
		uint32_t       _us = 0; // Microseconds, < _us_per_s
	};

	static void    sleep_system (uint32_t dur_us) noexcept;
	static void    sleep_active_no_check (uint32_t dur_us) noexcept;
	static void    sleep_active_from (const ::timeval &beg, uint32_t dur_us) noexcept;
	static void    sleep_active_until (const ::timeval &end) noexcept;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               SleepLinux ()                               = delete;
	               SleepLinux (const SleepLinux &other)        = delete;
	               SleepLinux (SleepLinux &&other)             = delete;
	SleepLinux &   operator = (const SleepLinux &other)        = delete;
	SleepLinux &   operator = (SleepLinux &&other)             = delete;
	bool           operator == (const SleepLinux &other) const = delete;
	bool           operator != (const SleepLinux &other) const = delete;

}; // class SleepLinux



}  // namespace hw
}  // namespace mfx



//#include "mfx/hw/SleepLinux.hpp"



#endif   // mfx_hw_SleepLinux_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
