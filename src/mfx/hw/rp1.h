/*****************************************************************************

        rp1.h
        Author: Laurent de Soras, 2024

Source:
Raspberry Pi RP1 Peripherals, draft 2023-11-07
https://datasheets.raspberrypi.com/rp1/rp1-peripherals.pdf

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_rp1_HEADER_INCLUDED)
#define mfx_hw_rp1_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace hw
{
namespace rp1
{



// Base address for the peripherals in the local RP1 address space, in bytes
static constexpr uint32_t _base_local = 0x40000000;

// Length of the peripheral space, in bytes
static constexpr uint32_t _len        = 0x00400000;



}  // namespace rp1
}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_rp1_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
