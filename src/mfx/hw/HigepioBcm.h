/*****************************************************************************

        HigepioBcm.h
        Author: Laurent de Soras, 2023

Only Pi 3 and 4 are supported.
Cannot access GPIO pins >= 31

Exclusion mask in the constructor is ignored and only here to ensure API
compatibility with HigepioLinux.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_HigepioBcm_HEADER_INCLUDED)
#define mfx_hw_HigepioBcm_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/hw/bcm2837gpio.h"
#include "mfx/hw/GpioBcm.h"
#include "mfx/hw/PinPullType.h"

#include <cstdint>



namespace mfx
{
namespace hw
{



class HigepioBcm
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_gpio = bcm2837gpio::_nbr_gpio;

	               HigepioBcm ()  = default;
	explicit       HigepioBcm (uint64_t /*exclusion_mask*/) { }
	               ~HigepioBcm () = default;

	inline void    set_pin_dir (int gpio, bool out_flag) noexcept;
	inline void    set_pull (int gpio, PinPullType pull) noexcept;
	inline void    set_pin_mode (int gpio, bcm2837gpio::PinFnc mode) noexcept;
	inline int     read_pin (int gpio) const noexcept;
	inline void    write_pin (int gpio, int val) const noexcept;

	void           sleep (uint32_t dur_us) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	GpioBcm        _ga;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	               HigepioBcm (const HigepioBcm &other)        = delete;
	               HigepioBcm (HigepioBcm &&other)             = delete;
	HigepioBcm &   operator = (const HigepioBcm &other)        = delete;
	HigepioBcm &   operator = (HigepioBcm &&other)             = delete;
	bool           operator == (const HigepioBcm &other) const = delete;
	bool           operator != (const HigepioBcm &other) const = delete;

}; // class HigepioBcm



}  // namespace hw
}  // namespace mfx



#include "mfx/hw/HigepioBcm.hpp"



#endif   // mfx_hw_HigepioBcm_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
