/*****************************************************************************

        bcm2837pwm.h
        Author: Laurent de Soras, 2019

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_hw_bcm2837pwm_HEADER_INCLUDED)
#define mfx_hw_bcm2837pwm_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cstdint>



namespace mfx
{
namespace hw
{
namespace bcm2837pwm
{



// PWM registers relative to the base address (p. 141)
static constexpr uint32_t  _pwm_ofs = 0x0020C000;
static constexpr uint32_t  _pwm_len = 0x30;

// Register map
static constexpr uint32_t  _ctl  = 0x00;
static constexpr uint32_t  _sta  = 0x04;
static constexpr uint32_t  _dmac = 0x08;
static constexpr uint32_t  _rng1 = 0x10;
static constexpr uint32_t  _dat1 = 0x14;
static constexpr uint32_t  _fif1 = 0x18;
static constexpr uint32_t  _rng2 = 0x20;
static constexpr uint32_t  _dat2 = 0x24;

// CTL (p. 142)
static constexpr uint32_t  _msen2 = 1u << 15; // Channel 2 M/S Enable
static constexpr uint32_t  _usef2 = 1u << 13; // Channel 2 Use Fifo
static constexpr uint32_t  _pola2 = 1u << 12; // Channel 2 Polarity
static constexpr uint32_t  _sbit2 = 1u << 11; // Channel 2 Silence Bit
static constexpr uint32_t  _rptl2 = 1u << 10; // Channel 2 Repeat Last Data
static constexpr uint32_t  _mode2 = 1u <<  9; // Channel 2 Mode
static constexpr uint32_t  _pwen2 = 1u <<  8; // Channel 2 Enable
static constexpr uint32_t  _msen1 = 1u <<  7; // Channel 1 M/S Enable
static constexpr uint32_t  _clrf1 = 1u <<  6; // Clear Fifo
static constexpr uint32_t  _usef1 = 1u <<  5; // Channel 1 Use Fifo
static constexpr uint32_t  _pola1 = 1u <<  4; // Channel 1 Polarity
static constexpr uint32_t  _sbit1 = 1u <<  3; // Channel 1 Silence Bit
static constexpr uint32_t  _rptl1 = 1u <<  2; // Channel 1 Repeat Last Data
static constexpr uint32_t  _mode1 = 1u <<  1; // Channel 1 Mode
static constexpr uint32_t  _pwen1 = 1u <<  0; // Channel 1 Enable

// STA (p. 144)
static constexpr uint32_t  _sta4  = 1u << 12; // Channel 4 State
static constexpr uint32_t  _sta3  = 1u << 11; // Channel 3 State
static constexpr uint32_t  _sta2  = 1u << 10; // Channel 2 State
static constexpr uint32_t  _sta1  = 1u <<  9; // Channel 1 State
static constexpr uint32_t  _berr  = 1u <<  8; // Bus Error Flag
static constexpr uint32_t  _gapo4 = 1u <<  7; // Channel 4 Gap Occurred Flag
static constexpr uint32_t  _gapo3 = 1u <<  6; // Channel 3 Gap Occurred Flag
static constexpr uint32_t  _gapo2 = 1u <<  5; // Channel 2 Gap Occurred Flag
static constexpr uint32_t  _gapo1 = 1u <<  4; // Channel 1 Gap Occurred Flag
static constexpr uint32_t  _rerr1 = 1u <<  3; // Fifo Read Error Flag
static constexpr uint32_t  _werr1 = 1u <<  2; // Fifo Write Error Flag
static constexpr uint32_t  _empt1 = 1u <<  1; // Fifo Empty Flag
static constexpr uint32_t  _full1 = 1u <<  0; // Fifo Full Flag 

// DMAC (p. 145)
static constexpr uint32_t  _enab  = 1u << 31; // DMA Enable
static constexpr int       _panic =        8; // 8 bits - DMA Threshold for PANIC signal
static constexpr int       _dreq  =        0; // 8 bits - DMA Threshold for DREQ signal



}  // namespace bcm2837pwm
}  // namespace hw
}  // namespace mfx



#endif   // mfx_hw_bcm2837pwm_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
