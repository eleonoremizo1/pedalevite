/*****************************************************************************

        SleepLinux.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/hw/SleepLinux.h"

#include <sys/time.h>

#if fstb_ARCHI == fstb_ARCHI_X86
	#include <emmintrin.h>
#elif fstb_ARCHI == fstb_ARCHI_ARM
	#include <arm_acle.h>
#endif

#include <cassert>
#include <ctime>



namespace mfx
{
namespace hw
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	SleepLinux::sleep (uint32_t dur_us) noexcept
{
	if (dur_us <= 0)
	{
		return;
	}

	// Short duration: active wait
	if (dur_us < 100)
	{
		sleep_active_no_check (dur_us);
	}

	// Longer duration: system call
	else
	{
		sleep_system (dur_us);
	}
}



void	SleepLinux::sleep_active (uint32_t dur_us) noexcept
{
	if (dur_us <= 0)
	{
		return;
	}

	sleep_active_no_check (dur_us);
}



void	SleepLinux::sleep_accurate (uint32_t dur_us) noexcept
{
	if (dur_us <= 0)
	{
		return;
	}

	timeval        beg {};
	gettimeofday (&beg, nullptr);

	// Average overhead + standard deviation for nanosleep(), microseconds.
	// Results of measures on a high-priority thread.
	constexpr uint32_t   overhead =
#if PV_RPI_VER_MAJOR == 5
		55
#elif PV_RPI_VER_MAJOR == 4
		63
#else
		1000 // 1 ms default value if unknown
#endif
		;

	if (dur_us >= overhead)
	{
		sleep_system (dur_us - overhead);
	}

	sleep_active_from (beg, dur_us);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



SleepLinux::TSUs::TSUs (uint32_t dur_us) noexcept
:	_s (dur_us / _us_per_s)
,	_us (dur_us - _s * _us_per_s)
{
	// Nothing
}



void	SleepLinux::sleep_system (uint32_t dur_us) noexcept
{
	const TSUs     part (dur_us);
	timespec       duration {};
	duration.tv_sec  = time_t (part._s);
	duration.tv_nsec = long (part._us) * 1000;
	nanosleep (&duration, nullptr);
}



void	SleepLinux::sleep_active_no_check (uint32_t dur_us) noexcept
{
	timeval        now {};
	gettimeofday (&now, nullptr);
	sleep_active_from (now, dur_us);
}



void	SleepLinux::sleep_active_from (const ::timeval &beg, uint32_t dur_us) noexcept
{
	const TSUs     part (dur_us);
	::timeval      duration {};
	duration.tv_sec  = time_t (part._s);
	duration.tv_usec = suseconds_t (part._us);

	::timeval      end {};
	timeradd (&beg, &duration, &end);
	sleep_active_until (end);
}



void	SleepLinux::sleep_active_until (const ::timeval &end) noexcept
{
	auto           y = [] () noexcept {
#if fstb_ARCHI == fstb_ARCHI_X86
		_mm_pause (); // 140 cycles
#elif fstb_ARCHI == fstb_ARCHI_ARM
	#if fstb_WORD_SIZE == 64
		// ISB SY saves more power on Aarch64
		// https://stackoverflow.com/questions/70810121
		// 15 ns per call on a RPi4
		// https://discord.com/channels/382895736356077570/490937633078247424/1211658028819025970
		#if defined (__clang__)
			__isb (15); __isb (15); __isb (15); __isb (15);
		#elif defined (__GNUC__)
			asm ("isb 15\n\tisb 15\n\tisb 15\n\tisb 15\n\t");
		#endif
	#else
		#if defined (__clang__)
			__yield (); __yield (); __yield (); __yield ();
		#elif defined (__GNUC__)
			asm ("yield\n\tyield\n\tyield\n\tyield\n\t");
		#endif
	#endif
#endif
	};

	::timeval      now {};
	while (true)
	{
		gettimeofday (&now, nullptr);
		if (! timercmp (&now, &end, < ))
		{
			break;
		}
		y (); y (); y (); y ();
		y (); y (); y (); y ();
	}
}



}  // namespace hw
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
