/*****************************************************************************

        SplDataRetrievalInterface.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include	"mfx/dsp/SplDataRetrievalInterface.h"

#include	<cassert>



namespace mfx
{
namespace dsp
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: get_data
Description:
	Retrieve sample data as separate channels. The number of channels is not
	specified or controlled here, it depends on the application. Anyway there
	should be at least one channel.
	Source data out of range are assumed to be 0; accessing them should not
	cause an error.
	The implementation must avoid calling functions that are not real-time
	safe, for example memory allocation or disc access.
Input parameters:
	- pos: Block position within the whole file
	- len: Number of requested sample frames.
	- backward_flag: true if data has to be copied backward. In this case,
		first output data is still [pos], but then it goes backward in the
		source:
			chn_data_ptr_arr [?] [0] <- source [pos    ]
			chn_data_ptr_arr [?] [1] <- source [pos - 1]
			etc.
Output parameters:
	- chn_data_ptr_arr: Contains pointers on channel data to be filled.
==============================================================================
*/

void	SplDataRetrievalInterface::get_data (float *chn_data_ptr_arr [], int64_t pos, int len, bool backward_flag) noexcept
{
	assert (chn_data_ptr_arr != nullptr);
	assert (chn_data_ptr_arr [0] != nullptr);
	assert (len > 0);

	do_get_data (chn_data_ptr_arr, pos, len, backward_flag);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}	// namespace dsp
}	// namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
