/*****************************************************************************

        Downsampler8xSimd.h
        Author: Laurent de Soras, 2016

This object must be aligned on a 16-byte boundary!

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_iir_Downsampler8xSimd_HEADER_INCLUDED)
#define mfx_dsp_iir_Downsampler8xSimd_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/iir/Downsampler2xSimd.h"



namespace mfx
{
namespace dsp
{
namespace iir
{



template <int NC84, int NC42, int NC21>
class Downsampler8xSimd
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           set_coefs (const double coef_84 [NC84], const double coef_42 [NC42], const double coef_21 [NC21]) noexcept;

	inline float   process_sample (const float src_ptr [8]) noexcept;
	void           process_block (float data_ptr [], int nbr_spl) noexcept;
	void           process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;

	inline float   process_sample_4x (const float src_ptr [4]) noexcept;
	void           process_block_4x (float data_ptr [], int nbr_spl) noexcept;
	void           process_block_4x (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;

	inline float   process_sample_2x (const float src_ptr [2]) noexcept;
	void           process_block_2x (float data_ptr [], int nbr_spl) noexcept;
	void           process_block_2x (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;

	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _buf_len = 64;

	using Dwnspl84 = Downsampler2xSimd <NC84>;
	using Dwnspl42 = Downsampler2xSimd <NC42>;
	using Dwnspl21 = Downsampler2xSimd <NC21>;

	Dwnspl84       _ds_84;
	Dwnspl42       _ds_42;
	Dwnspl21       _ds_21;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Downsampler8xSimd &other) const = delete;
	bool           operator != (const Downsampler8xSimd &other) const = delete;

}; // class Downsampler8xSimd



}  // namespace iir
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/iir/Downsampler8xSimd.hpp"



#endif   // mfx_dsp_iir_Downsampler8xSimd_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
