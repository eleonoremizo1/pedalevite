/*****************************************************************************

        Downsampler2xSimd.h
        Author: Laurent de Soras, 2023

Selects the fastest HIIR downsampler (SIMD or not) available, given the
current architecture and number of filter coefficients.

This object must be aligned on a 16-byte boundary!

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_iir_Downsampler2xSimd_HEADER_INCLUDED)
#define mfx_dsp_iir_Downsampler2xSimd_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"

#include "hiir/Downsampler2xFpu.h"
#if defined (fstb_HAS_SIMD) && fstb_ARCHI == fstb_ARCHI_X86
	#include "hiir/Downsampler2xSse.h"
#elif defined (fstb_HAS_SIMD) && fstb_ARCHI == fstb_ARCHI_ARM
	#include "hiir/Downsampler2xNeon.h"
	#include "hiir/Downsampler2xNeonOld.h"
#endif

#include <type_traits>



namespace mfx
{
namespace dsp
{
namespace iir
{



#if defined (fstb_HAS_SIMD) && fstb_ARCHI == fstb_ARCHI_X86
	template <int NC>
	using Downsampler2xSimd = typename std::conditional <
		(NC >= 1) // Current SSE version is always faster than the FPU
	,	hiir::Downsampler2xSse <NC>
	,	hiir::Downsampler2xFpu <NC>
	>::type;
#elif defined (fstb_HAS_SIMD) && fstb_ARCHI == fstb_ARCHI_ARM && (defined (__clang__) || fstb_WORD_SIZE == 64)
	template <int NC>
	using Downsampler2xSimd = typename std::conditional <
		(NC >= 20 && fstb_WORD_SIZE == 32)
	,	hiir::Downsampler2xNeonOld <NC>
	,	hiir::Downsampler2xNeon <NC>
	>::type;
#else
	template <int NC>
	using Downsampler2xSimd = hiir::Downsampler2xFpu <NC>;
#endif



}  // namespace iir
}  // namespace dsp
}  // namespace mfx



//#include "mfx/dsp/iir/Downsampler2xSimd.hpp"



#endif   // mfx_dsp_iir_Downsampler2xSimd_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
