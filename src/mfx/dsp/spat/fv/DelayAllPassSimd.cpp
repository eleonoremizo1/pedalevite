/*****************************************************************************

        DelayAllPassSimd.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/ToolsSimd.h"
#include "mfx/dsp/spat/fv/DelayAllPassSimd.h"

#include <algorithm>

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace spat
{
namespace fv
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	DelayAllPassSimd::set_delay (int len)
{
	_delay_line.set_delay (len);
}



void	DelayAllPassSimd::set_feedback (float coef) noexcept
{
	assert (coef > -1);
	assert (coef < 1);

	_fdbk = coef;
}



void	DelayAllPassSimd::clear_buffers () noexcept
{
	_delay_line.clear_buffers ();
}



/*
Note: the implementation in the original version (by "Jezar at Dreampoint")
is not really an all-pass filter.
It becomes a flat filter (APF with an additional gain) only if the feedback
coefficient is the golden ratio: _fdbk = 0.5 * (sqrt (5) - 1)

See also:
https://ccrma.stanford.edu/~jos/pasp/Freeverb_Allpass_Approximation.html
http://freeverb3vst.osdn.jp/tips/allpass.shtml
*/

void	DelayAllPassSimd::process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (nbr_spl > 0);

	const auto     fdbk = fstb::Vf32 (_fdbk);

	int            pos = 0;
	do
	{
		const int      max_work_len = _delay_line.get_max_rw_len ();
		const int      rem_len      = nbr_spl - pos;
		const int      work_len     = std::min (rem_len, max_work_len);

		const float *  r_ptr        = _delay_line.use_read_data ();
		float *        w_ptr        = _delay_line.use_write_data ();

		const int      n_4          = work_len & -4;

		for (int p2 = 0; p2 < n_4; p2 += 4)
		{
			const auto    buf = fstb::Vf32::loadu (r_ptr   + p2);
			const auto    src = fstb::Vf32::loadu (src_ptr + p2);
			const auto    dst = buf - src;
			const auto    dly = src + buf * fdbk;
			dst.storeu (dst_ptr + p2);
			dly.storeu (w_ptr   + p2);
		}

		for (int p2 = n_4; p2 < work_len; ++p2)
		{
			const auto    buf = r_ptr [p2];
			const auto    src = src_ptr [p2];
			const auto    dst = buf - src;
			const auto    dly = src + buf * _fdbk;
			dst_ptr [p2] = dst;
			w_ptr [p2] = dly;
		}

		_delay_line.step (work_len);
		pos     += work_len;
		src_ptr += work_len;
		dst_ptr += work_len;
	}
	while (pos < nbr_spl);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace fv
}  // namespace spat
}  // namespace dsp
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
