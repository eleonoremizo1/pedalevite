/*****************************************************************************

        DelayAllPass.h
        Author: Laurent de Soras, 2020

All-pass delay
https://ccrma.stanford.edu/~jos/pasp/Allpass_Two_Combs.html

Template parameters: same as DelayFrac.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_spat_DelayAllPass_HEADER_INCLUDED)
#define mfx_dsp_spat_DelayAllPass_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/spat/UniComb.h"




namespace mfx
{
namespace dsp
{
namespace spat
{



template <typename T, int NPL2>
using DelayAllPass = UniComb <T, NPL2, true>;



}  // namespace spat
}  // namespace dsp
}  // namespace mfx



#endif   // mfx_dsp_spat_DelayAllPass_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
