/*****************************************************************************

        UniComb.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_spat_UniComb_CODEHEADER_INCLUDED)
#define mfx_dsp_spat_UniComb_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/ToolsSimd.h"

#include <algorithm>
#include <array>

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace spat
{



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// dst_ptr [i] = src_0_ptr [i] * b0 + src_m_ptr [i] * (APD ? 1 : bm)
template <typename T, bool APD>
fstb_FORCEINLINE void	UniComb_fir_scalar (T * fstb_RESTRICT dst_ptr, const T * fstb_RESTRICT src_0_ptr, const T * fstb_RESTRICT src_m_ptr, T b0, [[maybe_unused]] T bm, int nbr_spl) noexcept
{
	for (int pos = 0; pos < nbr_spl; ++pos)
	{
		const auto     x0 = src_0_ptr [pos] * b0;
		auto           xm = src_m_ptr [pos];
		if constexpr (! APD)
		{
			xm *= bm;
		}
		dst_ptr [pos] = x0 + xm;
	}
}



template <typename T, bool APD>
fstb_FORCEINLINE void	UniComb_fir (T * fstb_RESTRICT dst_ptr, const T * fstb_RESTRICT src_0_ptr, const T * fstb_RESTRICT src_m_ptr, T b0, [[maybe_unused]] T bm, int nbr_spl) noexcept
{
	// Standard scalar processing
	if constexpr (! (std::is_same_v <T, float> && fstb_HAS_SIMD))
	{
		UniComb_fir_scalar (dst_ptr, src_0_ptr, src_m_ptr, b0, bm, nbr_spl);
	}

	// Vector processing
	else
	{
		const auto     b0v = fstb::Vf32 (b0);
		[[maybe_unused]]
		const auto     bmv = fstb::Vf32 (bm);

		const int      len_m16 = nbr_spl & ~15;
		for (int pos = 0; pos < len_m16; pos += 16)
		{
			const auto     x0  = fstb::Vf32::loadu (src_0_ptr + pos     );
			const auto     x1  = fstb::Vf32::loadu (src_0_ptr + pos +  4);
			const auto     x2  = fstb::Vf32::loadu (src_0_ptr + pos +  8);
			const auto     x3  = fstb::Vf32::loadu (src_0_ptr + pos + 12);
			auto           xm0 = fstb::Vf32::loadu (src_m_ptr + pos     );
			auto           xm1 = fstb::Vf32::loadu (src_m_ptr + pos +  4);
			auto           xm2 = fstb::Vf32::loadu (src_m_ptr + pos +  8);
			auto           xm3 = fstb::Vf32::loadu (src_m_ptr + pos + 12);
			if constexpr (! APD)
			{
				xm0 *= bmv;
				xm1 *= bmv;
				xm2 *= bmv;
				xm3 *= bmv;
			}
			xm0.mac (x0, b0v);
			xm1.mac (x1, b0v);
			xm2.mac (x2, b0v);
			xm3.mac (x3, b0v);
			xm0.storeu (dst_ptr + pos     );
			xm1.storeu (dst_ptr + pos +  4);
			xm2.storeu (dst_ptr + pos +  8);
			xm3.storeu (dst_ptr + pos + 12);
		}

		const int      rem = nbr_spl - len_m16;
		if (rem > 0)
		{
			UniComb_fir_scalar <T, APD> (
				dst_ptr   + len_m16,
				src_0_ptr + len_m16,
				src_m_ptr + len_m16,
				b0, bm, rem
			);
		}
	}
}



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_max_len
Description:
	Sets the maximum delay length.
	Mandatory call before processing any sample.
Input parameters:
	- len: maximum length, samples. Should be >= _delay_min.
Throws: std::vector related exception
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_max_len (int len)
{
	assert (len >= _delay_min);

	_delay.set_max_len (len);
}



/*
==============================================================================
Name: set_delay_flt
Description:
	Sets the current delay, floating point version. Obtained delay may be
	slightly different, depending on the phase resolution of the interpolation
	filter.
	Rounding mode for the float to fixed conversion is unspecified.
	The function doesn't allocate memory.
Input parameters:
	- len_spl: delay time, in samples. Must be in [_delay_min ; max_len].
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_delay_flt (float len_spl) noexcept
{
	set_delay_fix (fstb::conv_int_fast (len_spl * _nbr_phases));
}



/*
==============================================================================
Name: set_delay_fix
Description:
	Sets the current delay, fixed point version.
	The function doesn't allocate memory.
Input parameters:
	- len_fixp: delay time in fractions of samples: _nbr_phases <=> 1 sample.
		Same constraints as the floating point version, with the scaled length.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_delay_fix (int len_fixp) noexcept
{
	assert (len_fixp >= _delay_min * _nbr_phases);

	_delay.set_delay_fix (len_fixp);
}



/*
==============================================================================
Name: set_coef
Description:
	Simplified coefficient setting. Use this function in allpass delay mode.
	Default value is 0.
Input parameters:
	- coef: direct feedforward (b0) or inverted delayed feedback (-aM)
		Range: [-1 ; 1].
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_coef (T coef) noexcept
{
	assert (coef >= T (-1.f));
	assert (coef <= T (+1.f));

	set_b0 (coef);
}



/*
==============================================================================
Name: set_b0
Description:
	Sets the direct feedforward coefficient (b0).
	In allpass delay mode, the delayed feedback coefficient is automatically
	set to the opposite value.
	Default value is 0.
Input parameters:
	- b0: direct feedforward coefficient. Should be in [-1 ; 1] in allpass
		delay mode.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_b0 (T b0) noexcept
{
	_b0 = b0;
	if constexpr (_apd_flag)
	{
		assert (b0 >= T (-1.f));
		assert (b0 <= T (+1.f));
		_am = -b0;
	}
}



/*
==============================================================================
Name: set_bm
Description:
	Sets the delayed feedforward coefficient (bM)
	In allpass delay mode, this function shouldn't be called with a coefficient
	different of 1 (it won't have any effect anyway).
	Default value is 1.
Input parameters:
	- bm: delayed feedforward coefficient.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_bm (T bm) noexcept
{
	assert (! _apd_flag || bm == T (1));

	_bm = bm;
}



/*
==============================================================================
Name: set_am
Description:
	Sets the delayed feedback coefficient (b0).
	In allpass delay mode, the direct feedforward coefficient is automatically
	set to the opposite value.
	Default value is 0.
Input parameters:
	- am: delayed feedback coefficient. Should be in [-1 ; 1].
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::set_am (T am) noexcept
{
	assert (am >= T (-1.f));
	assert (am <= T (+1.f));

	_am = am;
	if constexpr (_apd_flag)
	{
		_b0 = -am;
	}
}



/*
==============================================================================
Name: process_sample
Description:
	Filters a single sample and updates the object state.
Input parameters:
	- x: input sample
Returns: the output sample.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
T	UniComb <T, NPL2, APD>::process_sample (T x) noexcept
{
	T              y { _delay.read () };
	x += y * _am;
	_delay.write (x);
	if constexpr (! _apd_flag)
	{
		y *= _bm;
	}
	y += x * _b0;
	_delay.step ();

	return y;
}



/*
==============================================================================
Name: read
Description:
	Processes the input without updating the filter internal delay line.
	This allows the client to insert additional processing in the feedback
	path.
	Usage, equivalent to y = process_sample (x):
		std::tie (y, v) = read (x);
		write (v);
		step ();
Input parameters:
	- x: input sample to process
Returns: A pair of samples made of:
	- first: the regular filter output.
	- second: the feedback value, already multiplied by the delayed feedback
		coefficient. In standard operations, this value should be written
		immediately to the internal delay line with the write() function.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
std::pair <T, T>	UniComb <T, NPL2, APD>::read (T x) const noexcept
{
	T              u { _delay.read () };
	const T        v { x + u * _am };
	if constexpr (! _apd_flag)
	{
		u *= _bm;
	}
	const T        y { u + v * _b0 };

	return std::make_pair (y, v);
}



/*
==============================================================================
Name: read_at
Description:
	Just reads the internal delay line at a random position.
Input parameters:
	- delay: delay in samples, must be in [_delay_min ; max_len].
Returns: the content delayed.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
T	UniComb <T, NPL2, APD>::read_at (int delay) const noexcept
{
	return _delay.read_at (delay);
}



/*
==============================================================================
Name: write
Description:
	Writes the internal delay line (feedback path). Does not update the
	positions, this shoud be done by a subsequent call to step().
	This function is intended to be used in conjunction with read() and step().
Input parameters:
	- v: feedback value to be written to the delay line.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::write (T v) noexcept
{
	_delay.write (v);
}



/*
==============================================================================
Name: step
Description:
	Makes the delay advance of 1 sample in time.
	This function is intended to be used in conjunction with read() and
	write().
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::step () noexcept
{
	_delay.step ();
}



/*
==============================================================================
Name: get_max_block_len
Description:
	Finds the maximum block length for block processing.
	This function should be checked before calling process_block*().
	The result depends on the current delay time, and should be called again
	upon delay time change.
Returns: the maximum block processing length, in samples. >= 0.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
int	UniComb <T, NPL2, APD>::get_max_block_len () const noexcept
{
	return _delay.get_max_block_len ();
}



/*
==============================================================================
Name: get_max_block_len
Description:
	Finds the maximum block length for block processing when using the given
	delay time.
Input parameters:
	- delay: delay time in samples, rounded down to the previous integer.
Returns: the maximum block processing length, in samples. >= 0.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
int	UniComb <T, NPL2, APD>::get_max_block_len (int delay) const noexcept
{
	return _delay.get_max_block_len (delay);
}




/*
==============================================================================
Name: process_block
Description:
	Processes a full block of samples.
Input parameters:
	- src_ptr: pointer on a buffer containing the input samples.
	- nbr_spl: number of samples to process, > 0.
Output parameters:
	- dst_ptr: pointer on a buffer where nbr_spl output samples will be
		written.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::process_block (T dst_ptr [], const T src_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (dst_ptr != nullptr);
	assert (nbr_spl > 0);

	constexpr int  buf_len = 128;
	std::array <T, buf_len> buf_x;
	std::array <T, buf_len> buf_y;
	T * fstb_RESTRICT buf_x_ptr = buf_x.data ();
	T * fstb_RESTRICT buf_y_ptr = buf_y.data ();

	int            max_blk_len = get_max_block_len ();
	if (max_blk_len < 16) // Arbitrary limit. Should be at least 1.
	{
		for (int pos = 0; pos < nbr_spl; ++pos)
		{
			dst_ptr [pos] = process_sample (src_ptr [pos]);
		}
		return;
	}

	max_blk_len = std::min (max_blk_len, buf_len);

	int            pos = 0;
	do
	{
		const int      work_len = std::min (nbr_spl - pos, max_blk_len);

		_delay.read_block (buf_y_ptr, work_len);
		UniComb_fir <T, true> (
			buf_x_ptr, buf_y_ptr, src_ptr + pos, _am, 0, work_len
		);
		_delay.write_block (buf_x_ptr, work_len);
		_delay.step_block (work_len);
		UniComb_fir <T, APD> (
			dst_ptr + pos, buf_x_ptr, buf_y_ptr, _b0, _bm, work_len
		);

		pos += work_len;
	}
	while (pos < nbr_spl);
}



/*
==============================================================================
Name: process_block_var_dly
	Processes a full block of samples and updates the delay time at each
	sample given a user-provided list of times.
	Because of the staggered feedback processing, the internal delay should
	never access (read) a value that will be written during the current block.
	Using the get_max_block_len() on the minimum of the planned delay times
	is a bit too conservative but safe to limit the current block length.
	Note: there is an internal temporary buffer of 128 samples, so the problem
	is mitigated when the delay time stays a few samples above 128.
Input parameters:
	- src_ptr: pointer on a buffer containing the input samples.
	- dly_frc_ptr: buffer containing the delay values, in fractions of samples:
		_nbr_phases <=> 1 sample. Delay shouldn't exceed the specified maximum
		delay time.
	- nbr_spl: number of samples to process, > 0.
Output parameters:
	- dst_ptr: pointer on a buffer where nbr_spl output samples will be
		written.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::process_block_var_dly (T dst_ptr [], const T src_ptr [], const int32_t dly_frc_ptr [], int nbr_spl) noexcept
{
	assert (src_ptr != nullptr);
	assert (dst_ptr != nullptr);
	assert (dly_frc_ptr != nullptr);
	assert (nbr_spl > 0);

	constexpr int  buf_len = 128;
	std::array <T, buf_len> buf_x;
	std::array <T, buf_len> buf_y;
	T * fstb_RESTRICT buf_x_ptr = buf_x.data ();
	T * fstb_RESTRICT buf_y_ptr = buf_y.data ();

	int            pos = 0;
	do
	{
		const int      work_len = std::min (nbr_spl - pos, buf_len);

		_delay.read_block_var_dly (buf_y_ptr, dly_frc_ptr + pos, work_len);
		UniComb_fir <T, true> (
			buf_x_ptr, buf_y_ptr, src_ptr + pos, _am, 0, work_len
		);
		_delay.write_block (buf_x_ptr, work_len);
		_delay.step_block (work_len);
		UniComb_fir <T, APD> (
			dst_ptr + pos, buf_x_ptr, buf_y_ptr, _b0, _bm, work_len
		);

		pos += work_len;
	}
	while (pos < nbr_spl);
}



/*
==============================================================================
Name: read_block_at
Description:
	Reads a full block of samples in the delay line at a random time position.
	The position is always integer (no interpolation).
	This function is not directly constrained by get_max_block_len().
Input parameters:
	- delay: delay of the first (oldest) sample to read, in samples.
		Shoud be in range [0 ; current delay].
	- len: Number of samples to read, in [1 ; delay + 1].
Output parameters:
	- dst_ptr: pointer on a buffer where the read samples are stored.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::read_block_at (T dst_ptr [], int delay, int len) const noexcept
{
	_delay.read_block_at (dst_ptr, delay, len);
}



/*
==============================================================================
Name: clear_buffers
Description:
	Clears the filter state by filling the delay line with zeroes.
==============================================================================
*/

template <typename T, int NPL2, bool APD>
void	UniComb <T, NPL2, APD>::clear_buffers () noexcept
{
	_delay.clear_buffers ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace spat
}  // namespace dsp
}  // namespace mfx



#endif // mfx_dsp_spat_UniComb_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
