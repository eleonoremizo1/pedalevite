/*****************************************************************************

        DelayFrac.hpp
        Author: Laurent de Soras, 2020

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_dsp_spat_DelayFrac_CODEHEADER_INCLUDED)
#define mfx_dsp_spat_DelayFrac_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "fstb/ToolsSimd.h"
#include "mfx/dsp/rspl/InterpFtor.h"

#include <algorithm>

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace spat
{



template <typename T>
static void	DelayFrac_interpolate_block_std (T * fstb_RESTRICT dst_ptr, const T * fstb_RESTRICT src_ptr, const float * fstb_RESTRICT phase_ptr, int len) noexcept
{
	const T        i0 { phase_ptr [0] };
	const T        i1 { phase_ptr [1] };
	const T        i2 { phase_ptr [2] };
	const T        i3 { phase_ptr [3] };

	T              v0 { src_ptr [0] };
	T              v1 { src_ptr [1] };
	T              v2 { src_ptr [2] };
	const int      len_m4 = len & ~3;
	for (int pos = 0; pos < len_m4; pos += 4)
	{
		const T        v3 { src_ptr [pos + 3] };
		const T        x0 { (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3) };
		v0 = src_ptr [pos + 4];
		const T        x1 { (v1 * i0 + v2 * i1) + (v3 * i2 + v0 * i3) };
		v1 = src_ptr [pos + 5];
		const T        x2 { (v2 * i0 + v3 * i1) + (v0 * i2 + v1 * i3) };
		v2 = src_ptr [pos + 6];
		const T        x3 { (v3 * i0 + v0 * i1) + (v1 * i2 + v2 * i3) };
		dst_ptr [pos    ] = x0;
		dst_ptr [pos + 1] = x1;
		dst_ptr [pos + 2] = x2;
		dst_ptr [pos + 3] = x3;
	}
	for (int pos = len_m4; pos < len; ++pos)
	{
		const T        v3 { src_ptr [pos + 3] };
		dst_ptr [pos] = (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3);
		v0 = v1;
		v1 = v2;
		v2 = v3;
	}
}



template <typename T>
static fstb_FORCEINLINE void	DelayFrac_interpolate_block (T * fstb_RESTRICT dst_ptr, const T * fstb_RESTRICT src_ptr, const float * fstb_RESTRICT phase_ptr, int len) noexcept
{
	DelayFrac_interpolate_block_std (dst_ptr, src_ptr, phase_ptr, len);
}

#if defined (fstb_HAS_SIMD)

template <>
inline void	DelayFrac_interpolate_block (float * fstb_RESTRICT dst_ptr, const float * fstb_RESTRICT src_ptr, const float * fstb_RESTRICT phase_ptr, int len) noexcept
{
	const auto     i0 = fstb::Vf32 (phase_ptr [0]);
	const auto     i1 = fstb::Vf32 (phase_ptr [1]);
	const auto     i2 = fstb::Vf32 (phase_ptr [2]);
	const auto     i3 = fstb::Vf32 (phase_ptr [3]);

#if fstb_ARCHI == fstb_ARCHI_ARM

	// len - 1 because vg collects an extra sample
	const int      len_m16 = (len - 1) & ~15;
	auto           v0 = fstb::Vf32::loadu (src_ptr);
	for (int pos = 0; pos < len_m16; pos += 16)
	{
		const auto     v4 = fstb::Vf32::loadu (&src_ptr [pos +  4]);
		const auto     v8 = fstb::Vf32::loadu (&src_ptr [pos +  8]);
		const auto     vc = fstb::Vf32::loadu (&src_ptr [pos + 12]);
		const auto     vg = fstb::Vf32::loadu (&src_ptr [pos + 16]);
		const auto     v1 = fstb::Vf32::compose <1> (v0, v4);
		const auto     v2 = fstb::Vf32::compose <2> (v0, v4);
		const auto     v3 = fstb::Vf32::compose <3> (v0, v4);
		const auto     v5 = fstb::Vf32::compose <1> (v4, v8);
		const auto     v6 = fstb::Vf32::compose <2> (v4, v8);
		const auto     v7 = fstb::Vf32::compose <3> (v4, v8);
		const auto     v9 = fstb::Vf32::compose <1> (v8, vc);
		const auto     va = fstb::Vf32::compose <2> (v8, vc);
		const auto     vb = fstb::Vf32::compose <3> (v8, vc);
		const auto     vd = fstb::Vf32::compose <1> (vc, vg);
		const auto     ve = fstb::Vf32::compose <2> (vc, vg);
		const auto     vf = fstb::Vf32::compose <3> (vc, vg);
		const auto     x0 = (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3);
		const auto     x4 = (v4 * i0 + v5 * i1) + (v6 * i2 + v7 * i3);
		const auto     x8 = (v8 * i0 + v9 * i1) + (va * i2 + vb * i3);
		const auto     xc = (vc * i0 + vd * i1) + (ve * i2 + vf * i3);
		x0.storeu (&dst_ptr [pos     ]);
		x4.storeu (&dst_ptr [pos +  4]);
		x8.storeu (&dst_ptr [pos +  8]);
		xc.storeu (&dst_ptr [pos + 12]);
		v0 = vg;
	}

#else // fstb_ARCHI

	const int      len_m16 = len & ~15;
	for (int pos = 0; pos < len_m16; pos += 16)
	{
		const auto     v0 = fstb::Vf32::loadu (&src_ptr [pos     ]);
		const auto     v1 = fstb::Vf32::loadu (&src_ptr [pos +  1]);
		const auto     v2 = fstb::Vf32::loadu (&src_ptr [pos +  2]);
		const auto     v3 = fstb::Vf32::loadu (&src_ptr [pos +  3]);
		const auto     v4 = fstb::Vf32::loadu (&src_ptr [pos +  4]);
		const auto     v5 = fstb::Vf32::loadu (&src_ptr [pos +  5]);
		const auto     v6 = fstb::Vf32::loadu (&src_ptr [pos +  6]);
		const auto     v7 = fstb::Vf32::loadu (&src_ptr [pos +  7]);
		const auto     v8 = fstb::Vf32::loadu (&src_ptr [pos +  8]);
		const auto     v9 = fstb::Vf32::loadu (&src_ptr [pos +  9]);
		const auto     va = fstb::Vf32::loadu (&src_ptr [pos + 10]);
		const auto     vb = fstb::Vf32::loadu (&src_ptr [pos + 11]);
		const auto     vc = fstb::Vf32::loadu (&src_ptr [pos + 12]);
		const auto     vd = fstb::Vf32::loadu (&src_ptr [pos + 13]);
		const auto     ve = fstb::Vf32::loadu (&src_ptr [pos + 14]);
		const auto     vf = fstb::Vf32::loadu (&src_ptr [pos + 15]);
		const auto     x0 = (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3);
		const auto     x4 = (v4 * i0 + v5 * i1) + (v6 * i2 + v7 * i3);
		const auto     x8 = (v8 * i0 + v9 * i1) + (va * i2 + vb * i3);
		const auto     xc = (vc * i0 + vd * i1) + (ve * i2 + vf * i3);
		x0.storeu (&dst_ptr [pos     ]);
		x4.storeu (&dst_ptr [pos +  4]);
		x8.storeu (&dst_ptr [pos +  8]);
		xc.storeu (&dst_ptr [pos + 12]);
	}

#endif // fstb_ARCHI

	const int      rem = len - len_m16;
	if (rem > 0)
	{
		DelayFrac_interpolate_block_std (
			dst_ptr + len_m16, src_ptr + len_m16, phase_ptr, rem
		);
	}
}

#endif // fstb_HAS_SIMD



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_max_len
Description:
	Sets the maximum delay length.
	Mandatory call before processing any sample.
	This function allocates memory.
Input parameters:
	- len: maximum length, samples. Should be >= _delay_min.
Throws: std::vector related exception
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::set_max_len (int len)
{
	assert (len >= _delay_min);

   if (! _interp_init_flag)
   {
		init_interpolator ();
   }

	const int      buf_len_l2 = fstb::get_next_pow_2 (len + _nbr_phases - 1);
	const int      buf_len    = 1 << buf_len_l2;
	_delay_len = len;
	_buf_msk   = buf_len - 1;
	_buffer.resize (buf_len);

	clear_buffers ();
}



/*
==============================================================================
Name: set_delay_flt
Description:
	Sets the current delay, floating point version. Obtained delay may be
	slightly different, depending on the phase resolution of the interpolation
	filter.
	Rounding mode for the float to fixed conversion is unspecified.
	The function doesn't allocate memory.
Input parameters:
	- len_spl: delay time, in samples. Must be in [_delay_min ; max_len].
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::set_delay_flt (float len_spl) noexcept
{
	set_delay_fix (fstb::conv_int_fast (len_spl * _nbr_phases));
}



/*
==============================================================================
Name: set_delay_fix
Description:
	Sets the current delay, fixed point version.
	The function doesn't allocate memory.
Input parameters:
	- len_fixp: delay time in fractions of samples: _nbr_phases <=> 1 sample.
		Same constraints as the floating point version, with the scaled length.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::set_delay_fix (int len_fixp) noexcept
{
	assert (len_fixp >= _delay_min * _nbr_phases);

	find_phase_and_delay (_phase_ptr, _delay_int, _delay_frc, len_fixp);
}



/*
==============================================================================
Name: get_delay_len_int
Returns: the integer part of the current delay in samples.
==============================================================================
*/

template <typename T, int NPL2>
int	DelayFrac <T, NPL2>::get_delay_len_int () const noexcept
{
	assert (_phase_ptr != nullptr);

	return _delay_int;
}



/*
==============================================================================
Name: process_sample
Description:
	Feeds the delay with an input samples and gets the corresponding delayed
	value from the output of the delay line.
Input parameters:
	- x: input sample
Returns: the delayed sample
==============================================================================
*/

template <typename T, int NPL2>
T	DelayFrac <T, NPL2>::process_sample (T x) noexcept
{
	assert (_phase_ptr != nullptr);

	write (x);
	const T        val { read () };
	step ();

	return val;
}



/*
==============================================================================
Name: read
Description:
	Reads the delay, obtaining a delayed sample at the current state.
Returns: The delayed sample.
==============================================================================
*/

template <typename T, int NPL2>
T	DelayFrac <T, NPL2>::read () const noexcept
{
	assert (_phase_ptr != nullptr);

	return read_safe (_pos_write - _delay_int, *_phase_ptr);
}



/*
==============================================================================
Name: read_at
Description:
	Reads the delay line at a random integer position. The specified delay is
	not constrained by the current delay time.
Input parameters:
	- delay: Desired delay time in integer samples, in [1 ; max_len]
Returns: the delayed value.
==============================================================================
*/

template <typename T, int NPL2>
T	DelayFrac <T, NPL2>::read_at (int delay) const noexcept
{
	assert (delay > 0);
	assert (delay <= _delay_len);

	return _buffer [(_pos_write - delay) & _buf_msk];
}



/*
==============================================================================
Name: write
Description:
	Write (or overwrite) the delay at time 0.
Input parameters:
	- x: Value to be written.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::write (T x) noexcept
{
	assert (_phase_ptr != nullptr);

	_buffer [_pos_write] = x;
}



/*
==============================================================================
Name: step
Description:
	Makes the delay advance of 1 sample in time.
	This function is intended to be used in conjunction with read() and
	write().
	See process_sample() for basic use.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::step () noexcept
{
	assert (_phase_ptr != nullptr);

	_pos_write = (_pos_write + 1) & _buf_msk;
}



/*
==============================================================================
Name: get_max_block_len
Description:
	Finds the maximum block length for block processing when feedback is
	involved when using the given delay time.
	The result depends on the current delay time, and should be called again
	upon delay time change.
	Warning: for very short delay times, block processing with feedback may be
	not available.
Input parameters:
	- delay: delay time in samples, rounded down to the previous integer.
Returns: the maximum block processing length, in samples. >= 0.
==============================================================================
*/

template <typename T, int NPL2>
int	DelayFrac <T, NPL2>::get_max_block_len (int delay) const noexcept
{
	assert (_phase_ptr != nullptr);
	assert (delay >= _delay_min);
	assert (delay <= _delay_len);

	const auto     mbl = delay - (_phase_len - 1);

	return std::max (mbl, 0);
}



/*
==============================================================================
Name: get_max_block_len
Description:
	Finds the maximum block length for block processing when feedback is
	involved.
	The result depends on the current delay time, and should be called again
	upon delay time change.
	Warning: for very short delay times, block processing with feedback may be
	not available.
Returns: the maximum block processing length, in samples. >= 0.
==============================================================================
*/

template <typename T, int NPL2>
int	DelayFrac <T, NPL2>::get_max_block_len () const noexcept
{
	return get_max_block_len (_delay_int);
}



/*
==============================================================================
Name: read_block
Description:
	Reads a delayed block of samples.
	It's the client responsibility to keep the read consistent: trying to read
	values that haven't been written yet will get unspecified values.
Input parameters:
	- len: number of samples to read, in [1 ; integer delay time]
Output parameters:
	- dst_ptr: pointer on a block where to store the delayed samples.
		First sample is the oldest one.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::read_block (T dst_ptr [], int len) const noexcept
{
	assert (_phase_ptr != nullptr);
	assert (len > 0);
	assert (len <= _delay_int);

	const int      buf_len  = int (_buffer.size ());
	int            pos_read = (_pos_write - _delay_int) & _buf_msk;
	int            pos      = 0; // Position within the block
	do
	{
		const int      room     = buf_len - (pos_read + _ph_post);
		const int      rem      = len - pos;
		int            len_part = std::min (rem, room);

		// Read without pointer wrap
		if (pos_read >= _ph_pre && len_part > 0)
		{
			const auto *   src_ptr = &_buffer [pos_read] - _ph_pre;

			if constexpr (_nbr_phases_l2 == 0)
			{
				fstb::copy_no_overlap (dst_ptr + pos, src_ptr, len_part);
			}
			else
			{
				DelayFrac_interpolate_block (
					dst_ptr + pos, src_ptr, _phase_ptr->data (), len_part
				);
			}
		}

		// Read with pointer wrap
		else
		{
			len_part = std::min (rem, _phase_len);
			for (int k = 0; k < len_part; ++k)
			{
				dst_ptr [pos + k] = read_safe (pos_read + k, *_phase_ptr);
			}
		}

		pos     += len_part;
		pos_read = (pos_read + len_part) & _buf_msk;
	}
	while (pos < len);
}



/*
==============================================================================
Name: read_block_var_dly
Description:
	Reads a delayed block of samples. The delay time is updated with the user-
	provided list before each sample read.
	At the end, the delay time is kept to the last dly_fix_ptr value.
	It's the client responsibility to keep the read consistent: trying to read
	values that haven't been written yet will get unspecified values.
Input parameters:
	- dly_fix_ptr: buffer containing the delay values, in fractions of samples:
		_nbr_phases <=> 1 sample. Maximum delay shouldn't exceed the specified
		maximum delay time.
	- len: number of samples to read, in [1 ; max_len]
Output parameters:
	- dst_ptr: pointer on a buffer where nbr_spl output samples will be
		written.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::read_block_var_dly (T dst_ptr [], const int32_t dly_fix_ptr [], int len) noexcept
{
	assert (_phase_ptr != nullptr);
	assert (len > 0);
	assert (len <= _delay_len);

	const Phase *  phase_ptr = _phase_ptr;
	int            delay_int = _delay_int;
	int            delay_frc = _delay_frc;

	for (int pos = 0; pos < len; ++pos)
	{
		find_phase_and_delay (phase_ptr, delay_int, delay_frc, dly_fix_ptr [pos]);
		dst_ptr [pos] = read_safe (_pos_write - delay_int + pos, *phase_ptr);
	}

	_phase_ptr = phase_ptr;
	_delay_int = delay_int;
	_delay_frc = delay_frc;
}



/*
==============================================================================
Name: read_block_at
Description:
	Reads a full block of samples in the delay line at a random time position.
	The position is always integer (no interpolation).
	Not constrained by the current delay length.
Input parameters:
	- delay: delay of the first (oldest) sample to read, in samples.
		Shoud be in range [0 ; current delay].
	- len: Number of samples to read, in [1 ; delay + 1].
Output parameters:
	- dst_ptr: pointer on a buffer where the read samples are stored.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::read_block_at (T dst_ptr [], int delay, int len) const noexcept
{
	assert (delay >= 0);
	assert (delay <= _delay_len);
	assert (len > 0);
	assert (len <= delay + 1);

	const int      buf_len  = int (_buffer.size ());
	int            pos_read = (_pos_write - delay) & _buf_msk;
	const int      room     = buf_len - pos_read;
	const int      len_1    = std::min (len, room);
	const int      len_2    = len - len_1;
	fstb::copy_no_overlap (dst_ptr, &_buffer [pos_read], len_1);
	if (len_2 > 0)
	{
		fstb::copy_no_overlap (dst_ptr + len_1, _buffer.data (), len_2);
	}
}



/*
==============================================================================
Name: write_block
Description:
	Writes a block of input samples to the delay line.
	Delay state is not updated. Call step_block() afterwards.
Input parameters:
	- src_ptr: pointer on the block of input samples.
	- len: number of samples to write, in [1 ; max_len]
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::write_block (const T src_ptr [], int len) noexcept
{
	assert (_phase_ptr != nullptr);
	assert (src_ptr != 0);
	assert (len > 0);
	assert (len <= _delay_len);

	const int      buf_len = int (_buffer.size ());
	const int      room    = buf_len - _pos_write;
	const int      len_1   = std::min (len, room);
	const int      len_2    = len - len_1;
	fstb::copy_no_overlap (&_buffer [_pos_write], src_ptr, len_1);
	if (len_2 > 0)
	{
		fstb::copy_no_overlap (_buffer.data (), src_ptr + len_1, len_2);
	}
}



/*
==============================================================================
Name: step_block
Description:
	Updates the delay line read and write heads after block processing.
Input parameters:
	- len: Number of samples to advance in [1 ; max_len].
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::step_block (int len) noexcept
{
	assert (_phase_ptr != nullptr);
	assert (len > 0);
	assert (len <= _delay_len);

	_pos_write = (_pos_write + len) & _buf_msk;
}



/*
==============================================================================
Name: clear_buffers
Description:
	Clears the filter state by filling the delay line with zeroes.
==============================================================================
*/

template <typename T, int NPL2>
void	DelayFrac <T, NPL2>::clear_buffers () noexcept
{
	std::fill (_buffer.begin (), _buffer.end (), T (0.f));
	_pos_write = 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename T, int NPL2>
bool	DelayFrac <T, NPL2>::_interp_init_flag = false;

template <typename T, int NPL2>
typename DelayFrac <T, NPL2>::PhaseArray	DelayFrac <T, NPL2>::_phase_arr;



template <typename T, int NPL2>
T	DelayFrac <T, NPL2>::read_safe (int pos_read, const Phase &phase) const noexcept
{
	if constexpr (_nbr_phases_l2 == 0)
	{
		return _buffer [pos_read & _buf_msk];
	}

	else
	{
		const T        v0 { _buffer [(pos_read - 2) & _buf_msk] };
		const T        v1 { _buffer [(pos_read - 1) & _buf_msk] };
		const T        v2 { _buffer [(pos_read    ) & _buf_msk] };
		const T        v3 { _buffer [(pos_read + 1) & _buf_msk] };

#if 1

		// FIR interpolation
		const T        i0 { phase [0] };
		const T        i1 { phase [1] };
		const T        i2 { phase [2] };
		const T        i3 { phase [3] };

		return (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3);

#else

		// Simple linear interpolation, for testing
		return v2 + _delay_frc * (v1 - v2) * T (1.f / _nbr_phases);

#endif
	}
}



template <typename T, int NPL2>
T	DelayFrac <T, NPL2>::read_nocheck (int pos_read, const Phase &phase) const noexcept
{
	if constexpr (_nbr_phases_l2 == 0)
	{
		return _buffer [pos_read];
	}

	else
	{
		const T        v0 { _buffer [pos_read - 2] };
		const T        v1 { _buffer [pos_read - 1] };
		const T        v2 { _buffer [pos_read    ] };
		const T        v3 { _buffer [pos_read + 1] };

		// FIR interpolation
		const T        i0 { phase [0] };
		const T        i1 { phase [1] };
		const T        i2 { phase [2] };
		const T        i3 { phase [3] };

		return (v0 * i0 + v1 * i1) + (v2 * i2 + v3 * i3);
	}
}



template <typename T, int NPL2>
void  DelayFrac <T, NPL2>::find_phase_and_delay (const Phase * &phase_ptr, int &delay_int, int &delay_frc, int len_fixp) const noexcept
{
	assert (len_fixp >= _delay_min * _nbr_phases);

	delay_frc = len_fixp & _phase_msk;
	phase_ptr = &_phase_arr [delay_frc];
	delay_int = len_fixp >> _nbr_phases_l2;
	assert (delay_int <= _delay_len);
}



template <typename T, int NPL2>
void  DelayFrac <T, NPL2>::init_interpolator () noexcept
{
	// We use a cubic hermite interpolator to build the FIR.
	// Probably not optimal but close enough to rock'n'roll.
	rspl::InterpFtor::CubicHermite   interp;

	constexpr float   frac_mul = 1.f / _nbr_phases;
	for (int ph_idx = 0; ph_idx < _nbr_phases; ++ph_idx)
	{
		Phase &        phase = _phase_arr [ph_idx];
		const float    frac  = 1 - float (ph_idx) * frac_mul;
		for (int pos = 0; pos < _phase_len; ++pos)
		{
			std::array <float, _phase_len> imp {};
			imp [pos] = 1;
			phase [pos] = interp (frac, imp.data () + 1);
		}
	}

	_interp_init_flag = true;
}



}  // namespace spat
}  // namespace dsp
}  // namespace mfx



#endif   // mfx_dsp_spat_DelayFrac_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
