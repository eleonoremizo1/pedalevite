﻿# spat — spatialisation, environment

Building blocks and complete models for reverberation and acousitcs simulation.

- **`ApfLine`**: reverberator building block, made of all-pass delays, a simple delay and a filter
- **`Cascade`**: utility class to cascade two audio processors
- **`DelayAllPass`**: simple all-pass delay diffusion block
- **`DelayFrac`**: fractional delay using 4-tap FIR interpolation
- **`EarlyRef`**: early-refelection processor, using a multi-tap delay
- **`ReverbDattorro`**: implementation of a reverberator described by Jon Dattorro
- **`ReverbSC`**: implementation of a reverberator designed by Sean Costello
- **`UniComb`**: canonical universal comb filter, using feedforward and feedback paths and a fractional delay

Subnamespaces:

- **`fv`**: implementation of the freeverb model
- **`ltc`**: adaptation of the Takamitsu Endo’s LatticeReverb model
