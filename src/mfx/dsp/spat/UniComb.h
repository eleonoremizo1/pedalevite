/*****************************************************************************

        UniComb.h
        Author: Laurent de Soras, 2024

Canonical universal comb filter, using feedforward and feedback paths.
The delay is fractional.

H(z) = (b0 + bM * z^-M) / (1 - aM * z^-M)

An allpass delay can be derived from it with aM = -b0 and bM = 1
https://ccrma.stanford.edu/~jos/pasp/Allpass_Two_Combs.html

Call at least once set_max_len() before processing data (allocates memory) and
one of the set_delay_*() functions.

Template parameters:

- T, NPL2: see DelayFrac template parameters

- APD: indicates we're using simplifed operations to implement an allpass
	delay.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_spat_UniComb_HEADER_INCLUDED)
#define mfx_dsp_spat_UniComb_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "mfx/dsp/spat/DelayFrac.h"

#include <utility>

#include <cstdint>



namespace mfx
{
namespace dsp
{
namespace spat
{



template <typename T, int NPL2, bool APD = false>
class UniComb
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	typedef T DataType;
	typedef DelayFrac <T, NPL2> Delay;

	static constexpr bool   _apd_flag   = APD;
	static constexpr int _nbr_phases_l2 = Delay::_nbr_phases_l2;
	static constexpr int _nbr_phases    = Delay::_nbr_phases;
	// + 1 because we read the line before writing it.
	static constexpr int _delay_min     = Delay::_delay_min + 1; // Samples

	void           set_max_len (int len);

	fstb_FORCEINLINE void
	               set_delay_flt (float len_spl) noexcept;
	fstb_FORCEINLINE void
	               set_delay_fix (int len_fixp) noexcept;
	fstb_FORCEINLINE void
	               set_coef (T coef) noexcept;
	fstb_FORCEINLINE void
	               set_b0 (T b0) noexcept;
	fstb_FORCEINLINE void
	               set_bm (T bm) noexcept;
	fstb_FORCEINLINE void
	               set_am (T am) noexcept;

	fstb_FORCEINLINE std::pair <T, T>
	               read (T x) const noexcept;
	fstb_FORCEINLINE T
	               read_at (int delay) const noexcept;
	fstb_FORCEINLINE void
	               write (T v) noexcept;
	fstb_FORCEINLINE void
	               step () noexcept;
	fstb_FORCEINLINE T
	               process_sample (T x) noexcept;

	fstb_FORCEINLINE int
	               get_max_block_len () const noexcept;
	fstb_FORCEINLINE int
	               get_max_block_len (int delay) const noexcept;
	void           process_block (T dst_ptr [], const T src_ptr [], int nbr_spl) noexcept;
	void           process_block_var_dly (T dst_ptr [], const T src_ptr [], const int32_t dly_frc_ptr [], int nbr_spl) noexcept;
	fstb_FORCEINLINE void
	               read_block_at (T dst_ptr [], int delay, int len) const noexcept;

	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	Delay         _delay;      // z^-M

	// Filter coefficients
	T             _b0 { 0.f }; // Direct feedforward
	T             _bm { 1.f }; // Delayed feedforward. Ignored in allpass mode.
	T             _am { 0.f }; // Delayed feedback. Should be -_b0 in allpass.



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const UniComb &other) const = delete;
	bool           operator != (const UniComb &other) const = delete;

}; // class UniComb



}  // namespace spat
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/spat/UniComb.hpp"



#endif // mfx_dsp_spat_UniComb_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
