/*****************************************************************************

        DownsamplerLerp.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_fir_DownsamplerLerp_CODEHEADER_INCLUDED)
#define mfx_dsp_fir_DownsamplerLerp_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace fir
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: process_sample
Description:
	Downsamples N input samples to generate a single output sample.
Input parameters:
	- src_ptr: pointer on a buffer containing the N input samples
Returns: the output sample
==============================================================================
*/

template <int N>
float	DownsamplerLerp <N>::process_sample (const float * fstb_RESTRICT src_ptr) noexcept
{
	assert (src_ptr != nullptr);

	return process_sample_internal (src_ptr, _state);
}



/*
==============================================================================
Name: process_block
Description:
	Downsamples a block of samples.
	Cannot work in-place.
Input parameters:
	- src_ptr: pointer on a buffer containing the N * nbr_spl input samples
	- nbr_spl: number of output samples to generate, > 0
Output parameters:
	- dst_ptr: pointer on a buffer receiving nbr_spl output samples.
==============================================================================
*/

template <int N>
void	DownsamplerLerp <N>::process_block (float * fstb_RESTRICT dst_ptr, const float * fstb_RESTRICT src_ptr, int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (dst_ptr != src_ptr);
	assert (nbr_spl > 0);

	auto           cur = _state;
	for (int pos = 0; pos < nbr_spl; ++pos)
	{
		dst_ptr [pos] = process_sample_internal (src_ptr, cur);
		src_ptr += _ratio;
	}
	_state = cur;
}



/*
==============================================================================
Name: clear_buffers
Description:
	Resets the downsampler state
==============================================================================
*/

template <int N>
void	DownsamplerLerp <N>::clear_buffers () noexcept
{
	_state = 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <int N>
float	DownsamplerLerp <N>::process_sample_internal (const float * fstb_RESTRICT src_ptr, float &state) noexcept
{
	float          sum = 0;
	for (int k = 0; k < _ratio; ++k)
	{
		const auto     x = src_ptr [k];
		state += x * float (_ratio - k);
		sum   += x * float (         k);
	}
	const auto     y = state * _scale;
	state = sum;

	return y;
}



}  // namespace fir
}  // namespace dsp
}  // namespace mfx



#endif // mfx_dsp_fir_DownsamplerLerp_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
