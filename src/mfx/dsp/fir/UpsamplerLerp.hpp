/*****************************************************************************

        UpsamplerLerp.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_fir_UpsamplerLerp_CODEHEADER_INCLUDED)
#define mfx_dsp_fir_UpsamplerLerp_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace fir
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: process_sample
Description:
	Upsample a single sample to generate N output samples.
Input parameters:
	- x: input sample
Output parameters:
	- dst_ptr: pointer on a buffer receiving the N output samples.
==============================================================================
*/

template <int N>
void	UpsamplerLerp <N>::process_sample (float * fstb_RESTRICT dst_ptr, float x) noexcept
{
	assert (dst_ptr != nullptr);

	process_sample_internal (dst_ptr, x, _state);
}



/*
==============================================================================
Name: process_block
Description:
	Upsamples a block of samples.
	Cannot work in-place.
Input parameters:
	- src_ptr: pointer on a buffer containing the nbr_spl input samples.
	- nbr_spl: number of input samples to process, > 0
Output parameters:
	- dst_ptr: pointer on a buffer receiving the N * nbr_spl output samples.
==============================================================================
*/

template <int N>
void	UpsamplerLerp <N>::process_block (float * fstb_RESTRICT dst_ptr, const float * fstb_RESTRICT src_ptr, int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (dst_ptr != src_ptr);
	assert (nbr_spl > 0);

	auto           cur = _state;
	for (int pos = 0; pos < nbr_spl; ++pos)
	{
		process_sample_internal (dst_ptr, src_ptr [pos], cur);
		dst_ptr += _ratio;
	}
	_state = cur;
}



/*
==============================================================================
Name: clear_buffers
Description:
	Resets the upsampler state
==============================================================================
*/

template <int N>
void	UpsamplerLerp <N>::clear_buffers () noexcept
{
	_state = 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <int N>
void	UpsamplerLerp <N>::process_sample_internal (float * fstb_RESTRICT dst_ptr, float x, float &state) noexcept
{
	const auto     step = (x - state) * _scale;
	for (int k = 0; k < _ratio; ++k)
	{
		dst_ptr [k] = state;
		state      += step;
	}
}



}  // namespace fir
}  // namespace dsp
}  // namespace mfx



#endif // mfx_dsp_fir_UpsamplerLerp_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
