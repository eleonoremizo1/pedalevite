/*****************************************************************************

        UpsamplerLerp.h
        Author: Laurent de Soras, 2024

Upsampler using linear interpolation.
Most appropriate for already oversampled data, or for the highest stages of a
complete oversampler.
Filter is phase-linear.
Group delay is 1 sample at the lowest rate.

Template parameters:

- N: oversampling ratio, > 0

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_fir_UpsamplerLerp_HEADER_INCLUDED)
#define mfx_dsp_fir_UpsamplerLerp_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"



namespace mfx
{
namespace dsp
{
namespace fir
{



template <int N>
class UpsamplerLerp
{
	static_assert (N > 0, "Oversampling ratio must be > 0");

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _ratio = N;

	fstb_FLATINLINE void
	               process_sample (float * fstb_RESTRICT dst_ptr, float x) noexcept;
	void           process_block (float * fstb_RESTRICT dst_ptr, const float * fstb_RESTRICT src_ptr, int nbr_spl) noexcept;

	inline void    clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr float  _scale  = 1.f / float (_ratio);

	static fstb_FORCEINLINE void
	               process_sample_internal (float * fstb_RESTRICT dst_ptr, float x, float &state) noexcept;

	// Last input value
	float          _state = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const UpsamplerLerp <N> &other) const = delete;
	bool           operator != (const UpsamplerLerp <N> &other) const = delete;

}; // class UpsamplerLerp



}  // namespace fir
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/fir/UpsamplerLerp.hpp"



#endif // mfx_dsp_fir_UpsamplerLerp_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
