/*****************************************************************************

        StreamSimple.h
        Author: Laurent de Soras, 2021

Easy sample streaming with variable resampling rate.
Mono only (1 channel), no random access.
Reverse playback not supported (yet?)

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_rspl_StreamSimple_HEADER_INCLUDED)
#define mfx_dsp_rspl_StreamSimple_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/FixedPoint.h"

#include <cstdint>



namespace mfx
{
namespace dsp
{

class SplDataRetrievalInterface;

namespace rspl
{



class InterpolatorInterface;

class StreamSimple
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           set_interpolator (InterpolatorInterface &interp) noexcept;
	const InterpolatorInterface &
	               use_interpolator () const noexcept;
	void           set_sample_freq (double sample_freq, int ovrspl_l2);
	void           set_data (SplDataRetrievalInterface &data_provider, double sample_freq) noexcept;

	void           set_rate (double rate, double rate_step) noexcept;
	void           process_block (float dst_ptr [], int nbr_spl) noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// 0: interpolator not set.
	InterpolatorInterface *	
	               _interp_ptr = nullptr;

	// Impulse length, samples. > 0
	int            _imp_len = 1;

	// Group delay, samples. [0 ; _imp_len - 1]
	fstb::FixedPoint
	               _group_dly { 0, 0 };

	// 0 indicates that the voice is not active
	SplDataRetrievalInterface *
	               _data_provider_ptr = nullptr;

	// Base-2 logarithm of the oversampling. >= 0.
	int            _ovrspl_l2   = 0;

	// Output (possibly oversampled) sample frequency, Hz, > 0.
	double         _sample_freq = 44100;

	// Input sample frequency, Hz, > 0
	double         _in_fs       = 44100;

	// Resampling rate
	fstb::FixedPoint
	               _rate { 1, 0 };

	// Resampling rate increment (per output sample)
	fstb::FixedPoint
	               _rate_step { 0, 0 };

	// Current position in the source, integer part
	int64_t        _pos_int  = 0;

	// Current position in the source, fractionnal part
	uint32_t       _pos_frac = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

#if 0
	               StreamSimple ()                               = delete;
	               StreamSimple (const StreamSimple &other)      = delete;
	               StreamSimple (StreamSimple &&other)           = delete;
	StreamSimple & operator = (const StreamSimple &other)        = delete;
	StreamSimple & operator = (StreamSimple &&other)             = delete;
#endif
	bool           operator == (const StreamSimple &other) const = delete;
	bool           operator != (const StreamSimple &other) const = delete;

}; // class StreamSimple



}  // namespace rspl
}  // namespace dsp
}  // namespace mfx



//#include "mfx/dsp/rspl/StreamSimple.hpp"



#endif   // mfx_dsp_rspl_StreamSimple_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
