/*****************************************************************************

        StreamSimple.cpp
        Author: Laurent de Soras, 2021

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/rspl/InterpolatorInterface.h"
#include "mfx/dsp/rspl/StreamSimple.h"
#include "mfx/dsp/SplDataRetrievalInterface.h"

#include <array>

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace rspl
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_interpolator
Description:
	Associate an interpolator with the streaming object.
	At this point, the interpolator must be configured and its configuration
	should stay constant during the object lifetime.
	Mandatory call before generating audio.
Input parameters:
	- interp: interpolator object to connect.
==============================================================================
*/

void	StreamSimple::set_interpolator (InterpolatorInterface &interp) noexcept
{
	_interp_ptr = &interp;
	_imp_len    = _interp_ptr->get_impulse_len ();
	_group_dly  = _interp_ptr->get_group_delay ();
	_interp_ptr->start (1);
}



/*
==============================================================================
Name: use_interpolator
Description:
	Gives access to the attached interpolator. It should have been associated
	before.
Returns: Interpolator object
==============================================================================
*/

const InterpolatorInterface &	StreamSimple::use_interpolator () const noexcept
{
	assert (_interp_ptr != nullptr);

	return *_interp_ptr;
}



/*
==============================================================================
Name: set_sample_freq
Description:
	Sets the output sample frequency.
	Mandatory call before generating audio.
Input parameters:
	- sample_freq: output sampling rate in Hz, > 0.
		If the process is oversampled, this should be the oversampled rate.
	- ovrspl_l2: base-2 log of the output oversampling rate, if the audio is
		generated at a rate higher than the nominal one and downsampled later.
		This parameter is passed directly to the interpolator.
Throws: depends on the interpolator
==============================================================================
*/

void	StreamSimple::set_sample_freq (double sample_freq, int ovrspl_l2)
{
	assert (_interp_ptr != nullptr);
	assert (sample_freq > 0);
	assert (ovrspl_l2 >= 0);

	_sample_freq = sample_freq;
	_ovrspl_l2   = ovrspl_l2;
	_interp_ptr->set_ovrspl_l2 (_ovrspl_l2);
}



/*
==============================================================================
Name: set_data
Description:
	Attach source data to the streaming object.
	Mandatory call before generating audio.
Input parameters:
	- data_provider: object giving access to the source samples.
	- sample_freq: source sampling rate, in Hz. > 0.
==============================================================================
*/

void	StreamSimple::set_data (SplDataRetrievalInterface &data_provider, double sample_freq) noexcept
{
	assert (sample_freq > 0);

	_data_provider_ptr = &data_provider;
	_in_fs             = sample_freq;
}



/*
==============================================================================
Name: set_rate
Description:
	Sets the playback rate, independently of the source and output sampling
	rate. When set to 1, the sample is played at normal speed.
	It is possible to specify a per-sample rate increment for sample-accurate
	rate ramps.
	In case of ramp, the actual rate is modified between all output samples
	until rate_step is changed again.
	Make sure to keep the rate change under control, so it never goes below 0.
Input parameters:
	- rate: playback rate, at the beginning of the block. >= 0.
	- rate_step: per sample increment to the rate.
==============================================================================
*/

void	StreamSimple::set_rate (double rate, double rate_step) noexcept
{
	assert (rate >= 0);

	const auto       rate_scale = _in_fs / _sample_freq;
	_rate.set_val (rate * rate_scale);
	_rate_step.set_val (rate_step * rate_scale);
}



/*
==============================================================================
Name: process_block
Description:
	Resamples a block of samples from the source.
	Positions and rates are automatically updated.
Input parameters:
	- nbr_spl: number of output samples to generate. > 0.
Output parameters:
	- dst_ptr: pointer to the beginning of a buffer where the output samples
		will be stored.
==============================================================================
*/

void	StreamSimple::process_block (float dst_ptr [], int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (nbr_spl > 0);
	assert ((_rate + _rate_step * nbr_spl).get_int_val () >= 0);

	// Computes the required source length for the whole block
	auto           src_end_pos =
		  _interp_ptr->integrate_rate (nbr_spl, _rate, _rate_step)
		+ fstb::FixedPoint (0, _pos_frac)
		- _group_dly;
	auto           req_len =
		_group_dly.get_ceil () + src_end_pos.get_ceil () + _imp_len - 1;

	constexpr int  buf_len = 1024;
	std::array <float, buf_len>   buf;
	float *        buf_ptr = buf.data ();

	int            pos_dst = 0;
	do
	{
		// Relative to the _pos_int
		const auto     pos_src = fstb::FixedPoint (0, _pos_frac);

		// Gets the source data
		const auto     buf_beg   = pos_src - _group_dly;
		const auto     buf_beg_i = buf_beg.get_int_val ();
		const auto     buf_beg_r = fstb::FixedPoint (0, buf_beg.get_frac_val ());
		const int      work_len_src = std::min (req_len, int (buf_len));
		_data_provider_ptr->get_data (
			&buf_ptr, _pos_int + buf_beg_i, work_len_src, false
		);

		// Resamples as much data as possible
		const auto     work_len_dst = _interp_ptr->process_block (
			&dst_ptr, &buf_ptr,
			pos_dst, buf_beg_r, nbr_spl,
			0, work_len_src,
			_rate, _rate_step
		);

		// Next position
		src_end_pos  =
			  _interp_ptr->integrate_rate (work_len_dst, _rate, _rate_step)
			+ pos_src;
		const auto      advance = src_end_pos.get_int_val ();
		req_len  -= advance;
		_pos_int += advance;
		_pos_frac = src_end_pos.get_frac_val ();
		_rate    += _rate_step * work_len_dst;
		pos_dst  += work_len_dst;
	}
	while (pos_dst < nbr_spl);
}



/*
==============================================================================
Name: clear_buffers
Description:
	Resets the playback position to 0.
	Rate parameters are not affected.
==============================================================================
*/

void	StreamSimple::clear_buffers () noexcept
{
	_pos_int  = 0;
	_pos_frac = 0;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace rspl
}  // namespace dsp
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
