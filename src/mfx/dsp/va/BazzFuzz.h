/*****************************************************************************

        BazzFuzz.h
        Author: Laurent de Soras, 2024

Bazz Fuzz - ultra-simple fuzz circuit
https://home-wrecker.com/bazz.html

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_va_BazzFuzz_HEADER_INCLUDED)
#define mfx_dsp_va_BazzFuzz_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/va/dkm/Simulator.h"



namespace mfx
{
namespace dsp
{
namespace va
{



class BazzFuzz
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	void           set_sample_freq (double sample_freq);
	void           clear_buffers () noexcept;
	float          process_sample (float x) noexcept;
	void           process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef dkm::Simulator Simul;

	static constexpr int _max_it = 20;

	void           setup_circuit ();

	Simul          _sim;
	int            _idx_in  = -1;
	int            _idx_out = -1;
	bool           _constructed_flag = false;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

#if 0
	               BazzFuzz ()                               = delete;
	               ~BazzFuzz ()                              = delete;
	               BazzFuzz (const BazzFuzz &other)          = delete;
	               BazzFuzz (BazzFuzz &&other)               = delete;
	BazzFuzz &     operator = (const BazzFuzz &other)        = delete;
	BazzFuzz &     operator = (BazzFuzz &&other)             = delete;
#endif
	bool           operator == (const BazzFuzz &other) const = delete;
	bool           operator != (const BazzFuzz &other) const = delete;

}; // class BazzFuzz



}  // namespace va
}  // namespace dsp
}  // namespace mfx



//#include "mfx/dsp/va/BazzFuzz.hpp"



#endif   // mfx_dsp_va_BazzFuzz_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
