/*****************************************************************************

        BazzFuzz.cpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/va/BazzFuzz.h"

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace va
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	BazzFuzz::set_sample_freq (double sample_freq)
{
	assert (sample_freq > 0);

	if (_constructed_flag)
	{
		_sim.update_sample_freq (sample_freq);
	}
	else
	{
		setup_circuit ();

		_sim.prepare (sample_freq);
		/*** To do: check if reordering the jacobian can be useful ***/

		clear_buffers ();

		_constructed_flag = true;
	}
}



void	BazzFuzz::clear_buffers () noexcept
{
	_sim.clear_buffers ();
}



float	BazzFuzz::process_sample (float x) noexcept
{
	assert (_constructed_flag);

	_sim.set_src_v (_idx_in, x);
	_sim.process_sample ();
	x = float (_sim.get_output (_idx_out));

	return x;
}



void	BazzFuzz::process_block (float dst_ptr [], const float src_ptr [], int nbr_spl) noexcept
{
	assert (dst_ptr != nullptr);
	assert (src_ptr != nullptr);
	assert (nbr_spl > 0);

	for (int pos = 0; pos < nbr_spl; ++pos)
	{
		dst_ptr [pos] = process_sample (src_ptr [pos]);
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
Here we don't even simulate the potentiometer. We assume it is always set
to the max.
Emitter resistor added to avoid degenerated cases.

     VCC ---/\/\/\---.
                     |
                     +---||---.
           +---|<|---+        |
           |         |C       |
           |        |/        ><--- Out
In ---||---+--------|         <
                  B |\        >
                      V       |
                      |       |
                      >       |
                      <       |
                      >       |
                      |       |
                     GND------'
*/

void	BazzFuzz::setup_circuit ()
{
	// Node identifiers
	enum Node : Simul::IdNode
	{
		nid_vcc = 1,
		nid_src,
		nid_dst,
		nid_b,
		nid_c,
		nid_e
	};
	const auto     nid_gnd = Simul::_nid_gnd;

	// Default values
	constexpr float   v_psu = 9.0f;
	constexpr float   r_col = 10e3f;
	constexpr float   r_e   = 10.0f;
	constexpr float   c_in  = 4.7e-6f;
	constexpr float   c_out = 100e-9f;
	constexpr float   r_out = 100e3f;

	// Circuit
	_sim.add_resistor (nid_e, nid_gnd, r_e);
	_sim.add_resistor (nid_vcc, nid_c, r_col);
	_sim.add_resistor (nid_dst, nid_gnd, r_out);
	_sim.add_capacitor (nid_src, nid_b, c_in);
	_sim.add_capacitor (nid_c, nid_dst, c_out);
//	_sim.add_bjt_npn (nid_e, nid_b, nid_c, 5.911e-15f, 1.f, 1122.f, 1.271f); // 2N5089
	_sim.add_bjt_npn (nid_e, nid_b, nid_c, 360e-15f, 1.f, 337.f, 4.f); // MPSA13
	_sim.add_diode (nid_c, nid_b, 4.352e-9f, 1.906f); // 1N914

	// Misc setup
	_sim.set_max_nbr_it (_max_it);
	_sim.add_src_v (nid_vcc, nid_gnd, v_psu);
	_idx_in  = _sim.add_src_v (nid_src, nid_gnd, 0.f);
	_idx_out = _sim.add_output (nid_dst, nid_gnd);
}



}  // namespace va
}  // namespace dsp
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
