/*****************************************************************************

        OscTri.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/ctrl/lfo/OscTri.h"

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace ctrl
{
namespace lfo
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	OscTri::do_set_sample_freq (double sample_freq) noexcept
{
	_phase_gen.set_sample_freq (sample_freq);
}



void	OscTri::do_set_period (double per) noexcept
{
	_phase_gen.set_period (per);
}



void	OscTri::do_set_phase (double phase) noexcept
{
	_phase_gen.set_phase (phase);
}



void	OscTri::do_set_chaos (double chaos) noexcept
{
	_phase_gen.set_chaos (chaos);
}



void	OscTri::do_set_phase_dist (double dist) noexcept
{
	_phase_dist.set_phase_dist (dist);
}



void	OscTri::do_set_phase_dist_offset (double ofs) noexcept
{
	_phase_dist.set_phase_dist_offset (ofs);
}



void	OscTri::do_set_sign (bool inv_flag) noexcept
{
	_inv_flag = inv_flag;
}



void	OscTri::do_set_polarity (bool unipolar_flag) noexcept
{
	_unipolar_flag = unipolar_flag;
}



void	OscTri::do_set_variation (int /*param*/, double /*val*/) noexcept
{
	// Nothing
}



bool	OscTri::do_is_using_variation (int /*param*/) const noexcept
{
	return false;
}



void	OscTri::do_tick (int nbr_spl) noexcept
{
	_phase_gen.tick (nbr_spl);
}



double	OscTri::do_get_val () const noexcept
{
	double         val = _phase_dist.process_phase (_phase_gen.get_phase ());
	val = fabs (val - 0.5f); // 0.5 ... 0 ... 0.5

	if (_inv_flag)
	{
		val = (_unipolar_flag) ?     val * 2 : val * 4 - 1;
	}
	else
	{
		val = (_unipolar_flag) ? 1 - val * 2 : 1 - val * 4;
	}

	return val;
}



double	OscTri::do_get_phase () const noexcept
{
	return _phase_gen.get_phase ();
}



void	OscTri::do_clear_buffers () noexcept
{
	_phase_gen.clear_buffers ();
}



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace lfo
}  // namespace ctrl
}  // namespace dsp
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
