/*****************************************************************************

        OscParabola.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/ctrl/lfo/OscParabola.h"

#include <cassert>
#include <cmath>



namespace mfx
{
namespace dsp
{
namespace ctrl
{
namespace lfo
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



void	OscParabola::do_set_sample_freq (double sample_freq) noexcept
{
	_phase_gen.set_sample_freq (sample_freq);
}



void	OscParabola::do_set_period (double per) noexcept
{
	_phase_gen.set_period (per);
}



void	OscParabola::do_set_phase (double phase) noexcept
{
	_phase_gen.set_phase (phase);
}



void	OscParabola::do_set_chaos (double chaos) noexcept
{
	_phase_gen.set_chaos (chaos);
}



void	OscParabola::do_set_phase_dist (double dist) noexcept
{
	_phase_dist.set_phase_dist (dist);
}



void	OscParabola::do_set_phase_dist_offset (double ofs) noexcept
{
	_phase_dist.set_phase_dist_offset (ofs);
}



void	OscParabola::do_set_sign (bool inv_flag) noexcept
{
	_inv_flag = inv_flag;
}



void	OscParabola::do_set_polarity (bool unipolar_flag) noexcept
{
	_unipolar_flag = unipolar_flag;
}



void	OscParabola::do_set_variation (int /*param*/, double /*val*/) noexcept
{
	// Nothing
}



bool	OscParabola::do_is_using_variation (int /*param*/) const noexcept
{
	return false;
}



void	OscParabola::do_tick (int nbr_spl) noexcept
{
	_phase_gen.tick (nbr_spl);
}



double	OscParabola::do_get_val () const noexcept
{
	const float    phase =
		float (_phase_dist.process_phase (_phase_gen.get_phase ()));
	float          val   = phase * 2 - 1;
	val = 1 - val * val * 2;
	if (_inv_flag)
	{
		val = -val;
	}
	if (_unipolar_flag)
	{
		val = (val + 1) * 0.5f;
	}

	return val;
}



double	OscParabola::do_get_phase () const noexcept
{
	return _phase_gen.get_phase ();
}



void	OscParabola::do_clear_buffers () noexcept
{
	_phase_gen.clear_buffers ();
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace lfo
}  // namespace ctrl
}  // namespace dsp
}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
