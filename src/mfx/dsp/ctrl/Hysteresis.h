/*****************************************************************************

        Hysteresis.h
        Author: Laurent de Soras, 2023

Soft hysteresis effect, without hard step when the slope of the value
changes of direction, but with the total range reduced at the top
from the hysteresis value. This has to be handled by the caller.

Template parameter:

- T: contained data type. Should have numerical type semantics.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_ctrl_Hysteresis_HEADER_INCLUDED)
#define mfx_dsp_ctrl_Hysteresis_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace dsp
{
namespace ctrl
{



template <typename T>
class Hysteresis
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	inline void    set_hyst (T hyst) noexcept;
	inline T       get_hyst () const noexcept;
	inline T       process_sample (T v) noexcept;
	inline void    force_val (T v) noexcept;
	inline T       get_val () const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Last output value
	T              _val     = T (0);

	// Hysteresis amount
	T              _hyst    = T (0);



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const Hysteresis &other) const = delete;
	bool           operator != (const Hysteresis &other) const = delete;

}; // class Hysteresis



}  // namespace ctrl
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/ctrl/Hysteresis.hpp"



#endif // mfx_dsp_ctrl_Hysteresis_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
