/*****************************************************************************

        Hysteresis.hpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_dsp_ctrl_Hysteresis_CODEHEADER_INCLUDED)
#define mfx_dsp_ctrl_Hysteresis_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace ctrl
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: set_hyst
Description:
	Sets the hysteresis value. When the input value oscillates in this range,
	the output stays constant.
	Default is 0.
Input parameters:
	- hyst: hysteresis amount, >= 0.
==============================================================================
*/

template <typename T>
void	Hysteresis <T>::set_hyst (T hyst) noexcept
{
	assert (hyst >= 0);

	_hyst = hyst;
}



/*
==============================================================================
Name: get_hyst
Returns: Current hysteresis amount, >= 0.
==============================================================================
*/

template <typename T>
T	Hysteresis <T>::get_hyst () const noexcept
{
	return _hyst;
}



/*
==============================================================================
Name: process_sample
Description:
	Pass the input value through the hysteresis.
Input parameters:
	- v: input value
Returns: processed value
==============================================================================
*/

template <typename T>
T	Hysteresis <T>::process_sample (T v) noexcept
{
	if (v < _val)
	{
		_val = v;
	}
	else
	{
		const auto     v_bot = v - _hyst;
		if (v_bot > _val)
		{
			_val = v_bot;
		}
	}

	return _val;
}



/*
==============================================================================
Name: force_val
Description:
	Sets a value forming immediately the output value.
Input parameters:
	- v: the value
==============================================================================
*/

template <typename T>
void	Hysteresis <T>::force_val (T v) noexcept
{
	_val = v;
}



/*
==============================================================================
Name: get_val
Returns: latest output value, or 0 if it has never been set.
==============================================================================
*/

template <typename T>
T	Hysteresis <T>::get_val () const noexcept
{
	return _val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace ctrl
}  // namespace dsp
}  // namespace mfx



#endif   // mfx_dsp_ctrl_Hysteresis_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
