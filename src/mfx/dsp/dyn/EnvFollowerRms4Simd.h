/*****************************************************************************

        EnvFollowerRms4Simd.h
        Author: Laurent de Soras, 2023

This class can be inherited but is not polymorph.

Template parameters:

- VD: class writing and reading memory with SIMD vectors (destination access).
	Typically, the fstb::DataAlign classes for aligned and unaligned data.
	Requires:
	static bool VD::check_ptr (const void *ptr) noexcept;
	static fstb::Vf32 VD::load_f32 (const void *ptr) noexcept;
	static void VD::store_f32 (void *ptr, const fstb::Vf32 val) noexcept;

- VS: same as VD, but for reading only (source access)
	Requires:
	static bool VS::check_ptr (const void *ptr) noexcept;
	static fstb::Vf32 VS::load_f32 (const void *ptr) noexcept;

- VP: same as VD, but for parametering and internal data.
	Requires: same as VD.

- ORD: filter order. In [1 ; 8]

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_dyn_EnvFollowerRms4Simd_HEADER_INCLUDED)
#define mfx_dsp_dyn_EnvFollowerRms4Simd_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/fnc.h"
#include "fstb/Vf32.h"
#include "mfx/dsp/dyn/EnvFollowerAR4SimdHelper.h"

#include <array>



namespace mfx
{
namespace dsp
{
namespace dyn
{



template <class VD, class VS, class VP, int ORD>
class EnvFollowerRms4Simd
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	static constexpr int _nbr_units_l2 = fstb::Vf32::_len_l2;
	static constexpr int _nbr_units    = fstb::Vf32::_length;

	typedef VD V128Dest;
	typedef VS V128Src;
	typedef VP V128Par;

	               EnvFollowerRms4Simd ();
	               EnvFollowerRms4Simd (const EnvFollowerRms4Simd &other) = default;
	               EnvFollowerRms4Simd (EnvFollowerRms4Simd &&other) = default;
	               ~EnvFollowerRms4Simd ()                       = default;
	EnvFollowerRms4Simd &
	               operator = (const EnvFollowerRms4Simd &other) = default;
	EnvFollowerRms4Simd &
	               operator = (EnvFollowerRms4Simd &&other)      = default;

	void           set_sample_freq (double fs) noexcept;
	void           set_times (fstb::Vf32 at, fstb::Vf32 rt) noexcept;
	void           set_times_same (float at, float rt) noexcept;
	void           set_times_one (int index, float at, float rt) noexcept;

	fstb_FORCEINLINE fstb::Vf32
	               process_sample (fstb::Vf32 x) noexcept;
	fstb_FORCEINLINE fstb::Vf32
	               process_sample_no_sqrt (fstb::Vf32 x) noexcept;

	void           process_block (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	void           process_block_no_sqrt (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	void           process_block_raw (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	fstb::Vf32     analyse_block (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	fstb::Vf32     analyse_block_no_sqrt (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	fstb::Vf32     analyse_block_raw (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept;
	inline fstb::Vf32
	               get_state_no_sqrt () const noexcept;

	void           clear_buffers () noexcept;

	inline void    set_state (fstb::Vf32 lvl_sq) noexcept;
	inline void    apply_volume (fstb::Vf32 gain) noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	typedef EnvFollowerAR4SimdHelper <VD, VS, VP, ORD> Helper;
	typedef std::array <float, _nbr_units> DataVec;

	void           update_parameters () noexcept;

	Helper         _h; // State = squared result

	alignas (fstb_SIMD128_ALIGN) DataVec
	               _time_a      = {};     // s, >= 0
	alignas (fstb_SIMD128_ALIGN) DataVec
	               _time_r      = {};     // s, >= 0

	float          _sample_freq = 44100;  // Hz, > 0



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const EnvFollowerRms4Simd &other) const = delete;
	bool           operator != (const EnvFollowerRms4Simd &other) const = delete;

}; // class EnvFollowerRms4Simd



}  // namespace dyn
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/dyn/EnvFollowerRms4Simd.hpp"



#endif   // mfx_dsp_dyn_EnvFollowerRms4Simd_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
