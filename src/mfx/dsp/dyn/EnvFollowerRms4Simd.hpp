/*****************************************************************************

        EnvFollowerRms4Simd.hpp
        Author: Laurent de Soras, 2023

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (mfx_dsp_dyn_EnvFollowerRms4Simd_CODEHEADER_INCLUDED)
#define mfx_dsp_dyn_EnvFollowerRms4Simd_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "mfx/dsp/dyn/EnvHelper.h"

#include <cassert>



namespace mfx
{
namespace dsp
{
namespace dyn
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <class VD, class VS, class VP, int ORD>
EnvFollowerRms4Simd <VD, VS, VP, ORD>::EnvFollowerRms4Simd ()
{
	set_times_same (0.001f, 0.100f);
	clear_buffers ();
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::set_sample_freq (double fs) noexcept
{
	assert (fs > 0);

	_sample_freq = float (fs);
	update_parameters ();
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::set_times (fstb::Vf32 at, fstb::Vf32 rt) noexcept
{
	assert (at >= fstb::Vf32 (0));
	assert (rt >= fstb::Vf32 (0));

	V128Par::store_f32 (_time_a.data (), at);
	V128Par::store_f32 (_time_r.data (), rt);
	update_parameters ();
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::set_times_same (float at, float rt) noexcept
{
	set_times (fstb::Vf32 (at), fstb::Vf32 (rt));
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::set_times_one (int index, float at, float rt) noexcept
{
	assert (index >= 0);
	assert (index < _nbr_units);
	assert (at >= 0);
	assert (rt >= 0);

	_time_a [index] = at;
	_time_r [index] = rt;
	update_parameters ();
}



template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::process_sample (fstb::Vf32 x) noexcept
{
	const auto     y2 = process_sample_no_sqrt (x);
	const auto     y  = y2.sqrt_approx ();

	return y;
}



template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::process_sample_no_sqrt (fstb::Vf32 x) noexcept
{
	return _h.process_sample (x * x);
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::process_block (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	using namespace envfollowerar4simdhelper;
	_h.process_block (dst_ptr, src_ptr, nbr_spl, opv_r, opv_s);
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::process_block_no_sqrt (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	using namespace envfollowerar4simdhelper;
	_h.process_block (dst_ptr, src_ptr, nbr_spl, opv_b, opv_s);
}



// Input is not squared
// Output is not square-rooted
template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::process_block_raw (fstb::Vf32 dst_ptr [], const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	_h.process_block (dst_ptr, src_ptr, nbr_spl);
}



template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::analyse_block (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	using namespace envfollowerar4simdhelper;
	return _h.analyse_block (src_ptr, nbr_spl, opv_r, opv_s);
}



template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::analyse_block_no_sqrt (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	using namespace envfollowerar4simdhelper;
	return _h.analyse_block (src_ptr, nbr_spl, opv_b, opv_s);
}



// Input is not squared
// Output is not square-rooted
template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::analyse_block_raw (const fstb::Vf32 src_ptr [], int nbr_spl) noexcept
{
	return _h.analyse_block (src_ptr, nbr_spl);
}



template <class VD, class VS, class VP, int ORD>
fstb::Vf32	EnvFollowerRms4Simd <VD, VS, VP, ORD>::get_state_no_sqrt () const noexcept
{
	return _h.get_state ();
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::clear_buffers () noexcept
{
	_h.clear_buffers ();
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::set_state (fstb::Vf32 lvl_sq) noexcept
{
	_h.set_state (lvl_sq);
}



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::apply_volume (fstb::Vf32 gain) noexcept
{
	assert (gain >= fstb::Vf32::zero ());

	auto           s = _h.get_state ();
	s *= gain * gain;
	_h.set_state (s);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <class VD, class VS, class VP, int ORD>
void	EnvFollowerRms4Simd <VD, VS, VP, ORD>::update_parameters () noexcept
{
	const auto     fs  = fstb::Vf32 (_sample_freq);
	const auto     ta  = V128Par::load_f32 (_time_a.data ());
	const auto     tr  = V128Par::load_f32 (_time_r.data ());
	const auto     tac = EnvHelper::compensate_order (ta, ORD);
	const auto     trc = EnvHelper::compensate_order (tr, ORD);
	const auto     ca  = EnvHelper::compute_env_coef_simple (tac, fs);
	const auto     cr  = EnvHelper::compute_env_coef_simple (trc, fs);
	_h.set_atk_coef (ca);
	_h.set_rls_coef (cr);
}



}  // namespace dyn
}  // namespace dsp
}  // namespace mfx



#endif   // mfx_dsp_dyn_EnvFollowerRms4Simd_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
