## shape — waveshapers, look-up table functions and distortions

- **`Dist*`**: State-full distortions
- **`FncScale`**: Scales a function, keeping the same slope at 0.
- **`FncLin0`**: Inserts a linear segment at 0 in an existing shaper
- **`HardclipBl`**: Band-limited hard-clipping.
- **`MapSaturate`**: Convenient, reciprocal-based shaping function to saturate or desaturate a positive signal. Curvature is a template parameter. Great for remapping the range of a parameter for presentation to the user.
- **`MapSaturateBipolar`**: Same as `MapSaturate`, but symmetric.
- **`Piecewise2dPoly3`**: Look-up table to interpolate any 2D-function with regular spacing
- **`PiecewisePoly3`**: Look-up table to interpolate any function with regular spacing
- **`PiecewisePoly3Adaa`**: Same as `PiecewisePoly3`, using 2 ADAA methods to reduce aliasing
- **`Poly3L1Bias`**: Simple and fast asymmetric shaper and clipping function. Intended to be used within the feedback loop of a filter.
- **`SigmoidAdaa`**: Soft clipping curve using two kinds of ADAA.
- **`SineQPoly3`**: Sine-like shape in [0 ; π/2].
- **`SlewRateLimiter`**: Simple slew-rate limiter.
- **`Ws*`**: State-less waveshaper. Functors or block processing functions.
- **`WsAsym1*`**: asymmetric, bounded, monotonic, C1 continuous shaping functors. Unitary slope at (0, 0). [Graphs](https://www.desmos.com/calculator/vdcy3npihw)
