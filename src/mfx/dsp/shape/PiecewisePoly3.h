/*****************************************************************************

        PiecewisePoly3.h
        Author: Laurent de Soras, 2016

Approximates a function using piecewise 3rd-order polynomials.
This class is actually stateless.

Template parameters:

- BL: Lower bound for the function definition

- BU: Upper bound

- GF: functor of the interpolated function. Requires:
	GF::GF () noexcept;
	double GF::operator () (double x) const noexcept;

- RES: internal resolution (number of segments per input unit), > 0

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_shape_PiecewisePoly3_HEADER_INCLUDED)
#define mfx_dsp_shape_PiecewisePoly3_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Vf32.h"

#include <array>



namespace mfx
{
namespace dsp
{
namespace shape
{



template <int BL, int BU, class GF, int RES = 1>
class PiecewisePoly3
{

	static_assert (BL < BU, "");
	static_assert (RES >= 1, "");

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               PiecewisePoly3 () noexcept;
	               PiecewisePoly3 (const PiecewisePoly3 <BL, BU, GF, RES> &other) = default;
	               PiecewisePoly3 (PiecewisePoly3 <BL, BU, GF, RES> &&other) = default;
	virtual        ~PiecewisePoly3 () = default;
	PiecewisePoly3 <BL, BU, GF, RES> &
	               operator = (const PiecewisePoly3 <BL, BU, GF, RES> &other) = default;
	PiecewisePoly3 <BL, BU, GF, RES> &
	               operator = (PiecewisePoly3 <BL, BU, GF, RES> &&other) = default;

	inline float   operator () (float x) const noexcept;
	inline float   process_sample (float x) const noexcept;
	inline fstb::Vf32
	               operator () (fstb::Vf32 x) const noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	static constexpr int _table_size = (BU - BL) * RES;
	static constexpr int _order      = 3;
	static constexpr int _prec_frac  = 1024 * RES;

	typedef std::array <float, _order + 1> Curve;
	typedef std::array <Curve, _table_size + 1> CurveTable;	// +1 to secure rounding errors

	static void    init_coefs () noexcept;

	static CurveTable
	               _coef_arr;
	static float   _val_min;
	static float   _val_max;
	static bool    _init_flag;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PiecewisePoly3 <BL, BU, GF, RES> &other) const = delete;
	bool           operator != (const PiecewisePoly3 <BL, BU, GF, RES> &other) const = delete;

}; // class PiecewisePoly3



}  // namespace shape
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/shape/PiecewisePoly3.hpp"



#endif   // mfx_dsp_shape_PiecewisePoly3_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
