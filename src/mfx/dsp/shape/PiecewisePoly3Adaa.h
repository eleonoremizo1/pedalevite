/*****************************************************************************

        PiecewisePoly3Adaa.h
        Author: Laurent de Soras, 2024

Approximates a function using piecewise 3rd-order polynomials and anti-
derivative anti-aliasing (ADAA).

Two 1st-order ADAA methods are available.

As side effect, the process induces a delay (1/2 or 1 sample) and an
attenuation on high frequencies.

High frequencies could be partially restored with a 1st-order IIR filter
applied on the input:
	y = (1 + k) * x - k * y_old
with 0 <= k < 1. High k means better restoration but higher aliasing.

Template parameters:

- BL: Lower bound for the function definition

- BU: Upper bound

- GF: functor of the interpolated function. Requires:
	GF::GF () noexcept;
	double GF::operator () (double x) const noexcept;

- RES: internal resolution (number of segments per input unit), > 0

- UD: enables unit delay latency
	false: standard 1st order ADAA, 1/2 sample delay, -oo dB at Nyquist
		To restore high frequencies if required, set k between 0.25 and 0.5
	true : Vicanek  1st order ADAA, 1   sample delay,  -6 dB at Nyquist
		Use 2 IIR filters, one on the input and the other one on the output,
		both with k = 1 / (3 + sqrt (8)).
		Processing is slower than standard ADAA.

References:

Julian D. Parker, Vadim Zavalishin, Efflam Le Bivic,
Reducing the Aliasing of Nonlinear Waveshaping Using Continuous-Time
Convolution, Proceedings of the 19th International Conference on Digital Audio
Effects (DAFx-16), 2016, pp. 137-144

Martin Vicanek, Note on Alias Suppression in Digital Distortion,
https://vicanek.de/articles/AADistortion.pdf, 2023-11

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_shape_PiecewisePoly3Adaa_HEADER_INCLUDED)
#define mfx_dsp_shape_PiecewisePoly3Adaa_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <array>



namespace mfx
{
namespace dsp
{
namespace shape
{



template <int BL, int BU, class GF, int RES = 1, bool UD = false>
class PiecewisePoly3Adaa
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	               PiecewisePoly3Adaa () noexcept;
	               PiecewisePoly3Adaa (const PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &other) = default;
	               PiecewisePoly3Adaa (PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &&other) = default;

	PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &
	               operator = (const PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &other) = default;
	PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &
	               operator = (PiecewisePoly3Adaa <BL, BU, GF, RES, UD> &&other) = default;

	inline float   process_sample (float x) noexcept;
	void           clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	// Number of segments. +1 because rounding errors may reference one
	// segment past the minimal table.
	static constexpr int _table_size = (BU - BL) * RES + 1;

	// Polynomial order, not to be changed.
	static constexpr int _order      = 3;

	// Divider of the delta to compute the derivative of the original function
	static constexpr int _prec_frac  = RES << 10;

	// Delta threshold below which we use direct calculation.
	// Relative to the input sample values
	static constexpr float  _thr_rel = 1e-4f;

	// Same, but absolute threshold
	static constexpr float  _thr_abs = 1e-20f;

	class Segment
	{
	public:

		// Approximation polynomial, coefficient orders from 0 to 3
		// Input values are scaled by RES, ranging in [0 ; 1] for each segment.
		std::array <float, _order + 1>
		               _poly_std;

		// Integral of the approximation polynomial (F), coefficient orders
		// from 0 to 4.
		// Input values are scaled by RES, ranging in [0 ; 1] for each segment.
		std::array <float, _order + 2>
		               _poly_itg;
	};

	typedef std::array <Segment, _table_size> SegmentTable;

	inline float   process_sample_std (float x) noexcept;
	inline float   process_sample_vic (float x) noexcept;

	static inline float
	               compute_integ (float &x) noexcept;
	static inline bool
	               is_under_threshold (float &delta, float x0, float x1) noexcept;
	static inline float
	               eval_direct (float x) noexcept;
	static void    init_coefs () noexcept;

	// Previous input value
	float          _xz  = 0;

	// Previous F value
	float          _fz  = 0;

	// Vicanek-specific states

	// F value with a 3/2 sample delay
	float          _fzh = 0;

	// x at n-2
	float          _xz2 = 0;

	// 0.5 * previous direct evaluation at n-1
	float          _yz  = 0;

	static SegmentTable
	               _seg_arr;

	// Direct output values for the min/max inputs
	static float   _val_min;
	static float   _val_max;

	// Integrated output values for the min/max inputs
	static float   _itg_min;
	static float   _itg_max;

	// true when the static data is filled.
	static bool    _init_flag;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const PiecewisePoly3Adaa &other) const = delete;
	bool           operator != (const PiecewisePoly3Adaa &other) const = delete;

}; // class PiecewisePoly3Adaa



}  // namespace shape
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/shape/PiecewisePoly3Adaa.hpp"



#endif // mfx_dsp_shape_PiecewisePoly3Adaa_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
