/*****************************************************************************

        SigmoidAdaa.h
        Author: Laurent de Soras, 2024

Soft-clipping sigmoid f (x) = x / sqrt (1 + x^2) with anti-derivative anti-
aliasing (ADAA). This function is quite fast to compute with no check needed
for numerical stability (constant execution time).

Two 1st-order ADAA methods are available.

As side effect, the process induces a delay (1/2 or 1 sample) and an
attenuation on high frequencies.

High frequencies could be partially restored with a 1st-order IIR filter
applied on the input:
	y [n] = (1 + k) * x [n] - k * y [n-1]
with 0 <= k < 1. High k means better restoration but higher aliasing.

Template parameters:

- UD: enables unit delay latency
	false: standard 1st order ADAA, 1/2 sample delay, -oo dB at Nyquist
		To restore high frequencies if required, set k between 0.25 and 0.5
	true : Vicanek  1st order ADAA, 1   sample delay,  -6 dB at Nyquist
		Use 2 IIR filters, one on the input and the other one on the output,
		both with k = 1 / (3 + sqrt (8)).
		Processing is slower than standard ADAA.

Reference:

Martin Vicanek, Note on Alias Suppression in Digital Distortion,
https://vicanek.de/articles/AADistortion.pdf, 2023-11

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_shape_SigmoidAdaa_HEADER_INCLUDED)
#define mfx_dsp_shape_SigmoidAdaa_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace mfx
{
namespace dsp
{
namespace shape
{



template <bool UD = false>
class SigmoidAdaa
{

/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

public:

	inline float     operator () (float x) noexcept;
	inline float     process_sample (float x) noexcept;
	void             clear_buffers () noexcept;



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

protected:



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	inline float     process_sample_std (float x) noexcept;
	inline float     process_sample_vic (float x) noexcept;

	// Previous input value
	float          _xz  = 0;

	// Previous F value
	float          _fz  = 0;

	// Vicanek-specific states

	// Input value at n-2
	float          _xz2 = 0;

	// F value with a 3/2 sample delay
	float          _fzh = 0;



/*\\\ FORBIDDEN MEMBER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

private:

	bool           operator == (const SigmoidAdaa &other) const = delete;
	bool           operator != (const SigmoidAdaa &other) const = delete;

}; // class SigmoidAdaa



}  // namespace shape
}  // namespace dsp
}  // namespace mfx



#include "mfx/dsp/shape/SigmoidAdaa.hpp"



#endif // mfx_dsp_shape_SigmoidAdaa_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
