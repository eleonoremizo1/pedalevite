/*****************************************************************************

        PiecewisePoly3Adaa.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_shape_PiecewisePoly3Adaa_CODEHEADER_INCLUDED)
#define mfx_dsp_shape_PiecewisePoly3Adaa_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "fstb/Poly.h"

#include <cassert>
#include <cmath>



namespace mfx
{
namespace dsp
{
namespace shape
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
==============================================================================
Name: ctor
==============================================================================
*/

template <int BL, int BU, class GF, int RES, bool UD>
PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::PiecewisePoly3Adaa () noexcept
{
	if (! _init_flag)
	{
		init_coefs ();
	}
}



/*
==============================================================================
Name: process_sample
Description:
	Waveshapes a single sample with ADAA processing.
	Side effects: adds a 1/2 sample delay and attenuates the high frequencies.
Input parameters:
	- x: input sample
Returns: the shaped sample.
==============================================================================
*/

template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::process_sample (float x) noexcept
{
	assert (_init_flag);

	if constexpr (UD)
	{
		return process_sample_vic (x);
	}
	else
	{
		return process_sample_std (x);
	}
}



/*
==============================================================================
Name: clear_buffers
Description: Resets the ADAA state.
==============================================================================
*/

template <int BL, int BU, class GF, int RES, bool UD>
void	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::clear_buffers () noexcept
{
	_xz  = 0;
	_xz2 = 0;
	if constexpr (UD)
	{
		_fz  = 0;
		_fzh = 0;
		_yz  = 0;
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::process_sample_std (float x) noexcept
{
	assert (_init_flag);

	const float    f = compute_integ (x);
	float          y = 0;

	float          delta = 0;
	if (is_under_threshold (delta, x, _xz))
	{
		// When delta is small, calculation errors will be too high.
		// So we have to compute f (xh) instead.
		const auto     xh = (x + _xz) * 0.5f;
		y = eval_direct (xh);
	}

	else
	{
		assert (delta != 0);
		y = (f - _fz) / delta;
	}

	_xz  = x;
	_fz  = f;

	return y;
}



template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::process_sample_vic (float x) noexcept
{
	assert (_init_flag);

	const float    f  = compute_integ (x);
	float          xh = (x + _xz) * 0.5f;
	const float    fh = compute_integ (xh);
	float          y  = 0;

	float          delta2 = 0;
	if (is_under_threshold (delta2, _xz, _xz2))
	{
		y = _yz;
	}
	else
	{
		assert (delta2 != 0);
		y = (_fz - _fzh) / delta2;
	}

	float          delta1 = 0;
	if (is_under_threshold (delta1, x, _xz))
	{
		_yz = eval_direct (_xz) * 0.5f;
		y += _yz;
	}
	else
	{
		assert (delta1 != 0);
		y += (fh - _fz) / delta1;
	}

	_xz2 = _xz;
	_fzh = fh;
	_xz  = x;
	_fz  = f;

	return y;
}



template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::compute_integ (float &x) noexcept
{
	float          f = 0;
	if (x <= BL)
	{
		x = BL;
		f = _itg_min;
	}
	else if (x >= BU)
	{
		x = BU;
		f = _itg_max;
	}
	else
	{
		const auto     xs      = x * float (RES);
		const auto     xs_int  = fstb::floor_int (xs);
		const auto     xs_frac = xs - float (xs_int);
		const auto &   seg     = _seg_arr [xs_int - BL * RES];
		f = fstb::Poly::eval (
			xs_frac,
			seg._poly_itg [0],
			seg._poly_itg [1],
			seg._poly_itg [2],
			seg._poly_itg [3],
			seg._poly_itg [4]
		);
	}

	return f;
}



template <int BL, int BU, class GF, int RES, bool UD>
bool	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::is_under_threshold (float &delta, float x0, float x1) noexcept
{
	delta = x0 - x1;
	auto           thr = _thr_rel * std::max (fabsf (x0), fabsf (x1));
	thr = std::max (thr, _thr_abs);

	return (fabsf (delta) < thr);
}



template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::eval_direct (float x) noexcept
{
	if (x <= BL)
	{
		return _val_min;
	}
	else if (x >= BU)
	{
		return _val_max;
	}

	const auto     xs      = x * float (RES);
	const auto     xs_int  = fstb::floor_int (xs);
	const auto     xs_frac = xs - float (xs_int);
	const auto     seg_idx = xs_int - BL * RES;
	const auto &   seg     = _seg_arr [seg_idx];
	const auto     y       = fstb::Poly::eval (
		xs_frac,
		seg._poly_std [0],
		seg._poly_std [1],
		seg._poly_std [2],
		seg._poly_std [3]
	);

	return y;
}



template <int BL, int BU, class GF, int RES, bool UD>
void	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::init_coefs () noexcept
{
	std::array <double, _table_size + 1> y;
	std::array <double, _table_size + 1> slope;
	GF             fnc;

	// First, samples the function and its derivative on all segment ends
	const auto     delta = 0.5 / double (_prec_frac);
	for (int pos = 0; pos < _table_size; ++pos)
	{
		const auto     x  = double (BL) + double (pos) / RES;

		const auto     y0 = double (fnc (x        ));
		const auto     ym = double (fnc (x - delta));
		const auto     yp = double (fnc (x + delta));

		y [pos]     = y0;
		slope [pos] = (yp - ym) * _prec_frac;
	}

	// Last safety segment (just after the real work area) is flat
	y [_table_size]     = y [_table_size - 1];
	slope [_table_size] = 0;

	_val_min = float (y [0              ]);
	_val_max = float (y [_table_size - 1]);

	// Computes the polynomial coefficients
	std::array <double, _table_size> offset;
	double         ofs_cur = 0;
	for (int pos = 0; pos < _table_size; ++pos)
	{
		Segment &      seg = _seg_arr [pos];

		const double   p   = slope [pos    ] / RES;
		const double   q   = slope [pos + 1] / RES;
		const double   dy  = y [pos + 1] - y [pos];

		// Coefficients in [0 ; 1] matching end point values and slopes
		// f (x) = c0 + c1 * x + c2 * x^2 + c3 * x^3
		// f'(x) = c1 + 2 * c2 * x + 3 * c3 * x^2
		// f (0) = c0
		// f'(0) = c1
		// f (1) = c0 + c1 + c2 + c3
		// f'(1) = c1 + 2 * c2 + 3 * c3
		// c0                        = y
		//      c1                   = p
		// c0 + c1 +     c2 +     c3 = y + dy
		//      c1 + 2 * c2 + 3 * c3 = q
		seg._poly_std [3] = float ( q +     p - 2 * dy);
		seg._poly_std [2] = float (-q - 2 * p + 3 * dy);
		seg._poly_std [1] = float (p);
		seg._poly_std [0] = float (y [pos]);

		// Integrates the polynomial and stores the constant in a separate
		// table with a higher numerical accuracy
		offset [pos]      = ofs_cur;
		seg._poly_itg [0] = float (ofs_cur); // Not necessary
		for (int k = 0; k <= _order; ++k)
		{
			const auto        c = seg._poly_std [k] / double ((k + 1) * RES);
			seg._poly_itg [k + 1] = float (c);

			// Computes the integrated value at the end of the segment (x = 1).
			ofs_cur += c;
		}
	}

	// If 0 is in the input range, sets the integral constant to get F (0) = 0.
	// This is not mandatory but should increase numerical accuracy around 0.
	if (BU * BL <= 0)
	{
		const auto     pos_zero = -BL * RES;
		const auto     delta_c  = offset [pos_zero];
		for (int pos = 0; pos < _table_size; ++pos)
		{
			Segment &      seg = _seg_arr [pos];
			seg._poly_itg [0] = float (offset [pos] - delta_c);
		}
	}

	_itg_min = _seg_arr.front ()._poly_itg [0];
	_itg_max = _seg_arr.back () ._poly_itg [0];

	_init_flag = true;
}



template <int BL, int BU, class GF, int RES, bool UD>
typename PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::SegmentTable	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_seg_arr;
template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_val_min   = 0;
template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_val_max   = 0;
template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_itg_min   = 0;
template <int BL, int BU, class GF, int RES, bool UD>
float	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_itg_max   = 0;
template <int BL, int BU, class GF, int RES, bool UD>
bool	PiecewisePoly3Adaa <BL, BU, GF, RES, UD>::_init_flag = false;



}  // namespace shape
}  // namespace dsp
}  // namespace mfx



#endif // mfx_dsp_shape_PiecewisePoly3Adaa_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
