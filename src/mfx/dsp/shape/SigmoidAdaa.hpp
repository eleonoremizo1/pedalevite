/*****************************************************************************

        SigmoidAdaa.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (mfx_dsp_shape_SigmoidAdaa_CODEHEADER_INCLUDED)
#define mfx_dsp_shape_SigmoidAdaa_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include <cmath>



namespace mfx
{
namespace dsp
{
namespace shape
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <bool UD>
float	SigmoidAdaa <UD>::operator () (float x) noexcept
{
	return process_sample (x);
}



template <bool UD>
float	SigmoidAdaa <UD>::process_sample (float x) noexcept
{
	if constexpr (UD)
	{
		return process_sample_vic (x);
	}
	else
	{
		return process_sample_std (x);
	}
}



template <bool UD>
void	SigmoidAdaa <UD>::clear_buffers () noexcept
{
	_xz  = 0;
	_fz  = 0;

	if constexpr (UD)
	{
		_xz2 = 0;
		_fzh = 0;
	}
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <bool UD>
float	SigmoidAdaa <UD>::process_sample_std (float x) noexcept
{
	// Eq. 9
	// A faster approximation could be used for sqrt() but requires accuracy.
	// fstb::Approx::sqrt() is correct but not transparent.
	const auto     f = sqrtf (1 + x * x);
	const auto     y = (_xz + x) / (_fz + f);

	_xz = x;
	_fz = f;

	return y;
}



template <bool UD>
float	SigmoidAdaa <UD>::process_sample_vic (float x) noexcept
{
	// Eq. 10
	const auto     xh  = (_xz + x) * 0.5f;
	const auto     fh  = sqrtf (1 + xh * xh);
	const auto     f   = sqrtf (1 + x  * x );

	const auto     xz3 = 3 * _xz;
	const auto     y2  = (_xz2 + xz3) / (_fzh + _fz);
	const auto     y1  = ( x   + xz3) / ( fh  + _fz);
	const auto     y   = 0.25f * (y2 + y1);

	_xz2 = _xz;
	_fzh = fh;
	_xz  = x;
	_fz  = f;

	return y;
}



}  // namespace shape
}  // namespace dsp
}  // namespace mfx



#endif // mfx_dsp_shape_SigmoidAdaa_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
