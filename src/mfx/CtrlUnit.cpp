/*****************************************************************************

        CtrlUnit.cpp
        Author: Laurent de Soras, 2016

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

*Tab=3***********************************************************************/



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/fnc.h"
#include "mfx/doc/CtrlLink.h"
#include "mfx/CtrlUnit.h"
#include "mfx/ProcessingContext.h"
#include "mfx/ToolsParam.h"

#include <cassert>
#include <cmath>



namespace mfx
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



CtrlUnit::CtrlUnit (const doc::CtrlLink &other, bool abs_flag)
:	_source (other._source)
,	_spd_mult (other._spd_mult)
,	_val (0)
,	_curve (other._curve)
,	_u2b_flag (other._u2b_flag)
,	_abs_flag (abs_flag)
,	_base (other._base)
,	_amp (other._amp)
,	_notch_list (other._notch_list)
,	_notch_radius (other._notch_radius)
{
	set_clip (
		other._clip_flag,
		other._clip_src_beg, other._clip_src_end,
		other._clip_dst_beg, other._clip_dst_end
	);
}



void	CtrlUnit::set_clip (bool enable_flag, float src_beg, float src_end, float dst_beg, float dst_end)
{
	assert (! enable_flag || src_beg < src_end);
	assert (! enable_flag || dst_beg < dst_end);

	_clip_flag     = enable_flag;
	_clip_src_beg  = src_beg;
	_clip_src_end  = src_end;
	_clip_dst_beg  = dst_beg;
	_clip_dst_end  = dst_end;

	if (_clip_flag)
	{
		_clip_mul =
			  (_clip_dst_end - _clip_dst_beg)
			/ (_clip_src_end - _clip_src_beg);
		_clip_add = _clip_dst_beg - _clip_src_beg * _clip_mul;
	}
	else
	{
		_clip_mul = 1;
		_clip_add = 0;
	}
}



bool	CtrlUnit::is_src_clipped () const
{
	return _clip_flag;
}



float	CtrlUnit::get_src_beg () const
{
	return _clip_src_beg;
}



float	CtrlUnit::get_src_end () const
{
	return _clip_src_end;
}



float	CtrlUnit::get_dst_beg () const
{
	return _clip_dst_beg;
}



float	CtrlUnit::get_dst_end () const
{
	return _clip_dst_end;
}



void	CtrlUnit::update_internal_val (float val_nrm)
{
	assert (_abs_flag);

	float               mod_val = val_nrm - _base;
	if (_amp != 0)
	{
		mod_val /= _amp;
	}
	mod_val = fstb::limit (mod_val, 0.f, 1.f);

	// Inverse curve
	mod_val = ControlCurve_apply_curve (mod_val, _curve, true);

	// Inverse clip mapping
	if (_clip_flag)
	{
		mod_val -= _clip_add;
		if (_clip_mul != 0)
		{
			mod_val /= _clip_mul;
		}
	}

	_val = mod_val;
}



void	CtrlUnit::update_abs_val (float raw_val)
{
	if (_source.is_relative ())
	{
		_val += raw_val * _spd_mult;
		_val = fstb::limit (_val, 0.f, 1.f);
	}
	else
	{
		_val = raw_val;
	}
}



// param_val is the original parameter value, or modulated with the previous
// modulations in the mod chain.
// param_val and the return value are normalized but can be out of the
// [0; 1] range, so clipping happens only on the final value.
float	CtrlUnit::evaluate (float param_val) const
{
	const float    mod_val = eval_mod (_val);

	if (_abs_flag)
	{
		param_val = _base + mod_val;
	}
	else
	{
		param_val += mod_val;
	}

	param_val = process_notches (param_val);

	return param_val;
}



float	CtrlUnit::eval_mod (float mod_val) const noexcept
{
	if (_u2b_flag)
	{
		mod_val = mod_val * 2 - 1;
	}

	if (_clip_flag)
	{
		mod_val = fstb::limit (mod_val, _clip_src_beg, _clip_src_end);
		mod_val *= _clip_mul;
		mod_val += _clip_add;
	}

	mod_val  = ControlCurve_apply_curve (mod_val, _curve, false);
	mod_val *= _amp;

	return mod_val;
}



std::optional <float>	CtrlUnit::eval_mod_inv (float mod_val_out) const noexcept
{
	constexpr auto thr = 1e-6f;

	auto           mod_val = mod_val_out;

	if (fabsf (_amp) < thr)
	{
		if (fabsf (mod_val) > thr)
		{
			return {};
		}
		else
		{
			mod_val = 0;;
		}
	}
	else
	{
		mod_val /= _amp;
	}

	const auto     val_save = mod_val;
	mod_val = ControlCurve_apply_curve (mod_val, _curve, true);
	const auto     val_test = ControlCurve_apply_curve (mod_val, _curve, false);
	constexpr auto thr2 = 1e-4f;
	if (fabsf (val_test - val_save) > thr2)
	{
		return {};
	}

	if (_clip_flag)
	{
		mod_val -= _clip_add;

		if (fabsf (_clip_mul) < thr)
		{
			if (fabsf (mod_val) > thr)
			{
				return {};
			}
			else
			{
				mod_val = 0;
			}
		}
		else
		{
			mod_val /= _clip_mul;
		}

		if (mod_val < _clip_src_beg || mod_val > _clip_src_end)
		{
			return {};
		}
	}

	if (_u2b_flag)
	{
		mod_val = (mod_val + 1) * 0.5f;
	}

	return mod_val;
}



float	CtrlUnit::process_notches (float param_val) const noexcept
{
	constexpr auto eps = 1e-6f; // Don't div by smth << eps

	if (! _notch_list.empty () && _notch_radius >= eps)
	{
		const auto     [it, it2] =
			ToolsParam::find_closest_notch (param_val, _notch_list);
		const auto     notch_val = *it;
		const auto     dif       = param_val - notch_val;
		const auto     dist      = fabsf (dif);
		if (dist <= _notch_radius)
		{
			// Transition length between snapped and un-snapped values
			// As a ratio of the notch radius.
			constexpr auto trans_ratio = 0.1f;
			auto           trans_len   = _notch_radius * trans_ratio;
			const auto     trans_beg   = _notch_radius - trans_len;
			const auto     trans_pos   = dist - trans_beg;
			if (trans_pos <= 0)
			{
				// We're on the flat area, not on the transition area
				return notch_val;
			}

			// If there is another neighbour notch
			if (it2 != _notch_list.end ())
			{
				// Checks if the notch areas are overlapping.
				// If so, shorten the transition length.
				const auto     dist_2n_h = fabsf (notch_val - *it2) * 0.5f;
				assert (dist <= dist_2n_h);
				const auto     len_avail = dist_2n_h - trans_beg;
				if (len_avail <= eps)
				{
					// Notches too close, the transition area doesn't even exist
					return notch_val;
				}
				trans_len = std::min (trans_len, len_avail);
			}

			// On the transition area: computes the resulting value.
			assert (trans_len > 0);
			const auto     pos   = trans_pos / trans_len;
			assert (pos >= 0);
			assert (pos <= 1);
			const auto     curve = pos * pos; // Parabolic curve
			param_val = notch_val + dif * curve;
		}
	}

	return param_val;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



}  // namespace mfx



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
