**Pédale Vite** is a DIY multi-FX pedalboard for guitar, bass or any other electric instrument.

It is based on a Raspberry Pi 5 and uses a custom audio board.

![Pédale Vite](https://ldesoras.fr/doc/articles/pedale-vite/pedalevite2-overview-small.jpg)

Key features:

- A large collection of effects within a single box: distortion, chorus, flanger, delay, reverb…
- Programs containing any free combination of the above effects
- 12 footswitches to activate the programs or other functionalities
- Fast and smooth transition between programs
- Up to 3 expression pedals to drive effects like wha, volume, LFOs…
- Infinite modulation possibilities
- Free effect routing
- Designed for stage and regular transport
- Silent and accurate tuner activated with a footswitch
- Standalone, no need for any external computer
- Building is easy and simple, can be reproduced and adapted by any person with minimal DIY skills (soldering…)
- Free software and hardware design
- And much more…

More information [here](https://ldesoras.fr/doc/articles/pedale-vite/pedale-vite-en.html).

The [/doc/](/doc/) folder contains everything you need to build the pedalboard.
